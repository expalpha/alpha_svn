
SUBDIRS = alphavmc/geant3/lib/tgt_$$RPLATFORM alphavmc rootana alphaAnalysis/lib alphaAnalysis alphaAnalysis/macros alphaStrips  alphaMVA/dumpers alphaMVA/tmva

all: extras BuildDB LinkDirs  $(SUBDIRS)  

BuildDB:
	$(info building aux/main.db)
	$(shell sqlite3 aux/main.db < aux/settings.sql)

LinkDirs:
	$(shell ln -s ../aux/magnets $$RELEASE/alphavmc/)
	$(shell ln -s ../aux/geo $$RELEASE/alphavmc/)
	$(shell ln -s ../../aux/magnets $$RELEASE/alphavmc/macros )
	$(shell ln -s ../../aux/geo $$RELEASE/alphavmc/macros)
	$(shell ln -s ../aux/magnets $$RELEASE/alphaAnalysis/)
	$(shell mkdir -p $$RELEASE/alphavmc/macros/MCfiles)
	$(shell mkdir -p $$RELEASE/alphavmc/macros/data)
	$(shell mkdir -p $$RELEASE/alphaAnalysis/data)
	$(shell mkdir -p $$RELEASE/alphaAnalysis/midasdata)
	$(shell ln -s ../alphaAnalysis/data $$RELEASE/alphaStrips/data)
	$(shell ln -s ../alphaAnalysis/midasdata $$RELEASE/alphaStrips/midasdata)

$(SUBDIRS):
	$(MAKE) -C $@ MAKEFLAGS=

.PHONY:clean
CLEANDIRS := $(SUBDIRS)
clean :
	rm aux/main.db 
	for dir in $(SUBDIRS); do \
		$(MAKE) -C $$dir clean; \
	done

extras:
ROOTANA_DIR := $(shell ls -d rootana | tail -n 1)
ifneq ("$(wildcard $(ROOTANA_DIR))","")
$(info rootana found)
else
$(info rootana not found... fetching from git)
$(shell echo "******************************" >> alphaINSTALL.log)
$(info '      Updating rootana      ' )
$(shell echo "*      Updating rootana      *" >> alphaINSTALL.log)
$(shell echo "******************************" >> alphaINSTALL.log)
$(shell git clone https://bitbucket.org/tmidas/rootana.git >>alphaINSTALL.log)
$(shell cd rootana && git submodule update --init >> alphaINSTALL.log)
$(shell cd rootana/libMidasServer && g++ -c -g -O2 -Wall -Wuninitialized `$ROOTSYS/bin/root-config --cflags` -I$ROOTSYS/include -I../include midasServer.cxx)
endif

GEANT3_DIR := $(shell ls -d alphavmc/geant3 | tail -n 1)
ifneq ("$(wildcard $(GEANT3_DIR))","")
$(info geant3 found)
else
$(info geany3 not found... fetching from git)
$(shell echo "******************************" >> alphaINSTALL.log)
$(info '      Updating geant3       ' )
$(shell echo "*      Updating geant3       *" >> alphaINSTALL.log)
$(shell echo "******************************" >> alphaINSTALL.log)
#$(shell cd $$RELEASE/alphavmc && git clone http://root.cern.ch/git/geant3.git >> alphaINSTALL.log && cd geant3 &&  git checkout v1-14 >> alphaINSTALL.log)

#$(shell cd $$RELEASE/alphavmc && git clone http://root.cern.ch/git/geant3.git >> alphaINSTALL.log && cd geant3 &&  git checkout v2-4 >> alphaINSTALL.log && mkdir build && cd build && cmake ../ && make -j8)
$(shell cd $$RELEASE/alphavmc && git clone http://github.com/vmc-project/geant3.git >> alphaINSTALL.log && cd geant3 &&  git checkout v2-5 >> alphaINSTALL.log )
$(shell mkdir -p $$RELEASE/alphavmc/geant3/lib/tgt_$$RPLATFORM && cd $$RELEASE/alphavmc/geant3/lib/tgt_$$RPLATFORM && cmake ../../  >> alphaINSTALL.log )
endif

ifeq (,$(ROOTANA_DIR))
  endif
ifneq (,$(ROOTANA_DIR))

endif

SPR:
	$(shell echo "******************************" >> alphaINSTALL.log)
	$(info '       Installing SPR       ' )
	$(shell echo "*       Installing SPR       *" >> alphaINSTALL.log)
	$(shell echo "******************************" >> alphaINSTALL.log)
	$(shell cd $$RELEASE/alphaMVA && bash installSPR.sh >> alphaINSTALL.log )
	$(shell echo "...done updating" >> alphaINSTALL.log)

.PHONY: $(SUBDIRS)
