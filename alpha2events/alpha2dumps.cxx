// ===========================================================================
//
// alphaonline
//
// ===========================================================================

#include <list>
#include <stdio.h>
#include <sys/time.h>
#include <signal.h>
#include <assert.h>
#include <iostream>
#include <time.h>
#include <stdint.h>
#include <math.h>

#define __STDC_FORMAT_MACROS
#include <inttypes.h>

#ifndef ROOT_TGDockableFrame
#include "TGDockableFrame.h"
#endif
#ifndef ROOT_TGMenu
#include "TGMenu.h"
#endif
#ifndef ROOT_TGMdiDecorFrame
#include "TGMdiDecorFrame.h"
#endif
#ifndef ROOT_TG3DLine
#include "TG3DLine.h"
#endif
#ifndef ROOT_TGMdiFrame
#include "TGMdiFrame.h"
#endif
#ifndef ROOT_TGMdiMainFrame
#include "TGMdiMainFrame.h"
#endif
#ifndef ROOT_TGMdiMenu
#include "TGMdiMenu.h"
#endif
#ifndef ROOT_TGListBox
#include "TGListBox.h"
#endif
#ifndef ROOT_TGNumberEntry
#include "TGNumberEntry.h"
#endif
#ifndef ROOT_TGScrollBar
#include "TGScrollBar.h"
#endif
#ifndef ROOT_TGComboBox
#include "TGComboBox.h"
#endif
#ifndef ROOT_TGuiBldHintsEditor
#include "TGuiBldHintsEditor.h"
#endif
#ifndef ROOT_TRootBrowser
#include "TRootBrowser.h"
#endif
#ifndef ROOT_TGuiBldNameFrame
#include "TGuiBldNameFrame.h"
#endif
#ifndef ROOT_TGFrame
#include "TGFrame.h"
#endif
#ifndef ROOT_TGFileDialog
#include "TGFileDialog.h"
#endif
#ifndef ROOT_TGShutter
#include "TGShutter.h"
#endif
#ifndef ROOT_TGButtonGroup
#include "TGButtonGroup.h"
#endif
#ifndef ROOT_TGCommandPlugin
#include "TGCommandPlugin.h"
#endif
#ifndef ROOT_TGCanvas
#include "TGCanvas.h"
#endif
#ifndef ROOT_TGFSContainer
#include "TGFSContainer.h"
#endif
#ifndef ROOT_TGuiBldEditor
#include "TGuiBldEditor.h"
#endif
#ifndef ROOT_TGColorSelect
#include "TGColorSelect.h"
#endif
#ifndef ROOT_TGTextEdit
#include "TGTextEdit.h"
#endif
#ifndef ROOT_TGButton
#include "TGButton.h"
#endif
#ifndef ROOT_TGFSComboBox
#include "TGFSComboBox.h"
#endif
#ifndef ROOT_TGLabel
#include "TGLabel.h"
#endif
#ifndef ROOT_TGView
#include "TGView.h"
#endif
#ifndef ROOT_TGMsgBox
#include "TGMsgBox.h"
#endif
#ifndef ROOT_TRootGuiBuilder
#include "TRootGuiBuilder.h"
#endif
#ifndef ROOT_TGFileBrowser
#include "TGFileBrowser.h"
#endif
#ifndef ROOT_TGTab
#include "TGTab.h"
#endif
#ifndef ROOT_TGListView
#include "TGListView.h"
#endif
#ifndef ROOT_TGSplitter
#include "TGSplitter.h"
#endif
#ifndef ROOT_TGTextEditor
#include "TGTextEditor.h"
#endif
#ifndef ROOT_TRootCanvas
#include "TRootCanvas.h"
#endif
#ifndef ROOT_TGStatusBar
#include "TGStatusBar.h"
#endif
#ifndef ROOT_TGListTree
#include "TGListTree.h"
#endif
#ifndef ROOT_TGuiBldGeometryFrame
#include "TGuiBldGeometryFrame.h"
#endif
#ifndef ROOT_TGToolTip
#include "TGToolTip.h"
#endif
#ifndef ROOT_TGToolBar
#include "TGToolBar.h"
#endif
#ifndef ROOT_TRootEmbeddedCanvas
#include "TRootEmbeddedCanvas.h"
#endif
#ifndef ROOT_TCanvas
#include "TCanvas.h"
#endif
#ifndef ROOT_TGuiBldDragManager
#include "TGuiBldDragManager.h"
#endif
#ifndef ROOT_TGHtmlBrowser
#include "TGHtmlBrowser.h"
#endif

#include "Riostream.h"
#include "TMidasOnline.h"
#include "TMidasEvent.h"
#include "TMidasFile.h"
#include "XmlOdb.h"
#include "midasServer.h"
#include "midas.h"

#include <TEnv.h>
#include <TROOT.h>
#include <TSystem.h>
#include <TClass.h>
#include <TApplication.h>
#include <TTimer.h>
#include <TFile.h>
#include <TDirectory.h>
#include <TFolder.h>
#include <TTree.h>
#include <TBranch.h>
#include <TError.h>
#include <TCanvas.h>
#include <TText.h>
#include <TMath.h>

#include "sinotation.cxx"

#include "TSpill.h"
#include "TSisEvent.h"
#include "TSeq_Event.h"
#include "TSeq_Dump.h"
#include "TSISChannels.h"

#include "Sequencer2.h"

using namespace std;


// <<< --- to be fixed 
#define CSI2 11
#define MCP 15
#define FC 16



#define MAXDET 8 // numero massimo di detector da considerare
#define SISCHAN 32 // numero di canali di UN sis
//#define NUMSEQ 4 // numero di sequencer


// per cambiare il nome associato a un sequencer, puoi modificare SeqNames; per permutare, cambia l'enum
#ifdef ALPHA1COMP
  #define  NUMSEQ 2
  TString SeqNames[NUMSEQ]={"PBAR","Mix"};
  enum {PBAR,MIX};
  enum {NOTADUMP,DUMP,EPDUMP};
#else
  #define NUMSEQ 9
  #define USED_SEQ 4
  TString SeqNames[NUMSEQ]={"cat","rct","atm","pos","rct_botg","atm_botg","atm_topg","rct_topg","bml"};
  enum {PBAR,RECATCH,ATOM,POS,RCT_BOTG,ATM_BOTG,ATM_TOPG,RCT_TOPG,BML};
  enum {NOTADUMP,DUMP,EPDUMP};  
#endif


// integral counts in PMT_CATCH_OR below
// this value result in a warning
Int_t HOT_DUMP_LOW_THR = 500;


Int_t det_ptr[MAXDET] = {0};
TTree* det_tree[MAXDET] = {NULL};
TSisEvent* det_event[MAXDET]  = {NULL};

Int_t sis_ptr[USED_SEQ] = {0};
TTree* sis_tree[USED_SEQ] = {NULL};
TSisEvent* sis_event[USED_SEQ]  = {NULL};

Int_t dump_ptr[USED_SEQ] = {0};
TTree* dump_tree[USED_SEQ] = {NULL};
TSisEvent* dump_event[USED_SEQ]  = {NULL};

TSeq_Dump * gPendingDump[USED_SEQ] = {NULL};


Int_t totalcnts[USED_SEQ]={0};
Int_t cSeq[NUMSEQ]={0}; // contatore del numero di sequenze, per tipo
Int_t gPendingSeqStart[USED_SEQ] = {0};

Bool_t pendingUpdates[USED_SEQ] = {kFALSE}; // used in SyncSeq

list<TSeq_Dump*>  ListOfDumps[USED_SEQ];
vector<Int_t> seq_startevent[USED_SEQ];
vector<Int_t> seq_counts[USED_SEQ] ;

Int_t timestamp[USED_SEQ] = {-1};

// sis channels

Int_t detectorCh[MAXDET]={-1};
TString detectorName[MAXDET];

Int_t tstampCh[USED_SEQ]={-1};
Int_t seqstartCh[USED_SEQ]={-1};
Int_t dumpstartCh[USED_SEQ]={-1};
Int_t dumpstopCh[USED_SEQ]={-1};

Int_t clock10MHzSIS1Ch=0;
Int_t clock10MHzSIS2Ch=0;

Int_t ADSpillCh=0;
Int_t PosTranf=0;

TSISChannels	    *gSISChannels = NULL; 


// Global Variables
bool gIsRunning = false;
bool gIsPedestalsRun = false;
bool gIsOffline = false;
Int_t  gRunNumber = 0;
Int_t gSpillNumber = 0;
Int_t gPosSpillNumber = 0;
TSpill * gPendingSpill = NULL;
Int_t gSpillLogEntry = -1;
Bool_t isPendingADE0Data = false;
double ade0data_buffer[3];
Bool_t fileCache=kTRUE; //Write to disk before sending to elog, necessary for very long runs

// labview stuff
Double_t gLastcsi2 = 0.;
Double_t gLastmcp = 0.;
Double_t gLastfc = 0.;


time_t gTime; // system timestamp of the midasevent

list<TSpill*> Spill_List;


TGMainFrame* fMainFrameGUI = NULL;
TGListBox* fListBoxSeq[4]; 
TGListBox* fListBoxLogger;
TGTextEdit* fTextEditBuffer;
TGTextButton *fTextButtonCopy;

TGNumberEntry* fNumberEntryDump[4];
TGNumberEntry* fNumberEntryTS[4];

#include <arpa/inet.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>

#define PORTNUM 2343

struct sockaddr_in dest; /* socket info about the machine connecting to us */
struct sockaddr_in serv; /* socket info about our server */
int mysocket;            /* socket used to listen for incoming connections */
int consocket;

void CatchUp(Bool_t dumps);
void SyncSeq(Int_t itype, Bool_t dumps);

void HandleLabviewADE0(int,const void *);
void CatchUpAndDraw(); 

TFile 				*gOutputFile = NULL;
VirtualOdb	 		*gOdb = NULL;



class alphaFrame: public TGMainFrame {
  
public: 
  
  alphaFrame();
  alphaFrame(const char* name, const char* description);
  
  virtual ~alphaFrame();
  
  Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t parm2);
  void CloseWindow();


};
  
alphaFrame::alphaFrame(){
  alphaFrame("alphaFrame","alpha Frame");
}

alphaFrame::alphaFrame(const char* name, const char* description)
  : TGMainFrame(gClient->GetRoot(),10,10,kMainFrame | kVerticalFrame){

}

alphaFrame::~alphaFrame(){

}

Bool_t alphaFrame::ProcessMessage(Long_t msg, Long_t parm1, Long_t parm2){

  if ( (int)msg == 259 && parm1 == 100001) {

   fTextEditBuffer->RemoveAll();
   fTextEditBuffer->Layout();

   int nentries = fListBoxLogger->GetNumberOfEntries();

   if ( nentries > 0) { 

     //     TGTextLBEntry* lbentry = (TGLBEntry*)fListBoxLogger->GetSelectedEntry();
     TGTextLBEntry* lbentry = (TGTextLBEntry*)fListBoxLogger->GetSelectedEntry();
     if (!lbentry){
       lbentry = (TGTextLBEntry*)fListBoxLogger->Select(nentries - 1);
     }
     
     TGString str = lbentry->GetText();
     fTextEditBuffer->LoadBuffer( str.Data() ) ;

   }
  
   fTextEditBuffer->Layout();
   fTextEditBuffer->SelectAll();
   fTextEditBuffer->Copy();
  
   return kTRUE;

  }

  return kFALSE;

}


void alphaFrame::CloseWindow(){

  gApplication->Terminate(0);
  // xapp
}

Int_t getIntegral( TTree * tree, TSisEvent * event, Double_t start, Double_t stop);

void InitSISChannels(int numChan=SISCHAN){


  if (gSISChannels) delete gSISChannels;
  gSISChannels = new TSISChannels(gRunNumber);
  
  ADSpillCh = gSISChannels->GetChannel("SIS_AD",gRunNumber);
  PosTranf = gSISChannels->GetChannel("POS_TRANS",gRunNumber);
  //  printf("ADSpillCh: %d\n", ADSpillCh);

  clock10MHzSIS1Ch = gSISChannels->GetChannelInRange("SIS_10Mhz_CLK",gRunNumber,0*numChan,(0+1)*numChan); 
  clock10MHzSIS2Ch = gSISChannels->GetChannelInRange("SIS_10Mhz_CLK",gRunNumber,1*numChan,(1+1)*numChan); 
  
  tstampCh[PBAR] = gSISChannels->GetChannel("SIS_PBAR_SEQ_TS",gRunNumber);
  seqstartCh[PBAR] = gSISChannels->GetChannel("SIS_PBAR_SEQ_START",gRunNumber);
  dumpstartCh[PBAR] = gSISChannels->GetChannel("SIS_PBAR_DUMP_START",gRunNumber);
  dumpstopCh[PBAR] = gSISChannels->GetChannel("SIS_PBAR_DUMP_STOP",gRunNumber);

  tstampCh[RECATCH] = gSISChannels->GetChannel("SIS_RECATCH_SEQ_TS",gRunNumber);
  seqstartCh[RECATCH] = gSISChannels->GetChannel("SIS_RECATCH_SEQ_START",gRunNumber);
  dumpstartCh[RECATCH] = gSISChannels->GetChannel("SIS_RECATCH_DUMP_START",gRunNumber);
  dumpstopCh[RECATCH] = gSISChannels->GetChannel("SIS_RECATCH_DUMP_STOP",gRunNumber);

  tstampCh[ATOM] = gSISChannels->GetChannel("SIS_ATOM_SEQ_TS",gRunNumber);
  seqstartCh[ATOM] = gSISChannels->GetChannel("SIS_ATOM_SEQ_START",gRunNumber);
  dumpstartCh[ATOM] = gSISChannels->GetChannel("SIS_ATOM_DUMP_START",gRunNumber);
  dumpstopCh[ATOM] = gSISChannels->GetChannel("SIS_ATOM_DUMP_STOP",gRunNumber);
  
  tstampCh[ATOM] = gSISChannels->GetChannel("SIS_POS_SEQ_TS",gRunNumber); //Does not exist!
  seqstartCh[POS] = gSISChannels->GetChannel("SIS_POS_SEQ_START",gRunNumber); //Does not exist!
  dumpstartCh[POS] = gSISChannels->GetChannel("SIS_POS_DUMP_START",gRunNumber);
  dumpstopCh[POS] = gSISChannels->GetChannel("SIS_POS_DUMP_STOP",gRunNumber);
  
  detectorCh[0] = gSISChannels->GetChannel("SIS_PMT_CATCH_OR");
  detectorName[0] = "CATCH_OR";
  detectorCh[1] = gSISChannels->GetChannel("SIS_PMT_CATCH_AND");
  detectorName[1] = "CATCH_AND";
  detectorCh[2] = gSISChannels->GetChannel("SIS_PMT_ATOM_OR");
  detectorName[2] = "ATOM_OR";
  detectorCh[3] = gSISChannels->GetChannel("SIS_PMT_ATOM_AND");
  detectorName[3] = "ATOM_AND";
  detectorCh[4] = gSISChannels->GetChannel("PMT_12_AND_13");
  detectorName[4] = "CTSTICK";
  detectorCh[5] = gSISChannels->GetChannel("IO32_TRIG_NOBUSY");
  detectorName[5] = "IO32_TRIG";
  detectorCh[6] = 42;//gSISChannels->GetChannel("SIS_PMT_10_AND_PMT_11");
  //detectorCh[6] = 40;//gSISChannels->GetChannel("PMT_10");
  detectorName[6] = "ATOMSTICK";
  
  detectorCh[7] = 47;
  detectorName[7] = "NewATOMSTICK";
  //detectorName[6] = "ATOMSTICK (PMT9)";

  //  SIS_DUMP_START      
  //  SIS_DUMP_STOP       


}

void LayoutListBox(TGListBox* fLb){

  fLb->Layout();
  TGVScrollBar* fsb = fLb->GetVScrollbar();
  fsb->SetPosition(fsb->GetRange());
  fLb->Layout();

}


void Messages(TSeq_Dump* se){
  
  // perform routine checks and issues vocal alarms
  if( strncmp( se->GetDescription().Data(), "Hot", 3) == 0 &&
      se->GetDetIntegral(0) < HOT_DUMP_LOW_THR ) // [0] -> SIS_PMT_CATCH_OR
    cm_msg(MTALK,"alpha2dumps","Warning Hot Dump is low");
  
}


Int_t IsStartDump(TSeq_Event* se) {
  if ( strncmp( se->GetEventName().Data(), "startDump", 9 ) == 0 ) 
    return DUMP;
  else if ( strncmp( se->GetEventName().Data(), "startDump_e+", 12 ) == 0 )
    return EPDUMP;
  else 
    return NOTADUMP;
}

Int_t IsStopDump(TSeq_Event* se) {
  if ( strncmp( se->GetEventName().Data(), "stopDump",8) == 0 ) 
    return DUMP;
  else if ( strncmp( se->GetEventName().Data(), "stopDump_e+", 11 ) == 0 ) 
    return EPDUMP;
  else 
    return NOTADUMP;
}

Int_t gHot, gCold;
Double_t gts;

Bool_t IsHotDump(TSeq_Dump* se) 
{
  if((se->GetDescription().BeginsWith(TString("Hot"), TString::kIgnoreCase))) 
    {
      gHot=se->GetDetIntegral(0);
      return kTRUE;
    }
  else return kFALSE;
}

Bool_t IsColdDump(TSeq_Dump* se) 
{
  if((se->GetDescription().BeginsWith(TString("Cold"), TString::kIgnoreCase)))
    {
      gts = se->GetStoponTime();
      gCold = se->GetDetIntegral(0);
      return kTRUE;
    }
  else return kFALSE;
}

void SendData(TSeq_Dump* sd)
{
  socklen_t socksize = (socklen_t) sizeof(struct sockaddr_in);
  //printf("socket: %d\t size: %d\n",mysocket,socksize);
 
  /* start listening, allowing a queue of up to 1 pending connection */
  consocket = accept(mysocket, (struct sockaddr *)&dest, &socksize);
//  printf("Connection Accepted? %d\n",consocket);
  
  char msg[25];
  sprintf(msg,"%5.2f\t%5.0d\t%5.0d\n",gts,gHot,gCold);
  
  if(consocket)
    {
     // printf("Incoming connection from %s - sending data\n", inet_ntoa(dest.sin_addr));
      //      int stat = send(consocket, msg.Data(), strlen(msg.Data()), 0 );
     // printf("%s\t",msg);
      /*int stat =*/ send(consocket, msg, strlen(msg), 0 );
    //  printf("pkg stat %d\n",stat);
    }  
  close(consocket);
}

Bool_t MatchDumpDescription(TSeq_Event* se1, TSeq_Event* se2) {
  return ( strcmp(se1->GetDescription().Data(), se2->GetDescription().Data()) == 0 ); 
}

void FormatHeader(TString* log){

  char buf[300];

  //  *log += "                     | "; // indentation     
  *log += "                "; // indentation     


  sprintf(buf,"%-21s","Dump Time");
  *log += buf;

  sprintf(buf,"| %-33s        | ","CAT Event       RCT Event       ATM Event       POS Event"); // description 
  *log += buf;


  for (int iDet = 0; iDet<MAXDET; iDet++){
    sprintf(buf,"%-9s ", detectorName[iDet].Data());
    *log += buf;
  }


}

// to be fixed 
void DrawSpills(Bool_t endofrun);



static double GetTimeSec()
{
  struct timeval tv;
  gettimeofday(&tv,NULL);
  return tv.tv_sec + 0.000001*tv.tv_usec;
}

class MyPeriodic : public TTimer
{
public:
  typedef void (*TimerHandler)(void);

  int          fPeriod_msec;
  TimerHandler fHandler;
  double       fLastTime;
  bool         fInHandler;

  MyPeriodic(int period_msec,TimerHandler handler)
  {
    assert(handler != NULL);
    fPeriod_msec = period_msec;
    fHandler  = handler;
    fLastTime = GetTimeSec();
    fInHandler = false;
    Start(period_msec,kTRUE);
  }

  Bool_t Notify()
  {
    // kill recursive calls into Timer handlers
    if (fInHandler)
      return kTRUE;

    double t = GetTimeSec();
    //printf("timer notify, period %f should be %f!\n",t-fLastTime,fPeriod_msec*0.001);

    if (t - fLastTime >= 0.9*fPeriod_msec*0.001)
      {
        fInHandler = true;
        //printf("timer: call handler %p\n",fHandler);
        if (fHandler)
          (*fHandler)();

        fLastTime = t;
        fInHandler = false;
      }

    Reset();
    return kTRUE;
  }

  ~MyPeriodic()
  {
    TurnOff();
  }
};


uint64_t gSaveClock = 0; // SIS1 Clock (10MHz) 
uint64_t gSaveClock_2 = 0; // SIS2 Clock (10MHz)

double clock2time(uint64_t clock, uint64_t offset ) {
  const double freq = 10000000.0; // 10 MHz AD clk
  return (clock - offset)/freq;
}

Double_t entry2time( TTree * tree, TSisEvent * event, Int_t entry ) {
  tree->GetEntry(entry);
  return event->GetRunTime();
}

TString LogSpills() {

  TString log = "";


  TGString logstr = "";
  for (int i = 0; i < fListBoxLogger->GetNumberOfEntries(); i++){
    TGString logstr = ((TGTextLBEntry*)fListBoxLogger->GetEntry(i))->GetText(); 
    log += TString::Format("%s\n",logstr.Data());
  }
  std::cout << std::endl << "--- Run summary: ---" << std::endl;
  std::cout << log.Data() << std::endl << std::endl;


  for (int iSeqType = 0; iSeqType < USED_SEQ; iSeqType++){

    list<TSeq_Dump*>::iterator itd;
    for ( itd=ListOfDumps[iSeqType].begin() ; itd != ListOfDumps[iSeqType].end(); itd++ ){
      TSeq_Dump * sd = (TSeq_Dump*)*itd;

      if(sd->IsDone()) continue;

      log += "LogSpills: Msg: INCOMPLETE EVENT:";
      log += sd->GetDescription().Data();
      log += "\n";
    }
  }

  return log;
}

void DrawSpills(Bool_t endofrun = kFALSE) 
{

  if( gIsRunning == kFALSE && endofrun == kFALSE ) return;

  cout<<"Draw Spills"<<endl;

  double y = 0.1;
  double y_step = 0.05;
  //  double x = 0.05;

  list<TSpill*>::reverse_iterator it;
  for ( it=Spill_List.rbegin() ; it != Spill_List.rend(); ++it ) {
  
    TSpill * s = (TSpill*)*it;

    s->SetYStep(y_step);
    //Colour set but not used... removing Sept 2017
    //Int_t colour = kBlack;
    //if( it == Spill_List.rbegin() ) // lo spill piu` recente (quello corrente) e` evidenziato in rosso
    //  colour = kRed;
    
    y+= y_step;
    
    if( y > 1. )
      break;
    
    int m = s->GetNumDump();
    y += y_step * m;
        
    //    s->PrintSpill( gCanvas, x,y, colour );
    
  }
  
  //  gCanvas->Modified();
  //  gCanvas->Update();

  /*
  if( gIsOffline == false )   {
    char filename[80];
    sprintf(filename,"~/public_html/alphadumps/events_last_spill.gif");
    gCanvas->Print(filename,"gif");
    sprintf(filename,"~/public_html/alphadumps/events_run_%5d_spill_%d.gif",gRunNumber,gSpillNumber);
    // gCanvas->Print(filename,"gif");
  }
  */

}





FILE * gDumpLog;

void startRun(int transition,int run,int time) {

  // 

  gIsRunning = true;
  gRunNumber = run; // this has to be defined _before_ InitSISChannels
  gSpillNumber = 0;
  gPosSpillNumber = 0;
  InitSISChannels();  

  // reinitialize the clocks
  gSaveClock = 0;
  gSaveClock_2 = 0;

  fListBoxLogger->RemoveAll();
  LayoutListBox(fListBoxLogger);

  // reset the sequence vectors and sis trees
  for (int iSeqType=0; iSeqType<USED_SEQ; iSeqType++){

    fListBoxSeq[iSeqType]->RemoveAll();
    LayoutListBox(fListBoxSeq[iSeqType]);

    fNumberEntryTS[iSeqType]->SetIntNumber(0);
    fNumberEntryTS[iSeqType]->Layout();

    fNumberEntryDump[iSeqType]->SetIntNumber(0);
    fNumberEntryDump[iSeqType]->Layout();

    ListOfDumps[iSeqType].clear();
    seq_startevent[iSeqType].clear();
    seq_counts[iSeqType].clear();

    gPendingDump[iSeqType] = NULL;
    gPendingSeqStart[iSeqType] = 0; 
    timestamp[iSeqType] = 0; // <<<<< --- controlla 
    cSeq[iSeqType] = 0; // <<<<< --- controlla 

    dump_tree[iSeqType] = new TTree("dump_tree","dump_tree");
    dump_event[iSeqType] = new TSisEvent();
    dump_tree[iSeqType]->Branch("SisEvent","TSisEvent",&dump_event[iSeqType],16000,1);
    // dump_tree[iSeqType]->Branch("SisEvent","TSisEvent",&dump_event[iSeqType],16000,1);
 
    sis_tree[iSeqType] = new TTree("sis_tree","sis_tree");
    sis_event[iSeqType] = new TSisEvent();
    sis_tree[iSeqType]->Branch("SisEvent","TSisEvent",&sis_event[iSeqType],16000,1);
    //    sis_tree[iSeqType]->Branch("SisEvent","TSisEvent",&sis_event[iSeqType],16000,1);

  }

  // initialize detector trees
  for (int iDet=0; iDet<MAXDET; iDet++){
    //    cout << detectorCh[iDet] << endl;

    if (detectorCh[iDet]>-1) {
      det_tree[iDet] = new TTree("sis_tree","sis_tree");
      det_tree[iDet]->SetCircular(10000000); //10 Million detector events
      det_event[iDet] = new TSisEvent();
      det_tree[iDet]->Branch("SisEvent","TSisEvent",&det_event[iDet],16000,1);
    }
  }

  TSpill *s = new TSpill( gRunNumber, gSpillNumber, gTime, MAXDET);
  gPendingSpill = s;
  Spill_List.push_back(s);
  
  char message[30];
  sprintf(message,"Run %d",gRunNumber);

  fListBoxLogger->AddEntrySort(TGString(message),fListBoxLogger->GetNumberOfEntries());
  LayoutListBox(fListBoxLogger);

  TString log;
  FormatHeader(&log);
  fListBoxLogger->AddEntrySort(TGString(log.Data()),fListBoxLogger->GetNumberOfEntries());
  LayoutListBox(fListBoxLogger);  

  log = "";
  for (int i=0; i<174; i++){
    log+="-";
  }

  fListBoxLogger->AddEntrySort(TGString(log.Data()),fListBoxLogger->GetNumberOfEntries());
  LayoutListBox(fListBoxLogger);  

  /*
  for (int i = 0; i< MAXDET; i++){
    cout <<det_ptr[i] << " " ;
  }
  cout << endl;

  for (int i = 0; i< USED_SEQ; i++){
    cout <<sis_ptr[i] << " " ;
  }
  cout << endl;

  for (int i = 0; i< USED_SEQ; i++){
    cout <<dump_ptr[i] << " " ;
  }
  cout << endl;
  */

}





void endRun(int transition,int run,int time)
{

  CatchUpAndDraw();


  gIsRunning = false;
  gRunNumber = run;


  char message[30];
  sprintf(message,"End run %d",gRunNumber);
  fListBoxLogger->AddEntrySort(TGString(message),fListBoxLogger->GetNumberOfEntries());
  LayoutListBox(fListBoxLogger);

  
  // write results to file
  TString log = "";

  
  TGString logstr = "";
  for (int i = 0; i < fListBoxLogger->GetNumberOfEntries(); i++){
    TGString logstr = ((TGTextLBEntry*)fListBoxLogger->GetEntry(i))->GetText(); 
    log += TString::Format("%s\n",logstr.Data());
  }
  std::cout << std::endl << "--- Run summary: ---" << std::endl;
  std::cout << log.Data() << std::endl << std::endl;
  
  if( !gIsOffline )  {
    char cmd[1024000];
    if (fileCache)
    {
      //TString DataLoaderPath=outfileName(0,outfileName.Length()-5);
      TString spillLogName="R";
      spillLogName+=gRunNumber;
      spillLogName+=".log";
      std::cout <<"Log file: "<<spillLogName<<std::endl;
      std::ofstream spillLog (spillLogName);
      spillLog<<"[code]"<<log.Data()<<"[/code]"<<std::endl;
      spillLog.close();
      sprintf(cmd,"cat %s | ssh -x alphadaq ~/packages/elog/elog -h localhost -p 8080 -l SpillLog -a Run=%d -a Author=alpha2dumps &",spillLogName.Data(),gRunNumber);
      printf("--- Command: \n%s\n", cmd);
      system(cmd);
    }
    else
    {
      sprintf(cmd,"echo \'[code]%s[/code]\' | ssh -x alphadaq ~/packages/elog/elog -h localhost -p 8080 -l SpillLog -a Run=%d -a Author=alpha2dumps &", log.Data(), gRunNumber);
      printf("--- Command: \n%s\n", cmd);
      system(cmd);
    }
  } 


  // cleanup result trees
  for (int iDet=0; iDet<MAXDET; iDet++){
    if(det_tree[iDet]){
      delete det_tree[iDet]; det_tree[iDet] = NULL;
    }
    if(det_event[iDet]){
      delete det_event[iDet]; det_event[iDet] = NULL;
    }
  }

  for (int iSeqType=0; iSeqType<USED_SEQ; iSeqType++){
    if(sis_tree[iSeqType]){
      delete sis_tree[iSeqType]; sis_tree[iSeqType] = NULL;
    }
    if(sis_event[iSeqType]){
      delete sis_event[iSeqType]; sis_event[iSeqType] = NULL;
    }

    if(dump_tree[iSeqType]){
      delete dump_tree[iSeqType]; dump_tree[iSeqType] = NULL;
    }
    if(dump_event[iSeqType]){
      delete dump_event[iSeqType]; dump_event[iSeqType] = NULL;
    }
  }
}





// Strip names of special characters 
TString Sanitize( TString s ) {
  TString ss = (TString) s.Strip((TString::EStripType)3,'\n');
  TString sss = (TString) ss.Strip((TString::EStripType)3,'.');
  TString ssss = (TString) sss.Strip((TString::EStripType)3,'\"');
  return ssss;
}


// Sequencer XML parsing interface
char sequheader[400000];

void HandleSeq2( char * sequdata, int size )
{

  //cout<<"HandleSeq2"<<endl;

  char *shdr = sequheader;
  for(int i = 0;i<40000 && (*sequdata!=60);i++) { //get the first line ; char 60 is "<"      //  && (*sequdata!=10) && (*sequdata!=0)(*sequdata!=13) 
    *shdr++=*sequdata++;
  }
  *shdr++ = 0;

  
  TDOMParser *fParser = new TDOMParser();
  fParser->SetValidate(false);
	
  int bufLength = strlen(sequdata);
  char*buf = (char*)malloc(bufLength);
  memcpy(buf, sequdata, bufLength);
  
  for (int i=0; i<bufLength; i++)
    if (!isascii(buf[i]))
      buf[i] = 'X';
    else if (buf[i] == 0x1D)
      buf[i] = 'X';
  
  int parsecode = fParser->ParseBuffer(buf,bufLength);
    
  if (parsecode < 0 ) {
    std::cerr << fParser->GetParseCodeMessage(parsecode) << std::endl;
    return;
  }  
  free (buf);
  
  TXMLNode * node = fParser->GetXMLDocument()->GetRootNode();
  SeqXML* mySeq = new SeqXML(node);

  Int_t iSeqType=-1;
  // PBAR, RECATCH, ATOM definiti in un enum, precedentemente
  for (int i=0; i<NUMSEQ; i++)
  {
    if( strcmp( ((TString)mySeq->getSequencerName()).Data(), SeqNames[i].Data()) == 0 )
    {
      iSeqType = i;
      break;
    }
  }
  /*
  if( strcmp( ((TString)mySeq->getSequencerName()).Data(), SeqNames[PBAR].Data()) == 0 ) {
    iSeqType = PBAR;
  }
  else if( strcmp( ((TString)mySeq->getSequencerName()).Data(), SeqNames[RECATCH].Data()) == 0 ) {
    iSeqType = RECATCH;
  }
  else if( strcmp( ((TString)mySeq->getSequencerName()).Data(), SeqNames[ATOM].Data()) == 0 ) {
    iSeqType = ATOM;
  }
  else if ( strcmp( ((TString)mySeq->getSequencerName()).Data(), SeqNames[POS].Data()) == 0 ) {
    iSeqType = POS;
  }
  else {
    iSeqType = -1;
    cout << "unknown sequencer name: " << ((TString)mySeq->getSequencerName()).Data() << endl;
  
  }*/
  if (iSeqType<0)
  {
    cout << "unknown sequencer name: " << ((TString)mySeq->getSequencerName()).Data() << endl;
    return;
    //assert(0);
  }  
  if (iSeqType>=USED_SEQ)
  {
    cout <<"Not using sequencer: " << ((TString)mySeq->getSequencerName()).Data() << endl;
    return;
  }
  cSeq[iSeqType]++;
  std::cout <<"iSeqType:"<<std::endl;
  TString msgfirst = TString::Format("--- %d ---",cSeq[iSeqType]);

  fListBoxSeq[iSeqType]->AddEntrySort(msgfirst.Data(),fListBoxSeq[iSeqType]->GetNumberOfEntries());
  LayoutListBox(fListBoxSeq[iSeqType]);

  // first, make a list of all the sequencer events
  list<TSeq_Event*> Seq_List;

  SeqXML_ChainLink *cl;
  TIter myChains((TObjArray*)mySeq->getChainLinks(), true); 
  while((cl = (SeqXML_ChainLink *) myChains.Next())) {        
  
    SeqXML_Event *event;
    TIter myEvents(cl->getEvents());
    while((event = (SeqXML_Event *) myEvents.Next())) {     

      TSeq_Event * s = new TSeq_Event();

      s->SetSeq( mySeq->getSequencerName() );
      s->SetSeqNum(cSeq[iSeqType]); 

      s->SetID( event->GetID() );
      s->SetEventName( event->GetNameTS() );
      s->SetDescription( Sanitize(event->GetDescription()) );
      s->SetonCount( event->GetCount() );
      s->SetonState( event->GetStateID() );
      Seq_List.push_back(s);
    }

    totalcnts[iSeqType] += cl->gettotalCnt(); //  non utilizzato, per ora    
  }

  // construct a dump event list 

  // look for a start dump 
  // doesn't behave well with nested dumps, if they have the same description
  list<TSeq_Event*>::iterator itstart;
  list<TSeq_Event*>::iterator itend;
  for ( itstart=Seq_List.begin() ; itstart != Seq_List.end(); itstart++ ) 
  {
    TSeq_Event * se = (TSeq_Event*)*itstart;
    Int_t testStartDump = IsStartDump(se);
    if( testStartDump > 0 ) 
    {
      // look the first occurrence of an associated stop dump
      for ( itend=itstart ; itend != Seq_List.end(); itend++ ) 
      {
        TSeq_Event * se_end = (TSeq_Event*)*itend;
        if ( IsStopDump(se_end) == testStartDump ) 
        {
          if ( MatchDumpDescription(se,se_end) ) 
          {
            TSeq_Dump * dump = new TSeq_Dump();
            dump->SetSeq( se->GetSeq() );
            dump->SetSeqNum( se->GetSeqNum() );
            dump->SetID( se->GetID() );
            dump->SetEventName( se->GetEventName() );
            dump->SetDescription( se->GetDescription() );
            dump->SetStartonCount( se->GetonCount() );
            dump->SetStoponCount( se_end->GetonCount() );
            dump->SetDone( kFALSE );
            dump->SetStarted( kFALSE );
            
            if (testStartDump == EPDUMP) 
              dump->SetDone( kTRUE ); // this prevents these dumps to be treated as regular dumps 

            ListOfDumps[iSeqType].push_back(dump);

            
            //            TString msg = TString::Format("(%s) %s",SeqNames[iSeqType].Data(), dump->GetDescription().Data());
            TString msg = TString::Format("%s", dump->GetDescription().Data());

            fListBoxSeq[iSeqType]->AddEntrySort(msg.Data(),fListBoxSeq[iSeqType]->GetNumberOfEntries());
            LayoutListBox(fListBoxSeq[iSeqType]);

            //            printf("%s\n",se->GetDescription().Data());
            break;
          }
        }
      }

    }

  }



  // Print the list
  list<TSeq_Dump*>::iterator it;
  for ( it=ListOfDumps[iSeqType].begin() ; it != ListOfDumps[iSeqType].end(); it++ )
  {
    TSeq_Dump * sd = (TSeq_Dump*)*it;
    
    //    printf("HandleSeq2 - Msg: %s %d %d %d %d\n",sd->GetDescription().Data(),
    //           sd->GetSeqNum(),sd->GetStartonCount(),sd->GetStoponCount(),sd->IsDone()); 

    char message[256];
    sprintf(message,"%s (%d)",sd->GetDescription().Data(),sd->GetSeqNum());


    //    cout << iSeqType << endl;
    //    cout << fListBoxSeq[iSeqType] << endl;
    //    cout << fListBoxSeq[iSeqType]->GetNumberOfEntries() << endl;

    // 
    //    fListBoxSeq[iSeqType]->AddEntrySort("prova",fListBoxSeq[iSeqType]->GetNumberOfEntries());
    //    fListBoxSeq[iSeqType]->Layout();
        
  }
  //  cout << ListOfDumps[iSeqType].size() << endl;


  //  printf("HandleSeq2 - Msg: End\n");
  while(!Seq_List.empty()) delete Seq_List.front(), Seq_List.pop_front();
  Seq_List.clear();

  delete mySeq;
  delete fParser;


  
}


void fill_tree( TTree * tree, TSisEvent * event, Int_t Channel, Int_t Counts, uint64_t Time, uint64_t Time_Offset) {
  event->ClearSisEvent();
  event->SetChannel( Channel );
  event->SetCountsInChannel((Int_t)Counts);
  event->SetClock(Time);
  event->SetRunTime(clock2time(Time,Time_Offset));
  event->SetRunNumber(gRunNumber);
  
  tree->Fill();
}

Int_t getIntegral( TTree * tree, TSisEvent * event, Double_t start, Double_t stop) {

  // binary search to find the first entry
  Int_t start_entry = 0;

  Int_t low = 0;
  Int_t high = tree->GetEntries()-1;
  while (low < high) {
    Int_t mid = Int_t((low + high)/2.);
    tree->GetEntry(mid);
    if (event->GetRunTime() < start)
      low = mid + 1; 
    else
      //can't be high = mid-1: here A[mid] >= value,
      //so high can't be < mid if A[mid] == value
      high = mid; 
  }

  tree->GetEntry(low); 
  if ((low < (tree->GetEntries()-1)) && (event->GetRunTime() == start))
    start_entry = low; // found
  
  // calculate the integral
  Int_t integral = 0;
  for( Int_t i = start_entry; i<tree->GetEntries(); i++ ) {
    tree->GetEntry(i);
    if( event->GetRunTime() < start ) continue;
    if( event->GetRunTime() > stop ) break;
    integral += event->GetCountsInChannel();     
  }
  return integral;
}


void UpdateDumpIntegrals(TSeq_Dump* se){

  // update event container;

  for (int iDet=0; iDet<MAXDET; iDet++){

    if (detectorCh[iDet]>-1){
      Int_t val = getIntegral(det_tree[iDet], det_event[iDet], se->GetStartonTime(), se->GetStoponTime());

     // cout <<"Channel " <<detectorCh[iDet] <<"  Integral "<< val << endl;

      se->SetDetIntegral( iDet, val);
    }

  }

}



void SISCounts( Int_t Channel, Int_t Counts, uint64_t Time, uint64_t Time_Offset) {


   //printf("Channel: %d, Counts: %d ; start=%d, stop=%d; Time= %" PRIu64 "\n", Channel, Counts, dumpstartCh[ATOM], dumpstopCh[ATOM], Time);

  //If channel 0 (Timestamp channel, don't bother)...
  if (!Channel) return;
  // increment the spill number
  if( Channel == ADSpillCh ) {
    //    CatchUpAndDraw(); // < --- 
    gSpillNumber++;
    
    TSpill *s = new TSpill( gRunNumber, gSpillNumber, gTime, MAXDET );
    gPendingSpill = s;
    Spill_List.push_back(s);
    
    
     printf("AD spill\n");
    TString log = "";
    s->FormatADInfo(&log);
    gSpillLogEntry = fListBoxLogger->GetNumberOfEntries();

    TGFont* lfont = gClient->GetFontPool()->GetFont("courier",12,kFontWeightNormal,kFontSlantItalic); 
    
    if (!lfont){
      exit(123);
    }

    TGTextLBEntry* lbe = new TGTextLBEntry(fListBoxLogger->GetContainer(), new TGString(log.Data()), gSpillLogEntry,  TGTextLBEntry::GetDefaultGC()(), lfont->GetFontStruct());
    TGLayoutHints *lhints = new TGLayoutHints(kLHintsExpandX | kLHintsTop);

    fListBoxLogger->AddEntrySort(lbe,lhints);

    //    fListBoxLogger->AddEntrySort(TGString(log.Data()),gSpillLogEntry);
    LayoutListBox(fListBoxLogger);
    // <-----
    
    if(isPendingADE0Data)
    {
		isPendingADE0Data = false;
		HandleLabviewADE0(3,ade0data_buffer);
		
	}
  }

  if (Channel == PosTranf ) {
	  gPosSpillNumber++;
	  printf("Positrons!\n");
	  struct tm  *ts;
      char       time[80];      
      ts = localtime(&gTime);
      strftime(time, sizeof(time), "%H:%M:%S", ts);
      char message[200];
      sprintf(message," Positron Transfer %d   [%s]    %.2fs                                                                <------------------------------------------------------------------------------",
      gPosSpillNumber,time,clock2time(Time,Time_Offset));
      
      TSpill * s = new TSpill(message);
	  //TSpill *s = new TSpill( gRunNumber, gSpillNumber, gTime, MAXDET );
    //gPendingSpill = s;
      Spill_List.push_back(s);
      //DrawSpills();
      
      fListBoxLogger->AddEntrySort(TGString(message),fListBoxLogger->GetNumberOfEntries());
      LayoutListBox(fListBoxLogger);
      printf("Positrons done");
	  //
  }

  // read detector entries
  for (int iDet = 0; iDet<MAXDET; iDet++){
    if (iDet>-1)
      if (Channel == detectorCh[iDet])      
        fill_tree( det_tree[iDet], det_event[iDet], Channel, Counts, Time, Time_Offset );
  }




  // read timestamps
  for (int iSeqType = 0; iSeqType < USED_SEQ; iSeqType++){

    if (Channel == tstampCh[iSeqType]){
      for( Int_t i = 0; i<Counts; i++ )  {   // for the timestamps, add one entry per count. why?
        fill_tree( sis_tree[iSeqType], sis_event[iSeqType], Channel, 1, Time, Time_Offset );
        fNumberEntryTS[iSeqType]->SetIntNumber(fNumberEntryTS[iSeqType]->GetIntNumber()+1);
        fNumberEntryTS[iSeqType]->Layout();
      }
    }
    else if (Channel == seqstartCh[iSeqType]) {
      for( Int_t i = 0; i<Counts; i++ ){ 
        fill_tree( dump_tree[iSeqType], dump_event[iSeqType], Channel, 1, Time, Time_Offset );
        fill_tree( sis_tree[iSeqType], sis_event[iSeqType], Channel, 1, Time, Time_Offset );
      }
    }
    else if (Channel == dumpstartCh[iSeqType]){
      
      if(Counts > 1) {
        cout << "Warning: increase LNE rate or increase interval between dumps does this change 1" << endl;
      }

      for( Int_t i = 0; i<Counts; i++ )
        fill_tree( dump_tree[iSeqType], dump_event[iSeqType], Channel, 1, Time, Time_Offset );
    
      //      if (Channel == 26) 
      //        cout << " START " << dumpstartCh[iSeqType] << endl;
    }
    else if (Channel == dumpstopCh[iSeqType]){

      if(Counts > 1) {
        cout << "Warning: increase LNE rate or increase interval between dumps does this change 2" << endl;
      }

      for( Int_t i = 0; i<Counts; i++ )
        fill_tree( dump_tree[iSeqType], dump_event[iSeqType], Channel, 1, Time, Time_Offset );
           if (Channel == 27) 
               cout << " STOP " << dumpstopCh[iSeqType] << endl;
    }
  }

}


void CatchUp(Bool_t useDumps = kTRUE) 
{
  if( gIsRunning == kFALSE ) return;

  for (int iSeqType=0; iSeqType<USED_SEQ; iSeqType++)
    {
      // sort the relevant tree
      Int_t* index;
      Int_t nentries;
      if (!useDumps)
        {
          nentries = sis_tree[iSeqType]->GetEntries();
          index = new Int_t[nentries];
          sis_tree[iSeqType]->Draw("SisEvent.Clock","","goff"); // or .RunTime
          //      TMath::Sort(nentries,sis_tree[iSeqType]->GetV1(),index,kFALSE);
          TMath::BubbleLow(nentries,sis_tree[iSeqType]->GetV1(),index);
        }
      else 
        {
          nentries = dump_tree[iSeqType]->GetEntries();
          index = new Int_t[nentries];
          dump_tree[iSeqType]->Draw("SisEvent.Clock","","goff");
          //      TMath::Sort(nentries,dump_tree[iSeqType]->GetV1(),index,kFALSE);
          TMath::BubbleLow(nentries,dump_tree[iSeqType]->GetV1(),index);
          //        cout<<"CatchUp: Sort"<<endl;
        }
      //cout << "Seq " << iSeqType << "; TS + seqstart: " <<   sis_tree[iSeqType]->GetEntries()   << "; SIS ptr: " << sis_ptr[iSeqType]   << "; Dumps: " << dump_tree[iSeqType]->GetEntries() << "; Dump ptr: " << dump_ptr[iSeqType] << endl;
      // use timestamps 
      if (!useDumps)
        {
          // tree_ptr -> global pointer to the current entry in the tree of sis entries for each sequencer 
          for( Int_t j = sis_ptr[iSeqType]; j < sis_tree[iSeqType]->GetEntries(); j++ ) 
            {
              Int_t i = index[j];
              sis_tree[iSeqType]->GetEntry(i);
              
              int ch = sis_event[iSeqType]->GetChannel();
              
              /*
                cout << i << " " 
                << ch << " " 
                << tstampCh[iSeqType] << " " 
                << seqstartCh[iSeqType] << " "
                << endl;
              */

              if( ch == tstampCh[iSeqType] )
                timestamp[iSeqType]++;
              else if( ch == seqstartCh[iSeqType] )
                {
                  
                  // <<<<<--------- what does this check mean????
                  if ( entry2time( sis_tree[iSeqType], sis_event[iSeqType], i ) < 0 ) continue;
                  
                  seq_startevent[iSeqType].push_back(i);
                  seq_counts[iSeqType].push_back(timestamp[iSeqType]);
                  gPendingSeqStart[iSeqType] = seq_startevent[iSeqType].size();
                  
                  //            TSpill * s = new TSpill(message);
                  //            s->SetRunNumber( gRunNumber );
                  //            Spill_List.push_back(s);
                  //            DrawSpills();  
                  // }
                }
            }
          sis_ptr[iSeqType] = sis_tree[iSeqType]->GetEntries();
          
          // synchronize (with timestamps)
          SyncSeq(iSeqType,useDumps); 
        }
    else 
      { // .. or use dumps
        for( Int_t j = dump_ptr[iSeqType]; j < dump_tree[iSeqType]->GetEntries(); j++ )
          {
            Bool_t dosync = kFALSE;
            Int_t i = index[j];
            
            dump_tree[iSeqType]->GetEntry(i);
            
            /*        
                      if (dump_event[iSeqType]->GetChannel() == 27) {
                      cout << dump_event[iSeqType]->GetChannel() << endl;
                      exit(123);
                      }
            */
            
            if( dump_event[iSeqType]->GetChannel() == seqstartCh[iSeqType] )
              {
     
                // <<<<<--------- what does this check mean????
                if ( entry2time( dump_tree[iSeqType], dump_event[iSeqType], i ) < 0 ) continue;
                
                seq_startevent[iSeqType].push_back(i);
                gPendingSeqStart[iSeqType] = seq_startevent[iSeqType].size();
                
                //          TSpill * s = new TSpill(message);
                // s->SetRunNumber( gRunNumber );
                // Spill_List.push_back(s);
                
                //      DrawSpills();  
                //          }          
                
                dosync = kTRUE;
//                cout<<"CatchUp: SeqStart"<<endl;
              }
            else if( dump_event[iSeqType]->GetChannel() == dumpstartCh[iSeqType] ) 
              {
                if(gPendingDump[iSeqType]) 
                  {
                    cout << "Warning: increase LNE rate or increase interval between dumps 3" <<gPendingDump[iSeqType]<< endl;
                    continue; // <-- potential problem?  ***********************************************************************************yes, it's a problem
                  }
                  
                  

                TSeq_Dump * d = new TSeq_Dump();
                d->SetStartonTime( entry2time( dump_tree[iSeqType], dump_event[iSeqType], i ) );
                gPendingDump[iSeqType] = d;
 //               cout<<"CatchUp: DumpStart"<<endl;
 cout << "start time is "<<entry2time( dump_tree[iSeqType], dump_event[iSeqType], i )<<endl;
              }
            else if( dump_event[iSeqType]->GetChannel() == dumpstopCh[iSeqType] ) 
              {
                TSeq_Dump * d = gPendingDump[iSeqType];
				cout<<"stop time is "<<entry2time( dump_tree[iSeqType], dump_event[iSeqType], i ) <<endl;
                if(!d) 
                  {
                    cout << "Warning: increase LNE rate or increase interval between dumps 4" << endl;
                    continue; // <-- potential problem?
                  }

                d->SetStoponTime( entry2time( dump_tree[iSeqType], dump_event[iSeqType], i ) );
                d->SetDone(kTRUE);
                
                fNumberEntryDump[iSeqType]->SetIntNumber(fNumberEntryDump[iSeqType]->GetIntNumber()+1);
                fNumberEntryDump[iSeqType]->Layout();
                
                dosync = kTRUE;

                /*
                  cout << "checking " << dump_event[iSeqType]->GetChannel() << endl;
                  exit(123);
                */
  //              cout<<"CatchUp: DumpStop"<<endl;
              }

            // synchronize (with dumps) (fill trees associated to dump)
            if ( dosync ) SyncSeq(iSeqType,useDumps);
      }
      dump_ptr[iSeqType] = dump_tree[iSeqType]->GetEntries();

      /*
        if (gPendingDump[iSeqType] && ( (int) ListOfDumps[iSeqType].size() < 1 || (int) seq_startevent[iSeqType].size() < cSeq[iSeqType] )){
        gPendingDump[iSeqType]->SetSeqNum(-1);
        }
      // synchronize (with dumps) (fill trees associated to dump)
      SyncSeq(iSeqType,useDumps);
      */
      
      }     // end  -- [ if(!useDumps) .. else ..]
      delete[] index;
    } // end loop seq type

}


void SyncSeq(Int_t iSeqType, Bool_t useDumps)
{ 
  // update the list of sequencer dumps with info about their completion

  // cout << "Num of dumps: " << ListOfDumps[iSeqType].size() << " ; " << iSeqType << " =? " << RECATCH << endl;
  
  // if checking advancement using dumps, handle the case in which no corresponding sequence xml has been received 
  // in that case, don't even look for a sequence start (assume seqstart and XML come at about the same time)
  if ( ListOfDumps[iSeqType].size() < 1 )
    { 
      //    if ( ListOfDumps[iSeqType].size() < 1 || seq_startevent[iSeqType].size() < ListOfDumps[iSeqType].size() ){ 
      //    if ( (int) ListOfDumps[iSeqType].size() < 1 || (int) seq_startevent[iSeqType].size() < cSeq[iSeqType] ){ 
      //    if (gPendingDump[iSeqType]->GetSeqNum() < 0){
      
      if (gPendingDump[iSeqType] && useDumps)
        {
          if ( gPendingDump[iSeqType]->IsDone() ) 
            {
              gPendingDump[iSeqType]->SetStartonTime( gPendingDump[iSeqType]->GetStartonTime() );
              gPendingDump[iSeqType]->SetStoponTime(  gPendingDump[iSeqType]->GetStoponTime() );
              
              UpdateDumpIntegrals(gPendingDump[iSeqType]);

              if(gPendingSpill)
                {
                  TString log ="";
                  gPendingSpill->FormatDumpInfo(&log,gPendingDump[iSeqType],kTRUE);
                  cout<<log<<endl;
                  //  gPendingSpill->AddDump(se);
                  fListBoxLogger->AddEntrySort(log.Data(),fListBoxLogger->GetNumberOfEntries());
                  LayoutListBox(fListBoxLogger);
//                  cout<<"SyncSeq pending spill"<<endl;
                }

              //              if( !gIsOffline )
              Messages(gPendingDump[iSeqType]);

              gPendingDump[iSeqType] = NULL; // or 0
            }
        }
    } 
  // <<<<<------ end

  // loop over sequencers
  list<TSeq_Dump*>::iterator it;
  for ( it = ListOfDumps[iSeqType].begin() ; it != ListOfDumps[iSeqType].end(); it++ )
    {
      TSeq_Dump * se = (TSeq_Dump*)*it;
      Int_t dumpSeqIndex = se->GetSeqNum() - 1;

      // if the dump is already been completed, skip to next dump
      if ( se->IsDone() )
        continue;

      if(gPendingSeqStart[iSeqType] > 0 && dumpSeqIndex + 1 >= gPendingSeqStart[iSeqType]) 
        { // in the ListOfDumps elements, dumpseqindex should be >= 0
          // Mark start of sequence 
          //          if(gPendingSpill){
          char message[200];
          //      sprintf(message,"-----  %s sequence %d start  -----", SeqNames[iSeqType].Data(),cSeq[iSeqType]);
          if (SeqNames[iSeqType].Data()[0]=='c')
          if (iSeqType == PBAR)
            sprintf(message,"                                -----  %s sequence %d start  -----", SeqNames[iSeqType].Data(),gPendingSeqStart[iSeqType]);
          if (iSeqType == RECATCH)
            sprintf(message,"                                                -----  %s sequence %d start  -----", SeqNames[iSeqType].Data(),gPendingSeqStart[iSeqType]);
          if (iSeqType == ATOM)
            sprintf(message,"                                                                -----  %s sequence %d start  -----", SeqNames[iSeqType].Data(),gPendingSeqStart[iSeqType]); 
          if (iSeqType == POS)
            sprintf(message,"                                                                                -----  %s sequence %d start  -----", SeqNames[iSeqType].Data(),gPendingSeqStart[iSeqType]); 
  
          
          fListBoxLogger->AddEntrySort(message,fListBoxLogger->GetNumberOfEntries());
          LayoutListBox(fListBoxLogger);
          
          cout << message << endl;
          
          gPendingSeqStart[iSeqType] = 0;
        }
      
      //    // if sequence is not yet started, exit loop
      if ( dumpSeqIndex + 1 > (int) seq_startevent[iSeqType].size() )
        break;
      
      if(!useDumps)           // check advancement using timestamps
        {     
          // if timestamp has progressed past the start count of the current dump     
          if ( timestamp[iSeqType] >= seq_counts[iSeqType].at(dumpSeqIndex) + se->GetStartonCount() )
            {
              se->SetStarted( kTRUE );
              se->SetStartOfSeq( seq_startevent[iSeqType].at(dumpSeqIndex) );
            }
        }
      else 
        {    // check advancement with dumps
          if (gPendingDump[iSeqType])
            {
              se->SetStarted( kTRUE );
              se->SetStartOfSeq( seq_startevent[iSeqType].at(dumpSeqIndex) );
//              cout<<"SyncSeq dump started"<<endl;
            }
        }
      
    // if the dump has not yet started (even after the above check), exit loop
    if ( !se->HasStarted() )
      break;
    
    // check if the dump has finished (N.B.: during this call to the function)
    if(!useDumps) 
      {     // check advancement using timestamps                 
        if ( timestamp[iSeqType] >= seq_counts[iSeqType].at(dumpSeqIndex) + se->GetStoponCount() ) 
          {
            
            se->SetDone(kTRUE);        // flag the dump as finished
            
            se->SetStartonTime( entry2time( sis_tree[iSeqType], sis_event[iSeqType], se->GetStartOfSeq() + se->GetStartonCount() ) );
            se->SetStoponTime(  entry2time( sis_tree[iSeqType], sis_event[iSeqType], se->GetStartOfSeq() + se->GetStoponCount() ) );
            
            UpdateDumpIntegrals(se);          
            
            cout << se->GetDescription() << endl;
        }        
    }
    else 
      {     // check advancement using dumps
        if (gPendingDump[iSeqType])
          {
            if ( gPendingDump[iSeqType]->IsDone() ) 
              {
                se->SetDone(kTRUE);        // flag the dump as finished
                
                se->SetStartonTime( gPendingDump[iSeqType]->GetStartonTime() );
                se->SetStoponTime(  gPendingDump[iSeqType]->GetStoponTime() );
                
                UpdateDumpIntegrals(se);          
                
                gPendingDump[iSeqType] = NULL; // or 0            
                
                cout << se->GetDescription() << endl;
//                cout<<"SyncSeq integral"<<endl;
                IsHotDump(se);
                if(IsColdDump(se)) SendData(se);
              }
          }
      }

    // if after the cycle it is done 
    if (se->IsDone())
      {
        if(gPendingSpill)
          {
            TString buf = "(" + SeqNames[iSeqType] + ")";
            TString log =TString::Format(" %-4s ",buf.Data());
            gPendingSpill->FormatDumpInfo(&log,se,kFALSE);
            //        gPendingSpill->FormatDumpInfo(&log,se,kTRUE);
            cout<<log<<endl;
            //  gPendingSpill->AddDump(se);
            fListBoxLogger->AddEntrySort(log.Data(),fListBoxLogger->GetNumberOfEntries());
            LayoutListBox(fListBoxLogger);
  //          cout<<"SyncSeq dump done"<<endl;
          }
        
        //      DrawSpills();
        
        //      if( !gIsOffline )
        Messages(se);
      }
     
  }

  
}


void CatchUpAndDraw()
{
  //   cout<<"CatchUpAndDraw"<<endl;
  //  CatchUp(kFALSE);
  CatchUp(); // default: use dumps
  //  DrawSpills();
}



void HandleSIS(int numChan,int size,const void*ptr,int size_2,const void*ptr_2) 
{

  //  cout<<"HandleSIS"<<endl;

  const uint32_t *sis = (uint32_t*)ptr; // (0-31)
  const uint32_t *sis_2 = (uint32_t*)ptr_2; // Pointer to the second SIS data (0-31)

  static uint64_t gClock = 0;
  static uint64_t gClock_2 = 0;


  // loop over databank   
  // clocks for SIS1 and SIS2 are handled here

  int i;
  for (i=0; i<size; i+=numChan, sis+=numChan) { // multievent -> navigate through different events, each of which is 'numChan' long

    // update Clock
    uint64_t clock = sis[clock10MHzSIS1Ch]; // num of 10MHz clks
    gClock += clock;	
    
    if (gSaveClock == 0)
      gSaveClock = gClock;
    
    // loop over sis1 channels
    for (int j=0; j<numChan; j++)  {	    

      if(sis[j])
        SISCounts(j,sis[j],gClock,gSaveClock);
    }

  }//loop over databank (n.b. pointer to databank, sis, increases by numChan on each loop
  assert(i==size);
  
  if(size_2) {
    for (i=0; i<size_2; i+=numChan, sis_2+=numChan) {

      uint64_t clock_2 = sis_2[clock10MHzSIS2Ch-SISCHAN]; // num of 10MHz clks
      gClock_2 += clock_2;
      
      if (gSaveClock_2 == 0)
        gSaveClock_2 = gClock_2;
        
      // loop over sis2 channels
      for (int j=0; j<numChan; j++) {
        if(sis_2[j])
          SISCounts(j+SISCHAN,sis_2[j],gClock_2,gSaveClock_2);
      }

    }
    assert(i==size_2);    

  }


  //  printf("Handling SIS: %d ev + %d - %d ev + %d ; clock1: %" PRIu64 " - %" PRIu64 ", clock2: %" PRIu64 " - %" PRIu64 "\n", size/32, size%32, size_2/32, size_2%32, gClock, gSaveClock, gClock_2, gSaveClock_2);


}

void HandleLabviewADE0(int ade0size,const void * ade0ptr)
{
  
  double * ade0data = (double*)ade0ptr;

  double trans_value = ade0data[2];  // -- 2014
  cout<<"Handle AD"<<endl;
  
  //ade0data[0] is a timestamp (in labview)
  time_t unixtime =  (time_t) round(ade0data[0] - 2082844800.);
  
     
   struct tm  *ts;
  char       ade0time[80];

  ts = localtime(&unixtime);
  strftime(ade0time, sizeof(ade0time), "%H:%M:%S", ts);
  
   printf("ade0data for spill at [%ld / %s] : %lf\t%lf\t%lf\n", unixtime, ade0time, ade0data[0], ade0data[1], ade0data[2]);
   
   printf("gPendingSpill time is %ld\n", *(gPendingSpill->GetTime()));
   
   if( ( *(gPendingSpill->GetTime())  - unixtime) < -10 )
   {
	   printf("ade0data is too early!!\n");
	   memcpy(ade0data_buffer, ade0data, sizeof(double)*4);
	   isPendingADE0Data=true;
   }
   else
   {

  if(gPendingSpill)
    {

      gPendingSpill->SetTransformer( trans_value );
      
      TGTextLBEntry* el = (TGTextLBEntry*)fListBoxLogger->GetEntry(gSpillLogEntry);
      
      if(el){ // e.g., skips the spill created at the start of the run
        TString log = "";
        gPendingSpill->FormatADInfo(&log);
    
        cout << log.Data() << " " << trans_value << " " << gPendingSpill->GetTransformer() <<  endl;
        
        TGString* tglog = new TGString(log.Data());
        el->SetText(tglog);
        //      el->Update();
        LayoutListBox(fListBoxLogger);
   

    }
  }
}

}

void HandleLabviewMCP(int size, void*ptr)
{
  cout<<"Handle MCP"<<endl;
  double *mcpdata = (double*)ptr;
  
  if(gPendingSpill)
    {
      char message[100];
      sprintf(message,"                                  MCP Image: %.0lf.%02.0lf.%02.0lf %02.0lf:%02.0lf:%02.3lf",mcpdata[0], mcpdata[1], mcpdata[2], mcpdata[3], mcpdata[4], mcpdata[5]);
      TSpill * s = new TSpill(message);
      s->SetRunNumber( gRunNumber );
      Spill_List.push_back(s);
      //      DrawSpills();

      fListBoxLogger->AddEntrySort(TGString(message),fListBoxLogger->GetNumberOfEntries());
      LayoutListBox(fListBoxLogger);
    }
}

void HandleLabviewLFC4(int size, void*ptr)
{
  
  double *lfc4data = (double*)ptr;
  
  if(gPendingSpill)
    {
      char message[30];
      sprintf(message,"Positrons=%8.3g",lfc4data[1]);
      TSpill * s = new TSpill(message);
      s->SetRunNumber( gRunNumber );
      Spill_List.push_back(s);
      //      DrawSpills();
      
      
      fListBoxLogger->AddEntrySort(TGString(message),fListBoxLogger->GetNumberOfEntries());
      LayoutListBox(fListBoxLogger);

    }
}

void HandleLabviewCSI(int size, void*ptr)
{
  
  double *csidata = (double*)ptr;
  
  if(gPendingSpill && csidata[8] != gLastcsi2)
    {
      char message[300];
      //sprintf(message,"                |CSI [%.1fs]    (1) %4.2f     (2) %4.2f     (A) %4.2f     (B) %4.2f     (C) %4.2f     (D) %4.2f     (E) %4.2f     (F) %4.2f",csidata[1],csidata[2],csidata[3],csidata[4],csidata[5],csidata[6],csidata[7],csidata[8],csidata[9],csidata[10]);
      sprintf(message,"                |CSI [%.1fs]    (1) %4.2f     (2) %4.2f     (A) %4.2f     (B) %4.2f     (C) %4.2f     (D) %4.2f     (E) %4.2f     (F) %4.2f",csidata[1],csidata[2],csidata[3],csidata[4],csidata[5],csidata[6],csidata[7],csidata[8],csidata[9]);
      gLastcsi2 = csidata[8];
      TSpill * s = new TSpill(message);
      s->SetRunNumber( gRunNumber );
      Spill_List.push_back(s);
      //      DrawSpills();

      fListBoxLogger->AddEntrySort(TGString(message),fListBoxLogger->GetNumberOfEntries());
      LayoutListBox(fListBoxLogger);

    }
}

//----------------------------------
// obsolete
void HandleLabviewLPOS(int size, void*ptr)
{
  
  double *trdata = (double*)ptr;
  
  if(gPendingSpill && (trdata[CSI2] != gLastcsi2) && (trdata[MCP] != gLastmcp) && (trdata[FC] !=gLastfc))
    {
      struct tm  *ts;
      char       time[80];      
      ts = localtime(&gTime);
      strftime(time, sizeof(time), "%H:%M:%S", ts);

      char message[100];
      sprintf(message,"\n%15s%s] %8s CsI2 = %6.1lf %8s CSI_MCP = %6.1lf %8s CsI_FC = %6.1lf","[",time, " ", trdata[CSI2], " ",trdata[MCP], " ", trdata[FC]);

      gLastcsi2 = trdata[CSI2];
      gLastmcp = trdata[MCP];
      gLastfc = trdata[FC];

      TSpill * s = new TSpill(message);
      s->SetRunNumber( gRunNumber );
      Spill_List.push_back(s);
      DrawSpills();

      
      fListBoxLogger->AddEntrySort(TGString(message),fListBoxLogger->GetNumberOfEntries());
      LayoutListBox(fListBoxLogger);

    }
}

void HandleLabviewCTRW(int size, void*ptr) {
  
  double *rwdata = (double*)ptr;
  
  if(gPendingSpill) {

    char message[100];
    char f1[20];
    sprintf(f1, "%s",  sinot(rwdata[3], 4, 0));
    char f2[20];
    sprintf(f2, "%s",  sinot(rwdata[4], 4, 0));
    sprintf(message,"                                  RW%.0fS%.0f: %s-%sHz, %.0fs, %.1f-%.1fV, type:(%.0f,%.0f)",rwdata[1]+1, rwdata[2]-1, f1, f2, rwdata[5],  rwdata[7], rwdata[8], rwdata[9], rwdata[10]);
    //sprintf(message,"                                  RW%.0fS%.0f: %.2e-%.2eHz, %.0fs, %.1f-%.1fV, type:(%.0f,%.0f)",rwdata[1]+1, rwdata[2]-1, rwdata[3], rwdata[4], rwdata[6],  rwdata[7], rwdata[8], rwdata[9], rwdata[10]);

    TSpill * s = new TSpill(message);
    s->SetRunNumber( gRunNumber );
    Spill_List.push_back(s);

    fListBoxLogger->AddEntrySort(TGString(message),fListBoxLogger->GetNumberOfEntries());
    LayoutListBox(fListBoxLogger);
    
    //      DrawSpills();
  }
}
//----------------------------------

void HandleLabviewRWWithName(int size, void*ptr, const char* name) {
  
  double *rwdata = (double*)ptr;
  
  if(gPendingSpill) {

    char message[200];

    // for (int i=0; i<3; i++){
    //   char f1[20];
    //   sprintf(f1, "%s",  sinot(rwdata[1+i*13], 4, 0));
    //   char f2[20];
    //   sprintf(f2, "%s",  sinot(rwdata[2+i*13], 4, 0));
    //   // sw1, sw2, sw3
      
      //   sprintf(message,"                                  %s SW%d: %s-%sHz, %.0fs, %.1f-%.1fV, type:(%.0f,%.0f)",name, i+1, f1, f2, rwdata[5+i*13],  rwdata[6+i*13], rwdata[7+i*13], rwdata[9+i*13], rwdata[10+i*13]);
      sprintf(message," ");
      //sprintf(message,"       |ROTATING WALL|                      |                      %s S%.0f   %5.1f -- %5.1f kHz  %2.2f s  %.1f V",name, rwdata[1], rwdata[2]/1e3, rwdata[3]/1e3, rwdata[4],rwdata[6] );
      if (name[0]=='C')
      //sprintf(message,"%s|ROTATING WALL|%s|%s        %s S%.0f   %5.1f -- %5.1f kHz  %2.2f s  %.1f V",leftindent,indent,indent,name, rwdata[1], rwdata[2]/1e3, rwdata[3]/1e3, rwdata[4],rwdata[6] );
      sprintf(message,"                                       ROTATING WALL                                                    |  %s S%.0f   %5.1f -- %5.1f kHz  %2.2f s  %.1f V",name, rwdata[1], rwdata[2]/1e3, rwdata[3]/1e3, rwdata[4],rwdata[6] );
      if (name[0]=='A' && name[3]=='1')
      sprintf(message,"                                                       ROTATING WALL                                    |  %s S%.0f   %5.1f -- %5.1f kHz  %2.2f s  %.1f V",name, rwdata[1], rwdata[2]/1e3, rwdata[3]/1e3, rwdata[4],rwdata[6] );
      if (name[0]=='A' && name[3]=='2')
      sprintf(message,"                                                                       ROTATING WALL                    |  %s S%.0f   %5.1f -- %5.1f kHz  %2.2f s  %.1f V",name, rwdata[1], rwdata[2]/1e3, rwdata[3]/1e3, rwdata[4],rwdata[6] );
      cout<<"RWdata ";
      for(unsigned int i=0; i<sizeof(rwdata); ++i)
        printf(" %d\t%4.2f",i,rwdata[i]);
      cout<<""<<endl;

      TSpill * s = new TSpill(message);
      s->SetRunNumber( gRunNumber );
      Spill_List.push_back(s);
      
      fListBoxLogger->AddEntrySort(TGString(message),fListBoxLogger->GetNumberOfEntries());
      LayoutListBox(fListBoxLogger);
      //    }
    
    //      DrawSpills();
  }
}


void HandleMidasEvent(TMidasEvent& event)
{
  if(!gIsRunning) return;

  int eventId = event.GetEventId();
  //event.Print();

  gTime = event.GetTimeStamp();


  if(eventId==8) // Sequencer 
    {
//      event.Print();

      //      printf("Sequencer\n");
      
      void *ptr;
      int size = event.LocateBank(event.GetData(),"SEQ2",&ptr);
      //      std::cout << "Handling SEQ2 Midas Event: size = " << size << std::endl; 
      char *sequdata = (char*)ptr;
      HandleSeq2( sequdata, size ); 
    }
  if(eventId==6) //labview
    {

	  // AD Transformer
      void *ade0ptr;
      int ade0size = event.LocateBank(event.GetData(),"ADE1",&ade0ptr);
      if(ade0size)
        HandleLabviewADE0(ade0size,ade0ptr);

	  // // Positrons to the faraday cup	
      // void *lfc4ptr; 	 
      // int lfc4size = event.LocateBank(event.GetData(),"LFC4",&lfc4ptr); 	 
      // if(lfc4size) 	 
      //   HandleLabviewLFC4(lfc4size,lfc4ptr);

      // CSI transients
      //REMOVED TO CLEAN UP TEXT
      //void *csiptr; 	 
      //int csisize = event.LocateBank(event.GetData(),"PADT",&csiptr); 	 
      //if(csisize) 	 
      //  HandleLabviewCSI(csisize,csiptr);// takes values of positrons transients: CsI2, MCP, FC 

      // MCP image
      void *mcpptr;
      int mcpsize = event.LocateBank(event.GetData(),"MCPI",&mcpptr);
      if(mcpsize)
        HandleLabviewMCP(mcpsize,mcpptr); 
      
	  // void *ctrwptr; // old 
      // int ctrwsize = event.LocateBank(event.GetData(),"CTRW1",&ctrwptr);
      // if(ctrwsize)
      //   HandleLabviewCTRW(ctrwsize,ctrwptr); 

	  void *ctrw1ptr;
      int ctrw1size = event.LocateBank(event.GetData(),"CRW1",&ctrw1ptr);
      if(ctrw1size)
      {
        HandleLabviewRWWithName(ctrw1size,ctrw1ptr,"CRW1"); 
       }

	  void *ctrw2ptr;
      int ctrw2size = event.LocateBank(event.GetData(),"CRW2",&ctrw2ptr);
      if(ctrw2size)
        HandleLabviewRWWithName(ctrw2size,ctrw2ptr,"CRW2"); 

	  void *atrw1ptr;
      int atrw1size = event.LocateBank(event.GetData(),"ARW1",&atrw1ptr);
      if(atrw1size)
        HandleLabviewRWWithName(atrw1size,atrw1ptr,"ARW1"); 

	  void *atrw2ptr;
      int atrw2size = event.LocateBank(event.GetData(),"ARW2",&atrw2ptr);
      if(atrw2size)
        HandleLabviewRWWithName(atrw2size,atrw2ptr,"ARW2"); 

    }
  else if (eventId == 11) { // VME bulk data 

      void *ptr0;
      int size0 = event.LocateBank(NULL,"MCS0",&ptr0); // SIS 1

      void *ptr1;
      int size1 = event.LocateBank(NULL,"MCS1",&ptr1); // SIS 2

      assert( size0 % SISCHAN == 0 && size1 % SISCHAN == 0 ); // check SIS databank size is a muliple of 32 

      if (size0>0 || size1>0) {
        HandleSIS(SISCHAN,size0,ptr0,size1,ptr1);
      }

  }
  else {


    //event.Print();
  }

}

void eventHandler(const void*pheader,const void*pdata,int size)
{
  TMidasEvent event;
  memcpy(event.GetEventHeader(),pheader,sizeof(TMidas_EVENT_HEADER));
  event.SetData(size, (char*)pdata);
  event.SetBankList();
  HandleMidasEvent(event);
}

int ProcessMidasFile(TApplication*app,const char*fname)
{
  cout<<"Process MIDAS file"<<endl;
    cout <<"Parsing fname"<<endl;
  TString path="";
  TString runString;
  TString tmp="";
  for (Int_t i=0; i<100; i++)
  {
    if (fname[i]=='r' && fname[i+1]=='u' && fname[i+2]=='n' ) 
    {
      path=tmp;
      for (Int_t j=3; j<3+5; j++)
      runString+=fname[i+j];
      tmp="";
      break;
    }
    tmp+=fname[i];
  }
  Int_t runNumber=runString.Atoi();
  Int_t subNumber=0;
  cout <<"Path:"<<path<<endl;
  cout <<"runNumber:"<<runNumber<<endl;
  
  
  
  TMidasFile f;
  bool tryOpen = f.Open(fname);

  if (!tryOpen) {
    printf("Cannot open input file \"%s\", error %d (%s)\n", fname, f.GetLastErrno(), f.GetLastError());
    return -1;
  }

  while (1) {
 
    TMidasEvent event;
    if (!f.Read(&event)) {
      printf("Cannot read event from file, error %d (%s)\n", f.GetLastErrno(), f.GetLastError());
      break;
    }

    int eventId = event.GetEventId();
    
    if ((eventId & 0xFFFF) == 0x8000) {        // begin run
     
      // Load ODB contents from the ODB XML file
      if (gOdb)
        delete gOdb;
      gOdb = new XmlOdb(event.GetData(),event.GetDataSize());

      startRun(0,(Int_t)event.GetSerialNumber(),0);

                cout << event.GetSerialNumber() << endl;

      //      gRunNumber = gOdb->odbReadInt("/runinfo/Run number");
      //      startRun(0,gRunNumber,0);

    }

    else if ((eventId & 0xFFFF) == 0x8001) {	  // end run
      f.Close();          //          std::cout << "closing " << fname << std::endl;
            
      subNumber++;
      char nextfname[200];
      //      printf( "basestr %s sufstr %s \n", basestr,sufstr);
      // AO: the original line screws up with C++11 inserting an extra non-printing char
      //      sprintf(nextfname,"%sb%05d.%s",basestr,midindex,sufstr);
      sprintf(nextfname,"%s/run%05dsub%05d.mid.gz",path.Data(),runNumber,subNumber);
      tryOpen = f.Open(nextfname);
      std::cout << "opening " << nextfname << std::endl;
       if (!tryOpen){
         printf("Checking for next input file \"%s\", midindex %d, returned %d (%s)\n", nextfname,subNumber, f.GetLastErrno(), f.GetLastError());
 //       endRun(0,gRunNumber,0);
        break;
       }
    }

    else {
      event.SetBankList();
      HandleMidasEvent(event);
      CatchUpAndDraw();
    }

  }//while(1)

  endRun(0,gRunNumber,0);

  f.Close();

  return 0;

}


#ifdef HAVE_MIDAS

void MidasPollHandler() {
  if (!(TMidasOnline::instance()->poll(0)))
    gSystem->ExitLoop(); 
}

int ProcessMidasOnline(TApplication*app)
{

  cout<<"Welcom to MIDAS online"<<endl;

  for (int i = 0; i< MAXDET; i++){
    cout <<det_ptr[i] << " " ;
  }
  cout << endl;

  for (int i = 0; i< USED_SEQ; i++){
    cout <<sis_ptr[i] << " " ;
  }
  cout << endl;

  for (int i = 0; i< USED_SEQ; i++){
    cout <<dump_ptr[i] << " " ;
  }
  cout << endl;


   TMidasOnline *midas = TMidasOnline::instance();

   int err = midas->connect(NULL,NULL,"alpha2dumps"); 
   assert(err == 0);

   gOdb = midas;

   midas->setTransitionHandlers(startRun,endRun,NULL,NULL);
   midas->registerTransitions();

   /* reqister event requests */

   midas->setEventHandler(eventHandler);       // call 'eventHandler' when an event comes in
   midas->eventRequest("SYSTEM",-1,-1,(1<<1));

   /* fill present run parameters */

   gRunNumber = gOdb->odbReadInt("/runinfo/Run number");

   if ((gOdb->odbReadInt("/runinfo/State") == 3))
     startRun(0,gRunNumber,0);


   /*---- start main loop ----*/
   MyPeriodic tm(200,MidasPollHandler); //doubled
   MyPeriodic cm(400,CatchUpAndDraw); //doubled

   //loop_online();
   app->Run(kTRUE);

   /* disconnect from experiment */
   midas->disconnect();

   return 0;
}

#endif



TApplication* xapp;

// Main function call

int main(int argc, char *argv[])
{
  printf("***************************\n"); 	 
  printf("     ALPHA  events      \n"); 	 
  printf("***************************\n\n");

  if (USED_SEQ>4){
    printf("Not (yet) enough displays for USED_SEQ>4. \n");
    assert(0);
  }


  setbuf(stdout,NULL);
  setbuf(stderr,NULL);

  signal(SIGILL,  SIG_DFL);
  signal(SIGBUS,  SIG_DFL);
  signal(SIGSEGV, SIG_DFL);
 
  for (int i = 0; i< MAXDET; i++){
    cout <<det_ptr[i] << " " ;
  }
  cout << endl;


  for (int i = 0; i< USED_SEQ; i++){
    cout <<sis_ptr[i] << " " ;
  }
  cout << endl;

  for (int i = 0; i< USED_SEQ; i++){
    cout <<dump_ptr[i] << " " ;
  }
  cout << endl;

  // TString default_font   = gEnv->GetValue("Gui.DefaultFont",  "-*-helvetica-medium-r-*-*-12-*-*-*-*-*-iso8859-1");
  gEnv->SetValue("Gui.DefaultFont","-*-courier-medium-r-*-*-12-*-*-*-*-*-iso8859-1");
  

  std::vector<std::string> args;
  for (int i=0; i<argc; i++)
    args.push_back(argv[i]);


  if(gROOT->IsBatch()) {
    printf("Cannot run in batch mode\n");
    return 1;
  }

  TApplication *app = new TApplication("alpha2dumps", &argc, argv);
  extern TApplication* xapp;
  xapp = app;


  // main frame
  //  TGMainFrame 
  alphaFrame* fMainFrameGUI = new alphaFrame();
  fMainFrameGUI->SetName("fMainFrameGUI");
  fMainFrameGUI->SetWindowName("alpha2dumps");
  fMainFrameGUI->SetLayoutBroken(kTRUE);

  // list box
  TGLabel *fLabelSeq1 = new TGLabel(fMainFrameGUI,SeqNames[PBAR].Data());
  fLabelSeq1->SetTextJustify(36);
  fLabelSeq1->SetMargins(0,0,0,0);
  fLabelSeq1->SetWrapLength(-1);
  fMainFrameGUI->AddFrame(fLabelSeq1, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
  fLabelSeq1->MoveResize(25,8,62,16);
  fListBoxSeq[0] = new TGListBox(fMainFrameGUI);
  fListBoxSeq[0]->SetName("fListBoxSeq1");
  fListBoxSeq[0]->Resize(290,116);
  fMainFrameGUI->AddFrame(fListBoxSeq[0], new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
  fListBoxSeq[0]->MoveResize(25,24,290,116);

  // list box
  TGLabel *fLabelSeq2 = new TGLabel(fMainFrameGUI,SeqNames[RECATCH].Data());
  fLabelSeq2->SetTextJustify(36);
  fLabelSeq2->SetMargins(0,0,0,0);
  fLabelSeq2->SetWrapLength(-1);
  fMainFrameGUI->AddFrame(fLabelSeq2, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
  fLabelSeq2->MoveResize(345,8,62,16);
  fListBoxSeq[1] = new TGListBox(fMainFrameGUI);
  fListBoxSeq[1]->SetName("fListBoxSeq2");
  fListBoxSeq[1]->Resize(290,116);
  fMainFrameGUI->AddFrame(fListBoxSeq[1], new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
  fListBoxSeq[1]->MoveResize(345,24,290,116);
  
  // list box
  TGLabel *fLabelSeq3 = new TGLabel(fMainFrameGUI,SeqNames[ATOM].Data());
  fLabelSeq3->SetTextJustify(36);
  fLabelSeq3->SetMargins(0,0,0,0);
  fLabelSeq3->SetWrapLength(-1);
  fMainFrameGUI->AddFrame(fLabelSeq3, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
  fLabelSeq3->MoveResize(665-20,8,62,16);
  fListBoxSeq[2] = new TGListBox(fMainFrameGUI);
  fListBoxSeq[2]->SetName("fListBoxSeq3");
  fListBoxSeq[2]->Resize(290,116);
  fMainFrameGUI->AddFrame(fListBoxSeq[2], new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
  fListBoxSeq[2]->MoveResize(665,24,290,116);
   
  // list box
  TGLabel *fLabelSeq4 = new TGLabel(fMainFrameGUI,SeqNames[POS].Data());
  fLabelSeq4->SetTextJustify(36);
  fLabelSeq4->SetMargins(0,0,0,0);
  fLabelSeq4->SetWrapLength(-1);
  fMainFrameGUI->AddFrame(fLabelSeq4, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
  fLabelSeq4->MoveResize(985,8,62,16);
  fListBoxSeq[3] = new TGListBox(fMainFrameGUI);
  fListBoxSeq[3]->SetName("fListBoxSeq4");
  fListBoxSeq[3]->Resize(290,116);
  fMainFrameGUI->AddFrame(fListBoxSeq[3], new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
  fListBoxSeq[3]->MoveResize(985,24,290,116);
   


  // TGRadioButton *fTextButtonUseDumps = new TGRadioButton(fMainFrameGUI,"Use Dumps");
  // fTextButtonUseDumps->SetTextJustify(36);
  // fTextButtonUseDumps->SetMargins(0,0,0,0);
  // fTextButtonUseDumps->SetWrapLength(-1);
  // fMainFrameGUI->AddFrame(fTextButtonUseDumps, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
  // fTextButtonUseDumps->MoveResize(200,550,200,17);
  // TGRadioButton *fTextButtonUseTimeStamps = new TGRadioButton(fMainFrameGUI,"Use Timestamps");
  // fTextButtonUseTimeStamps->SetTextJustify(36);
  // fTextButtonUseTimeStamps->SetMargins(0,0,0,0);
  // fTextButtonUseTimeStamps->SetWrapLength(-1);
  // fMainFrameGUI->AddFrame(fTextButtonUseTimeStamps, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
  // fTextButtonUseTimeStamps->MoveResize(450,550,200,17);
  
  // // buffer 
  // fTextEditBuffer = new TGTextEdit(fMainFrameGUI,1150,25);
  // fTextEditBuffer->SetName("fTextEditBuffer");
  // fMainFrameGUI->AddFrame(fTextEditBuffer, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
  // fTextEditBuffer->MoveResize(25,600,1150,25);
  
  // fTextButtonCopy = new TGTextButton(fMainFrameGUI,"Copy Selection",100001);
  // fTextButtonCopy->SetTextJustify(36);
  // fTextButtonCopy->SetMargins(0,0,0,0);
  // fTextButtonCopy->SetWrapLength(-1);
  // fTextButtonCopy->Resize(40,40);
  // fMainFrameGUI->AddFrame(fTextButtonCopy, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
  // fTextButtonCopy->MoveResize(855,550,120,40);
  // //   fTextButtonCopy->Connect();
  
  // list box
  fListBoxLogger = new TGListBox(fMainFrameGUI);
  fListBoxLogger->SetName("fListBoxLogger");
  //  fListBoxLogger->Resize(1150,326);
  fListBoxLogger->Resize(1350,326);
  fMainFrameGUI->AddFrame(fListBoxLogger, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
  //  fListBoxLogger->MoveResize(25,208,1150,600);
  fListBoxLogger->MoveResize(25,208,1350,600);
  fListBoxLogger->SetMultipleSelections(kTRUE);

  fNumberEntryDump[0] = new TGNumberEntry(fMainFrameGUI, (Double_t) 0,7,-1,(TGNumberFormat::EStyle) 5);
  fNumberEntryDump[0]->SetName("fNumberEntryDump1");
  fMainFrameGUI->AddFrame(fNumberEntryDump[0], new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
  fNumberEntryDump[0]->MoveResize(25,144,64,20);
  fNumberEntryDump[1] = new TGNumberEntry(fMainFrameGUI, (Double_t) 0,6,-1,(TGNumberFormat::EStyle) 5);
  fNumberEntryDump[1]->SetName("fNumberEntryDump2");
  fMainFrameGUI->AddFrame(fNumberEntryDump[1], new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
  fNumberEntryDump[1]->MoveResize(345,144,64,20);
  fNumberEntryDump[2] = new TGNumberEntry(fMainFrameGUI, (Double_t) 0,6,-1,(TGNumberFormat::EStyle) 5);
  fNumberEntryDump[2]->SetName("fNumberEntryDump3");
  fMainFrameGUI->AddFrame(fNumberEntryDump[2], new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
  fNumberEntryDump[2]->MoveResize(665,144,64,20);
  fNumberEntryDump[3] = new TGNumberEntry(fMainFrameGUI, (Double_t) 0,6,-1,(TGNumberFormat::EStyle) 5);
  fNumberEntryDump[3]->SetName("fNumberEntryDump4");
  fMainFrameGUI->AddFrame(fNumberEntryDump[3], new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
  fNumberEntryDump[3]->MoveResize(985,144,64,20);
  
  fNumberEntryTS[0] = new TGNumberEntry(fMainFrameGUI, (Double_t) 0,7,-1,(TGNumberFormat::EStyle) 5);
  fNumberEntryTS[0]->SetName("fNumberEntryTS1");
  fMainFrameGUI->AddFrame(fNumberEntryTS[0], new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
  fNumberEntryTS[0]->MoveResize(25,168,64,20);
  fNumberEntryTS[1] = new TGNumberEntry(fMainFrameGUI, (Double_t) 0,6,-1,(TGNumberFormat::EStyle) 5);
  fNumberEntryTS[1]->SetName("fNumberEntryTS2");
  fMainFrameGUI->AddFrame(fNumberEntryTS[1], new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
  fNumberEntryTS[1]->MoveResize(345,168,64,20);
  fNumberEntryTS[2] = new TGNumberEntry(fMainFrameGUI, (Double_t) 0,6,-1,(TGNumberFormat::EStyle) 5);
  fNumberEntryTS[2]->SetName("fNumberEntryTS3");
  fMainFrameGUI->AddFrame(fNumberEntryTS[2], new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
  fNumberEntryTS[2]->MoveResize(665,168,64,20);
  fNumberEntryTS[3] = new TGNumberEntry(fMainFrameGUI, (Double_t) 0,6,-1,(TGNumberFormat::EStyle) 5);
  fNumberEntryTS[3]->SetName("fNumberEntryTS4");
  fMainFrameGUI->AddFrame(fNumberEntryTS[3], new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
  fNumberEntryTS[3]->MoveResize(985,168,64,20);
  
  
  fMainFrameGUI->MapSubwindows();
  
  //  fMainFrameGUI->Resize(fMainFrameGUI->GetDefaultSize());
  fMainFrameGUI->MapWindow();
  fMainFrameGUI->Resize(1400,916);
  //  fMainFrameGUI->Resize(1200,916);
  //  fMainFrameGUI->Resize(896,916);
 
  memset(&serv, 0, sizeof(serv));    /* zero the struct before filling the fields */
  serv.sin_family = AF_INET;         /* set the type of connection to TCP/IP */
  serv.sin_addr.s_addr = INADDR_ANY; /* set our address to any interface */
  printf("port: %d\n",PORTNUM);
  serv.sin_port = htons(PORTNUM);    /* set the server port number */    
 
  mysocket = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);
  // struct linger so_linger;
  // so_linger.l_onoff = 1;
  // so_linger.l_linger = 2;
  // int z = setsockopt(mysocket,SOL_SOCKET,SO_LINGER,&so_linger,sizeof so_linger);
  // printf("to status %d\n",z);

  /* bind serv information to mysocket */
  int stat = bind(mysocket, (struct sockaddr *)&serv, sizeof(struct sockaddr));
  printf("bind status %d\n",stat);
  stat = listen(mysocket, 5);
  printf("listen status %d\n",stat);

   gIsOffline = false;
   for (unsigned int i=1; i<args.size(); i++) {
     const char* arg = args[i].c_str();
     if (arg[0] != '-') {  
       gIsOffline = true;
       ProcessMidasFile(app,arg);
     }
   }
  // // if we processed some data files,
  // // do not go into online mode.
   if(gIsOffline)  {
     delete app;
     return 0;
   }

  gIsOffline = false;
#ifdef HAVE_MIDAS
  ProcessMidasOnline(app);
#endif

  fMainFrameGUI->CloseWindow();
  delete app;	    
  close(mysocket);
  return 0;
}
//end

