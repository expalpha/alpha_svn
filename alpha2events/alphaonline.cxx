// ===========================================================================
//
// alphaonline
//
// SIS display
//
// ===========================================================================

#include <list>
#include <stdio.h>
#include <sys/time.h>
#include <signal.h>
#include <assert.h>
#include <iostream>
#include <time.h>
#include <stdint.h>
#include <vector>
#include <map>

#include "TMidasOnline.h"
#include "TMidasEvent.h"
#include "TMidasFile.h"
#include "XmlOdb.h"
#include "midasServer.h"

#include <TTree.h>
#include <TROOT.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TStyle.h>
#include <TSystem.h>
#include <TClass.h>
#include <TApplication.h>
#include <TTimer.h>
#include <TFile.h>
#include <TDirectory.h>
#include <TFolder.h>

#include "TSISChannels.h"
#include "TSpill.h"
#include "TSisEvent.h"
#include "TSeq_Event.h"
#include "TSeq_Dump.h"


class SIS_readout
{
public:
  SIS_readout() {}
  ~SIS_readout() {}

  Int_t counts;
  Double_t runtime;

  void Print()
  {
    printf("[%3.3lf] counts: %d\n",runtime,counts);
  }
};

struct SIS_channel
{
  Int_t channel;
  TH1D * histo;

  std::vector<SIS_readout*> readouts; 

  void TrimROs()
  {
    Double_t start_time = (Double_t) readouts.back()->runtime;
    
    std::vector<SIS_readout*>::iterator ro_it;
    for( ro_it=readouts.begin(); ro_it<readouts.end(); ro_it++ )
      {
        SIS_readout* ro = *ro_it;
        histo->Fill(ro->runtime-start_time, ro->counts);
      }   

  }

  void UpdateHisto()
  {
    histo->Reset();
    
    if( readouts.empty() )
      return;
    Double_t back_time = (Double_t) readouts.back()->runtime;
    
    std::vector<SIS_readout*>::iterator ro_it;
    for( ro_it=readouts.begin(); ro_it<readouts.end(); ro_it++ )
      {
        SIS_readout* ro = *ro_it;
        histo->Fill(ro->runtime-back_time, ro->counts);

        if( (ro->runtime-back_time)< histo->GetXaxis()->GetXmin() )
          {
            delete ro;
            readouts.erase( ro_it );
          }
      }  
  }
   
  void Clear()
  {
    std::vector<SIS_readout*>::iterator ro_it;
    for( ro_it=readouts.begin(); ro_it<readouts.end(); ro_it++ )
      {
        SIS_readout* ro = *ro_it;
        delete ro;
        readouts.erase( ro_it );
      }
  }
  
  void AddReadout( SIS_readout* ro )
  {
    readouts.push_back( ro );
  }

  Int_t GetNReadouts()
  {
    return readouts.size();
  }
};

std::vector<SIS_channel*> g_SIS_channels;

class Panel : public TObject {
public:
  Panel() {}
  Panel( Int_t nbr, Int_t c, TString t, Double_t mi, Double_t ma, Int_t n ): number(nbr), Channel(c), Title(t), xmin(mi), xmax(ma), nbins(n){ }
  ~Panel() { }
  
  Int_t Getnumber() const { return number; }
  Int_t GetChannel() const { return Channel; }
  TString GetPanelTitle() const { TString s="SIS"; s+=(1+Channel); s+=" "; s+=Title; return s; }
  Double_t GetXmin() const { return xmin; }
  Double_t GetXmax() const { return xmax; }
  Int_t Getnbins() const { return nbins; }

private:
  Int_t number;
  Int_t Channel;
  TString Title;
  Double_t xmin;
  Double_t xmax;
  Int_t nbins;
};

class MonitorXML {
public:
  MonitorXML() {
    listofpanels = new TList();
  }
  
  Int_t ParseFile( TString filename ){
    TDOMParser *domParser = new TDOMParser();
    domParser->SetValidate(false);
    Int_t parsecode = domParser->ParseFile(filename);
    if(parsecode < 0) {
      std::cerr << domParser->GetParseCodeMessage(parsecode) << std::endl;
      return -1;
    }
    TXMLNode * node = domParser->GetXMLDocument()->GetRootNode();
    
    totalpanels = 0;
    ParseDetails(node);
    return 0;
  }
  
  Panel* ParsePanel(TXMLNode *node, Int_t number) {
    Int_t Channel=0;
    TString Title;
    Double_t xmin = 0;
    Double_t xmax = 0;
    Int_t nbins = 0;
  
    for(;node;node = node->GetNextNode()){
      if(node->GetNodeType() == TXMLNode::kXMLElementNode) {
        if(strcmp(node->GetNodeName(),"Channel")==0)
          Channel = atoi(node->GetText());
        if(strcmp(node->GetNodeName(),"Title")==0)
          Title = node->GetText();
        if(strcmp(node->GetNodeName(),"Xmin")==0)
          xmin = atof(node->GetText());
        if(strcmp(node->GetNodeName(),"Xmax")==0)
          xmax = atof(node->GetText());
        if(strcmp(node->GetNodeName(),"Nbins")==0)
          nbins = atoi(node->GetText());       
      }
    }
    return new Panel(number, Channel, Title, xmin, xmax, nbins);
  }
  
  void ParseDetails(TXMLNode *node) {
    
    for(;node;node = node->GetNextNode()){
      if(node->GetNodeType() == TXMLNode::kXMLElementNode) {
        //printf("Title: %s\n",node->GetText());
        if(strcmp(node->GetNodeName(),"WindowTitle")==0)
          WindowTitle = node->GetText();
        if(strcmp(node->GetNodeName(),"Length")==0)
          Length = atof(node->GetText());
        if(strcmp(node->GetNodeName(),"nCols")==0)
          nCols = atoi(node->GetText());
        if(strcmp(node->GetNodeName(),"nRows")==0)
          nRows = atoi(node->GetText());
        if(strcmp(node->GetNodeName(),"xcanvas")==0)
          xcanvas = atoi(node->GetText()); 
        if(strcmp(node->GetNodeName(),"ycanvas")==0)
          ycanvas = atoi(node->GetText()); 
        if(strcmp(node->GetNodeName(),"text_size")==0)
          text_size = atof(node->GetText()); 
        if(strcmp(node->GetNodeName(),"xlabelsize")==0)
          xlabelsize = atof(node->GetText()); 
        if(strcmp(node->GetNodeName(),"ylabelsize")==0)
          ylabelsize = atof(node->GetText()); 
        if(strcmp(node->GetNodeName(),"ylabeloffset")==0)
          ylabeloffset = atof(node->GetText());
        if(strcmp(node->GetNodeName(),"ynoexponent")==0)
          ynoexponent = atoi(node->GetText()); 
        if(strcmp(node->GetNodeName(),"Panel")==0) {
          int number = 0;
          totalpanels++;
          if(node->HasAttributes()) {
            TList *attrList = node->GetAttributes();
            TXMLAttr *attr = 0;
            TIter next(attrList);
            while((attr=(TXMLAttr*)next())){
              if( strcmp(attr->GetName(),"number") == 0 ) {
                number = atoi(attr->GetValue());
                break;
              }
            }
          }
          listofpanels->AddAt(ParsePanel(node->GetChildren(),number),number);
        }         
      }
      ParseDetails(node->GetChildren());
    }
  }  
  
  Int_t Gettotalpanels() const { return totalpanels; }
  TString GetWindowTitle() const { return WindowTitle; }
  Double_t GetLength() const { return Length; }
  Int_t GetnCols() const { return nCols; }
  Int_t GetnRows() const { return nRows; }
  Int_t Getxcanvas() const { return xcanvas; }
  Int_t Getycanvas() const { return ycanvas; }
  Double_t Gettext_size() const { return text_size; }
  Double_t Getxlabelsize() const { return xlabelsize; }
  Double_t Getylabelsize() const { return ylabelsize; }
  Double_t Getylabeloffset() const { return ylabeloffset; }
  bool Getynoexponent() const { return ynoexponent;}
  Panel* GetPanel(int number) const {
    return (Panel*)listofpanels->At(number); }

private:
  TList* listofpanels;
  Int_t totalpanels;
  Double_t Length;
  TString WindowTitle;
  Int_t nCols;
  Int_t nRows;
  Int_t xcanvas;
  Int_t ycanvas;
  Double_t text_size;
  Double_t xlabelsize;
  Double_t ylabelsize;
  Double_t ylabeloffset;
  bool ynoexponent;
  Panel* detailsPanel;
};


MonitorXML *gMonitorXML = NULL;


int ShowMem(const char* label);

// Global Variables
int  gRunNumber = 0;
bool gIsRunning = false;
bool gIsOffline = false;

VirtualOdb	 		*gOdb = NULL;

static double GetTimeSec()
{
  struct timeval tv;
  gettimeofday(&tv,NULL);
  return tv.tv_sec + 0.000001*tv.tv_usec;
}

class MyPeriodic : public TTimer
{
public:
  typedef void (*TimerHandler)(void);

  int          fPeriod_msec;
  TimerHandler fHandler;
  double       fLastTime;
  bool         fInHandler;

  MyPeriodic(int period_msec,TimerHandler handler)
  {
    assert(handler != NULL);
    fPeriod_msec = period_msec;
    fHandler  = handler;
    fLastTime = GetTimeSec();
    fInHandler = false;
    Start(period_msec,kTRUE);
  }

  Bool_t Notify()
  {
    // kill recursive calls into Timer handlers
    if (fInHandler)
      return kTRUE;

    double t = GetTimeSec();
    //printf("timer notify, period %f should be %f!\n",t-fLastTime,fPeriod_msec*0.001);

    if (t - fLastTime >= 0.9*fPeriod_msec*0.001)
      {
        fInHandler = true;
        //printf("timer: call handler %p\n",fHandler);
        if (fHandler)
          (*fHandler)();

        fLastTime = t;
        fInHandler = false;
      }

    Reset();
    return kTRUE;
  }

  ~MyPeriodic()
  {
    TurnOff();
  }
};

uint64_t gSaveClock = 0;
time_t   gSaveTime  = 0;
uint64_t gSaveClock_2 = 0;
time_t   gSaveTime_2  = 0;

double clock2time(uint64_t clock)
{
  const double freq = 10000000.0; // 10 MHz AD clk

  if (gSaveClock == 0)
	    {
		  gSaveClock = clock;
	    }
  return (clock - gSaveClock)/freq;

}

double clock2time(uint64_t clock, uint64_t offset )
{
  const double freq = 10000000.0; // 10 MHz AD clk

  return (clock - offset)/freq;
}

double clock2time_2(uint64_t clock)
{
  const double freq = 10000000.0; // 10 MHz AD clk

  if (gSaveClock_2 == 0)
	    {
		  gSaveClock_2 = clock;
	    }
  return (clock - gSaveClock_2)/freq;
}


void startRun(int transition,int run,int time)
{
  gIsRunning = true;
  gRunNumber = run;
  printf("Begin run: %d, IsRunning: %d\n", gRunNumber, gIsRunning);

  ShowMem("startRun::entry");
	
  // reinitialize the clocks
  gSaveClock = 0;
  gSaveTime = 0;	
  gSaveClock_2 = 0;
  gSaveTime_2 = 0;	

  std::vector<SIS_channel*>::iterator c_it;
  for( c_it=g_SIS_channels.begin(); c_it != g_SIS_channels.end(); c_it++ )
    {
      SIS_channel* channel = *c_it;
      channel->Clear();
    }

  ShowMem("startRun::exit");
}


void endRun(int transition,int run,int time)
{
  gIsRunning = false;
  gRunNumber = run;

  ShowMem("endRun::exit");

  printf("End of run %d\n",run);
}

void Draw_SIS()
{
  static TCanvas * gCanvas = NULL;

  if(!gCanvas)
    {
      gCanvas = new TCanvas((const char*)gMonitorXML->GetWindowTitle(),
                            (const char*)gMonitorXML->GetWindowTitle(),
                            gMonitorXML->Getxcanvas(),
                            gMonitorXML->Getycanvas());
      gCanvas->Divide(gMonitorXML->GetnCols(),gMonitorXML->GetnRows(),0.001,0.001);
    }

  Int_t i = 0;
  std::vector<SIS_channel*>::iterator c_it;
  for( c_it=g_SIS_channels.begin(); c_it != g_SIS_channels.end(); c_it++ )
    {
      SIS_channel* channel = *c_it;

      channel->UpdateHisto();
      gCanvas->cd(gMonitorXML->GetPanel(i)->Getnumber());
      channel->histo->Draw();
      i++;
    }

  gCanvas->Modified();
  gCanvas->Update();
}

void HandleSIS(int numChan,int size,const void*ptr,int size_2,const void*ptr_2)
{
  const uint32_t *sis = (uint32_t*)ptr;
  const uint32_t *sis_2 = (uint32_t*)ptr_2; // Pointer to the second SIS data

  static uint64_t gClock = 0;
  static uint64_t gClock_2 = 0;

  // loop over databank 
  int i;
  for (i=0; i<size; i+=numChan, sis+=numChan)
    {
	  uint64_t clock = sis[0]; // num of 10MHz clks
      gClock += clock;	

      for( Int_t c_it = 0; c_it < (Int_t)g_SIS_channels.size(); c_it++ )
        {
          SIS_channel* channel = g_SIS_channels.at(c_it);
          if(channel->channel >=32 ) continue;

          Int_t counts = sis[channel->channel];
          SIS_readout* ro = new SIS_readout();
          ro->counts = counts;
          ro->runtime = clock2time(gClock);
          channel->AddReadout(ro);
        }

    }//loop over databank (n.b. pointer to databank, sis, increases by numChan on each loop

  assert(i==size);
 
  if(size_2)
   {
     for (i=0; i<size_2; i+=numChan, sis_2+=numChan)
       {
         uint64_t clock_2 = sis_2[0]; // num of 10MHz clks
         gClock_2 += clock_2;

         for( Int_t c_it = 0; c_it < (Int_t)g_SIS_channels.size(); c_it++ )
           {
             SIS_channel* channel = g_SIS_channels.at(c_it);
             if(channel->channel <32 ) continue;

             Int_t counts = sis_2[channel->channel - 32];
             SIS_readout* ro = new SIS_readout();
             ro->counts = counts;
             ro->runtime = clock2time(gClock_2);
             channel->AddReadout(ro);
           }
       }
   }

}


void HandleMidasEvent(TMidasEvent& event)
{
  int eventId = event.GetEventId();
  //event.Print();
  
  if ((eventId == 1)&&(gIsRunning==true)) // SIS data (legacy)
    {
    }
  else if ((eventId==6)&&(gIsRunning==true)) // LabVIEW data
    {
    }
  else if((eventId==4)&&(gIsRunning==true)) // VF48 (legacy)
    {
    }
  else if((eventId==8)&&(gIsRunning==true)) // Sequencer 
    {
    }
  else if (eventId == 11) // VME bulk data
    {
      void *ptr0;
      int size0 = event.LocateBank(NULL,"MCS0",&ptr0);
      // Pointer to the second SIS module
      void *ptr1;
      int size1 = event.LocateBank(NULL,"MCS1",&ptr1);

      //printf( "sis1 \t %d \t %d \t sis2 \t %d \t %d \n",size0, (size0 % 32), size1, (size1 % 32) );   
      assert( size0 % 32 == 0 && size1 % 32 == 0 ); // check SIS databank size is a muliple of 32 

      if ( (size0>0 || size1>0) ) // Don't bother with SIS when searching for si events
        {
          HandleSIS(32,size0,ptr0,size1,ptr1);
        }
    }
  else
    {
      //event.Print();
    }
}

void eventHandler(const void*pheader,const void*pdata,int size)
{
  TMidasEvent event;
  memcpy(event.GetEventHeader(),pheader,sizeof(TMidas_EVENT_HEADER));
  event.SetData(size, (char*)pdata);
  event.SetBankList();
  HandleMidasEvent(event);
}

int ProcessMidasFile(TApplication*app,const char*fname)
{
  TMidasFile f;
  bool tryOpen = f.Open(fname);

  if (!tryOpen)
    {
      printf("Cannot open input file \"%s\", error %d (%s)\n", fname, f.GetLastErrno(), f.GetLastError());
      return -1;
    }

  // Initalize the clock2time() function
  
  while (1)
    {
      TMidasEvent event;
      if (!f.Read(&event))
        {
          printf("Cannot read event from file, error %d (%s)\n", f.GetLastErrno(), f.GetLastError());
          break;
        }

      int eventId = event.GetEventId();
      //printf("Event %d of type %d\n",i, eventId);

      if ((eventId & 0xFFFF) == 0x8000)
        {
          // begin run
          //event.Print();

          //char buf[256];
          //memset(buf,0,sizeof(buf));
          //memcpy(buf,event.GetData(),255);
          //printf("buf is [%s]\n",buf);

          //
          // Load ODB contents from the ODB XML file
          //
          if (gOdb)
            delete gOdb;
          gOdb = new XmlOdb(event.GetData(),event.GetDataSize());

          startRun(0,event.GetSerialNumber(),0);
        }
      else if ((eventId & 0xFFFF) == 0x8001)
        {
	  // end run
	  break;
        }
      else
        {
          event.SetBankList();
          //event.Print();
          HandleMidasEvent(event);
        }
	
    }//while(1)

  endRun(0,gRunNumber,0);
  f.Close();
  return 0;
}

#ifdef HAVE_MIDAS


void MidasPollHandler()
{
  if (!(TMidasOnline::instance()->poll(0)))
    gSystem->ExitLoop(); 
}

int ProcessMidasOnline(TApplication*app)
{
   
   TMidasOnline *midas = TMidasOnline::instance();

   int err = midas->connect(NULL,NULL,"alphaonline"); 
   assert(err == 0);

   gOdb = midas;

   midas->setTransitionHandlers(startRun,endRun,NULL,NULL);
   midas->registerTransitions();

   /* reqister event requests */

   midas->setEventHandler(eventHandler);       // call 'eventHandler' when an event comes in
   midas->eventRequest("SYSTEM",-1,-1,(1<<1));

   /* fill present run parameters */

   gRunNumber = gOdb->odbReadInt("/runinfo/Run number");

   if ((gOdb->odbReadInt("/runinfo/State") == 3))
     startRun(0,gRunNumber,0);

   printf("Startup: run %d, is running: %d\n",gRunNumber,gIsRunning);
   
   /*---- start main loop ----*/
   MyPeriodic tm(100,MidasPollHandler);
   MyPeriodic tp(1000,Draw_SIS);

   //loop_online();
   app->Run(kTRUE);

   /* disconnect from experiment */
   midas->disconnect();

   return 0;
}

#endif


static bool gEnableShowMem = false;

int ShowMem(const char* label)
{
  if (!gEnableShowMem)
    return 0;

  FILE* fp = fopen("/proc/self/statm","r");
  if (!fp)
    return 0;

  int mem = 0;
  fscanf(fp,"%d",&mem);
  fclose(fp);

  if (label)
    printf("memory at %s is %d\n", label, mem);

  return mem;
}

void help()
{
  printf("\nALPHA online usage:\n");
  printf("\n./alphaonline.exe [file1]\n");
  exit(1);
}

TApplication* xapp;

// Main function call

int main(int argc, char *argv[])
{
  printf("***************************\n"); 	 
  printf("     ALPHA  online         \n"); 	 
  printf("***************************\n\n");

  setbuf(stdout,NULL);
  setbuf(stderr,NULL);

  signal(SIGILL,  SIG_DFL);
  signal(SIGBUS,  SIG_DFL);
  signal(SIGSEGV, SIG_DFL);
 
  std::vector<std::string> args;
  for (int i=0; i<argc; i++)
    {
      args.push_back(argv[i]);
      if (strcmp(argv[i],"-h")==0)
        help();
    }

  if(gROOT->IsBatch()) {
    printf("Cannot run in batch mode\n");
    return 1;
  }

  gStyle->SetOptStat(0);
  gStyle->SetStatFontSize(0.09);
  gStyle->SetTextSize(0.1);
  gStyle->SetTitleFontSize(0.1);
  gStyle->SetTitleAlign(23);
  gStyle->SetTitleX(0.5);
  
  if(!gMonitorXML) {
    gMonitorXML = new MonitorXML();
    if(gMonitorXML->ParseFile("cfg.xml") != 0) {
      printf("Cannot open cfg.xml");
      return 1;
    }
  }

  for(int i = 0;i<gMonitorXML->Gettotalpanels();i++) {
    char title[80];
    sprintf(title,"%s",(const char*)gMonitorXML->GetPanel(i)->GetPanelTitle());

    SIS_channel* c = new SIS_channel();
    c->channel = gMonitorXML->GetPanel(i)->GetChannel();
    c->histo = new TH1D(title,
                        title,
                        gMonitorXML->GetPanel(i)->Getnbins(),
                        gMonitorXML->GetPanel(i)->GetXmin(),
                        gMonitorXML->GetPanel(i)->GetXmax()); 
 
    c->histo->GetXaxis()->SetLabelSize(gMonitorXML->Getxlabelsize());  
    c->histo->GetYaxis()->SetLabelSize(gMonitorXML->Getylabelsize());
    c->histo->GetYaxis()->SetLabelOffset(gMonitorXML->Getylabeloffset());
    c->histo->GetYaxis()->SetNoExponent(gMonitorXML->Getynoexponent());

    g_SIS_channels.push_back(c);
  }

  TApplication *app = new TApplication("alphaAnalysis", &argc, argv);
  extern TApplication* xapp;
  xapp = app;
   
  // gErrorIgnoreLevel = 2000; // (0)>=Print,(1000)>=Info,(2000)>=Warning,(3000)>=Error
  // printf("Output information level set to %d \n", gErrorIgnoreLevel );
  // printf("where (0)>=Print,(1000)>=Info,(2000)>=Warning,(3000)>=Error \n\n" );

  for (unsigned int i=1; i<args.size(); i++) // loop over the commandline options
    {
      const char* arg = args[i].c_str();
      //printf("argv[%d] is %s\n",i,arg);
	   
      if (strncmp(arg,"-m",2)==0) // Enable memory debugging
        gEnableShowMem = true;
      else if (strncmp(arg,"-commands",9)==0)
        help(); // does not return
      else if (arg[0] == '-')
        help(); // does not return
    }
    
  gIsOffline = false;

  for (unsigned int i=1; i<args.size(); i++)
    {
      const char* arg = args[i].c_str();

      if (arg[0] != '-' && !isdigit( arg[0] ) )  
        {  
          gIsOffline = true;
    	  ProcessMidasFile(app,arg);
	    }
    }

  // if we processed some data files,
  // do not go into online mode.
  ShowMem("::End end");
  if(gIsOffline) return 0;

  gIsOffline = false;
#ifdef HAVE_MIDAS
  ProcessMidasOnline(app);
#endif
   
  delete app;	
  return 0;
}
//end

