// HandleVF48.cxx

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdint.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <string>
#include <map>

using std::ifstream;
using std::cout;
#include "TFile.h"
#include "TH2.h"
#include "TF1.h"
#include "TLatex.h"
#include "TText.h"
#include "TBox.h"
#include "TApplication.h"
#include "TCanvas.h"
#include "TTree.h"
#include "TSystem.h"

#include "UnpackVF48.h"
UnpackVF48* vfu = new UnpackVF48();

#include "TROOT.h"
#include "TFolder.h"
#include "TMath.h"
#include "TStyle.h"
#include "libMidasServer/midasServer.h"

#include <TGFrame.h>
#include <TGNumberEntry.h>
#include <TButton.h>

#include "./lib/include/TSiliconEvent.h"
#include "./lib/include/TSisEvent.h"
#include "./lib/include/TADCTriggerEvent.h"
#include "./lib/include/TTCEvent.h"
#include "./lib/include/TSiliconStrip.h"
#include "./lib/include/TSiliconVA.h"
#include "./lib/include/TSiliconPedestals.h"
#include "./lib/include/TSiliconStripRMS.h"
#include "./lib/include/TUtilities.h"
#include "./lib/include/TSISChannels.h"
#include "./lib/include/TAnalysisReport.h"
#include "./lib/include/TalphaStripsReport.h"
#include "./lib/include/TTimestampHelper.h"

#include "./lib/include/TSettings.h"

#include "TAbsUtil.h"


#include "TAlphaEventVertex.h"
#include "TAlphaEventCosmicHelix.h"
#include "TAlphaEvent.h"
#include "TAlphaDisplay.h"

#define NUM_SI_MODULES nSil // defined in alphavmc/include/SiMod.h
#define NUM_VF48_MODULES nVF48 // defined in alphavmc/include/SiMod.h

#define NUM_SI_ALPHA1 nSiAlpha1 // defined in alphavmc/include/SiMod.h
#define NUM_SI_ALPHA2 nSiAlpha2 // defined in alphavmc/include/SiMod.h

#define NUM_DAILY_RUNS 1000  // search back n. runs when looking for alphaStrips output

// debug settings
#define VERBOSE 0
#define DEBUG 0

extern Bool_t         gWriteAlphaEventTree;
extern TAlphaEvent    *gAlphaEvent;
extern TTree          *gAlphaEventTree;
extern TObjArray      *gAlphaEvents;
extern TAnalysisReport *gAnalysisReport;
extern TalphaStripsReport *gStripReport;
extern TTimestampHelper *TimeStampHelper;
extern TTree            *gTimestampHelperTree;
TAlphaEvent* AlphaEvent = NULL;

extern TFile          *gOutputFile;

extern TSiliconEvent  *gSiliconEvent;
extern TSisEvent      *gSisEvent;
extern TTree          *gSisTree;
extern TTCEvent       *gTTCEvent;
extern int32_t              gTTCEventID;
extern TTree	      *gSiliconTree;
extern TObjArray      *gSiliconEvents;
extern TObjArray      *gTTCEvents;
uint32_t LastTTCEntry=0; // counter to record the last TTC saved to sil_event... save looping over whole tree
//std::map <uint32_t,uint32_t> TTCmap;
std::map <uint32_t,uint32_t> FPGA0map;
std::map <uint32_t,uint32_t> FPGA1map;
std::map <uint32_t,uint32_t> FPGA2map;
std::map <uint32_t,uint32_t> FPGA3map;

extern Double_t        gVF48RunTimeOffset;
extern TSystem *gSystem;

extern TObjArray *gADCTriggerEvents;

extern int gSisCounter;
extern int gLabVIEWCounter;
extern int gVertexCounter;
extern TSISChannels	    *gSISChannels;

extern TTree               *gSisTreeArray[65];
extern TSisEvent           *gSisEventArray[64];


extern Bool_t g_flag_siliconEvents_ready_to_flush;

extern int gRunNumber;
extern int outRunNumber;
extern bool gIsOffline;
extern bool gIsCosmicRun;
extern bool gDelCosmic;
extern int gExptNumber_2;

extern bool gBlindNumber;

extern Int_t gEventID;
extern std::vector<Int_t> gEventIDs;

extern Int_t gSiEventNum;
extern Int_t gSiEventLast;
extern Int_t gDisplayEvent;
extern Int_t gAutoSaveEventDisplay;
extern Int_t gNHitsCut;
extern Int_t gTerminateEventDisplay;

bool gDoSamplesVF48 = false;

extern Bool_t gEnableSiDAQCtrl;

extern uint16_t thres;

extern Bool_t gForceStripsFile;
extern char CustomStripsFile[];

extern Bool_t gUseTimeRange;
extern Double_t starttime;
extern Double_t stoptime;

extern Bool_t gRecOff;
extern Bool_t gEnableSuppression;
extern Bool_t gPolyFit;
extern TUtilities* gUtils;

#include "./lib/include/TVF48SiMap.h"
extern TVF48SiMap * gVF48SiMap;

#include "include/XmlOdb.h"
extern XmlOdb *gOdb;

extern Int_t gStripRun;
extern TObjString *galphaStrip_filename;

//External clock sync 
extern unsigned long int gClock[NUM_SIS_MODULES];
extern unsigned long int gvf48clock;

int ShowMem(const char* label);


extern TSettings* gSettingsDB;

// Strip RMS arrays
// Strip number = 512*(Si Module # ) + 128*(ASIC # - 1) + Strip #
// ... where Si Modules #s {0,...,59} & ASIC # {1,2,3,4} & Strip # {0,...,127}
// <<<<<<< ------
Double_t StripRMSs[NUM_SI_MODULES*4*128];
Double_t StripMeans[NUM_SI_MODULES*4*128];
Double_t SumRMSs[NUM_SI_MODULES*4*128];
// end Strip RMS arrays

TTree * pol_tree = NULL;
Int_t polVF48EventNumber;
Float_t pol0[NUM_SI_MODULES*4];
Float_t pol1[NUM_SI_MODULES*4];
Float_t pol2[NUM_SI_MODULES*4];
Float_t chi[NUM_SI_MODULES*4];

extern std::vector<TAbsUtil*> gAddons;

extern double nVASigma;
extern double nClusterSigma;
extern double pVASigma;
extern double pClusterSigma;
extern Double_t hitSigmaCut;
extern Double_t hitThresholdCut;


extern Bool_t gCustomTrackCuts;
extern Double_t gHitSepCutPhi;
extern Double_t gHitSepCutZ;
extern Double_t gCorCut;
extern Double_t gChi2Cut;
extern Double_t gd0_trapCut;

extern Bool_t gCustomVertCuts;
extern Double_t gVertRadCut;
extern Double_t gVertDCACut;


#define MAX_CHANNELS VF48_MAX_CHANNELS
#define VF48_COINCTIME 0.000010

// see UnpackVF48.* in:
// URL: https://ladd00.triumf.ca/svn/daqsvn/trunk/vme
// Repository Root: https://ladd00.triumf.ca/svn/daqsvn


////set VF48 sub sampling 'divisor' parameter
//      int divisor = odbReadInt("/equipment/VF48/Settings/VF48_Divisor", i, 0);
//      int common_pretrigger = odbReadInt("/equipment/VF48/Settings/Common_Pretrigger", 0, 0);
//      int pretrigger = odbReadInt("/equipment/VF48/Settings/VF48_Pretrigger", i, 0);

double gSettingsSubsample[VF48_MAX_MODULES];
int    gSettingsOffset[VF48_MAX_MODULES];
int    gSettingsSoffset[VF48_MAX_MODULES];
int    gSettingsSamples[VF48_MAX_MODULES];

double gSettingsFrequencies[VF48_MAX_MODULES];
double StripGains[512][NUM_SI_MODULES];

//zero suppression counters March2015
int suppEvents;
int suppTotal;
int suppMod[VF48_MAX_MODULES]; // 8 vf48 modules
int suppFE[VF48_MAX_MODULES][6]; // 8*6
int suppChan[VF48_MAX_MODULES][6][8]; // 8*6*8
int totalTotal;
int totalMod[VF48_MAX_MODULES]; // 8 vf48 modules
int totalFE[VF48_MAX_MODULES][6]; // 8*6
int totalChan[VF48_MAX_MODULES][6][8]; // 8*6*8

//2012 Passed cuts:
Double_t REScuteq2=2.0;
Double_t RADcuteq2=4.0;
Double_t REScutgr2=0.05;
Double_t RADcutgr2=4.0;

Double_t gVF48Grad=1;


// TODO(jfriedly):  This should be a utility somewhere.  
Bool_t ListIsEmpty(TList* l)
{
  // First() returns 0 when a TList is empty.
  if (l->First() == 0)
      return true;
  return false;
}


void VF48BeginRun()
{
  suppEvents = 0;
  suppTotal = 0;
  totalTotal = 0;
  for (int i=0; i<VF48_MAX_MODULES; i++) {
    totalMod[i] = 0;
    suppMod[i] = 0;
    for (int j=0; j<6; j++) {
      totalFE[i][j] = 0;
      suppFE[i][j] = 0;
      for (int k=0; k<8; k++) {
	totalChan[i][j][k] = 0;
	suppChan[i][j][k] = 0;
      }
    }
  }

  int module = 0;
  int samples    = gOdb->odbReadInt("/equipment/VF48/Settings/VF48_NumSamples",module,0);
  int grpEnabled = gOdb->odbReadInt("/equipment/VF48/Settings/VF48_GroupEnable",module,0);

  if (VERBOSE)
    printf("VF48BeginRun: Module %d, samples: %d, grpEnable: 0x%x\n", module, samples, grpEnabled);

  //vfu->fDisasmStream = true ;
  vfu->SetFlushIncompleteThreshold(40);
  vfu->SetNumModules(NUM_VF48_MODULES);
  vfu->SetGroupEnableMask(-1, grpEnabled);
  vfu->SetNumSamples(-1, samples);
  vfu->SetCoincTime(VF48_COINCTIME);
  vfu->Reset();

  for (int m=0; m<NUM_VF48_MODULES; m++)
    {
      gSettingsFrequencies[m]= gSettingsDB->GetVF48Frequency( gRunNumber, m);
      vfu->SetTsFreq(m,gSettingsFrequencies[m]);
    }

  // load gains for each strip in each module
  std::string gainstr;
  TString stripgn_filename(getenv("RELEASE"));

  if(NUM_SI_MODULES == NUM_SI_ALPHA2)
    stripgn_filename+="/aux/HYBGAINS.ALPHA2.TXT";
  else if(NUM_SI_MODULES == NUM_SI_ALPHA1)
    stripgn_filename+="/aux/HYBGAINS.ALPHA1.TXT";
  else {
    printf("error while loading gain file: num of silicon modules mismatch: %d", NUM_SI_MODULES);
    exit(123);
  }
  std::cout<<"Using GAIN file: "<<stripgn_filename<<std::endl;

  std::ifstream myfile(stripgn_filename.Data());
  std::string aline;

  double gainMin=FLT_MAX;
  double gainMax=0;

  int loop=0;

  if (myfile.is_open()) { //if the file is open
    while(myfile.good()&& loop<512){

      getline(myfile,aline); //get one line from the file

      std::stringstream ss(aline);

      for (int b=0;b<NUM_SI_MODULES;b++) { // silicon module
        ss >> StripGains[loop][b];
        if (StripGains[loop][b]<gainMin)
          gainMin=StripGains[loop][b];
        if (StripGains[loop][b]>gainMax)
          gainMax=StripGains[loop][b];
      }
      loop++;
    }
    myfile.close(); //closing the file
  }
  else {
    std::cout << "Unable to open file " << stripgn_filename.Data() << std::endl; //if the file is not open output
  }
  std::cout << " gain range: " << gainMin << " - " << gainMax << std::endl;

  if (gPolyFit) gAnalysisReport->SetPolyFit(true);

  if (!gWriteAlphaEventTree) gAnalysisReport->WriteAlphaEventFalse();
  // Read Si strip pedestals for this run
  if(!gRecOff)
  {

    TFile * striprms_file = NULL;
    TTree * striprms_tree = NULL;

    gStripRun = gRunNumber;
    if ( gForceStripsFile )
    {
      if (gBlindNumber) std::cout <<"WARNING: alphaStrips file number will not be blinded in TAnalysisReport object"<<std::endl;
      char striprms_filename[256];
      if (CustomStripsFile[0]=='/')
      {
        sprintf(striprms_filename,"%s",CustomStripsFile);
      }
      else
      {
        //cout << "no leading slash" <<endl;
        sprintf(striprms_filename,"%s/%s",getenv("STRIPFILES"),CustomStripsFile);
      }
      striprms_file = new TFile( striprms_filename, "READ" );
      gStripRun=0;
      striprms_tree = (TTree*) striprms_file->Get("alphaStrip Tree");
      if (gPolyFit) pol_tree = (TTree*) striprms_file->Get("polStripTree");
      

      printf("Using FORCED alphaStrips file: %s\n", striprms_filename);
      gAnalysisReport->SetStripsFileForced(kTRUE);
      gAnalysisReport->SetStripFile(striprms_filename);
    }
    else
    {
      gAnalysisReport->SetStripsFileForced(kFALSE);
      for( Int_t run = gRunNumber; run > gRunNumber - NUM_DAILY_RUNS; run-- )
      {
        if (run != gRunNumber && gBlindNumber) break; //force the same strip file when blinding runNumber
        char striprms_filename[256];
        sprintf( striprms_filename, "%s/alphaStrips%doffline.root", getenv("STRIPFILES"), run );
        FileStat_t filestat_buf;
        if (gSystem->GetPathInfo(striprms_filename, filestat_buf) ) // file doesn't exist
        {
	  std::cout<<"Stripfile for"<<run<<"  not found"<<std::endl;
          continue;
        }
        striprms_file = new TFile( striprms_filename, "READ" );
        gStripRun=run;
        if( !striprms_file->IsOpen() )
        {
          striprms_file->Close();
          striprms_file = NULL;
        }
        else
        {
          if (ListIsEmpty(striprms_file->GetListOfKeys()))
          {
            cout << "Skipping alphaStrips" << run << "offline.root because it has no keys" << endl;
            continue;
          }
          striprms_tree = (TTree*) striprms_file->Get("alphaStrip Tree");
          if (gPolyFit) pol_tree = (TTree*) striprms_file->Get("polStripTree");
          printf("Using alphaStrips file: %s\n", striprms_filename);
          if (!gBlindNumber) gAnalysisReport->SetStripFile( striprms_filename);
          else
          {
            TString blind_filename(getenv("STRIPFILES"));
            blind_filename+="_BLINDEDNUMBER_"; //hide run number from blinded output
            blind_filename+="offline.root";
            gAnalysisReport->SetStripFile(blind_filename);
          }
          break;
        }
      }
    }
   

    if( !(striprms_file) || !striprms_file->IsOpen() || striprms_file->IsZombie() || ListIsEmpty(striprms_file->GetListOfKeys()))
    {
      printf("Cannot find strip RMS file, exiting\n");
      exit(1);
    }
    TDatime fileDate = striprms_file->GetCreationDate();
    gAnalysisReport->SetStripFileDate(fileDate);

    Int_t stripNumber(0);
    Float_t stripRMS(5.);
    Float_t polRMS(0.);
    Float_t stripMean(0.);



    striprms_tree->SetBranchAddress("stripNumber", &stripNumber );
    if (VERBOSE) printf("Set stripNumber branch address\n");
    
    if (gPolyFit)
      striprms_tree->SetBranchAddress("stripPolRMS", &stripRMS );
    else
      striprms_tree->SetBranchAddress("stripMeanSubRMS", &stripRMS );
    if (VERBOSE) printf("Set stripMeanSubRMS branch address\n");
    striprms_tree->SetBranchAddress("stripMean", &stripMean );
    if (VERBOSE) printf("Set stripMean branch address\n");
    Int_t BadRMSValues=0; //JTKM:Count runs with negative RMS values (protection against zero detector data... ie detector off, trigger on)

    gStripReport = new TalphaStripsReport();
    //Check strips Report exists, add compatability to old strip files
    if (striprms_file->GetListOfKeys()->Contains("StripReportTree"))
    {
      TTree* StripReportTree=(TTree*) striprms_file->Get("StripReportTree");
      StripReportTree->SetBranchAddress("gStripReport",&gStripReport);
      StripReportTree->GetEntry();
    }
    gAnalysisReport->SetSVNalphaStripsRevision(gStripReport->GetSVNalphaStripsRevision());
    // gStripReport->Print();

    if (VERBOSE) printf("gPolyFit is %s\n", gPolyFit ? "true" : "false");
    if (gPolyFit)
    {
      striprms_tree->SetBranchAddress("stripPolRMS", &polRMS );
      pol_tree->SetBranchAddress("VF48EventNumber",&polVF48EventNumber);
      pol_tree->SetBranchAddress("pol0",pol0);
      pol_tree->SetBranchAddress("pol1",pol1);
      pol_tree->SetBranchAddress("pol2",pol2);
      pol_tree->SetBranchAddress("chi",chi);
    }

    //TH1D* RMSHisto=new TH1D("Strips RMS","Strip RMS",-1,99,100);
    //AO protection from crazy pedestal values
    for(Int_t i=0; i<(NUM_SI_MODULES*4*128); i++)
    {
      striprms_tree->GetEntry(i);
      assert(stripNumber < (NUM_SI_MODULES*4*128) );
      //RMSHisto->Fill( stripRMS );
      //StripRMSs[i] = fabs(stripRMS) < 200. ? stripRMS : 200.;  //This is a source of 'hot strips' bug...
      //StripMeans[i]= fabs(stripMean)<200? stripMean : 0.;    //Strips with high values of RMS or mean are set lower here... it results in the last strips of a ASIC often being 'noisey'
      if (stripRMS<0) BadRMSValues++;

      StripRMSs[i] = stripRMS;// fabs(stripRMS) < 200. ? stripRMS : 200.;
      StripMeans[i]= stripMean;//fabs(stripMean)<200? stripMean : 0.;
    }
    if ((double)BadRMSValues/((double)NUM_SI_MODULES*4.*128.)>0.2)
    {
      if (gStripRun == gRunNumber)
        {
          gRecOff=kTRUE;
          printf("################################################################\n");
          printf("Error: Strips files has %d strips with no good pedistal data... \nIf the detector is off, turn off the trigger too!\n",BadRMSValues);
          printf("Attempting to continue analysis with reconstruction off\n");
          printf("################################################################\n");
        }
        else
        {
          printf("Error: Bad strip file from run %d\n", gStripRun);
          exit(1234);
        }
    }
  //If more than 20% of the detector has no good RMS values... turn off reconstruction.
  // End read Si strip pedestals for this run
  }


  for( int vf48modnum=0; vf48modnum<NUM_VF48_MODULES; vf48modnum++ ) {

    // extract VF48 sampling parameters from sqlite db
    gSettingsSubsample[vf48modnum] = gSettingsDB->GetVF48subsample( gRunNumber, vf48modnum );
    gSettingsOffset[vf48modnum]    = gSettingsDB->GetVF48offset( gRunNumber, vf48modnum );
    gSettingsSoffset[vf48modnum]   = gSettingsDB->GetVF48soffset( gRunNumber, vf48modnum );
    gSettingsSamples[vf48modnum]   = gSettingsDB->GetVF48Samples( gRunNumber, vf48modnum );

    if ( gSettingsSamples[vf48modnum] != samples ) {
      printf("Warning: mismatch in module %i  between ODB samples = %i and SQL databases = %i /n", vf48modnum, samples, gSettingsSamples[vf48modnum]);
      exit(123);
    }

    if (VERBOSE)
      printf("VF48BeginRun: SQLLITE: Module %d \t subsample = %f \t offset = %d \t soffset = %d \t samples = %d \n", vf48modnum, gSettingsSubsample[vf48modnum], gSettingsOffset[vf48modnum], gSettingsSoffset[vf48modnum], gSettingsSamples[vf48modnum]);

  }



  // Start reconstruction

  AlphaEvent = new TAlphaEvent();
  if (gCustomTrackCuts)
    AlphaEvent->SetTrackCuts( gHitSepCutPhi, gHitSepCutZ, gCorCut,  gChi2Cut, gd0_trapCut);
  if (gCustomVertCuts)
    AlphaEvent->SetVertCuts( gVertRadCut, gVertDCACut);
  if (VERBOSE)
    AlphaEvent->GetVerbose()->SetLevel( 1 );
  else
    AlphaEvent->GetVerbose()->SetLevel( 0 );

  // init addons
  for (unsigned int i=0; i<gAddons.size(); i++){
    (gAddons.at(i))->Begin();
  }

}

void PruneADCEvents()
{
  
  std::cout <<"Pruning..."<<std::endl;
  //Pattern match VF48 timestamps with SIS ADC triggers... remove excess ADC triggers
                                                           //ADD IN MISSING? -> Set as -1
  Int_t SiliconEntries=gSiliconEvents->GetEntriesFast();
  std::vector<Double_t> VF48Times;
  std::vector<Double_t> SISTimes;
  //Track sis times position:
  std::vector<Int_t> SISID;
  for( Int_t i=0; i <SiliconEntries ; i++ )
  {
    gSiliconEvent = (TSiliconEvent*) gSiliconEvents->At(i);
    VF48Times.push_back(gVF48Grad*gSiliconEvent->GetVF48Timestamp()+ gVF48RunTimeOffset);
    TADCTriggerEvent * ADCTriggerEvent = (TADCTriggerEvent*) gADCTriggerEvents->At( gSiliconEvent->GetVF48NTrigger() - 1 );
    if (ADCTriggerEvent)
    {
      SISTimes.push_back(ADCTriggerEvent->GetRunTime());
      SISID.push_back(gSiliconEvent->GetVF48NTrigger() - 1 );
    }
  }
  
  //Time differences between timestamps 
  std::vector<Double_t> VF48Times_D1;
  std::vector<Double_t> SISTimes_D1;

  std::cout <<"VF48 Events: "<<VF48Times.size()<<std::endl;
  for( UInt_t i=1; i <VF48Times.size(); i++ )
  {
    VF48Times_D1.push_back(VF48Times[i]-VF48Times[i-1]);
  }
  std::cout <<"SIS Events: "<<SISTimes.size()<<std::endl;
  for (UInt_t i=1; i<SISTimes.size();i++)
  {
    SISTimes_D1.push_back(SISTimes[i]-SISTimes[i-1]);
  }
  
  //Find nearest timestamp (record offset to it)
  UInt_t Smallest= (VF48Times.size() > SISTimes.size())?VF48Times.size():SISTimes.size();
  Int_t OffsetMin[Smallest];
  for (UInt_t i=0; i<Smallest; i++)
  {
//    std::cout <<"Diff:\t";
    Int_t minj=-99;
    Double_t MinD=999.;
    for (Int_t j=-2; j<3; j++)
    {
      if (((Int_t)i+j)<0) continue;
      if ((i+j)>Smallest) continue;
//      std::cout<<VF48Times_D1[i]-SISTimes_D1[i+j] <<" \t";
      if (MinD>fabs(VF48Times_D1[i]-SISTimes_D1[i+j]))
      {
        MinD=fabs(VF48Times_D1[i]-SISTimes_D1[i+j]);
        minj=j;
      }
    }
    OffsetMin[i]=minj;
//    std::cout <<"j:"<<minj<<std::endl;
  }
  //Require the offset to aggree for 3 events in a row
  for (UInt_t i=3; i<Smallest-3; i++)
  {
    if (OffsetMin[i-3]==OffsetMin[i-1])
    if (OffsetMin[i-2]==OffsetMin[i-1])
    if (OffsetMin[i-1]<OffsetMin[i])
    if (OffsetMin[i]==OffsetMin[i+1])
    if (OffsetMin[i]==OffsetMin[i+2])
    if (OffsetMin[i]==OffsetMin[i+3])
    {
      std::cout <<"Offset step at "<<i<<std::endl;
      gADCTriggerEvents->RemoveAt( SISID[i] );
    }
  }
  gADCTriggerEvents->Compress();
  
}
  unsigned long int VF48Ticks=0;
  unsigned long int SISTicks=0;
  unsigned long int _50MHzTicks=0;
Int_t LastVF48Clock=0;
Int_t LastSISClock=0;
Int_t Last50MhzClock=0;
void FitVF48Gradient()
{
#if 0 //Slow old method that creates allot file IO... instead use global clock counts...
  int sisclockchan = gSISChannels->GetChannel("SIS_10Mhz_CLK");
  int VF48clockchan= gSISChannels->GetChannel("SIS_VF48_CLOCK");
  int _50MHzclockchan= gSISChannels->GetChannel("SIS_50Mhz_CLK");
  
  Int_t SISEntries = gSisTreeArray[sisclockchan]->GetEntriesFast();
  Int_t VF48Entries= gSisTreeArray[VF48clockchan]->GetEntriesFast();
  Int_t _50MHzEntries= gSisTreeArray[_50MHzclockchan]->GetEntriesFast();

  if (VF48Entries == SISEntries && SISEntries == _50MHzEntries)
  {
    if (VERBOSE) std::cout <<"SIS, VF48 and 50Mhz clock entries match"<<std::endl;
  }
  else
  {
    std::cerr<<"Warning: Calibration clock entries do not match:"<<std::endl;
    std::cerr<<"SISClockEntries: "<<SISEntries<<std::endl;
    std::cerr<<"VF48ClockEntries:"<<VF48Entries<<std::endl;
    std::cerr<<"50MhzClockEntries:"<<_50MHzEntries<<std::endl;
  }

  
  for (Int_t i=LastSISClock; i<SISEntries; i++)
  {
    gSisTreeArray[sisclockchan]->GetEntry(i);
    if (gSisEventArray[sisclockchan]->GetCountsInChannel()<2) std::cout <<"SIS:"<<i<<std::endl;
    SISTicks+=gSisEventArray[sisclockchan]->GetCountsInChannel();
  }
  LastSISClock=SISEntries;
  for (Int_t i=Last50MhzClock; i<_50MHzEntries; i++)
  {
    gSisTreeArray[_50MHzclockchan]->GetEntry(i);
    if (gSisEventArray[_50MHzclockchan]->GetCountsInChannel()<2) std::cout <<"50:"<<i<<std::endl;

    _50MHzTicks+=gSisEventArray[_50MHzclockchan]->GetCountsInChannel();
  }
  Last50MhzClock=_50MHzEntries;
  if (VF48clockchan<0) 
  {
	std::cerr<<"Warning: This run does not have VF48 clock recorded... faking it (turning off time calibration)"<<std::endl;
	//VF48Ticks=20*_50MHzTicks/50;
	//or Turn off time calibration:
	VF48Ticks=2*SISTicks;
  }
  else 
  if (SISEntries==0 && VF48Entries>10)
  {
    cerr<<"ERROR: NO 10MHz clock! Clock calibration will fail"<<endl;
    //SISTicks=VF48Ticks/2;
  }

  else
  for (Int_t i=LastVF48Clock; i<VF48Entries; i++)
  {
    gSisTreeArray[VF48clockchan]->GetEntry(i);
    if (gSisEventArray[VF48clockchan]->GetCountsInChannel()<2) std::cout <<"VF48:"<<i<<std::endl;
    VF48Ticks+=gSisEventArray[VF48clockchan]->GetCountsInChannel();
  }
  LastVF48Clock=VF48Entries;
  gAnalysisReport->AddCalibrationEvent(VF48Ticks, SISTicks);
  gVF48Grad = gAnalysisReport->GetVF48TimeCalibration(VF48Ticks);
  std::cout << "Clock calibration at "<<(Double_t)(VF48Ticks/20E6) <<"s. Drift: "<<1-gVF48Grad <<std::endl;

#else
  
  gAnalysisReport->AddCalibrationEvent(gvf48clock, gClock[0]);
  gVF48Grad = gAnalysisReport->GetVF48TimeCalibration(gvf48clock);
  //gVF48Grad= (Double_t)(SISTicks)/(Double_t) VF48Ticks *(2.);
  std::cout << "Clock calibration at "<<(Double_t)(gvf48clock/20E6) <<"s. Drift: "<<1-gVF48Grad <<std::endl;
#endif
  //gVF48Grad= (((Double_t) VF48Ticks / (Double_t) _50MHzTicks) * 5. / 2. );
}

void FlushSiliconEvents(Bool_t LastFlush=kFALSE) {
  int tmpid = gEventID;
  //TSiliconEvent* tmpsil = gSiliconEvent;
  //PruneADCEvents();
  FitVF48Gradient();
  //TObjArray* tempSiliconEvents=NULL; //Tempory storages for SiliconEvents that dont match with TTC events on this flush
  //TObjArray* tempSiliconEvents=new TObjArray();
  //TADCTriggerEvent* ADCTriggerEvent;
  Int_t TTCEntries=gTTCEvents->GetEntriesFast();
  for (Int_t j=LastTTCEntry; j<TTCEntries; j++)
  {
    gTTCEvent = (TTCEvent*) gTTCEvents->At(j);
    if (gTTCEvent != NULL)
    {
      if (gTTCEvent->GetFPGA()==0) FPGA0map[gTTCEvent->GetTTCNEvent() ]=j;//gTTCEvent->GetID( );
      if (gTTCEvent->GetFPGA()==1) FPGA1map[gTTCEvent->GetTTCNEvent() ]=j;//gTTCEvent->GetID( );
      if (gTTCEvent->GetFPGA()==2) FPGA2map[gTTCEvent->GetTTCNEvent() ]=j;//gTTCEvent->GetID( );
      if (gTTCEvent->GetFPGA()==3) FPGA3map[gTTCEvent->GetTTCNEvent() ]=j;//gTTCEvent->GetID( );
    }
    //else { std::cout << j <<" is NULL!?" << std::endl; }
  }
  
  LastTTCEntry=TTCEntries;
  Int_t LastWrittenSilArrayEntry=0;
  std::vector<Int_t> tempEventIDs;
  Int_t SiliconEntries=gSiliconEvents->GetEntriesFast();
  Int_t ADCEvents=gADCTriggerEvents->GetEntriesFast();
  for( Int_t i=0; i <SiliconEntries ; i++ )
  {
    gSiliconEvent = (TSiliconEvent*) gSiliconEvents->At(i);
    gEventID = gEventIDs.at(i);
    
    //printf("gSiliconEvent->GetVF48NTrigger() = %d \n", gSiliconEvent->GetVF48NTrigger() );
    if( ( gSiliconEvent->GetVF48NTrigger() - 1) < ADCEvents )
    {
      TADCTriggerEvent * ADCTriggerEvent = (TADCTriggerEvent*) gADCTriggerEvents->At( gSiliconEvent->GetVF48NTrigger() - 1 );
//SIS time
      gSiliconEvent->SetTSRunTime( ADCTriggerEvent->GetRunTime() );
//SIS time from mixing start
//SIS time from mixing start
      gSiliconEvent->SetExptTime( ADCTriggerEvent->GetExptTime() );
      gSiliconEvent->SetExptNumber( ADCTriggerEvent->GetExptNumber() );
      for (Int_t j=0; j<=3;j++)
      {
        Int_t TTCmapID=-1;
        if (j==0) TTCmapID=FPGA0map.find(gSiliconEvent->GetVF48NTrigger() -1 )->second;
        if (j==1) TTCmapID=FPGA1map.find(gSiliconEvent->GetVF48NTrigger() -1 )->second;
        if (j==2) TTCmapID=FPGA2map.find(gSiliconEvent->GetVF48NTrigger() -1 )->second;
        if (j==3) TTCmapID=FPGA3map.find(gSiliconEvent->GetVF48NTrigger() -1 )->second;
        //std::cout <<gSiliconEvent->GetVF48NTrigger()<<"FPGA"<<j+1 << ":" << TTCmapID << " " <<gSiliconEvent->GetTSRunTime()<<std::endl;
        if (TTCmapID<0) continue;
        gTTCEvent = (TTCEvent*) gTTCEvents->At(TTCmapID);
        //std::cout << TTCmapID <<"\t"<< gTTCEvent->GetTTCNEvent()<< ":" << gSiliconEvent->GetVF48NTrigger() - 1<<":"<<j << std::endl;
        //gSiliconEvent->SetTTCCounter(TTCmapID,gTTCEvent->GetFPGA());
        gSiliconEvent->SetTTCCounter(TTCmapID,j);
        //gSiliconEvent->SetTTCCounter((4*(gSiliconEvent->GetVF48NTrigger() - 1)+j),j);
      }
      //printf("ADCTrigTime %f VF48TS %f gVF48RunTimeOffset  %f\n",ADCTriggerEvent->GetRunTime(),
      //gSiliconEvent->GetVF48Timestamp(), gVF48RunTimeOffset);
    }
    //RunTime now set from gVF48Time corrected by  .492 ppm clock difference from SIS taken from run 36700 and VF48RunTimeOffset.
    //gSiliconEvent->SetRunTime( gSiliconEvent->GetVF48Timestamp()+ gSiliconEvent->GetVF48Timestamp()*4.920e-7+ gVF48RunTimeOffset );
    Int_t PushBackThreshold=500;
    if ((gSiliconEvent->GetTTCCounter(0)<=0
      || gSiliconEvent->GetTTCCounter(1)<=0
      || gSiliconEvent->GetTTCCounter(2)<=0
      || gSiliconEvent->GetTTCCounter(3)<=0)
      && (SiliconEntries-i)<PushBackThreshold && !LastFlush)
      //)
    {
      //std::cout <<gSiliconEvents->GetEntries() <<std::endl;
      for (Int_t j=i;j<SiliconEntries; j++)
      {
        //std::cout<<j <<" being pushed back... " << std::endl;
        gEventID = gEventIDs.at(j);
        tempEventIDs.push_back(gEventID);
      }
      break;
    }
    else
    {
      gSiliconEvent->SetRunTime(gAnalysisReport->GetVF48TimeCalibration_sec( gSiliconEvent->GetVF48Timestamp()) * gSiliconEvent->GetVF48Timestamp() + gVF48RunTimeOffset);
  //    cout << "time calibration: "<< gAnalysisReport->GetVF48TimeCalibration_ms( gSiliconEvent->GetVF48Timestamp()) << "\n" ;
      if(VERBOSE) cout << "event time: " <<gSiliconEvent->GetVF48Timestamp()  << "\t1 - time calibration: "<< 1. - gAnalysisReport->GetVF48TimeCalibration_sec( gSiliconEvent->GetVF48Timestamp()) << "\n" ;
      gSiliconTree->Fill();
      gSiliconEvent->Delete();
      LastWrittenSilArrayEntry=i;
    }
  }


  for (Int_t i=LastWrittenSilArrayEntry; i>=0; i--)
  {
    gSiliconEvents->RemoveAt(i);
  }
  gSiliconEvents->Compress();

  gEventIDs.clear();
  Int_t CarriedEntries=gSiliconEvents->GetEntriesFast();
  if (CarriedEntries>0)
  std::cout<<"Carrying " << CarriedEntries<< " Silicon Events to next flush" << std::endl;
  for (Int_t i=0; i<CarriedEntries; i++)
  {
    gEventIDs.push_back(tempEventIDs.at(i));
  }
  gEventID = tmpid;
//  gSiliconEvent = tmpsil;
}


// ======= Fill TAlphaEvent tree =========
void FlushAlphaEvent()
{
  if (gWriteAlphaEventTree)
  {
    gAlphaEventTree->Fill();
    gAlphaEvent->DeleteEvent();
    //  if(gAlphaEvent)
    //    delete gAlphaEvent;
    //  gAlphaEvent = NULL;
  }
}


int gNext = 1;
extern int gNext;

int gBadVF48Events = 0;
int gGoodVF48Events = 0;
int gRecEvents = 0;
int gPassEvents = 0;

void HandleVF48event(const VF48event* e) {
  ShowMem("HandleVF48::start");
  if(VERBOSE) printf("VF48Event %d \n",(int)e->eventNo);
  if (0) {
    delete e;
    return;
  }
  // search for events within selected range
  if(gUseTimeRange){
	  if(e->timestamp<starttime) {
		  delete e;
		  return;
	  }
  }
  if(gUseTimeRange){
	  if(e->timestamp>stoptime) {
		  delete e;
		  return;
	  }
  }

  if(gSiEventNum){
    if((int)e->eventNo < gSiEventNum ) {
      delete e;
      return;
    }
  }
  if(gSiEventLast){
    if((int)e->eventNo > gSiEventLast ){
      delete e;
      return;
    }
  }

 

  if (gPolyFit) {
	  pol_tree->GetEntry(e->eventNo-1);
      if (polVF48EventNumber!=e->eventNo) {
      Int_t diff=e->eventNo - polVF48EventNumber;
      std::cout <<"Diff:"<<diff<<std::endl;
        pol_tree->GetEntry(e->eventNo + diff );
	}
        
}
  if (0) {
    printf("Last event headers:  ");
    for (int i=0; i<NUM_VF48_MODULES; i++)
      printf(" 0x%08x", vfu->wLastEventHeader[i][0]);
    printf("\n");
    printf("Last event trailers: ");
    for (int i=0; i<NUM_VF48_MODULES; i++)
      printf(" 0x%08x", vfu->wLastEventTrailer[i][0]);
    printf("\n");
  }

  // Check for VF48 errors
  int trigs = 0;
  for( int imod = 0; imod < NUM_VF48_MODULES; imod++) {
    VF48module* the_Module = e->modules[imod];

    // All modules should be present
    // there is probably a problem with the event
    // <<<< -----
    if(  !the_Module ) {
      printf("Event %d: Error VF48 module %d not present\n", (int)e->eventNo, imod);
      if(VERBOSE)
      e->PrintSummary();
      gBadVF48Events++;
      delete e;
      return;
    }

    if( !the_Module ) continue;


    if( the_Module->error != 0 ) {
      printf("Event %d: Found VF48 error, not using event\n", (int)e->eventNo);
      if(VERBOSE) e->PrintSummary();
      gBadVF48Events++;
      delete e;
      return;
    }

    if ( the_Module->trigger == 0 )
      trigs ++;

    if (trigs >= 1) {
      printf("Event %d: Found VF48 trig error, not using event\n", (int)e->eventNo);
      gBadVF48Events++;
      delete e;
      return;
    }

  }
  gGoodVF48Events++;
  // end checking for VF48 errors

  if(VERBOSE)  {
    printf("=======================================================================================\n");
    printf("--> "); e->PrintSummary();
    printf("=======================================================================================\n");


  }


  // handles advancement requests from GUI
  if (!gNext) {
    delete e;
    return;
  }
  gNext=1;  // force next event

  // == Process addons related to VF48Event objects
  for (unsigned int i=0; i<gAddons.size(); i++){
    (gAddons.at(i))->Process(e);
  }

  if (gEnableSuppression) {
    suppEvents++;

    for (int i=0; i<NUM_VF48_MODULES; i++) {

      for (int j=0; j<6; j++) {
	for (int k=0; k<8; k++) {
	  int suppressed = (e->modules[i]->channels[j*8+k].numSamples == 0);

	  totalTotal++;
	  totalMod[i]++;
	  totalFE[i][j]++;
	  totalChan[i][j][k]++;

	  if (suppressed) {
	    suppTotal++;
	    suppMod[i]++; // 8 vf48 modules
	    suppFE[i][j]++; // 8*6
	    suppChan[i][j][k]++; // 8*6*8
	  }
	}
      }
    }
  }

  // Construct TSiliconEvent ===========================================================

  TSiliconEvent* SiliconEvent = new TSiliconEvent();

  gEventID = e->eventNo;
  gEventIDs.push_back(gEventID);

  SiliconEvent->SetVF48NEvent( e->eventNo );                    // vf48 event number
  SiliconEvent->SetVF48NTrigger( e->modules[1]->trigger );      // vf48 trigger number
  SiliconEvent->SetVF48Timestamp( e->timestamp );               // vf48 timestamp
  SiliconEvent->SetRunNumber( outRunNumber );                     // runnumber
  SiliconEvent->SetCounters( gSisCounter, gLabVIEWCounter );    // counter links

  // init sub-sampling settings
  int soffset(-1);
  double subsample(-1);
  int offset(-1);

  // VF48 - SiModule mapping variables
  int SiModNumber = -1;  // 0 <= SiModNumber < NUM_SI_MODULES
  int ASIC = -1;         // 1 <= ASIC <= 4
  int FRCNumber = -1;    //
  int FRCPort = -1;      //
  int TTCChannel = -1;

  TSiliconModule* SiliconModule = NULL;
  TSiliconVA* SiliconVA = NULL;
  TSiliconStrip* SiliconStrip = NULL;

  Double_t NSideRawHits=0;
  Double_t PSideRawHits=0;

  for( int vf48modnum=0; vf48modnum<NUM_VF48_MODULES; vf48modnum++ ) {

    // Get the VF48 module
    VF48module* the_Module = e->modules[vf48modnum];
    if( !the_Module ) continue;

    // identify number of samples/strip runs
    //VF48channel channel = the_Module->channels[0];

    subsample = gSettingsSubsample[vf48modnum];
    offset = gSettingsOffset[vf48modnum];
    soffset = gSettingsSoffset[vf48modnum];

    if (VERBOSE>1) printf("HandleVF48Event: SQLLITE: Module %d \t subsample = %f \t offset = %d \t soffset = %d \t samples = %d \n", vf48modnum, subsample, offset, soffset, gSettingsDB->GetVF48Samples( gRunNumber, vf48modnum) );


    int vf48group=-1;
    for( int vf48chan=0; vf48chan<48; vf48chan++ ){
      if( vf48chan%16==0 ) vf48group++;

      gVF48SiMap->GetSil( vf48modnum, vf48chan, SiModNumber, ASIC, FRCNumber, FRCPort, TTCChannel );

      if( SiModNumber < 0 ) continue;

      if( vf48chan%4 == 0 ) {
        SiliconModule = new TSiliconModule( SiModNumber, vf48modnum, vf48group, FRCNumber, FRCPort );
      }

      // Get the VF48 channel
      if( vf48chan >= MAX_CHANNELS ) { printf("Exceeded MAX_CHANNELS\n"); exit(1); }
      VF48channel the_Channel = the_Module->channels[vf48chan];

      SiliconVA = new TSiliconVA( ASIC, vf48chan );
      if(vf48chan%4==2 || vf48chan%4==3) SiliconVA->SetPSide( true );


      if (the_Channel.numSamples > 0) {
        // N.B.: if channel is suppressed numSamples = 0

        int s = soffset;
        for( int k=0; k<128; k++) { // loop over the strips
          if( s >= the_Channel.numSamples ) continue;

          Int_t stripNumber = k + 128*(ASIC-1) + 512*(SiModNumber);
          Double_t stripMean= StripMeans[stripNumber];
          if (!gPolyFit)
             SiliconStrip = new TSiliconStrip( k, the_Channel.samples[s+offset] - TMath::Nint(stripMean));
          else
            SiliconStrip = new TSiliconStrip( k, the_Channel.samples[s+offset]);

          SiliconVA->AddStrip( SiliconStrip );

          s += (int)subsample;
        }
      }
      else {
        for( int k=0; k<128; k++) {
          SiliconStrip = new TSiliconStrip( k, 0);
          SiliconVA->AddStrip( SiliconStrip );
        }

      }


      if( SiliconVA->NoStrips() ) {
        delete SiliconVA;
        SiliconVA = NULL;
        continue;
      }

      if (!gPolyFit)
      {
        // Calculate the ASIC strip mean/rms
        SiliconVA->CalcRawADCMeanSigma();

        // Calculate the filtered ASIC strip mean by removing hit-like strips
        SiliconVA->CalcFilteredADCMean();

        // Subtract the mean (filted) ASIC strip value from each strip (pedestal subtraction)
        SiliconVA->CalcPedSubADCs_NoFit();
      }
      else
      {
		 //std::cout <<"SiModNumber:"<<SiModNumber<<"\t"<<ASIC<<std::endl;
		 //std::cout <<"alphaA VF48 number:"<<e->eventNo<<std::endl;
		 //std::cout <<"Pol VF48 number"<<polVF48EventNumber<<std::endl;
        SiliconVA->SetPol2Fit(pol0[4*(SiModNumber)+(ASIC-1)],
                              pol1[4*(SiModNumber)+(ASIC-1)],
                              pol2[4*(SiModNumber)+(ASIC-1)],
                              chi[4*(SiModNumber)+(ASIC-1)]);
        SiliconVA->CalcPedSubADCs();
      }
      if(vf48chan%4==2 || vf48chan%4==3)
      {
        Double_t thesigma(pVASigma);
        PSideRawHits+=SiliconVA->CalcHits( thesigma, StripRMSs, SiModNumber );
      }
      else
      {
        Double_t thesigma(nVASigma);
        NSideRawHits+=SiliconVA->CalcHits( thesigma, StripRMSs, SiModNumber );
     }
      //SiliconVA->SuppressNoiseyStrips();

      SiliconModule->AddASIC( SiliconVA );

      // when reached the last channel (asic) in the module, add silicon module
      if( vf48chan%4 == 3 ) SiliconEvent->AddSiliconModule( SiliconModule );


    }//loop over VF48 channels


  } // loop over VF48 modules

  // == End construction of Silicon Event



  // == Process addons related to TSiliconEvent objects
  for (unsigned int i=0; i<gAddons.size(); i++){
    (gAddons.at(i))->Process(SiliconEvent);
    //    (gAddons.at(i))->Process(e);
  }






  // == Compression of Silicon Event

  SiliconEvent->CompressSiliconModules();

  SiliconEvent->SetPsideNRawHits( PSideRawHits );
  SiliconEvent->SetNsideNRawHits( NSideRawHits );
  // SiliconEvent->SetNRawHits( SiliconEvent->CalcNRawHits() );
  // == End Compression of Silicon Event



  // Reconstruction routines ======================================================

  if( 1 ) {
    AlphaEvent->DeleteEvent();
    if( AlphaEvent ) {
      int m, c, ttcchannel;
      for( int isil = 0; isil < NUM_SI_MODULES; isil++ ) {
        TSiliconModule * module = SiliconEvent->GetSiliconModule( isil );
        if( !module ) continue;

        gVF48SiMap->GetVF48( isil,1, m, c, ttcchannel); // check that the mapping exists
        if( m == -1 ) continue; // if not, continue

        Char_t * name = (Char_t*)gVF48SiMap->GetSilName(isil).c_str();
        TAlphaEventSil *sil = new TAlphaEventSil(name);
#ifdef USE_ALPHASI
        sil->Setdx(0.);
        sil->Setdy(0.);
        sil->Setdz(0.);
        sil->Setdphi(0.);
        sil->Setdtheta(0.);
        sil->Setdpsi(0.);
        sil->SetCenter();
#endif

        AlphaEvent->AddSil(sil);
        for( int iASIC = 1; iASIC <= 4; iASIC++ ) {
          gVF48SiMap->GetVF48( isil,iASIC, m, c, ttcchannel);
          TSiliconVA * asic = module->GetASIC( iASIC );
          if( !asic ) continue;

          TObjArray * strip_array = asic->GetStrips();
          Int_t nASIC  = asic->GetASICNumber();

          for(int s = 0; s<strip_array->GetEntriesFast(); s++) {
            TSiliconStrip * strip = (TSiliconStrip*) strip_array->At(s);
            Int_t ss = strip->GetStripNumber();
            double* theRMS=NULL;
            double* theASIC=NULL;
            if( nASIC == 1 ) {
              theASIC = sil->GetASIC1();
              theRMS= sil->GetRMS1();
            }
            else if( nASIC == 2) {
              theASIC = sil->GetASIC2();
              theRMS= sil->GetRMS2();
            }
            else if( nASIC == 3) {
              theASIC = sil->GetASIC3();
              theRMS= sil->GetRMS3();
            }
            else if( nASIC == 4) {
              theASIC = sil->GetASIC4();
              theRMS= sil->GetRMS4();
            }

            theASIC[ss] = fabs(strip->GetPedSubADC());
            theRMS[ss] = strip->GetStripRMS();
          }
        }
      }

      AlphaEvent->SetNHitsCut(gNHitsCut);
      AlphaEvent->SetNClusterSigma(nClusterSigma);
      AlphaEvent->SetPClusterSigma(pClusterSigma);
      AlphaEvent->SetHitSignificance(hitSigmaCut);
      AlphaEvent->SetHitThreshold(hitThresholdCut);

      if(gIsCosmicRun == false && gDelCosmic == false) // flag to set manually in alphaAnalysis.cxx
        {
          AlphaEvent->RecEvent();
          Int_t Ntracks=AlphaEvent->GetNGoodHelices() ;
          SiliconEvent->SetNTracks( Ntracks );
          Double_t res=AlphaEvent->CosmicTest();
          SiliconEvent->SetResidual( res );
          SiliconEvent->SetDCA( AlphaEvent->GetCosmicTrack()->GetDCA() );
          TVector3 CosmicUnitVec= TVector3(AlphaEvent->GetCosmicTrack()->Getunitvector());
          SiliconEvent->SetCosmicVector( &CosmicUnitVec );
          
#ifdef USE_ALPHASI
          TVector3 * ivtx =  AlphaEvent->GetVertex();
          
#endif

#ifdef USE_ALPHAVMC
          Int_t ivtx = (Int_t)(AlphaEvent->GetVertex()->IsGood());
#endif

          if(ivtx && gSiliconEvent)
            {
              gRecEvents++;
              gVertexCounter++;
            }
        }
      else if (gDelCosmic == true)
        {
          if (VERBOSE) printf("cosmic run\n");
          AlphaEvent->SetCosmic(kTRUE);
          AlphaEvent->RecClusters();
          AlphaEvent->RecHits();
          std::cout<<"a:"<<AlphaEvent->GatherHits()->GetEntriesFast()<<std::endl;
          TAlphaEventCosmicHelix* cos=AlphaEvent->FindHelix();
          if (cos)
          {
            std::cout<<"Res:"<<cos->GetResiduals()<<"\tChi:"<<cos->GetChi2()<<" Dist:"<<cos->DistanceToFurthestHit()<<std::endl;
            if (cos->GetResiduals()>200 )
            //if (cos->DistanceToFurthestHit()<1)
            {
              AlphaEvent->AddHelix( cos );
              for (Int_t i=0; i<cos->GetNHits(); i++)
              { 
                TAlphaEventHit *a=cos->GetHit(i);
                TAlphaEventSil* sil=AlphaEvent->GetSilByNumber(a->GetSilNum());
                sil->RemoveHit(a);
              }
            }
          }

          AlphaEvent->SetCosmic(kFALSE);
          AlphaEvent->RecTrackCandidates();
          AlphaEvent->PruneTracks();
          AlphaEvent->RecVertex();
          AlphaEvent->RecRPhi();
          std::cout<<"c:"<<AlphaEvent->GatherHits()->GetEntriesFast()<<std::endl;
          
        }

      else if(gIsCosmicRun == true)
        {
          if (VERBOSE) printf("cosmic run\n");
          AlphaEvent->SetCosmic(kTRUE);
          AlphaEvent->RecClusters();
          AlphaEvent->RecHits();
          if(AlphaEvent->FindCosmic(6)) gRecEvents++ ;
        }

      for( int isil = 0; isil < NUM_SI_MODULES; isil++ ){
        char silname[5]; // <<<<< ---- limits the num of char used for name of the silicon
        sprintf(silname,"%s",gVF48SiMap->GetSilName(isil).c_str());
        TAlphaEventSil * sil = (TAlphaEventSil*)AlphaEvent->GetSilByName( silname );
        if(!sil) continue;
        SiliconEvent->SetNHits( SiliconEvent->GetNHits() + sil->GetNHits() );
      }
    }
  }//fi (gRecOn)

  // Add vertex to the SiliconEvent ==================================================
#ifdef USE_ALPHASI
  if( AlphaEvent->GetVertex() ) {
    TVector3 * vertex = AlphaEvent->GetVertex();

    SiliconEvent->SetVertex( vertex );
    SiliconEvent->SetNVertices( 1 );
  }
#endif


#ifdef USE_ALPHAVMC
if(gIsCosmicRun == false)
{

  Int_t vertType = 0;
  TAlphaEventVertex * vertex = (TAlphaEventVertex*)AlphaEvent->GetVertex();
  if( vertex->IsGood() )
  {
    TVector3* v = new TVector3();
    v->SetXYZ( vertex->X(), vertex->Y(), vertex->Z() );
    SiliconEvent->SetVertex( v );
    SiliconEvent->SetNVertices( 1 );
    Int_t Ntracks=AlphaEvent->GetNGoodHelices();
    Double_t rad=v->Perp();
    Double_t res=AlphaEvent->CosmicTest();
    if(Ntracks==2)
      {
        if(res<REScuteq2 || rad>RADcuteq2) {}
        else {gPassEvents++;}
      }
    else if(Ntracks>2)
      {
        if(res<REScutgr2 || rad>RADcutgr2) {}
        else {gPassEvents++;}
      }
    vertType += (1 << 0);
    SiliconEvent->SetVertexType(vertType);
    
    
    delete v;
  }

  TProjClusterBase * ProjVert = (TProjClusterBase*)AlphaEvent->GetProjClusterVertex();
  if(ProjVert)
  {
      SiliconEvent->SetNVertices(1);
      TVector3* pv = new TVector3();
      Double_t phi = ProjVert->RPhi()/TrapRadius;
      Double_t xx = TrapRadius*(TMath::Cos(phi));
      Double_t yy = TrapRadius*(TMath::Sin(phi));

//       Int_t nh = 0;
//       for(Int_t it = 0; it<AlphaEvent->GetNHelices(); it++)
// 	{
// 	  TAlphaEventHelix * h = (TAlphaEventHelix*)AlphaEvent->GetHelix(it);
// 	  if(h->GetHelixStatus()>0) nh++;
// 	}
//       SiliconEvent->SetNTracks( nh );

      pv->SetXYZ(xx,yy,ProjVert->Z());
      SiliconEvent->SetProjVertex( pv );
      vertType += (1 << 1);
      SiliconEvent->SetVertexType(vertType);
//      SiliconEvent->SetResidual(AlphaEvent->CosmicTest());
      delete pv;
  }

}
 #endif

  // display the event
  if( gDisplayEvent ) {
    char text[256];
    sprintf(text,"Run %d, Event %d, Trigger %d, VF48 Time %lf",
            gRunNumber,e->eventNo,e->modules[1]->trigger,e->timestamp);


    TAlphaDisplay * display = new TAlphaDisplay(&AlphaEvent,text, gAutoSaveEventDisplay, gRunNumber, e->eventNo);

    extern TApplication* xapp;
    //    printf("TApplication object: %p\n", xapp);

    if(gTerminateEventDisplay == 1)
      xapp->Terminate();

    if(gTerminateEventDisplay == 0)
      xapp->Run(kTRUE);

    //    printf("Done\n");
    delete display;


  }

  // == Process addons related to TAlphaEvent objects
  for (unsigned int i=0; i<gAddons.size(); i++) (gAddons.at(i))->Process(AlphaEvent);

  // Finally add SiliconEvent to the container of SiliconEvents
  gSiliconEvents->Add( SiliconEvent );


  // Flush SiliconEvents===========================================================

  TSiliconEvent* lastSiliconEvent;
  //TTCEvent* lastTTCEvent;
  int adc_triggers_required_for_flush(999999);

  if (VERBOSE)
  printf("gADCTriggerEvents size = %d  gSiliconEvents buffer size = %d \n", gADCTriggerEvents->GetEntriesFast(), gSiliconEvents->GetEntriesFast() );

  // these variables are modified by the flushsiliconevent routine

  //Bool_t gexitflag = kFALSE;
  Int_t SilEvents=gSiliconEvents->GetEntriesFast();
  if( SilEvents > 1000)  {
    g_flag_siliconEvents_ready_to_flush = kTRUE;
    lastSiliconEvent = (TSiliconEvent*) gSiliconEvents->At( gSiliconEvents->GetLast() );
    //lastTTCEvent = (TTCEvent*) gTTCEvents->At( gTTCEvents->GetLast() );
    adc_triggers_required_for_flush = lastSiliconEvent->GetVF48NTrigger();
  }
//gTTCEvents->GetLast()>=gSiliconEvents->GetLast();
  Int_t ADCEntries=gADCTriggerEvents->GetEntriesFast();
  if( (g_flag_siliconEvents_ready_to_flush && ADCEntries >= adc_triggers_required_for_flush) || //IF threshold exceeded and ready to dump
  //Bringing limit down to 2000 (to target less than 1Gb memory usage in analysis
  (SilEvents >=  2000)) //OR if 2000 silicon events, dump what you can (memory saver for long cosmic runs)
  {
    printf("Calling  FlushSiliconEvents : gADCTriggerEvents size = %d  gSiliconEvents size = %d \n", ADCEntries, SilEvents );
    FlushSiliconEvents();
    g_flag_siliconEvents_ready_to_flush = kFALSE;
    //gexitflag=kTRUE;
  }

  gAlphaEvent = AlphaEvent;
  FlushAlphaEvent();

  delete e;
}


void VF48EndRun()
{
  while (1)
    {
      VF48event* e = vfu->GetEvent(true);
      if (!e) break;
      HandleVF48event(e);
    }
  if (gGoodVF48Events>0) FlushSiliconEvents(kTRUE);

  if( AlphaEvent ) {
    AlphaEvent->DeleteEvent();
    delete AlphaEvent;
    AlphaEvent = NULL;
  }

  //Prints the date and the alphaStrip used for the analysis, same loop as in "VF48BeginRun()": - prints moved to TAnalysisReport::Print()
  //std::cout << "\n\n\n=================================================================================" << std::endl;


  if (1)
    {
      Int_t VF48badDataCount=vfu->fBadDataCount;

      gAnalysisReport->SetVF48Counts(gGoodVF48Events, gBadVF48Events, VF48badDataCount, gRecEvents);
      gAnalysisReport->SetPassedCutEvents(gPassEvents);
      //printf("VF48EndRun: Good VF48 events: %d, bad: %d, Total: %d, data errors: %d, Reconstructed: %d, Eff: %lf\n",gGoodVF48Events,gBadVF48Events,gGoodVF48Events+gBadVF48Events, VF48badDataCount, gRecEvents,(double)gRecEvents/(double(gGoodVF48Events+gBadVF48Events)));
    }
  delete vfu;
  
  //Check time alignment at beginning of run
  Int_t StartSize=100;
  
  TSiliconEvent* SilEvent=new TSiliconEvent();
  gSiliconTree->SetBranchAddress("SiliconEvent",&SilEvent);
  gSiliconTree->SetBranchStatus("SiliconModules",0);
  if (gADCTriggerEvents->GetEntries()> StartSize)
  {
    Double_t SIStime[StartSize];
    Double_t VF48time[StartSize];
    for (Int_t i=0; i<StartSize; i++)
    {
      TADCTriggerEvent * ADCTrig = (TADCTriggerEvent*) gADCTriggerEvents->At( i );
      //TSiliconEvent* SilEvent = (TSiliconEvent*) 
      gSiliconTree->GetEntry(i);
      SIStime[i]= ADCTrig->GetRunTime();
      VF48time[i]=SilEvent->GetRunTime();
    }
    gAnalysisReport->SetStartTimeMatching(SIStime,VF48time);
  }
  TimeStampHelper->ReserveVFEvents(gSiliconTree->GetEntries());
  Int_t FlushCounter=0;
  for (Int_t i=0; i<gSiliconTree->GetEntries(); i++)
  {
    gSiliconTree->GetEntry(i);
    TimeStampHelper->AddVF48Timestamp(SilEvent->GetRunTime());
    FlushCounter++;
    if (FlushCounter>1000000)  //Write to file every 1M entries?
    {
      gTimestampHelperTree->Fill();
      TimeStampHelper->ClearVectors();
      FlushCounter=0;
    }  
  }
  delete SilEvent;
  gAnalysisReport->SetrungVF48offset(gVF48RunTimeOffset);


  for (unsigned int i=0; i<gAddons.size(); i++){
    (gAddons.at(i))->End();
  }

  if (gEnableSuppression) // Print Threshold Data CSV files (temp.csv is for basic information, temp2.csv for more per module data)
    {
//const char FName;
//FName="temp.csv";
//printf("1\n");
      std::ofstream ThresholdFile;
//stringstream runno;
//runno<<(gRunNumber);
printf("THRESHOLD_INNIT %d \n",thres);
char FileName[100];
strcpy(FileName,"ZS_R");
char StringX[5];
sprintf(StringX,"%d",gRunNumber);
strcat(FileName,StringX);
strcat(FileName,"_Thres");
char StringY[3];
sprintf(StringY,"%d",thres);
strcat(FileName,StringY);
ThresholdFile.open(strcat( FileName,".csv"));
//ThresholdFile.open(sprintf("ThresholdScan%d.csv",StringX , "Threshold.csv"));
      printf("Total Events: %d Total Suppressed: %d is %5.1f%% \n",suppEvents,   suppTotal, 100.0*suppTotal/(double)totalTotal);
      printf("Module Number,   Suppressed\n");
      for (int i=0; i<NUM_VF48_MODULES; i++) // Print suppression for modules
	{
	  printf("%d \t %5.1f%% \n" ,i, 100.0*suppMod[i]/(double)totalMod[i]); // 8 vf48 modules
	}
      printf("===============================================\n");
     ThresholdFile << "FE FPGA \n 1,2,3,4,5,6,TotalSupressed\n";
     ThresholdFile.setf(std::ios::fixed, std::ios::floatfield);
     for (int i=0; i<NUM_VF48_MODULES; i++)
       {
       for (int j=0; j<6; j++)
	 {
	   printf("  %5.1f%%  ", 100.0*suppFE[i][j]/(double)totalFE[i][j]);
	   ThresholdFile << suppFE[i][j]<< ",";
	 }
       printf("\n");
       ThresholdFile << suppMod[i] << "\n";
     }
     ThresholdFile << "Total Events:"<<suppEvents<< "Total Suppressed:"<<suppEvents<<suppTotal<<"\n";

     std::ofstream ThresholdDetail; // Write a more detailed file
char DetailName[100];
strcpy(DetailName,"ZS_R");
sprintf(StringX,"%d",gRunNumber);
strcat(DetailName,StringX);
strcat(DetailName,"_Thres");
sprintf(StringY,"%d",thres);
strcat(DetailName,StringY);
ThresholdDetail.open(strcat( DetailName,"detail.csv"));
ThresholdDetail.setf(std::ios::fixed, std::ios::floatfield);


     for (int j=0; j<6; j++)
       {
       for (int k=0; k<8; k++)
	 {
	   ThresholdDetail << "FE1 Addr " << j<<"sub "<< k<<",";
	 }
       }
     ThresholdDetail << "\n";
     for (int i=0; i<NUM_VF48_MODULES; i++)
       {
       for (int j=0; j<6; j++)
	 {
	 for (int k=0; k<8; k++)
	   {
	     ThresholdDetail<< suppChan[i][j][k]<<","; //8*6*8
	   }
	 }
       ThresholdDetail << "\n";
       }
    }




}

// end
