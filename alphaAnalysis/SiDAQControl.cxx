#include "SiDAQControl.h"
#include "./lib/include/TVF48SiMap.h"

extern TVF48SiMap * gVF48SiMap;
extern int    gSettingsSamples[NUM_VF48_MODULES];

#define VA_RANGE_MIN 0
#define VA_RANGE_MAX 1100

#define ADC_RANGE_MIN -600
#define ADC_RANGE_MAX 600



SiDAQControl::SiDAQControl(){
  SiDAQControl("SiDAQControl","Si DAQ Control");
}

SiDAQControl::SiDAQControl(const char* name, const char* description) // ctor
  : TGMainFrame(gClient->GetRoot(),500,715,kMainFrame | kVerticalFrame), 
    TAbsUtil(name,description)
{
   SetWindowName("SiDAQControl");  
   SetName("SiDAQControl");
   SetLayoutBroken(kTRUE);

   /////////////////////////////////////////////////////
   //  Create display canvas 
   /////////////////////////////////////////////////////
   
   // embedded canvas 1
   TRootEmbeddedCanvas *fRootEmbeddedCanvas2186 = new TRootEmbeddedCanvas(0,this,320,200);
   fRootEmbeddedCanvas2186->SetName("fRootEmbeddedCanvas2186");
   Int_t wfRootEmbeddedCanvas2186 = fRootEmbeddedCanvas2186->GetCanvasWindowId();
   sampleCanvas = new TCanvas("sampleCanvas", 10, 10, wfRootEmbeddedCanvas2186);
   fRootEmbeddedCanvas2186->AdoptCanvas(sampleCanvas);
   this->AddFrame(fRootEmbeddedCanvas2186, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
   fRootEmbeddedCanvas2186->MoveResize(10,55,480,300);


   // embedded canvas 2
   TRootEmbeddedCanvas *fRootEmbeddedCanvas2202 = new TRootEmbeddedCanvas(0,this,320,200);
   fRootEmbeddedCanvas2202->SetName("fRootEmbeddedCanvas2202");
   Int_t wfRootEmbeddedCanvas2202 = fRootEmbeddedCanvas2202->GetCanvasWindowId();
   stripCanvas = new TCanvas("stripCanvas", 10, 10, wfRootEmbeddedCanvas2202);
   fRootEmbeddedCanvas2202->AdoptCanvas(stripCanvas);
   this->AddFrame(fRootEmbeddedCanvas2202, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
   fRootEmbeddedCanvas2202->MoveResize(10,405,480,300);




   TGLabel *labelVF48Mod = new TGLabel(this,"VF48 module");
   labelVF48Mod->SetTextJustify(36);
   labelVF48Mod->SetMargins(0,0,0,0);
   labelVF48Mod->SetWrapLength(-1);
   this->AddFrame(labelVF48Mod, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
   labelVF48Mod->MoveResize(10,5,100,18);

   TGLabel *labelVF48Chan = new TGLabel(this,"VF48 channel");
   labelVF48Chan->SetTextJustify(36);
   labelVF48Chan->SetMargins(0,0,0,0);
   labelVF48Chan->SetWrapLength(-1);
   this->AddFrame(labelVF48Chan, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
   labelVF48Chan->MoveResize(145,5,100,18);


   numberVF48Mod = new TGNumberEntry(this, (Double_t) 0,16,-1,(TGNumberFormat::EStyle) TGNumberFormat::kNESInteger, 
                                     (TGNumberFormat::EAttribute)TGNumberFormat::kNEANonNegative, (TGNumberFormat::ELimit) TGNumberFormat::kNELLimitMinMax,0,NUM_VF48_MODULES-1);
                                     
   numberVF48Mod->SetName("numberVF48Mod");
   this->AddFrame(numberVF48Mod, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
   numberVF48Mod->MoveResize(10,25,128,22);

   numberVF48Chan = new TGNumberEntry(this, (Double_t) 0,16,-1,(TGNumberFormat::EStyle) TGNumberFormat::kNESInteger, 
                                      (TGNumberFormat::EAttribute)TGNumberFormat::kNEANonNegative, (TGNumberFormat::ELimit) TGNumberFormat::kNELLimitMinMax,0,MAX_CHANNELS-1);
   numberVF48Chan->SetName("numberVF48Chan");
   this->AddFrame(numberVF48Chan, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
   numberVF48Chan->MoveResize(145,25,128,22);


   TGLabel *labelSiMod = new TGLabel(this,"Si module");
   labelSiMod->SetTextJustify(36);
   labelSiMod->SetMargins(0,0,0,0);
   labelSiMod->SetWrapLength(-1);
   this->AddFrame(labelSiMod, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
   labelSiMod->MoveResize(10,355,100,18);

   TGLabel *labelASIC = new TGLabel(this,"ASIC");
   labelASIC->SetTextJustify(36);
   labelASIC->SetMargins(0,0,0,0);
   labelASIC->SetWrapLength(-1);
   this->AddFrame(labelASIC, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
   labelASIC->MoveResize(145,355,100,18);

   numberSiMod = new TGNumberEntry(this, (Double_t) 0,16,-1,(TGNumberFormat::EStyle) TGNumberFormat::kNESInteger, 
                                   (TGNumberFormat::EAttribute)TGNumberFormat::kNEANonNegative, (TGNumberFormat::ELimit) TGNumberFormat::kNELLimitMinMax,0,NUM_SI_MODULES-1);
   numberSiMod->SetName("numberSiMod");
   this->AddFrame(numberSiMod, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
   numberSiMod->MoveResize(10,375,128,22);
   
   numberASIC = new TGNumberEntry(this, (Double_t) 1,16,-1,(TGNumberFormat::EStyle) TGNumberFormat::kNESInteger, 
                                  (TGNumberFormat::EAttribute)TGNumberFormat::kNEANonNegative, (TGNumberFormat::ELimit) TGNumberFormat::kNELLimitMinMax,1,4);
   numberASIC->SetName("numberASIC");
   this->AddFrame(numberASIC, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
   numberASIC->MoveResize(145,375,128,22);




   TGTextButton *buttonPlotSamples = new TGTextButton(this,"Plot Samples",10020);
   buttonPlotSamples->SetTextJustify(36);
   buttonPlotSamples->SetMargins(0,0,0,0);
   buttonPlotSamples->SetWrapLength(-1);
   buttonPlotSamples->Resize(50,40);
   this->AddFrame(buttonPlotSamples, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
   buttonPlotSamples->MoveResize(280,10,100,40);

   TGTextButton *buttonPlotStrips = new TGTextButton(this,"Plot Strips",10010);
   buttonPlotStrips->SetTextJustify(36);
   buttonPlotStrips->SetMargins(0,0,0,0);
   buttonPlotStrips->SetWrapLength(-1);
   buttonPlotStrips->Resize(50,40);
   this->AddFrame(buttonPlotStrips, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
   buttonPlotStrips->MoveResize(280,360,100,40);


   MapSubwindows(); 
   MapWindow();
   //   Resize(490,372);
}



Bool_t SiDAQControl::ProcessMessage(Long_t msg, Long_t parm1, Long_t parm2)
{
  // printf("GUI Message %d %d %d\n",(int)msg,(int)parm1,(int)parm2);
  // printf("... %d \n", fNumberEntry515->GetIntNumber() );
  
  int imod, ichan, isil, iva, frcno, frcpt, ttcchannel;

  if ( (int)msg == 259 && (int) parm1 == 10010 ){ // 'plot strips' button

    isil =  numberSiMod->GetIntNumber();
    iva =  numberASIC->GetIntNumber();

    PlotStrips(isil,iva);

    gVF48SiMap->GetVF48( isil,iva, imod, ichan, ttcchannel); 

    numberVF48Mod->SetIntNumber(imod);
    numberVF48Chan->SetIntNumber(ichan);
    PlotSamples(imod,ichan);
  }
  if ( (int)msg == 259 && (int) parm1 == 10020 ){ // 'plot samples' button
    imod =  numberVF48Mod->GetIntNumber(); 
    ichan =  numberVF48Chan->GetIntNumber(); 
    
    PlotSamples(imod,ichan);

    gVF48SiMap->GetSil( imod, ichan, isil, iva, frcno, frcpt, ttcchannel );
  
    if (isil<0 || iva<1){
      Msg(TString::Format("Couldn't find Si module: %d %d",isil,iva));
      stripCanvas->Clear();
      stripCanvas->Modified();
      stripCanvas->Update();  
    }
    else{
      numberSiMod->SetIntNumber(isil);
      numberASIC->SetIntNumber(iva);
      PlotStrips(isil,iva);
    }
  }
  return kTRUE;
}      

void SiDAQControl::Begin(){
  
  Msg("Begin");

  sub_dir = gUtils->Get_Directory( "SiQC", gOutputFile );
        
  char name[256];
  char title[256];

  for( int imod = 0; imod < NUM_VF48_MODULES; imod++ ) {
    int numSamples = gSettingsSamples[imod];
    for (int ich = 0; ich < MAX_CHANNELS; ich++) {
      sprintf( name, "VF48Module%d_Channel%03d_Samples", imod, ich );
      sprintf( title, "VF48 Module #%d, Channel #%3d : Samples", imod, ich );
      sample_hist[imod*MAX_CHANNELS+ich] = gUtils->Get_TH1D( sub_dir, name, title, numSamples, 0, numSamples);
    }
  }
   
  for( int isil = 0; isil < NUM_SI_MODULES; isil++ ) {
    for( int iva = 1; iva<=4; iva++ ){
      sprintf( name, "Silicon%02d_ASIC%d_Strips", isil, iva );
      sprintf( title, "Silicon #%d ASIC# %d : Strips", isil, iva );
      strip_hist[4*isil + (iva-1)] = gUtils->Get_TH1D( sub_dir, name, title, 128, 0, 128); 
    }
  }  
  



}

void SiDAQControl::Process(TSiliconEvent* sil){

  for( int isil = 0; isil < NUM_SI_MODULES; isil++ ) {
    TSiliconModule * module = sil->GetSiliconModule( isil );
    if( !module ) continue;
    
    for( int iva = 1; iva<=4; iva++ ) {
      TSiliconVA * asic = module->GetASIC( iva );
      if( !asic ) continue;
      
      TObjArray * strip_array = asic->GetStrips();
      Int_t nASIC  = asic->GetASICNumber() - 1;
      
      TH1D* h = strip_hist[isil*4 + nASIC];
      h->Reset();

      for(int s = 0; s<strip_array->GetEntries(); s++) {
        TSiliconStrip * strip = (TSiliconStrip*) strip_array->At(s);
        if(!strip) continue;
        
        Double_t pedsubadc = strip->GetPedSubADC();
        h->SetBinContent(h->FindBin(s),pedsubadc );
        
      }// loop over strips
    }// loop over asics
  }//loop over sil modules
  
  ProcessMessage(259, 10020,0); // refresh by emulating plot samples button (using "samples" is intentional)
}

void SiDAQControl::Process(const VF48event* e){
  
  for( int imod = 0; imod < NUM_VF48_MODULES; imod++ ) {
    if (!e->modules[imod]){
      continue;
    }
  
    for (int ich = 0; ich < MAX_CHANNELS; ich++) {
      VF48channel the_Channel = e->modules[imod]->channels[ich];

      TH1D* h = sample_hist[imod*MAX_CHANNELS + ich];
      h->Reset();

      int numSamples = the_Channel.numSamples;
      
      // loop over samples
      for( int is=0; is<numSamples; is++ ){
        h->SetBinContent(h->FindBin(is),the_Channel.samples[is] );
      }
    }
  }

  ProcessMessage(259, 10020,0); // refresh by emulating plot samples button
}




TH1D* SiDAQControl::Plot_ADC_Silicon_Strips(int isil, int iva)
{

  TDirectory * dir_adc = gUtils->Get_Directory( "SiQC", gOutputFile );

  char name[256];
  sprintf( name, "Silicon%02d_ASIC%d_Strips", isil, iva );
  TH1D* adc_hist = gUtils->Get_TH1D( dir_adc, name, 128, 0, 128);

  return adc_hist;

}

TH1D* SiDAQControl::Plot_ADC_Channel_Samples(int imod, int ichan)
{

  TDirectory* dir = gUtils->Get_Directory( "SiQC", gOutputFile );

  char name[256];
  sprintf( name, "VF48Module%d_Channel%03d_Samples", imod, ichan );
  TH1D* hist = gUtils->Get_TH1D( dir, name, 1000, 0, 1000 );

  return hist;

}

/*
TH1D* SiDAQControl::PlotZS_ADC_Channel_Samples(int imod, int ichan)
{

  TDirectory* dir = gUtils->Get_Directory( "SiQC", gOutputFile );

  char name[256];
  sprintf( name, "VF48 Module #%d, Channel #%3d : Samples ZeroSuppression", imod, ichan );
  TH1D* hist = gUtils->Get_TH1D( dir, name, 1000, 0, 1000 );

  return hist;

  }*/



void SiDAQControl::PlotSamples(int imod, int ichan)
{

  TH1D* hist;

  sampleCanvas->cd();

  hist = Plot_ADC_Channel_Samples(imod,ichan);
  hist->GetYaxis()->SetRangeUser(VA_RANGE_MIN,VA_RANGE_MAX);
  gStyle->SetOptStat(111111);
  hist->Draw();

  sampleCanvas->Modified();
  sampleCanvas->Update();  


}


void SiDAQControl::PlotStrips(int isil, int iva)
{

  TH1D* hist;

  stripCanvas->cd();

  hist= Plot_ADC_Silicon_Strips(isil,iva);    
  hist->GetYaxis()->SetRangeUser(ADC_RANGE_MIN,ADC_RANGE_MAX);
  gStyle->SetOptStat(111111);
  hist->Draw();
  
  stripCanvas->Modified();
  stripCanvas->Update();

}

void SiDAQControl::CloseWindow()
{
}

SiDAQControl::~SiDAQControl()
{
  //lables
  delete labelVF48Mod;
  delete labelVF48Chan;
  delete labelSiMod;

  // number entries
  delete numberVF48Mod;
  delete numberVF48Chan;
  delete numberSiMod;

} 
