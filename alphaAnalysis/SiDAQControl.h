#ifndef SIDAQCONTROL_H
#define SIDAQCONTROL_H

#include <iostream>
#include <fstream>
#include <vector>

#include "TRootEmbeddedCanvas.h"

#include "TFile.h"
#include "TH2.h"
#include "TF1.h"
#include "TLatex.h"
#include "TText.h"
#include "TBox.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TGCanvas.h"

#include "TAbsUtil.h"

#include <TGFrame.h>
#include <TGNumberEntry.h>
#include <TButton.h>
#include "Gui.h"


#include "UnpackVF48.h"
#include "SiMod.h"

//#include "Globals.h"
#include "./lib/include/TUtilities.h"

extern TFile          *gOutputFile;
extern TUtilities* gUtils;


// SiDAQ Control Class ============================================================
//
// GUI control class for Si DAQ analysis. 
//
// ================================================================================
class SiDAQControl: public TGMainFrame, public TAbsUtil {

private:


  TDirectory* sub_dir;
  TH1D* strip_hist[NUM_SI_MODULES*4];
  TH1D* sample_hist[NUM_VF48_MODULES*VF48_MAX_CHANNELS];

  //lables
  TGLabel *labelVF48Mod;
  TGLabel *labelVF48Chan;
  TGLabel *labelSiMod;
  TGLabel *labelASIC;

  // number entries
  TGNumberEntry *numberVF48Mod;
  TGNumberEntry *numberVF48Chan;
  TGNumberEntry *numberSiMod;
  TGNumberEntry *numberASIC;
  
  // buttons 
  TGTextButton *buttonPlotStrips;
  TGTextButton *buttonPlotSamples;

  TCanvas *stripCanvas;
  TCanvas *sampleCanvas;



public:
  SiDAQControl();
  SiDAQControl(const char* name, const char* description);

  virtual ~SiDAQControl(); 
  virtual void CloseWindow();

  void Begin();

  void virtual Process(TSiliconEvent* sil);
  void virtual Process(const VF48event* e);
  virtual void Process(TAlphaEvent* evt) { } ;
  

  Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t parm2);
  Int_t GetVF48ModNum(){ return numberVF48Mod->GetIntNumber(); }
  Int_t GetVF48ChanNum(){ return numberVF48Chan->GetIntNumber(); }
  Int_t GetSiModNum(){ return numberSiMod->GetIntNumber(); }

  TH1D* Plot_ADC_Silicon_Strips(int, int);
  TH1D* Plot_ADC_Channel_Samples(int, int);
  //  TH1D* PlotZS_ADC_Channel_Samples(int, int);

  void PlotStrips(int isil, int iva);
  void PlotSamples(int imod, int ich);
};

#endif
