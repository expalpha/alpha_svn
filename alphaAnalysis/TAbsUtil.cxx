#include "TAbsUtil.h"

TAbsUtil::TAbsUtil(): fName(), fDescription() { 

};

TAbsUtil::TAbsUtil(const char *name, const char *description) : fName(name), fDescription(description) { 

};


void TAbsUtil::Msg(TString msg){
  
  std::cout << "*** " << fName << " : " << msg.Data() << std::endl;

};


