#ifndef TABSUTIL_H
#define TABSUTIL_H


#include <iostream>
#include <fstream>
#include <vector>

#include "TFile.h"
#include "TH2.h"
#include "TF1.h"
#include "TLatex.h"
#include "TText.h"
#include "TBox.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "./lib/include/TSiliconEvent.h"

#include "include/XmlOdb.h"

#include "UnpackVF48.h"
#include "SiMod.h"

#include "TAlphaEvent.h"

#define MAX_CHANNELS VF48_MAX_CHANNELS
#define NUM_SI_MODULES nSil // defined in alphavmc/include/SiMod.h
#define NUM_VF48_MODULES nVF48 // defined in alphavmc/include/SiMod.h


// =================================================================================================================
// === Abstract Class for building tools to be executed during HandleVF48.cxx without clogging that function ... === 
//
// contact: Simone
//
// =================================================================================================================

class TAbsUtil {

protected:
  TString   fName;            //name 
  TString   fDescription;           //description
  
  void Msg(TString msg);
  VirtualOdb* fOdb;

public:

  TAbsUtil();
  TAbsUtil(const char *name, const char *description);

  virtual ~TAbsUtil() { } 
  
  virtual void Begin(){ };

  virtual void Process(TSiliconEvent* sil)=0;
  virtual void Process(const VF48event* e)=0;
  virtual void Process(TAlphaEvent* evt)=0;

  virtual void End(){ };

  void SetOdb(VirtualOdb* odb){fOdb=odb;};
 
};

#endif


