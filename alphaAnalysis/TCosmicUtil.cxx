//==============================================================================================
// TCosmicUtil.h                                                                               =
//                                                                                             =
// Cosmic Run Analyser                                                                         =
// 											       =
// $ alphaAnalysis --iscosmic --cosmicutil midasdata/run******sub_____.mid.gz                 =
//											       =
//                                                                                             =
// author: AndreaC									       =
//==============================================================================================

#include "TAlphaEventSil.h"
#include "TAlphaEventHit.h"

#include "./lib/include/TUtilities.h"

#include "TCosmicUtil.h"

extern TUtilities*  gUtils;
extern TFile*       gOutputFile;

extern bool gIsCosmicRun;

//-------------------------------------------------------------------------------------------------
// ctor
TCosmicUtil::TCosmicUtil()
{
	TCosmicUtil("TCosmicUtils","Cosmic Run Analyser");
}

//-------------------------------------------------------------------------------------------------
// ctor
TCosmicUtil::TCosmicUtil(const char* name, 
			 const char* description): TAbsUtil(name,description),
						   /*fEvent(0),fCosmic(0),*/
						   hcor(0),hdir(0),fCos2(0),htexp(0),
						   hhybyres(0),hhybzres(0),
						   fpResHyb("TH1D",NUM_SI_MODULES),
						   fnResHyb("TH1D",NUM_SI_MODULES),
						   fResStrip("TH2D",NUM_SI_MODULES),
						   fdir(0)
{
  if(!gIsCosmicRun)
    {
      Msg("Must be a cosmic run: use --iscosmic\n");
      exit(1);
    }
	
  // the Track (i.e. straight line) is defined by its slope and a point
  fSlope = new Double_t [3];
  fPoint = new Double_t [3];

  ftime=ftemp=0.;
	
  fCosmicCounter=0;
  fNoiseCounter=0;
}

//-------------------------------------------------------------------------------------------------
// dtor
TCosmicUtil::~TCosmicUtil()
{
  delete[] fSlope;
  delete[] fPoint;
}

//-------------------------------------------------------------------------------------------------
// create output histograms
void TCosmicUtil::Begin()
{
  Msg("Begin Cosmic Analyser");
  TDirectory* dir = gUtils->Get_Directory( (char*)"Cosmics", gOutputFile );
	
  hcor = gUtils->Get_TH1D(dir, (char*)"HCor", (char*)"Hit Correlation",1000,0.9999,1.);
  hdir = gUtils->Get_TH1D(dir, (char*)"HDir", (char*)"Cosmic Direction;#alpha [rad];events",200,-TMath::PiOver2(),TMath::PiOver2());
  fCos2 = new TF1("fCos2","[0]*pow(cos([1]*x+[2]),2.)",-TMath::Pi(),TMath::Pi());
  fCos2->SetParName(0,"AMP");
  fCos2->SetParameter(0,200.);
  fCos2->SetParName(1,"PUL");
  fCos2->SetParameter(1,1.);
  fCos2->SetParName(2,"OFF");
  fCos2->SetParameter(2,0.);
  fCos2->SetParLimits(2,-TMath::PiOver2(),TMath::PiOver2());
  
  

  htexp = gUtils->Get_TH1D(dir, (char*)"HTexp", (char*)"Cosmic Interval",1000,0.,1.5);
	
  fdir = gUtils->Get_Sub_Directory( (char*) "Residuals", (char*)"Cosmics",gOutputFile );
  hhybyres = gUtils->Get_TH2D( fdir, (char*)"pResHyb", (char*)"Y Residual (p-strips)",72,0.,72.,200,-2000.,2000.);
  hhybyres->GetXaxis()->SetTitle("hybrid #");
  hhybyres->GetYaxis()->SetTitle("[#mum]");
  hhybzres = gUtils->Get_TH2D( fdir, (char*)"nResHyb", (char*)"Z Residual (n-strips)",72,0.,72.,200,-2000.,2000.);
  hhybzres->GetXaxis()->SetTitle("hybrid #");
  hhybzres->GetYaxis()->SetTitle("[#mum]");
	
  TDirectory* res_dir = gUtils->Get_Sub_Directory(  (char*)"ResHybrids", (char*)"Cosmics",gOutputFile );
  TString hname,htitle;
  for(Int_t i=0;i<NUM_SI_MODULES;++i)
    {	
      hname="pResHyb"; htitle="Y Residual - Hybrid # ";
      if(i<10) hname+=0;
      hname+=i; htitle+=i;
      new(fpResHyb[i]) TH1D(hname.Data(),htitle.Data(),200,-2000.,2000.);
      ((TH1D*)fpResHyb[i])->GetXaxis()->SetTitle("res [#mu m]");
      res_dir->Add(fpResHyb[i]);
		
      hname="nResHyb"; htitle="Z Residual - Hybrid # ";
      if(i<10) hname+=0;
      hname+=i; htitle+=i;
      new(fnResHyb[i]) TH1D(hname.Data(),htitle.Data(),200,-2000.,2000.);
      ((TH1D*)fnResHyb[i])->GetXaxis()->SetTitle("res [#mu m]");
      res_dir->Add(fnResHyb[i]);
		
      hname="StripRes"; htitle="Residual per Strip - Hybrid # ";
      if(i<10) hname+=0;
      hname+=i; htitle+=i;
      new(fResStrip[i]) TH2D(hname.Data(),htitle.Data(),512,0.,512.,200,-2000.,2000.);
      ((TH2D*)fResStrip[i])->GetXaxis()->SetTitle("VATAC CH");
      ((TH2D*)fResStrip[i])->GetYaxis()->SetTitle("res [#mu m]");
      res_dir->Add(fResStrip[i]);
    }
	
}
//-------------------------------------------------------------------------------------------------
void TCosmicUtil::Process(TSiliconEvent* sil)
{
  ftime = sil->GetVF48Timestamp();
}
//-------------------------------------------------------------------------------------------------
// calls the Calculation methods
void TCosmicUtil::Process(TAlphaEvent* evt)
{
  if(!evt->IsACosmic())
    {
      Msg("Epic Fail! AlphaEvent must be Cosmic");
      exit(2);
    }
  
  //  Msg("Processing ALPHA Event");

  TAlphaEventTrack* aCosmic=evt->GetCosmicTrack(); // grab THE track
  //When no cosmic has been reconstructed, the correlation is set to -999
  Double_t cor = aCosmic->Getcor();
  if (cor<0. ) ++fNoiseCounter;
  else //A cosmic track has been found
    {
      ++fCosmicCounter;
      htexp->Fill(ftime-ftemp);
      ftemp=ftime;
      
      hcor->Fill(cor); // correlation among hits

      // the Track (i.e. straight line) is defined by its slope and a point
      SetSlopeAndPoint(aCosmic);
      if(fSlope[0]==0.&&fSlope[1]==0.&&fSlope[2]==0.) return;

      // determine the angle from the y-axis of the cosmic track
      ExtractDirection(aCosmic);

      CalculateResiduals(aCosmic);     // it does what it says
    }

	return;
}

//-------------------------------------------------------------------------------------------------
// calculate the residual in the y direction (p-strip) and in the z direction (n-strip) for each hit in the track
// as follows:
// determine the intersection of the track with the hybrid in the local coordinate system of the hybrid
// then compute the difference w.r.t. the position of the hit in the hybrid
// 13/06/2013 Similar function to this are duplicated many times all over the ALPHA code
//            either improve this or use one from TAlphaEvent  -- AC
void TCosmicUtil::CalculateResiduals(TAlphaEventTrack* aCosmic)
{
  Int_t Nhits = aCosmic->GetNHits();
  //    std::cout<<"Nhits: "<<Nhits<<std::endl;
  if(Nhits<1) return; // do not proceed if there are not hits
	
  TAlphaEventHit* hit;
  Double_t hy,hz; // local position of TAlphaEventHit on the hybrid
	
	Double_t scp,ssp,sxc,syc; // hybrid position
	
  Double_t t; // track parameter
  Double_t* r = new Double_t [3]; // track position -- given by EvaluateTrack(slope,point,param) --
	
  // residuals container
  Double_t yres; // distance along y axis between the actual hit and calculated track position
  Double_t zres; // distance along z axis between the actual hit and calculated track position

  //std::cout<<"Slope (x,y,z): "<<fSlope[0]<<"\t"<<fSlope[1]<<"\t"<<fSlope[2]<<std::endl;

  // loop over all the hit that are forming the track
  for(Int_t h=0; h<Nhits; ++h)
    {
      hit = (TAlphaEventHit*) aCosmic->GetHit(h);
      hy = hit->Y(); hz = hit->Z(); // local position of hit --> Y = p-strip, Z = n-strip
      Int_t hN = h;
		
      Int_t silNum=hit->GetSilNum(); // the hit knows at which hybrid belongs
      TAlphaEventSil* sil = new TAlphaEventSil(silNum); // create the hybrid
      // hybrid position
      scp = sil->GetCos();     ssp = sil->GetSin();
      sxc = sil->GetXCenter(); syc = sil->GetYCenter();
      
      TAlphaEventTrack* Track = new TAlphaEventTrack();
      for(Int_t i=0; i<Nhits; ++i)
	{
	  if(i == hN) continue;
	  else
	    Track->AddHit((TAlphaEventHit*) aCosmic->GetHit(i));
	}

      Track->MakeLinePCA();
      // TString track_info = TString::Format("Track found: Nhits = %d  Corr = %1.5f",Track->GetNHits(),Track->Getcor());
      // Msg(track_info);

      //  determine the intersection between the track and the plane pinpointed by the hybrid
      //      t = EvaluateIntersection(&sxc,&syc,&scp,&ssp);
      TVector3 TrackUnitVector=Track->Getunitvector();
      TVector3 r0=Track->Getr0();
      //t = EvaluateIntersection( &Track->Getunitvector(), &Track->Getr0(), <- gave an error with gcc48
      t = EvaluateIntersection( &TrackUnitVector, &r0,
			       &sxc,&syc,
			       &scp,&ssp);
		
      //  determine (x,y,z) of the track at the intersection
      //      r = EvaluateTrack(t);
      r = EvaluateTrack(&TrackUnitVector,&r0,t);

      delete Track;

      //std::cout<<"(x,y) global: "<<r[0]<<"\t"<<r[1]<<std::endl;
      // transform the intersection point to the local coordinate sytstem of the module
      Global2Local(&r[0],&r[1],&sxc,&syc,&scp,&ssp);
       
      // residuals calculation in um
      yres = (hy - r[1])*10000.;
      zres = (hz - r[2])*10000.;

      //std::cout<<"(y,z) local: "<<r[1]<<"\t"<<r[2]<<std::endl;
      //std::cout<<"yes: "<<yres<<" um\tzes: "<<zres<<" um"<<std::endl;
		
      // histograms filling
      hhybyres->Fill((Double_t)silNum,yres);
      hhybzres->Fill((Double_t)silNum,zres);
      ((TH1D*)fpResHyb[silNum])->Fill(yres);
      ((TH1D*)fnResHyb[silNum])->Fill(zres);
        
      ((TH2D*)fResStrip[silNum])->Fill( (Double_t) sil->ReturnNStrip(hz), zres);
      ((TH2D*)fResStrip[silNum])->Fill( ((Double_t) sil->ReturnPStrip(hy))+256., yres);

      delete sil; // delete the hybrid
    }
  
  delete[] r;
  
  return;
}

//-------------------------------------------------------------------------------------------------
// find the hybrids on the cosmic trajectory, returns the number of them
Int_t TCosmicUtil::FindModules(TObjArray* SilNumArray)
{
	Double_t cp,sp,xc,yc; // hybrid position variable
	
	Double_t* r = new Double_t [3]; // track position -- given by EvaluateTrack(slope,point,param) --
	Double_t t; // track parameter
	
	// loop over ALL the hybrids
	for(Int_t m = 0; m<NUM_SI_MODULES;++m)
	{
		TAlphaEventSil* sil = new TAlphaEventSil(m);
		cp = sil->GetCos();     sp = sil->GetSin();
		xc = sil->GetXCenter(); yc = sil->GetYCenter();
		
		// determine the intersection between the track and the plane pinpointed by the hybrid
		t = EvaluateIntersection(&xc,&yc,&cp,&sp);
		// determine (x,y,z) of the track at the intersection
		r = EvaluateTrack(t);
		
		// transform the intersection point to the local coordinate sytstem of the module
		Global2Local(&r[0],&r[1],&xc,&yc,&cp,&sp);
		
		// determine whether the track has crossed the hybrid --> has hit a strip
		if( (sil->ReturnPStrip(r[1])>=0) && (sil->ReturnNStrip(r[2])>=0) ) SilNumArray->Add(sil);
	
		delete sil;
	}
	
	delete[] r;
	
	//!!! if you call this function, remember to use 
	//    the Delete method on the TObjArray !!!
	return SilNumArray->GetEntriesFast();
}
//-------------------------------------------------------------------------------------------------
Double_t TCosmicUtil::ExtractDirection(TAlphaEventTrack* aCosmic)
{
  Double_t phi,Angle_From_Vertical=-999.;
  TVector3 slope = aCosmic->Getunitvector();
  phi=slope.Phi();
  
  if(phi>=0.) Angle_From_Vertical = TMath::PiOver2()-phi;
  else if(phi<0.) Angle_From_Vertical = -phi-TMath::PiOver2();

  hdir->Fill(Angle_From_Vertical);

  return Angle_From_Vertical;
}

//-------------------------------------------------------------------------------------------------
/* a hybrid lies in the 3D plane
 (x-xc)cos(phi)+(y-yc)sin(phi)=0   (1)
 where xc and yc are the coordinate of its center and phi is its polar angle

 a track is described by its slope u and a point p, therefore it takes the parametric form 
 / x(t) = u_x t + p_x 
 | y(t) = u_y t + p_y		   (2)
 \ z(t) = u_z t + p_z 
*/
// by plugging (2) in (1), I find the value of the parameter t such that the track intersects the plane
Double_t TCosmicUtil::EvaluateIntersection(const Double_t* xc, const Double_t* yc, const Double_t* cp, const Double_t* sp)
{
	Double_t x = *xc; Double_t y = *yc;
	Double_t cos = *cp; Double_t sin = *sp;
	Double_t N = (x-fPoint[0])*cos + (y-fPoint[1])*sin;
	Double_t D = fSlope[0]*cos + fSlope[1]*sin;
	return N/D;
}

//-------------------------------------------------------------------------------------------------
Double_t TCosmicUtil::EvaluateIntersection(const TVector3* slope, const TVector3* point,
					   const Double_t* xc, const Double_t* yc,
					   const Double_t* cp, const Double_t* sp)
{
  Double_t x = *xc; Double_t y = *yc;
  Double_t cos = *cp; Double_t sin = *sp;
	
  Double_t N = (x-point->X())*cos + (y-point->Y())*sin;
  Double_t D = slope->X()*cos + slope->Y()*sin;

  return N/D;

}
//-------------------------------------------------------------------------------------------------
// given the above description of a track, find its position in cartesian coordinates at a specified parameter
Double_t* TCosmicUtil::EvaluateTrack(Double_t t)
{
	Double_t* pos = new Double_t [3];
	for(Int_t i=0; i<3; ++i) pos[i]=fSlope[i]*t+fPoint[i];
	return pos;
}

//-------------------------------------------------------------------------------------------------
Double_t* TCosmicUtil::EvaluateTrack(const TVector3* slope, const TVector3* point, Double_t t)
{
  Double_t* pos = new Double_t [3];
  //  for(Int_t i=0; i<3; ++i) pos[i]=fSlope[i]*t+fPoint[i];  
  pos[0]=t*slope->X()+point->X();
  pos[1]=t*slope->Y()+point->Y();
  pos[2]=t*slope->Z()+point->Z();
  return pos;
}

//-------------------------------------------------------------------------------------------------
/*/ xL'\   / x \   / xC \
  |    | = |   | - |    |      traslation (3)
  \ yL'/   \ y /   \ yC /

  / xL\    /  cos(phi)  sin(phi) \  / xL'\
  |    | = |			 |  |	 |   rotation (4)
  \ yL/    \ -sin(phi)  cos(phi) /  \ yL'/
*/
void TCosmicUtil::Global2Local(Double_t* x, Double_t* y, const Double_t* xc, const Double_t* yc, const Double_t* cos, const Double_t* sin)
{
	// global coordinate
	Double_t locx = *x; Double_t locy = *y;
	// tralation 2-vector
	Double_t posx= *xc; Double_t posy=*yc;
	// rotation matrix entries
	Double_t c=*cos; Double_t s=*sin;
	
	// the following corresponds to (3)
	locx -= posx;
	locy -= posy;
	// the following corresponds to (4)
	*x = c * locx + s * locy;
	*y = c * locy - s * locx;
}

//-------------------------------------------------------------------------------------------------
// setter
void TCosmicUtil::SetSlope(TAlphaEventTrack* the_Track)
{

	fSlope[0] = (the_Track->Getunitvector()).X();
	fSlope[1] = (the_Track->Getunitvector()).Y();
	fSlope[2] = (the_Track->Getunitvector()).Z();

}

//-------------------------------------------------------------------------------------------------
// setter
void TCosmicUtil::SetPoint(TAlphaEventTrack* the_Track)
{
	fPoint[0] = (the_Track->Getr0()).X(); 
	fPoint[1] = (the_Track->Getr0()).Y();
	fPoint[2] = (the_Track->Getr0()).Z();
}

//-------------------------------------------------------------------------------------------------
// setter
void TCosmicUtil::SetSlopeAndPoint(TAlphaEventTrack* aCosmic)
{
  fSlope[0] = (aCosmic->Getunitvector()).X();
  fSlope[1] = (aCosmic->Getunitvector()).Y();
  fSlope[2] = (aCosmic->Getunitvector()).Z();
  fPoint[0] = (aCosmic->Getr0()).X(); 
  fPoint[1] = (aCosmic->Getr0()).Y();
  fPoint[2] = (aCosmic->Getr0()).Z();
}

//-------------------------------------------------------------------------------------------------
// fit the distribution of the residuals per hybrid with a gaussian
// printout the number of (un)reconstructed events
void TCosmicUtil::End()
{	
  hdir->Fit("fCos2");
	
  Double_t SilNum[NUM_SI_MODULES];
	
  Double_t pRes[NUM_SI_MODULES];
  Double_t ep;
  Double_t pResErr[NUM_SI_MODULES];
  
  Double_t nRes[NUM_SI_MODULES];
  Double_t en;
  Double_t nResErr[NUM_SI_MODULES];
  for(Int_t i=0;i<NUM_SI_MODULES;++i)
    {	
      ((TH1D*)fpResHyb[i])->GetXaxis()->SetTitle("[#mum]");
      ((TH1D*)fnResHyb[i])->GetXaxis()->SetTitle("[#mum]");
      
      SilNum[i]=(Double_t) i;
      pRes[i]=((TH1D*)fpResHyb[i])->GetMean();
      ep = (Double_t)((TH1D*)fpResHyb[i])->GetEntries();
      pResErr[i]=((TH1D*)fpResHyb[i])->GetRMS();
      nRes[i]=((TH1D*)fnResHyb[i])->GetMean();
      en= (Double_t)((TH1D*)fnResHyb[i])->GetEntries();
      nResErr[i]=((TH1D*)fnResHyb[i])->GetRMS();
    }
  
  if (ep != en)
    std::cout <<"ep != en"<<std::endl;
  TGraphErrors* resp = gUtils->Make_TGraphErrors(fdir, (char*)"p-strip Residuals",NUM_SI_MODULES,SilNum,pRes,0,pResErr);
  resp->GetXaxis()->SetTitle("hybrid #");
  resp->GetYaxis()->SetTitle("[#mum]");
  TGraphErrors* resn = gUtils->Make_TGraphErrors(fdir, (char*)"n-strip Residuals",NUM_SI_MODULES,SilNum,nRes,0,nResErr);
  resn->GetXaxis()->SetTitle("hybrid #");
  resn->GetYaxis()->SetTitle("[#mum]");
  
  TGraphErrors* respRMS = gUtils->Make_TGraphErrors(fdir, (char*)"p-strip Residuals RMS",NUM_SI_MODULES,SilNum,pResErr,0,0);
  respRMS->GetXaxis()->SetTitle("hybrid #");
  respRMS->GetYaxis()->SetTitle("[#mum]");
  TGraphErrors* resnRMS = gUtils->Make_TGraphErrors(fdir,(char*)"n-strip Residuals RMS",NUM_SI_MODULES,SilNum,nResErr,0,0);
  resnRMS->GetXaxis()->SetTitle("hybrid #");
  resnRMS->GetYaxis()->SetTitle("[#mum]");
	
  Msg(TString::Format("Number of reconstructed cosmic tracks %d",fCosmicCounter));
  Msg(TString::Format("Number of noise-like events %d",fNoiseCounter));
  Msg("End Cosmic Analyser");
}
