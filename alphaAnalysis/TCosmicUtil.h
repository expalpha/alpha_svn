//==============================================================================================
// TCosmicUtil.h                                                                               =
//                                                                                             =
// Cosmic Run Analyser                                                                         =
// 											       =
// $ alphaAnalyisis --iscosmic --cosmicutil midasdata/run******sub_____.mid.gz                 =
//											       =
//                                                                                             =
// author: AndreaC									       =
//==============================================================================================


#ifndef TCOSUTIL_H
#define TCOSUTIL_H

#include "TClonesArray.h"

#include "TAlphaEventTrack.h"

#include "TAbsUtil.h"

class TCosmicUtil: public TAbsUtil
{

private: 
  Double_t* fSlope; // track slope
  Double_t* fPoint; // track point

  Double_t ftime;
  Double_t ftemp;
  
  TH1D* hcor;   // hit correlation histogram
  TH1D* hdir;   // direction of cosmic tracks
  TF1* fCos2;  // cos^2 function to fit to hdir
  TH1D* htexp;
  
  TH2D* hhybyres; // p-strip residual distribution per hybrid
  TH2D* hhybzres; // n-strip residual distribution per hybrid
  
  TClonesArray fpResHyb; // contains p-strip residual distribution for each hybrid
  TClonesArray fnResHyb; // contains n-strip residual distribution for each hybrid
  
  TClonesArray fResStrip;
  
  TDirectory* fdir;
  
  Int_t fCosmicCounter;
  Int_t fNoiseCounter;

public:
  TCosmicUtil();
  TCosmicUtil(const char* name, const char* description);
  
  ~TCosmicUtil();
  
  void Begin();
  
  virtual void Process(TSiliconEvent* sil);
  virtual void Process(const VF48event* e) { }
  virtual void Process(TAlphaEvent* evt);
  
  void End();
  
   // it does what it says	
  void CalculateResiduals(TAlphaEventTrack*);
  
  // find the hybrids on the cosmic trajectory
  Int_t FindModules(TObjArray* SilNumArray);
  
  // determine the angle from the y-axis of the cosmic track
  Double_t ExtractDirection(TAlphaEventTrack*);
  
  // determine the intersection between the track and the plane pinpointed by the hybrid
  Double_t EvaluateIntersection(const Double_t* xc, const Double_t* yc, 
                                const Double_t* cp, const Double_t* sp);

  Double_t EvaluateIntersection(const TVector3* slope, const TVector3* point,
				const Double_t* xc, const Double_t* yc,
				const Double_t* cp, const Double_t* sp);

  // determine (x,y,z) of the track at the intersection		
  Double_t* EvaluateTrack(Double_t t);
  
  Double_t* EvaluateTrack(const TVector3* slope, const TVector3* point, Double_t t);

  
  // transform the intersection point to the local coordinate sytstem of the module
  void Global2Local(Double_t* x, Double_t* y, 
                    const Double_t* xc, const Double_t* yc, 
                    const Double_t* cos, const Double_t* sin);
  
  // *etters
  void SetSlope(TAlphaEventTrack* the_Track);
  void SetPoint(TAlphaEventTrack* the_Track);
  void SetSlopeAndPoint(TAlphaEventTrack*);
  Double_t* GetSlope() {return fSlope;}
  Double_t* GetPoint() {return fPoint;}	
};


#endif
