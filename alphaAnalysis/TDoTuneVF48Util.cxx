#include "TDoTuneVF48Util.h"

#include "./lib/include/TVF48SiMap.h"
#include "./lib/include/TUtilities.h"
#include "TFile.h"


extern TVF48SiMap*  gVF48SiMap;
extern TUtilities*  gUtils;
extern TFile*       gOutputFile;

  /////////////////////////////////////////////////////////////////
  // Analysis of VF48 sample adjustement and ADC pulse height
  /////////////////////////////////////////////////////////////////

void TDoTuneVF48Util::Begin(){

  Msg("Begin");

  sub_dir = gUtils->Get_Sub_Directory( "VF48 Tuning", "QOD", gOutputFile );

  char name[256];

  for( int vf48modnum=0; vf48modnum<NUM_VF48_MODULES; vf48modnum++ ) {

    sprintf( name, "VF48 Module %d  : edge samples 0..1", vf48modnum );
    h0_1[vf48modnum] = gUtils->Get_TH2D( sub_dir, name,  55, 0, 1100, 55, 0, 1100);
    
    sprintf( name, "VF48 Module %d  : edge samples 1..2", vf48modnum );
    h1_2[vf48modnum] = gUtils->Get_TH2D( sub_dir, name,  55, 0, 1100, 55, 0, 1100);
    
    sprintf( name, "VF48 Module %d  : edge samples 2..3", vf48modnum );
    h2_3[vf48modnum] = gUtils->Get_TH2D( sub_dir, name,  55, 0, 1100, 55, 0, 1100);
    
    sprintf( name, "VF48 Module %d  : edge samples 3..4", vf48modnum );
    h3_4[vf48modnum] = gUtils->Get_TH2D( sub_dir, name,  55, 0, 1100, 55, 0, 1100);
    
    sprintf( name, "VF48 Module %d  : edge samples 123..124", vf48modnum );
    h123_124[vf48modnum] = gUtils->Get_TH2D( sub_dir, name,  55, 0, 1100, 55, 0, 1100);
    
    sprintf( name, "VF48 Module %d  : edge samples 124..125", vf48modnum );
    h124_125[vf48modnum] = gUtils->Get_TH2D( sub_dir, name,  55, 0, 1100, 55, 0, 1100);
    
    sprintf( name, "VF48 Module %d  : edge samples 125..126", vf48modnum );
    h125_126[vf48modnum] = gUtils->Get_TH2D( sub_dir, name,  55, 0, 1100, 55, 0, 1100);
    
    sprintf( name, "VF48 Module %d  : edge samples 126..127", vf48modnum );
    h126_127[vf48modnum] = gUtils->Get_TH2D( sub_dir, name,  55, 0, 1100, 55, 0, 1100);
    
    //sprintf( name, "VF48 Module %d  : edge samples 127..128", vf48modnum );
    //h127_128[vf48modnum] = gUtils->Get_TH2D( sub_dir, name,  55, 0, 1100, 55, 0, 1100);
    
    //sprintf( name, "VF48 Module %d  : edge samples 128..129", vf48modnum );
    //h128_129[vf48modnum] = gUtils->Get_TH2D( sub_dir, name,  55, 0, 1100, 55, 0, 1100);
    
    sprintf( name, "VF48 Module %d  : min adc", vf48modnum );
    hmin_adc[vf48modnum] = gUtils->Get_TH1D( sub_dir, name,  110, 0, 1100);
    
    sprintf( name, "VF48 Module %d  : max adc", vf48modnum );
    hmax_adc[vf48modnum] = gUtils->Get_TH1D( sub_dir, name,  110, 0, 1100);
    
  }
  
};


void TDoTuneVF48Util::Process(const VF48event* e){


  for( int vf48modnum=0; vf48modnum<NUM_VF48_MODULES; vf48modnum++ ) {
    if (!e->modules[vf48modnum])
      continue;

    int min = 999999;
    int max = 0;
    
    for( int vf48chan=0; vf48chan<48; vf48chan++ ) {

      int isil, iva, frcno, frcpt, ttcchannel;

      // only process the modules / channels that are connected to a silicon module
      gVF48SiMap->GetSil( vf48modnum, vf48chan, isil, iva, frcno, frcpt, ttcchannel );
      if (isil<0 || iva<1)
        continue;

      int num_samples = e->modules[vf48modnum]->channels[vf48chan].numSamples;
      
      if (num_samples != 130 && num_samples != 128)
        continue;

      for (int i=0; i<num_samples; i++) {
        int a =  e->modules[vf48modnum]->channels[vf48chan].samples[i];
        if (a < min)
          min = a;
        if (a > max)
          max = a;
      }

      
      h0_1[vf48modnum]->Fill(e->modules[vf48modnum]->channels[vf48chan].samples[0], e->modules[vf48modnum]->channels[vf48chan].samples[1]);
      h1_2[vf48modnum]->Fill(e->modules[vf48modnum]->channels[vf48chan].samples[1], e->modules[vf48modnum]->channels[vf48chan].samples[2]);
      h2_3[vf48modnum]->Fill(e->modules[vf48modnum]->channels[vf48chan].samples[2], e->modules[vf48modnum]->channels[vf48chan].samples[3]);
      h3_4[vf48modnum]->Fill(e->modules[vf48modnum]->channels[vf48chan].samples[3], e->modules[vf48modnum]->channels[vf48chan].samples[4]);
      h123_124[vf48modnum]->Fill(e->modules[vf48modnum]->channels[vf48chan].samples[123], e->modules[vf48modnum]->channels[vf48chan].samples[124]);
      h124_125[vf48modnum]->Fill(e->modules[vf48modnum]->channels[vf48chan].samples[124], e->modules[vf48modnum]->channels[vf48chan].samples[125]);
      h125_126[vf48modnum]->Fill(e->modules[vf48modnum]->channels[vf48chan].samples[125], e->modules[vf48modnum]->channels[vf48chan].samples[126]);
      h126_127[vf48modnum]->Fill(e->modules[vf48modnum]->channels[vf48chan].samples[126], e->modules[vf48modnum]->channels[vf48chan].samples[127]);
      //if (num_samples > 128) {
      //  h127_128[vf48modnum]->Fill(e->modules[vf48modnum]->channels[vf48chan].samples[127], e->modules[vf48modnum]->channels[vf48chan].samples[128]);
      //  h128_129[vf48modnum]->Fill(e->modules[vf48modnum]->channels[vf48chan].samples[128], e->modules[vf48modnum]->channels[vf48chan].samples[129]);
      //}
    }
    
    hmin_adc[vf48modnum]->Fill(min);
    hmax_adc[vf48modnum]->Fill(max);
  }
  
};  


void TDoTuneVF48Util::End(){

  Msg("End");

};




