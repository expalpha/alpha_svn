#ifndef TDOTUNEVF48UTIL_H
#define TDOTUNEVF48UTIL_H

#include <iostream>
#include <fstream>
#include <vector>

#include "TAbsUtil.h"
#include "SiMod.h"

class TDoTuneVF48Util: public TAbsUtil {

protected: 
  TDirectory* sub_dir;
  TH2D* h0_1[NUM_VF48_MODULES];
  TH2D* h1_2[NUM_VF48_MODULES];
  TH2D* h2_3[NUM_VF48_MODULES];
  TH2D* h3_4[NUM_VF48_MODULES];
  TH2D* h123_124[NUM_VF48_MODULES];
  TH2D* h124_125[NUM_VF48_MODULES];
  TH2D* h125_126[NUM_VF48_MODULES];
  TH2D* h126_127[NUM_VF48_MODULES];
  
  TH1D* hmin_adc[NUM_VF48_MODULES];
  TH1D* hmax_adc[NUM_VF48_MODULES];


public:
  TDoTuneVF48Util()  { } 
  TDoTuneVF48Util(const char* name, const char* description) : TAbsUtil(name,description) { } 

  ~TDoTuneVF48Util() { } 
  
  void Begin();

  void virtual Process(const VF48event* e);
  void virtual Process(TSiliconEvent* sil){ };
  virtual void Process(TAlphaEvent* evt) { } ;

  void End();

};

#endif


