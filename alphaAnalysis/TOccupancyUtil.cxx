//AO: Added ADCmean/strip and ADC/asic histos 20/08/14 
#include "TOccupancyUtil.h"

#include "./lib/include/TVF48SiMap.h"
#include "./lib/include/TUtilities.h"


//#include "./lib/include/TADCTriggerEvent.h"
//#include "./lib/include/TSiliconStrip.h"
//#include "./lib/include/TSiliconEvent.h"
#include "TFile.h"

using namespace std;

extern TVF48SiMap*  gVF48SiMap;
extern TUtilities*  gUtils;
extern TFile*       gOutputFile;


extern Int_t gEventID;

extern Double_t starttime;
extern Double_t stoptime;
extern bool gUseTimeRange;
//extern Double_t        gVF48RunTimeOffset; 

//extern TObjArray 
extern TSiliconEvent  *gSiliconEvent;
//extern ADCTriggerEvent;
extern unsigned long int gClock[2];


void TOccupancyUtil::Begin(){

  Msg("Begin Occ");

  TDirectory* sub_dir = gUtils->Get_Sub_Directory( "Occupancy", "QOD", gOutputFile );
  TDirectory * sub_dir_adc = gUtils->Get_Sub_Directory( "ADC", "QOD", gOutputFile );  
  char name[256];
  char title[256];
  int m, c, ttcchannel;

  for (int isil=0; isil<NUM_SI_MODULES; isil++){    

    for( int iASIC = 1; iASIC <= 4; iASIC++ ) {      

      gVF48SiMap->GetVF48( isil,iASIC, m, c, ttcchannel);
      sprintf( name, "VF48%d_Channel%03d_Si%02d_ASIC%d_OCC", m, c, isil, iASIC );
//      sprintf( name, "Si%02d_ASIC%d_OCC", isil, iASIC ); //Joe Simplified this
      sprintf( title, "VF48# %d Channel# %3d Si# %02d ASIC# %d : OCC", m, c, isil, iASIC );
      hist[isil*4 + (iASIC-1)] = gUtils->Get_TH1D( sub_dir, name, title, 128, 0, 128 );    

      sprintf( name, "VF48%d_Channel%03d_Si%02d_ASIC%d_ADCmean", m, c, isil, iASIC );
//      sprintf( name, "Si%02d_ASIC%d_ADCmean", isil, iASIC );
      sprintf( title, "VF48# %d Channel# %3d Si# %02d ASIC# %d : ADC mean", m, c, isil, iASIC );     
      strip_hist[isil*4 + (iASIC-1)] =  gUtils->Get_TH1D( sub_dir_adc, name, title,128,0.,128. );

      sprintf( name, "VF48%d_Channel%03d_Si%02d_ASIC%d_ADC", m, c, isil, iASIC );
//      sprintf( name, "Si%02d_ASIC%d_ADC", isil, iASIC );
      sprintf( title, "VF48# %d Channel# %3d Si# %02d ASIC# %d : ADC", m, c, isil, iASIC );
      adc_hist[isil*4 + (iASIC-1)] =  gUtils->Get_TH1D( sub_dir_adc, name, title,201,-1000.,1000. );
    }

  }
  hist_si_occ = gUtils->Get_TH1D( sub_dir,(char*)"Si_Occs", (char*)"Silicon Module Occupancies",  NUM_SI_MODULES,0,NUM_SI_MODULES);
};

void TOccupancyUtil::Process(TSiliconEvent* sil){

extern uint64_t gClock[2];

   //Msg(TString::Format("0x%x",*sil));
  for (int isil=0; isil<NUM_SI_MODULES; isil++){    

    TSiliconModule * module = sil->GetSiliconModule( isil );
    if( !module ) continue;
    

    for( int iASIC = 1; iASIC <= 4; iASIC++ ) {      
      TSiliconVA * asic = module->GetASIC( iASIC );
      if( !asic ) continue;
      
      TObjArray * strip_array = asic->GetStrips();
      Int_t nASIC  = asic->GetASICNumber() - 1;
      
      for(int s = 0; s<strip_array->GetEntries(); s++) {
        TSiliconStrip * strip = (TSiliconStrip*) strip_array->At(s);

        if( strip->IsAHit() ){

//cout << "Event"<<  gEventID << "Clock" << (gClock[1])/10000000.0  << endl;
if (gUseTimeRange)
{

//cout << "Sil:" << gSiliconEvent << "-" << starttime << endl; 
if ( ((gClock[1])/10000000.0 > starttime) && ((gClock[1])/10000000.0 < stoptime) ) {
// && ADCTriggerEvent->GetRunTime()>stoptime ) {
//cout << "Within time bin" << endl;
if (isil==0) {
cout << "Event"<<  gEventID << "Clock" << (gClock[1])/10000000.0  << endl;
}
	  Int_t istrip=strip->GetStripNumber();
       	  Double_t pedsubadc= strip->GetPedSubADC();
          hist[isil*4 + nASIC]->Fill( (Double_t) istrip );
	  hist_si_occ->Fill(isil);
       	  strip_hist[isil*4 +nASIC]->Fill(istrip, pedsubadc );
       	  adc_hist[isil*4 +nASIC]->Fill(pedsubadc );

}
}
else {
	  Int_t istrip=strip->GetStripNumber();
       	  Double_t pedsubadc= strip->GetPedSubADC();
          hist[isil*4 + nASIC]->Fill( (Double_t) istrip );
	  hist_si_occ->Fill(isil);
       	  strip_hist[isil*4 +nASIC]->Fill(istrip, pedsubadc );
       	  adc_hist[isil*4 +nASIC]->Fill(pedsubadc );
}

        }
      }
    }
  }

}


void TOccupancyUtil::End(){

  Msg("End Occ");
  //Divide by occupancy to get means for each strip
 for (int isil=0; isil<NUM_SI_MODULES; isil++){    
    for( int iASIC = 0; iASIC < 4; iASIC++ ) {
      if( (strip_hist[isil*4 +iASIC]->GetEntries())*(hist[isil*4+iASIC]->GetEntries())>0)      
      strip_hist[isil*4 +iASIC]->Divide(hist[isil*4+iASIC]);

    }//loop iASIC
  }//loop iSil

};




