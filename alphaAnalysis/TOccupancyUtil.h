//AO: Added ADCmean/strip and ADC/asic histos 20/08/14 
#ifndef TOCCUTIL_H
#define TOCCUTIL_H

#include <iostream>
#include <fstream>
#include <vector>

#include "TAbsUtil.h"
#include "SiMod.h"

class TOccupancyUtil: public TAbsUtil {

protected: 
  TDirectory* sub_dir;
  TH1D* hist[NUM_SI_MODULES*4];
  TH1D* hist_si_occ;
  TDirectory* sub_dir_adc;
  TH1D* strip_hist[NUM_SI_MODULES*4];
  TH1D* adc_hist[NUM_SI_MODULES*4];

public:
  TOccupancyUtil()  { } 
  TOccupancyUtil(const char* name, const char* description) : TAbsUtil(name,description) { } 

  ~TOccupancyUtil() { } 
  
  void Begin();

  virtual void Process(TSiliconEvent* sil);
  virtual void Process(const VF48event* e) { } ;
  virtual void Process(TAlphaEvent* evt) { } ;
  
  void End();

};

#endif


