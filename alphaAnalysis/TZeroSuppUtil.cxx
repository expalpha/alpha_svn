#include "TZeroSuppUtil.h"
#include "./lib/include/TVF48SiMap.h"
#include "./lib/include/TUtilities.h"
#include "TFile.h"

#include "include/VirtualOdb.h"
extern VirtualOdb  *gOdb;

extern TVF48SiMap*  gVF48SiMap;
extern TUtilities*  gUtils;
extern TFile*       gOutputFile;
extern uint16_t thres;

static int xthresholds[8][6][8];

//////////////////////////////////////////////
// Analysis of VF48 channel suppression
//////////////////////////////////////////////


int TZeroSuppUtil::VF48_HaveHit_Old(const VF48channel *chan, int16_t threshold, int16_t nskip, int16_t nrunavg) {

  int num_samples = chan->numSamples;

  assert(num_samples > 2);
  assert(nskip>0);

  int Navg = nrunavg;
  int Ns = Navg+nskip; // A(i) - A(i-Ns) 
  int have_hit = 0;
  //int d1_max = 0;
  //int d2_max = 0;
  //int dd_max = 0;
  
   
  for (int i=Ns; i<num_samples; i++) {
    
    int da = 0; 
    for (int j=0; j<Navg; j++) 
      da += chan->samples[i-Ns+j]; 
    da /= Navg;
     
    int db = 0;
    for (int j=0; j<Navg; j++)
      db += chan->samples[i-j]; 
    db /= Navg;
     
    int d1 = chan->samples[i] - da;
    int d2 = da - chan->samples[i];
    
    int d3 = chan->samples[i-Ns] - db;
    int d4 = db - chan->samples[i-Ns];
    
    int h1 = d1 > threshold;
    int h2 = d2 > threshold;
    int h3 = d3 > threshold;
    int h4 = d4 > threshold;
    int h = (h1||h2||h3||h4);
    
    have_hit |= h;
    
  }
  
  return have_hit;

}


int TZeroSuppUtil::VF48_HaveHit(const VF48channel *chan, int16_t threshold, int Navg, bool lastSampleBug, int16_t* smin, int16_t* smax) {
  
  int num_samples = chan->numSamples;

  assert(num_samples > 2);

  *smin = 9999;
  *smax = -9999;
            
  if (lastSampleBug)
    num_samples -= 1;

  for (int i=1; i<num_samples-Navg; i++) {
    int da = 0; 
    for (int j=0; j<Navg; j++) {
      da += chan->samples[i+j];
    }
    da /= Navg;
    
    int16_t data9 = chan->samples[i-1];
    int16_t data0 = chan->samples[i+Navg];
    
    int16_t r = da;
     
    int16_t mon0 = data0 - r;
    int16_t mon9 = data9 - r;

    if (mon0 <  *smin)
      *smin = mon0;

    if (mon9 <  *smin)
      *smin = mon9;

    if (mon0 > *smax)
      *smax = mon0;

    if (mon9 > *smax)
      *smax = mon9;
  }
  
  int16_t t = threshold;
  int16_t tinv = ~t;
  
  int hitp = (*smax > t);
  int hitn = (*smin <= tinv);
  int h = (hitp||hitn);
 

  return h;
}

void TZeroSuppUtil::Begin(){

  xcgood = 0;
  xcswbad = 0;
  xcrdbad = 0;
    
  ctotal = 0;
  cpresent = 0;
  if (thres==0)
    {
#if 0
      gVF48commonThreshold = 0;
      gVF48thresholds[0][0] = 55; gVF48thresholds[0][1] = 54; gVF48thresholds[0][2] = 54; gVF48thresholds[0][3] = 56; gVF48thresholds[0][4] = 55; gVF48thresholds[0][5] = 20; 
      gVF48thresholds[1][0] = 56; gVF48thresholds[1][1] = 56; gVF48thresholds[1][2] = 56; gVF48thresholds[1][3] = 56; gVF48thresholds[1][4] = 56; gVF48thresholds[1][5] = 54; 
      gVF48thresholds[2][0] = 56; gVF48thresholds[2][1] = 55; gVF48thresholds[2][2] = 53; gVF48thresholds[2][3] = 53; gVF48thresholds[2][4] = 55; gVF48thresholds[2][5] = 53; 
      gVF48thresholds[3][0] = 54; gVF48thresholds[3][1] = 20; gVF48thresholds[3][2] = 20; gVF48thresholds[3][3] = 20; gVF48thresholds[3][4] = 20; gVF48thresholds[3][5] = 20; 
      gVF48thresholds[4][0] = 52; gVF48thresholds[4][1] = 52; gVF48thresholds[4][2] = 43; gVF48thresholds[4][3] = 43; gVF48thresholds[4][4] = 47; gVF48thresholds[4][5] = 20; 
      gVF48thresholds[5][0] = 48; gVF48thresholds[5][1] = 47; gVF48thresholds[5][2] = 49; gVF48thresholds[5][3] = 48; gVF48thresholds[5][4] = 48; gVF48thresholds[5][5] = 49; 
      gVF48thresholds[6][0] = 47; gVF48thresholds[6][1] = 47; gVF48thresholds[6][2] = 40; gVF48thresholds[6][3] = 47; gVF48thresholds[6][4] = 49; gVF48thresholds[6][5] = 49; 
      gVF48thresholds[7][0] = 48; gVF48thresholds[7][1] = 20; gVF48thresholds[7][2] = 20; gVF48thresholds[7][3] = 20; gVF48thresholds[7][4] = 20; gVF48thresholds[7][5] = 20; 
#endif
      const char* fname = getenv("THRESFILE");//"Threshold_Levels30.csv";

      printf("Reading VF48 thresholds file %s\n", fname);

      FILE *fp = fopen(fname, "r");
      assert(fp != NULL);
      for (int i=0; i<NUM_VF48_MODULES; i++) {
	char s[1024];
	fgets(s, sizeof(s), fp);
	char* sptr = s;
	for (int j=0; j<VF48_MAX_GROUPS; j++)
	  for (int k=0; k<8; k++) {
	    xthresholds[i][j][k] = strtoul(sptr, &sptr, 0);
	    sptr++;
	  }
      }
      fclose(fp);

      // values have to be doctored by hand
      xthresholds[2][0][0] = 10;

      for (int i=0; i<NUM_VF48_MODULES; i++) {
	for (int j=0; j<VF48_MAX_GROUPS; j++)
	  for (int k=0; k<8; k++)
	    printf(" %3d", xthresholds[i][j][k]);
	printf("\n");
      }
    }
  else
    {
      gVF48commonThreshold = thres;
      
      //int odb_common_threshold = gOdb->odbReadInt("/equipment/VF48/Settings/Common_Threshold", 0, 0);
      
      for (int m=0; m<NUM_VF48_MODULES; m++) {
	for (int g=0; g<VF48_MAX_GROUPS; g++) {
	  //int t = gOdb->odbReadInt("/equipment/VF48/Settings/VF48_Threshold", m*VF48_MAX_GROUPS+g, 0);
	  //printf("odb common threshold %d, module %d group %d threshold %d\n", odb_common_threshold, m, g, t);

	  //  int t = gVF48commonThreshold; // Joe added line
	  //  if (t==0)
	  //	{
	  gVF48thresholds[m][g] = gVF48commonThreshold;
	}
      }    
    }

  printf("in TZeroSuppUtil gOdb %p\n", gOdb);

  if (0) {
    int odb_common_threshold = gOdb->odbReadInt("/equipment/VF48/Settings/Common_Threshold", 0, 0);
    
    for (int m=0; m<NUM_VF48_MODULES; m++) {
      for (int g=0; g<VF48_MAX_GROUPS; g++) {
	int t = gOdb->odbReadInt("/equipment/VF48/Settings/VF48_Threshold", m*VF48_MAX_GROUPS+g, 0);

	printf("odb common threshold %d, module %d group %d threshold %d\n", odb_common_threshold, m, g, t);
      }    
    }
  }
  
  sub_dir = gUtils->Get_Sub_Directory( "VF48 ChSupp", "QOD", gOutputFile );

  char name[256];
  
  for( int vf48modnum=0; vf48modnum<NUM_VF48_MODULES; vf48modnum++ ) {
    
    sprintf( name, "VF48 Module %d  : chsupp min", vf48modnum );
    hmin[vf48modnum] = gUtils->Get_TH1D( sub_dir, name,  500, -500, 0);
    
    sprintf( name, "VF48 Module %d  : chsupp max", vf48modnum );
    hmax[vf48modnum] = gUtils->Get_TH1D( sub_dir, name,  500, 0, 500);

    sprintf( name, "VF48 Module %d  : ch0 supp min", vf48modnum );
    hmin_ch0[vf48modnum] = gUtils->Get_TH1D( sub_dir, name,  500, -500, 0);
    
    sprintf( name, "VF48 Module %d  : ch0 supp max", vf48modnum );
    hmax_ch0[vf48modnum] = gUtils->Get_TH1D( sub_dir, name,  500, 0, 500);

    sprintf( name, "VF48 Module %d  : ch1 supp min", vf48modnum );
    hmin_ch1[vf48modnum] = gUtils->Get_TH1D( sub_dir, name,  500, -500, 0);
    
    sprintf( name, "VF48 Module %d  : ch1 supp max", vf48modnum );
    hmax_ch1[vf48modnum] = gUtils->Get_TH1D( sub_dir, name,  500, 0, 500);
    
    sprintf( name, "VF48 Module %d  : ch2 supp min", vf48modnum );
    hmin_ch2[vf48modnum] = gUtils->Get_TH1D( sub_dir, name,  500, -500, 0);
    
    sprintf( name, "VF48 Module %d  : ch2 supp max", vf48modnum );
    hmax_ch2[vf48modnum] = gUtils->Get_TH1D( sub_dir, name,  500, 0, 500);
    
    sprintf( name, "VF48 Module %d  : ch3 supp min", vf48modnum );
    hmin_ch3[vf48modnum] = gUtils->Get_TH1D( sub_dir, name,  500, -500, 0);
    
    sprintf( name, "VF48 Module %d  : ch3 supp max", vf48modnum );
    hmax_ch3[vf48modnum] = gUtils->Get_TH1D( sub_dir, name,  500, 0, 500);
  
  }
  
  Msg(TString::Format("Begin with threshold %d", gVF48commonThreshold));
};

void TZeroSuppUtil::Process(const VF48event* e) {
  //Msg(TString::Format("0x%x",e));

  for( int vf48modnum=0; vf48modnum<NUM_VF48_MODULES; vf48modnum++ ) {
    if (!e->modules[vf48modnum])
      continue;

    //int fwmask[VF48_MAX_GROUPS];
    int swmask[VF48_MAX_GROUPS];
    int rdmask[VF48_MAX_GROUPS];

    for (int grp=0; grp<VF48_MAX_GROUPS; grp++) {
      
      fwmask[grp] = e->modules[vf48modnum]->suppressMask[grp];
      swmask[grp] = 0;
      rdmask[grp] = 0;

      for (int ch=0; ch<8; ch++ ) {
        int vf48chan = grp*8+ch;
        int num_samples = e->modules[vf48modnum]->channels[vf48chan].numSamples;

        //int fwhit = (fwmask[grp] & (1<<ch));

        ctotal++;
        xctotal[vf48modnum][grp]++;

        if (num_samples > 0) {
          cpresent++;
          xcpresent[vf48modnum][grp]++;
 
          rdmask[grp] |= (1<<ch); // <<<<<<<------
    
          if (1) {
            int threshold = gVF48commonThreshold;		
            if (gVF48commonThreshold==0) { 
              //int threshold = gVF48thresholds[vf48modnum][grp];
              threshold = xthresholds[vf48modnum][grp][ch];
            }
            int16_t smin = 0;
            int16_t smax = 0;
            int swhit = VF48_HaveHit(&e->modules[vf48modnum]->channels[vf48chan], threshold, Navg, lastSampleBug, &smin, &smax); //...
            if (swhit)
              swmask[grp] |= (1<<ch);

#if 1
            hmin[vf48modnum]->Fill(smin);
            hmax[vf48modnum]->Fill(smax);

            if (vf48chan==0) {
              hmin_ch0[vf48modnum]->Fill(smin);
              hmax_ch0[vf48modnum]->Fill(smax);
            }
            
            if (vf48chan==1) {
              hmin_ch1[vf48modnum]->Fill(smin);
              hmax_ch1[vf48modnum]->Fill(smax);
            }
            
            if (vf48chan==2) {
              hmin_ch2[vf48modnum]->Fill(smin);
              hmax_ch2[vf48modnum]->Fill(smax);
            }
            
            if (vf48chan==3) {
              hmin_ch3[vf48modnum]->Fill(smin);
              hmax_ch3[vf48modnum]->Fill(smax);
            }
#endif

            if (1) {// emulate hardware channel suppression
              if (!swhit) {
                // printf("%d",e->modules[vf48modnum]->channels[vf48chan].samples[0]); //Output before zeroing
                e->modules[vf48modnum]->channels[vf48chan].numSamples = 0;
                for (int s=0; s<num_samples; s++) {
                  e->modules[vf48modnum]->channels[vf48chan].samples[s] = 0;
                }
              }
              // printf("-%d \n",e->modules[vf48modnum]->channels[vf48chan].samples[0]); //Zeroed outputs
            }
          }
        }
      }
    }

#if 0
    bool swgood = true;
    bool rdgood = true;
    
    for (int grp=0; grp<VF48_MAX_GROUPS; grp++) {
      if (swmask[grp] != fwmask[grp])
        swgood = false;
      if (rdmask[grp] != 0xFF)
        if (rdmask[grp] != fwmask[grp])
          rdgood = false;
    }
    
    if (swgood && rdgood)
      xcgood ++;
    if (!swgood)
      xcswbad ++;
    if (!rdgood)
      xcrdbad ++;
    
    if (!swgood || !rdgood) {
      Msg(TString::Format("module %d: ", vf48modnum));
      Msg(TString::Format("masks: "));
      Msg(TString::Format("fw: %02x %02x %02x %02x %02x %02x", fwmask[5], fwmask[4], fwmask[3], fwmask[2], fwmask[1], fwmask[0]));
      Msg(TString::Format(", sw: %02x %02x %02x %02x %02x %02x", swmask[5], swmask[4], swmask[3], swmask[2], swmask[1], swmask[0]));
      Msg(TString::Format(", rd: %02x %02x %02x %02x %02x %02x", rdmask[5], rdmask[4], rdmask[3], rdmask[2], rdmask[1], rdmask[0]));
      Msg(TString::Format(", swgood %d, rdgood %d", swgood, rdgood));
      Msg(TString::Format(", total %d, present %d, supprate %f\n", ctotal, cpresent, (ctotal-cpresent)*1.0/ctotal));
    }
#endif
  }
};


void TZeroSuppUtil::End(){


  
  Msg("VF48 channel suppression report:");
  
  int ttotal = 0;
  int tpresent = 0;
  int mtotal[VF48_MAX_MODULES];
  int mpresent[VF48_MAX_MODULES];
  
  for (int i=0; i<VF48_MAX_MODULES; i++) {
    mtotal[i] = 0;
    mpresent[i] = 0;
    
    for (int j=0; j<VF48_MAX_GROUPS; j++) {
      int total = xctotal[i][j];
      int present = xcpresent[i][j];
      
      if (!total)
        continue;
      
      ttotal += total;
      tpresent += present;
      
      mtotal[i] += total;
      mpresent[i] += present;
      
      double rate = (total-present)*1.0/total;
      Msg(TString::Format("module %d: group %d: total %6d, present %6d, suppression rate %5.1f%%", i, j, total, present, 100.0*rate));
    }
  }

  for (int i=0; i<VF48_MAX_MODULES; i++) {
    if (!mtotal[i])
      continue;
    double rate = (mtotal[i]-mpresent[i])*1.0/mtotal[i];
    Msg(TString::Format("module %d: total %6d, present %6d, suppression rate %5.1f%%", i, mtotal[i], mpresent[i], 100.0*rate));
  }
  
  if (1) {
    double rate = (ttotal-tpresent)*1.0/ttotal;
    Msg(TString::Format("total %d, present %d, good %d, sw mismatch %d, rd mismatch %d, suppression rate %.1f%%", ttotal, tpresent, xcgood, xcswbad, xcrdbad, 100.0*rate));
  }
  
  
};




