#ifndef TZSUPPUTIL_H
#define TZSUPPUTIL_H

#include <iostream>
#include <fstream>
#include <vector>

#include "TAbsUtil.h"
#include "SiMod.h"

class TZeroSuppUtil: public TAbsUtil {

protected:

  TDirectory* sub_dir;

  TH1D* hmin[NUM_VF48_MODULES];
  TH1D* hmax[NUM_VF48_MODULES];
  TH1D* hmin_ch0[NUM_VF48_MODULES];
  TH1D* hmax_ch0[NUM_VF48_MODULES];
  TH1D* hmin_ch1[NUM_VF48_MODULES];
  TH1D* hmax_ch1[NUM_VF48_MODULES];
  TH1D* hmin_ch2[NUM_VF48_MODULES];
  TH1D* hmax_ch2[NUM_VF48_MODULES];
  TH1D* hmin_ch3[NUM_VF48_MODULES];
  TH1D* hmax_ch3[NUM_VF48_MODULES];

  int xcgood;
  int xcswbad;
  int xcrdbad;
  int xctotal[VF48_MAX_MODULES][VF48_MAX_GROUPS];
  int xcpresent[VF48_MAX_MODULES][VF48_MAX_GROUPS];

  int fwmask[VF48_MAX_GROUPS];
  int swmask[VF48_MAX_GROUPS];
  int rdmask[VF48_MAX_GROUPS];
  
  int ctotal;
  int cpresent;

  int    gVF48commonThreshold;

  int    gVF48thresholds[VF48_MAX_MODULES][VF48_MAX_GROUPS];

  int VF48_HaveHit(const VF48channel *chan, int16_t threshold, int Navg, bool lastSampleBug, int16_t* smin, int16_t* smax); 


  int VF48_HaveHit_Old(const VF48channel *chan, int16_t threshold, int16_t nskip, int16_t nrunavg);

public:
  TZeroSuppUtil()  { } ;
  TZeroSuppUtil(const char* name, const char* description) : TAbsUtil(name,description), Navg(8), lastSampleBug(false) { } ;

  ~TZeroSuppUtil() { } ;

  int Navg;
  bool lastSampleBug;

  void Begin();

  

  virtual void Process(TSiliconEvent* sil){ };
  virtual void Process(const VF48event* e);
  virtual void Process(TAlphaEvent* evt) { } ;
  
  void End();

};

#endif
