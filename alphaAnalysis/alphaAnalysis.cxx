
// ===========================================================================
//
// alphaAnalysis
//
// MIDAS analyser for the production of ROOT trees for offline analysis. 
//
// Contributors; Art, James, Konstantin, Richard & Sarah. AndreaC, Joseph
//
// ===========================================================================

// bertsche 19/ 09/ 2010 - added a flag '-es' to the command line arguments
//                       - which allows autosaves when running the event
//                       - display. Autosaves to ./plots
 
#include <stdio.h>
#include <sys/time.h>
#include <signal.h>
#include <assert.h>
#include <iostream>
#include <time.h>
#include <stdint.h>

#ifdef HAVE_MIDAS
#include "include/TMidasOnline.h"
#endif
#include "include/TMidasEvent.h"
#include "include/TMidasFile.h"
#include "include/XmlOdb.h"
#include "include/xmlServer.h"
//#include "obsolete/midasServer.h"
#include "UnpackVF48.h"
extern UnpackVF48* vfu;

#include <TROOT.h>
#include <TSystem.h>
#include <TClass.h>
#include <TApplication.h>
#include <TTimer.h>
#include <TFile.h>
#include <TDirectory.h>
#include <TFolder.h>
#include <TTree.h>
#include <TBranch.h>
#include <TError.h>

#include "./lib/include/TSisEvent.h"
#include "./lib/include/TSiliconEvent.h"
#include "./lib/include/TLabVIEWEvent.h"
#include "./lib/include/TSeq_Event.h"
#include "./lib/include/TSeq_State.h"
#include "./lib/include/TSequenceDriver.h"
#include "./lib/include/TADCTriggerEvent.h"
#include "./lib/include/TTCEvent.h"
#include "./lib/include/TSiliconVA.h"
#include "./lib/include/TVF48SiMap.h"
#include "./lib/include/TSettings.h"
#include "./lib/include/TUtilities.h"
#include "./lib/include/TSISChannels.h"
#include "./lib/include/TAnalysisReport.h"
#include "./lib/include/TalphaStripsReport.h"
#include "./lib/include/TTimestampHelper.h"
#include "./lib/include/TSeq_EventHelper.h"

#include "./lib/include/Sequencer2.h"

#include "TAbsUtil.h"
#include "TOccupancyUtil.h"
#include "TZeroSuppUtil.h"
#include "TDoTuneVF48Util.h"
#include "SiDAQControl.h"
#include "TCosmicUtil.h"

#include <TGeoManager.h>
#include "TAlphaGeoDetectorXML.h"
#include "TAlphaGeoMaterialXML.h"
#include "TAlphaGeoEnvironmentXML.h"

#include "TAlphaEvent.h"

#include "alphaAnalysisSVNLog.h"

#define NUM_VF48_MODULES nVF48 // defined in alphavmc/include/SiMod.h      
#define MAX_TREE_SIZE 2147483647 // max tree size

#define NUM_FPGA TTC_TA_inputs/128

extern bool gDoSamplesVF48;

int ShowMem(const char* label);
using namespace std;

// Global Variables
int  gRunNumber = 0; //Run number for loading files
int  outRunNumber = 0; //Output run number (incase using blinding tools)
bool gIsRunning = false;
bool gIsPedestalsRun = false;
bool gIsOffline = false;
bool gVf48disasm = false;
double gVf48eventTime = 0;
bool gIsCosmicRun = false;
bool gDelCosmic = false;
bool gCosmicUtils = false;
bool gEOS = false;
bool gWait = false;
bool gWaitForever = false;
Bool_t gPolyFit = kFALSE;

bool gBlindNumber = false;
bool gBlindDump = false;
TString gBlindDumpList[100];
Int_t gNBlindDumps=0;
bool gBlindLabview = false;
TString gBlindLabviewList[100];
Int_t gNBlindLabview=0;
bool gBlindSequence = false;

int gSisCounter = 0;
int gBadSisCounter = 0;
int gVertexCounter = 0;
int gLabVIEWCounter = 0;

Int_t gSiEventNum = 0;
Int_t gSiEventLast = 0;
Int_t gDisplayEvent = 0;
Int_t gAutoSaveEventDisplay = 0;
 // tStart_cpu = clock();
//  tStart_user = time(NULL);

clock_t tStart_cpu = clock();
time_t tStart_user = time(NULL);
   

//static 
unsigned long int gClock[NUM_SIS_MODULES] = {0}; // synchronize the clocks of all databanks
unsigned long int gvf48clock = 0; //Use in HandleVF48.cxx for calibration
Int_t gTerminateEventDisplay = 0;

Int_t gNHitsCut = 200;
#ifdef ALPHA1COMP
  #define  NUMSEQ 2
  TString SeqNames[NUMSEQ]={"PBAR","Mix"};
  enum {PBAR,MIX};
  enum {NOTADUMP,DUMP,EPDUMP};
#else
  #define NUMSEQ 4
TString SeqNames[NUMSEQ]={"cat","rct","atm","pos"};
  enum {PBAR,RECATCH,ATOM,POS};
  enum {NOTADUMP,DUMP,EPDUMP}; 
#endif
Int_t totalcnts[NUMSEQ]={0};
Int_t cSeq[NUMSEQ]={0}; // contatore del numero di sequenze, per tipo
//Add aditional type for 'other' dumps... Used only for Laser Experiment dumps so far
Int_t cID[NUMSEQ]={0}; //counter for assignment of unique sequencer ID's
Int_t sID[NUMSEQ]={0}; //counter for assignment of unique sequencer state ID's
Int_t cIDextra=0;
Bool_t g_flag_siliconEvents_ready_to_flush = kFALSE;

double nVASigma = 2.375;//3.125;
double nClusterSigma = 3.5;//nVASigma;
double pVASigma = 2.75;//3.75;
double pClusterSigma = 6;//pVASigma;
Double_t hitSigmaCut = 0.;//nVASigma;
Double_t hitThresholdCut = 99999;

//Track cuts (only used if customised)
Bool_t gCustomTrackCuts=kFALSE;
Double_t gHitSepCutPhi;
Double_t gHitSepCutZ;
Double_t gCorCut;
Double_t gChi2Cut;
Double_t gd0_trapCut;

//Vertex candidate cuts (only used if cusomised)
Bool_t gCustomVertCuts=kFALSE;
Double_t gVertRadCut;
Double_t gVertDCACut;

TSettings* gSettingsDB = NULL;

TUtilities* gUtils = NULL;

Bool_t gRecOff = kFALSE;
Bool_t gADCspecs=kFALSE;


Bool_t gEnableSuppression = kFALSE;
Bool_t gUseTimeRange = kFALSE;
Bool_t gForceFilename = kFALSE;
char CustomOutput[256];
Bool_t gForceStripsFile=kFALSE;
char CustomStripsFile[256];

Double_t starttime = 0;
Double_t stoptime = 0;
Double_t datasize = 0;
Double_t nhits = 0;
uint16_t thres = 0;
int nskipsupp = 1;
int nrunavg = 2;

TFile 				*gOutputFile = NULL;
TDirectory 			*gSISDirectory = NULL;

VirtualOdb	 		*gOdb = NULL;
VirtualOdb	 		*endOdb = NULL;
TSisEvent 			*gSisEvent = NULL;
TSisEvent           *gSisEventArray[64];

Bool_t              gWriteAlphaEventTree = kTRUE;
TAlphaEvent         *gAlphaEvent = NULL;
TTree                           *gAlphaEventTree = NULL;
TObjArray         *gAlphaEvents = NULL;


TSiliconEvent       *gSiliconEvent = NULL;
TTCEvent            *gTTCEvent = NULL;
TLabVIEWEvent       *gLabVIEWEvent = NULL;
TSeq_Event          *gSeq_Event = NULL;
TSeq_State          *gSeq_State = NULL;
TSequencerDriver    *gSeq_Driver = NULL;
TTree               *gSisTree = NULL;
TTree               *gSisTreeArray[65];
TTree               *gSiliconTree = NULL;
TTree               *gLabVIEWTree = NULL;
TTree               *gSeqTree = NULL;
TTree               *gSeqStateTree = NULL;
TTree               *gSeqDriverTree = NULL;
TTree               *gTTCTree = NULL;
TTree               *gAnalysisReportTree = NULL;
TTree               *gStripReportTree = NULL;
TTree               *gTimestampHelperTree = NULL;
TTree               *gSeqEventHelperTree = NULL;
// Sequencer stuff
SeqXML* gSeqXML=0;
TTree*  gSeqXMLTree=0;

Int_t               gEventID = 0;
std::vector<Int_t> gEventIDs;

TObjArray           *gSiliconEvents = NULL;
TObjArray           *gTTCEvents = NULL;
int32_t              gTTCEventID;
uint32_t              gTTCEventNoverflow[4]={0};
Bool_t               gTTCReadyToOverflow[4]={kFALSE};
double               gVF48RunTimeOffset = -999.;
TObjArray           *gADCTriggerEvents = NULL;
Int_t                gADCTriggerNumber = 0;

TVF48SiMap 			*gVF48SiMap = NULL;

  TSISChannels	    *gSISChannels = NULL; 
Int_t               gSISdiff =0;
Int_t               SISdiffPrev =0;


// TTree               *gSisDetTriggerTree = NULL;    // SIS Detector Triggers
// TSisEvent           *gSisDetTriggerEvent = NULL;

// PMT trees
TTree               *gSisPMTDEGTree = NULL;
TSisEvent  		    *gSisPMTDEGEvent = NULL; 
TTree               *gSisPMTTRAPTree = NULL;
TSisEvent  		    *gSisPMTTRAPEvent = NULL; 
TTree               *gSisPMTDOWNTree = NULL;
TSisEvent  		    *gSisPMTDOWNEvent = NULL; 

// char * gCFM_CAL_PATH;
char gSiRMSFilename[80];

//alphaStrips information:
Int_t gStripRun = 0;
TTree *gStripRunTree = NULL;
TObjString *galphaStrip_filename;


std::vector<TAbsUtil*> gAddons;

//Analysis Report Object
TAnalysisReport *gAnalysisReport = new TAnalysisReport();
TalphaStripsReport* gStripReport = NULL;

TTimestampHelper *TimeStampHelper = new TTimestampHelper();
TSeq_EventHelper* seqEventHelper= NULL;
// Local Variables
int EventCutoff = 0;

void VF48BeginRun();
void VF48EndRun();
void HandleVF48event(const VF48event *e);


static double GetTimeSec() {
  struct timeval tv;
  gettimeofday(&tv,NULL);
  return tv.tv_sec + 0.000001*tv.tv_usec;
}

class MyPeriodic : public TTimer {
public:
  typedef void (*TimerHandler)(void);

  int          fPeriod_msec;
  TimerHandler fHandler;
  double       fLastTime;
  bool         fInHandler;

  MyPeriodic(int period_msec,TimerHandler handler) {
    assert(handler != NULL);
    fPeriod_msec = period_msec;
    fHandler  = handler;
    fLastTime = GetTimeSec();
    fInHandler = false;
    Start(period_msec,kTRUE);
  }

  Bool_t Notify() {
    // kill recursive calls into Timer handlers
    if (fInHandler)
      return kTRUE;

    double t = GetTimeSec();
    //printf("timer notify, period %f should be %f!\n",t-fLastTime,fPeriod_msec*0.001);

    if (t - fLastTime >= 0.9*fPeriod_msec*0.001) {
      fInHandler = true;
      //printf("timer: call handler %p\n",fHandler);
      if (fHandler)
        (*fHandler)();

      fLastTime = t;
      fInHandler = false;
    }
    
    Reset();
    return kTRUE;
  }
  
  ~MyPeriodic() {
    TurnOff();
  }

};

// initialize clocks

time_t   gSaveTime  = 0;// a cosa serve?


unsigned long int    gExptStartClock[NUM_SIS_MODULES]={0};

// labview clock
double 	 glabviewtime = 0;


double labviewtime(int timestamp) {
  if (gSaveTime && glabviewtime == 0)
    glabviewtime = gSaveTime;
  else if (glabviewtime == 0 && gSaveTime ==0) {
    glabviewtime = (double)timestamp;
  }
  return (double)timestamp;
}


double clock2time(unsigned long int clock){
  const double freq = 10000000.0; // 10 MHz AD clk
  
 return clock/freq;
}

double clock2time(unsigned long int clock, unsigned long int offset ){
  const double freq = 10000000.0; // 10 MHz AD clk
  
  return (clock - offset)/freq;
}


void startRun(int transition,int run,int time) {

  gIsRunning = true;
  gRunNumber = run;
  gAnalysisReport->SetAlphaAnalysisExeDateBinary(AlphaAnalysisCompileTime);
  gAnalysisReport->SetSVNalphaAnalysisRevision(_AlphaAnalysisSVNRevision_);

  if (gBlindDump) gAnalysisReport->SetDumpBlinded(gNBlindDumps);
  if (gBlindLabview) gAnalysisReport->SetLabviewBlinded(gNBlindLabview);
  if (!gBlindNumber) 
  {
    outRunNumber=gRunNumber;
  }
  else
  {
    //gAnalysisReport->SetRunNumberBlinded(run);
    gAnalysisReport->SetRunNumberBlinded(1);
    cout <<"\nChecking new number has matching SIS database entry..."<<endl;
    TSISChannels* inSISChannels = new TSISChannels(gRunNumber);
    TSISChannels* outSISChannels = new TSISChannels(outRunNumber);
    
    for (Int_t i=0; i<NUM_SIS_MODULES*NUM_SIS_CHANNELS; i++)
    {
      TString in =inSISChannels->GetDescription( i, gRunNumber );
      TString out=outSISChannels->GetDescription( i, outRunNumber );
      //cout << in << " " << out << endl;
      if (in.CompareTo(out, TString::ECaseCompare::kExact)!=0)
      {
        cout <<"Error blinding run number: SIS channels for new number do not match original!"<<endl;
        cout <<"Please choose a new number in a similar range as the input number..."<<endl;
        cout <<"TSISChannels object uses an sqlite database that requires a runNumber"<<endl;
        exit(0);
      }
    }
    cout <<"\tGood. Chosen blinding number will not screw up SIS channel labels..."<<endl;
  }
  gSISChannels = new TSISChannels(outRunNumber);



  gIsPedestalsRun = gOdb->odbReadBool("/experiment/edit on start/Pedestals run");
#ifdef ALPHA1COMP
    printf("ALPHA_1 Version: %d silicon modules \n",nSil);
#else
    printf("ALPHA_2 Version: %d silicon modules \n",nSil);
#endif 

  printf("Begin run: %d, pedestal run: %d\n", gRunNumber, gIsPedestalsRun);
  
  ShowMem("startRun::entry");

  // e.g., don't reinitialize the clocks if loading a file that belongs to the same run	
  if (transition==0){
    // reinitialize the clocks
    gSaveTime = 0;	
    glabviewtime = 0;
    for (int i=0;i<NUM_SIS_MODULES;i++) gExptStartClock[i]=0;

  }    

  Long64_t maxTreeSize(MAX_TREE_SIZE);
  TTree::SetMaxTreeSize(maxTreeSize*64); //128 GB limit 
  
  char filename[80];

  const char* dir = getenv("TREEFILES");
  if (dir == NULL) {dir = ".";}

  TString FLAGS="";
  if (gIsCosmicRun) {FLAGS+="_IsCosmic";}
  if (gCosmicUtils) {FLAGS+="_CosmicUtil";}
//  if (nVASigma!=3.125) 
//    {FLAGS+="_nSigma"; FLAGS+=nVASigma; FLAGS+="_";} //if not default value
//  if (pVASigma!=3.75) 
//    {FLAGS+="_pSigma"; FLAGS+=pVASigma; FLAGS+="_";} //if not default value
  //if (gNHitsCut!=90) {FLAG+="_HitCut"; FLAG+=gNHitCut; FLAGS+="_";}
    
  if(!gEnableSuppression) {

    if(gIsOffline && !gSiEventNum) {
      sprintf(filename,"%s/%s%05d%s%s", dir, getenv("TREEPREFIX"), outRunNumber,(const char*)FLAGS, getenv("TREEPOSTFIX"));
    }
    else {
      sprintf(filename,"%s%s%05d%s.root", dir, getenv("TREEPREFIX"), outRunNumber,(const char*)FLAGS);    }	     
  }
    else { // test data suppression 
    //    sprintf(filename,"data/tree%05doffline.%03d.%02d.%02d.root",gRunNumber,thres,nskipsupp,nrunavg);
      sprintf(filename,"%s/zstree%05d%s", dir, outRunNumber, getenv("TREEPOSTFIX"));
  }
  
  if (gForceFilename)
  {
    if (CustomOutput[0]=='/') 
    {
      //cout << "leading slash found" << endl;
      sprintf(filename,"%s",CustomOutput);      
    }
    else
    {
      //cout << "no leading slash" <<endl;
      sprintf(filename,"%s/%s",dir,CustomOutput);
    }  
  }
  
  if (transition==0){
    gOutputFile = new TFile(filename,"RECREATE");
  }
  else {
    gOutputFile = new TFile(filename,"UPDATE");
  }      

  char dbName[255]; 
  sprintf(dbName,"%s/aux/main.db",getenv("RELEASE"));
  gSettingsDB = new TSettings(dbName,gRunNumber);


  //  cout << dbName << endl;

  if (1) {
    gUtils = new TUtilities();

    char name[200];

#ifdef USE_ALPHAVMC
    // Initialize geometry
    new TGeoManager("TGeo", "Root geometry manager");
    
    // load the material definitions
    TString dir = gSettingsDB->GetDetectorGeoDir();
    TString mat = gSettingsDB->GetDetectorMat( gRunNumber );
    sprintf(name,"%s%s%s",getenv("RELEASE"),dir.Data(),mat.Data());
    TAlphaGeoMaterialXML * materialXML = new TAlphaGeoMaterialXML();
    materialXML->ParseFile(name);
    delete materialXML;
    
    TAlphaGeoEnvironmentXML * environmentXML = new TAlphaGeoEnvironmentXML();
    TString env = gSettingsDB->GetDetectorEnv( gRunNumber );
    sprintf(name,"%s%s%s",getenv("RELEASE"),dir.Data(),env.Data());
    environmentXML->ParseFile(name);
    delete environmentXML;
    
    TString det = gSettingsDB->GetDetectorGeo( gRunNumber );
    sprintf(name,"%s%s%s",getenv("RELEASE"),dir.Data(),det.Data() );
    printf(" det: %s\n",name);
    TAlphaGeoDetectorXML * detectorXML = new TAlphaGeoDetectorXML();
    detectorXML->ParseFile(name);
    delete detectorXML;
    
    
    gGeoManager->CloseGeometry();
#endif

    sprintf(name,"%s%s%s",getenv("RELEASE"),gSettingsDB->GetVF48MapDir().Data(),gSettingsDB->GetVF48Map(gRunNumber).Data());
    printf("name: %s\n",name);
    gVF48SiMap = new TVF48SiMap(name);
  }

  if(!gSisTree) {
    // initialise trees
    gSisTree = new TTree("gSisTree","Sis Tree"); 

    gSisEvent = new TSisEvent();
    gSisTree->Branch("SisEvent","TSisEvent",&gSisEvent,16000,1);
   

    int nsisch = NUM_SIS_MODULES*NUM_SIS_CHANNELS;
    for( Int_t i = 0; i<nsisch; i++) { // SIS1  + SIS2
      char name[80], title[80];
      sprintf(name,"gSisTree%02d",i); 
      sprintf(title,"Sis Tree %02d",i);
      gSisTreeArray[i] = new TTree(name,title);
      gSisEventArray[i] = new TSisEvent();
      gSisTreeArray[i]->Branch("SisEvent","TSisEvent",&gSisEventArray[i],16000,1);
    }

    gAnalysisReportTree = new TTree("gAnalysisReport","Analysis Report Tree");
    gAnalysisReportTree->Branch("gAnalysisReport","TAnalysisReport",&gAnalysisReport,16000,1);
    
    gStripReportTree = new TTree("gStripReport","alphaStrips Report Tree");
    gStripReportTree->Branch("gStripReport","TalphaStripsReport",&gStripReport,16000,1);
    
    gTimestampHelperTree = new TTree("TimestampHelper","Timestamp Helper Tree");
    gTimestampHelperTree->Branch("TimestampHelper","TTimestampHelper",&TimeStampHelper,16000,1);
    
    gSeqEventHelperTree = new TTree("SeqEventHelper","Sequencer Event Helper Tree");
    gSeqEventHelperTree->Branch("SeqEventHelper","TSeq_EventHelper",&seqEventHelper,16000,1);
    
    gAlphaEvent = new TAlphaEvent();
    
    gAlphaEventTree = new TTree("gAlphaEventTree","Alpha Event Tree");
    gAlphaEventTree->Branch("AlphaEvent","TAlphaEvent",&gAlphaEvent,16000,1);
    gAlphaEventTree->Branch("eventID",&gEventID,"eventID/I");
    gAlphaEvent->DeleteEvent();
    if(gAlphaEvent)
      delete gAlphaEvent;
    gAlphaEvent = NULL;
    
    gSiliconTree = new TTree("gSiliconTree","Silicon Tree");
    gSiliconEvent = new TSiliconEvent();
    gSiliconTree->Branch("SiliconEvent","TSiliconEvent",&gSiliconEvent,16000,1);
    gSiliconTree->Branch("eventID",&gEventID,"eventID/I");
    gSiliconEvent->Delete();

    gLabVIEWTree = new TTree("gLabVIEWTree","LabVIEW Tree");
    gLabVIEWEvent = new TLabVIEWEvent();
    gLabVIEWTree->Branch("LabVIEWEvent","TLabVIEWEvent",&gLabVIEWEvent,16000,1);

    gSeqTree = new TTree("gSeqTree","Sequencer Event Tree");
    gSeq_Event = new TSeq_Event();
    gSeqTree->Branch("SequenceEvent","TSeq_Event",&gSeq_Event,16000,1);
    
    gSeqStateTree = new TTree("gSeqStateTree","Sequencer State Tree");
    gSeq_State = new TSeq_State();
    gSeqStateTree->Branch("SequenceState","TSeq_State",&gSeq_State,16000,1);
    
    gSeqDriverTree = new TTree("gSeqDriverTree","Sequencer Driver Tree");
    gSeq_Driver = new TSequencerDriver();
    gSeqDriverTree->Branch("SequenceDriver","TSequencerDriver",&gSeq_Driver,16000,1);

    // tree with XML sequencer stuff !!!
    gSeqXMLTree = new TTree("gSeqXMLTree","Sequencer XML Tree");
    //    gSeqXML = new SeqXML();
    gSeqXMLTree->Branch("seqXML","SeqXML",&gSeqXML,16000,1);

    gTTCTree = new TTree("gTTCTree","TTC Tree");
    gTTCEvent = new TTCEvent();
    gTTCTree->Branch("TTCEvent","TTCEvent",&gTTCEvent,16000,1);
     

    //new tree with the alphaStrip run used and the file name
    gStripRunTree = new TTree("gStripRunTree","Strip Tree"); 
    gStripRunTree->Branch("stripRun",&gStripRun,"gStripRun/I");
    gStripRunTree->Branch("stripRunFile","TObjString",&galphaStrip_filename,16000,1);

    
    // event containers
    gSiliconEvents = new TObjArray();
    //Set entries to max number of SiliconEvents before forced flush
    //gSiliconEvents->Expand(2000);
    
    gAlphaEvents = new TObjArray();

    gADCTriggerEvents = new TObjArray();
    gADCTriggerNumber = 0;
    
    gTTCEvents = new TObjArray();
    

  }


  if (transition==0){
    for (int iseq = 0; iseq<NUMSEQ; iseq++){
      cSeq[iseq]=0; 
      cID[iseq]=0;
    }
  }
  
  VF48BeginRun(); 

  ShowMem("startRun::exit");
}

void TTCEndRun()
{
  // create TTC tree from gTTCEvents container
  
  TADCTriggerEvent* ADCTriggerEvent;
  Int_t ADCtriggers=gADCTriggerEvents->GetEntriesFast();
  for( Int_t i=0; i < gTTCEvents->GetEntriesFast(); i++ )
  {
    gTTCEvent = (TTCEvent*) gTTCEvents->At(i);
    if( ( (int64_t)gTTCEvent->GetTTCCounter() ) <=  ADCtriggers )
    {
      ADCTriggerEvent = (TADCTriggerEvent*) gADCTriggerEvents->At( gTTCEvent->GetTTCCounter() );
      if ( ADCTriggerEvent != NULL )
      {
        gTTCEvent->SetRunTime( ADCTriggerEvent->GetRunTime() );
        gTTCEvent->SetExptTime( ADCTriggerEvent->GetExptTime() );
        gTTCEvent->SetExptNumber( ADCTriggerEvent->GetExptNumber() );
      }
      else 
      {
        //cout << "ADCTriggerEvent == NULL " << i << endl;
      }
    }
    else //bad events need to remain so that sil_event links are correct
    {
      //cout << "gTTCEvent->GetTTCCounter() ) <= gADCTriggerEvents->GetEntries() at " << i << endl;
      //gTTCEvent->ClearTTCEvent(); //nuke corrupt TTC events
    }
    gTTCTree->Fill();
    delete gTTCEvent;
  }//loop over gTTCEvents
  /*
  // check contents of the TTC tree
  if(0)
    {      
      for( Int_t i=0; i < gTTCTree->GetEntries(); i++ )
        {
          printf( "i = %d ", i );
          gTTCTree->GetEntry(i);
          gTTCEvent->Print();     
        }
    }
  */
}

Double_t Get_Online_RunTime_of_SequencerEvent(TSeq_Event *seqEvent)
//This functino should only be called at endRun!!!
//Its been lifted to TRootUtils Get_RunTime_of_SequencerEvent
{
  Bool_t NotStartOrStop = kFALSE;
  Int_t startChannel=-1;
  if (!(seqEvent->GetSeq().CompareTo(TString("cat"))))
  {
    if (!seqEvent->GetEventName().CompareTo(TString("startDump")))
      startChannel = gSISChannels->GetChannel("SIS_PBAR_DUMP_START");
    else
      startChannel = gSISChannels->GetChannel("SIS_PBAR_DUMP_STOP");
  }
  else if (!(seqEvent->GetSeq().CompareTo(TString("rct"))))
  {
    if (!seqEvent->GetEventName().CompareTo(TString("startDump")))
      startChannel = gSISChannels->GetChannel("SIS_RECATCH_DUMP_START");
    else
      startChannel = gSISChannels->GetChannel("SIS_RECATCH_DUMP_STOP");
  }
  else if (!(seqEvent->GetSeq().CompareTo(TString("atm"))))
  {
    if (!seqEvent->GetEventName().CompareTo(TString("startDump")))
      startChannel = gSISChannels->GetChannel("SIS_ATOM_DUMP_START");
    else if (!seqEvent->GetEventName().CompareTo(TString("stopDump")))
      startChannel = gSISChannels->GetChannel("SIS_ATOM_DUMP_STOP");
    else
    {
      NotStartOrStop = kTRUE;
      startChannel = gSISChannels->GetChannel("SEQLASERPULSE");
    }
  }
  else if (!(seqEvent->GetSeq().CompareTo(TString("pos"))))
  {
    if (!seqEvent->GetEventName().CompareTo(TString("startDump")))
      startChannel = gSISChannels->GetChannel("SIS_POS_DUMP_START");
    else if (!seqEvent->GetEventName().CompareTo(TString("stopDump")))
      startChannel = gSISChannels->GetChannel("SIS_POS_DUMP_STOP");
  }
  else
  {
    Error("Get_RunTime_of_SequencerEvent", "Unknown Sequencer %s", seqEvent->GetSeq().Data());
    return -999.;
  }
  Int_t ID = (seqEvent->GetID()) / 2;
  if (NotStartOrStop)
  {
    ID = seqEvent->GetID();
  }

  TTree *tree = gSisTreeArray[startChannel];

  TSisEvent *sis_event = new TSisEvent();
  tree->SetBranchAddress("SisEvent", &sis_event);
  tree->GetEntry(ID);
  Double_t run_time = sis_event->GetRunTime();
  if (run_time<0) std::cout<<"Bad SIS time of dump ("<<seqEvent->GetDescription()<<" "<<seqEvent->GetEventName()<<" in "<<seqEvent->GetSeq()<<")"<<std::endl;
  delete sis_event;
  return run_time;
}

void endRun_TSeq_EventHelper()
{
  std::cout <<"Building TSeq_EventHelper..."<<std::endl;
  TSeq_Event* seqEvent = new TSeq_Event();
  gSeqTree->SetBranchAddress("SequenceEvent", &seqEvent);
  Int_t sequencerEntries=gSeqTree->GetEntries();
  for (Int_t i=0; i<sequencerEntries; i++)
  {
    gSeqTree->GetEntry(i);
    Double_t time=Get_Online_RunTime_of_SequencerEvent(seqEvent);
    seqEventHelper->AddTSeq_Event(seqEvent,time);
  }
  delete seqEvent;
}




void endRun(int transition,int run,int time_int)
{
  gIsRunning = false;
  gRunNumber = run;
  if (!gBlindNumber) outRunNumber = gRunNumber;

  gAnalysisReport->SetRunNumber(outRunNumber);
  TimeStampHelper->SetRunNumber(gRunNumber);
  gAnalysisReport->PrintCompilationDate(1);
  
  std::cout <<"Building TTimestampHelper..."<<std::endl;
  TSisEvent* sis_event=new TSisEvent();
  Int_t FlushCounter=0;
  for( Int_t i = 0; i<NUM_SIS_MODULES*NUM_SIS_CHANNELS; i++)// SIS1  + SIS2
  {
    gSisTreeArray[i]->SetBranchAddress("SisEvent",&sis_event);
    //TimeStampHelper->ReserveSISEvents(gSisTreeArray[i]->GetEntries(),i);
    for (Int_t j=0; j<gSisTreeArray[i]->GetEntries(); j++)
    {
      gSisTreeArray[i]->GetEntry(j);
      TimeStampHelper->AddSISTimestamp(sis_event->GetRunTime(),i);
      FlushCounter++;
      if (FlushCounter>1000000)  //Write to file every 1M entries?
      {
        //TimeStampHelper->Print();
        gTimestampHelperTree->Fill();
        TimeStampHelper->ClearVectors();
        FlushCounter=0;
      }
    }
  }
  delete sis_event;
  

  
//ODB
   gAnalysisReport->SetStartTimeBinary(gOdb->odbReadUint32("/Runinfo/Start time binary"));
   if (endOdb!=NULL)
   {gAnalysisReport->SetStopTimeBinary(endOdb->odbReadUint32("/Runinfo/Stop time binary"));}
   else
   {gAnalysisReport->SetStopTimeBinary(0);}
   gAnalysisReport->SetCosmicTriggerBool(gOdb->odbReadBool("/Experiment/edit on start/Cosmic_Run"));
   
  ShowMem("endRun::entry");
 if (!gRecOff  ) {
    VF48EndRun();
    if (gAnalysisReport->GetGoodVF48Events()>0)	{
      TTCEndRun();
    }
  }
  
  gAnalysisReport->SetSISCounters(gSisCounter,gBadSisCounter,gSISdiff,gClock[0]-gExptStartClock[0],gClock[1]-gExptStartClock[1]);
  gStripRunTree->Fill();//filling AlphaStrip Tree just once.
  double cputime = (double)(clock() - tStart_cpu)/CLOCKS_PER_SEC;
  gAnalysisReport->SetCPUtime(cputime);
  double usertime = difftime(time(NULL),tStart_user);
  gAnalysisReport->SetUserTime(usertime);

  gAnalysisReportTree->Fill();
  gStripReportTree->Fill();
  gTimestampHelperTree->Fill();

  seqEventHelper=new TSeq_EventHelper(gRunNumber);
  endRun_TSeq_EventHelper();
  gSeqEventHelperTree->Fill();
  
  if(gOutputFile) gOutputFile->Write();
  
  if (gStripReport)
  {
    if (gStripReport->GetRunNumber()>0)
      gStripReport->Print();
    else cout <<"No valid Strip Report to Print"<<endl;
    delete gStripReport;
  }
  if (gStripReportTree)    delete gStripReportTree;
  gAnalysisReport->Print();
  delete gAnalysisReport;
  delete TimeStampHelper;
  delete seqEventHelper;
  if (gAnalysisReportTree) delete gAnalysisReportTree;
  if (gTimestampHelperTree) delete gTimestampHelperTree;
  if (gSeqEventHelperTree) delete gSeqEventHelperTree;
  if (gStripRunTree)       delete gStripRunTree;
  
  if (gSISChannels)        delete gSISChannels;
  if (gSettingsDB)         delete gSettingsDB;
  if (gSisTree)            delete gSisTree;
    int nsisch = NUM_SIS_MODULES*NUM_SIS_CHANNELS;
    for( Int_t i = 0; i<nsisch; i++ ) //AO <<<<<< ---- 
    {
      delete gSisTreeArray[i];
      delete gSisEventArray[i];
    }
  if(gSiliconTree)         delete gSiliconTree;
 

  if(gLabVIEWTree)         delete gLabVIEWTree;
  if(gTTCTree)             delete gTTCTree;
  if(gSisEvent)            delete gSisEvent;
  if(gLabVIEWEvent)        delete gLabVIEWEvent;
  
  if(gSiliconEvents->GetEntries()>0)
  {
    std::cout << gSiliconEvents->GetEntries() << " Silicon Events not written to file (no time stamp)" << std::endl;
    gSiliconEvents->SetOwner(kTRUE);
    gSiliconEvents->Delete();
  }
  //if(gSiliconEvent)        delete gSiliconEvent;
  /*if(gTTCEvents)
  { 
    std::cout << gTTCEvents->GetEntries() << " TTC events not written to file (no time stamp)" << std::endl;
    gTTCEvents->SetOwner(kTRUE);
    gTTCEvents->Delete();
  }*/
  //if(gTTCEvent)            delete gTTCEvent;
  
  if (gADCTriggerEvents)
  {
    gADCTriggerEvents->SetOwner(kTRUE);
    gADCTriggerEvents->Delete();
    delete gADCTriggerEvents;
  }
  if (gOdb)                delete gOdb;
  if (endOdb)              delete endOdb;
  if(gVF48SiMap)           delete gVF48SiMap;
  if(gOutputFile)          delete gOutputFile;

  ShowMem("endRun::exit");

  //printf("End of run %d\n",run);
  //std::cout << "=================================================================================\n\n\n" << std::endl;
   
}


void HandleSeq2( char * sequdata, int size ){
  // Sequencer XML parsing interface
  TString sequheader="";
  for(int i = 0;i<size && (*sequdata!=60);i++) { //get the first line ; char 60 is "<"      //  && (*sequdata!=10) && (*sequdata!=0)(*sequdata!=13) 
    sequheader+=*sequdata++;
  }
  sequheader+=0;
  
  TDOMParser *fParser = new TDOMParser();
  fParser->SetValidate(false);
	
  int bufLength = strlen(sequdata);
  char*buf = (char*)malloc(bufLength);
  memcpy(buf, sequdata, bufLength);
  
  for (int i=0; i<bufLength; i++)
    if (!isascii(buf[i]))
      buf[i] = 'X';
    else if (buf[i] == 0x1D)
      buf[i] = 'X';

  int parsecode = fParser->ParseBuffer(buf,bufLength);
    
  if (parsecode < 0 ) {
    std::cerr << fParser->GetParseCodeMessage(parsecode) << std::endl;
    return;
  }  
  free (buf);
  
  TXMLNode * node = fParser->GetXMLDocument()->GetRootNode();

  
  gSeq_Driver->Clear();
  gSeq_Driver->Parse(node,gSeq_Driver->DigitalMap ,"DOConfig"    ,"DOCfg"     ,"BitNum"    ,"id");
  gSeq_Driver->Parse(node,gSeq_Driver->AnalogueMap,"AOChnList"   ,"AOChn"     ,"name"      ,"physChn");
  gSeq_Driver->Parse(node,gSeq_Driver->TriggerMap ,"HVElectrodes","HVElect"   ,"ElectNum"  ,"HVBitNum");
  gSeq_Driver->Parse(node,gSeq_Driver->HVMap      ,"TrigInConfig","TrigInLine","BitNum"    ,"id");
  
    
  SeqXML* mySeq = new SeqXML(node);
  delete fParser;
  
  Int_t iSeqType;
  // PBAR, MIX, POS definiti in un enum, precedentemente
#ifdef ALPHA1COMP
    if( strcmp( ((TString)mySeq->getSequencerName()).Data(), SeqNames[PBAR].Data()) == 0 ) 
      iSeqType = PBAR;
    else if( strcmp( ((TString)mySeq->getSequencerName()).Data(), SeqNames[MIX].Data()) == 0 ) 
      iSeqType = MIX;
#else
    if( strcmp( ((TString)mySeq->getSequencerName()).Data(), SeqNames[PBAR].Data()) == 0 ) 
      iSeqType = PBAR;
    else if( strcmp( ((TString)mySeq->getSequencerName()).Data(), SeqNames[RECATCH].Data()) == 0 ) 
      iSeqType = RECATCH;
    else if( strcmp( ((TString)mySeq->getSequencerName()).Data(), SeqNames[ATOM].Data()) == 0 ) 
      iSeqType = ATOM;
    else if( strcmp( ((TString)mySeq->getSequencerName()).Data(), SeqNames[POS].Data()) == 0 ) 
      iSeqType = POS;
#endif
  
  else {
    iSeqType = -1;
    std::cout << "unknown sequencer name: " << ((TString)mySeq->getSequencerName()).Data() <<" seq names "<<SeqNames[PBAR].Data()<<"  "<<SeqNames[RECATCH].Data()<<"  "<<SeqNames[ATOM].Data() <<"  " << SeqNames[POS].Data() <<std::endl;
    //   assert(0);

  }
  cSeq[iSeqType]++;

  gSeq_Driver->SeqName=mySeq->getSequencerName().Data();
  gSeq_Driver->SeqNum=iSeqType;
  
  gSeqDriverTree->Fill();

  TIter myChains((TObjArray*)mySeq->getChainLinks(), true);
  SeqXML_ChainLink *cl;

  while((cl = (SeqXML_ChainLink *) myChains.Next()))
  {
    SeqXML_Event *event;
    TIter myEvents(cl->getEvents());
    while((event = (SeqXML_Event *) myEvents.Next()))
    {
      //Turning off default printing of dumps Sept 2017 JTKM
      //event->Print("");
      gSeq_Event->SetSeq( mySeq->getSequencerName() );
      gSeq_Event->SetSeqNum(cSeq[iSeqType]); 
      if (event->GetNameTS()=="startDump" || event->GetNameTS()=="stopDump")
      {
        gSeq_Event->SetID( cID[iSeqType]++ );
      }
      else
      {
        gSeq_Event->SetID(cIDextra++);
      } //Assign to an additional sequencer counter... 
        //for use with general purpose laser dumps
        //(the dump marker may use a differnt SIS channel, like SEQLASERPULSE)
      gSeq_Event->SetEventName( event->GetNameTS() );
      if (gBlindDump)
      {
        Bool_t BlankDump=kFALSE;
        for (Int_t i=0; i<gNBlindDumps; i++) //Loop over input forbidden dump names
        {
          if (event->GetDescription().Contains(gBlindDumpList[i] ,
                              TString::ECaseCompare::kIgnoreCase) ==1) 
          //If forbidden string is contained in dump string
          {
            //cout << gBlindDumpList[i] <<" = " <<  event->GetDescription() << endl;
            TString Blank="BLINDDUMP";
            BlankDump=kTRUE;
            Blank+=i;
            gSeq_Event->SetDescription(Blank);
            break;
          }
          else
          {
            //cout << gBlindDumpList[i] <<" != " <<  event->GetDescription() << endl;
          }
        }
        if (!BlankDump) gSeq_Event->SetDescription( event->GetDescription() ); // No need to blind this dump
      }
      else 
      {
        gSeq_Event->SetDescription( event->GetDescription() );
      }
      if (!gBlindSequence) gSeq_Event->SetSeqHeader( sequheader );
      gSeq_Event->SetonCount( event->GetCount() );
      gSeq_Event->SetonState( event->GetStateID() );
      gSeqTree->Fill();
    }
    SeqXML_State* state;
    TIter myStates(cl->getStates());
    while ((state= (SeqXML_State *) myStates.Next()))
    {

      gSeq_State->SetSeq( mySeq->getSequencerName() );
      gSeq_State->SetSeqNum(cSeq[iSeqType]);
      gSeq_State->SetID(sID[iSeqType]++);
      gSeq_State->SetState( state->getID() );
      gSeq_State->SetTime( state->getTime() );
      
      //AO
      if (state->GetAOi()->size())
      {
         AnalogueOut* AO=new AnalogueOut;
         AO->steps=state->getLoopCnt();
         AO->AOi=*state->GetAOi();
         AO->AOf=*state->GetAOf();
         AO->PrevState=-999;
         gSeq_State->AddAO(AO);
      }
      
      //DO
      if (state->GetDO()->size())
      {
         DigitalOut* DO=new DigitalOut;
         DO->Channels=*state->GetDO();
         gSeq_State->AddDO(DO);
      }
      
      //Trigger unset for now
      
      gSeq_State->SetComment(*state->getComment() );
      
      gSeqStateTree->Fill();
    }
/*
    Seq_DriverConsts* drvC=mySeq->getSeq_DriverConsts();
    
    TSequencerDriver* dr=new TSequencerDriver();
    dr->DriverVer      = drvC->getDriverVers();
    dr->NumDO          = drvC->getNumDOLines();
    dr->NumTrig        = drvC->getNumTrigInLines();
    dr->NumA0          = drvC->getNumAOTrigLines();

    
    SeqXML_IOConfig* AOC=mySeq->getIOConfig();
    std::vector<SeqXML_AOChn*>* achns= AOC->GetAOChns();
    for (int i=0; i<achns->size(); i++)
    {
      SeqXML_AOChn* ch=achns->at(i);
      std::cout<<"ANA NAME:"<<ch->getChnName();//<<std::endl;
      std::cout<<"ANA NUM: "<<ch->getNum();//<<std::endl;
      std::cout<<"ANA PHYS:"<<ch->getPhysChn()<<std::endl;
  
    }
  */  
  /*  std::vector<SeqXML_DOChn*>* dchns= AOC->GetDOChns();
    for (int i=0; i<dchns->size(); i++)
    {
      SeqXML_DOChn* ch=dchns->at(i);
      std::cout<<"DIG NAME:"<<ch->getChnName()<<std::endl;
      std::cout<<"DIG NUM: "<<ch-> getId()<<std::endl;
      //std::cout<<"PHYS:"<<getDescription()<<std::endl;
      
    }
    
*/    

  }
  gSeqXML=mySeq;
  gSeqXMLTree->Fill();
  delete mySeq;
}


void HandleSIS(int numChan,int* size,void** ptr){

  uint32_t* sis[NUM_SIS_MODULES];

  for (int j=0; j<NUM_SIS_MODULES; j++){
    sis[j] = (uint32_t*)ptr[j]; // get pointers
  }

  // clocks
  
  //  for (int i=0;i<NUM_SIS_MODULES; i++) gClock[i] = 0; // synchronize the clocks of all databanks
  int iclk[NUM_SIS_MODULES] = {0};
  static std::vector<unsigned long int> expstart;


  // find databank containing adc triggers (from io32)
  int clkchan[NUM_SIS_MODULES]={-1};
  int vf48clkchan=gSISChannels->Get_20MHz_VF48clk();
  // Find the 10MHz clock channels for each sis
  for (int j=0; j<NUM_SIS_MODULES; j++) {
    clkchan[j] = gSISChannels->Get10MHz_clk(j);
    if(gExptStartClock[j]==0 && sis[j]) {
      gExptStartClock[j]=sis[j][clkchan[j]];  //first clock reading
      //printf(" sisnum %d  gExptStartClock %lu ", j,gExptStartClock[j]);
    } 
    //find  the channel for the 'new experiment' trigger in each SIS databank.  
    iclk[j] = gSISChannels->GetSeqAtomStart(j);
    if(size[j] && iclk[j]>0) { 
      uint32_t* sisclk = sis[j];
      unsigned long int clk = gClock[j];
      for (int i=0; i<size[j]; i+=numChan, sisclk+=numChan) {
        clk += sisclk[clkchan[j]];
        //printf("bankstart: module=%d bank=%d sis= %d, clk 0x%lx, TS %lu TS0 %lu\n",j,i, sisclk[gSISChannels->GetSeqAtomStart(j)%32], clk, clk,gClock[j]/10000000);
        if( sisclk[gSISChannels->GetSeqAtomStart(j)%32]>0 ) {
          //       printf("quenchstart: sis %d ,%d  is %d, clk 0x%lx,clk %lu, TS %lf TS0 %lf\n",j, gSISChannels->GetSeqAtomStart(j)%32, sisclk[gSISChannels->GetSeqAtomStart(j)%32], clk,clk, clock2time(clk,gExptStartClock[j]),clock2time(clk)); 
          expstart.push_back(clk);//expstart is the time from the start of the recatch signal
          break;
        }
      }
    }
  }
  //std::cout <<size[0]<<"\t"<<vf48clkchan<<"\t"<<gvf48clock<<std::endl;
  //if (size[0]) gvf48clock+=sis[0][vf48clkchan];
  
  for (int j=0; j<NUM_SIS_MODULES; j++){ // loop over databanks
  int i;
    for (i=0; i<size[j]; i+=numChan, sis[j]+=numChan) {
      unsigned long int clock = sis[j][clkchan[j]]; // num of 10MHz clks
      gClock[j] += clock;
      // look for start clock and seq start.
      int theExptNumber = 0;
      unsigned long int theExptStartClock = gExptStartClock[j];
      //AO gets the correct chain number when expstart is defined
      for (int k=expstart.size(); k>0; k--){
        if (expstart.at(k-1)<=gClock[j]){
          theExptStartClock = expstart.at(k-1);
          theExptNumber = k;
          // printf("Bankno %d , gClock %lu , theExptStartClock %lu theExptStartClock %f \n",j, gClock[j], theExptStartClock, clock2time(theExptStartClock)); 
          break;
        } 
      }
      if (j==gSISChannels->GetBNKADC()){
        if(gVF48RunTimeOffset<0. ) {// 1st VF48 SIS trigger of the run
          if( sis[j][gSISChannels->GetIADC()] || sis[j][gSISChannels->GetIO32_NOBUSY()] ){
            printf("DEBUG gVF48RunTimeOffse checking: %d %lu %lu  %10.6lf   %10.6lf \n", j, gClock[j],gExptStartClock[j],clock2time( gClock[j],gExptStartClock[j] ),clock2time( gClock[1-j],gExptStartClock[1-j] ));
            gVF48RunTimeOffset = clock2time( gClock[j],gExptStartClock[j] ); //Seq time of 1st Si trigger
          }
        }
        if( sis[j][gSISChannels->GetIADC()] ) {
          for( UInt_t iu=0; iu<sis[j][gSISChannels->GetIADC()]; iu++ ) {
            gADCTriggerNumber++;
            TADCTriggerEvent* ADCTriggerEvent = new TADCTriggerEvent(); 
            ADCTriggerEvent->SetSIS_ADC_TrigNumber( gADCTriggerNumber );
            ADCTriggerEvent->SetExptNumber( theExptNumber ); 
            ADCTriggerEvent->SetExptTime(clock2time(gClock[j], theExptStartClock));
            // printf("ExptTime %f",clock2time(gClock[j], theExptStartClock)); 
            ADCTriggerEvent->SetRunTime(clock2time(gClock[j],gExptStartClock[j])); 
            gADCTriggerEvents->Add( ADCTriggerEvent ); 
          }          
        }
      }
      if (j==0)
        gvf48clock+=sis[0][vf48clkchan];
      double exptTime(-1.);
      for (int kk=0; kk<numChan; kk++){
        
        int index = kk+j*numChan;
        
        if(sis[j][kk] && gSisEvent) {	
 
          gSisEvent->ClearSisEvent();
          gSisEvent->SetChannel(index);
          gSisEvent->SetCountsInChannel((Int_t)sis[j][kk]);
          gSisEvent->SetClock(gClock[j]);
          //printf("bankno %d, gClock %lu theExptStartClock %lu RunTime %f\n",
          // j,gClock[j], theExptStartClock, clock2time(gClock[j],gExptStartClock[j])  );
          gSisEvent->SetRunTime(clock2time(gClock[j],gExptStartClock[j]));
          gSisEvent->SetRunNumber(outRunNumber); 
          gSisEvent->SetExptNumber(theExptNumber);
          if(theExptNumber>0 ) {
            exptTime = clock2time(gClock[j], theExptStartClock);
            //printf("bankno %d, gClock %lu theExptStartClock %lu exptTime %f\n",
            //j,gClock[j], theExptStartClock,   exptTime);
          }
          gSisEvent->SetExptTime( exptTime );
          gSisEvent->SetCounters(gLabVIEWCounter,gVertexCounter);
          
          gSisTree->Fill();
          
          gSisEventArray[index]->ClearSisEvent();
          gSisEventArray[index]->SetChannel(index);
          gSisEventArray[index]->SetCountsInChannel((Int_t)sis[j][kk]);
          gSisEventArray[index]->SetClock(gClock[j]);
          gSisEventArray[index]->SetRunTime(clock2time(gClock[j],gExptStartClock[j]));  
          gSisEventArray[index]->SetRunNumber(outRunNumber);
          gSisEventArray[index]->SetExptNumber(theExptNumber);
          if( theExptNumber>0 )  exptTime = clock2time(gClock[j], theExptStartClock);
          gSisEventArray[index]->SetExptTime( exptTime );
          gSisEventArray[index]->SetCounters(gLabVIEWCounter,gVertexCounter);
          gSisTreeArray[index]->Fill();
        }
        
      }
      gSisCounter++;
    }
  }//end of loop over databanks
  // N.B.: not filling TS Trigger Weed 
}





void HandleTTC(int* sizettc, uint16_t** ptrttc){

  const int kBufsize = 100;
  static int wptr[NUM_FPGA];
  static uint16_t buf[NUM_FPGA][kBufsize];

  static uint32_t xtime;
  static uint32_t xevent[NUM_FPGA];

  for (int ifpga=0; ifpga<NUM_FPGA; ifpga++) {
    int size = sizettc[ifpga];
    const uint16_t* ptr = ptrttc[ifpga];


    if (0)  {
      printf("FPGA %d, size %d, data 0x", ifpga, size);
      for (int i=0; i<size; i++)
        printf(" %04x", ptr[i]);
      printf("\n");
    }

    
    for (int i=0; i<size; i++){
      buf[ifpga][wptr[ifpga]++] = ptr[i];
      if (ptr[i] == 0xaaaa) {
        int j=0;
        for (; j<wptr[ifpga]; j++)
          if (buf[ifpga][j] == 0x5555)
            break;
        
        j++;
              
        TTCEvent* ttcEvent = new TTCEvent(); 
        //I assume sequential TTCEvents with non missing
    //    if  (buf[ifpga][j+0]==65535) gTTCReadyToOverflow[ifpga]=kTRUE;
    //    if (buf[ifpga][j+0]==0 && gTTCReadyToOverflow[ifpga])
    //    {
          //cout << "Run:"<< gRunNumber<<"Overflowing "<<ifpga <<endl;
    //      gTTCEventNoverflow[ifpga]++;
    //      gTTCReadyToOverflow[ifpga]=kFALSE;
    //    }

    //I may be a future bugfix, if there are missig TTC events, this *should* catch over flows properly
        if  (buf[ifpga][j+0]>60000 && buf[ifpga][j+0]<62000 && !gTTCReadyToOverflow[ifpga]) gTTCReadyToOverflow[ifpga]=kTRUE;
        if (buf[ifpga][j+0]>0 && buf[ifpga][j+0]<1000 && gTTCReadyToOverflow[ifpga])
        {
		cout << "Overflowing "<<ifpga <<endl;
		gTTCEventNoverflow[ifpga]++;
		gTTCReadyToOverflow[ifpga]=kFALSE;
	}
	ttcEvent->SetTTCCounter ( ((65536)*(gTTCEventNoverflow[ifpga]))+((uint32_t)buf[ifpga][j+0]) );
	//        cout<<ttcEvent->GetTTCCounter() <<endl;
        ttcEvent->SetTTCTimestamp( buf[ifpga][j+1] | (buf[ifpga][j+2]<<16) );

        for (int k=0; k<8; k++) 
          ttcEvent->SetLatch( buf[ifpga][j+4+k] , k );

        ttcEvent->SetMult( buf[ifpga][j+12] );
        ttcEvent->SetMult2( buf[ifpga][j+13] );
        
        if (/*xevent[ifpga]==0 ||*/ ttcEvent->GetTTCCounter()<xevent[ifpga])
          xevent[ifpga] = ttcEvent->GetTTCCounter();
        
        ttcEvent->SetTTCNEvent( ttcEvent->GetTTCCounter() - xevent[ifpga] );
              
        if (xtime==0 || ttcEvent->GetTTCTimestamp()<xtime)
          xtime = ttcEvent->GetTTCTimestamp();
        
        ttcEvent->SetEventTime( (ttcEvent->GetTTCTimestamp()-xtime)/20.0e6 ); // convert 20 MHz clock into seconds.
        ttcEvent->SetFPGA( ifpga );
        ttcEvent->SetID(gTTCEventID);
        gTTCEventID++;
        gTTCEvents->Add( ttcEvent );

        wptr[ifpga] = 0;
      }
    } 
  }
}


void HandleMidasEvent(TMidasEvent& event) {
  int eventId = event.GetEventId();
  
  if ((eventId==6)&&(gIsRunning==true)){ // LabVIEW data
    
    //retrieve time
    int time = event.GetTimeStamp();
    double labvtime = labviewtime( time );
    
    for (int ibank = 0; ibank < event.SetBankList(); ibank++) {
      void *ptr;
      const char * bankname = (const char*)event.GetBankList();
      Bool_t BlankBank=kFALSE;
      if (gBlindLabview)
      {
		
        for (Int_t i=0; i<gNBlindLabview; i++) //Loop over input forbidden dump names
        {
          //cout <<gBlindLabviewList[i] << " : " << bankname <<endl;
          if (gBlindLabviewList[i].CompareTo((bankname) ,
          TString::ECaseCompare::kIgnoreCase) ==0) 
          //If forbidden string is the same in the bank string
          {
            BlankBank=kTRUE;
            break;
          }
        }
      }
      if (BlankBank) {continue;}
      
      int size = event.LocateBank(event.GetData(),&bankname[ibank],&ptr);
      for(int isize = 0; isize < size; isize++){
        gLabVIEWEvent->ClearLabVIEWEvent();
        double* value = (double*) ptr;
        gLabVIEWEvent->SetCounters( gSisCounter, gVertexCounter );
        gLabVIEWEvent->SetLabVIEWEvent(bankname,isize,value[isize],labvtime-glabviewtime);
        gLabVIEWTree->Fill();
      }
      gLabVIEWCounter++;
    }
  }
  else if((eventId==8)&&(gIsRunning==true)) { // Sequencer 
    void *ptr;
    int size = event.LocateBank(event.GetData(),"SEQ2",&ptr);
    char *sequdata = (char*)ptr;
    HandleSeq2( sequdata, size ); 
  }
/*
  else if (eventId==10) { //SY127 data
	//  int time = event.GetTimeStamp();
	double labvtime = labviewtime(event.GetTimeStamp()) ;
//	  std::cout <<"BOING! SY127 DATA HERE!" << labvtime << std::endl;
	  void *ptr;
	    
    for (int ibank = 0; ibank < event.SetBankList(); ibank++) {
      void *ptr;
//      	std::cout << "\t ibank: " << ibank << "\t" << event.GetBankList() << endl;
      	const char * bankname = (const char*)event.GetBankList();
		int size = event.LocateBank(event.GetData(),&bankname[ibank],&ptr);
		for(int isize = 0; isize < 2*size; isize++){
		  std::cout<< isize << " time " << labvtime-glabviewtime << std::endl;
		  //Long32_t* value32 = (Long32_t*) ptr;
		  Long64_t* value64 = (Long64_t*) ptr;
		  float* valuef = (float*) ptr;
		//cout << value32 << endl;
//		cout << value64[isize] << endl;
//		cout << valuef[isize] << endl;
		//cout << valuef[isize] << endl;
		}

  }
  
  }
  */
  
 // Beginning of making a Sy127 tree
  else if (eventId == 11) { // VME bulk data
    if(glabviewtime == 0 && gSaveTime == 0) // Used to sync clocks offline
      glabviewtime = event.GetTimeStamp();	
    

    void* ptr[NUM_SIS_MODULES];
    int size[NUM_SIS_MODULES];
    
    char bankname[] = "MCS0";
    int totalsize = 0;
    Int_t SISdiff =0;

    for (int i=0; i<NUM_SIS_MODULES; i++){
      bankname[3] = '0' + i; 
      size[i] = event.LocateBank(NULL,bankname,&ptr[i]);
      totalsize+=size[i];
      (i==0)? SISdiff+=size[i]:SISdiff-=size[i]; 
      assert( size[i] % NUM_SIS_CHANNELS == 0);// check SIS databank size is a multiple of 32
    }
    gSISdiff+=SISdiff;
    if (size[0] != size[1])   {
      //      if (SISdiffPrev !=0 && SISdiff+ SISdiffPrev !=0 ){
      if (SISdiff+ SISdiffPrev !=0 )  {
      //   printf("Unpaired SIS buffers %d  %d  diff %d \n", size[0]/NUM_SIS_CHANNELS, size[1]/NUM_SIS_CHANNELS, gSISdiff/NUM_SIS_CHANNELS);
        gBadSisCounter++;
      }
      SISdiffPrev+=SISdiff; 
    }

    if (totalsize>0) { // Don't bother with SIS when searching for si events
 
    if(!gSiEventNum) HandleSIS(NUM_SIS_CHANNELS,size,ptr);
    
    else 
	printf("size0: %d, size1 %d, gSiEventNum: %d \n",  size[0],  size[1], gSiEventNum); 
  }

    

    void* ptrttc[NUM_FPGA];
    int sizettc[NUM_FPGA];
    bool foundfpga=false;
    for (int i=0; i<NUM_FPGA; i++) {
      char bankname[5];
      bankname[0] = 'T';
      bankname[1] = 'E';
      bankname[2] = 'L';
      bankname[3] = '0' + i;
      bankname[4] = 0;
      sizettc[i] = event.LocateBank(NULL,bankname,&ptrttc[i]);
      
      if (sizettc[i]>0)
        foundfpga=true;
    }

    if ( foundfpga && !gSiEventNum && !gRecOff){ // Don't bother with TTC when searching for si events
      HandleTTC(sizettc,(uint16_t**) ptrttc); 
    }
    
    gVf48eventTime = event.GetTimeStamp();
    
    for (int i=0; i<NUM_VF48_MODULES; i++) {
      char bankname[5];
      void *ptr;
      bankname[0] = 'V';
      bankname[1] = 'F';
      bankname[2] = 'A';
      bankname[3] = '0' + i;
      bankname[4] = 0;
      int size = event.LocateBank(NULL,bankname,&ptr);
      if (size > 0 && (!gRecOff||gADCspecs))
      {
	  // printf("VF48 bank %s size %d\n", bankname, size);
          //          UnpackVF48(i, size, ptr, gVf48disasm,false); //new Konstantin function
          //          vfu->fBadDataCount = 0;
          vfu->UnpackStream(i, ptr, size);
          while (1) 
            {
              VF48event* e = vfu->GetEvent();
              if (!e) break;
              HandleVF48event(e);
              
            }
        }
    }
  }  
}

void eventHandler(const void*pheader,const void*pdata,int size){
  TMidasEvent event;
  memcpy(event.GetEventHeader(),pheader,sizeof(TMidas_EVENT_HEADER));
  event.SetData(size, (char*)pdata);
  event.SetBankList();
  HandleMidasEvent(event);
}
static bool gEnableShowMem = false;


int CheckMidasFileOnEOS(TString filename)
{
  if( (strncmp(gSystem->HostName(),"alphadaq",8)==0) ) //AND I am NOT an alphadaq* machine (a safety system to stop deletion of files)
  {
    std::cerr <<"This machine is blacklisted from using --EOS flag"<<std::endl;
    gEOS=false;
    return -99;
  }
  if (filename.Contains("midasdata/"))
    filename.Remove(0,10);
  TString EOSdir="/eos/experiment/alpha/midasdata/";
  EOSdir+=filename;
  TString LocalPath=getenv("MIDASDIR");
  LocalPath+="/";
  LocalPath+=filename;
  TString EOScheck="eos ls -l ";
    EOScheck+=EOSdir;
    EOScheck+=" | awk '{print $5}'";
  Int_t InitialSize=gSystem->GetFromPipe(EOScheck).Atoi();
  if (InitialSize>0)
  {
    cout<<filename<< " found and not empty"<<endl;
    gSystem->Sleep(5000);
    Int_t SizeAfterWait=gSystem->GetFromPipe(EOScheck).Atoi();
    if (InitialSize==SizeAfterWait)
    {
      cout <<"File size confirmed to be :" << SizeAfterWait <<endl;
      return 1;
    }
    else
    {
      cout <<"File still being updated... try again later"<<endl;
      return 0;
    }
  }
  else
  {
    cout <<"File "<<filename<< " not found" <<endl;
    return -1;
  }
}

int CopyMidasFileFromEOS(TString filename, Int_t AllowedRetry=5)
{
	
  if(  /* (strncmp(gSystem->HostName(),"alphacpc",8)==0) ||*/ //AND I am NOT an alphacpc* machine (a safety system to stop deletion of files)
      (strncmp(gSystem->HostName(),"alphadaq",8)==0) ) //AND I am NOT an alphadaq* machine (a safety system to stop deletion of files)
  {
    std::cerr <<"This machine is blacklisted from using --EOS flag"<<std::endl;
    gEOS=false;
    return -99;
  }
  if (filename.Contains("midasdata/"))
    filename.Remove(0,10);
  TString EOSdir="/eos/experiment/alpha/midasdata/";
  EOSdir+=filename;
  TString LocalPath=getenv("MIDASDIR");
  LocalPath+="/";
  LocalPath+=filename;
  TString EOScheck="eos ls ";
    EOScheck+=EOSdir;
    EOScheck+="*";


  
  assert(EOSdir.EndsWith(".mid.gz"));
  TMidasFile f;
  Int_t status=-99;
  Bool_t tryOpen = f.Open(LocalPath);
  if (tryOpen)
  {
    std::cout <<filename <<" found locally, not fetching from EOS"<<std::endl;
    return -1;
  }
  if (gSystem->GetFromPipe(EOScheck).Sizeof()!=1 ) //If file exists, 
  {
    std::cout << "Midas file not found, --EOS enabled, hostname matches compatibility list... fetching file from EOS" << std::endl;  //Don't check the first file... I want an error printed if there is no file
    TString EOScopy="eos cp ";
      EOScopy+=EOSdir;
      EOScopy+=" ";
      EOScopy+=getenv("MIDASDIR");
      EOScopy+="/";
      status=gSystem->Exec(EOScopy);
    if (status!=0 )
    {
      if (AllowedRetry<=0) exit(555);
      std::cout <<"Fetching failed with error: "<<status<<std::endl;
      gSystem->Sleep(5000);
      std::cout <<"Trying again ("<<AllowedRetry<<" more attempt(s) until abort"<<std::endl;
      CopyMidasFileFromEOS(filename,AllowedRetry-1);
    }
    else
    {
      //fetchedFromEOS=true;
    }
    tryOpen = f.Open(filename);
    std::cout << "Status:"<<status<<" File fetched successfully from EOS, I will remove the midas file when done with it" << std::endl;
    f.Close();
    return status;
  }
  else
  {
    std::cout<< std::endl <<filename<< " not found on EOS" << std::endl;
    return -2;
  }
}

int CopyMidasFileFromEOS(Int_t runNumber, Int_t subIndex, Int_t AllowedRetry=5)
{
  TString filename="run";
  filename+=runNumber;
  filename+="sub";
  if (subIndex<10)
    filename+="0000";
  if (subIndex<100)
    filename+="000";
  if (subIndex<1000)
    filename+="00";
  if (subIndex<10000)
    filename+="0";
  filename+=subIndex;
  return CopyMidasFileFromEOS(filename,AllowedRetry);
}

bool WaitForFile(TMidasFile f, const char* fname)
{
  Int_t WaitTime=0;
  std::cout <<"Waiting for file " << fname<<" to appear in path..."<<std::endl;
  while (WaitTime<(60*60*3)) // wait up 3 hours
  {
    if (gEOS)
    {
      if (CheckMidasFileOnEOS(fname)==1)
      {
        CopyMidasFileFromEOS(fname, 5);
      }
      else
      {
        gSystem->Sleep(30*1000);
        WaitTime++;
      }
    }
    bool tryOpen = f.Open(fname);
    if (tryOpen) {
      std::cout <<"File found"<<std::endl;
      gSystem->Sleep(1000); //Wait a little time so file is not empty
      return true;
    }
    gSystem->Sleep(1000); //If not there, wait 
    if (!gWaitForever)
      WaitTime++;
    if ((WaitTime%20)==0)  std::cout <<"\b-";//<<WaitTime<<std::endl;
    if ((WaitTime%20)==5)  std::cout <<"\b\\";//<<WaitTime<<std::endl;
    if ((WaitTime%20)==15)  std::cout <<"\b|";//<<WaitTime<<std::endl;
    if ((WaitTime%20)==20)  std::cout <<"\b/";//<<WaitTime<<std::endl;
  }
  std::cout <<"wait timed out after "<<WaitTime <<" seconds"<<std::endl;
  return false;
}

int ProcessMidasFile(TApplication*app,const char*fname) {

  TMidasFile f;
  bool tryOpen = f.Open(fname);
  
  bool fetchedFromEOS[100];
  for (Int_t i=0; i<100; i++) fetchedFromEOS[i]=false;
  TString EOSdir="/eos/experiment/alpha/midasdata/";
   //cout<<gSystem->HostName()<<endl;
  
  if (gEOS && !(gWait || gWaitForever) ) //and I am not an alphadaq* machine (a safety system to stop deletion of files)
  {
    TString midasFileName="";
    for (uint i = strlen(fname)-23; i < strlen(fname); i++) //I assume the midas filename is 23 chars long...
    {
      midasFileName+=fname[i];
    }
    Int_t status=CopyMidasFileFromEOS(midasFileName);
    if (status==0) fetchedFromEOS[0]=true;
    tryOpen = f.Open(fname);
  }
  // parse the filename to extract the current "sub"-index for that run
  int midindex = 0;
  int urindex = 0;
  std::string sfname(fname);
  char substr[200];
  char basestr[200];
  char sufstr[200];
  char gMidasPath[200];
  strncpy(gMidasPath,&fname[0],(sfname.find("sub"))-5-4);
  gMidasPath[(sfname.find("sub"))-5-4]='\0';
  std::cout <<"Midas file path: "<<gMidasPath <<std::endl;
  strncpy(basestr,fname,sfname.find("sub")+3);
  strncpy(substr,&fname[sfname.find("sub")+4],5);
  strcpy(sufstr,&fname[sfname.find("sub")+4+5]);
  midindex=atoi(substr);
  urindex = midindex;
  
  if (!tryOpen && gWait) {
    tryOpen=WaitForFile(f,fname);
    if (tryOpen) tryOpen=f.Open(fname);
  }
  if (!tryOpen) {
    printf("Cannot open input file \"%s\", error %d (%s)\n", fname, f.GetLastErrno(), f.GetLastError());
    return -1;
  }

  // Initalize the clock2time() function
  
  int MidasEvent11=0;
  while (1) {
    TMidasEvent event;
    if (!f.Read(&event)) {
      printf("Cannot read event from file, error %d (%s)\n", f.GetLastErrno(), f.GetLastError());
      break;
    }
    
    int eventId = event.GetEventId();
    
    if ((eventId & 0xFFFF) == 0x8000 ) { // begin run
      // Load ODB contents from the ODB XML file      
      if (midindex==urindex){       
        if (gOdb)
          delete gOdb;
        gOdb = new XmlOdb(event.GetData(),event.GetDataSize());
        startRun(0,event.GetSerialNumber(),0);
      }
    }
    else if ((eventId & 0xFFFF) == 0x8001) {           // end run
		
      endOdb = new XmlOdb(event.GetData(),event.GetDataSize()); //load end of run odb data
        
      f.Close();          //          std::cout << "closing " << fname << std::endl;
      char currentfname[200];
      sprintf(currentfname,"%s/run%05dsub%05d.mid.gz",getenv("MIDASDIR"),gRunNumber,midindex);
      if (fetchedFromEOS[midindex] && gEOS) {
		  TString DeleteFile="rm -v ";
		  DeleteFile+=currentfname;
		  gSystem->Exec(DeleteFile); 
	  }
      
      
      midindex++;
      char nextfname[200];
      //      printf( "basestr %s sufstr %s \n", basestr,sufstr);
      // AO: the original line screws up with C++11 inserting an extra non-printing char
      //      sprintf(nextfname,"%sb%05d.%s",basestr,midindex,sufstr);
      sprintf(nextfname,"%s/run%05dsub%05d.mid.gz",gMidasPath,gRunNumber,midindex);
      tryOpen = f.Open(nextfname);
      // std::cout << "opening " << nextfname << std::endl;
      
      EOSdir="/eos/experiment/alpha/";
      //std::cout<<gSystem->HostName()<<endl;
      
      char nextFileName[200];
      sprintf(nextFileName,"run%05dsub%05d.mid.gz",gRunNumber,midindex);
      if (!tryOpen && gWait && !endOdb->odbReadUint32("/Runinfo/Stop time binary")) {
        if (gEOS) 
        tryOpen=WaitForFile(f,nextfname);
        if (tryOpen) {
          tryOpen=f.Open(nextfname);
          fetchedFromEOS[midindex]=true;
        }
      }
      if (gEOS && !gWait ) 
      {
        Int_t status=CopyMidasFileFromEOS((TString)nextFileName);
        tryOpen = f.Open(nextfname);
        if (status==0) fetchedFromEOS[midindex]=true;
        else {
          if (!tryOpen)
          {
            printf("... run%05dsub%05d.mid.gz not found on EOS... ending run\n",gRunNumber,midindex);
            endRun(0,gRunNumber,0);
            break;
          }
          //std::cout<< std::endl <<nextfname<< " already exists... assuming intentional... not fetching from EOS, I wont delete it either (safty feature)" << std::endl << std::endl;
          fetchedFromEOS[midindex]=false;
        }
      }

      if (!tryOpen){
        printf("Checking for next input file \"%s\", midindex %d, returned %d (%s)\n", nextfname,midindex, f.GetLastErrno(), f.GetLastError());
        endRun(0,gRunNumber,0);
        break;
      }
      else
      {
        printf("Opened next input file \"%s\", midindex %d\n", nextfname,midindex);
      }
    }
    else {
      event.SetBankList();
      HandleMidasEvent(event);
    }

    // count the number of processed events	
    if(eventId==11){
      MidasEvent11++;
    if(MidasEvent11%1000 == 0) printf("Number of midas events processed: %d\n",MidasEvent11);
    } 
    if(gEnableShowMem) printf("event11no  %d, eventId %d\n",MidasEvent11, eventId);

    if ((EventCutoff!=0)&&(MidasEvent11>=EventCutoff)) {
      printf("Reached event %d, exiting loop.\n",MidasEvent11);
      endRun(0,gRunNumber,0);
      f.Close();
      break;
    }
  }//while(1)
  
  return 0;
}

#ifdef HAVE_MIDAS

void MidasPollHandler() {
  if (!(TMidasOnline::instance()->poll(0)))
    gSystem->ExitLoop(); 
}

int ProcessMidasOnline(TApplication*app) {
   
   TMidasOnline *midas = TMidasOnline::instance();

   int err = midas->connect(NULL,NULL,"alphaAnalysis"); 
   assert(err == 0);

   gOdb = midas;

   midas->setTransitionHandlers(startRun,endRun,NULL,NULL);
   midas->registerTransitions();

   /* reqister event requests */

   midas->setEventHandler(eventHandler);       // call 'eventHandler' when an event comes in
   midas->eventRequest("SYSTEM",-1,-1,(1<<1));

   /* fill present run parameters */

   gRunNumber = gOdb->odbReadInt("/runinfo/Run number");
      
   if ((gOdb->odbReadInt("/runinfo/State") == 3))
     startRun(0,gRunNumber,0);

   printf("Startup: run %d, is running: %d, is pedestals run: %d\n",gRunNumber,gIsRunning,gIsPedestalsRun);
   
   /*---- start main loop ----*/
   MyPeriodic tm(100,MidasPollHandler);

   //loop_online();
   app->Run(kTRUE);

   /* disconnect from experiment */
   midas->disconnect();

   return 0;
}

#endif




int ShowMem(const char* label) {
  if (!gEnableShowMem)
    return 0;

  FILE* fp = fopen("/proc/self/statm","r");
  if (!fp)
    return 0;

  int mem = 0;
  fscanf(fp,"%d",&mem);
  fclose(fp);

  if (label)
    printf("memory at %s is %d\n", label, mem);

  return mem;
}

void help() 
{
  // <<<<<< REWRITE

  printf("\nALPHA Analysis usage:\n");

  printf("\n./alphaAnalysis.exe [-eMaxEvents] [-m] [-p9090] [--vf48disasm] [file1 file2 ...]\n");

  printf("\n");

  printf("\t-h: Print this help message\n");

  printf("\n** MIDAS options:\n");
  printf("\t-p: Start the midas histogram server on specified tcp port (for use with roody)\n");

  printf("\n** Debugging options:\n");
  printf("\t-m: Enable memory leak debugging\n");
  printf("\t--vf48disasm: disassemble the VF48 data banks (very verbose)\n");

  printf("\n** Navigation options:\n");
  printf("\t-e<N>: limit the number of VF48 events to be read to N (only works in offline mode)\n");
  printf("\t-se<N> -ss<M>: start from N-th TSiliconEvent, stop at M-th TSiliconEvent\n");
  printf("\t--usetimerange=<timestart>:<timestop> : Analyze only Silicon Events in time range (in seconds). Es. --usetimerange=516.089:517.687\n");

  printf("\n** Reconstruction options:\n");
  printf("\t--recoff: disable VF48 unpacking and vertex reconstruction\n");
  printf("\t--iscosmic: cosmic (straight line) reconstruction\n");
  printf("\t--psigma<N>: set hit threshold at P sigma; default = %g\n",pVASigma);
  printf("\t--nsigma<N>: set hit threshold at N sigma; default = %g\n",nVASigma);
  printf("\t--pclustersigma<N>: set threshold of Pside hit cluster; default = %g\n",pClusterSigma);
  printf("\t--nclustersigma<N>: sets threshold at Nside hit cluster; default = %g\n",nClusterSigma);
   printf("\t--nhitscut<N>: reject events with more than N hits; default = %d\n",gNHitsCut);
  printf("\t--polyfit: Enable use of polynomial pedestal fitting (needs to have --polyfit alphaStrips\n");
  printf("\t--settrackcuts <fHitSepCutPhi> <fHitSepCutZ> <fCorCut> <fChi2Cut> <fd0_trapCut> \n"); 
  printf("\t--setvertcuts <fVertRadCut> <fVertDCACut>: Set vertex candidate cuts\n");
  printf("Unused:\n");
  printf("\t--hitsignifiance<N>: set hit threshold on combined sigma thereshold (between 0 and 1)\n");
  printf("\t--hitthreshold<N>: set hit threshold for single module\n");
  printf("\n** Data quality options:\n");
  printf("\t--adcspecs: creates occupancy histograms\n");
  printf("\t--tuneVF48: creates correlation histograms between vf48 edge samples\n");
  printf("\t--cosmicutil: performs analysis related to cosmic runs, requires --iscosmic\n");
  printf("\t--delhelix: Experimental framework under development for the removal of hits that conform with 6 hit helix.\n");
  printf("\t--enablesupp: Enable zero suppression test routines\n");
  
  
  printf("\t--EOS: Enable automatic fetching then deletion of midas files from EOS (not for online analysis)\n");
  printf("\t--wait: Wait for midas file to appear, I time out if I wait too long");
  printf("\t--waitforever: Wait for midas file to appear, I do not time out if I wait too long");

  printf("\n** Output file options:\n");
  printf("\t--output=tree12345custom.root: Custom output file\n");
  printf("\t\tif proceeding character is \"/\" the path is absolute, otherwise will be relative to %s\n",getenv("TREEFILES"));
  printf("\t--stripsfile=alphaStrips1235custom.root: Custom alphaStrips file\n");
  printf("\t--savespace: Dont write TAlphaEvents to save disk space\n");
  
  printf("\n** Blinding options:\n");
  printf("\t--blind-number<nnnnn>: overwrite run number in output file (please keep 5 digit number)\n"); 
  printf("\t--blind-dump \"<Dump Name>\": blank out dump name with matching string (partial match ok)\n");
  printf("\t--blind-labview<243F>: dont fill trees with this labview channel\n"); 
  printf("\t--blind-sequence: dont fill sequence trees (will disable run identification by trappingHash in PlotDumps\n");

  printf("\n** Display options:\n");
  printf("\t-ev<N>,-es<N>,-et<N>: enable event display (-es: autosave, -et: autosave & terminate); start from N-th TSiliconEvent\n");
  printf("\t--enablesidaqctrl: enable waveform display\n");


  //  printf("\t--enablesupp<threshold>: Enable zero suppression test routines. Es. --enablesupp60 or --enablesupp \n");
  //  printf("\t--nskipsupp<nskip>: when zero suppression is enabled, compare A(i) w/ A(i-nskip). Default --nskipsupp1\n");
  //  printf("\t--nrunavg<navg>: when zero suppression is enabled, compare A(i) to running average over navg elements. Default --nrunavg2\n");

  printf("\n");
  printf("Example1: analyze online data: alphaAnalysis.exe\n");
  printf("Example2: analyze existing data: alphaAnalysis.exe /data/alpha/current/run00500.mid.gz\n");
  printf("\n");

  exit(1);

  //  printf("\t-g: Enable graphics display when processing data files\n");

}

TApplication* xapp;

// Main function call

int main(int argc, char *argv[])
{
  printf("***************************\n"); 	 
  printf("     ALPHA  Analysis      \n"); 	 
  printf("***************************\n\n");

  setbuf(stdout,NULL);
  setbuf(stderr,NULL);

  signal(SIGILL,  SIG_DFL);
  signal(SIGBUS,  SIG_DFL);
  signal(SIGSEGV, SIG_DFL);

  //tStart_cpu = clock();
  //tStart_user = time(NULL);
   
  // constructs an array for the commandline arguments
  std::vector<std::string> args;
  for (int i=0; i<argc; i++) {
    args.push_back(argv[i]);
    if (strcmp(argv[i],"-h")==0 || strcmp(argv[i],"--help")==0)
      help();
  }
  
  /*
  // <<<<<<<---- why?
  if(gROOT->IsBatch()) {
    printf("Cannot run in batch mode\n");
    return 1;
  }
  */
;

  TApplication *app = new TApplication("alphaAnalysis", &argc, argv);
  extern TApplication* xapp;
  xapp = app;
  
  int  tcpPort = 0;

  gErrorIgnoreLevel = 2000; // (0)>=Print,(1000)>=Info,(2000)>=Warning,(3000)>=Error
  printf("Output information level set to %d \n", gErrorIgnoreLevel );
  printf("where (0)>=Print,(1000)>=Info,(2000)>=Warning,(3000)>=Error \n\n" );

  // parse command line options and sets the relevant global flags
  for (unsigned int i=1; i<args.size(); i++) { // loop over the commandline options
    
    const char* arg = args[i].c_str();
    // printf("argv[%d] is %s\n",i,arg);
	
    if (strncmp(arg,"-ev",3)==0) { // Event display
      gSiEventNum = atoi(arg+3);
      gAutoSaveEventDisplay = 0;
      gDisplayEvent = 1;
    }
    else if (strncmp(arg,"-es",3)==0){ // Autosave event displays
      gSiEventNum = atoi(arg+3);
      gAutoSaveEventDisplay = 1;
      gDisplayEvent = 1;
    }
    else if (strncmp(arg,"-se",3)==0){ // Start from nth event
      gSiEventNum = atoi(arg+3);
    }
    else if (strncmp(arg,"-ss",3)==0){ // Stop at nth event
      gSiEventLast = atoi(arg+3);
    }
    
    // Terminate TDisplay after the event is saved
    else if (strncmp(arg,"-et",3)==0){ // Autosave event displays
      gSiEventNum = atoi(arg+3);
      gAutoSaveEventDisplay = 1;
      gTerminateEventDisplay = 1;
    }

    if (strncmp(arg,"-e",2)==0)  // Event cutoff flag (only applicable in offline mode)
      EventCutoff = atoi(arg+2);
    else if (strncmp(arg,"-m",2)==0) // Enable memory debugging
      gEnableShowMem = true;
    else if (strncmp(arg,"-p",2)==0) // Set the histogram server port
      tcpPort = atoi(arg+2);
    else if (strcmp(arg,"--vf48disasm")==0)
      gVf48disasm = true;

    else if (strcmp(arg,"--nsigma")==0)
      {
        nVASigma = atof(args[i+1].c_str());
        std::cout <<"nSigma:"<< nVASigma << std::endl;
      }
    else if (strcmp(arg,"--psigma")==0)
      {
        pVASigma = atof(args[i+1].c_str());
       std::cout <<"pSigma:"<< pVASigma << std::endl;
      }
    else if (strcmp(arg,"--nclustersigma")==0)
      {
        nClusterSigma = atof(args[i+1].c_str());
        std::cout <<"nClusterSigma:"<< nClusterSigma << std::endl;
      }
    else if (strcmp(arg,"--pclustersigma")==0)
      {
        pClusterSigma = atof(args[i+1].c_str());
        std::cout <<"pClusterSigma:"<< pClusterSigma << std::endl;
      }
    else if (strcmp(arg,"--hitsignifiance")==0)
      hitSigmaCut = atof(args[i+1].c_str());
    else if (strcmp(arg,"--hitthreshold")==0)
      hitThresholdCut = atof(args[i+1].c_str());
    else if (strcmp(arg,"--settrackcuts")==0)
    {
      gCustomTrackCuts=kTRUE;
      gHitSepCutPhi=atof(args[i+1].c_str());
      gHitSepCutZ   = atof(args[i+2].c_str());
      gCorCut       = atof(args[i+3].c_str());
      gChi2Cut      = atof(args[i+4].c_str());
      gd0_trapCut   = atof(args[i+5].c_str());
      std::cout <<"Custom Track Cuts:"<<std::endl<<
      "  HitSepCutPhi:"<< gHitSepCutPhi<<std::endl<<
      "  HitSepCutZ:"<<gHitSepCutZ <<std::endl<<
      "  CorCut:"<<gCorCut      <<std::endl<<
      "  Chi2Cut:"<<gChi2Cut    <<std::endl<<
      "  d0_trapCut:"<<gd0_trapCut<<std::endl;
    }
    else if (strcmp(arg,"--setvertcuts")==0)
    {
	  gCustomVertCuts=kTRUE;
      gVertRadCut=atof(args[i+1].c_str());
      gVertDCACut=atof(args[i+2].c_str());
      std::cout <<"Custom Vert Candidate Cuts:" <<std::endl<<
      "  VertRadCut:"<< gVertRadCut <<std::endl<<
      "  VertDCACut:"<< gVertDCACut <<std::endl;
	}
    else if (strcmp(arg,"--savespace")==0)
    {
      std::cout << " Not write TAlphaEvents to save disk space" <<std::endl;
      gWriteAlphaEventTree=kFALSE;
    }

   
    else if (strcmp(arg,"--nhitscut")==0)
      gNHitsCut = atoi(args[i+1].c_str());


    else if (strcmp(arg,"--recoff")==0)
      gRecOff = kTRUE;
    else if (strcmp(arg,"--iscosmic")==0)
      gIsCosmicRun = true;
    else if (strcmp(arg,"--delhelix")==0)
      gDelCosmic = true;

    else if (strcmp(arg,"--adcspecs")==0){
      TAbsUtil* util = new TOccupancyUtil("TOccupancyUtil", "draw occupancy plots"); 
      gAddons.push_back(util);
      gADCspecs = true;
   }
    else if (strcmp(arg,"--tuneVF48")==0) {
      TAbsUtil* util = new TDoTuneVF48Util("TDoTuneVF48Util", "draw plots of correlations between strips"); 
      gAddons.push_back(util);
    }
    else if(strcmp(arg,"--cosmicutil")==0) {
    TAbsUtil* util = new TCosmicUtil("TCosmicUtils","Cosmic Run Analyser");
    gCosmicUtils= true;
    gAddons.push_back(util);
    }
    else if (strcmp(arg,"--samplesVF48")==0) {
      gDoSamplesVF48 = kTRUE; }

    else if (strncmp(arg,"--usetimerange=",15)==0){
      gUseTimeRange=kTRUE;
      sscanf(arg,
             "--usetimerange=%lg:%lg",
             &starttime,&stoptime);
      std::cout<< "selecting time range  " << starttime << " : " << stoptime << std::endl;
    }

    else if (strncmp(arg,"--enablesidaqctrl",17)==0){
      TAbsUtil* util = new SiDAQControl("SiDAQControl", "show waveforms"); 
      gAddons.push_back(util);
      gDisplayEvent = 1;
    }
   
    else if (strncmp(arg,"--enablesupp",12)==0){
       
 gEnableSuppression = kTRUE;     
      TAbsUtil* util = new TZeroSuppUtil("TZeroSuppUtil", "test and simulation of zero suppression in firmware, w/ nskip=0, navg=8, thres from odb"); 

      //util->SetOdb(gOdb);
      ((TZeroSuppUtil*)util)->lastSampleBug=true;
      ((TZeroSuppUtil*)util)->Navg=8;
      gAddons.push_back(util);
      
      thres =  atoi(arg+12);
      printf("AAAAAAaaaa %d",thres);

    }
    /*
    else if (strncmp(arg,"--nskipsupp",11)==0){
      nskipsupp =  atoi(arg+11);
    }
    else if (strncmp(arg,"--nrunavg",9)==0){
      nrunavg =  atoi(arg+9);
    }
    */
	else if (strncmp(arg,"--EOS",9)==0)
      gEOS=true; 
    else if (strncmp(arg,"--waitforever",13)==0)
    {
      gWait=true; // does not return
      gWaitForever=true;
    }
    else if (strncmp(arg,"--wait",6)==0)
      gWait=true; // does not return

    else if (strncmp(arg,"--output=",9)==0)
      {
        gForceFilename=kTRUE;
        sscanf(arg,
             "--output=%s",
             CustomOutput);
        cout <<"Forcing output to: "<<CustomOutput<<endl;
      }
    else if (strncmp(arg,"--stripsfile=",13)==0)
      {
        gForceStripsFile=kTRUE;
        sscanf(arg,
             "--stripsfile=%s",
             CustomStripsFile);
        cout <<"Forcing strip file to: "<<CustomStripsFile<<endl;
      }
    else if (strncmp(arg,"--polyfit",9)==0)
      {
        gPolyFit=kTRUE;
        std::cout <<"Enabling polyfit..."<<std::endl;
      }
    else if (strncmp(arg,"--blind-number",14)==0)
      {
        gBlindNumber=kTRUE;
        outRunNumber=atoi(arg+14);
        cout <<"Blinding run to number: "<<outRunNumber<<endl;

      }
    else if (strncmp(arg,"--blind-dump=",13)==0)
      {
        gBlindDump=kTRUE;
        gBlindDumpList[gNBlindDumps]=(arg+13);
        printf("Blinding dump: \"%s\"\n",gBlindDumpList[gNBlindDumps].Data());
        gNBlindDumps++;
      }
    else if (strncmp(arg,"--blind-labview=",16)==0)
      {
        gBlindLabview=kTRUE;
        gBlindLabviewList[gNBlindLabview]=(arg+16);
        printf("Blinding labview bank: \"%s\"\n",gBlindLabviewList[gNBlindLabview].Data());
        gNBlindLabview++;
      }
    else if (strncmp(arg,"--blind-sequence",16)==0)
      {
        printf("Blind Sequence data\n");
        gBlindSequence=kTRUE;
      }
    else if (strncmp(arg,"-commands",9)==0)
      help(); // does not return
    else if (
             arg[0] == '-' && 
             (strncmp(arg,"-se",3)!=0) && 
             (strncmp(arg,"-ss",3)!=0)
             )
      help(); // does not return
  }
  gAnalysisReport->SaveGlobalVars( EventCutoff, gEnableShowMem, gVf48disasm, nVASigma, pVASigma, gNHitsCut, gRecOff, gIsCosmicRun, gADCspecs, gCosmicUtils, gDoSamplesVF48, gUseTimeRange, starttime, stoptime, gEnableSuppression, gEOS);
  /* 
  // OBSOLETE
  if (tcpPort > 0)
    StartMidasServer(tcpPort);
  */

  XmlServer* xmlServer = NULL;
  if (tcpPort)  {
    xmlServer = new XmlServer();
    xmlServer->SetVerbose(true);
    xmlServer->Start(tcpPort);
    //      xmlServer->Export(gOnlineHistDir, gOnlineHistDir->GetName());
  }
  

  gIsOffline = false;
  
  // parameters without flags are treated as file names
  for (unsigned int i=1; i<args.size(); i++) {
    const char* arg = args[i].c_str();
    
    if (arg[0] != '-' && !isdigit( arg[0] ) )  {  
      gIsOffline = true;
      ProcessMidasFile(app,arg);
    }
  }


  // if we processed some data files,
  // do not go into online mode.
  ShowMem("::End end");
  if(gIsOffline)
    {
      return 0;
    }
  
  gIsOffline = false;
#ifdef HAVE_MIDAS
  ProcessMidasOnline(app);
#endif
   

  for (size_t i=0; i<gAddons.size(); i++){
    delete gAddons.at(i);
  }


  delete app;	
  return 0;
}
//end

