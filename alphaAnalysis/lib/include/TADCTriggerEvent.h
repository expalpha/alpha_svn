#ifndef __TADCTriggerEvent__
#define __TADCTriggerEvent__

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TADCTriggerEvent                                                     //
//                                                                      //
// Object describing an ADC trigger in the SIS                          //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include "TNamed.h"
#include "TVector3.h"

class TADCTriggerEvent : public TNamed 
{
private:

  Int_t		SIS_ADC_TrigNumber;   
  Int_t     ExptNumber; 
  Double_t  RunTime;
  Double_t  ExptTime;

public:
  TADCTriggerEvent();
  virtual ~TADCTriggerEvent();

  // setters
  void SetSIS_ADC_TrigNumber( Int_t num ){ SIS_ADC_TrigNumber = num; }
  void SetExptNumber( Int_t num ){ ExptNumber = num; }
  void SetRunTime( Double_t time ){ RunTime = time; }
  void SetExptTime( Double_t time ){ ExptTime = time; }
 
   // getters
  Int_t GetSIS_ADC_TrigNumber( ){ return SIS_ADC_TrigNumber; }
  Int_t GetExptNumber( ){ return ExptNumber; }
  Double_t GetRunTime( ){ return RunTime; }
  Double_t GetExptTime( ){ return ExptTime; }

  void ClearEvent();
  void Print();
  
  ClassDef(TADCTriggerEvent,1)
};

#endif
