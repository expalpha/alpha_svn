#ifndef __TAlphaPlot__
#define __TAlphaPlot__

#include "TObject.h"
#include "TH1D.h"
#include "TSiliconEvent.h"
#include "TSisEvent.h"
#include "TCanvas.h"
#include "TRootUtils.h"

#include "TLegend.h"
#include "TPaveText.h"
#include "TLine.h"

#define VERTEX_HISTO_Z 0
#define VERTEX_HISTO_R 1
#define VERTEX_HISTO_XY 2
#define VERTEX_HISTO_ZR 3
#define VERTEX_HISTO_ZT 4
#define VERTEX_HISTO_T 5
#define VERTEX_HISTO_IO32 6
#define VERTEX_HISTO_IO32_NOTBUSY 7
#define VERTEX_HISTO_ATOM_OR 8
#define VERTEX_HISTO_PHI 9
#define VERTEX_HISTO_ZPHI 10
#define VERTEX_HISTO_RDENS 11
#define VERTEX_HISTO_TMVA 14 // this must always be the last in the list
#define VERTEX_HISTO_VF48 12
#define VERTEX_HISTO_TPHI 13


//using namespace TRootUtils;
//extern Bool_t TRootUtils::gApplyCuts;


//extern Bool_t gApplyCuts;
extern Double_t grfcut;
struct VertexEvent {
	Int_t runNumber;
	Int_t EventNo;
	Int_t CutsResult;
	Int_t NVerts;
	//TVector3* vtx;
	Double_t x;
	Double_t y;
	Double_t z;
	Double_t t;
	Double_t RunTime;
};

struct SISEvent {
	Int_t runNumber;
    //Int_t Clock;
    Double_t t;
    Double_t RunTime;
	Int_t Counts;
	Int_t SIS_Channel;
};

class TAlphaPlot : public TObject
{
private:
  Int_t MVAMode; //Default 0;
  Int_t Nbin; // 100;
  Int_t DrawStyle; //Switch between colour modes
  
  Int_t gLegendDetail; // = 1;
  Bool_t gApplyCuts;
  Double_t TMin;
  Double_t TMax;
  
  TH1D *hz;
  TH1D *hr;
  TH1D *hphi;
  TH2D *hxy;
  TH2D *hzr;
  TH2D *hzt;
  TH2D *hphit;
  TH2D *hzphi;

  TH1D *ht = NULL;
  TH1D *ht_MVA = NULL;
  TH1D *ht_IO32;
  TH1D *ht_IO32_sistime;
  TH1D *ht_IO32_notbusy;
  TH1D *ht_ATOM_OR;
  
  //Detector SIS channels
  Int_t trig;
  Int_t trig_nobusy;
  Int_t atom_or;
  
  //Beam injection/ ejection markers:
  Int_t Beam_Injection;
  Int_t Beam_Ejection;
  std::vector<Double_t> Ejections;
  std::vector<Double_t> Injections;
  
  //Dump marks:
  Int_t CATStart;
  Int_t CATStop;
  Int_t RCTStart;
  Int_t RCTStop;
  Int_t ATMStart;
  Int_t ATMStop;
  std::vector<Double_t> DumpStarts;
  std::vector<Double_t> DumpStops;
  
  //List of all SIS channels above,used to loop over them
  //(Set in SetSISChannel(runNumber))
  std::vector<Int_t> SISChannels;
  
  std::vector<Int_t> Runs;

  Float_t gTMVAVarFloat[100];
  Int_t gTMVAVarInt[100];
  //TMVA::Reader *reader;

  //Double_t gZcutMax;
  //Double_t gZcutMin;
  //Double_t grfcut;
  std::vector<VertexEvent> VertexEvents;
  std::vector<SISEvent> SISEvents;

public:


  
  
  void SetCutsOn() { gApplyCuts = kTRUE; }
  void SetCutsOff() { gApplyCuts = kFALSE; }
  //Setters
  void SetBinNumber(Int_t bin) { Nbin = bin; }
  void SetMVAMode(Int_t Mode) { MVAMode = Mode; }

  UInt_t GetVertexEventEntries() { return VertexEvents.size(); }
  UInt_t GetSISEventEntreis() { return SISEvents.size(); }

  void AddSilEvent(TSiliconEvent *sil_event, Double_t StartOffset = 0.);

  void AddToTAlphaPlot(TAlphaPlot *ialphaplot);
  void AddToTAlphaPlot(TString file="plot.root");
  TAlphaPlot* LoadTAlphaPlot(TString file="plot.root");

  void AddSisEvent(TSisEvent *sis_event, Double_t StartOffset = 0.);

  void AddEvents(Int_t runNumber, char *description, Int_t repetition = 1, Double_t Toffset = 0, Bool_t zeroTime = kTRUE);
  void AddEvents(Int_t runNumber, Double_t tmin, Double_t tmax, Double_t Toffset = 0., Bool_t zeroTime = kTRUE);

  void SetSISChannels(Int_t runNumber);
  void AutoTimeRange();
  void SetTimeRange(Double_t tmin_, Double_t tmax_);
  Double_t GetTimeRangeMax() {return TMax; }
  Double_t GetTimeRangeMin() {return TMin; }
  
  Double_t GetSilEventMaxT();
  Double_t GetSilEventMinT();
  Double_t GetSISEventMaxT();
  Double_t GetSISEventMinT();
  
  void SetUpHistograms();
  void PrintTimeRange() { std::cout << "Tmin: " << TMin << " Tmax: " << TMax << std::endl; }


  void FillHisto();
  TObjArray GetHisto();
  void ClearHisto();
  TCanvas *Canvas(TString Name = "cVTX");

  // default class member functions
  TAlphaPlot(Bool_t ApplyCuts = kTRUE, Int_t MVAMode = 0);
  
  void ExportCSV(TString filename, Bool_t PassedCutOnly=kFALSE);
  virtual ~TAlphaPlot();
  using TObject::Print;
  virtual void Print();
  using TObject::Draw;
  virtual void Draw(Option_t *option="");

  ClassDef(TAlphaPlot, 3)
};

#endif
