#ifndef _TAnalysisReport_
#define _TAnalysisReport_

#include "TObject.h"
#include "TString.h"
#include "TDatime.h"
#include <iostream>
#include "stdlib.h" 
#include "time.h"
#include "TTimeStamp.h"
#include "TSystem.h"
#include "TSpline.h"
#include "TCanvas.h"
#include "TH1D.h"
using namespace std;


class TAnalysisReport : public TObject
{
private:
  Int_t    runNumber;
  TString  SVNvmcRevision;
  TString  SVNlibRevision;
  TString  SVNalphaStripsRevision;
  TString  SVNalphaAnalysisRevision;
  TString  SVNFull;
  TString  HostName;
  TString  UserAnalysisNotes;
  
  

  TString StripFile;
  TDatime StripDate;
  TDatime AnalysisDate;
  
  //ODB
  //unsigned int StartTimeBinary;
  unsigned int StartTimeBinary;
  
  //unsigned int StopTimeBinary; 
  unsigned int StopTimeBinary; 
  Bool_t CosmicTriggerBool;


  //Compilation file time:
  unsigned int AlphaAnalysisExeDateBinary;
  unsigned int AlphaVMCLibDateBinary;
  unsigned int AlphaLibDateBinary;
  
  //VF48
  Int_t GoodVF48Events;
  Int_t BadVF48Events;
  Int_t DataErrors;
  Int_t Reconstructed;
  Int_t PassedCutEvents;
  Double_t Efficiency;

  //SIS
  Int_t GoodSISEvents;
  Int_t BadSISEvents;
  Int_t gSISdiff;
  long unsigned int SISClockCheck_SIS1;
  long unsigned int SISClockCheck_SIS2;
  Double_t gVF48offset;
  
  //GlobalBools
  Int_t EventCutoff;
  Bool_t gEnableShowMem;
  Bool_t gVf48disasm;
  Double_t nVASigma;
  Double_t pVASigma;
  Int_t gNHitsCut;
  Bool_t gRecOff;
  Bool_t gIsCosmicRun;
  Bool_t gADCspecs;
  Bool_t gCosmicUtils;
  Bool_t gDoSamplesVF48;
  Bool_t gUseTimeRange;
  Double_t TimeRangeStart;
  Double_t TimeRangeStop;
  Bool_t gEnableSuppression;
  Bool_t gEOS;

  Bool_t gWriteAlphaEventTree;

  //Blinding tools
  Int_t RunNumberBlinded;
  Int_t DumpBlinded;
  Int_t LabviewBlinded;
  
  Bool_t StripsFileForced;
  Bool_t OutputFileForced;
  Bool_t PolyFit;
  //Debug Info
  double fCPUtime;
  double fUserTime;

//time calibration tools
  std::vector<double> VF48Ticksd;
  std::vector<double> SISTicksd;
  std::vector<unsigned long int> VF48Ticks;
  std::vector<unsigned long int> SISTicks;
  TSpline3* SISvsVF48_clock_spline=NULL;

  Double_t FirstSISTimestamps[100];
  Double_t FirstVF48Timestamps[100];
  Double_t rungVF48offset;

public:

//time calibration tools
	void GenerateCalibrationSpline(Int_t verbose=0) ; 
	void AddCalibrationEvent(unsigned long int iVF48Ticks, unsigned long int iSISTicks, Int_t verbose=0){
		if ( VF48Ticks.size() >0 )
			if ((iVF48Ticks-VF48Ticks.back())<10*20E6 || (iSISTicks-SISTicks.back()<10*10E6)) 
			{
				//if (verbose>0) 
				cout <<"Not adding element, last element too recent (sub 10s)" << endl;
				return;
			}
		VF48Ticks.push_back(iVF48Ticks);
		if (verbose>0)
		{
			cout << "ading elements to tick vectors:\n";
			cout << iVF48Ticks << "\t" << iSISTicks <<"\n"; 
		}
		SISTicks.push_back(iSISTicks);
		VF48Ticksd.push_back((double)iVF48Ticks);
		SISTicksd.push_back((double)iSISTicks);
		if ( VF48Ticks.size() >1 )  GenerateCalibrationSpline();
	}
	double GetVF48TimeCalibration(double iVF48Ticks);
	double GetVF48TimeCalibration_sec(double iVF48Time);
	TSpline3* GetCalibrationSpline(){ return SISvsVF48_clock_spline;}
	std::vector<unsigned long int>* GetVF48Ticks(){ return &VF48Ticks;}
	Double_t GetVF48TicksTotal() {return (Double_t) VF48Ticks.back();}
	std::vector<unsigned long int>* GetSISTicks(){ return &SISTicks;}
	std::vector<double>* GetVF48TicksDouble(){ return &VF48Ticksd;}
	std::vector<double>* GetSISTicksDouble(){ return &SISTicksd;}

  void SetStartTimeMatching(Double_t* _SIS,Double_t* _VF48)
  {
    memcpy( FirstSISTimestamps, _SIS, 100*sizeof(Double_t)); 
    memcpy( FirstVF48Timestamps, _VF48, 100*sizeof(Double_t));
  }

  void AddExtraTimeMatchingEventToSIS(int position = 50){
    if (position >98 || position <0 ){printf("Index of insertion out of bounds (0-98)\n");} 
    else {
      double tempinsert = FirstSISTimestamps[position];
      double tempcopy; 
      for (int i = position+1; i < 100; i++){
        tempcopy = FirstSISTimestamps[i];
        FirstSISTimestamps[i]= tempinsert; 
        tempinsert=tempcopy;
      }
    }
  }
  void AddExtraTimeMatchingEventToVF48(int position = 50){
    if (position >98 || position <0 ){printf("Index of insertion out of bounds (0-98)\n");} 
    else {
      double tempinsert = FirstVF48Timestamps[position];
      double tempcopy; 
      for (int i = position+1; i < 100; i++){
        tempcopy = FirstVF48Timestamps[i];
        FirstVF48Timestamps[i]= tempinsert; 
        tempinsert=tempcopy;
      }
    }

  }
 
  Double_t* GetFirstSISTimestamps(){
    return FirstSISTimestamps;
  }
  Double_t* GetFirstVF48Timestamps(){
    return FirstVF48Timestamps;
  }

  // setters
  
  //OD=B
 // void SetStartTimeString(TString StartTimeStr)	{StartTimeString= StartTimeStr;}
  void SetStartTimeBinary(unsigned int StartTimeBin){StartTimeBinary= StartTimeBin;}
  void SetrungVF48offset(double ioffset){rungVF48offset = ioffset;} 
  //void SetStopTimeString(const char* StopTimeStr)		{StopTimeString= StopTimeStr;} 
  void SetStopTimeBinary(unsigned int StopTimeBin)		{StopTimeBinary= StopTimeBin;}  
  void SetCosmicTriggerBool(Bool_t CosmicTrigger)	{CosmicTriggerBool=CosmicTrigger;}

  void SetAlphaAnalysisExeDateBinary(unsigned int FileTime) { AlphaAnalysisExeDateBinary=FileTime;}
  void SetAlphaVMCLibDateBinary(unsigned int FileTime) { AlphaVMCLibDateBinary=FileTime; }
  void SetAlphaLibDateBinary(unsigned int FileTime) { AlphaLibDateBinary=FileTime; }
  
  void SetRunNumber(Int_t RunNo)		{runNumber=RunNo;} //Is this redundant?
  
  void SetSVNvmcRevision(TString SVNRev)			 { SVNvmcRevision = SVNRev; }
  void SetSVNlibRevision(TString SVNRev)			 { SVNlibRevision = SVNRev; }
  void SetSVNalphaStripsRevision(TString SVNRev)			 { SVNalphaStripsRevision = SVNRev; }
  void SetSVNalphaAnalysisRevision(TString SVNRev)			 { SVNalphaAnalysisRevision = SVNRev; }
  void SetSVNFull(TString SVNFullReport)	 { SVNFull = SVNFullReport; }
  void SetHostName(TString HostN)			 { HostName = HostN; }
  void SetUserAnalysisNotes()		{ UserAnalysisNotes = getenv( "ANALYSISNOTES" ); }//Get environment variable of user notes (optional usage, by default empty)
  void SetStripFile(TString StripTreeFile)		{   StripFile =  StripTreeFile; }
  void SetStripFileDate(TDatime StripFileTime)	{  StripDate = StripFileTime;}
  void SetAnalysisDate(TDatime AnalysisDatime)	{  AnalysisDate = AnalysisDatime; }
  
  //VF48
  void SetGoodVF48Events(Int_t Goodvf48Events)	{  GoodVF48Events = Goodvf48Events; }
  void SetBadVF48Events(Int_t Badvf48Events)	{  BadVF48Events = Badvf48Events; }
  void SetDataErrors(Int_t Dataerrors)	{  DataErrors = Dataerrors; }
  void SetTotalReconstructedEvents(Int_t ReconstructedEvents)	{  Reconstructed = ReconstructedEvents; }
  void SetVF48Counts(Int_t gGoodVF48Events, Int_t gBadVF48Events, Int_t VF48badDataCount, Int_t gRecEvents)
  {
	  SetGoodVF48Events(gGoodVF48Events);
	  SetBadVF48Events(gBadVF48Events);
	  SetDataErrors(VF48badDataCount);
	  SetTotalReconstructedEvents(gRecEvents);
  }  
  void SetPassedCutEvents(Int_t _pass) { PassedCutEvents = _pass; }
  Int_t GetPassedCutEvents() { return PassedCutEvents; }
  //SIS
  void SetGoodSISEvents(Int_t GoodsisEvents)	{  GoodSISEvents = GoodsisEvents; }
  void SetBadSISEvents(Int_t BadsisEvents)	{  BadSISEvents = BadsisEvents; }
  void SetSISdiff(Int_t SISdiff) { gSISdiff = SISdiff; }
  void SetSISClockCheck_SIS1(long unsigned int SISCounts)	{  SISClockCheck_SIS1 = SISCounts; }
  void SetSISClockCheck_SIS2(long unsigned int SISCounts)	{  SISClockCheck_SIS2 = SISCounts; }
  void SetVF48Offset(Double_t _off) { gVF48offset=_off; }
  
  
  void SetSISCounters(Int_t Good, Int_t Bad, Int_t SISdiff, long unsigned int SISClockCheck_SIS1, long unsigned int SISClockCheck_SIS2) {
	  SetGoodSISEvents(Good);
	  SetBadSISEvents(Bad);
	  SetSISdiff(SISdiff);
	  SetSISClockCheck_SIS1(SISClockCheck_SIS1);
	  SetSISClockCheck_SIS2(SISClockCheck_SIS2);
  }
  
  
  //Blinding tools
  
  void SetRunNumberBlinded(Int_t run) { RunNumberBlinded=run; } //save, but hide the run number
  void SetDumpBlinded(Int_t Num) { DumpBlinded=Num; }
  void IncrementDumpBlinded() { DumpBlinded++; }
  void SetLabviewBlinded(Int_t Num) { LabviewBlinded=Num; }
  void IncrementLabviewBlinded() { LabviewBlinded++; }
  
  
  void SetStripsFileForced(Bool_t Setting) { StripsFileForced = Setting; }
  void SetOutputFileForced(Bool_t Setting) { OutputFileForced = Setting; }
  
  void SetPolyFit(Bool_t Setting) { PolyFit = Setting; }
  
  //Global Vars:
  
  void SaveGlobalVars(Bool_t CutOff, Bool_t EnableShowMem, Bool_t Vf48disasm, Double_t n_VASigma, Double_t p_VASigma, Int_t NHitsCut, Bool_t RecOff, Bool_t IsCosmicRun, Bool_t ADCspecs, Bool_t CosmicUtils, Bool_t DoSamplesVF48, Bool_t UseTimeRange, Double_t TStart, Double_t TStop, Bool_t EnableSuppression, Bool_t EOS) {
	   EventCutoff=CutOff;
	   gEnableShowMem=EnableShowMem;
	   gVf48disasm=Vf48disasm;
	   nVASigma=n_VASigma;
	   pVASigma=p_VASigma;
	   gNHitsCut=NHitsCut;
	   gRecOff=RecOff;
	   gIsCosmicRun=IsCosmicRun;
	   gADCspecs=ADCspecs;
	   gCosmicUtils=CosmicUtils;
	   gDoSamplesVF48=DoSamplesVF48;
	   gUseTimeRange=UseTimeRange;
	   TimeRangeStart=TStart;
	   TimeRangeStop=TStart;
	   gEnableSuppression=EnableSuppression;
	   gEOS=EOS;
  }
  void WriteAlphaEventTrue() { gWriteAlphaEventTree=kTRUE; }
  void WriteAlphaEventFalse() { gWriteAlphaEventTree=kFALSE; }

  // getters
  
  Int_t		GetRunNumber()		{ return runNumber; }
  //Int_t     GetSVNRevision()	{ return SVNRevision; }
  TString GetSVNvmcRevision() { return SVNvmcRevision; }
  TString GetSVNlibRevision() { return SVNlibRevision; }
  TString GetSVNalphaStripsRevision() { return SVNalphaStripsRevision; }
  TString GetSVNalphaAnalysisRevision() { return SVNvmcRevision; }
  TSpline3* GetSISvsVF48_clock_spline() {return SISvsVF48_clock_spline;}
  double GetrungVF48offset(){ return rungVF48offset;}
  
  
  
  TString   GetSVNFull()		{ return SVNFull; }
  TString   GetHostname()		{ return HostName; }
  TString	GetUserAnalysisNotes()	{ return UserAnalysisNotes; }
  TString GetStripFile()	{ return StripFile; }
  TDatime GetStripFileDate()	{ return StripDate; }
  Int_t GetStripFileDateDay()	{ return StripDate.GetDay(); }
  Int_t GetStripFileDateMonth()	{ return StripDate.GetMonth(); }
  Int_t GetStripFileDateYear()	{ return StripDate.GetYear(); }
  
  TDatime GetAnalysisFileDate()	{ return AnalysisDate; }
  Int_t GetAnalysisDateDay()	{ return AnalysisDate.GetDay(); }
  Int_t GetAnalysisFileDateMonth()	{ return AnalysisDate.GetMonth(); }
  Int_t GetAnalysisFileDateYear()	{ return AnalysisDate.GetYear(); }
  
  Int_t GetRunNumberBlinded() { if (RunNumberBlinded>0) {return 1;}
	  else {return 0;} } //save, but hide the run number
  Int_t GetDumpBlinded() { return DumpBlinded; }
  Int_t GetLabviewBlinded() { return LabviewBlinded; }
  
  //ODB
  TString GetStartTimeString()	{ time_t ts=StartTimeBinary;
	 const struct tm*  timeinfo = localtime(&ts);
	 TString TimeString=asctime(timeinfo);
	 TimeString.ReplaceAll("\n","");
	 return TimeString; }
	 
  Int_t GetRunStartDay() {
	 TDatime dt;
	 dt.Set(StartTimeBinary);
	 return dt.GetDay();
  }
  Int_t GetRunStartMonth() {
	 TDatime dt;
	 dt.Set(StartTimeBinary);
	 return dt.GetMonth();
  }
  Int_t GetRunStartYear() {
	 TDatime dt;
	 dt.Set(StartTimeBinary);
	 return dt.GetYear();
  }
  TString GetRunStartDate() {
	  
	  TString ReturnString="";
	  ReturnString+=GetRunStartDay();
	  ReturnString+="/";
	  ReturnString+=GetRunStartMonth();
	  ReturnString+="/";
	  ReturnString+=GetRunStartYear();
	  return ReturnString;
  }

  TH1D* GetSISVF48DiffHist(){
	TH1D* hist= new TH1D("name", "title", 99, -0.01, 0.01);
	for( int i =0; i < 100; i++){
		hist->Fill(FirstSISTimestamps[i]- FirstVF48Timestamps[i]);	
	}
	hist->SetTitle("SIS - VF48 time");
	hist->GetXaxis()->SetTitle("Time [s]");
	hist->GetYaxis()->SetTitle("Counts");
	return hist;
  }

double GetMeanSISVF48Diff(){
	TH1D* hist = GetSISVF48DiffHist();
	double mean = hist->GetMean();
	delete hist;
	return mean; 
}

double GetVarSISVF48Diff(){
	TH1D* hist = GetSISVF48DiffHist();
	double stddev = hist->GetStdDev();
	delete hist;
	return stddev; 
}
 
  TH1D* GetSISDiffHist(){
	TH1D* hist= new TH1D("name", "title", 99, -0.01, 0.3);
	for( int i =0; i < 99; i++){
		hist->Fill(FirstSISTimestamps[i+1]- FirstSISTimestamps[i]);	
	}
	return hist;

  }

  TH1D* GetVF48DiffHist(){
	TH1D* hist= new TH1D("name", "title", 99, -0.01, 0.3);
	for( int i =0; i < 99; i++){
		hist->Fill(FirstVF48Timestamps[i+1]- FirstVF48Timestamps[i]);	
	}
	return hist;

  }	  

TH1D* GetCalibrationsdSISminusdVF48(){
	double*	sists = GetFirstSISTimestamps();
	double* vf48ts = GetFirstVF48Timestamps();
	TH1D* hist= new TH1D("name", "title", 99, -0.005, 0.005);
	for( int i =0; i < 99; i++){
		hist->Fill(vf48ts[i+1]- vf48ts[i] - sists[i+1] + sists[i]);	
	}

	hist->SetTitle("dVF48 time - dSIS time");
	hist->GetXaxis()->SetTitle("Time [s]");
	hist->GetYaxis()->SetTitle("Counts");
	return hist;

}
	  
double GetMeanCalibrationsdSISminusdVF48(){
	TH1D* hist = GetCalibrationsdSISminusdVF48();
	double mean = hist->GetMean();
	delete hist;
	return mean; 
}

double GetVarCalibrationsdSISminusdVF48(){
	TH1D* hist = GetCalibrationsdSISminusdVF48();
	double stddev= hist->GetStdDev();
	delete hist;
	return stddev; 
}

double GetgOffset(){return rungVF48offset;}

void PrintTimingSampleQOD(){
	printf("100 event timing sample QOD. Run:\t%i\n", runNumber);
	printf("%-18s:\tMean:%E\tSigma:%E\n", "sis[n] - vf48[n]", GetMeanSISVF48Diff(), GetVarSISVF48Diff());
	printf("%-18s:\tMean:%E\tSigma:%E\n", "dsis[n] - dvf48[n]", GetMeanCalibrationsdSISminusdVF48(), GetVarCalibrationsdSISminusdVF48());
	printf("%-18s:\t%E\n", "gVF48 clock offset", rungVF48offset);
}
 
  TString GetStartTimeExcelFormat() {
	  TDatime dt;
	  dt.Set(StartTimeBinary);
	  TString ExcelString="";
	  ExcelString+=GetRunStartDate();
	  ExcelString+=" ";
	  ExcelString+=dt.GetHour();
	  ExcelString+=":";
	  ExcelString+=dt.GetMinute();
	  ExcelString+=":";
	  ExcelString+=dt.GetSecond();
	  //sprintf(ExcelString,"%d/%d/%d %d:%d:%d",day,month,year,hour,min,sec);
	  return ExcelString;
  }  
  unsigned int GetStartTimeBinary()		{ return StartTimeBinary; } 
  
  
  
  TString GetStopTimeString()	{ time_t ts=StopTimeBinary;
	 const struct tm*  timeinfo = localtime(&ts);
	 TString TimeString=asctime(timeinfo);
	 TimeString.ReplaceAll("\n","");
	 return TimeString; }
  TString GetStopTimeExcelFormat() {
	  TDatime dt;
	  dt.Set(StopTimeBinary);
	  TString ExcelString="";
	  ExcelString+=GetRunStartDate();
	  ExcelString+=" ";
	  ExcelString+=dt.GetHour();
	  ExcelString+=":";
	  ExcelString+=dt.GetMinute();
	  ExcelString+=":";
	  ExcelString+=dt.GetSecond();
	  return ExcelString;
  }  
  unsigned int GetStopTimeBinary()		{ return StopTimeBinary; }
  Bool_t GetCosmicTriggerBool()	{ return CosmicTriggerBool; }
  
  
   
  //Compilation dates and time
  unsigned int GetAlphaAnalysisExeDateBinary()  { return AlphaAnalysisExeDateBinary; }
  TString GetAlphaAnalysisExeDateString()	{ time_t ts=AlphaAnalysisExeDateBinary;
	 const struct tm*  timeinfo = localtime(&ts);
	 TString TimeString=asctime(timeinfo);
	 TimeString.ReplaceAll("\n","");
	 return TimeString; }
  
  unsigned int GetAlphaVMCLibDateBinary()  { return AlphaVMCLibDateBinary; }
  TString GetAlphaVMCLibDateString()	{ time_t ts=AlphaVMCLibDateBinary;
	 const struct tm*  timeinfo = localtime(&ts);
	 TString TimeString=asctime(timeinfo);
	 TimeString.ReplaceAll("\n","");
	 return TimeString; }
  
  unsigned int GetAlphaLibDateBinary()  { return AlphaLibDateBinary; }
  TString GetAlphaLibDateString()	{ time_t ts=AlphaLibDateBinary;
	 const struct tm*  timeinfo = localtime(&ts);
	 TString TimeString=asctime(timeinfo);
	 TimeString.ReplaceAll("\n","");
	 return TimeString; }
  
  
  
  //VF48
  Int_t GetGoodVF48Events()		{ return GoodVF48Events; }
  Int_t GetBadVF48Events()		{ return BadVF48Events; }
  Int_t GetTotalVF48Events()	{ Int_t TotalVF48Events=GoodVF48Events+BadVF48Events;
	   return (TotalVF48Events); }
  Int_t GetDataErrors()			{ return DataErrors; }
  Int_t GetTotalReconstructedEvents(){ return Reconstructed; }
  Double_t GetRecoEfficiency() { Double_t RecoEf=(double)Reconstructed/(double(GoodVF48Events+BadVF48Events)); return RecoEf; }

  //SIS
  Int_t GetGoodSISEvents() { return GoodSISEvents; }
  Int_t GetBadSISEvents() { return BadSISEvents; }
  Int_t GetSISdiff() { return gSISdiff; }
  long unsigned int GetSISClockCheck_SIS1() { return SISClockCheck_SIS1; }
  long unsigned int GetSISClockCheck_SIS2() { return SISClockCheck_SIS2; }
  Double_t GetVF48Offset() { return gVF48offset; }


  //Global bools
  Int_t GetEventCutoff() { return EventCutoff; }
  Bool_t GetgEnableShowMem() { return gEnableShowMem; }
  Bool_t GetgVf48disasm() { return gVf48disasm; }
  Double_t GetnVASigma() { return nVASigma; }
  Double_t GetpVASigma() { return pVASigma; }
  Int_t GetgNHitsCut() { return gNHitsCut; }
  Bool_t GetgRecOff() { return gRecOff; }
  Bool_t GetgIsCosmicRun() { return gIsCosmicRun; }
  Bool_t GetgADCspecs() { return gADCspecs; }
  Bool_t GetgCosmicUtils() { return gCosmicUtils; }
  Bool_t GetgDoSamplesVF48() { return gDoSamplesVF48; }
  Bool_t GetgUseTimeRange() { return gUseTimeRange; }
  Double_t GetTimeRangeStart() { return TimeRangeStart; }
  Double_t GetTimeRangeStop() { return TimeRangeStop; }
  Bool_t GetgEnableSuppression() { return gEnableSuppression; }
  Bool_t GetgEOS() { return gEOS; }
  Bool_t GetPolyFit() { return PolyFit; }
  
  Bool_t HasAlphaEvents() { return gWriteAlphaEventTree; }
 
 
 
  Bool_t GetStripsFileForced() { return StripsFileForced; };
  Bool_t GetOutputFileForced() { return OutputFileForced; };
  
  void PrintGlobalBools(Int_t Verbosity=0);
  void PrintCompilationDate(Int_t Verbosity=0);
  void PrintBlindingTool(Int_t Verbosity=0);
  using TObject::Print;
  virtual void Print(Int_t Verbosity=0);

  //Debug Info
  double GetCPUtime() const  {return fCPUtime;}
  void SetCPUtime(double t)  {fCPUtime=t;}
  double GetUserTime() const {return fUserTime;}
  void SetUserTime(double t) {fUserTime=t;}
  
  // default class member functions
  TAnalysisReport();
  virtual ~TAnalysisReport();
  
  ClassDef(TAnalysisReport,8);

  
};

#endif
