#ifndef _TLabVIEWEvent_
#define _TLabVIEWEvent_
/*
 *  TLavVIEWEvent.h
 *  
 *
 *  Created by Sarah Seif El Nasr on 09/08/07.
 *  Copyright 2007 __MyCompanyName__. All rights reserved.
 *
 *  SSN: Header file used to define the new root library
 *       containing the Tobject TLavVIEWEvent 
 */ 

#include "TObject.h"
#include "TString.h"
#include <iostream>

// Define object of type SisTreeEvent , inherits from TObject
class TLabVIEWEvent : public TObject
{
private:

	TString 	bankName;
	Int_t   	arrayNumber;
	Double_t	value;
	Double_t    	time;
	
	Int_t		fSisCounter;
	Int_t		fVertexCounter;
		
public:
	
	TString GetBankName() 		{ return bankName; }
	Int_t GetArrayNumber()		{ return arrayNumber; }
    	Double_t GetValue()			{ return value; }
	Double_t GetTime()			{ return time; }
	
	//Set functions for private members of SisTreeEvent class
	void SetBankName(TString name) 	{ bankName = name; }
	void SetArrayNumber(Int_t number)	{ arrayNumber = number; }
	void SetValue(Double_t ivalue)		{ value = ivalue; }
	void SetTime(Double_t	Time)		{ time = Time; }
	
	Int_t GetSISCounter() 		{ return fSisCounter; }
    	Int_t GetVertexCounter()	{ return fVertexCounter; }
	
    	void SetSISCounter( Int_t event ) 		{ fSisCounter = event; }
    	void SetVertexCounter( Int_t event )	{ fVertexCounter = event; }
	void SetCounters( Int_t SisCounter, Int_t VertexCounter) { 
	SetSISCounter( SisCounter ); SetVertexCounter( VertexCounter); }
	// data manipulation fuctions
	void SetLabVIEWEvent(TString name, Int_t number, Double_t value, Double_t time);
	void ClearLabVIEWEvent();
	// default class member functions
	TLabVIEWEvent();  
	virtual ~TLabVIEWEvent(); 
	
	ClassDef(TLabVIEWEvent,1) 
};
#endif
