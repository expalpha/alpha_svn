#ifndef __TRootUtils__
#define __TRootUtils__
#include "TNamed.h"
#include "TObject.h"
#include "TFile.h"
#include "TROOT.h"
#include "TError.h"
#include "TSystem.h"
#include "TTree.h"
#include "TEventList.h"
#include <iostream>
#include <iomanip>
#include <fstream>

#include "TAlphaPlot.h"


#include "TSeq_Event.h"
#include "TSisEvent.h"
#include "TSiliconEvent.h"
#include "TSISChannels.h"
#include "TTCEvent.h"
#include "TLabVIEWEvent.h"
//#include "../lib/include/SIS_Channels.h.alpha2"
#include "TVF48SiMap.h"
#include "TAlphaEvent.h"
#include "TAnalysisReport.h"
#include "TalphaStripsReport.h"
#include "TTimestampHelper.h"
#include "TSeq_EventHelper.h"
#include "TSequenceDriver.h"

#include "TMVA/Tools.h"
#include "TMVA/Reader.h"
#include "TMVA/MethodCuts.h"


namespace TRootUtils
{

/*
class TRootUtils : public TNamed
{
private:
public:*/
void SetZCut(double min=-999., double max=999.);

Int_t ApplyCuts(TSiliconEvent *sil_event);

Int_t ApplyMVA(TSiliconEvent *sil_event, Double_t *rfout=new Double_t());

void AnnounceOnSpeaker(Int_t runNumber, TString Phrase);
void AnnounceOnSpeaker(TString Phrase);
TString MakeAutoPlotsFolder(TString subFolder = "");



TFile *Get_Strip_File(Int_t run_number, Bool_t die = kTRUE);
TFile *Get_Cosmic_File(Int_t run_number, Bool_t die = kTRUE);
TFile *Get_File(Int_t run_number, Bool_t die = kTRUE);
void Close_File(Int_t run_number);
TFile *Get_Dumper_File(Int_t run_number);
TTree *Get_Dumper_Tree(Int_t run_number);
//TFile *Get_MVA_File(Int_t run_number, Bool_t die = kTRUE);

//long HashMySpr();
void RunMVADumper(Int_t run_number, const char *PHASE = "ALL");



void RunMVACosmicDumper(Int_t run_number, Double_t tmin, Double_t tmax);         //0
void RunMVACosmicHitDumper(Int_t run_number, Double_t tmin, Double_t tmax);      //1
void RunMVACosmicHitTrackDumper(Int_t run_number, Double_t tmin, Double_t tmax); //3
//void RunApplyRF(Int_t run_number, const char *phase = "all");
//void RunApplyRF(TString ifilename);

TTree *GetSequencerTree(Int_t runNumber);
TTree *GetSequencerStateTree(Int_t runNumber);
TTree *GetSequencerXMLTree(Int_t runNumber);
TTree *GetSequenceDriverTree(Int_t runNumber);
TTree *Get_Sis_Tree(Int_t run_number);
TTree *Get_Sis_Tree(Int_t run_number, Int_t channel);
TTree *Get_Vtx_Tree(Int_t run_number);
TTree *Get_TTC_Tree(Int_t run_number);
TTree *Get_LabVIEW_Tree(Int_t run_number);

TSequencerDriver* GetSequenceDriver(Int_t runNumber, int SeqNum);
int GetSequenceDO(Int_t runNumber, Int_t SeqNum, TString Bit);
int GetSequenceDOByDescription(Int_t runNumber, Int_t SeqNum, TString Description);
int GetSequenceDOStopDump(Int_t runNumber, Int_t SeqNum);
int GetSequenceDOStartDump(Int_t runNumber, Int_t SeqNum);


Int_t CountEventsInRun(Int_t runNumber, const char *eventName, const char *description, bool exact_match = false);
Bool_t TestForEventInRun(Int_t runNumber, const char *eventName, const char *description, Int_t repetition = 1, Int_t offset = 0, bool exact_match = false);
Double_t MatchEventToTime(Int_t runNumber, const char *eventName, const char *description, Int_t repetition = 1, Int_t offset = 0, bool exact_match = false);
Double_t GetStateID(Int_t runNumber, const char *eventName, const char *description, Int_t repetition, Int_t offset, bool exact_match);
Int_t GetFirstVF48EventInDump(Int_t runNumber, const char *description, Int_t repetition = 1, Int_t offset = 0);
Int_t GetLastVF48EventInDump(Int_t runNumber, const char *description, Int_t repetition = 1, Int_t offset = 0);


Double_t Get_RunTime_of_SequencerEvent(Int_t run_number, TSeq_Event *seqEvent, Int_t seqOffset = 0, Bool_t RedundantCheck = kTRUE);
Double_t Get_RunTime_of_Count(TTree *tree, Int_t count, Double_t tmin, Double_t tmax);
Double_t Get_RunTime_of_Count(Int_t run_number, Int_t channel, Int_t count, Double_t tmin, Double_t tmax);
Double_t Get_RunTime_of_Count(TTree *tree, Int_t count, Int_t StartPos=0);
Double_t Get_RunTime_of_Count(Int_t run_number, Int_t channel, Int_t count);
Double_t Get_RunTime_of_SISChan(Int_t run_number, Int_t channel, Int_t rep);
TArray *Get_RunTime_of_Pulse(Int_t runNumber, vector<Int_t> VetoChan, Int_t tmin, Int_t tmax);
Double_t GetTotalRunTime(Int_t runNumber);
Double_t FindSISQuenchFlagTime(Int_t runNumber, Int_t repetition = 1, Bool_t FailOnError = true);

//alphaStripsReport utilities
TalphaStripsReport *GetStripReport(Int_t runNumber);
TTree *GetStripReportTree(Int_t runNumber);

//AnalysisReport utilities
void PrintProcessingTime(Int_t runNumber);
Double_t GetProcessingTime(Int_t runNumber);
TString GetSVNVersion(Int_t runNumber);
TAnalysisReport *GetAnalysisReport(TFile* root_file);
TAnalysisReport *GetAnalysisReport(Int_t runNumber);
TTree *GetAnalysisReportTree(TFile* root_file);
TTree *GetAnalysisReportTree(Int_t runNumber);

//Timestamp helper utilities

TTree *GetTimestampHelperTree(Int_t runNumber);
TTimestampHelper *GetTimestampHelper(Int_t runNumber);
TTimestampHelper *GetTimestampHelper(Int_t runNumber,Int_t channel);
TTree *GetSeqEventHelperTree(Int_t runNumber);
TSeq_EventHelper *GetSeqEventHelper(Int_t runNumber);


Int_t GetFirstVF48EventBeforeTime(Int_t runNumber, Double_t t);
Int_t GetFirstSISEventBeforeTime(Int_t runNumber, Int_t SISch, Double_t t);

//Sequencer string and hash utilities
TString GetRWinRun(int runNumber, TString RW);
TString GetSequenceQueueFull(int runNumber, TString SequencerName);
TString GetSequenceQueue(int runNumber, TString SequencerName);
void PrintSequences(int runNumber, TString SequencerName);
TString GetQWPDir(int runNumber);
long int GetTrappingHash(Int_t runNumber);
TString CalculateSequenceFlagHex(TString SequenceString);
TString GetSequencerOnlyString(Int_t runNumber);
long int GetSequencerOnlyStringHash(Int_t runNumber);
TString GetRWOnlyString(Int_t runNumber);
long int GetRWOnlyStringHash(Int_t runNumber);
TString GetSequenceString(Int_t runNumber);
long int GetSequenceStringHash(Int_t runNumber);

//Labview utilities
Double_t LabVIEW_at_SIS(Int_t runNumber, Int_t SIS_channel, Int_t repetition, TString Bank, Int_t Array_number, Int_t Interp_Option = 2);
Double_t LabVIEW_at_dump(Int_t runNumber, const char *description, Int_t repetition, Int_t offset, TString Bank, Int_t Array_number, Int_t Interp_Option);
Double_t GetLabVIEWEvent(Int_t runNumber, Double_t runTime, TString Bank, Int_t Array_number, Int_t Interp_Option);
Double_t GetLabVIEWEvent(TTree *labVIEW_tree, Double_t runTime, TString Bank, Int_t Array_number, Int_t Interp_Option);
//Int_t Extract_Bank_Array(Int_t run_number, TString bank, Int_t array_num, Double_t *time, Double_t *LVData);
Int_t Extract_Bank_Array(Int_t run_number, TString bank, Int_t array_num, Double_t *time, Double_t *LVData, Double_t tmin = 0., Double_t tmax = -1.);

Int_t Count_SIS_Triggers(Int_t run_number, Int_t SIS_Channel, const char *description, Int_t repetition = 1, Int_t offset = 0, bool exact_match = false);
Int_t Count_SIS_Triggers(Int_t run_number, Int_t SIS_Channel, Double_t StartTime, Double_t StopTime);
Int_t Count_SIS_Triggers(Int_t run_number, Int_t SIS_Channel);

//Double_t Get_RunTime_of_SequencerEvent(Int_t run_number, TSeq_Event* seqEvent, Int_t seqOffset);

//Functions imported for dumper
TSeq_Event *FindSequencerEvent(TTree *sequencerTree, const char *eventName, const char *description);
TSeq_Event *FindSequencerEvent(TTree *sequencerTree, const char *eventName, const char *description, Int_t repetition, Int_t &offset);
TSeq_Event *FindSequencerEvent(Int_t runNumber, const char *eventName, const char *description, Int_t repetition, Int_t &offset, bool exact_match = false);

Double_t GetRunTimeOfCount(TTree *tree, Int_t count);
Double_t GetRunTimeOfCount(TTree *tree, Int_t count, Double_t seq_start_time);

TTree *GetAlphaEvents(TFile *theFile);
TTree *GetSiliconEvents(TFile *theFile);
TTree *GetSequencerTree(TFile *theFile);
TTree *GetSISTree(TFile *theFile);
TTree *GetSISTree(TFile *theFile, Int_t channel);

Double_t GetRunTimeOfEntry(TTree *tree, Int_t entry);

// Verison for multiple dump names
void GetDumpTime(TFile *theFile, std::vector<TString> EventDescription, std::vector<Double_t> &StartTime, std::vector<Double_t> &StopTime, std::vector<TString> &dumpname);
// version for repeated dumps same dumpname
void GetDumpTime(TFile *theFile, const char *EventDescription, std::vector<Double_t> &StartTime, std::vector<Double_t> &StopTime, std::vector<TString> &dumpname, Int_t nreps);
//single dumptime version
void GetDumpTime(TFile *theFile, const char *EventDescription, Double_t &StartTime, Double_t &StopTime, Double_t repetition = 0);

Double_t GetClockCalib(TFile *theFile);
Double_t GetClockCalib(TTree *tree1, TTree *tree2);

Int_t CountsMixingTriggers(Int_t runNumber);
Double_t FindSISMixingFlagTime(Int_t runNumber, Int_t repetition);
Double_t FindMixingGate(TFile *theFile, Double_t &StartTime, Double_t &StopTime);
Double_t FindQuenchGate(TFile *theFile, Double_t &StartTime, Double_t &StopTime);

Int_t FileToRuns(const char *filename, Int_t *runNumber = NULL, Int_t *dumps = NULL, TObjArray *legendText = NULL);
Int_t FileToSubFiles(const char *filename, TObjArray *subFilenames, TObjArray *legendText = NULL);

Int_t CheckForDuplicates(Int_t nRuns, Int_t *runs);
//TString GetQWPDir(int runNumber);
Int_t *TTC_Multiplicity_by_Layer(Int_t runNumber, Int_t EventNumber[]);

Bool_t IsDumpInCAT(Int_t runNumber, const char *eventName, const char *description, Int_t repetition = 1, Int_t offset = 0);

Bool_t IsDumpInRCT(Int_t runNumber, const char *eventName, const char *description, Int_t repetition = 1, Int_t offset = 0);
Bool_t IsDumpInATM(Int_t runNumber, const char *eventName, const char *description, Int_t repetition = 1, Int_t offset = 0);
};

#endif
