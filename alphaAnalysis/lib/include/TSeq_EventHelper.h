#ifndef _TSeq_EventHelper_
#define _TSeq_EventHelper_

#include "TObject.h"
#include "TString.h"
#include "TTree.h"
#include "TSeq_Event.h"
#include <vector>
#include <iostream>
#include "stdlib.h" 

using namespace std;
//This object is used as a cache for timestamps for a Sequence. This means a 
//small lump of data can be loaded as an index of all events, meaning 
//loops over all events in a file can be avoided
//JTKM

class TSeq_EventHelper : public TObject
{
private:
  Int_t    runNumber;
  //TObjArray* Dumps; //Array of TSeq_Dumps? Its a possibility... but, No, lets not...

  std::vector<TString> fSeq;
  std::vector<Int_t> fSeqNum;
  std::vector<Int_t> fID;
  std::vector<TString> fEventName;
  std::vector<TString> fDescription;

  std::vector<Int_t> fonCount;
  //std::vector<Int_t> fStartIntCount;

  std::vector<Double_t> RunTime;

public:

  //using TObject::Print;
  //virtual void Print(Int_t Verbosity=0);
  
  void SetRunNumber(Int_t RunNo) { runNumber=RunNo; }
  Int_t GetRunNumber() { return runNumber; }
  
  void FillWithTree(TTree* sequencerTree);
  void AddTSeq_Event(TSeq_Event* Event, Double_t RunTime);
  
  Double_t GetRunTimeOfDump( TSeq_Event* seqEvent, Int_t seqOffset=0);
  Double_t GetRunTimeOfDump( const char *eventName, const char *description, Int_t repetition=1, Int_t offset=0, bool exact_match=kFALSE);
  // default class member functions
  TSeq_EventHelper();
  TSeq_EventHelper(Int_t RunNo);
  
  virtual ~TSeq_EventHelper();
  ClassDef(TSeq_EventHelper, 1);

};

#endif
