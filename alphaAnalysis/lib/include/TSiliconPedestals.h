#ifndef __TSiliconPedestals__
#define __TSiliconPedestals__

// TSiliconPedestals Class =======================================================
//
// Class representing the Si detector pedestals
//
// ===============================================================================

#include "TSiliconStrip.h"
#include "../../../alphavmc/include/SiMod.h"

class TSiliconPedestals: public TObject {

private:
  double ped_adc_rms[nVF48*48];  // pedestal sigma for n VF48s * 48 channels/VF48 
  int run_number; // run from which pedestal data is determined

public:
  TSiliconPedestals(){};
  TSiliconPedestals( int _run_number );
  virtual ~TSiliconPedestals();

  void Set_Run_Number( int _run_number ){ run_number = _run_number; } 
  int Get_Run_Number( ){ return run_number; }
  void Set_Ped_Adc_Rms( int _strips_id, double _ped_adc_rms );
  double Get_Ped_Adc_Rms( int _strips_id );

  void Print();

  int Write_Channel_Pedestal( int vf48modnum, int chan, TF1* myGauss );
  int Write_Pedestal_Tree( );
  int Read_Pedestal_Tree( char * fname );
  int Read_Pedestal_Tree( );

  ClassDef(TSiliconPedestals,1)
};

#endif
