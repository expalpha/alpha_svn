#ifndef __TSiliconStripRMS__
#define __TSiliconStripRMS__

// TSiliconStripRMS Class =========================================================================================
//
// Class representing the RMS of a single silicon strip.
//
// JWS 02/07/2009
//
// ==========================================================================================================

#include "TSiliconStrip.h"

class TSiliconStripRMS : public TObject {

private:  
  Int_t ModuleNumber;
  Int_t ASICNumber;
  Int_t StripNumber;

  Int_t sum0; // number VF48 events
  Double_t sum1; // rolling sum of strip ADC values
  Double_t sum2; // rolling sum of strip ADC*ADC values
  Double_t rawadc; // rolling sum of the raw ADC values

public:
  TSiliconStripRMS();
  TSiliconStripRMS( Int_t _ModuleNumber, Int_t _ASICNumber, Int_t _StripNumber );
  virtual ~TSiliconStripRMS();

  // getters
  Int_t GetModuleNumber(){ return ModuleNumber; }
  Int_t GetASICNumber(){ return ASICNumber; }
  Int_t GetStripNumber(){ return StripNumber; }
  Int_t GetNumberEvents(){ return sum0; }
  Double_t GetStripMean(){ return sum1 / (Double_t) sum0 ; }
  Double_t GetRawStripMean() { return rawadc / sum0; }
  Double_t GetStripRMS();

  // setters
  void SetModuleNumber( Int_t _ModuleNumber ){ ModuleNumber = _ModuleNumber; }
  void SetASICNumber( Int_t _ASICNumber ){ ASICNumber = _ASICNumber; }
  void SetStripNumber( Int_t _StripNumber ){ StripNumber = _StripNumber; }

  // adders
  void AddADCValue( Double_t _ADC );
  void AddADCValue( Double_t _ADC, Double_t rawADC );


  ClassDef(TSiliconStripRMS,1)
};

#endif
