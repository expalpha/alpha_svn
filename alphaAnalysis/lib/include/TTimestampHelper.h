#ifndef _TTimestampHelper_
#define _TTimestampHelper_

#include "TObject.h"
#include "TString.h"
#include <iostream>
#include "stdlib.h" 

using namespace std;
//This object is used as a cache for timestamps in a run. This means a 
//small lump of data can be loaded as an index of all events, meaning 
//loops over all events in a file can be avoided
//JTKM

class TTimestampHelper : public TObject
{
private:
  Int_t    runNumber;
  std::vector<Double_t> VF48TS;
  std::vector<Double_t> SISTS[64];

public:

  void SetRunNumber(Int_t RunNo) { runNumber=RunNo; }
  void ReserveVFEvents(Int_t Events) { VF48TS.reserve(Events); }
  void ReserveSISEvents(Int_t Events, Int_t SISCh)
  { 
    if (SISCh==-1) return  VF48TS.reserve(Events); 
    else SISTS[SISCh].reserve(Events); 
  }
  
  void SetVF48Timestamp(Double_t Timestamp, Int_t EventNo) { VF48TS[EventNo]=Timestamp; }
  void AddVF48Timestamp(Double_t Timestamp) { VF48TS.push_back(Timestamp); }

  void SetSISTimestamp(Double_t Timestamp, Int_t EventNo, Int_t SISchannel) { SISTS[SISchannel][EventNo]=Timestamp; }
  void AddSISTimestamp(Double_t Timestamp, Int_t SISchannel) { SISTS[SISchannel].push_back(Timestamp); }
  // getters
  Int_t GetRunNumber() { return runNumber; }
  Int_t GetVF48Events() { return VF48TS.size(); } 
  Int_t GetSISEvents(Int_t SISChan) 
  {
    if(SISChan==-1) return VF48TS.size();
    else return SISTS[SISChan].size();
  }
  Int_t GetVF48EventCapacity() { return VF48TS.capacity(); }
  Int_t GetSISEventCapacity(Int_t SISChan) 
  {
    if(SISChan==-1) return VF48TS.capacity();
    else return SISTS[SISChan].capacity();
  }
  
  //Be aware this sends a COPY of the vector... you cannot add to it outside
  std::vector<Double_t> GetVF48TSVector() { return VF48TS; }
  std::vector<Double_t> GetSISTSVector(Int_t SISChan) 
  {
    if (SISChan==-1) return VF48TS; 
    else return SISTS[SISChan]; 
  }
  
  Int_t GetFirstVF48EventBefore(Double_t Timestamp);
  Int_t GetFirstSISEventBefore(Int_t SisCh, Double_t Timestamp); 

  Double_t GetTimeOfSISCount(Int_t SisCh,Int_t Entry);
   
  using TObject::Print;
  virtual void Print(Int_t Verbosity=0);
   
  // default class member functions
  TTimestampHelper();
  void ClearVectors();
  void ClearSISVectorsExcept(Int_t ChannelToKeep);
  void AddAtEnd(TTimestampHelper* addme);
  virtual ~TTimestampHelper();
  
  ClassDef(TTimestampHelper,1);

  
};

#endif
