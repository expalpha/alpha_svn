#ifndef _TUTILITIES_H 
#define _TUTILITIES_H

// TUtilities Class =========================================================================================
//
// Class collecting togther low level functions used in alphaAnalysis. 
//
// JWS 25/06/2010
//
// ==========================================================================================================

#include "TNamed.h"
#include "TFile.h"
#include "TH1.h"
#include "TH2.h"
#include "TDirectory.h"
#include "TGraph.h"
#include "TGraphErrors.h"

class TUtilities : public TNamed
{
private:
  
public:
  TUtilities();
  virtual ~TUtilities();
  
  TH1D* Get_TH1D( TDirectory* dir, char* name, int nbinsx, double xlow, double xup );
  TH1D* Get_TH1D( TDirectory* dir, char* name, char* title, int nbinsx, double xlow, double xup );
  TH2D* Get_TH2D( TDirectory* dir, char* name, int nbinsx, double xlow, double xup, int nbinsy, double ylow, double yup );
  TH2D* Get_TH2D( TDirectory* dir, char* name, char* title, int nbinsx, double xlow, double xup, int nbinsy, double ylow, double yup );
  TDirectory* Get_Directory( const char* name, TFile* location );
  TDirectory* Get_Sub_Directory( const char* sub_name, const char* dir_name, TFile* location );
  TGraph* Make_TGraph( TDirectory* dir, char* name, TH1 * hist );
  TGraphErrors* Make_TGraphErrors( TDirectory* dir, char* name, Int_t n,  Double_t* x, Double_t* y, Double_t* ex, Double_t* ey );

  ClassDef(TUtilities,1)
    
 };

#endif



