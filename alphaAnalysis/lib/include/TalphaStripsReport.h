#ifndef _TalphaStripsReport_
#define _TalphaStripsReport_

#include "TObject.h"
#include "TString.h"
#include "TDatime.h"
#include <iostream>
#include "stdlib.h" 
#include "time.h"
#include "TTimeStamp.h"
#include <vector>
#include "TH1D.h"
#include "TH2D.h"
#include "TObjArray.h"
#include "TSystem.h"
class TalphaStripsReport : public TObject
{
private:
  Int_t    runNumber;
  TString  SVNalphaStripsRevision;
  TString  SVNFull;
  TString  HostName;
  
  //Compilation file time:
  unsigned int alphaStripsExeDateBinary;

  //unsigned int StartTimeBinary;
  unsigned int StartTimeBinary;
  
  //unsigned int StopTimeBinary; 
  unsigned int StopTimeBinary; 

  TString StripFile;
  TDatime StripDate;
  TDatime AnalysisDate;
    
  TObjArray* StripSettings;
  TObjArray* Histos2D;
  TObjArray* Histos1D;
  

  int GoodVF48Events;
  int BadVF48Events;
  
  Double_t nSideQuietThresh;
  Double_t nSideNoiseyThresh;
  Double_t pSideQuietThresh;
  Double_t pSideNoiseyThresh;
  
public:

  
  // setters
  
  void SetAlphaStripsExeDateBinary(unsigned int FileTime) { alphaStripsExeDateBinary=FileTime; }
  void SetStartTimeBinary(unsigned int StartTimeBin)		{StartTimeBinary= StartTimeBin;}
  void SetStopTimeBinary(unsigned int StopTimeBin)		{StopTimeBinary= StopTimeBin;}  
 
 
  void SaveHistogram2D(TH2D* _AddMe) { Histos2D->AddLast(_AddMe);  }
  void SaveHistogram1D(TH1D* _AddMe) { Histos1D->AddLast(_AddMe); }
  
  void SetRunNumber(Int_t RunNo)         { runNumber=RunNo; }
  void SetVF48Events(int good, int bad)  { GoodVF48Events=good; BadVF48Events =bad; }
  int GetGoodVF48Events()                { return GoodVF48Events; }
  int GetBadVF48Events()                 { return BadVF48Events; }
 
  void SetSVNalphaStripsRevision(TString SVNRev)            { SVNalphaStripsRevision = SVNRev; }
  void SetSVNalphaStripsFULL(TString SVNRev)              { SVNFull= SVNRev; }
  void SetHostName(TString HostN)                         { HostName=HostN; }
  
  void SetAnalysisDate(TDatime AnalysisDatime)            {  AnalysisDate = AnalysisDatime; }
  
  void SetnSideQuietThresh( Double_t _thres )             { nSideQuietThresh=_thres; }
  void SetnSideNoiseyThresh( Double_t _thres )            { nSideNoiseyThresh=_thres; }
  void SetpSideQuietThresh( Double_t _thres )             { pSideQuietThresh=_thres; }
  void SetpSideNoiseyThresh( Double_t _thres )            { pSideNoiseyThresh=_thres; }
  
  /*
  //void SetStopTimeString(const char* StopTimeStr)		{StopTimeString= StopTimeStr;} 
  void SetStopTimeBinary(unsigned int StopTimeBin)		{StopTimeBinary= StopTimeBin;}  
  */
  Int_t GetRunNumber() { return runNumber; }
  TDatime GetAnalysisFileDate()	{ return AnalysisDate; }
  Int_t GetAnalysisDateDay()	{ return AnalysisDate.GetDay(); }
  Int_t GetAnalysisFileDateMonth()	{ return AnalysisDate.GetMonth(); }
  Int_t GetAnalysisFileDateYear()	{ return AnalysisDate.GetYear(); }
  
  TString GetSVNalphaStripsRevision() { return SVNalphaStripsRevision; }
  // setters
  TH2D* GetHistogram2D(const char* _name) 
  {
    return (TH2D*)Histos2D->FindObject(_name);
  }
  void PlotNsideRMS()
  {
    TH2D* nside=GetHistogram2D("stripRMS_nside");
    nside->Draw();
  }
  void PlotPsideRMS()
  {
    TH2D* pside=GetHistogram2D("stripRMS_pside");
    pside->Draw();
  }
  TH1D* GetHistogram1D(TString _name) 
  {
    return (TH1D*)Histos1D->FindObject(_name);
  }
  
  Double_t GetnSideQuietThresh() { return nSideQuietThresh; }
  Double_t GetnSideNoiseyThresh() { return nSideNoiseyThresh; }
  Double_t GetpSideQuietThresh() { return pSideQuietThresh; }
  Double_t GetpSideNoiseyThresh() { return pSideNoiseyThresh; }
  
  Double_t GetnSideRMSMean() {
    TH2D* nside=GetHistogram2D("stripRMS_nside");
    if (nside)
      return nside->GetMean();
    else
      return -1;
  }
  Double_t GetpSideRMSMean() {
    TH2D* pside=GetHistogram2D("stripRMS_pside");
    if (pside)
      return pside->GetMean();
    else
      return -1;
  }
  
  Int_t GetnSideQuietCount() {
    TH2D* nside=GetHistogram2D("stripRMS_nside");
    if (nside)
      return nside->Integral(0,nside->GetXaxis()->FindBin(nSideQuietThresh),0,72);
    else
      return -1;
  }
    Int_t GetnSideNoisyCount() {
    TH2D* nside=GetHistogram2D("stripRMS_nside");
    if (nside)
      return nside->Integral(nside->GetXaxis()->FindBin(nSideNoiseyThresh),1000,0,72);
    else
      return -1;
  }
  Int_t GetpSideQuietCount() {
    TH2D* pside=GetHistogram2D("stripRMS_pside");
    if (pside)
      return pside->Integral(0,pside->GetXaxis()->FindBin(pSideQuietThresh),0,72);
    else
      return -1;
  }
  Int_t GetpSideNoisyCount() {
    TH2D* pside=GetHistogram2D("stripRMS_pside");
    if (pside)
      return pside->Integral(pside->GetXaxis()->FindBin(pSideNoiseyThresh),1000,0,72);
    else
      return -1;
  }
  
  void PrintHistos2D(Int_t Verbosity=0);
  
  void PrintQuietStrips(Int_t Verbosity=0);
  using TObject::Print;
  virtual void Print(Int_t Verbosity=0);
  
  // default class member functions
  TalphaStripsReport();
  virtual ~TalphaStripsReport();
  
  ClassDef(TalphaStripsReport,1);

  
};


#endif
