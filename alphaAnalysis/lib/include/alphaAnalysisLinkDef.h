#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

//#pragma link C++ class TvmcVersion+;
#pragma link C++ class TSeq_Event+; 
#pragma link C++ class TSeq_State+;
#pragma link C++ class DigitalOut+;
#pragma link C++ class AnalogueOut+;
#pragma link C++ class TriggerIn+;
#pragma link C++ class TSequencerDriver+;
#pragma link C++ struct TSequencerDriverMap+;
#pragma link C++ class TLabVIEWEvent+;
#pragma link C++ class TTCEvent+;
#pragma link C++ class TSISChannels+;
#pragma link C++ class TSisEvent+;
#pragma link C++ class TADCTriggerEvent+;
#pragma link C++ class TSiliconPedestals+;
#pragma link C++ class TSiliconStripRMS+;
#pragma link C++ class TSiliconStrip+;
#pragma link C++ class TSiliconModule+;
#pragma link C++ class TSiliconVA+;
#pragma link C++ class TSiliconEvent+;
#pragma link C++ class TVF48SiMap+;
#pragma link C++ class TUtilities+;
#pragma link C++ class TSpill+;
#pragma link C++ class TSeq_Dump+;
#pragma link C++ class TSettings+;
#pragma link C++ class TTimestampHelper+;
#pragma link C++ class TSeq_EventHelper+;
#pragma link C++ class TAnalysisReport+;
#pragma link C++ class TalphaStripsReport+;
#pragma link C++ namespace TRootUtils+;
#pragma link C++ class TAlphaPlot+;

#pragma link C++ class Seq+;
#pragma link C++ class Seq_DriverConsts+;
#pragma link C++ class SeqXML_Obj+;
#pragma link C++ class SeqXML+;
#pragma link C++ class SeqXML_DriverConsts+;
#pragma link C++ class SeqXML_AOChn+;
#pragma link C++ class SeqXML_DOChn+;
#pragma link C++ class SeqXML_HVElec++;
#pragma link C++ class SeqXML_IOConfig++;
#pragma link C++ class SeqXML_State+;
#pragma link C++ class SeqXML_Event+;
#pragma link C++ class SeqXML_ChainLink+;

#endif
