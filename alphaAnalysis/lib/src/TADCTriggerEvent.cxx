#include "TADCTriggerEvent.h" 
#include "TVector.h"
#include "TVector3.h"

ClassImp(TADCTriggerEvent);


////////////////////////////////////////////////////////////////////////
//
//  TADCTriggerEvent  is an object describing an ADC trigger in the SIS  
//
////////////////////////////////////////////////////////////////////////

//__________________
TADCTriggerEvent::TADCTriggerEvent()
{
  ClearEvent();
}

TADCTriggerEvent::~TADCTriggerEvent()
{
}

void TADCTriggerEvent::ClearEvent()
{
  SIS_ADC_TrigNumber = -1;
  ExptNumber = -1;
  RunTime = -999.;
  ExptTime = -999.;
}

void TADCTriggerEvent::Print()
{
  printf( "Trig number = %d \t RunTime = %f \t ExptTime = %f \t ExptNumber = %d \n", SIS_ADC_TrigNumber, RunTime, ExptTime, ExptNumber );
}
