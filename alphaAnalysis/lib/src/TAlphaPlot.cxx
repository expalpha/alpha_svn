#include "TAlphaPlot.h"
#define SCALECUT 0.6

using namespace TRootUtils;
ClassImp(TAlphaPlot);

//Default Constructor
TAlphaPlot::TAlphaPlot(Bool_t ApplyCuts, Int_t MVAMode)
{
  Nbin=100; 
  DrawStyle=0;
  gLegendDetail=1; 

  TMin=-1;
  TMax=-1;
  hz=NULL;
  hr=NULL;
  hphi=NULL;
  hxy=NULL;
  hzr=NULL;
  hzt=NULL;
  hzphi=NULL;
  hphit=NULL;
  ht = NULL;
  ht_MVA = NULL;
  ht_IO32 = NULL;
  ht_IO32_sistime = NULL;
  ht_IO32_notbusy = NULL;
  ht_ATOM_OR = NULL;
  trig=-1;// = -1;
  trig_nobusy=-1; //Record of SIS channels
  atom_or=-1;
  Beam_Injection=-1;
  Beam_Ejection=-1;
  
  CATStart=-1;
  CATStop=-1;
  RCTStart=-1;
  RCTStop=-1;
  ATMStart=-1;
  ATMStop=-1;
  
  SetMVAMode(MVAMode);
  gApplyCuts=ApplyCuts;
}

TAlphaPlot* TAlphaPlot::LoadTAlphaPlot(TString file)
{
  TFile* f0=new TFile(file);
  TAlphaPlot* a;//=new TAlphaPlot();
  f0->GetObject("TAlphaPlot",a);
  f0->Close();
  delete f0;
  return a;
}

void TAlphaPlot::Draw(Option_t *option)
{
  TCanvas* a=Canvas("TAlphaPlot");
  a->Draw();
  return;
}

void TAlphaPlot::Print()
{
  cout<<"TAlphaPlot Summary"<<endl;
  FillHisto();
  cout <<""<<endl<<"Run(s): ";
  for (UInt_t i=0; i<Runs.size(); i++)
  {
     if (i>1) cout <<", ";
     cout <<Runs[i];
  }
  cout <<endl;
  if (ht_MVA)
    cout <<"Pass MVA:      "<<ht_MVA->Integral() <<endl;
  if (ht)
    cout<<"Passed Cuts:    "<<ht->Integral() <<endl;
  if (ht_IO32_sistime)
    cout<<"VF48 Events:    "<<ht_IO32_sistime->Integral()<<endl;
  if (ht_IO32)
    cout<<"SIS readout:    "<<ht_IO32->Integral()<<endl;
  if (ht_IO32_notbusy)
    cout<<"IO32 NotBusy:   "<<ht_IO32_notbusy->Integral() <<endl;
  
}

void TAlphaPlot::ExportCSV(TString filename, Bool_t PassedCutOnly)
{
  
  if (!filename.EndsWith(".csv"))
  {
    TString tmp("TAlphaPlot");
    tmp+=filename;
    tmp+="_vtx.csv";
    filename=tmp;
  }
  ofstream SaveCSV (filename);

  SaveCSV<<"RunNumber,Event Number,RunTime,NVerts,x,y,z,t,Passed Cut,Passed MVA"<<endl;
  for (UInt_t i=0; i<VertexEvents.size(); i++)
  {
    //Skip events that fail cuts if only saving passed cuts
    if (PassedCutOnly && !VertexEvents[i].CutsResult ) continue; 
    SaveCSV<<VertexEvents[i].runNumber<<",";
    SaveCSV<<VertexEvents[i].EventNo<<",";
    SaveCSV<<VertexEvents[i].RunTime<<",";
    SaveCSV<<VertexEvents[i].NVerts<<",";
    SaveCSV<<VertexEvents[i].x<<",";
    SaveCSV<<VertexEvents[i].y<<",";
    SaveCSV<<VertexEvents[i].z<<",";
    SaveCSV<<VertexEvents[i].t<<",";
    SaveCSV<<(VertexEvents[i].CutsResult&1)<<",";
    SaveCSV<<(VertexEvents[i].CutsResult&2)<<","<<endl;
  }
  SaveCSV.close();
  
}

//Default Destructor
TAlphaPlot::~TAlphaPlot()
{
  ClearHisto();
  Ejections.clear();
  Injections.clear();
  DumpStarts.clear();
  DumpStops.clear();
  SISChannels.clear();
  Runs.clear();
  VertexEvents.clear();
  SISEvents.clear();
}

void TAlphaPlot::ClearHisto() //Destroy all histograms
{
  if (hz)
  {
    delete hz;
    hz=NULL;
  }
  if (hr)
  {
    delete hr;
    hr=NULL;
  }
  if (hphi)
  {
    delete hphi;
    hphi=NULL;
  }
  if (hxy)
  {
    delete hxy;
    hxy=NULL;
  }
  if (hzr)
  {
    delete hzr;
    hzr=NULL;
  }
  if (hzt)
  {
    delete hzt;
    hzt=NULL;
  }
  if (hzphi)
  {
    delete hzphi;
    hzphi=NULL;
  }
  if (hphit)
  {
    delete hphit;
    hphit=NULL;
  }
  if (ht)
  {
    delete ht;
    ht=NULL;
  }
  if (ht_MVA)
  {
    delete ht_MVA;
    ht_MVA=NULL;
  }
  if (ht_IO32)
  {
    delete ht_IO32;
    ht_IO32=NULL;
  }
  if (ht_IO32_sistime)
  {
    delete ht_IO32_sistime;
    ht_IO32_sistime=NULL;
  }  
  if (ht_IO32_notbusy)
  {
    delete ht_IO32_notbusy;
    ht_IO32_notbusy=NULL;
  } 
  if (ht_ATOM_OR)
  {
    delete ht_ATOM_OR;
    ht_ATOM_OR=NULL;
  }
}



void TAlphaPlot::AddToTAlphaPlot(TAlphaPlot *ialphaplot)
{
  ClearHisto();
  SISEvents.insert(SISEvents.end(), ialphaplot->SISEvents.begin(), ialphaplot->SISEvents.end());
  VertexEvents.insert(VertexEvents.end(), ialphaplot->VertexEvents.begin(), ialphaplot->VertexEvents.end());
  Ejections.insert(Ejections.end(), ialphaplot->Ejections.begin(), ialphaplot->Ejections.end());
  Injections.insert(Injections.end(), ialphaplot->Injections.begin(), ialphaplot->Injections.end());
  Runs.insert(Runs.end(), ialphaplot->Runs.begin(), ialphaplot->Runs.end());
  //Draw();
}

void TAlphaPlot::AddToTAlphaPlot(TString file)
{
  TAlphaPlot* tmp=LoadTAlphaPlot(file);
  AddToTAlphaPlot(tmp);
  delete tmp;
  return;
}


void TAlphaPlot::AddSilEvent(TSiliconEvent *sil_event, Double_t StartOffset)
{
  VertexEvent Event;
  TVector3 *vtx = sil_event->GetVertex();
  Event.EventNo= sil_event->GetVF48NEvent();
  Event.runNumber = sil_event->GetRunNumber();
  Event.RunTime= sil_event->GetRunTime();
  Event.t= sil_event->GetRunTime() - StartOffset;
  Event.NVerts=sil_event->GetNVertices();
  Event.x=vtx->X();
  Event.y=vtx->Y();
  Event.z=vtx->Z();
  //Event.vtx=vtx;
  Int_t CutsResult = 3;

  //ht_IO32_sistime->Fill(run_time);
  if (MVAMode > 0)
  {
    CutsResult=TRootUtils::ApplyMVA(sil_event);
  }
  else
  {
    if (gApplyCuts)
      CutsResult=TRootUtils::ApplyCuts(sil_event);
  }
  Event.CutsResult=CutsResult;
  
  
  /*x = vtx->X();
  y = vtx->Y();
  z = vtx->Z();
  phi = vtx->Phi();
  hphi->Fill(phi);
  hzphi->Fill(z, phi);
  hxy->Fill(x, y);
  hz->Fill(z);
  hr->Fill(r);
  hzr->Fill(z, r);
  hzt->Fill(z, run_time);*/
  VertexEvents.push_back(Event);
  sil_event->ClearEvent(); // very important!
  return;
}

void TAlphaPlot::AddSisEvent(TSisEvent *sis_event, Double_t StartOffset)
{
  SISEvent Event;
  Event.runNumber=sis_event->GetRunNumber();
  Event.Counts=sis_event->GetCountsInChannel();
  Event.SIS_Channel=sis_event->GetChannel();
  Event.RunTime=sis_event->GetRunTime();
  Event.t=sis_event->GetRunTime()-StartOffset;
  SISEvents.push_back(Event);
}

//Maybe dont have this function... lets see...
void TAlphaPlot::AddEvents(Int_t runNumber, char *description, Int_t repetition, Double_t Toffset, Bool_t zeroTime)
{
  Double_t start_time = TRootUtils::MatchEventToTime(runNumber, "startDump", description, repetition);
  Double_t stop_time = TRootUtils::MatchEventToTime(runNumber, "stopDump", description, repetition);
  AddEvents(runNumber, start_time, stop_time, Toffset, zeroTime);
}

void TAlphaPlot::AddEvents(Int_t runNumber, Double_t tmin, Double_t tmax, Double_t Toffset, Bool_t zeroTime)
{
  if (TMax < 0. && TMin < 0.)
  {
    std::cout << "Setting time range" << std::endl;
    if (zeroTime)
      SetTimeRange(0. - Toffset, tmax - tmin - Toffset);
    else
      SetTimeRange(tmin - Toffset, tmax - Toffset);
    PrintTimeRange();

  } // If plot range not set... use first instance of range
  if (trig < 0.)
    SetSISChannels(runNumber);
  //cout <<"Sis channels set."<<endl;
  //Add Silicon Events:
  //cout <<"Adding Silicon Events"<<endl;

  if (std::find(Runs.begin(), Runs.end(), runNumber) == Runs.end() || Runs.size() == 0)
  {
    //cout <<"Adding runNo"<<endl;
    Runs.push_back(runNumber);
  }
  TSiliconEvent *sil_event = new TSiliconEvent();
  Double_t run_time;
  TTree *t0 = TRootUtils::Get_Vtx_Tree(runNumber);
  t0->SetBranchAddress("SiliconEvent", &sil_event);
  t0->SetBranchStatus("SiliconModules", 0);
  for (Int_t i = GetFirstVF48EventBeforeTime(runNumber,tmin); i < t0->GetEntries(); ++i)
  {
    t0->GetEntry(i);
    if (!sil_event)
      break;
    run_time = sil_event->GetRunTime();
    if (run_time <= tmin)
    {
      sil_event->ClearEvent();
      continue;
    }
    if (run_time > tmax)
    {
      sil_event->ClearEvent();
      break;
    }
    if (zeroTime)
      AddSilEvent(sil_event, Toffset + tmin);
    else
      AddSilEvent(sil_event, Toffset);
  }
  delete sil_event;
  delete t0;
  //Add SIS Events:
  //cout <<"Adding SIS Events"<<endl;
  //SetSISChannels(runNumber);


  for (UInt_t j=0; j<SISChannels.size(); j++)
  {
    TTree *sis_tree = TRootUtils::Get_Sis_Tree(runNumber, SISChannels[j]);
    TSisEvent *sis_event = new TSisEvent();
    sis_tree->SetBranchAddress("SisEvent", &sis_event);
    for (Int_t i = GetFirstSISEventBeforeTime(runNumber,SISChannels[j],tmin); i < sis_tree->GetEntriesFast(); ++i)
    {
      sis_tree->GetEntry(i);
      run_time = sis_event->GetRunTime();
      if (run_time <= tmin)
        continue;
      if (run_time > tmax)
        break;
      if (zeroTime)
        AddSisEvent(sis_event, Toffset + tmin);
      else
        AddSisEvent(sis_event, Toffset);
    }
    delete sis_event;
    delete sis_tree;
  }
}

void TAlphaPlot::SetSISChannels(Int_t runNumber)
{

  TSISChannels *sisch = new TSISChannels(runNumber);
  trig =           sisch->GetChannel("IO32_TRIG");
  trig_nobusy =    sisch->GetChannel("IO32_TRIG_NOBUSY");
  atom_or =        sisch->GetChannel("SIS_PMT_ATOM_OR");
  Beam_Injection = sisch->GetChannel("SIS_AD");
  Beam_Ejection =  sisch->GetChannel("SIS_AD_2");
  CATStart =       sisch->GetChannel("SIS_PBAR_DUMP_START");
  CATStop =        sisch->GetChannel("SIS_PBAR_DUMP_STOP");
  RCTStart =       sisch->GetChannel("SIS_RECATCH_DUMP_START");
  RCTStop =        sisch->GetChannel("SIS_RECATCH_DUMP_STOP");
  ATMStart =       sisch->GetChannel("SIS_ATOM_DUMP_START");
  ATMStop =        sisch->GetChannel("SIS_ATOM_DUMP_STOP");


  //Add all valid SIS channels to a list for later:
  if (trig>0)           SISChannels.push_back(trig);
  if (trig_nobusy>0)    SISChannels.push_back(trig_nobusy);
  if (atom_or>0)        SISChannels.push_back(atom_or);
  if (Beam_Injection>0) SISChannels.push_back(Beam_Injection);
  if (Beam_Ejection>0)  SISChannels.push_back(Beam_Ejection);
  if (CATStart>0)       SISChannels.push_back(CATStart);
  if (CATStop>0)        SISChannels.push_back(CATStop);
  if (RCTStart>0)       SISChannels.push_back(RCTStart);
  if (RCTStop>0)        SISChannels.push_back(RCTStop);
  if (ATMStart>0)       SISChannels.push_back(ATMStart);
  if (ATMStop>0)        SISChannels.push_back(ATMStop);
  //cout <<"Trig:"<<trig<<endl;
  //cout <<"TrigNoBusy:"<<trig_nobusy<<endl;
  //cout <<"Beam Injection:"<<Beam_Injection<<endl;
  //cout <<"Beam Ejection:"<<Beam_Ejection<<endl;
  delete sisch;
  return;
}

void TAlphaPlot::AutoTimeRange()
{
  cout <<"Automatically setting time range for histrograms: ";
  TMax=GetSilEventMaxT();
  TMin=GetSilEventMinT();
  cout <<TMin<<"s - "<<TMax<<"s"<<endl;
}

void TAlphaPlot::SetTimeRange(Double_t tmin_, Double_t tmax_)
{
  TMin = tmin_;
  TMax = tmax_;
  SetUpHistograms();
  return;
}

Double_t TAlphaPlot::GetSilEventMaxT()
{
  Double_t max=-1;
  for (UInt_t i=0; i<VertexEvents.size(); i++)
  {
    if (max<VertexEvents[i].t) max=VertexEvents[i].t;
  }
  return max;
}

Double_t TAlphaPlot::GetSISEventMaxT()
{
  Double_t max=-1;
  for (UInt_t i=0; i<SISEvents.size(); i++)
  {
    if (max<SISEvents[i].t) max=SISEvents[i].t;
  }
  return max;
}

Double_t TAlphaPlot::GetSilEventMinT()
{
  Double_t min=9999999.;
  for (UInt_t i=0; i<VertexEvents.size(); i++)
  {
    if (min>VertexEvents[i].t) min=VertexEvents[i].t;
  }
  return min;
}

Double_t TAlphaPlot::GetSISEventMinT()
{
  Double_t min=9999999.;
  for (UInt_t i=0; i<SISEvents.size(); i++)
  {
    if (min>SISEvents[i].t) min=SISEvents[i].t;
  }
  return min;
}

void TAlphaPlot::SetUpHistograms()
{
  Bool_t ScaleAsMiliSeconds=kFALSE;
  if (fabs(TMax-TMin)<SCALECUT)
    ScaleAsMiliSeconds=kTRUE;
  hz = new TH1D("zvtx", "Z Vertex;z [cm];events", Nbin, -26., 26.);
  hr = new TH1D("rvtx", "R Vertex;r [cm];events", Nbin, 0., 5.);
  hr->SetMinimum(0);
  hphi = new TH1D("phivtx", "phi Vertex;phi [rad];events", Nbin, -TMath::Pi(), TMath::Pi());
  hphi->SetMinimum(0);
  hxy = new TH2D("xyvtx", "X-Y Vertex;x [cm];y [cm]", Nbin, -5., 5., Nbin, -5., 5.);
  hzr = new TH2D("zrvtx", "Z-R Vertex;z [cm];r [cm]", Nbin, -26., 26., Nbin, 0., 5.);
  hzphi = new TH2D("zphivtx", "Z-Phi Vertex;z [cm];phi [rad]", Nbin, -26., 26., Nbin, -TMath::Pi(), TMath::Pi());
  if (ScaleAsMiliSeconds)
  {
    ht = new TH1D("tvtx", "t Vertex;t [ms];events", Nbin, TMin*1000., TMax*1000.);
    hzt = new TH2D("ztvtx", "Z-T Vertex;z [cm];t [ms]", Nbin, -26., 26., Nbin, TMin*1000., TMax*1000.);
    hphit = new TH2D("phitvtx", "Phi-T Vertex;phi [rad];t [s]", Nbin,-TMath::Pi(), TMath::Pi() ,  Nbin,TMin*1000., TMax*1000);
    if (MVAMode)
      ht_MVA = new TH1D("htMVA", "Vertex, Passcut and MVA;t [ms];Counts", Nbin, TMin*1000., TMax*1000.);
    ht_IO32 = new TH1D("hIO32", "IO32, IO32_NoBusy and Passed Cut;t [ms];Counts", Nbin, TMin*1000., TMax*1000.);
    ht_ATOM_OR = new TH1D("hATOM_OR", "ATOM_OR PMTs;t [ms];Counts", Nbin, TMin*1000., TMax*1000.);
    ht_IO32_notbusy = new TH1D("hIO32_notbusy", "IO32, IO32_NoBusy and Passed Cut;t [ms];Counts", Nbin, TMin*1000., TMax*1000.);
    ht_IO32_sistime = new TH1D("hIO32_sistime", "hIO32_sistime", Nbin, TMin*1000., TMax*1000.);
  }
  else
  {
    ht = new TH1D("tvtx", "t Vertex;t [s];events", Nbin, TMin, TMax);
    hzt = new TH2D("ztvtx", "Z-T Vertex;z [cm];t [s]", Nbin, -26., 26., Nbin, TMin, TMax);
    hphit = new TH2D("phitvtx", "Phi-T Vertex;phi [rad];t [s]", Nbin,-TMath::Pi(), TMath::Pi() ,  Nbin,TMin, TMax);
    if (MVAMode)
      ht_MVA = new TH1D("htMVA", "Vertex, Passcut and MVA;t [s];Counts", Nbin, TMin, TMax);
    ht_IO32 = new TH1D("hIO32", "IO32, IO32_NoBusy and Passed Cut;t [s];Counts", Nbin, TMin, TMax);
    ht_ATOM_OR = new TH1D("hATOM_OR", "ATOM_OR PMTs;t [s];Counts", Nbin, TMin, TMax);
    ht_IO32_notbusy = new TH1D("hIO32_notbusy", "IO32, IO32_NoBusy and Passed Cut;t [s];Counts", Nbin, TMin, TMax);
    ht_IO32_sistime = new TH1D("hIO32_sistime", "hIO32_sistime", Nbin, TMin, TMax);
  }
  return;
}

void TAlphaPlot::FillHisto()
{
  if (TMin<0 && TMax<0.) AutoTimeRange();
  ClearHisto();
  SetUpHistograms();
  
  //Fill SIS histograms
  
   for (UInt_t i=0; i<SISEvents.size(); i++)
  {
    
    if (trig < 0.)
    {
      std::cout << "Warning: TAlphaPlot->SetSISChannels(runNumber) should have been called before AddSisEvent" << std::endl;
      SetSISChannels(SISEvents[i].runNumber);
    }
    Double_t time = SISEvents[i].t;
    if (fabs(TMax-TMin)<SCALECUT) time=time*1000.;
    Int_t Channel = SISEvents[i].SIS_Channel;
    Int_t CountsInChannel = SISEvents[i].Counts;
    if (Channel == trig)
      ht_IO32->Fill(time, CountsInChannel);
    else if (Channel == trig_nobusy)
      ht_IO32_notbusy->Fill(time, CountsInChannel);
    else if (Channel == atom_or)
      ht_ATOM_OR->Fill(time, CountsInChannel);
    else if (Channel == Beam_Injection)
      Injections.push_back(time);
    else if (Channel == Beam_Ejection)
      Ejections.push_back(time);
    else if (Channel == CATStart || Channel == RCTStart || Channel == ATMStart)
      DumpStarts.push_back(time);
    else if (Channel == CATStop || Channel == RCTStop || Channel == ATMStop)
      DumpStops.push_back(time);
    else cout <<"Unconfigured SIS channel in TAlhaPlot"<<endl;
  }
  
  
  //Fill Vertex Histograms
  
  
  TVector3* vtx;
  for (UInt_t i=0; i<VertexEvents.size(); i++)
  {
    Double_t time = VertexEvents[i].t;
    if (time<TMin) continue;
    if (time>TMax) continue; //Cannot assume events are in order... cannot use break
    if (fabs(TMax-TMin)<SCALECUT) time=time*1000.;
    vtx=new TVector3(VertexEvents[i].x,VertexEvents[i].y,VertexEvents[i].z);
    ht_IO32_sistime->Fill(time);
    Int_t CutsResult=VertexEvents[i].CutsResult;
    if (MVAMode>0)
    {
      if (CutsResult & 1)
        ht->Fill(time); //Passed cut result!
      if (CutsResult & 2)
        ht_MVA->Fill(time); //MVA result!
      else
        continue; //Don't draw vertex if it tails MVA cut
    }
    else
    {
      if (gApplyCuts)
      {
        if (CutsResult & 1 )
          ht->Fill(time); //Passed cut result!
        else
          continue;
      }
      else
      {
        if ( VertexEvents[i].NVerts > 0) 
          ht->Fill(time); //Has Vertex and Cuts off!
        else 
          continue;
      }
      if (VertexEvents[i].NVerts != 1) continue; //Don't draw invaid vertices
    }
    hphi->Fill(vtx->Phi());
    hzphi->Fill(vtx->Z(), vtx->Phi());
    hphit->Fill(vtx->Phi(),time);
    hxy->Fill(vtx->X(), vtx->Y());
    hz->Fill(vtx->Z());
    hr->Fill(vtx->Perp());
    hzr->Fill(vtx->Z(), vtx->Perp());
    hzt->Fill(vtx->Z(), time);
    delete vtx;
  }
}

TObjArray TAlphaPlot::GetHisto()
{
  FillHisto();
  TH1D *hrdens = (TH1D *)hr->Clone("radial density");
  hrdens->Sumw2();
  TF1 *fr = new TF1("fr", "x", -100, 100);
  hrdens->Divide(fr);
  hrdens->Scale(hr->GetBinContent(hr->GetMaximumBin()) / hrdens->GetBinContent(hrdens->GetMaximumBin()));
  hrdens->SetMarkerColor(kRed);
  hrdens->SetLineColor(kRed);
  delete fr;

  ht_IO32->SetLineColor(kGreen);
  ht_IO32->SetMarkerColor(kGreen);
  ht_IO32->SetMinimum(0);
  ht_IO32_notbusy->SetMarkerColor(kRed);
  ht_IO32_notbusy->SetLineColor(kRed);
  ht_IO32_notbusy->SetMinimum(0);
  ht_IO32_sistime->SetLineColor(kAzure - 8);
  ht_IO32_sistime->SetMarkerColor(kAzure - 8);
  ht_IO32_sistime->SetMinimum(0);
  ht_ATOM_OR->SetLineColor(kMagenta - 9);
  ht_ATOM_OR->SetMarkerColor(kMagenta - 9);
  ht_ATOM_OR->SetMinimum(0);

  TObjArray histos(MVAMode?VERTEX_HISTO_TMVA+1:VERTEX_HISTO_TMVA);
  
  histos.AddAt(hz,VERTEX_HISTO_Z);
  histos.AddAt(hr,VERTEX_HISTO_R);
  histos.AddAt(hxy,VERTEX_HISTO_XY);
  histos.AddAt(hzr,VERTEX_HISTO_ZR);
  histos.AddAt(hzt,VERTEX_HISTO_ZT);
  histos.AddAt(ht,VERTEX_HISTO_T);
  histos.AddAt(ht_IO32,VERTEX_HISTO_IO32);
  histos.AddAt(ht_IO32_notbusy,VERTEX_HISTO_IO32_NOTBUSY);
  histos.AddAt(ht_ATOM_OR,VERTEX_HISTO_ATOM_OR);
  histos.AddAt(hphi,VERTEX_HISTO_PHI);
  histos.AddAt(hzphi,VERTEX_HISTO_ZPHI);
  histos.AddAt(hrdens,VERTEX_HISTO_RDENS);
  histos.AddAt(ht_IO32_sistime,VERTEX_HISTO_VF48);
  histos.AddAt(hphit,VERTEX_HISTO_TPHI);
  if (MVAMode)
  {
    ht_MVA->SetMarkerColor(kViolet);
    ht_MVA->SetLineColor(kViolet);
    ht_MVA->SetMinimum(0);
    histos.AddAt(ht_MVA,VERTEX_HISTO_TMVA);
  }
  return histos;
}

TCanvas *TAlphaPlot::Canvas(TString Name)
{
  TObjArray hh = GetHisto();
  TCanvas *cVTX = new TCanvas(Name, Name, 1600, 800);
  //Scale factor to scale down to ms:
  Double_t tFactor=1.;
  if (fabs(TMax-TMin)<SCALECUT) tFactor=1000.;
  cVTX->Divide(4, 2);

  if (gLegendDetail >= 1)
  {
    gStyle->SetOptStat(11111);
  }
  else
  {
    gStyle->SetOptStat("ni");	//just like the knights of the same name
  }

  cVTX->cd(1);
  //cVTX->cd(1)->SetFillStyle(4000 );
  // R-counts
  ((TH1D *)hh[VERTEX_HISTO_R])->Draw("HIST E1");
  ((TH1D *)hh[VERTEX_HISTO_RDENS])->Draw("HIST E1 SAME");
  TPaveText *rdens_label = new TPaveText(0.6, 0.8, 0.90, 0.85, "NDC NB");
  rdens_label->AddText("radial density [arbs]");
  rdens_label->SetTextColor(kRed);
  rdens_label->SetFillStyle(0);
  rdens_label->SetLineStyle(0);
  rdens_label->Draw();

  cVTX->cd(2); // Z-counts (with electrodes?)4
  TVirtualPad *cVTX_1 = cVTX->cd(2);
  gPad->Divide(1, 2);
  cVTX_1->cd(1);
  //cVTX->cd(2)->SetFillStyle(4000 );
  ((TH1D *)hh[VERTEX_HISTO_Z])->Draw("HIST E1");
  cVTX_1->cd(2);

  ((TH1D *)hh[VERTEX_HISTO_PHI])->Draw("HIST E1");

  cVTX->cd(3); // T-counts
  //cVTX->cd(3)->SetFillStyle(4000 );
  ((TH1D *)hh[VERTEX_HISTO_IO32_NOTBUSY])->Draw("HIST"); // io32-notbusy = readouts
  ((TH1D *)hh[VERTEX_HISTO_IO32])->Draw("HIST SAME");    // io32
  ((TH1D *)hh[VERTEX_HISTO_ATOM_OR])->Draw("HIST SAME");    // ATOM OR PMTs
  ((TH1D *)hh[VERTEX_HISTO_T])->Draw("HIST SAME");       //verticies
  ((TH1D *)hh[VERTEX_HISTO_VF48])->Draw("HIST SAME");    //io32 sistime
  if (MVAMode)
    ((TH1D *)hh[VERTEX_HISTO_TMVA])->Draw("HIST SAME"); //MVA results

  //auto legend = new TLegend(0.1,0.7,0.48,0.9);(0.75, 0.8, 1.0, 0.95
  //auto legend = new TLegend(1., 0.7, 0.45, 1.);//, "NDC NB");
  auto legend = new TLegend(1, 0.7, 0.55, .95); //, "NDC NB");
  char line[201];
  snprintf(line, 200, "IO32 NotBusy: %5.0lf", ((TH1D *)hh[VERTEX_HISTO_IO32_NOTBUSY])->Integral());
  legend->AddEntry(hh[VERTEX_HISTO_IO32_NOTBUSY], line, "f");
  snprintf(line, 200, "ATOM_OR: %5.0lf", ((TH1D *)hh[VERTEX_HISTO_ATOM_OR])->Integral());
  legend->AddEntry(hh[VERTEX_HISTO_ATOM_OR], line, "f");
  snprintf(line, 200, "IO32 sis time: %5.0lf", ((TH1D *)hh[VERTEX_HISTO_IO32])->Integral());
  legend->AddEntry(hh[VERTEX_HISTO_IO32], line, "f");
  snprintf(line, 200, "IO32 vf48 time: %5.0lf", ((TH1D *)hh[VERTEX_HISTO_VF48])->Integral());
  legend->AddEntry(hh[VERTEX_HISTO_VF48], line, "f");
  if (MVAMode)
  {
    snprintf(line, 200, "Pass Cuts: %5.0lf", ((TH1D *)hh[VERTEX_HISTO_T])->Integral());
    legend->AddEntry(hh[VERTEX_HISTO_T], line, "f");
    snprintf(line, 200, "Pass MVA (rfcut %0.1f): %5.0lf", grfcut, ((TH1D *)hh[VERTEX_HISTO_TMVA])->Integral());
    legend->AddEntry(hh[VERTEX_HISTO_TMVA], line, "f");
  }
  else
  {
    if (gApplyCuts)
      snprintf(line, 200, "Pass Cuts: %5.0lf", ((TH1D *)hh[VERTEX_HISTO_T])->Integral());
    else
      snprintf(line, 200, "vertices: %5.0lf", ((TH1D *)hh[VERTEX_HISTO_T])->Integral());
    legend->AddEntry(hh[VERTEX_HISTO_T], line, "f");
    legend->SetFillColor(kWhite);
    legend->SetFillStyle(1001);
    //std::cout <<"Drawing lines"<<std::endl;

    for (UInt_t i = 0; i < Injections.size(); i++)
    {
      TLine *l = new TLine(Injections[i]*tFactor, 0., Injections[i]*tFactor, ((TH1D *)hh[VERTEX_HISTO_IO32_NOTBUSY])->GetMaximum());
      l->SetLineColor(6);
      l->Draw();
      if (i == 0)
        legend->AddEntry(l, "AD fill", "l");
    }
    for (UInt_t i = 0; i < Ejections.size(); i++)
    {
      TLine *l = new TLine(Ejections[i]*tFactor, 0., Ejections[i]*tFactor, ((TH1D *)hh[VERTEX_HISTO_IO32_NOTBUSY])->GetMaximum());
      l->SetLineColor(7);
      l->Draw();
      if (i == 0)
        legend->AddEntry(l, "Beam to ALPHA", "l");
    }
    for (UInt_t i = 0; i < DumpStarts.size(); i++)
    {
      if (DumpStarts.size() > 4) continue; //Don't draw dumps if there are lots
      TLine *l = new TLine(DumpStarts[i]*tFactor, 0., DumpStarts[i]*tFactor, ((TH1D *)hh[VERTEX_HISTO_IO32_NOTBUSY])->GetMaximum());
      //l->SetLineColor(7);
      l->SetLineColorAlpha(kGreen, 0.35);
      //l->SetFillColorAlpha(kGreen,0.35);
      l->Draw();
      if (i == 0)
        legend->AddEntry(l, "Dump Start", "l");
    }
    for (UInt_t i = 0; i < DumpStops.size(); i++)
    {
      if (DumpStops.size() > 4) continue; //Don't draw dumps if there are lots
      TLine *l = new TLine(DumpStops[i]*tFactor, 0., DumpStops[i]*tFactor, ((TH1D *)hh[VERTEX_HISTO_IO32_NOTBUSY])->GetMaximum());
      //l->SetLineColor(7);
      l->SetLineColorAlpha(kRed, 0.35);
      l->Draw();
      if (i == 0)
        legend->AddEntry(l, "Dump Stop", "l");
    }
  }

  // legend->AddEntry("f1","Function abs(#frac{sin(x)}{x})","l");
  // legend->AddEntry("gr","Graph with error bars","lep");
  legend->Draw();
  cVTX->cd(4);
  // X-Y-counts
  //cVTX->cd(4)->SetFillStyle(4000 );
  ((TH2D *)hh[VERTEX_HISTO_XY])->Draw("colz");

  cVTX->cd(5);
  // Z-R-counts
  //cVTX->cd(5)->SetFillStyle(4000 );
  ((TH2D *)hh[VERTEX_HISTO_TPHI])->Draw("colz");

  cVTX->cd(6);
  // Z-T-counts
  //cVTX->cd(6)->SetFillStyle(4000 );
  ((TH2D *)hh[VERTEX_HISTO_ZT])->Draw("colz");

  cVTX->cd(7);
  // phi counts
  //cVTX->cd(7)->SetFillStyle(4000 );
  ((TH1D *)hh[VERTEX_HISTO_IO32_NOTBUSY])->SetStats(0);
  ((TH1D *)hh[VERTEX_HISTO_IO32_NOTBUSY])->GetCumulative()->Draw("HIST");
  ((TH1D *)hh[VERTEX_HISTO_IO32])->GetCumulative()->Draw("HIST SAME");
  ((TH1D *)hh[VERTEX_HISTO_T])->GetCumulative()->Draw("HIST SAME");
  ((TH1D *)hh[VERTEX_HISTO_VF48])->GetCumulative()->Draw("HIST SAME");

  if (MVAMode)
  {
    ((TH1D *)hh[VERTEX_HISTO_TMVA])->GetCumulative()->Draw("HIST SAME");
    TH1 *h = ((TH1D *)hh[VERTEX_HISTO_TMVA])->GetCumulative();
    {
      //Draw line at halfway point
      Double_t Max = h->GetBinContent(h->GetMaximumBin());
      Double_t Tmax = h->GetXaxis()->GetXmax();
      for (Int_t i = 0; i < h->GetMaximumBin(); i++)
      {
        if (h->GetBinContent(i) > Max / 2.)
        {
          TLine *half = new TLine(TMax*tFactor * (Double_t)i / (Double_t)Nbin, 0., Tmax * (Double_t)i / (Double_t)Nbin, Max / 2.);
          half->SetLineColor(kViolet);
          half->Draw();
          break;
        }
      }
    }
  }
  else
  {
    //Draw line at halfway point
    TH1 *h = ((TH1D *)hh[VERTEX_HISTO_T])->GetCumulative();
    Double_t Max = h->GetBinContent(h->GetMaximumBin());
    Double_t Tmax = h->GetXaxis()->GetXmax();
    for (Int_t i = 0; i < h->GetMaximumBin(); i++)
    {
      if (h->GetBinContent(i) > Max / 2.)
      {
        TLine *half = new TLine(TMax*tFactor * (Double_t)i / (Double_t)Nbin, 0., Tmax * (Double_t)i / (Double_t)Nbin, Max / 2.);
        half->SetLineColor(kBlue);
        half->Draw();
        break;
      }
    }
  }
  

  //IO32_NOTBUSY Halfway point

  TH1 *h2 = ((TH1D *)hh[VERTEX_HISTO_IO32_NOTBUSY])->GetCumulative();
  //Draw line at halfway point
  Double_t Max = h2->GetBinContent(h2->GetMaximumBin());
  Double_t Tmax = h2->GetXaxis()->GetXmax();
  for (Int_t i = 0; i < h2->GetMaximumBin(); i++)
  {
    if (h2->GetBinContent(i) > Max / 2.)
    {
      TLine *half = new TLine(TMax *tFactor* (Double_t)i / (Double_t)Nbin, 0., Tmax * (Double_t)i / (Double_t)Nbin, Max / 2.);
      half->SetLineColor(kRed);
      half->Draw();
      break;
    }
  }

  cVTX->cd(8);
  // Z-PHI-counts
  //cVTX->cd(8)->SetFillStyle(4000 );
  ((TH2D *)hh[VERTEX_HISTO_ZPHI])->Draw("colz");
  if (gApplyCuts)
  {
    cVTX->cd(1);
    TPaveText *applycuts_label = new TPaveText(0., 0.95, 0.20, 1.0, "NDC NB");
    if (MVAMode>0)
      applycuts_label->AddText("RF cut applied");
    else
      applycuts_label->AddText("Cuts applied");
    applycuts_label->SetTextColor(kRed);
    applycuts_label->SetFillColor(kWhite);
    applycuts_label->Draw();
  };
  //TLatex* runs_label = new  TLatex(-32.5,-.0625, 32.5, 5.6, "NDC NB");
  //
  cVTX->cd(0);
  TString run_txt = "Run(s): ";
  std::sort(Runs.begin(), Runs.end());
  for (UInt_t i = 0; i < Runs.size(); i++)
  {
    //cout <<"Run: "<<Runs[i] <<endl;
    if (i > 0)
      run_txt += ",";
    run_txt += Runs[i];
  }
  run_txt += " ";
  run_txt += Nbin;
  run_txt += " bins";
  /*if (gZcutMin > -998.)
    {
      run_txt += " gZcutMin=";
      run_txt += gZcutMin;
    }
    if (gZcutMax < 998.)
    {
      run_txt += " gZcutMax=";
      run_txt += gZcutMax;
    }*/
  //runs_label->AddText(run_txt);
  TLatex *runs_label = new TLatex(0., 0., run_txt);
  runs_label->SetTextSize(0.016);
  //runs_label->SetTextColor(kRed);
  //runs_label->SetFillColor(kWhite);
  //runs_label->SetFillStyle(1001);
  runs_label->Draw();

  return cVTX;
}
