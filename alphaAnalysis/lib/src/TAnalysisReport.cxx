#include "TAnalysisReport.h"
#include "LibVersion.h"
//#include "../../alphavmc/include/
#include "TvmcVersion.h"
#include "TCanvas.h"

// TAnalysis Class =========================================================================================
//
// Class collecting togther very basic version and QOD report data. 
//
// JTKM 7/12/14 (my first class)
//
// ==========================================================================================================


using namespace std;

ClassImp(TAnalysisReport);

TAnalysisReport::TAnalysisReport()
{

//  TString  SVN = gSystem->GetFromPipe("svn info | grep \"Revision\" | awk \'{print $2}\'");
//  TString SVN = (_LibSVNRevision_);
//  SetSVNRevision(SVN.Atoi());
  SetSVNlibRevision(_LibSVNRevision_);
//  SetSVNFull(gSystem->GetFromPipe("svn info"));
  SetSVNFull(_LibSVNFull_);
  SetHostName(gSystem->HostName());
  AddCalibrationEvent(0, 0);

    
  //Compilation dates
  
  //SetAlphaAnalysisExeDateBinary(atoi(gSystem->GetFromPipe("stat $RELEASE/alphaAnalysis/alphaAnalysis.exe -c %Y"))); // Now set in alphaAnalysis
  //SetAlphaVMCLibDateBinary(atoi(gSystem->GetFromPipe("stat ${RELEASE}/alphavmc/lib/tgt_${RPLATFORM}/libalphavmc.so -c %Y")));
  TvmcVersion* vmcReport=new TvmcVersion();
  TString vmcNo=vmcReport->GetvmcSVNRevision();
  SetSVNvmcRevision(vmcNo);
  delete vmcReport;
  SetSVNalphaAnalysisRevision(""); //Must be set inside alphaAnalysis.exe
  SetSVNalphaStripsRevision(""); //Yet to be implemented.. needs alphaStrips report object

  
  SetAlphaVMCLibDateBinary((unsigned int)vmcReport->GetgvmcTime());
  //SetAlphaLibDateBinary(atoi(gSystem->GetFromPipe("stat ${RELEASE}/alphaAnalysis/lib/libAlphaAnalysis.so -c %Y")));
  SetAlphaLibDateBinary(gLibTime);
  
	
  SetUserAnalysisNotes(); //Automatically load usernotes form enviroment variable (optional useage, by default empty)
  SetStartTimeBinary(0);
  SetStopTimeBinary(0);
  SetCosmicTriggerBool(kFALSE);
  
  SetRunNumber(-1);
  SetStripFile("");

  
  //VF48
  SetVF48Counts(-1,-1,-1,-1);
 
  //SIS
  SetSISCounters(-1, -1, 0,-1,-1);
	  
  
  //Blinding tools
  
  SetRunNumberBlinded(-1);
  SetDumpBlinded(0);
  SetLabviewBlinded(0);
  
  SetStripsFileForced(kFALSE);
  SetOutputFileForced(kFALSE);
  
  SetPolyFit(kFALSE);
  
  //Global Vars:
  
  SaveGlobalVars(kFALSE, kFALSE, kFALSE, -1, -1, -1,kFALSE, kFALSE, kFALSE, kFALSE, kFALSE, kFALSE, -1, -1, kFALSE, kFALSE);
  WriteAlphaEventTrue();
}

TAnalysisReport::~TAnalysisReport()
{
  VF48Ticksd.clear();
  VF48Ticksd.shrink_to_fit();
  SISTicksd.clear();
  SISTicksd.shrink_to_fit();
  VF48Ticks.clear();
  VF48Ticks.shrink_to_fit();
  SISTicks.clear();
  SISTicks.shrink_to_fit();
  delete SISvsVF48_clock_spline;
}

void TAnalysisReport::PrintGlobalBools(Int_t Verbosity)
{
	if ( StopTimeBinary == StartTimeBinary || StopTimeBinary == 0 )
	{
		cout << "Warning: Analysis incomplete, did not reach end of last .mid file. Unix stop time not set properly... ";
	}
	if (Verbosity>0) {
		cout << "anlphaAnalysis Settings: " << endl;
		cout << "EventCutoff:\t" << GetEventCutoff() << endl;
		cout << "gEnableShowMem:\t"<< GetgEnableShowMem() <<endl;
		cout << "gVf48disasm:\t"<< GetgVf48disasm() <<endl;
		cout << "nVASigma:\t" << GetnVASigma() <<endl;
		cout << "pVASigma:\t" << GetpVASigma() <<endl;
		cout << "gNHitsCut:\t" << GetgNHitsCut() << endl;
		cout << "gRecOff:\t" << GetgRecOff() << endl;
		cout << "gIsCosmicRun:\t" << GetgIsCosmicRun() << endl;
		cout << "gADCspecs:\t" << GetgADCspecs() << endl;
		cout << "gCosmicUtils:\t" << GetgCosmicUtils() << endl;
		cout << "gDoSamplesVF48:\t" << GetgDoSamplesVF48() << endl;
		cout << "gUseTimeRange:\t" << GetgUseTimeRange() << endl;
		cout << "TimeRangeStart:\t" << GetTimeRangeStart() << endl;
		cout << "TimeRangeStop:\t" << GetTimeRangeStop() << endl;
		cout << "gEnableSuppression:\t" << GetgEnableSuppression() << endl;
		cout << "gEOS:\t" << GetgEOS() << endl;
		if ( GetStripsFileForced() )  cout <<"Strips file forced"<<endl;
		if ( GetOutputFileForced() )  cout <<"Output file destination customised" <<endl;
	}
	else
	{
		if(GetgRecOff())  cout <<"Reconstruction off (gRecoff=true)" <<endl;
		if(GetPolyFit())  cout <<"Polynomial fitting of pedestals enabled (gPolyFit=true)" <<endl;
		if(GetgEnableShowMem()) cout << "ShowMem On (gEnableShowMem=true)" <<endl;
		if(!GetgRecOff()){
			if(GetgVf48disasm()) cout << "VF48 disam (gVf48disasm=true)"<<endl;
				if (Verbosity>0) {
				cout << "VA sigma threshold (nVASigma=" << GetnVASigma() << " pVASigma=" << GetpVASigma() << ")" <<endl;
				cout << "NHit threshold (gNHitsCut=" << GetgNHitsCut() << ")" <<endl;
			}
			if(GetgIsCosmicRun()) cout << "Is cosmic run (IsCosmicRun=true) " <<endl;
			if(GetgCosmicUtils()) cout << "Cosmic Utils On (gCosmicUtils=true) " <<endl;
			
		}
		if(GetgADCspecs()) cout << "ADC specs On (gADCSpecs=true)" << endl;
		if(GetgDoSamplesVF48()) cout << "Do Samples VF48 On (gDoSamplesVF48=true)" <<endl;
		if(GetgUseTimeRange()) {
			cout << "Limited time range used (gUseTimeRange=true)"<<endl;
			cout << "Start time: " << GetTimeRangeStart() << endl;
			cout << "Stop time:  " << GetTimeRangeStop() << endl;
		}
		if (GetgEnableSuppression()) cout << "Software suppresion enabled (gEnableSuppresion=true"<< endl;
		if (GetgEOS()) cout << "Midas data was fetched from EOS (gEOS=true)"<<endl;

		
	}
	
}
 
void TAnalysisReport::PrintCompilationDate(Int_t Verbosity)
{
	if (Verbosity>0)
	{
		cout << "libalphavmc.so complation date: " << GetAlphaVMCLibDateString() << endl;
		cout << "libAlphaAnalysis.so complation date: " << GetAlphaLibDateString() << endl;
		cout << "alphaAnalysis.exe compilation date: " << GetAlphaAnalysisExeDateString()<< endl;	
	}
}

void TAnalysisReport::PrintBlindingTool(Int_t Verbosity)
{
  if (GetRunNumberBlinded()>0) 
    cout <<"\tRun Number is blinded!"<<endl;
  if (GetDumpBlinded()>0) 
    cout << "\t"<<GetDumpBlinded()<<" Dump string(s) blinded!"<<endl;
  if (GetLabviewBlinded()>0) 
    cout << "\t"<<GetLabviewBlinded()<<" Labview bank names(s) blinded!"<<endl;

}
 
void TAnalysisReport::Print(Int_t Verbosity)
{
  Bool_t BadClocks=false;
  for (Int_t i=0; i<100; i++)
  {
    if (FirstSISTimestamps[i]-FirstVF48Timestamps[i] > 0.001)
      BadClocks=true;
  }
  if (BadClocks)
  {
    std::cerr<<"Warning: SIS and VF48 clock offset not correct at start of run"<<std::endl;
    for (Int_t i=0; i<100; i++)
    {
      std::cout <<"i:"<<i<<"\tADC: "<< FirstSISTimestamps[i] << "\tSil: "<< FirstVF48Timestamps[i] <<std::endl;
    }
  }
  std::cout << "\n\n\n=================================================================================" << std::endl;
  if (GetRunNumberBlinded()>0 || GetDumpBlinded()>0 || GetLabviewBlinded()>0)
  {
    std::cout <<"||                    Analysis summary for BLINDED Run " << GetRunNumber() << "                    ||"<< std::endl;
  }
  else
  {
    std::cout <<"||                       Analysis summary for Run " << GetRunNumber() << "                        ||"<< std::endl;
  }
  std::cout << "=================================================================================" << std::endl;
  PrintCompilationDate(Verbosity);
  PrintGlobalBools(Verbosity);
  PrintBlindingTool(Verbosity);
  if(!GetgRecOff())
    {
      std::cout <<"AlphaStrip file used";
      if (GetStripsFileForced()) std::cout <<" (forced)";
      std::cout <<": " << GetStripFile() << std::endl;
      std::cout << "Last time alphaStrips was run: " << GetStripFileDateDay() << "/" << GetStripFileDateMonth() << "/" << GetStripFileDateYear() << std::endl;

      printf("VF48EndRun: Good VF48 events: %d, bad: %d, Total: %d, data errors: %d, Passed: %d, Reconstructed: %d, RecoEff: %lf\n",GetGoodVF48Events(),GetBadVF48Events(),GetTotalVF48Events(),GetDataErrors(),GetPassedCutEvents(), GetTotalReconstructedEvents(),GetRecoEfficiency());    
    }
  printf("Good SIS events: %d, Bad: %d gSISdiff %d \n", GetGoodSISEvents(), GetBadSISEvents(), GetSISdiff());
  printf("SIS Clock check: SIS1  %lu  SIS2 %lu\n", GetSISClockCheck_SIS1(),GetSISClockCheck_SIS2());

  std::cout << "Analysis run on host: " << GetHostname() << " with SVN Version: ";
  TString strip=GetSVNalphaStripsRevision();
  TString lib=GetSVNlibRevision();
  //lib.ReplaceAll(":",".");
  TString alphaAnalysis=GetSVNalphaAnalysisRevision();
  //alphaAnalysis.ReplaceAll(":",".");
  TString vmc=GetSVNvmcRevision();  
  //vmc.ReplaceAll(":",".");
  //To be implemented:
  //Int_t alphaStrips=GetSVNalphaStripsRevision();
  if ( lib == alphaAnalysis && lib == vmc && lib == strip)
    {
      std::cout<< lib << " on: " << GetAnalysisDateDay() << "/" << GetAnalysisFileDateMonth() << "/" << GetAnalysisFileDateYear() << std::endl;
    }
  else
    {
      std::cout << std::endl << "\t\t alphavmc:\t\t"<<vmc;
      std::cout << std::endl << "\t\t alphaAnalysis/lib:\t"<<lib;
      std::cout << std::endl << "\t\t alphaAnalysis:\t\t"<<alphaAnalysis;
      std::cout << std::endl << "\t\t alphaStrips:\t\t"<<strip;
      std::cout <<" on: " << GetAnalysisDateDay() << "/" << GetAnalysisFileDateMonth() << "/" << GetAnalysisFileDateYear() << std::endl;
    }
  cout<<"Run start: "<<GetStartTimeString()<<endl;
  cout<<"Run stop:  "<<GetStopTimeString()<<endl;
  std::cout << "alphaAnalysis exec time:\tCPU: "<< GetCPUtime() <<"s\tUser: " << GetUserTime() << "s";
  if  (GetStopTimeBinary()>0)
    std::cout<<" (Runlength: ~"<<GetStopTimeBinary() - GetStartTimeBinary()<<"s)" << std::endl;
  else
    std::cout<<endl<<"End of midas file time marker not found"<<std::endl;
  std::cout << "=================================================================================\n\n\n" << std::endl;
  
}

void TAnalysisReport::GenerateCalibrationSpline(Int_t verbose) {
	if (SISvsVF48_clock_spline != NULL) delete SISvsVF48_clock_spline;
	int nevents = VF48Ticks.size();
	SISvsVF48_clock_spline = new TSpline3("VF48 to SIS Calibration", VF48Ticksd.data(), SISTicksd.data(), nevents);
	if (verbose>0)
	{
		printf("Ncalibrations: %d\n", nevents);
		for (int i =0; i < nevents; i++){
			printf("VF48 clock ticks: %lu \tSIS clock ticks: %lu\n", VF48Ticks[i], SISTicks[i]);
		}
	}
}


//multiply by 2 account for different clock frequencies 
//20,000,000 Hz clock
double TAnalysisReport::GetVF48TimeCalibration_sec(double iVF48Time){
	if (iVF48Time == 0) return 1;
	return GetVF48TimeCalibration((double)(iVF48Time * 20000000.));
}


double TAnalysisReport::GetVF48TimeCalibration(double iVF48Ticks){
	if (VF48Ticks.size()>1)
		return SISvsVF48_clock_spline->Eval(iVF48Ticks)/iVF48Ticks*2.;
	else
		return 1;
}
