/*
 *  TLabVIEWEvent.cpp
 *  
 *
 *  Created by Sarah Seif El Nasr on 09/08/07.
 *  Copyright 2007 __MyCompanyName__. All rights reserved.
 *
 */

#include "TLabVIEWEvent.h"

ClassImp(TLabVIEWEvent)

//Default Constructor
TLabVIEWEvent::TLabVIEWEvent()
{
  ClearLabVIEWEvent();
}
//Default Destructor
TLabVIEWEvent::~TLabVIEWEvent()
{
}

//Functions required for manipulating data in the tree
void TLabVIEWEvent::SetLabVIEWEvent(TString name, Int_t number, Double_t value, Double_t time)
{	
	SetBankName(name);
	SetArrayNumber(number);
	SetValue(value);	
	SetTime(time);
}

void TLabVIEWEvent::ClearLabVIEWEvent()
{
	SetBankName("");
	SetArrayNumber(0);
	SetValue(0);
}

