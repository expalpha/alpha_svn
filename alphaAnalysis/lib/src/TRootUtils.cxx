#include "TRootUtils.h"
// ******************************************************* 
// Automatically Generated Function Index for TRootUtils.cxx
// ******************************************************* 
// TRootUtils	TRootUtils.cxx	/^namespace TRootUtils$/;"	n	file:
// ApplyCuts	TRootUtils.cxx	/^Int_t ApplyCuts(TSiliconEvent *sil_event) \/\/Streamlining to one ApplyCuts function...$/;"	f	namespace:TRootUtils
// ApplyMVA	TRootUtils.cxx	/^Int_t ApplyMVA(TSiliconEvent *sil_event)$/;"	f	namespace:TRootUtils
// AnnounceOnSpeaker	TRootUtils.cxx	/^void AnnounceOnSpeaker(Int_t runNumber, TString Phrase)$/;"	f	namespace:TRootUtils
// AnnounceOnSpeaker	TRootUtils.cxx	/^void AnnounceOnSpeaker(TString Phrase)$/;"	f	namespace:TRootUtils
// MakeAutoPlotsFolder	TRootUtils.cxx	/^TString MakeAutoPlotsFolder(TString subFolder)$/;"	f	namespace:TRootUtils
// Get_Strip_File	TRootUtils.cxx	/^TFile *Get_Strip_File(Int_t run_number, Bool_t die)$/;"	f	namespace:TRootUtils
// Get_Cosmic_File	TRootUtils.cxx	/^TFile *Get_Cosmic_File(Int_t run_number, Bool_t die)$/;"	f	namespace:TRootUtils
// Close_File	TRootUtils.cxx	/^void Close_File(Int_t run_number)$/;"	f	namespace:TRootUtils
// Get_File	TRootUtils.cxx	/^TFile *Get_File(Int_t run_number, Bool_t die)$/;"	f	namespace:TRootUtils
// Get_MVA_File	TRootUtils.cxx	/^TFile *Get_MVA_File(Int_t run_number, Bool_t die)$/;"	f	namespace:TRootUtils
// CloseOpenFiles	TRootUtils.cxx	/^int CloseOpenFiles()$/;"	f	namespace:TRootUtils
// RunMVACosmicDumper	TRootUtils.cxx	/^void RunMVACosmicDumper(Int_t run_number, Double_t tmin, Double_t tmax)$/;"	f	namespace:TRootUtils
// RunMVACosmicHitDumper	TRootUtils.cxx	/^void RunMVACosmicHitDumper(Int_t run_number, Double_t tmin, Double_t tmax)$/;"	f	namespace:TRootUtils
// RunMVADumper	TRootUtils.cxx	/^void RunMVADumper(Int_t run_number, const char *PHASE)$/;"	f	namespace:TRootUtils
// Get_Dumper_File	TRootUtils.cxx	/^TFile *Get_Dumper_File(Int_t run_number) \/\/, const char* PHASE)$/;"	f	namespace:TRootUtils
// Get_Dumper_Tree	TRootUtils.cxx	/^TTree *Get_Dumper_Tree(Int_t run_number) \/\/, const char* phase)$/;"	f	namespace:TRootUtils
// Get_MVA_Tree	TRootUtils.cxx	/^TTree *Get_MVA_Tree(Int_t run_number, Bool_t die)$/;"	f	namespace:TRootUtils
// GetSequencerTree	TRootUtils.cxx	/^TTree *GetSequencerTree(Int_t runNumber)$/;"	f	namespace:TRootUtils
// GetSequencerXMLTree	TRootUtils.cxx	/^TTree *GetSequencerXMLTree(Int_t runNumber)$/;"	f	namespace:TRootUtils
// Get_Sis_Tree	TRootUtils.cxx	/^TTree *Get_Sis_Tree(Int_t run_number)$/;"	f	namespace:TRootUtils
// Get_Sis_Tree	TRootUtils.cxx	/^TTree *Get_Sis_Tree(Int_t run_number, Int_t channel)$/;"	f	namespace:TRootUtils
// Get_Vtx_Tree	TRootUtils.cxx	/^TTree *Get_Vtx_Tree(Int_t run_number)$/;"	f	namespace:TRootUtils
// Get_TTC_Tree	TRootUtils.cxx	/^TTree *Get_TTC_Tree(Int_t run_number)$/;"	f	namespace:TRootUtils
// TTC_Multiplicity_by_Layer	TRootUtils.cxx	/^Int_t *TTC_Multiplicity_by_Layer(Int_t runNumber, Int_t EventNumber[])$/;"	f	namespace:TRootUtils
// Get_RunTime_of_Pulse	TRootUtils.cxx	/^TArray *Get_RunTime_of_Pulse(Int_t runNumber, vector<Int_t> VetoChan, Int_t tmin, Int_t tmax)$/;"	f	namespace:TRootUtils
// Get_RunTime_of_Count	TRootUtils.cxx	/^Double_t Get_RunTime_of_Count(Int_t run_number, Int_t channel, Int_t count)$/;"	f	namespace:TRootUtils
// Get_RunTime_of_Count	TRootUtils.cxx	/^Double_t Get_RunTime_of_Count(TTree *tree, Int_t count)$/;"	f	namespace:TRootUtils
// Get_RunTime_of_Count	TRootUtils.cxx	/^Double_t Get_RunTime_of_Count(Int_t run_number, Int_t channel, Int_t count, Double_t tmin, Double_t tmax)$/;"	f	namespace:TRootUtils
// Get_RunTime_of_Count	TRootUtils.cxx	/^Double_t Get_RunTime_of_Count(TTree *tree, Int_t count, Double_t tmin, Double_t tmax)$/;"	f	namespace:TRootUtils
// Get_RunTime_of_SISChan	TRootUtils.cxx	/^Double_t Get_RunTime_of_SISChan(Int_t run_number, Int_t channel, Int_t rep)$/;"	f	namespace:TRootUtils
// GetTotalRunTimeByMidas	TRootUtils.cxx	/^Double_t GetTotalRunTimeByMidas(Int_t runNumber)$/;"	f	namespace:TRootUtils
// GetTotalRunTimeByVF48Clock	TRootUtils.cxx	/^Double_t GetTotalRunTimeByVF48Clock(Int_t runNumber)$/;"	f	namespace:TRootUtils
// GetTotalRunTimeBy50MHzClock	TRootUtils.cxx	/^Double_t GetTotalRunTimeBy50MHzClock(Int_t runNumber)$/;"	f	namespace:TRootUtils
// GetTotalRunTime	TRootUtils.cxx	/^Double_t GetTotalRunTime(Int_t runNumber)$/;"	f	namespace:TRootUtils
// Count_SIS_Triggers	TRootUtils.cxx	/^Int_t Count_SIS_Triggers(Int_t run_number, Int_t SIS_Channel, const char *description, Int_t repetition, Int_t offset, bool exact_match)$/;"	f	namespace:TRootUtils
// Count_SIS_Triggers	TRootUtils.cxx	/^Int_t Count_SIS_Triggers(Int_t run_number, Int_t SIS_Channel, Double_t StartTime, Double_t StopTime)$/;"	f	namespace:TRootUtils
// Count_SIS_Triggers	TRootUtils.cxx	/^Int_t Count_SIS_Triggers(Int_t run_number, Int_t SIS_Channel)$/;"	f	namespace:TRootUtils
// IsDumpInCAT	TRootUtils.cxx	/^Bool_t IsDumpInCAT(Int_t runNumber, const char *eventName, const char *description, Int_t repetition, Int_t offset)$/;"	f	namespace:TRootUtils
// IsDumpInRCT	TRootUtils.cxx	/^Bool_t IsDumpInRCT(Int_t runNumber, const char *eventName, const char *description, Int_t repetition, Int_t offset)$/;"	f	namespace:TRootUtils
// IsDumpInATM	TRootUtils.cxx	/^Bool_t IsDumpInATM(Int_t runNumber, const char *eventName, const char *description, Int_t repetition, Int_t offset)$/;"	f	namespace:TRootUtils
// CountEventsInRun	TRootUtils.cxx	/^Int_t CountEventsInRun(Int_t runNumber, const char *eventName, const char *description, bool exact_match)$/;"	f	namespace:TRootUtils
// TestForEventInRun	TRootUtils.cxx	/^Bool_t TestForEventInRun(Int_t runNumber, const char *eventName, const char *description, Int_t repetition, Int_t offset, bool exact_match)$/;"	f	namespace:TRootUtils
// MatchEventToTime	TRootUtils.cxx	/^Double_t MatchEventToTime(Int_t runNumber, const char *eventName, const char *description, Int_t repetition, Int_t offset, bool exact_match)$/;"	f	namespace:TRootUtils
// GetFirstVF48EventInDump	TRootUtils.cxx	/^Int_t GetFirstVF48EventInDump(Int_t runNumber, const char *description, Int_t repetition = 1, Int_t offset = 0)$/;"	f	namespace:TRootUtils
// GetLastVF48EventInDump	TRootUtils.cxx	/^Int_t GetLastVF48EventInDump(Int_t runNumber, const char *description, Int_t repetition = 1, Int_t offset = 0)$/;"	f	namespace:TRootUtils
// FindSequencerEvent	TRootUtils.cxx	/^TSeq_Event *FindSequencerEvent(Int_t runNumber, const char *eventName, const char *description, Int_t repetition, Int_t &offset, bool exact_match)$/;"	f	namespace:TRootUtils
// Get_RunTime_of_SequencerEvent	TRootUtils.cxx	/^Double_t Get_RunTime_of_SequencerEvent(Int_t run_number, TSeq_Event *seqEvent, Int_t seqOffset, Bool_t RedundantCheck)$/;"	f	namespace:TRootUtils
// FindSequencerEvent	TRootUtils.cxx	/^TSeq_Event *FindSequencerEvent(TTree *sequencerTree, const char *eventName, const char *description)$/;"	f	namespace:TRootUtils
// FindSequencerEvent	TRootUtils.cxx	/^TSeq_Event *FindSequencerEvent(TTree *sequencerTree, const char *eventName, const char *description, Int_t repetition, Int_t &offset)$/;"	f	namespace:TRootUtils
// GetAlphaEvents	TRootUtils.cxx	/^TTree *GetAlphaEvents(TFile *theFile)$/;"	f	namespace:TRootUtils
// GetSiliconEvents	TRootUtils.cxx	/^TTree *GetSiliconEvents(TFile *theFile)$/;"	f	namespace:TRootUtils
// GetSequencerTree	TRootUtils.cxx	/^TTree *GetSequencerTree(TFile *theFile)$/;"	f	namespace:TRootUtils
// GetSISTree	TRootUtils.cxx	/^TTree *GetSISTree(TFile *theFile)$/;"	f	namespace:TRootUtils
// GetSISTree	TRootUtils.cxx	/^TTree *GetSISTree(TFile *theFile, Int_t channel)$/;"	f	namespace:TRootUtils
// GetRunTimeOfCount	TRootUtils.cxx	/^Double_t GetRunTimeOfCount(TTree *tree, Int_t count)$/;"	f	namespace:TRootUtils
// GetRunTimeOfCount	TRootUtils.cxx	/^Double_t GetRunTimeOfCount(TTree *tree, Int_t count, Double_t seq_start_time)$/;"	f	namespace:TRootUtils
// GetRunTimeOfEntry	TRootUtils.cxx	/^Double_t GetRunTimeOfEntry(TTree *tree, Int_t entry)$/;"	f	namespace:TRootUtils
// GetDumpTime	TRootUtils.cxx	/^void GetDumpTime(TFile *theFile, std::vector<TString> EventDescription, std::vector<Double_t> &StartTime, std::vector<Double_t> &StopTime, std::vector<TString> &dumpname)$/;"	f	namespace:TRootUtils
// GetDumpTime	TRootUtils.cxx	/^void GetDumpTime(TFile *theFile, const char *EventDescription, std::vector<Double_t> &StartTime, std::vector<Double_t> &StopTime, std::vector<TString> &dumpname, Int_t nreps)$/;"	f	namespace:TRootUtils
// GetDumpTime	TRootUtils.cxx	/^void GetDumpTime(TFile *theFile, const char *EventDescription, Double_t &StartTime, Double_t &StopTime, Double_t repetition)$/;"	f	namespace:TRootUtils
// GetRWinRun	TRootUtils.cxx	/^TString GetRWinRun(int runNumber, TString RW)$/;"	f	namespace:TRootUtils
// GetSequenceQueueFull	TRootUtils.cxx	/^TString GetSequenceQueueFull(int runNumber, TString SequencerName)$/;"	f	namespace:TRootUtils
// GetSequenceQueue	TRootUtils.cxx	/^TString GetSequenceQueue(int runNumber, TString SequencerName)$/;"	f	namespace:TRootUtils
// PrintSequences	TRootUtils.cxx	/^void PrintSequences(int runNumber, TString SequencerName)$/;"	f	namespace:TRootUtils
// GetQWPDir	TRootUtils.cxx	/^TString GetQWPDir(int runNumber)$/;"	f	namespace:TRootUtils
// GetTrappingHash	TRootUtils.cxx	/^long int GetTrappingHash(Int_t runNumber)$/;"	f	namespace:TRootUtils
// CalculateSequenceFlagHex	TRootUtils.cxx	/^TString CalculateSequenceFlagHex(TString SequenceString)$/;"	f	namespace:TRootUtils
// GetSequencerOnlyString	TRootUtils.cxx	/^TString GetSequencerOnlyString(Int_t runNumber) \/\/cat all sequencers and RW's$/;"	f	namespace:TRootUtils
// GetSequencerOnlyStringHash	TRootUtils.cxx	/^long int GetSequencerOnlyStringHash(Int_t runNumber)$/;"	f	namespace:TRootUtils
// GetRWOnlyString	TRootUtils.cxx	/^TString GetRWOnlyString(Int_t runNumber) \/\/cat all sequencers and RW's$/;"	f	namespace:TRootUtils
// GetRWOnlyStringHash	TRootUtils.cxx	/^long int GetRWOnlyStringHash(Int_t runNumber)$/;"	f	namespace:TRootUtils
// GetSequenceString	TRootUtils.cxx	/^TString GetSequenceString(Int_t runNumber) \/\/cat all sequencers and RW's$/;"	f	namespace:TRootUtils
// GetSequenceStringHash	TRootUtils.cxx	/^long int GetSequenceStringHash(Int_t runNumber)$/;"	f	namespace:TRootUtils
// GetClockCalib	TRootUtils.cxx	/^Double_t GetClockCalib(TFile *theFile)$/;"	f	namespace:TRootUtils
// GetClockCalib	TRootUtils.cxx	/^Double_t GetClockCalib(TTree *tree1, TTree *tree2)$/;"	f	namespace:TRootUtils
// FindSISQuenchFlagTime	TRootUtils.cxx	/^Double_t FindSISQuenchFlagTime(Int_t runNumber, Int_t repetition, Bool_t FailOnError)$/;"	f	namespace:TRootUtils
// FindSISMixingFlagTime	TRootUtils.cxx	/^Double_t FindSISMixingFlagTime(Int_t runNumber, Int_t repetition)$/;"	f	namespace:TRootUtils
// FindMixingGate	TRootUtils.cxx	/^Double_t FindMixingGate(TFile *theFile, Double_t &StartTime, Double_t &StopTime)$/;"	f	namespace:TRootUtils
// FindQuenchGate	TRootUtils.cxx	/^Double_t FindQuenchGate(TFile *theFile, Double_t &StartTime, Double_t &StopTime)$/;"	f	namespace:TRootUtils
// PrintProcessingTime	TRootUtils.cxx	/^void PrintProcessingTime(Int_t runNumber)$/;"	f	namespace:TRootUtils
// GetProcessingTime	TRootUtils.cxx	/^Double_t GetProcessingTime(Int_t runNumber)$/;"	f	namespace:TRootUtils
// GetlibSVNVersion	TRootUtils.cxx	/^TString GetlibSVNVersion(Int_t runNumber)$/;"	f	namespace:TRootUtils
// GetalphaAnalysisSVNVersion	TRootUtils.cxx	/^TString GetalphaAnalysisSVNVersion(Int_t runNumber)$/;"	f	namespace:TRootUtils
// GetvmcSVNVersion	TRootUtils.cxx	/^TString GetvmcSVNVersion(Int_t runNumber)$/;"	f	namespace:TRootUtils
// GetSVNVersion	TRootUtils.cxx	/^TString GetSVNVersion(Int_t runNumber)$/;"	f	namespace:TRootUtils
// GetAnalysisReport	TRootUtils.cxx	/^TAnalysisReport *GetAnalysisReport(Int_t runNumber)$/;"	f	namespace:TRootUtils
// GetAnalysisReportTree	TRootUtils.cxx	/^TTree *GetAnalysisReportTree(Int_t runNumber)$/;"	f	namespace:TRootUtils
// GetStripReportTree	TRootUtils.cxx	/^TTree *GetStripReportTree(Int_t runNumber)$/;"	f	namespace:TRootUtils
// GetStripReport	TRootUtils.cxx	/^TalphaStripsReport *GetStripReport(Int_t runNumber)$/;"	f	namespace:TRootUtils
// PrintStripRMS	TRootUtils.cxx	/^void PrintStripRMS(Int_t runNumber)$/;"	f	namespace:TRootUtils
// Get_LabVIEW_Tree	TRootUtils.cxx	/^TTree *Get_LabVIEW_Tree(Int_t run_number)$/;"	f	namespace:TRootUtils
// LabVIEW_at_SIS	TRootUtils.cxx	/^Double_t LabVIEW_at_SIS(Int_t runNumber, Int_t SIS_channel, Int_t repetition, TString Bank, Int_t Array_number, Int_t Interp_Option)$/;"	f	namespace:TRootUtils
// LabVIEW_at_dump	TRootUtils.cxx	/^Double_t LabVIEW_at_dump(Int_t runNumber, const char *description, Int_t repetition, Int_t offset, TString Bank, Int_t Array_number, Int_t Interp_Option)$/;"	f	namespace:TRootUtils
// GetLabVIEWEvent	TRootUtils.cxx	/^Double_t GetLabVIEWEvent(Int_t runNumber, Double_t runTime, TString Bank, Int_t Array_number, Int_t Interp_Option)$/;"	f	namespace:TRootUtils
// GetLabVIEWEvent	TRootUtils.cxx	/^Double_t GetLabVIEWEvent(TTree *labVIEW_tree, Double_t runTime, TString Bank, Int_t Array_number, Int_t Interp_Option)$/;"	f	namespace:TRootUtils
// Extract_Bank_Array	TRootUtils.cxx	/^Int_t Extract_Bank_Array(Int_t run_number, TString bank, Int_t array_num, Double_t *time, Double_t *LVData, Double_t tmin, Double_t tmax)$/;"	f	namespace:TRootUtils
// FileToRuns	TRootUtils.cxx	/^Int_t FileToRuns(const char *filename, Int_t *runNumber, Int_t *dumps, TObjArray *legendText)$/;"	f	namespace:TRootUtils
// FileToSubFiles	TRootUtils.cxx	/^Int_t FileToSubFiles(const char *filename, TObjArray *subFilenames, TObjArray *legendText)$/;"	f	namespace:TRootUtils
// CheckForDuplicates	TRootUtils.cxx	/^Int_t CheckForDuplicates(Int_t nRuns, Int_t *runs)$/;"	f	namespace:TRootUtils
// TA_Mask_Run2010	TRootUtils.cxx	/^Bool_t TA_Mask_Run2010(Int_t TAnumber)$/;"	f	namespace:TRootUtils
// Get_ASIC_Number	TRootUtils.cxx	/^Double_t Get_ASIC_Number(Int_t TAnumber)$/;"	f	namespace:TRootUtils
using namespace std;
Bool_t gAllowSeqEventHelper= kTRUE; //Speed up code by using SeqEventHelper cache
Bool_t gApplyCuts = kTRUE;

Bool_t gAnnounce = kTRUE; //Make announcements on speaker (of alpha control room)
// MVA RFcut (prototype value)
Double_t grfcut = 0.128865;                                                // Edit value here or use SetRFCut(Double_t RFcut) below

TString gTreePrefix = getenv("TREEPREFIX");   //"/tree";
TString gTreePostfix = getenv("TREEPOSTFIX"); //"offline.root";
std::vector<TString> gTMVAVarsString;
std::vector<TString> gTMVAVarsType;
std::vector<TString> gTMVASpectatorsString;
std::vector<TString> gTMVASpectatorsType;

Float_t gTMVAVarFloat[10000];
Int_t gTMVAVarInt[10000];
Float_t gTMVASpectatorsFloat[10000];
Int_t gTMVASpectatorsInt[10000];
TString gdir ="";
TString prefix = "alphaClassification";
//std::vector<Float_t> gTMVAVarFloat;

//TMVA::Tools::Instance();
TString gmethodName = "";
TMVA::Reader *reader = NULL;

Int_t gLegendDetail = 1;
Double_t REScuteq2 = 2.0;
Double_t DCAcuteq2;
Double_t RADcuteq2 = 4.0;
Double_t REScutgr2 = 0.05;
Double_t DCAcutgr2;
Double_t RADcutgr2 = 4.0;

Double_t gZcutMax = 999.;
Double_t gZcutMin = -999.;


TSISChannels *gSISch;

TTimestampHelper *TimeStampHelperBuffer=NULL;
Bool_t TimeStampHelperBufferComplete=kFALSE;
TSeq_EventHelper *SeqEventHelperBuffer=NULL;
//using namespace TRootUtils;
namespace TRootUtils
{

void SetZCut(double min, double max)
{
  gZcutMin=min;
  gZcutMax=max;
  std::cout<<"Selecting vertices between "<< gZcutMin << " and "<< gZcutMax<<std::endl;
  std::cout<<"Note: Events with no vertex  have z=-99"<<std::endl;
}


Int_t ApplyCuts(TSiliconEvent *sil_event) //Streamlining to one ApplyCuts function...
//Return 0 if no passed cuts
//Return 1 if passed cuts
//Return  if MVA Pass (MVA needs to be enabled
{

#if TTCCuts //By default off
  if (!gApplyCuts)
    return 1;

  //if( ApplyTTC_Cut_Min(ttc_event)==kFALSE || ApplyTTC_Cut_Max(ttc_event)==kFALSE )
  Int_t TTCEvents[4] = {0};
  for (Int_t j = 0; j < 4; j++)
  {
    TTCEvents[j] = sil_event->GetTTCCounter(j);
  }
  for (Int_t j = 0; j < 4; j++)
  {
    TTCEvents[j] = sil_event->GetTTCCounter(j);
    //cout << "TTC" << TTCEvents[j] << endl;
  }
  if (!ApplyTTC_Cut_Min(runNumber, TTCEvents))
  {
    cout << "TTC min rejection of " << i << endl;
    return kFALSE;
  }
  if (!ApplyTTC_Cut_Max(runNumber, TTCEvents))
  {
    cout << "TTC max rejection of " << i << endl;
    return kFALSE;
  }
#endif

#if DCA
  Int_t Ntracks = sil_event->GetNTracks();
  Double_t res = sil_event->GetResidual();
  Double_t dca = sil_event->GetDCA();
  if (Z > gZcutMax || Z < gZcutMin)
    return kFALSE;
  if (Ntracks == 2)
  {
    if (res < REScuteq2 || dca > DCAcuteq2)
      return kFALSE;
  }
  else if (Ntracks > 2)
  {
    if (res < REScutgr2 || dca > DCAcutgr2)
      return kFALSE;
  }
  else
    return kFALSE;

  return kTRUE;
#endif

  //Regular passed cuts
  TVector3 *vtx = sil_event->GetVertex();
  Int_t Ntracks = sil_event->GetNTracks();
  Double_t res = sil_event->GetResidual();
  Double_t rad = vtx->Perp();
  Double_t Z = vtx->z();

  if (Z > gZcutMax || Z < gZcutMin)
    return kFALSE;
  if (!gApplyCuts)
  {
    return sil_event->GetNVertices();
  }

  if (Ntracks == 2)
  {
    if (res < REScuteq2 || rad > RADcuteq2)
      return kFALSE;
  }
  else if (Ntracks > 2)
  {
    if (res < REScutgr2 || rad > RADcutgr2)
      return kFALSE;
  }
  else
    return kFALSE;

  return kTRUE;

}


Int_t ApplyMVA(TSiliconEvent *sil_event, Double_t *rfout)
{
	
  if (!reader) reader = new TMVA::Reader( "!Color:!Silent" ); 
  Int_t RunNumber = sil_event->GetRunNumber();
  Int_t EventNo = sil_event->GetVF48NEvent();
  if (RunNumber <0 || EventNo < 0 )
  {
		std::cout<<"ERROR! BAD sil_event"<<std::endl;
		std::cerr<<"ERROR! BAD sil_event"<<std::endl;
		sil_event->Print();
		return (Int_t)ApplyCuts(sil_event);
	}
  TTree *t0 = Get_Dumper_Tree(RunNumber);
  //Insert for loop to replace this list and section:

  //Extras
  Float_t PassedCut;
  Int_t MVAEventID;
  //Double_t z;
  Float_t z;
  Int_t i(0);

  t0->SetBranchAddress("passcut_2012", &PassedCut);
  t0->SetBranchAddress("entry", &MVAEventID);
  t0->SetBranchAddress("z", &z);

  for (UInt_t i = 0; i < gTMVAVarsString.size(); i++)
  {
    if (gTMVAVarsType[i] == 'I')
    {
      t0->SetBranchAddress(gTMVAVarsString[i].Data(), &gTMVAVarInt[i]);
    }
    else if (gTMVAVarsType[i] == 'F')
    {
      t0->SetBranchAddress(gTMVAVarsString[i].Data(), &gTMVAVarFloat[i]);
    }
    else
    {
      std::cout << gTMVAVarsString[i] << "Has an unsupported data type of "
                << gTMVAVarsType[i] << std::endl;
    }
  }

  t0->GetEntry(0);
  i= EventNo - MVAEventID;
  t0->GetEntry(i);  
  //  cout<<"Init "<<EventNo<<":"<<MVAEventID<<endl;
  while (EventNo > MVAEventID)
  {
    t0->GetEntry( ++i);
    if(EventNo<MVAEventID) return 0;
  }
  while (EventNo < MVAEventID) {
    if(--i<0) return 0;
    t0->GetEntry(i);
  }
  if(EventNo!=MVAEventID){
    cout<<"alphaAnalysis tree and dumper tree missing event "<<EventNo<<":"<<MVAEventID<<endl;
    return 0;
  }
  //Double_t
   *rfout = reader->EvaluateMVA(gmethodName);

  Int_t Result = 0;
  if (grfcut < *rfout)
    Result += 1 << 1;
  if (ApplyCuts(sil_event) > 0)
    Result += 1 << 0;

  //  TVector3 *vtx = sil_event->GetVertex();

  //Check Z:
  //  if (i)
  //  {
  //    cout << "Warning: alphaAnalysis tree and dumper tree out of line... :" << endl;
  //    cout << EventNo << ":" <<EventNo+i<<":"<< MVAEventID << endl;
  //    cout << "Z" << vtx->Z() << ":" << z << endl;
  //  }
  if (z > gZcutMax || z < gZcutMin)
    return 0;
  if (!gApplyCuts)
    return 3;

  return Result;
}



void AnnounceOnSpeaker(Int_t runNumber, TString Phrase)
{
  if (gAnnounce == kFALSE)
    return;
  cout << gSystem->HostName() << endl;
  if (strcmp(gSystem->HostName(), "alphadaq.cern.ch") == 0 || strcmp(gSystem->HostName(), "alphacpc04.cern.ch") == 0 || strcmp(gSystem->HostName(), "alphacpc09.cern.ch") == 0 || strcmp(gSystem->HostName(), "alphacpc39.cern.ch") == 0 || strcmp(gSystem->HostName(), "alphaautomagic") == 0 || strcmp(gSystem->HostName(), "alphaautomagic2") == 0 || strcmp(gSystem->HostName(), "alphaautomagic.cern.ch") == 0 || strcmp(gSystem->HostName(), "alphaautomagic2.cern.ch") == 0 || strcmp(gSystem->HostName(), "alphadaq") == 0 || strcmp(gSystem->HostName(), "alphacpc04") == 0 || strcmp(gSystem->HostName(), "alphacpc09") == 0 || strcmp(gSystem->HostName(), "alphacpc39") == 0)
  {
    //Fewer ssh call option:
    //TAnalysisReport* report=GetAnalysisReport(runNumber);
    //Int_t AgeInSeconds=(Int_t)std::time(nullptr)-report->GetStopTimeBinary();
    //if (AgeInSeconds<43200) //Less than 12 hours old
    // {AnnounceOnSpeaker(Phrase);}
    //else {cout <<"RunNumber"<<runNumber<<" too old to announce in control room"}

    TString CurrentRunNo = gSystem->GetFromPipe("ssh -x alphadaq \'odbedit -d \"/Runinfo/Run number\" -c ls \' | grep -Eo [0-9]{5}");
    Int_t CurrentNo = CurrentRunNo.Atoi();
    if (CurrentNo < runNumber + 50)
    {
      AnnounceOnSpeaker(Phrase);
    }
    else
    {
      cout << "RunNumber too old to announce in control room (" << runNumber << " <= " << CurrentNo << " - 50)" << endl;
    }
  }
  return;
}

void AnnounceOnSpeaker(TString Phrase)
{
  if (gAnnounce == kFALSE)
    return;
  cout << gSystem->HostName() << endl;
  if (strcmp(gSystem->HostName(), "alphadaq.cern.ch") == 0 || strcmp(gSystem->HostName(), "alphacpc04.cern.ch") == 0 || strcmp(gSystem->HostName(), "alphacpc09.cern.ch") == 0 || strcmp(gSystem->HostName(), "alphacpc39.cern.ch") == 0 || strcmp(gSystem->HostName(), "alphaautomagic") == 0 || strcmp(gSystem->HostName(), "alphaautomagic2") == 0 || strcmp(gSystem->HostName(), "alphaautomagic.cern.ch") == 0 || strcmp(gSystem->HostName(), "alphaautomagic2.cern.ch") == 0 || strcmp(gSystem->HostName(), "alphadaq") == 0 || strcmp(gSystem->HostName(), "alphacpc04") == 0 || strcmp(gSystem->HostName(), "alphacpc09") == 0 || strcmp(gSystem->HostName(), "alphacpc39") == 0)
  {
    TString result = "ssh -x alphadaq \"echo \'";
    result += Phrase;
    result += "\' | ~/online/bin/google_speech/speech_google.py \"";
    gSystem->Exec(result);
    cout << Phrase << endl;
  }
}

TString MakeAutoPlotsFolder(TString subFolder)
{
  gSystem->mkdir("AutoPlots");
  // Make dated folder
  TDatime *TS1 = new TDatime;
  const unsigned int date = TS1->GetDate();
  TString savFolder("AutoPlots/");
  savFolder += date;
  if (((gSystem->OpenDirectory(savFolder)) == 0)) //gSystem causesing problem when compiling marco... will fix tomorrow
  {
    gSystem->mkdir(savFolder);
    savFolder = "AutoPlots/";
    savFolder += date;
    cout << "Plot output folder: " << savFolder << " created " << endl;
  }
  savFolder += "/";
  savFolder += (subFolder);
  if (((gSystem->OpenDirectory(savFolder)) == 0)) //gSystem causesing problem when compiling marco... will fix tomorrow
  {
    gSystem->mkdir(savFolder);
    cout << "Plot output folder: " << savFolder << " created " << endl;
  }
  else
  {
    cout << "The folder " << savFolder << " already exists, saving plots here" << endl;
  }
  return savFolder;
}























TFile *Get_Strip_File(Int_t run_number, Bool_t die)
{
  TFile *f = NULL;

  TString file_name(getenv("STRIPFILES"));
  file_name += "/alphaStrips";
  if (run_number < 10000)
    file_name += "0";
  if (run_number < 1000)
    file_name += "0";
  if (run_number < 100)
    file_name += "0";
  if (run_number < 10)
    file_name += "0";
  file_name += run_number;
  file_name += ("offline.root");

  TString EOS_name(getenv("EOS_MGM_URL"));
  EOS_name += "//eos/experiment/alpha/StripTrees";
  if (run_number < 10000)
    EOS_name += "0";
  if (run_number < 1000)
    EOS_name += "0";
  if (run_number < 100)
    EOS_name += "0";
  if (run_number < 10)
    EOS_name += "0";
  EOS_name += run_number;
  EOS_name += "offline.root";
  f = (TFile *)gROOT->GetListOfFiles()->FindObject(file_name);
  if (f == NULL)
  {
    f = (TFile *)gROOT->GetListOfFiles()->FindObject(EOS_name);
    if (f != NULL)
      return f;
  }
  else
  {
    return f;
  }
  f = new TFile(file_name.Data(), "READ");
  if (!f->IsOpen())
  {
    //If the file isnt found locally... find it remotely on EOS
    f = TFile::Open(EOS_name);
  }
  if (f == NULL)
  {
    if (die)
    {
      Error("Get_File", "\033[33mCould not open tree file for run %d\033[00m", run_number);
      TObject *dum = NULL;
      dum->GetName(); // This is to crash the CINT interface  instead of exiting (deliberately)
    }
    else
      return NULL;
  }
  if (!f->IsOpen())
  {
    if (die)
    {
      Error("Get_File", "\033[33mCould not open tree file for run %d\033[00m", run_number);
      TObject *dum = NULL;
      dum->GetName(); // This is to crash the CINT interface  instead of exiting (deliberately)
    }
    else
      return NULL;
  }
  return f;
}

TFile *Get_Cosmic_File(Int_t run_number, Bool_t die)
{
  TFile *f = NULL;

  TString file_name(getenv("TREEFILES"));
  file_name += gTreePrefix;
  if (run_number < 10000)
    file_name += "0";
  if (run_number < 1000)
    file_name += "0";
  if (run_number < 100)
    file_name += "0";
  if (run_number < 10)
    file_name += "0";
  file_name += run_number;
  file_name += "_IsCosmic_CosmicUtil";
  file_name += gTreePostfix;

  TString EOS_name(getenv("EOS_MGM_URL"));
  EOS_name += "//eos/experiment/alpha/alphaTrees";
  EOS_name += gTreePrefix;
  if (run_number < 10000)
    EOS_name += "0";
  if (run_number < 1000)
    EOS_name += "0";
  if (run_number < 100)
    EOS_name += "0";
  if (run_number < 10)
    EOS_name += "0";
  EOS_name += run_number;
  EOS_name += "_IsCosmic_CosmicUtil";
  EOS_name += gTreePostfix;

  //Set pointer to file if already open (by checking TFile file names)
  f = (TFile *)gROOT->GetListOfFiles()->FindObject(file_name);
  if (f == NULL)
  {
    f = (TFile *)gROOT->GetListOfFiles()->FindObject(EOS_name);
    if (f != NULL)
      return f;
  }
  else
  {
    return f;
  }
  f = new TFile(file_name.Data(), "READ");
  TString hostname = gSystem->HostName();
  if (!f->IsOpen() && strcmp(hostname.Data(), "alphasvnchecker.cern.ch") != 0) //disable alphasvnchecker getting files from cloud
  {
    //If the file isnt found locally... find it remotely on EOS
    f = TFile::Open(EOS_name);
  }
  //if(!f->IsOpen())
  if (f == NULL)
  {
    if (die)
    {
      Error("Get_File", "\033[33mCould not open tree file for run %d\033[00m", run_number);
      TObject *dum = NULL;
      dum->GetName(); // This is to crash the CINT interface  instead of exiting (deliberately)
    }
    else
      return NULL;
  }
  if (!f->IsOpen())
  {
    if (die)
    {
      Error("Get_File", "\033[33mCould not open tree file for run %d\033[00m", run_number);
      TObject *dum = NULL;
      dum->GetName(); // This is to crash the CINT interface  instead of exiting (deliberately)
    }
    else
      return NULL;
  }

  return f;
}
void Close_File(Int_t run_number)
{
  cout << "BEFORE:";
  gROOT->GetListOfFiles()->ls();
  TFile *f = NULL;
  TString file_name(getenv("TREEFILES"));
  file_name += gTreePrefix;
  if (run_number < 10000)
    file_name += "0";
  if (run_number < 1000)
    file_name += "0";
  if (run_number < 100)
    file_name += "0";
  if (run_number < 10)
    file_name += "0";
  file_name += run_number;
  file_name += gTreePostfix;

  TString EOS_name(getenv("EOS_MGM_URL"));
  EOS_name += "//eos/experiment/alpha/alphaTrees";
  EOS_name += gTreePrefix;
  if (run_number < 10000)
    EOS_name += "0";
  if (run_number < 1000)
    EOS_name += "0";
  if (run_number < 100)
    EOS_name += "0";
  if (run_number < 10)
    EOS_name += "0";
  EOS_name += run_number;
  EOS_name += gTreePostfix;
  //Set pointer to file if already open (by checking TFile file names)

  f = (TFile *)gROOT->GetListOfFiles()->FindObject(file_name);
  if (f == NULL)
  {
    f = (TFile *)gROOT->GetListOfFiles()->FindObject(EOS_name);
    if (f != NULL)
    {
      f->Close();
      delete f;
      cout << "AFTER:";
      gROOT->GetListOfFiles()->ls();
    }
  }
  else
  {
    f->Close();
    delete f;
    cout << "AFTER2:";
    gROOT->GetListOfFiles()->ls();
  }
}

TFile *Get_File(Int_t run_number, Bool_t die)
{
  TFile *f = NULL;
  TString file_name(getenv("TREEFILES"));
  file_name += gTreePrefix;
  if (run_number < 10000)
    file_name += "0";
  if (run_number < 1000)
    file_name += "0";
  if (run_number < 100)
    file_name += "0";
  if (run_number < 10)
    file_name += "0";
  file_name += run_number;
  file_name += gTreePostfix;

  TString EOS_name(getenv("EOS_MGM_URL"));
  EOS_name += "//eos/experiment/alpha/alphaTrees";
  EOS_name += gTreePrefix;
  if (run_number < 10000)
    EOS_name += "0";
  if (run_number < 1000)
    EOS_name += "0";
  if (run_number < 100)
    EOS_name += "0";
  if (run_number < 10)
    EOS_name += "0";
  EOS_name += run_number;
  EOS_name += gTreePostfix;
  //Set pointer to file if already open (by checking TFile file names)

  f = (TFile *)gROOT->GetListOfFiles()->FindObject(file_name);
  if (f == NULL)
  {
    f = (TFile *)gROOT->GetListOfFiles()->FindObject(EOS_name);
    if (f != NULL)
      return f;
  }
  else
  {
    return f;
  }
  f = new TFile(file_name.Data(), "READ");
  TString hostname = gSystem->HostName();
  if (!f->IsOpen() && strcmp(hostname.Data(), "alphasvnchecker.cern.ch") != 0) //disable alphasvnchecker getting files from cloud
  {
    //If the file isnt found locally... find it remotely on EOS
    f = TFile::Open(EOS_name);
  }
  if (!f->IsOpen())
  //if(f==NULL)
  {
    if (die)
    {
      Error("Get_File", "\033[33mCould not open tree file for run %d\033[00m", run_number);
      TObject *dum = NULL;
      dum->GetName(); // This is to crash the CINT interface  instead of exiting (deliberately)
    }
    else
      return NULL;
  }
  return f;
}

int CloseOpenFiles()
{
  gROOT->GetListOfFiles()->Delete();
  return 0;
}

void RunMVACosmicDumper(Int_t run_number, Double_t tmin, Double_t tmax)
{
  TString DumpRF = getenv("RELEASE");
  DumpRF += "/alphaMVA/dumpers/dumper/dumper.exe ";
  DumpRF += getenv("TREEFILES");
  DumpRF += gTreePrefix;
  DumpRF += run_number;
  DumpRF += gTreePostfix;
  DumpRF += " --usetimerange=";
  DumpRF += tmin;
  DumpRF += ":";
  DumpRF += tmax;
  DumpRF += " --autoQOD ";
  //DumpRF+=PHASE;
  DumpRF += "COSMICS &> CosmicDumper";
  DumpRF += run_number;
  DumpRF += ".log";
  cout << DumpRF << endl;
  gSystem->Exec(DumpRF);
}

void RunMVACosmicHitDumper(Int_t run_number, Double_t tmin, Double_t tmax)
{
  TString DumpRF = getenv("RELEASE");
  DumpRF += "/alphaMVA/dumpers/hitDumper/hitDumper.exe ";
  DumpRF += getenv("TREEFILES");
  DumpRF += gTreePrefix;

  DumpRF += run_number;
  DumpRF += gTreePostfix;
  DumpRF += " --usetimerange=";
  DumpRF += tmin;
  DumpRF += ":";
  DumpRF += tmax;
  DumpRF += " --autoQOD ";
  //DumpRF+=PHASE;
  DumpRF += "COSMICS &> CosmicDumper";
  DumpRF += run_number;
  DumpRF += ".log";
  cout << DumpRF << endl;
  gSystem->Exec(DumpRF);
}

void RunMVACosmicHitTrackDumper(Int_t run_number, Double_t tmin, Double_t tmax)
{
  TString DumpRF = getenv("RELEASE");
  DumpRF += "/alphaMVA/dumpers/trackDumper/hit_trackDumper.exe ";
  DumpRF += getenv("TREEFILES");
  DumpRF += gTreePrefix;

  DumpRF += run_number;
  DumpRF += gTreePostfix;
  DumpRF += " --usetimerange=";
  DumpRF += tmin;
  DumpRF += ":";
  DumpRF += tmax;
  DumpRF += " --autoQOD ";
  //DumpRF+=PHASE;
  DumpRF += "COSMICS &> CosmicDumper";
  DumpRF += run_number;
  DumpRF += ".log";
  cout << DumpRF << endl;
  gSystem->Exec(DumpRF);
}

void RunMVADumper(Int_t run_number, const char *PHASE)
{
  TFile *f = Get_File(run_number);
  TString s = f->GetPath();
  TString DumpRF = getenv("RELEASE");
  DumpRF += "/alphaMVA/dumpers/dumper/dumper.exe ";
  if (s.Contains("/eos/experiment/alpha/"))
  {
    DumpRF += getenv("EOS_MGM_URL");
    DumpRF += "//eos/experiment/alpha/alphaTrees";
    DumpRF += gTreePrefix;
  }
  else
  {
    DumpRF += getenv("TREEFILES");
    DumpRF += gTreePrefix;
  }

  DumpRF += run_number;
  DumpRF += gTreePostfix;
  DumpRF += " ";
  DumpRF += PHASE;
  DumpRF += " --allowOnline";
  cout << DumpRF << endl;
  gSystem->Exec(DumpRF);
  return;
}

TFile *Get_Dumper_File(Int_t run_number) //, const char* PHASE)
{

  //check for dumper output:

  TFile *f = NULL;
  TString DumperOut = getenv("MVATREES");
  //cout << MakeRF << endl;
  //MakeRF+="/alphaMVA/randforest/seldata/";
  DumperOut += "/";
  //  if (strcmp(phase,"ALL")==0) DumperOut+="all";
  DumperOut += "all";
  DumperOut += "_tree";
  DumperOut += run_number;
  DumperOut += gTreePostfix;

  f = (TFile *)gROOT->GetListOfFiles()->FindObject(DumperOut);

  if (f == NULL)
  {
    f = new TFile(DumperOut.Data(), "READ");
  }

  f->SetBit(kMustCleanup);

  if (!f->IsOpen())
  {
    //Error("Get_File", "\033[33mCould not open tree file for run %d\033[00m", run_number);
    cout << "dumper.exe output not found... running dumper" << endl;
    RunMVADumper(run_number);
    TString phase;
    f = Get_Dumper_File(run_number); //,phase);
  }
  return f;
}
TTree *Get_Dumper_Tree(Int_t run_number) //, const char* phase)
{
  TString tree = "selected_";
  //if (strcmp(PHASE,"ALL")==0) tree+="all";
  tree += "all";
  //tree+=phase;
  //else {std::cout <<"Phase not supported... find bug in Utils.C... hard coded shit needs more cases"<<std::endl; return NULL;}
  //tree+=phase;
  TFile *f = Get_Dumper_File(run_number); //, "all");
  return ((TTree *)f->Get(tree));
}

TTree *GetSequencerTree(Int_t runNumber)
{
  TFile *runFile = Get_File(runNumber);
  TTree *sequencerTree = (TTree *)runFile->Get("gSeqTree");

  if (sequencerTree == NULL)
  {
    Error("GetSequencerTree", "\033[31mTree file for run %d not found\033[00m", runNumber);
    sequencerTree->GetName(); // This is to crash the CINT interface  instead of exiting (deliberately)
  }

  return sequencerTree;
}
TTree *GetSequencerStateTree(Int_t runNumber)
{
  TFile *runFile = Get_File(runNumber);
  TTree *sequencerTree = (TTree *)runFile->Get("gSeqStateTree");

  if (sequencerTree == NULL)
  {
    Error("GetSequencerStateTree", "\033[31mTree file for run %d not found\033[00m", runNumber);
    sequencerTree->GetName(); // This is to crash the CINT interface  instead of exiting (deliberately)
  }

  return sequencerTree;
}
TTree *GetSequencerXMLTree(Int_t runNumber)
{
  TFile *runFile = Get_File(runNumber);
  TTree *sequencerTree = (TTree *)runFile->Get("gSeqXMLTree");

  if (sequencerTree == NULL)
  {
    Error("GetSequencerXMLTree", "\033[31mTree file for run %d not found\033[00m", runNumber);
    sequencerTree->GetName(); // This is to crash the CINT interface  instead of exiting (deliberately)
  }

  return sequencerTree;
}
TTree *GetSequenceDriverTree(Int_t runNumber)
{
  TFile *runFile = Get_File(runNumber);
  TTree *drvTree = (TTree *)runFile->Get("gSeqDriverTree");

  if (drvTree == NULL)
  {
    Error("gSequenceDriverTree", "\033[31mTree file for run %d not found\033[00m", runNumber);
    drvTree->GetName(); // This is to crash the CINT interface  instead of exiting (deliberately)
  }
  return drvTree;
}

TTree *Get_Sis_Tree(Int_t run_number)
{
  //load file
  TFile *root_file = Get_File(run_number);

  TTree *sis_tree = NULL;
  sis_tree = (TTree *)root_file->Get("gSisTree");
  if (sis_tree == NULL)
  {
    Error("Get_Sis_Tree", "\033[31mSIS Tree for run number %d not found\033[00m", run_number);
    sis_tree->GetName(); // This is to crash the CINT interface  instead of exiting (deliberately)
  }

  return sis_tree;
}

TTree *Get_Sis_Tree(Int_t run_number, Int_t channel)
{
  TFile *theFile = Get_File(run_number);

  TString treeName = "gSisTree";
  if (channel < 10)
    treeName += "0";
  treeName += channel;

  TTree *theTree = (TTree *)theFile->Get(treeName.Data());

  if (theTree == NULL)
  {
    Error("Get_Sis_Tree", "\033[31mCould not get a tree named %s for run %d\033[00m", treeName.Data(), run_number);
    theTree->GetName(); // This is to crash the CINT interface  instead of exiting (deliberately)
    return NULL;
  }

  return theTree;
}

TTree *Get_Vtx_Tree(Int_t run_number)
{
  //load file
  TFile *root_file = Get_File(run_number);

  TTree *vtx_tree = NULL;
  vtx_tree = (TTree *)root_file->Get("gSiliconTree");
  if (vtx_tree == NULL)
  {
    Error("Get_Vtx_Tree", "\033[31mVertex Tree for run number %d not found\033[00m", run_number);
    //gSystem->Exit(1);
    vtx_tree->GetName(); // This is to crash the CINT interface  instead of exiting (deliberately)
  }
  return vtx_tree;
}

TTree *Get_TTC_Tree(Int_t run_number)
{
  //load file
  TFile *root_file = Get_File(run_number);

  TTree *ttc_tree = NULL;
  ttc_tree = (TTree *)root_file->Get("gTTCTree");
  if (ttc_tree == NULL)
  {
    Error("Get_TTC_Tree", "\033[31mTTC Tree for run number %d not found\033[00m", run_number);
    //gSystem->Exit(1);
    ttc_tree->GetName(); // This is to crash the CINT interface  instead of exiting (deliberately)
  }
  return ttc_tree;
}

TSequencerDriver* GetSequenceDriver(Int_t runNumber, int SeqNum)
{
  TTree* t=GetSequenceDriverTree(runNumber);
  TSequencerDriver* drv=new TSequencerDriver();
  t->SetBranchAddress("SequenceDriver", &drv);
  for ( int i=0; i<t->GetEntries(); i++)
  {
    t->GetEntry(i);
    if (drv->SeqNum==SeqNum)
      return drv;
  }
  delete drv;
  return NULL;
}

int GetSequenceDO(Int_t runNumber, Int_t SeqNum, TString Bit)
{
  TSequencerDriver* drv=GetSequenceDriver(runNumber, SeqNum);
  int chn=-99;
  if (drv->DigitalMap->ChannelDescriptionMap.count(Bit))
    chn=drv->DigitalMap->ChannelDescriptionMap.at(Bit);
  return chn;
}

int GetSequenceDOByDescription(Int_t runNumber, Int_t SeqNum, TString Description)
{
  TSequencerDriver* drv=GetSequenceDriver(runNumber, SeqNum);
  int chn=-99;
  if (drv->DigitalMap->ChannelDescriptionMap.count(Description))
    chn=drv->DigitalMap->ChannelDescriptionMap.at(Description);
  return chn;
}

int GetSequenceDOStopDump(Int_t runNumber, Int_t SeqNum)
{
  TSequencerDriver* drv=GetSequenceDriver(runNumber, SeqNum);
  int chn=-99;
  if (drv->DigitalMap->ChannelDescriptionMap.count("Dump end"))
    chn=drv->DigitalMap->ChannelDescriptionMap.at("Dump end");
  return chn;
}

int GetSequenceDOStartDump(Int_t runNumber, Int_t SeqNum)
{
  TSequencerDriver* drv=GetSequenceDriver(runNumber, SeqNum);
  int chn=-99;
  if (drv->DigitalMap->ChannelDescriptionMap.count("Dump start"))
    chn=drv->DigitalMap->ChannelDescriptionMap.at("Dump start");
  return chn;
}


Int_t *TTC_Multiplicity_by_Layer(Int_t runNumber, Int_t EventNumber[])
{
  char mapName[255];
  sprintf(mapName, "%s/aux/maps/vf48_map.00015", getenv("RELEASE"));
  TVF48SiMap map(mapName);
  // VA number
  // TA number % 4 = 1 -> VA #1  (n-side)
  // TA number % 4 = 2 -> VA #2  (p-side)
  // TA number % 4 = 3 -> VA #3  (p-side)
  // TA number % 4 = 0 -> VA #4  (n-side)
  Int_t TAByModule[72]; //={0};
  for (Int_t i = 0; i < 72; i++)
  {
    TAByModule[i] = 0;
  }

  Int_t *TAbyLayer = new Int_t[3]; //{0};
  for (Int_t i = 0; i < 3; i++)
  {
    TAbyLayer[i] = 0;
  }
  Int_t TAs[16];
  for (Int_t i = 0; i < 16; i++)
  {
    TAs[i] = 0;
  }
  //populate triggers by module
  TTree *ttc_tree = Get_TTC_Tree(runNumber);
  TTCEvent *ttc_event = new TTCEvent();
  ttc_tree->SetBranchAddress("TTCEvent", &ttc_event);
  for (Int_t j = 0; j < 4; j++)
  {
    if (EventNumber[j] < 0)
      continue; //ignore bad values of EventNumber
    ttc_event->ClearTTCEvent();
    //cout << "Event:" << EventNumber[j] << endl;
    ttc_tree->GetEntry(EventNumber[j]);
    //ttc_event->Print();
    for (Int_t BankNumber = 0; BankNumber < 8; BankNumber++)
    {
      uint16_t latch = ttc_event->GetLatch(BankNumber);
      uint16_t compare = 1;
      int counter = 0;
      do
      {
        if (latch & compare)
        {
          TAs[counter] = 1;
          //        if (counter % 4==1 || counter % 4 == 2)
          //        {
          //         cout <<"FPGA:"<< j <<" Bank:"<< BankNumber << " counter:" << counter << endl;
          //       cout <<"TA: "<< counter + 16*BankNumber + 128*j <<" si: "<< map.GetSil(counter + 16*BankNumber + 128*j )<<endl;
          //   }
        }
        else
          TAs[counter] = 0;
        compare = compare << 1;
        counter++;
      } while (counter < 16);
      for (Int_t TA = 0; TA < 16; TA++)
      {
        if (TA % 4 == 0 || TA % 4 == 3)
        {
          continue;
        } //use only the trigger relivant TAs
        if (!TAs[TA])
          continue;
        if (ttc_event->GetFPGA() < 0)
        {
          cout << "Bad FPGA number" << endl;
          continue;
        }
        Int_t TAnumber = TA + 16 * BankNumber + 128 * ttc_event->GetFPGA();

        Int_t simod = map.GetSil(TAnumber);
        //Int_t layer = map.Get_ODB_Layer_Index(simod);
        //TAbyLayer[layer]++;
        if (simod >= 0)
        {
          TAByModule[simod]++;
          // cout <<"Layer for Si"<<simod<<":"<<layer <<endl;

          //cout<<simod<<endl;
        }
      }
    }
  }
  delete ttc_event;
  for (Int_t i = 0; i < 72; i++)
  {
    if (i < 10)
      TAbyLayer[0] += TAByModule[i]; //inner layer
    if (i >= 10 && i < 10 + 12)
      TAbyLayer[1] += TAByModule[i]; //middle layer
    if (i >= 22 && i < 10 + 12 + 14)
      TAbyLayer[2] += TAByModule[i]; //outer layer
    if (i >= 36 + 0 && i < 36 + 10)
      TAbyLayer[0] += TAByModule[i]; //inner layer
    if (i >= 36 + 10 && i < 36 + 10 + 12)
      TAbyLayer[1] += TAByModule[i]; //middle layer
    if (i >= 36 + 22 && i < 36 + 10 + 12 + 14)
      TAbyLayer[2] += TAByModule[i]; //outer layer
  }

  return TAbyLayer;
}

TArray *Get_RunTime_of_Pulse(Int_t runNumber, vector<Int_t> VetoChan, Int_t tmin, Int_t tmax)
{

  Int_t length = 0;
  for (uint j = 0; j < VetoChan.size(); j++)
  {
    //TTree* sis_tree = Get_Sis_Tree( runNumber, VetoChan.at(j) );
    // channel 39: "SIS_DIX_WALPHA"
    //
    length = length + Count_SIS_Triggers(runNumber, VetoChan.at(j), tmin, tmax);
    //sis_tree->GetEntries();
  }
  TArrayD *pulseArray = new TArrayD(length);
  cout << "Total pulses (in " << VetoChan.size() << " chans):" << length << endl;
  Int_t k = 0;
  for (uint j = 0; j < VetoChan.size(); j++)
  {
    cout << "CHAN" << VetoChan.at(j) << endl;
    TSisEvent *sis_event = new TSisEvent();
    TTree *sis_tree = Get_Sis_Tree(runNumber, VetoChan.at(j));

    if (length == 0)
      cout << "No AD pulses during time window" << endl;
    //cout<<length<<endl;

    sis_tree->SetBranchAddress("SisEvent", &sis_event);

    for (Int_t i = 0; i < sis_tree->GetEntries(); ++i)
    {
      sis_tree->GetEntry(i);
      Double_t run_time = sis_event->GetRunTime();
      if (run_time <= tmin)
        continue;
      if (run_time > tmax)
        break;
      pulseArray->SetAt(run_time, k);
      k++;
      cout << run_time << endl;
    }
  }
  return pulseArray;
}

Double_t Get_RunTime_of_Count(Int_t run_number, Int_t channel, Int_t count)
{
  Double_t run_time(-999.);
  TTimestampHelper* ts=GetTimestampHelper(run_number,channel);
  if (ts) run_time= ts->GetTimeOfSISCount(channel,count);
  if (run_time>0) return run_time;
  TTree *tree = Get_Sis_Tree(run_number, channel);
  if (tree == NULL)
    return -1.;
  run_time = Get_RunTime_of_Count(tree, count);

  if (run_time < 0.)
    Error("Get_RunTime_of_Count", "\033[33mCould not find runTime of trigger count %d in run %d, channel %d\033[00m",
          count, run_number, channel);

  return run_time;
}

Double_t Get_RunTime_of_Count(TTree *tree, Int_t count, Int_t StartPos)
{
  TSisEvent *sis_event = new TSisEvent();
  tree->SetBranchAddress("SisEvent", &sis_event);
  Int_t counts(0);
  Double_t time(-999.);
  for (Int_t i = StartPos; i < tree->GetEntriesFast(); i++)
  {
    tree->GetEntry(i);
    counts += sis_event->GetCountsInChannel();
    //      cout<<i<<"\t"<<counts<<endl;
    if (counts >= count)
    {
      time = sis_event->GetRunTime();
      break;
    }
  }

  //  Info("Get_RunTime_of_Count", "\033[33mFound runTime of trigger count %d in channel %d: %lf\033[00m",
  //	   counts, sis_event->GetChannel(), time);
  delete sis_event;
  delete tree;
  return time;
}

Double_t Get_RunTime_of_Count(Int_t run_number, Int_t channel, Int_t count, Double_t tmin, Double_t tmax)
{
  TTree *tree = Get_Sis_Tree(run_number, channel);
  if (tree == NULL)
    return -1.;
  //  cout<<"SIS "<<channel<<" tree entries "<<tree->GetEntries()<<endl;

  Double_t run_time(-999.);
  run_time = Get_RunTime_of_Count(tree, count, tmin, tmax);

  if (run_time == -999.)
    Error("Get_RunTime_of_Count", "\033[33mCould not find runTime of trigger count %d in run %d, channel %d\033[00m",
          count, run_number, channel);

  return run_time;
}

Double_t Get_RunTime_of_Count(TTree *tree, Int_t count, Double_t tmin, Double_t tmax)
{
  TSisEvent *sis_event = new TSisEvent();
  tree->SetBranchAddress("SisEvent", &sis_event);
  Int_t counts(0);
  /*
  char text[120];
  sprintf(text, "RunTime>=%.6lf && RunTime<=%.6lf", tmin, tmax);
  Long64_t drawCounts = tree->Draw(">>countList", text);
  TEventList* countList = (TEventList*)gDirectory->Get("countList");

  if(countList==NULL)
    {
      Error("Get_RunTime_of_Count", "Eventlist for %s, count %d (%.3lf s,->%.3lf s) is NULL",
	    tree->GetName(), count, tmin, tmax);
    }
*/
  Double_t time = -1.; // setting to -1 to remove compiler warning

  for (Int_t i = 0; i < tree->GetEntriesFast(); i++)
  {
    tree->GetEntry(i);
    if (sis_event->GetRunTime() < tmin)
      continue;
    if (sis_event->GetRunTime() > tmax)
      return -1;
    counts += sis_event->GetCountsInChannel();
    //      cout<<i<<"\t"<<counts<<endl;
    if (counts >= count)
    {
      time = sis_event->GetRunTime();
      break;
    }
  }
  //  Info("Get_RunTime_of_Count", "\033[33mFound runTime of trigger count %d in channel %d: %lf\033[00m",
  //	   counts, sis_event->GetChannel(), time);
  //  delete countList;
  delete sis_event;
  delete tree;
  return time;
}
Double_t Get_RunTime_of_SISChan(Int_t run_number, Int_t channel, Int_t rep)
{
  TTree *tree = Get_Sis_Tree(run_number, channel);
  if (tree == NULL)
    return -1.;
  TSisEvent *sis_event = new TSisEvent();
  tree->SetBranchAddress("SisEvent", &sis_event);
  tree->GetEntry(rep);
  Double_t run_time(-999.);
  run_time = sis_event->GetRunTime();

  if (run_time == -999.)
    Error("Get_RunTime_of_SISChan", "\033[33mCould not find runTime of trigger count %d in run %d, channel %d\033[00m", rep, run_number, channel);
  if (run_time == 0.)
    Info("Get_RunTime_of_SISChan", "\033[33m runTime=0  trigger count %d in run %d, channel %d\033[00m", rep, run_number, channel);
  delete sis_event;
  return run_time;
}

/*
 * Return the total run time in seconds with 10MHz precision from the SIS scaler modules
 */

Double_t GetTotalRunTimeByMidas(Int_t runNumber)
{
  std::cerr<<"Getting total run time from MIDAS time... is the SIS bad?"<<std::endl;
  TAnalysisReport* a=GetAnalysisReport(runNumber);
  Double_t RunTime=(Double_t) a->GetStopTimeBinary() - (Double_t) a->GetStartTimeBinary();
  return RunTime;
}
Double_t GetTotalRunTimeByVF48Clock(Int_t runNumber)
{
  std::cerr<<"Getting total run time from VF48 clock... is the SIS or 50Mhz bad?"<<std::endl;
  TAnalysisReport* a=GetAnalysisReport(runNumber);
  Double_t RunTime = ((Double_t)a->GetVF48TicksTotal())/20E6;
  delete a;
  if (RunTime<0.1) return GetTotalRunTimeByMidas(runNumber);
  return RunTime;
}

Double_t GetTotalRunTimeBy50MHzClock(Int_t runNumber)
{
  std::cerr<<"Getting total run time from 50Mhz clock... is the 10Mhz bad?"<<std::endl;
  TSISChannels *sisch = new TSISChannels(runNumber);
  Int_t channel = sisch->GetChannel("SIS_50Mhz_CLK");
  TTree *sis_tree = Get_Sis_Tree(runNumber, channel);
  TSisEvent *sis_event = new TSisEvent();
  sis_tree->SetBranchAddress("SisEvent", &sis_event);
  sis_tree->GetEntry(sis_tree->GetEntries() - 1);
  delete sisch;
  Double_t RunTime = sis_event->GetRunTime();
  delete sis_event;
  if (RunTime <0.1) return GetTotalRunTimeByVF48Clock(runNumber);
  return RunTime;
}
 
Double_t GetTotalRunTime(Int_t runNumber)
{
  TSISChannels *sisch = new TSISChannels(runNumber);
  Int_t channel = sisch->GetChannel("SIS_10Mhz_CLK");
  TTree *sis_tree = Get_Sis_Tree(runNumber, channel);
  TSisEvent *sis_event = new TSisEvent();
  sis_tree->SetBranchAddress("SisEvent", &sis_event);
  sis_tree->GetEntry(sis_tree->GetEntries() - 1);
  delete sisch;
  Double_t RunTime = sis_event->GetRunTime();
  delete sis_event;
  if (RunTime<0) return GetTotalRunTimeBy50MHzClock(runNumber);
  return RunTime;
}

Int_t Count_SIS_Triggers(Int_t run_number, Int_t SIS_Channel, const char *description, Int_t repetition, Int_t offset, bool exact_match)
{
  Double_t StartTime = MatchEventToTime(run_number, "startDump", description, repetition, offset, exact_match);
  Double_t StopTime = MatchEventToTime(run_number, "stopDump", description, repetition, offset, exact_match);
  return Count_SIS_Triggers(run_number, SIS_Channel, StartTime, StopTime);
}

Int_t Count_SIS_Triggers(Int_t run_number, Int_t SIS_Channel, Double_t StartTime, Double_t StopTime)
{
  TTree *SIS_Tree = Get_Sis_Tree(run_number, SIS_Channel);
  TSisEvent *sisEvent = new TSisEvent();
  SIS_Tree->SetBranchAddress("SisEvent", &sisEvent);

  Int_t sum_counts(0);
#if 0
  //Binary search for first entry
  Int_t StartEntry = 0;
  //Skip search if start time is close to beginning of run
  if (StartTime > 100.)
  {
    Int_t First = 0;
    Int_t Last = SIS_Tree->GetEntriesFast();
    while (First <= Last)
    {
      Int_t m = First + (Last - First - 1) / 2;
      SIS_Tree->GetEntry(m);
      Double_t time = sisEvent->GetRunTime();
      if (StartTime > time)
        First = m + 1;
      else if (StartTime < time)
        Last = m - 1;
      else
      {
        StartEntry = m;
        break;
      }
    }
  }
#else
  Int_t StartEntry=GetFirstSISEventBeforeTime(run_number,SIS_Channel,StartTime);
#endif
  for (Int_t i = StartEntry; i < SIS_Tree->GetEntriesFast(); i++)
  {
    SIS_Tree->GetEntry(i);
    if (sisEvent->GetRunTime() < StartTime)
      continue;
    if (sisEvent->GetChannel() != SIS_Channel)
      continue;
    if (sisEvent->GetRunTime() > StopTime)
      break;
    sum_counts += sisEvent->GetCountsInChannel();
  }
  delete sisEvent;
  return sum_counts;
}

Int_t Count_SIS_Triggers(Int_t run_number, Int_t SIS_Channel)
{
  TTree *SIS_Tree = Get_Sis_Tree(run_number, SIS_Channel);
  TSisEvent *sisEvent = new TSisEvent();
  SIS_Tree->SetBranchAddress("SisEvent", &sisEvent);

  Int_t sum_counts(0);

  for (Int_t i = 0; i < SIS_Tree->GetEntries(); i++)
  {
    SIS_Tree->GetEntry(i);
    if (sisEvent->GetChannel() != SIS_Channel)
      continue;

    sum_counts += sisEvent->GetCountsInChannel();
  }
  delete sisEvent;
  return sum_counts;
}

Bool_t IsDumpInCAT(Int_t runNumber, const char *eventName, const char *description, Int_t repetition, Int_t offset)
{
  TSeq_Event *seqEvent = FindSequencerEvent(runNumber, eventName, description, repetition, offset);
  if (seqEvent != NULL)
  {

    if (!(seqEvent->GetSeq().CompareTo(TString("cat"))))
    {
      delete seqEvent;
      return kTRUE;
    }
    delete seqEvent;
  }
  return kFALSE;
}

Bool_t IsDumpInRCT(Int_t runNumber, const char *eventName, const char *description, Int_t repetition, Int_t offset)
{
  TSeq_Event *seqEvent = FindSequencerEvent(runNumber, eventName, description, repetition, offset);
  if (seqEvent != NULL)
  {
    if (!(seqEvent->GetSeq().CompareTo(TString("rct"))))
    {
      delete seqEvent;
      return kTRUE;
    }
    delete seqEvent;
  }
  return kFALSE;
}

Bool_t IsDumpInATM(Int_t runNumber, const char *eventName, const char *description, Int_t repetition, Int_t offset)
{
  TSeq_Event *seqEvent = FindSequencerEvent(runNumber, eventName, description, repetition, offset);
  if (seqEvent != NULL)
  {
    if (!(seqEvent->GetSeq().CompareTo(TString("atm"))))
    {
      delete seqEvent;
      return kTRUE;
    }
    delete seqEvent;
  }
  return kFALSE;
}

Bool_t IsDumpInPOS(Int_t runNumber, const char *eventName, const char *description, Int_t repetition, Int_t offset)
{
  TSeq_Event *seqEvent = FindSequencerEvent(runNumber, eventName, description, repetition, offset);
  if (seqEvent != NULL)
  {
    if (!(seqEvent->GetSeq().CompareTo(TString("pos"))))
    {
      delete seqEvent;
      return kTRUE;
    }
  }
  return kFALSE;
}

Int_t CountEventsInRun(Int_t runNumber, const char *eventName, const char *description, bool exact_match)
{
  Int_t RunningCount = 0;
  //I don't print errors if I don't find a dump... useful if a function wants to test for a dump and not worry the user with errors.

  // return the repetition(th) sequencer Event which matches eventName and CONTAINS the text in description
  TTree *sequencerTree = GetSequencerTree(runNumber);
  TSeq_Event *seqEvent = new TSeq_Event();
  sequencerTree->SetBranchAddress("SequenceEvent", &seqEvent);

  char search_string[80];
  sprintf(search_string, "\"%s", description); // add a " before the name

  //Int_t rep(0);
  for (Int_t i = 0; i < sequencerTree->GetEntries(); i++)
  {
    sequencerTree->GetEntry(i);
    if (seqEvent->GetEventName().CompareTo(TString(eventName), TString::kIgnoreCase))
      continue;
    //      if(seqEvent->GetDescription().CompareTo(TString(description), TString::kIgnoreCase)) continue;
    if (exact_match)
    {
      if ((seqEvent->GetDescription().CompareTo(TString(search_string) + "\"", TString::kIgnoreCase)))
        continue;
    }
    else
    {
      if (!(seqEvent->GetDescription().BeginsWith(TString(search_string), TString::kIgnoreCase)))
        continue;
    }
    //      ++rep;
    //      if(rep<repetition) continue;

    /*Info("FindSequencerEvent", "Found matching Event #%d %s %s",
	   i, seqEvent->GetEventName().Data(), seqEvent->GetDescription().Data());*/
    RunningCount++;
  }
  delete seqEvent;
  delete sequencerTree;

  return RunningCount;
}
Bool_t TestForEventInRun(Int_t runNumber, const char *eventName, const char *description, Int_t repetition, Int_t offset, bool exact_match)
{ //I don't print errors if I don't find a dump... useful if a function wants to test for a dump and not worry the user with errors.

  // return the repetition(th) sequencer Event which matches eventName and CONTAINS the text in description
  TTree *sequencerTree = GetSequencerTree(runNumber);
  TSeq_Event *seqEvent = new TSeq_Event();
  sequencerTree->SetBranchAddress("SequenceEvent", &seqEvent);

  char search_string[80];
  sprintf(search_string, "\"%s", description); // add a " before the name

  Int_t rep(0);
  for (Int_t i = 0; i < sequencerTree->GetEntries(); i++)
  {
    sequencerTree->GetEntry(i);
    if (seqEvent->GetEventName().CompareTo(TString(eventName), TString::kIgnoreCase))
      continue;
    //      if(seqEvent->GetDescription().CompareTo(TString(description), TString::kIgnoreCase)) continue;
    if (exact_match)
    {
      if ((seqEvent->GetDescription().CompareTo(TString(search_string) + "\"", TString::kIgnoreCase)))
        continue;
    }
    else
    {
      if (!(seqEvent->GetDescription().BeginsWith(TString(search_string), TString::kIgnoreCase)))
        continue;
    }
    ++rep;
    if (rep < repetition)
      continue;

    /*Info("FindSequencerEvent", "Found matching Event #%d %s %s",
	   i, seqEvent->GetEventName().Data(), seqEvent->GetDescription().Data());*/
    delete seqEvent;
    delete sequencerTree;
    return kTRUE;
  }
  delete seqEvent;
  delete sequencerTree;
  return kFALSE;
}
Double_t MatchEventToTime(Int_t runNumber, const char *eventName, const char *description, Int_t repetition, Int_t offset, bool exact_match)
{
  if ((strcmp(description, "alltimes") == 0) && (strcmp(eventName, "startDump") == 0))
    return 0;
  if ((strcmp(description, "alltimes") == 0) && (strcmp(eventName, "stopDump") == 0))
    return GetTotalRunTime(runNumber);
  TSeq_EventHelper* Seq=NULL;
  if (gAllowSeqEventHelper)
    Seq=GetSeqEventHelper(runNumber);
  if (Seq)
  {
    return Seq->GetRunTimeOfDump(eventName,description,repetition,offset,exact_match);
  }
    
  TSeq_Event *seqEvent = FindSequencerEvent(runNumber, eventName, description, repetition, offset, exact_match);
  if (seqEvent == NULL)
  {
    Error("MatchEventToTime", "\033[33mCould not find sequencer event %s (%s) in run %d\033[00m", eventName, description, runNumber);
    return -1;
  }

  Double_t runTime = Get_RunTime_of_SequencerEvent(runNumber, seqEvent, offset);

  delete seqEvent;
  return runTime;
}

Double_t GetStateID(Int_t runNumber, const char *eventName, const char *description, Int_t repetition, Int_t offset, bool exact_match)
{
    
  TSeq_Event *seqEvent = FindSequencerEvent(runNumber, eventName, description, repetition, offset, exact_match);
  if (seqEvent == NULL)
  {
    Error("GetStateID", "\033[33mCould not find sequencer event %s (%s) in run %d\033[00m", eventName, description, runNumber);
    return -1;
  }
  return seqEvent->getonState();
}



Int_t GetFirstVF48EventInDump(Int_t runNumber, const char *description, Int_t repetition, Int_t offset)
{
  Double_t DumpStart = MatchEventToTime(runNumber, "startDump", description, repetition, offset);
  TTree *sil_tree = Get_Vtx_Tree(runNumber);
  //TVector3* vertex;
  if (sil_tree == NULL)
    return -1;
  TSiliconEvent *sil_event = new TSiliconEvent();
  sil_tree->SetBranchAddress("SiliconEvent", &sil_event);
  sil_tree->SetBranchStatus("SiliconModules", 0);
  for (Int_t i = 0; i < sil_tree->GetEntries(); i++)
  {
    sil_event->ClearEvent();
    sil_tree->GetEntry(i);
    if (sil_event->GetRunTime() < DumpStart)
      continue;
    Int_t VF48Event=sil_event->GetVF48NEvent();
    delete sil_event;
    return VF48Event;
  }
  return -1;
}

Int_t GetLastVF48EventInDump(Int_t runNumber, const char *description, Int_t repetition, Int_t offset)
{
  Double_t DumpStop = MatchEventToTime(runNumber, "stopDump", description, repetition, offset);
  TTree *sil_tree = Get_Vtx_Tree(runNumber);
  //TVector3* vertex;
  if (sil_tree == NULL)
    return -1;
  TSiliconEvent *sil_event = new TSiliconEvent();
  sil_tree->SetBranchAddress("SiliconEvent", &sil_event);
  sil_tree->SetBranchStatus("SiliconModules", 0);
  for (Int_t i = 0; i < sil_tree->GetEntries(); i++)
  {
    sil_event->ClearEvent();
    sil_tree->GetEntry(i);
    if (sil_event->GetRunTime() < DumpStop)
      continue;
    delete sil_event;
    return (i);
  }
  delete sil_event;
  return -1;
}

TSeq_Event *FindSequencerEvent(Int_t runNumber, const char *eventName, const char *description, Int_t repetition, Int_t &offset, bool exact_match)
{
  // return the repetition(th) sequencer Event which matches eventName and CONTAINS the text in description OR is an exact match if exact_match
  TTree *sequencerTree = GetSequencerTree(runNumber);
  TSeq_Event *seqEvent = new TSeq_Event();
  sequencerTree->SetBranchAddress("SequenceEvent", &seqEvent);

  char search_string[80];
  sprintf(search_string, "\"%s", description); // add a " before the name

  Int_t rep(0);
  for (Int_t i = 0; i < sequencerTree->GetEntries(); i++)
  {
    sequencerTree->GetEntry(i);
    if (!seqEvent->GetDescription().CompareTo(TString(""), TString::kIgnoreCase))
      continue; //Old bugfix, now resolved elsewhere {offset++;  continue;} //If event has no description... its likely a laserpulse and has no dump timestamp...
    if (seqEvent->GetEventName().CompareTo(TString(eventName), TString::kIgnoreCase))
      continue;
    //      if(seqEvent->GetDescription().CompareTo(TString(description), TString::kIgnoreCase)) continue;

    if (exact_match)
    {
      if ((seqEvent->GetDescription().CompareTo(TString(search_string) + "\"", TString::kIgnoreCase)))
        continue;
    }
    else
    {
      if (!(seqEvent->GetDescription().BeginsWith(TString(search_string), TString::kIgnoreCase)))
        continue;
    }

    ++rep;
    if (rep < repetition)
      continue;

    Info("FindSequencerEvent", "Found matching Event #%d %s %s",
         i, seqEvent->GetEventName().Data(), seqEvent->GetDescription().Data());
    delete sequencerTree;
    return seqEvent;
  }

  delete sequencerTree;
  Error("FindSequencerEvent", "\033[33mCould not find a match for event: %s\t %s\033[00m",
        eventName, description);
  delete seqEvent;
  return NULL;
}

Double_t Get_RunTime_of_SequencerEvent(Int_t run_number, TSeq_Event *seqEvent, Int_t seqOffset, Bool_t RedundantCheck)
{
  Double_t timeOfDump=-1.;
  TSeq_EventHelper* Seq=NULL;
  
  Int_t SEQ_TIMESTAMP_CHANNEL, SEQ_START_CHANNEL;
  
  if (gAllowSeqEventHelper && !RedundantCheck)
    Seq=GetSeqEventHelper(run_number);
  if (Seq)
  {
    timeOfDump=Seq->GetRunTimeOfDump(seqEvent, seqOffset);
  }
  else
  {
    TSISChannels *sisch = new TSISChannels(run_number);
    Bool_t NotStartOrStop = kFALSE;
    Int_t startChannel=-1;

    if (!(seqEvent->GetSeq().CompareTo(TString("cat"))))
    {
      SEQ_TIMESTAMP_CHANNEL = sisch->GetChannel("SIS_PBAR_SEQ_TS");
      SEQ_START_CHANNEL = sisch->GetChannel("SIS_PBAR_SEQ_START");
      if (!seqEvent->GetEventName().CompareTo(TString("startDump")))
      {
        startChannel = sisch->GetChannel("SIS_PBAR_DUMP_START");
      }
      else
      {
        startChannel = sisch->GetChannel("SIS_PBAR_DUMP_STOP");
      }
    }
    else if (!(seqEvent->GetSeq().CompareTo(TString("rct"))))
    {
      SEQ_TIMESTAMP_CHANNEL = sisch->GetChannel("SIS_RECATCH_SEQ_TS");
      SEQ_START_CHANNEL = sisch->GetChannel("SIS_RECATCH_SEQ_START");
      if (!seqEvent->GetEventName().CompareTo(TString("startDump")))
      {
        startChannel = sisch->GetChannel("SIS_RECATCH_DUMP_START");
      }
      else
      {
        startChannel = sisch->GetChannel("SIS_RECATCH_DUMP_STOP");
      }
    }
    else if (!(seqEvent->GetSeq().CompareTo(TString("atm"))))
    {
      SEQ_TIMESTAMP_CHANNEL = sisch->GetChannel("SIS_ATOM_SEQ_TS");
      SEQ_START_CHANNEL = sisch->GetChannel("SIS_ATOM_SEQ_START");
      if (!seqEvent->GetEventName().CompareTo(TString("startDump")))
      {
        startChannel = sisch->GetChannel("SIS_ATOM_DUMP_START");
      }
      else if (!seqEvent->GetEventName().CompareTo(TString("stopDump")))
      {
        startChannel = sisch->GetChannel("SIS_ATOM_DUMP_STOP");
      }
      else
      {
        NotStartOrStop = kTRUE;
        startChannel = sisch->GetChannel("SEQLASERPULSE");
      }
    }
    else if (!(seqEvent->GetSeq().CompareTo(TString("pos"))))
    {
      SEQ_TIMESTAMP_CHANNEL = sisch->GetChannel("SIS_POS_SEQ_TS"); //Does not exist
      SEQ_START_CHANNEL = sisch->GetChannel("SIS_POS_SEQ_START");
      if (!seqEvent->GetEventName().CompareTo(TString("startDump")))
      {
        startChannel = sisch->GetChannel("SIS_POS_DUMP_START");
        //cout <<"start"<<startChannel<<endl;
      }
      else if (!seqEvent->GetEventName().CompareTo(TString("stopDump")))
      {
        startChannel = sisch->GetChannel("SIS_POS_DUMP_STOP");
        //cout <<"stop"<<startChannel<<endl;
      }
    }
    else
    {
      Error("Get_RunTime_of_SequencerEvent", "Unknown Sequencer %s", seqEvent->GetSeq().Data());
      return -999.;
    }
    Int_t ID = (seqEvent->GetID() - seqOffset) / 2;
    if (NotStartOrStop)
    {
      ID = seqEvent->GetID() - seqOffset;
    }
    timeOfDump = Get_RunTime_of_SISChan(run_number, startChannel, ID);
    delete sisch;
  }
  
  if (RedundantCheck)
  {
    Double_t timeOfSequenceStart = Get_RunTime_of_Count(run_number,
                                                        SEQ_START_CHANNEL, seqEvent->GetSeqNum());
    
    Double_t time = -1;
    if (SEQ_TIMESTAMP_CHANNEL>0) time=Get_RunTime_of_Count(run_number,
                                         SEQ_TIMESTAMP_CHANNEL,
                                         seqEvent->GetonCount(),
                                         timeOfSequenceStart, 99999.);
    // Redundancy check
    if ((abs(timeOfDump - time) > 0.005))
      //if(((timeOfDump)<0.0))
      Info("Get_RunTime_of_SequencerEvent", "Redundant dumptime check: timeOfSequenceTS %f dumptime %f ", time, timeOfDump);
//cout<<endl<<"Redundant dumptime check: timeOfSequenceTS "<< time<<" dumptime "<<timeOfDump<<endl;
#if DEBUG > 1
    cout << "run trig ch " << SEQ_TIMESTAMP_CHANNEL << " timeOfSequenceTS " << time << " onCount " << seqEvent->GetonCount() << " runtime " << timeOfDump << endl;
#endif
  }

  return timeOfDump;
}

//Functions imported for dumper

TSeq_Event *FindSequencerEvent(TTree *sequencerTree, const char *eventName, const char *description)
{

  char search_string[80];
  sprintf(search_string, "\"%s", description); // add a " before the name
  printf(search_string, "\"%s", description);  // add a " before the name

  TSeq_Event *SeqEvent = new TSeq_Event();
  sequencerTree->SetBranchAddress("SequenceEvent", &SeqEvent);

  for (Int_t i = 0; i < sequencerTree->GetEntries(); i++)
  {
    sequencerTree->GetEntry(i);
    //      cout<<"SeqEvent name "<<SeqEvent->GetEventName()<<std::endl;
    if (SeqEvent->GetEventName().CompareTo(TString(eventName), TString::kIgnoreCase))
      continue;
    //      cout<<"SeqEvent Description "<<SeqEvent->GetDescription()<<std::endl;
    if (!(SeqEvent->GetDescription().BeginsWith(TString(search_string), TString::kIgnoreCase)))
      continue;
    return SeqEvent;
  }
  delete SeqEvent;
  return 0;
}
TSeq_Event *FindSequencerEvent(TTree *sequencerTree, const char *eventName, const char *description, Int_t repetition, Int_t &offset)
{
  char search_string[80];
  sprintf(search_string, "\"%s", description); // add a " before the name
  //printf(search_string, "@@@\"%s", description); // add a " before the name

  // return the repetition(th) sequencer Event which matches eventName and CONTAINS the text in description
  TSeq_Event *seqEvent = new TSeq_Event();
  sequencerTree->SetBranchAddress("SequenceEvent", &seqEvent);

  Int_t rep(0);
  for (Int_t i = 0; i < sequencerTree->GetEntries(); i++)
  {
    sequencerTree->GetEntry(i);
    if (!seqEvent->GetDescription().CompareTo(TString(""), TString::kIgnoreCase))
    {
      offset++;
      continue;
    } //If event has no description... its likely a laserpulse and has no dump timestamp...
    if (seqEvent->GetEventName().CompareTo(TString(eventName), TString::kIgnoreCase))
      continue;
    //      if(seqEvent->GetDescription().CompareTo(TString(description), TString::kIgnoreCase)) continue;
    if (!(seqEvent->GetDescription().BeginsWith(TString(search_string), TString::kIgnoreCase)))
      continue;

    ++rep;
    if (rep < repetition)
      continue;

    Info("FindSequencerEvent", "Found matching Event #%d %s %s",
         i, seqEvent->GetEventName().Data(), seqEvent->GetDescription().Data());
    //      delete sequencerTree;
    return seqEvent;
  }

  //  delete sequencerTree;
  Error("FindSequencerEvent", "\033[33mCould not find a match for event: %s\t %s\033[00m",
        eventName, description);
  delete seqEvent;
  return NULL;
}

TTree *GetAlphaEvents(TFile *theFile)
{
  assert(theFile->IsOpen());
  TTree *tree = (TTree *)theFile->Get("gAlphaEventTree");
  assert(tree);
  std::cout << "Getting " << tree->GetName() << std::endl;
  return tree;
}

TTree *GetSiliconEvents(TFile *theFile)
{
  assert(theFile->IsOpen());
  TTree *tree = (TTree *)theFile->Get("gSiliconTree");
  assert(tree);
  std::cout << "Getting " << tree->GetName() << std::endl;
  return tree;
}

TTree *GetSequencerTree(TFile *theFile)
{
  assert(theFile->IsOpen());
  TTree *tree = (TTree *)theFile->Get("gSeqTree");
  assert(tree);
  std::cout << "Getting " << tree->GetName() << std::endl;
  return tree;
}

TTree *GetSISTree(TFile *theFile)
{
  assert(theFile->IsOpen());
  TTree *tree = (TTree *)theFile->Get("gSisTree");
  assert(tree);
  std::cout << "Getting " << tree->GetName() << std::endl;
  return tree;
}

TTree *GetSISTree(TFile *theFile, Int_t channel)
{
  assert(theFile->IsOpen());

  TString treeName = "gSisTree";
  if (channel < 10)
    treeName += "0";
  treeName += channel;
  //  std::cout<<"SIS tree name: "<< treeName<<std::endl;
  TTree *tree = (TTree *)theFile->Get(treeName.Data());
  assert(tree);
  std::cout << "Getting " << tree->GetName() << std::endl;
  return tree;
}
Double_t GetRunTimeOfCount(TTree *tree, Int_t count)
{
  TSisEvent *SISevent = new TSisEvent();
  tree->SetBranchAddress("SisEvent", &SISevent);

  Int_t counts = 0;
  Double_t time = -99.;
  for (Int_t i = 0; i < tree->GetEntries(); ++i)
  {
    tree->GetEntry(i);
    counts += SISevent->GetCountsInChannel();
    if (counts >= count)
    {
      time = SISevent->GetRunTime();
      SISevent->ClearSisEvent();
      break;
    }
    SISevent->ClearSisEvent();
  }

  delete SISevent;
  return time;
}

Double_t GetRunTimeOfCount(TTree *tree, Int_t count, Double_t seq_start_time)
{
  TSisEvent *SISevent = new TSisEvent();
  tree->SetBranchAddress("SisEvent", &SISevent);

  Int_t counts = 0;
  Double_t time = -999.;
  for (Int_t i = 0; i < tree->GetEntries(); ++i)
  {
    tree->GetEntry(i);
    time = SISevent->GetRunTime();

    if (time < seq_start_time)
    {
      SISevent->ClearSisEvent();
      continue;
    }

    counts += SISevent->GetCountsInChannel();

    if (counts >= count)
    {
      SISevent->ClearSisEvent();
      break;
    }

    SISevent->ClearSisEvent();
  }

  delete SISevent;
  return time;
}

Double_t GetRunTimeOfEntry(TTree *tree, Int_t entry)
{
  TSisEvent *sis_event = new TSisEvent();
  tree->SetBranchAddress("SisEvent", &sis_event);
  tree->GetEntry(entry);
  Double_t run_time(-99.);
  run_time = sis_event->GetRunTime();

  if (run_time == -99.)
    Error("GetRunTimeOfEntry", "\033[33mCould not find runTime of entry %d in tree %s\033[00m", entry, tree->GetName());
  if (run_time == 0.)
    Info("GetRunTimeOfEntry", "\033[33m runTime=0 for entry %d in tree %s\033[00m", entry, tree->GetName());

  return run_time;
}

// Verison for multiple dump names
void GetDumpTime(TFile *theFile, std::vector<TString> EventDescription, std::vector<Double_t> &StartTime, std::vector<Double_t> &StopTime, std::vector<TString> &dumpname)
{
  Int_t offset(0);
  Int_t repetition(0);
  Int_t lastcount(0);

  TTree *sequencerTree = GetSequencerTree(theFile);
  for (UInt_t iDescript = 0; iDescript < EventDescription.size(); iDescript++)
  {

    TSeq_Event *seqEvent = FindSequencerEvent(sequencerTree, "startDump", (char *)EventDescription[iDescript].Data(), repetition, offset);
    Int_t StartChannel = -1, StopChannel = -1;
    if (!seqEvent)
    {
      // std::cout<<"No sequencer event "<<EventDescription[iDescript]<<" found \n";
      continue;
    }
    if (!(seqEvent->GetSeq().CompareTo(TString("cat"))))
    {
      StartChannel = -1;
      continue; //only interest in atm dumps
                // StartChannel = gSISch->GetChannel("SIS_PBAR_DUMP_START");
      //StopChannel = gSISch->GetChannel("SIS_PBAR_DUMP_STOP");
    }
    else if (!(seqEvent->GetSeq().CompareTo(TString("rct"))))
    {
      StartChannel = -1;
      continue; //only interest in atm dumps
                //StartChannel = gSISch->GetChannel("SIS_RECATCH_DUMP_START");
                //StopChannel = gSISch->GetChannel("SIS_RECATCH_DUMP_STOP");
    }
    else if (!(seqEvent->GetSeq().CompareTo(TString("atm"))))
    {
      StartChannel = gSISch->GetChannel("SIS_ATOM_DUMP_START");
      StopChannel = gSISch->GetChannel("SIS_ATOM_DUMP_STOP");
    }
    else if (!(seqEvent->GetSeq().CompareTo(TString("pos"))))
    {
      StartChannel = -1;
      continue; //only interest in atm dumps
                //StartChannel = gSISch->GetChannel("SIS_POS_DUMP_START");
                //StopChannel = gSISch->GetChannel("SIS_POS_DUMP_STOP");
    }
    else
    {
      Error("GetDumpTime", "Unknown Sequencer %s", seqEvent->GetSeq().Data());
      return;
    }
    assert(StartChannel >= 0);
    assert(StopChannel >= 0);
    //std::cout<<" --- "<<seqEvent->GetSeq()<<EventDescription[iDescript]<<" --- "<<std::endl;
    Int_t ID = seqEvent->GetID() / 2;
    Int_t count = ID + 1;
    if (count > lastcount + 20)
      count = lastcount + 1; //hack for ion events not in sis28/29
    lastcount = count;
    //    std::cout<<"Dump Number: "<<ID<<" Count "<<count<<std::endl;
    TTree *startTree = GetSISTree(theFile, StartChannel);
    //    StartTime.push_back(GetRunTimeOfCount(startTree, count));
    Double_t StartTimeEntry = GetRunTimeOfEntry(startTree, ID);
    StartTime.push_back(StartTimeEntry);
    std::cout << StartTimeEntry << "\t" << StartTime.back() << "\t";
    delete startTree;
    //std::cout<<seqEvent->GetName()<<" RunTime\tby count: "<<StartTime<<"\tby entry: "<<StartTimeEntry<<std::endl;
    TTree *stopTree = GetSISTree(theFile, StopChannel);
    //    StopTime.push_back(GetRunTimeOfCount(stopTree, count));
    Double_t StopTimeEntry = GetRunTimeOfEntry(stopTree, ID);
    StopTime.push_back(StopTimeEntry);
    std::cout << StopTimeEntry << "\t" << StopTime.back() << "\n";
    delete stopTree;
    dumpname.push_back(EventDescription[iDescript]);
    //std::cout<<seqEvent->GetName()<<" RunTime\tby count: "<<StopTime<<"\tby entry: "<<StopTimeEntry<<std::endl;
    //  cout<<"----------------------------------"<<endl;
    delete seqEvent;
  }
  delete sequencerTree;
}
// version for repeated dumps same dumpname
void GetDumpTime(TFile *theFile, const char *EventDescription, std::vector<Double_t> &StartTime, std::vector<Double_t> &StopTime, std::vector<TString> &dumpname, Int_t nreps)
{
  Int_t offset(0);
  Int_t lastcount(0);

  TTree *sequencerTree = GetSequencerTree(theFile);
  for (Int_t repetition = 0; repetition < nreps; repetition++)
  {
    TSeq_Event *seqEvent = FindSequencerEvent(sequencerTree, "startDump", EventDescription, repetition, offset);
    Int_t StartChannel = -1, StopChannel = -1;
    if (!seqEvent)
    {
      std::cout << "No sequencer event " << EventDescription << " found \n";
      break;
    }
    if (!(seqEvent->GetSeq().CompareTo(TString("cat"))))
    {
      StartChannel = gSISch->GetChannel("SIS_PBAR_DUMP_START");
      StopChannel = gSISch->GetChannel("SIS_PBAR_DUMP_STOP");
    }
    else if (!(seqEvent->GetSeq().CompareTo(TString("rct"))))
    {
      StartChannel = gSISch->GetChannel("SIS_RECATCH_DUMP_START");
      StopChannel = gSISch->GetChannel("SIS_RECATCH_DUMP_STOP");
    }
    else if (!(seqEvent->GetSeq().CompareTo(TString("atm"))))
    {
      StartChannel = gSISch->GetChannel("SIS_ATOM_DUMP_START");
      StopChannel = gSISch->GetChannel("SIS_ATOM_DUMP_STOP");
    }
    else if (!(seqEvent->GetSeq().CompareTo(TString("pos"))))
    {
      StartChannel = gSISch->GetChannel("SIS_POS_DUMP_START");
      StopChannel = gSISch->GetChannel("SIS_POS_DUMP_STOP");
    }
    else
    {
      Error("GetDumpTime", "Unknown Sequencer %s", seqEvent->GetSeq().Data());
      return;
    }

    assert(StartChannel >= 0);
    assert(StopChannel >= 0);
    std::cout << " --- " << EventDescription << " --- ";
    Int_t ID = seqEvent->GetID() / 2;
    Int_t count = ID + 1;
    if (count > lastcount + 20)
      count = lastcount + 1; //hack for ion events not in sis28/29
    lastcount = count;
    //    std::cout<<"Dump Number: "<<ID<<" Count "<<count<<std::endl;
    TTree *startTree = GetSISTree(theFile, StartChannel);
    //    StartTime.push_back(GetRunTimeOfCount(startTree, count));
    Double_t StartTimeEntry = GetRunTimeOfEntry(startTree, ID);
    StartTime.push_back(StartTimeEntry);
    std::cout << StartTimeEntry << "\t" << StartTime.back() << "\t";
    // std::cout<< GetRunTimeOfEntry(startTree, count)<<"\t"<<StartTime.back()<<"\t";
    delete startTree;
    //  cout<<seqEvent->GetName()<<" RunTime\tby count: "<<StartTime<<"\tby entry: "<<StartTimeEntry<<endl;
    TTree *stopTree = GetSISTree(theFile, StopChannel);
    //    StopTime.push_back(GetRunTimeOfCount(stopTree, count));
    Double_t StopTimeEntry = GetRunTimeOfEntry(stopTree, ID);
    StopTime.push_back(StopTimeEntry);
    std::cout << StopTimeEntry << "\t" << StopTime.back() << "\n";
    delete stopTree;
    dumpname.push_back(EventDescription);
    //  cout<<seqEvent->GetName()<<" RunTime\tby count: "<<StopTime<<"\tby entry: "<<StopTimeEntry<<endl;
    //  cout<<"----------------------------------"<<endl;
    delete seqEvent;
  } // for repetition
  delete sequencerTree;
}
//single dumptime version
void GetDumpTime(TFile *theFile, const char *EventDescription, Double_t &StartTime, Double_t &StopTime, Double_t repetition)
{
  Int_t offset = 0;
  TTree *sequencerTree = GetSequencerTree(theFile);
  TSeq_Event *seqEvent = FindSequencerEvent(sequencerTree, "startDump", EventDescription, repetition, offset);
  Int_t StartChannel = -1, StopChannel = -1;
  if (!seqEvent)
  {
    std::cout << "No sequencer event " << EventDescription << " found \n";
    StartTime = -1.;
    return;
  }
  if (!(seqEvent->GetSeq().CompareTo(TString("cat"))))
  {
    StartChannel = gSISch->GetChannel("SIS_PBAR_DUMP_START");
    StopChannel = gSISch->GetChannel("SIS_PBAR_DUMP_STOP");
  }
  else if (!(seqEvent->GetSeq().CompareTo(TString("rct"))))
  {
    StartChannel = gSISch->GetChannel("SIS_RECATCH_DUMP_START");
    StopChannel = gSISch->GetChannel("SIS_RECATCH_DUMP_STOP");
  }
  else if (!(seqEvent->GetSeq().CompareTo(TString("atm"))))
  {
    StartChannel = gSISch->GetChannel("SIS_ATOM_DUMP_START");
    StopChannel = gSISch->GetChannel("SIS_ATOM_DUMP_STOP");
  }
  else if (!(seqEvent->GetSeq().CompareTo(TString("pos"))))
  {
    StartChannel = gSISch->GetChannel("SIS_POS_DUMP_START");
    StopChannel = gSISch->GetChannel("SIS_POS_DUMP_STOP");
  }
  else
  {
    Error("GetDumpTime", "Unknown Sequencer %s", seqEvent->GetSeq().Data());
    StartTime = -2.;
    return;
  }

  assert(StartChannel >= 0);
  assert(StopChannel >= 0);
  Int_t lastcount(0);
  //  std::cout<<" --- "<<EventDescription<<" --- "<<std::endl;
  Int_t ID = seqEvent->GetID() / 2;
  Int_t count = ID + 1;
  if (count > lastcount + 20)
    count = lastcount + 1; //hack for ion events not in sis28/29
  lastcount = count;
  TTree *startTree = GetSISTree(theFile, StartChannel);
  //  StartTime = GetRunTimeOfCount(startTree, count);
  StartTime = GetRunTimeOfEntry(startTree, ID);
  delete startTree;
  //  cout<<seqEvent->GetName()<<" RunTime\tby count: "<<StartTime<<"\tby entry: "<<endl;
  TTree *stopTree = GetSISTree(theFile, StopChannel);
  //  StopTime = GetRunTimeOfCount(stopTree, count);
  StopTime = GetRunTimeOfEntry(stopTree, ID);
  delete stopTree;
  //  cout<<seqEvent->GetName()<<" RunTime\tby count: "<<StopTime<<"\tby entry: "<<StopTimeEntry<<endl;
  //  cout<<"----------------------------------"<<endl;

  delete seqEvent;
  delete sequencerTree;
}

//*************************************************************
// Sequencer Functions
//*************************************************************

TString GetRWinRun(int runNumber, TString RW)
{
  TString RWList = "";
  char RWString[200];
  TTree *labVIEW_tree = Get_LabVIEW_Tree(runNumber);
  TLabVIEWEvent *labVIEW_event = new TLabVIEWEvent();
  labVIEW_tree->SetBranchAddress("LabVIEWEvent", &labVIEW_event);
  Double_t SweepNo = -1;
  Double_t StartF = -1;
  Double_t StopF = -1;
  Double_t Duration = -1;
  Double_t Voltage = -1;
  for (Int_t i = 0; i < labVIEW_tree->GetEntries(); i++)
  {
    labVIEW_tree->GetEntry(i);
    if (strncmp(labVIEW_event->GetBankName(), RW.Data(), 4) == 0)
    { // Loop over all RW entries...
      //cout << "Array: " <<labVIEW_event->GetArrayNumber() << endl;
      if (labVIEW_event->GetArrayNumber() == 1)
        SweepNo = labVIEW_event->GetValue();
      else if (labVIEW_event->GetArrayNumber() == 2)
        StartF = labVIEW_event->GetValue() / 1e3;
      else if (labVIEW_event->GetArrayNumber() == 3)
        StopF = labVIEW_event->GetValue() / 1e3;
      else if (labVIEW_event->GetArrayNumber() == 4)
        Duration = labVIEW_event->GetValue();
      else if (labVIEW_event->GetArrayNumber() == 6)
      {
        Voltage = labVIEW_event->GetValue();
        sprintf(RWString, "Sweep:%.0f   %5.1f -- %5.1f kHz  %2.2f s  %.1f V", SweepNo, StartF, StopF, Duration, Voltage);
        //cout << RWString << endl;
        RWList += RWString;
      }
    }
  }
  delete labVIEW_event;
  return RWList;
}

TString GetSequenceQueueFull(int runNumber, TString SequencerName)
{
  TString SequenceList = "";
  TTree *sequencerTree = GetSequencerTree(runNumber);
  TSeq_Event *seqEvent = new TSeq_Event();
  sequencerTree->SetBranchAddress("SequenceEvent", &seqEvent);
  Int_t LastPrinted = -1;
  Int_t QueueEnd = -1;
  for (Int_t i = 0; i < sequencerTree->GetEntries(); i++)
  {
    sequencerTree->GetEntry(i);
    if (LastPrinted == seqEvent->GetSeqNum())
      continue;
    if (strcmp(SequencerName.Data(), seqEvent->GetSeq().Data()) == 0)
    {
      //cout<<"Sequencer Name "<<seqEvent->GetSeq().Data()
      //<<"\t Number "<<seqEvent->GetSeqNum()
      //<<"\t ID "<<seqEvent->GetID()
      ////<<"\t Event Header \n\n\n "<<seqEvent->GetSeqHeader()
      //<<endl;

      TString SeqHeader = seqEvent->GetSeqHeader();

      TObjArray *SequenceLines = SeqHeader.Tokenize("\n");
      TString QueueName = ".";
      QueueName += SequencerName;
      QueueName += ";";
      TString QueueLine = "";
      for (Int_t j = 0; j < SequenceLines->GetEntries(); j++)
      {
        //cout <<"LINE "<<j<<":"<<SequenceLines->At(j)->GetString();
        TString a = ((TObjString *)SequenceLines->At(j))->GetString();
        if (!a.Contains(QueueName))
          continue;
        if (!a.Contains("]; Flag"))
          continue; //Must have a flag...
        QueueLine = a;
        //cout <<((TObjString*)SequenceLines->At(j))->GetString()<<endl;
      }
      SequenceLines->SetOwner(kTRUE);
      SequenceLines->Delete();
      delete SequenceLines;
      QueueEnd = QueueLine.Index("]; Flag");
      QueueLine.Remove(QueueEnd + 2); //remove everything after matching queue name
      SequenceList += QueueLine;
      SequenceList += "\n";
      LastPrinted = seqEvent->GetSeqNum();
    }
  }
  delete seqEvent;
  delete sequencerTree;
  return SequenceList;
}

TString GetSequenceQueue(int runNumber, TString SequencerName)
{
  TTree *sequencerTree = GetSequencerTree(runNumber);
  TSeq_Event *seqEvent = new TSeq_Event();
  sequencerTree->SetBranchAddress("SequenceEvent", &seqEvent);
  Int_t LastPrinted = -1;
  for (Int_t i = 0; i < sequencerTree->GetEntries(); i++)
  {
    sequencerTree->GetEntry(i);
    if (LastPrinted == seqEvent->GetSeqNum())
      continue;
    if (strcmp(SequencerName.Data(), seqEvent->GetSeq().Data()) == 0)
    {

      cout << "Sequencer Name " << seqEvent->GetSeq().Data()
           << "\t Number " << seqEvent->GetSeqNum()
           << "\t ID " << seqEvent->GetID()
           //<<"\t Event Header \n\n\n "<<seqEvent->GetSeqHeader()
           << endl;

      TString SeqHeader = seqEvent->GetSeqHeader();
      //SeqHeader.Remove[150]; // Remove all but first 150 characters (Handy for debugging)

      TString QueueName = ".";
      QueueName += SequencerName;
      QueueName += ".queue";

      SeqHeader.Remove(SeqHeader.Index(QueueName) + 10); //remove everything after matching queue name
      SeqHeader.Remove(0, 34);                           //Remove additional junk before Sequence queue
      cout << SeqHeader << endl;
      TString ReturnString(SeqHeader);
      LastPrinted = seqEvent->GetSeqNum();
      delete seqEvent;
      delete sequencerTree;
      return ReturnString;
    }
  }
  delete seqEvent;
  delete sequencerTree;
  return "";
}
void PrintSequences(int runNumber, TString SequencerName)
{
  TTree *sequencerTree = GetSequencerTree(runNumber);
  TSeq_Event *seqEvent = new TSeq_Event();
  sequencerTree->SetBranchAddress("SequenceEvent", &seqEvent);
  Int_t LastPrinted = -1;
  for (Int_t i = 0; i < sequencerTree->GetEntries(); i++)
  {
    sequencerTree->GetEntry(i);
    if (LastPrinted == seqEvent->GetSeqNum())
      continue;
    if (strcmp(SequencerName, seqEvent->GetSeq().Data()) == 0)
    {

      cout << "Sequencer Name " << seqEvent->GetSeq().Data()
           << "\t Number " << seqEvent->GetSeqNum()
           << "\t ID " << seqEvent->GetID()
           << "\t Event Header \n\n\n " << seqEvent->GetSeqHeader()
           << endl;
      LastPrinted = seqEvent->GetSeqNum();
    }
  }

  delete seqEvent;
  delete sequencerTree;
}

TString GetQWPDir(int runNumber)
{
  //Get last ATM sequence in queue, inspect flags for clues if:
  // QWP L (not flag R, flag W)
  // QWP R (flag R, flag W)
  // No QWP (not flag W)
  TString SequencerName = "atm";
  TTree *sequencerTree = GetSequencerTree(runNumber);
  TSeq_Event *seqEvent = new TSeq_Event();
  sequencerTree->SetBranchAddress("SequenceEvent", &seqEvent);
  Int_t LastPrinted = -1;
  TString Header = "";
  for (Int_t i = 0; i < sequencerTree->GetEntries(); i++)
  {
    sequencerTree->GetEntry(i);
    if (LastPrinted == seqEvent->GetSeqNum())
      continue;
    if (strcmp(SequencerName, seqEvent->GetSeq().Data()) == 0)
    {
      Header = seqEvent->GetSeqHeader();
      //cout<<"\t Event Header \n\n\n "<<seqEvent->GetSeqHeader()<<endl;
      LastPrinted = seqEvent->GetSeqNum();
    }
  }
  //Clean away all but the last atm sequence...
  for (Int_t i = 0; i < 10; i++)
  {
    if (Header.Index(".atm;", i) > 0)
      Header.Remove(0, Header.Index(".atm;") + 5);
    //cout << Header<<endl<<endl<<endl<<endl;
  }

  //cout<<"R On: "<<Header.Contains("Flag R On;")<<endl;
  //cout<<"R Off:"<<Header.Contains("Flag R Off;")<<endl;
  //cout<<"W On: "<<Header.Contains("Flag W On;")<<endl;
  //cout<<"W Off:"<<Header.Contains("Flag W Off;")<<endl;
  TString QWPDir="N/A";
  if (Header.Contains("Flag W"))
  {
    if (Header.Contains("Flag R Off") && Header.Contains("Flag W On"))
      QWPDir="L";
    else if (Header.Contains("Flag R On") && Header.Contains("Flag W On"))
      QWPDir="R";
    else if (Header.Contains("Flag W Off"))
      QWPDir="No QWP";
  }
  else
  {
    if (Header.Contains("Flag R Off"))
      QWPDir="L";
    else if (Header.Contains("Flag R On"))
      QWPDir="R";
  }

  delete seqEvent;
  delete sequencerTree;
  return QWPDir;
}

long int GetTrappingHash(Int_t runNumber)
{
  TString QWPDir = GetQWPDir(runNumber);
  if (QWPDir.EqualTo("L") || QWPDir.EqualTo("R") || QWPDir.EqualTo("No QWP"))
  {
    //cout <<"QWPDir"<<endl;

    //Get last ATM sequence in queue, inspect flags for clues if:
    // QWP L (not flag R, flag W)
    // QWP R (flag R, flag W)
    // No QWP (not flag W)
    TString SequencerName = "atm";
    TTree *sequencerTree = GetSequencerTree(runNumber);
    TSeq_Event *seqEvent = new TSeq_Event();
    sequencerTree->SetBranchAddress("SequenceEvent", &seqEvent);
    Int_t LastPrinted = -1;
    TString Header = "";
    for (Int_t i = 0; i < sequencerTree->GetEntries(); i++)
    {
      sequencerTree->GetEntry(i);
      if (LastPrinted == seqEvent->GetSeqNum())
        continue;
      if (strcmp(SequencerName, seqEvent->GetSeq().Data()) == 0)
      {
        Header = seqEvent->GetSeqHeader();
        //cout<<"\t Event Header \n\n\n "<<seqEvent->GetSeqHeader()<<endl;
        LastPrinted = seqEvent->GetSeqNum();
      }
    }
    //Clean away all but the last atm sequence...
    for (Int_t i = 0; i < 10; i++)
    {
      if (Header.Index(".atm;", i) > 0)
        Header.Remove(0, Header.Index(".atm;") + 5);
      //cout << Header<<endl<<endl<<endl<<endl;
    }

    //cout<<"R On: "<<Header.Contains("Flag R On;")<<endl;
    //cout<<"R Off:"<<Header.Contains("Flag R Off;")<<endl;
    //cout<<"W On: "<<Header.Contains("Flag W On;")<<endl;
    //cout<<"W Off:"<<Header.Contains("Flag W Off;")<<endl;
    TString NewHeader(Header);
    TString OldHEX = CalculateSequenceFlagHex(Header);
    CalculateSequenceFlagHex(Header);
    NewHeader.ReplaceAll("Flag R On;", "Flag R irrelevant");
    NewHeader.ReplaceAll("Flag R Off;", "Flag R irrelevant");
    NewHeader.ReplaceAll("Flag W On;", "Flag W irrelevant");
    NewHeader.ReplaceAll("Flag W Off;", "Flag W irrelevant");
    TString NewHEX = CalculateSequenceFlagHex(NewHeader);
    TString ATM = GetSequenceQueueFull(runNumber, "atm");
    TString RCT = GetSequenceQueueFull(runNumber, "rct");
    TString CAT = GetSequenceQueueFull(runNumber, "cat");
    TString POS = GetSequenceQueueFull(runNumber, "pos");
    TString result = POS + ATM + RCT + CAT;
    //cout << result <<endl<<endl<<endl<<endl<<endl;
    result.ReplaceAll(Header.Data(), NewHeader.Data());
    result.ReplaceAll(OldHEX.Data(), NewHEX.Data());
    //cout << result <<endl<<endl<<endl<<endl<<endl;

    delete seqEvent;
    return result.Hash();
  }
  else
  {
    cout << "I cannot work out the QWP direction in run " << runNumber << endl;
    cout << "Are you sure its a trapping run?" << endl;
  }
  return 0;
}
TString CalculateSequenceFlagHex(TString SequenceString)
{
  char *LetterOrder = (char *)"QRSTUVWXYZ234567ABCDEFGHIJKLMNOP"; //MSB to LSB
  TString Binary = "";
  Int_t HEX = 0;
  for (int i = 0; i < 32; i++)
  {
    TString Flag = "Flag ";
    Flag += LetterOrder[i];
    Flag += " On";
    //cout <<Flag;
    if (SequenceString.Contains(Flag))
    {
      HEX += pow(2, (31 - i));
    }
  }
  char a[8];
  sprintf(a, "%08X", HEX);
  TString b(a);
  return b;
}

TString GetSequencerOnlyString(Int_t runNumber) //cat all sequencers and RW's
{
  TString POS = GetSequenceQueueFull(runNumber, "pos");
  TString ATM = GetSequenceQueueFull(runNumber, "atm");
  TString RCT = GetSequenceQueueFull(runNumber, "rct");
  TString CAT = GetSequenceQueueFull(runNumber, "cat");
  TString result = POS + ATM + RCT + CAT;
  return result;
}
long int GetSequencerOnlyStringHash(Int_t runNumber)
{
  return GetSequencerOnlyString(runNumber).Hash();
}
TString GetRWOnlyString(Int_t runNumber) //cat all sequencers and RW's
{
  TString CTRW1 = GetRWinRun(runNumber, "CRW1");
  TString CTRW2 = GetRWinRun(runNumber, "CRW2");
  TString ATRW1 = GetRWinRun(runNumber, "ARW1");
  TString ATRW2 = GetRWinRun(runNumber, "ARW2");
  TString result = CTRW1 + CTRW2 + ATRW1 + ATRW2;
  return result;
}
long int GetRWOnlyStringHash(Int_t runNumber)
{
  return GetRWOnlyString(runNumber).Hash();
}

TString GetSequenceString(Int_t runNumber) //cat all sequencers and RW's
{
  TString POS = GetSequenceQueueFull(runNumber, "pos");
  TString ATM = GetSequenceQueueFull(runNumber, "atm");
  TString RCT = GetSequenceQueueFull(runNumber, "rct");
  TString CAT = GetSequenceQueueFull(runNumber, "cat");

  TString CTRW1 = GetRWinRun(runNumber, "CRW1");
  TString CTRW2 = GetRWinRun(runNumber, "CRW2");
  TString ATRW1 = GetRWinRun(runNumber, "ARW1");
  TString ATRW2 = GetRWinRun(runNumber, "ARW2");
  TString result = POS + ATM + RCT + CAT + CTRW1 + CTRW2 + ATRW1 + ATRW2;
  return result;
}
long int GetSequenceStringHash(Int_t runNumber)
{
  return GetSequenceString(runNumber).Hash();
}

Double_t GetClockCalib(TFile *theFile)
{
  Int_t VF48clkchan = gSISch->GetChannel("SIS_VF48_CLOCK");
  TTree *tree1 = GetSISTree(theFile, VF48clkchan);

  // This forces use of the same sis as the vf48 for the clock protecting agains missing sis buffers
  TTree *tree2 = GetSISTree(theFile, gSISch->Get10MHz_clk(VF48clkchan / 32));
  Double_t clockdiff = GetClockCalib(tree1, tree2);
  delete tree1;
  delete tree2;
  return clockdiff;
}
Double_t GetClockCalib(TTree *tree1, TTree *tree2)
{
  Int_t NEntries = tree1->GetEntries();
  if (tree2->GetEntries() < NEntries)
  {
    NEntries = tree2->GetEntries();
    std::cout << "Warning: VF48clock and sisclock entries don't match" << std::endl;
  }
  TSisEvent *SISevent1 = new TSisEvent();
  tree1->SetBranchAddress("SisEvent", &SISevent1);
  TSisEvent *SISevent2 = new TSisEvent();
  tree2->SetBranchAddress("SisEvent", &SISevent2);

  Long_t countdiff(0);
  //  Double_t count1(0);
  for (Int_t i = 0; i < NEntries; ++i)
  {
    tree1->GetEntry(i);
    countdiff += SISevent1->GetCountsInChannel();
    //     count1+=SISevent1->GetCountsInChannel();
    SISevent1->ClearSisEvent();
    //      if(i<20)     std::cout<<"Count1 "<<count1<<" c1 "<<countdiff;
    tree2->GetEntry(i);
    countdiff -= SISevent2->GetCountsInChannel() * 2;
    //      if(i<20) std::cout<<" c2 "<<countdiff<< " Ratio " << double(countdiff)/count1<<" count1: "<<count1 <<" runtime*1.e7" << SISevent2->GetRunTime()*1.e7<<std::endl;
    SISevent2->ClearSisEvent();
  }
  tree2->GetEntry(NEntries - 1);
  Double_t run_time(-99.);
  run_time = SISevent2->GetRunTime();
  //  std::cout<<"count1 "<<count1<<"Countdiff "<<countdiff<<" runtime "<<run_time<<std::endl;
  delete SISevent1;
  delete SISevent2;
  return countdiff / (run_time * 2.e7);
}

Double_t FindSISQuenchFlagTime(Int_t runNumber, Int_t repetition, Bool_t FailOnError)
{
  Double_t sis_quench_trigger_time;
  TSISChannels *sisch = new TSISChannels(runNumber);
  Int_t quench_channel = sisch->GetChannel("QUENCH_FLAG");

  TTree *sis_quench_trigger_tree = Get_Sis_Tree(runNumber, quench_channel);
  if (sis_quench_trigger_tree == NULL)
    sis_quench_trigger_time = -1;

  Bool_t AllowQuenchDumpRecovery = FailOnError;

  if (sis_quench_trigger_tree->GetEntries() < 1)
  {
    cout << "ERROR, no quench SIS pulse found!" << endl;
    sis_quench_trigger_time = MatchEventToTime(runNumber, "startDump", "Quench dump");
    if (TestForEventInRun(runNumber, "startDump", "Quench dump"))
    {
      sis_quench_trigger_time = MatchEventToTime(runNumber, "startDump", "Quench dump");
    }
    if (TestForEventInRun(runNumber, "startDump", "FastRampDown"))
    {
      sis_quench_trigger_time = MatchEventToTime(runNumber, "startDump", "FastRampDown");
    }

    if (TestForEventInRun(runNumber, "startDump", "Ramp Down Dump")) //Used in bad sequences
    {
      sis_quench_trigger_time = MatchEventToTime(runNumber, "startDump", "Ramp Down Dump");
    }
    if (AllowQuenchDumpRecovery == kFALSE)
      sis_quench_trigger_time = -1;
  }
  else
  {

    sis_quench_trigger_time = Get_RunTime_of_Count(sis_quench_trigger_tree, repetition);
  }
  delete sisch;
  return sis_quench_trigger_time;
}
Int_t CountsMixingTriggers(Int_t runNumber)
{
  TSISChannels *sisch = new TSISChannels(runNumber);
  Int_t mixing_channel = sisch->GetChannel("MIXING_FLAG");
  delete sisch;
  return Count_SIS_Triggers(runNumber,mixing_channel);
}
Double_t FindSISMixingFlagTime(Int_t runNumber, Int_t repetition)
{
  Double_t sis_mixing_trigger_time;
  TSISChannels *sisch = new TSISChannels(runNumber);
  Int_t mixing_channel = sisch->GetChannel("MIXING_FLAG");

  TTree *sis_mixing_trigger_tree = Get_Sis_Tree(runNumber, mixing_channel);
  if (sis_mixing_trigger_tree == NULL)
    sis_mixing_trigger_time = -1;

  Bool_t AllowQuenchDumpRecovery = kTRUE;

  if ((sis_mixing_trigger_tree->GetEntries() < 1) && AllowQuenchDumpRecovery)
  {
    cout << "ERROR, no quench SIS pulse found!" << endl;
    sis_mixing_trigger_time = MatchEventToTime(runNumber, "startDump", "MIXING");
    //	  sis_quench_trigger_time= -1;
  }
  else
  {
    sis_mixing_trigger_time = Get_RunTime_of_Count(sis_mixing_trigger_tree, repetition);
  }
  delete sisch;
  return sis_mixing_trigger_time;
}

Double_t FindMixingGate(TFile *theFile, Double_t &StartTime, Double_t &StopTime)
{
  TTree *sequencerTree = GetSequencerTree(theFile);
  TSeq_Event *StopDump = FindSequencerEvent(sequencerTree, "stopDump", "Mixing");
  assert(StopDump);

  Int_t START_DUMP_CHANNEL = 0, STOP_DUMP_CHANNEL = 0;
  if (!(StopDump->GetSeq().CompareTo(TString("atm"))))
  {
    START_DUMP_CHANNEL = gSISch->GetChannel("SIS_ATOM_DUMP_START");
    STOP_DUMP_CHANNEL = gSISch->GetChannel("SIS_ATOM_DUMP_STOP");
  }
  assert(START_DUMP_CHANNEL);
  assert(STOP_DUMP_CHANNEL);

  Int_t MixingTrigCh = 0;
  MixingTrigCh = gSISch->GetChannel("MIXING_FLAG");
  assert(MixingTrigCh);

  TTree *MIXING_GATE_tree = GetSISTree(theFile, MixingTrigCh);
  Double_t timeOfMixingStart = GetRunTimeOfCount(MIXING_GATE_tree, 1);

  if (timeOfMixingStart < 0.)
  {
    std::cout << "WARNING: NO MIXING TRIGGER... USING DUMP TIMING" << std::endl;
    Double_t dummy_stop;
    GetDumpTime(theFile, (char *)"Mixing", timeOfMixingStart, dummy_stop);
  }
  assert(timeOfMixingStart > 0.);
  delete MIXING_GATE_tree;
  std::cout << "MIXING TS: " << timeOfMixingStart << " s" << std::endl;

  StartTime = timeOfMixingStart;

  TTree *stop_dump_tree = GetSISTree(theFile, STOP_DUMP_CHANNEL);
  StopTime = GetRunTimeOfCount(stop_dump_tree, 1, timeOfMixingStart);
  assert(StopTime >= StartTime);
  delete stop_dump_tree;
  delete StopDump;
  return timeOfMixingStart;
}

Double_t FindQuenchGate(TFile *theFile, Double_t &StartTime, Double_t &StopTime)
{
  TTree *sequencerTree = GetSequencerTree(theFile);
  TSeq_Event *StopDump = FindSequencerEvent(sequencerTree, "stopDump", "Quench");
  Int_t START_DUMP_CHANNEL = 0, STOP_DUMP_CHANNEL = 0;

  //  assert(StopDump);
  if (StopDump)
  {
    if (!(StopDump->GetSeq().CompareTo(TString("atm"))))
    {
      START_DUMP_CHANNEL = gSISch->GetChannel("SIS_ATOM_DUMP_START");
      STOP_DUMP_CHANNEL = gSISch->GetChannel("SIS_ATOM_DUMP_STOP");
    }
    assert(START_DUMP_CHANNEL);
    assert(STOP_DUMP_CHANNEL);
  }
  Int_t QuenchTrigCh = 0;
  QuenchTrigCh = gSISch->GetChannel("QUENCH_FLAG");
  assert(QuenchTrigCh);

  std::cout << "QUENCH: " << QuenchTrigCh << std::endl;
  //  std::cout<<"START DUMP: "<<START_DUMP_CHANNEL<<std::endl;
  //  std::cout<<"STOP DUMP: "<<STOP_DUMP_CHANNEL<<std::endl;

  TTree *QUENCH_GATE_tree = GetSISTree(theFile, QuenchTrigCh);
  Double_t timeOfQuenchStart = GetRunTimeOfCount(QUENCH_GATE_tree, 1);
  assert(timeOfQuenchStart > 0.);
  delete QUENCH_GATE_tree;
  std::cout << "QUENCH TS: " << timeOfQuenchStart << " s" << std::endl;

  // TTree* start_dump_tree = GetSISTree(theFile,START_DUMP_CHANNEL);
  // StartTime = GetRunTimeOfCount(start_dump_tree, 1, timeOfQuenchStart-(1.e-3));
  // assert(StartTime>0.);
  // delete start_dump_tree;
  StartTime = timeOfQuenchStart;
  if (StopDump)
  {
    TTree *stop_dump_tree = GetSISTree(theFile, STOP_DUMP_CHANNEL);
    StopTime = GetRunTimeOfCount(stop_dump_tree, 1, timeOfQuenchStart);
    assert(StopTime >= StartTime);
    delete stop_dump_tree;
  }
  else
    StopTime = StartTime + 2.1; // Default FRD if no dump info
  delete sequencerTree;
  delete StopDump;
  return timeOfQuenchStart;
}

//*************************************************************
// AnalysisReport utilities
//*************************************************************
void PrintProcessingTime(Int_t runNumber)
{
  cout << "AlphaAnalysis took " << GetProcessingTime(runNumber) << "s (CPU time) to run on " << GetAnalysisReport(runNumber)->GetHostname() << endl;
}

Double_t GetProcessingTime(Int_t runNumber)
{
  TAnalysisReport *AnalysisReport = GetAnalysisReport(runNumber);
  return AnalysisReport->GetCPUtime();
}

TString GetlibSVNVersion(Int_t runNumber)
{
  TAnalysisReport *gAnalysisReport = GetAnalysisReport(runNumber);
  return gAnalysisReport->GetSVNlibRevision();
}

TString GetalphaAnalysisSVNVersion(Int_t runNumber)
{
  TAnalysisReport *gAnalysisReport = GetAnalysisReport(runNumber);
  return gAnalysisReport->GetSVNalphaAnalysisRevision();
}
TString GetvmcSVNVersion(Int_t runNumber)
{
  TAnalysisReport *gAnalysisReport = GetAnalysisReport(runNumber);
  return gAnalysisReport->GetSVNvmcRevision();
}

TString GetSVNVersion(Int_t runNumber)
{
  std::cout << " GetSVNVersion(Int_t runNumber) depreciated, please use GetlibSVNVersion,GetalphaAnalysisSVNVersion or GetvmcSVNVersion" << std::endl;
  TAnalysisReport *gAnalysisReport = GetAnalysisReport(runNumber);
  return gAnalysisReport->GetSVNalphaAnalysisRevision();
}

TAnalysisReport *GetAnalysisReport(Int_t runNumber)
{
  TTree *AnalysisReportTree = GetAnalysisReportTree(runNumber);
  TAnalysisReport *gAnalysisReport = new TAnalysisReport();
  AnalysisReportTree->SetBranchAddress("gAnalysisReport", &gAnalysisReport);
  AnalysisReportTree->GetEntry(0);
  return gAnalysisReport;
}
TAnalysisReport *GetAnalysisReport(TFile* root_file)
{
  TTree *AnalysisReportTree = GetAnalysisReportTree(root_file);
  TAnalysisReport *gAnalysisReport = new TAnalysisReport();
  AnalysisReportTree->SetBranchAddress("gAnalysisReport", &gAnalysisReport);
  AnalysisReportTree->GetEntry(0);
  return gAnalysisReport;
}


TTree *GetAnalysisReportTree(TFile* root_file)
{
  TTree *AnalysisReportTree = (TTree *)root_file->Get("gAnalysisReport");
  if (AnalysisReportTree == NULL)
  {
    Error("GetAnalysisReportTree", "\033[31mAnalysisReport tree not found\033[00m");
    //gSystem->Exit(1);
    AnalysisReportTree->GetName(); // This is to crash the CINT interface  instead of exiting (deliberately)
  }

  return AnalysisReportTree;

}

TTree *GetAnalysisReportTree(Int_t runNumber)
{
  TFile *root_file = Get_File(runNumber);
  TTree *AnalysisReportTree = (TTree *)root_file->Get("gAnalysisReport");
  if (AnalysisReportTree == NULL)
  {
    Error("GetAnalysisReportTree", "\033[31mAnalysisReport tree for run number %d not found\033[00m", runNumber);
    //gSystem->Exit(1);
    AnalysisReportTree->GetName(); // This is to crash the CINT interface  instead of exiting (deliberately)
  }

  return AnalysisReportTree;
}



TTree *GetTimestampHelperTree(Int_t runNumber)
{
  TFile *root_file = Get_File(runNumber);
  TTree *TimestampHelperTree = (TTree *)root_file->Get("TimestampHelper");
  //if (TimestampHelperTree == NULL)
  //{
  //  Error("GetAnalysisReportTree", "\033[31mAnalysisReport tree for run number %d not found\033[00m", runNumber);
  //gSystem->Exit(1);
  //  AnalysisReportTree->GetName(); // This is to crash the CINT interface  instead of exiting (deliberately)
  //}
  return TimestampHelperTree;
}

TTimestampHelper *GetTimestampHelper(Int_t runNumber)
{
  if (TimeStampHelperBuffer)  //Don't read from file if buffered
  {
    if (TimeStampHelperBuffer->GetRunNumber()==runNumber   //If buffer helper is from right run
            && TimeStampHelperBufferComplete)              //If buffer helper is complete, or just has some channels
      return TimeStampHelperBuffer;
    else delete TimeStampHelperBuffer;
  }
  
  TTree *TimestampHelperTree = GetTimestampHelperTree(runNumber);
  if (!TimestampHelperTree) return NULL;
  TimeStampHelperBufferComplete=kTRUE;
  TimeStampHelperBuffer = new TTimestampHelper();
  TimeStampHelperBuffer->SetRunNumber(runNumber);
  TTimestampHelper* TimeStampHelperTmp=new TTimestampHelper();
  TimestampHelperTree->SetBranchAddress("TimestampHelper", &TimeStampHelperTmp);
  //TimeStampHelperBuffer->Print();
  for (Int_t i=0; i<TimestampHelperTree->GetEntries(); i++)
  {
    TimestampHelperTree->GetEntry(i);
    TimeStampHelperBuffer->AddAtEnd(TimeStampHelperTmp);
    //TimeStampHelperBuffer->Print();
  }   
  return TimeStampHelperBuffer;
}
TTimestampHelper *GetTimestampHelper(Int_t runNumber,Int_t channel)
{
  if (TimeStampHelperBuffer)  //Don't read from file if buffered
  {
    if (TimeStampHelperBuffer->GetRunNumber()==runNumber) // Has correct run number
    {
      if (channel == -1 )
      {
        if (TimeStampHelperBuffer->GetVF48EventCapacity()>0 || TimeStampHelperBufferComplete)
          return TimeStampHelperBuffer;
      }
      else
      {
        if (TimeStampHelperBuffer->GetSISEventCapacity(channel)>0 || TimeStampHelperBufferComplete)
          return TimeStampHelperBuffer;
      }
    }
    else 
    {
      delete TimeStampHelperBuffer;
    }
  }
  //std::cout <<"Caching channel: "<<channel<<std::endl;
  TimeStampHelperBufferComplete=kFALSE;
  TTree *TimestampHelperTree = GetTimestampHelperTree(runNumber);
  if (!TimestampHelperTree) return NULL;
  if (!TimeStampHelperBuffer) TimeStampHelperBuffer = new TTimestampHelper();
  TimeStampHelperBuffer->SetRunNumber(runNumber);
  TTimestampHelper* TimeStampHelperTmp=new TTimestampHelper();
  TimestampHelperTree->SetBranchAddress("TimestampHelper", &TimeStampHelperTmp);
  for (Int_t i=0; i<TimestampHelperTree->GetEntries(); i++)
  {
    TimestampHelperTree->GetEntry(i);
    TimeStampHelperTmp->ClearSISVectorsExcept(channel);
    TimeStampHelperBuffer->AddAtEnd(TimeStampHelperTmp);
  }
  
  //If the channel really has no counts... make the vector a non-zero 
  //length so it does not attempt to refil it.
  if (TimeStampHelperBuffer->GetSISEvents(channel)==0)
  {
    //std::cout <<"Reseving spot"<<std::endl;
    TimeStampHelperBuffer->ReserveSISEvents(1,channel);
  }
  return TimeStampHelperBuffer;
}

TTree *GetSeqEventHelperTree(Int_t runNumber)
{
  TFile *root_file = Get_File(runNumber);
  TTree *TimestampHelperTree = (TTree *)root_file->Get("SeqEventHelper");
  //if (TimestampHelperTree == NULL)
  //{
  //  Error("GetAnalysisReportTree", "\033[31mAnalysisReport tree for run number %d not found\033[00m", runNumber);
  //gSystem->Exit(1);
  //  AnalysisReportTree->GetName(); // This is to crash the CINT interface  instead of exiting (deliberately)
  //}
  return TimestampHelperTree;
}

TSeq_EventHelper *GetSeqEventHelper(Int_t runNumber)
{
  if (SeqEventHelperBuffer)  //Don't read from file if buffered
  {
    if (SeqEventHelperBuffer->GetRunNumber()==runNumber) return SeqEventHelperBuffer;
    else delete SeqEventHelperBuffer;
  }
  
  TTree *SeqEventHelperTree = GetSeqEventHelperTree(runNumber);
  if (!SeqEventHelperTree) return NULL;
  SeqEventHelperBuffer = new TSeq_EventHelper();
  SeqEventHelperBuffer->SetRunNumber(runNumber);
  TSeq_EventHelper* SeqEventHelperTmp=new TSeq_EventHelper();
  SeqEventHelperTree->SetBranchAddress("SeqEventHelper", &SeqEventHelperTmp);
  //SeqEventHelperBuffer->Print();
  SeqEventHelperTree->GetEntry(0);
  SeqEventHelperBuffer=SeqEventHelperTmp;
  return SeqEventHelperBuffer;
}

Int_t GetFirstVF48EventBeforeTime(Int_t runNumber, Double_t t)
{
  TTimestampHelper* ts=GetTimestampHelper(runNumber,-1);
  if (ts) return ts->GetFirstVF48EventBefore(t);
  else return 0;
}
Int_t GetFirstSISEventBeforeTime(Int_t runNumber, Int_t SISch, Double_t t)
{
  TTimestampHelper* ts=GetTimestampHelper(runNumber,SISch);
  if (ts) return ts->GetFirstSISEventBefore(SISch,t);
  else return 0;
}

TTree *GetStripReportTree(Int_t runNumber)
{
  TFile *root_file = Get_File(runNumber);
  TTree *StripReportTree = (TTree *)root_file->Get("gStripReport");
  if (StripReportTree == NULL)
  {
    Error("GetStripReportTree", "\033[31mStripReport tree for run number %d not found\033[00m", runNumber);
    //gSystem->Exit(1);
    StripReportTree->GetName(); // This is to crash the CINT interface  instead of exiting (deliberately)
  }

  return StripReportTree;
}

TalphaStripsReport *GetStripReport(Int_t runNumber)
{
  TTree *StripReportTree = GetStripReportTree(runNumber);
  TalphaStripsReport *gStripReport = new TalphaStripsReport();
  StripReportTree->SetBranchAddress("gStripReport", &gStripReport);
  StripReportTree->GetEntry();
  return gStripReport;
}

void PrintStripRMS(Int_t runNumber)
{
  TFile *s = Get_Strip_File(runNumber);
  TTree *MeanalphaTree = (TTree *)s->Get("alphaStrip Tree");
  TTree *RMSalphaTree = (TTree *)s->Get("alphaStrip Tree");
  Float_t MeanalphaValue, RMSalphaValue;
  MeanalphaTree->SetBranchAddress("stripMean", &MeanalphaValue);
  RMSalphaTree->SetBranchAddress("stripRMS", &RMSalphaValue);
  //const Int_t a = 100000;
  //const Int_t n = MeanalphaTree->GetEntries();
  //Float_t x[a], y[a];
  // Specific module
  //for(int i=0+moduleNumber*512+ASICNumber*128; i<128+moduleNumber*512+ASICNumber*128; i++)
  std::cout << "StripNumber\tStripMean\tStripRMS" << std::endl;
  for (int i = 0; i < 128 * 72 * 4; i++)
  {
    {
      MeanalphaTree->GetEvent(i);
      std::cout << i;
      std::cout << "\t" << MeanalphaValue;
      RMSalphaTree->GetEvent(i);
      std::cout << "\t" << RMSalphaValue << std::endl;
    }
  }
}

//*************************************************************
// LabView utilities
//*************************************************************

TTree *Get_LabVIEW_Tree(Int_t run_number)
{
  //load file
  TFile *root_file = Get_File(run_number);

  TTree *labview_tree = NULL;
  labview_tree = (TTree *)root_file->Get("gLabVIEWTree");
  if (labview_tree == NULL)
  {
    Error("Get_LabVIEW_Tree", "\033[31mLabview tree for run number %d not found\033[00m", run_number);
    //gSystem->Exit(1);
    labview_tree->GetName(); // This is to crash the CINT interface  instead of exiting (deliberately)
  }

  return labview_tree;
}

Double_t LabVIEW_at_SIS(Int_t runNumber, Int_t SIS_channel, Int_t repetition, TString Bank, Int_t Array_number, Int_t Interp_Option)
{

  if (repetition < 1) //allow for 0 or negative numbers to count backwards.
  {
    Int_t times = Count_SIS_Triggers(runNumber, SIS_channel);
    repetition += times;
  }

  Double_t trigger_time = Get_RunTime_of_SISChan(runNumber, SIS_channel, repetition);

  if (trigger_time == 999. || trigger_time <= 0.)
  {
    printf("!!! Could not Find SIS Trigger\n");
    return -1.;
  }

  return GetLabVIEWEvent(runNumber, trigger_time, Bank, Array_number, Interp_Option);
}

Double_t LabVIEW_at_dump(Int_t runNumber, const char *description, Int_t repetition, Int_t offset, TString Bank, Int_t Array_number, Int_t Interp_Option)
{
  Double_t trigger_time = MatchEventToTime(runNumber, "startDump", description, repetition, offset);
  return GetLabVIEWEvent(runNumber, trigger_time, Bank, Array_number, Interp_Option);
}

Double_t GetLabVIEWEvent(Int_t runNumber, Double_t runTime, TString Bank, Int_t Array_number, Int_t Interp_Option)
{
  Double_t before_val = -1;
  Double_t after_val = -1;
  Double_t before_time = -1;
  Double_t after_time = -1;
  Int_t found_before = 0;
  Int_t found_after = 0;

  TTree *labVIEW_tree = Get_LabVIEW_Tree(runNumber);
  TLabVIEWEvent *labVIEW_event = new TLabVIEWEvent();

  labVIEW_tree->SetBranchAddress("LabVIEWEvent", &labVIEW_event);

  //printf("..found %d LabVIEW entries\n", labVIEW_tree->GetEntries());

  for (Int_t i = 0; i < labVIEW_tree->GetEntries(); i++)
  {
    labVIEW_tree->GetEntry(i);
    if (labVIEW_event->GetArrayNumber() != Array_number)
      continue;
    if (strncmp(labVIEW_event->GetBankName(), Bank.Data(), 4) != 0)
      continue;

    if (labVIEW_event->GetTime() > runTime)
    {
      after_val = labVIEW_event->GetValue();
      after_time = labVIEW_event->GetTime();
      found_after = 1;
      break;
    }
    else
    {
      before_val = labVIEW_event->GetValue();
      before_time = labVIEW_event->GetTime();
      found_before = 1;
    }
  }
  delete labVIEW_event;
  labVIEW_tree->Delete();

  if (found_before != 1 && (Interp_Option == 0 || Interp_Option == -1))
  {
    printf("!!! Could not find a value BEFORE this trigger");
    return -1.;
  }
  if (found_after != 1 && (Interp_Option == 0 || Interp_Option == 1))
  {
    printf("!!! Could not find a value AFTER this trigger");
    return -1.;
  }

  switch (Interp_Option)
  {
  case -1: //before
    return before_val;
    break;
  case 1: //after
    return after_val;
    break;
  case 0: //linear interpolation
    return ((runTime - before_time) * before_val + (after_time - runTime) * after_val) / (after_time - before_time);
    break;
  case 2: //nearest
    if (found_before == 1 && found_after == 1)
    {
      if (runTime - before_time < after_time - runTime)
        return before_val;
      else
        return after_val;
      break;
    }
    else if (found_before == 1)
    {
      return before_val;
      break;
    }
    else if (found_after == 1)
    {
      return after_val;
      break;
    }
    else
    {
      printf("Could not find any values");
      return -1;
      break;
    }
  default:
    printf("!!! Invalid Option Value %d\n", Interp_Option);
    return -1.;
    break;
  }
}

Double_t GetLabVIEWEvent(TTree *labVIEW_tree, Double_t runTime, TString Bank, Int_t Array_number, Int_t Interp_Option)
{
  Double_t before_val = -1;
  Double_t after_val = -1;
  Double_t before_time = -1;
  Double_t after_time = -1;
  Int_t found_before = 0;
  Int_t found_after = 0;
  //TTree* labVIEW_tree = Get_LabVIEW_Tree(runNumber);
  TLabVIEWEvent *labVIEW_event = new TLabVIEWEvent();

  labVIEW_tree->SetBranchAddress("LabVIEWEvent", &labVIEW_event);

  //printf("..found %d LabVIEW entries\n", labVIEW_tree->GetEntries());

  for (Int_t i = 0; i < labVIEW_tree->GetEntries(); i++)
  {
    labVIEW_tree->GetEntry(i);

    if (labVIEW_event->GetArrayNumber() != Array_number)
      continue;
    if (strncmp(labVIEW_event->GetBankName(), Bank.Data(), 4) != 0)
      continue;
    if (labVIEW_event->GetTime() > runTime)
    {
      after_val = labVIEW_event->GetValue();
      after_time = labVIEW_event->GetTime();
      found_after = 1;
      break;
    }
    else
    {
      before_val = labVIEW_event->GetValue();
      before_time = labVIEW_event->GetTime();
      found_before = 1;
    }
  }
  delete labVIEW_event;
  if (found_before != 1 && (Interp_Option == 0 || Interp_Option == -1))
  {
    printf("!!! Could not find a value BEFORE this trigger");
    return -1.;
  }
  if (found_after != 1 && (Interp_Option == 0 || Interp_Option == 1))
  {
    printf("!!! Could not find a value AFTER this trigger");
    return -1.;
  }
  switch (Interp_Option)
  {
  case -1: //before
    return before_val;
    break;
  case 1: //after
    return after_val;
    break;
  case 0: //linear interpolation
    return ((runTime - before_time) * before_val + (after_time - runTime) * after_val) / (after_time - before_time);
    break;
  case 2: //nearest
    if (found_before == 1 && found_after == 1)
    {
      if (runTime - before_time < after_time - runTime)
        return before_val;
      else
        return after_val;
      break;
    }
    else if (found_before == 1)
    {
      return before_val;
      break;
    }
    else if (found_after == 1)
    {
      return after_val;
      break;
    }
    else
    {
      printf("Could not find any values");
      return -1;
      break;
    }
  default:
    printf("!!! Invalid Option Value %d\n", Interp_Option);
    return -1.;
    break;
  }
}

Int_t Extract_Bank_Array(Int_t run_number, TString bank, Int_t array_num, Double_t *time, Double_t *LVData, Double_t tmin, Double_t tmax)
{
  TTree *labVIEW_tree = new TTree();
  labVIEW_tree = Get_LabVIEW_Tree(run_number);
  TLabVIEWEvent *labVIEW_event = new TLabVIEWEvent();

  labVIEW_tree->SetBranchAddress("LabVIEWEvent", &labVIEW_event);
  if (tmax < 0)
    tmax = GetTotalRunTime(run_number);
  Int_t k = -1;

  for (Int_t i = 0; i < labVIEW_tree->GetEntries(); i++)
  {
    labVIEW_tree->GetEntry(i);
    if (labVIEW_event->GetBankName() != bank || labVIEW_event->GetArrayNumber() != array_num)
      continue;
    //  cout << labVIEW_event->GetTime() <<endl;
    if (labVIEW_event->GetTime() < tmin)
      continue;
    if (labVIEW_event->GetTime() > tmax)
      break;
    k++;
    time[k] = labVIEW_event->GetTime();
    LVData[k] = labVIEW_event->GetValue();
  }

  if (k > -1)
  {
    printf("Found %d events ", k + 1);
  }
  else
  {
    printf("No events found ");
  }

  printf("in %s[%d]\n", bank.Data(), array_num);

  delete labVIEW_tree;
  delete labVIEW_event;
  return k + 1;
}

//*************************************************************
// run file utilities
//*************************************************************

Int_t FileToRuns(const char *filename, Int_t *runNumber, Int_t *dumps, TObjArray *legendText)
{
  TString *TSfilename = new TString(filename);
  if (TSfilename->Contains(".filelist"))
  {
    Info("FileToRuns", "processing file %s as a filelist", filename);
    TObjArray *filelistnames = new TObjArray();
    Int_t nFiles = FileToSubFiles(filename, filelistnames);

    Int_t nTotalRuns(0), nRuns, i;
    for (i = 0; i < nFiles; i++)
    {
      nRuns = FileToRuns((char *)((TString *)filelistnames->At(i))->Data(), runNumber, dumps);

      nTotalRuns += nRuns;
      if (runNumber)
        runNumber = runNumber + nRuns;
      if (dumps)
        dumps = dumps + nRuns;
    }
    //CheckForDuplicates(nTotalRuns, runNumber-nTotalRuns);
    Info("FileToRuns", "Read %d runs from %s", i, filename);
    return nTotalRuns;
  }

  Int_t i(0), nRead;
  TString *label;

  FILE *myFile = fopen(filename, "r");
  if (myFile == NULL)
  {
    Error("PlotVertices", "\033[31mCould not open text file %s for reading\033[00m", filename);
    //gSystem->Exit(1);
    TObject *dum = NULL;
    dum->GetName(); // This is to crash the CINT interface  instead of exiting (deliberately)
  }

  char textLine[100];
  char text[100];

  Int_t run, dump;
  while (true)
  {
    run = 0;
    fgets(textLine, 100, myFile);
    nRead = sscanf(textLine, "%d %d %s", &run, &dump, text);
    if (runNumber && run)
      runNumber[i] = run;
    if (dumps)
      dumps[i] = dump;
    if (nRead < 3 && runNumber)
      sprintf(text, "%d", runNumber[i]);
    if (nRead < 2 && dumps)
      dumps[i] = 0;
    label = new TString(text);
    if (legendText)
      legendText->Add((TObject *)label);
    i++;
    if (feof(myFile))
      break;
  }
  i--;
  Info("FileToRuns", "Read %d runs from %s", i, filename);
  fclose(myFile);
  Int_t Total_Runs = CheckForDuplicates(i, runNumber);
  return Total_Runs;
}

Int_t FileToSubFiles(const char *filename, TObjArray *subFilenames, TObjArray *legendText)
{
  FILE *myFile = fopen(filename, "r");
  if (myFile == NULL)
  {
    Error("FileToSubFiles", "\033[31mCould not open file %s for reading\033[00m", filename);
    //gSystem->Exit(1);
    TObject *dum = NULL;
    dum->GetName(); // This is to crash the CINT interface  instead of exiting (deliberately)
  }
  char textLine[500];
  char thisFilename[500];
  char thisLabel[500];
  Int_t i(0), nRead;
  TString *myString;
  while (!feof(myFile))
  {
    fgets(textLine, 500, myFile);
    nRead = sscanf(textLine, "%s %s", thisFilename, thisLabel);
    myString = new TString(thisFilename);
    subFilenames->Add((TObject *)myString);
    if (legendText == NULL)
    {
    }
    else
    {
      if (nRead > 1)
        myString = new TString(thisLabel);
      legendText->Add((TObject *)myString);
    }
    i++;
  }
  i--;
  Info("FileToSubFiles", "Read %d files from %s", i, filename);
  fclose(myFile);
  return i;
}

Int_t CheckForDuplicates(Int_t nRuns, Int_t *runs)
{
  nRuns++;
  Int_t Total_Runs = nRuns;
  for (Int_t i = 0; i < Total_Runs; i++) //Remove blanks
  {
    //cout <<"Run "<< runs[i] <<endl;
    if (!(runs[i] > 20000 && runs[i] < 60000)) //not valid runs number range
    {
      Warning("CheckForEmptyLines", "\033[33mrun %d is invalid... removing\033[00m", runs[i]);
      for (Int_t k = i; k < nRuns; k++)
      {
        runs[k] = runs[k + 1];
      }
      nRuns--;
    }
  }
  //cout <<"Cleaned..." << nRuns << " runs remaining..." << endl;

  Total_Runs = nRuns;
  for (Int_t i = 0; i < Total_Runs; i++)
  {
    Int_t j = i + 1;
    while (j < Total_Runs)
    {
      if (runs[i] == runs[j] && i != j && i < nRuns && j < nRuns)
      {
        Warning("CheckForDuplicates", "\033[33mrun %d is duplicated... removing\033[00m", runs[i]);
        cout << "Line " << i << " and " << j << " the same (Total runs: " << nRuns << ")" << endl;
        for (Int_t k = j; k <= nRuns; k++)
        {
          runs[k] = runs[k + 1];
        }
        nRuns--;
      }
      else
      {
        j++;
      }
    }
  }

  //	cout << "De-duplicated run list:"<< endl;
  //   for(Int_t i=0; i<nRuns; i++)
  //    {
  //        cout << runs[i] << endl;
  //    }
  cout << "Total unique runs in list: " << nRuns << endl;
  return nRuns;
}

Bool_t TA_Mask_Run2010(Int_t TAnumber)
{
  Bool_t doMask = kFALSE;

  // TA banks that are not connected :
  // FPGA 0 bank 7 - TA inputs 0-15        == TAnumbers 112-127
  // FPGA 1 bank 7 - TA inputs 12,13,14,15 == TAnumbers 252,253,254,255

  if (TAnumber >= 112 && TAnumber <= 127)
    doMask = kTRUE;
  if (TAnumber >= 252 && TAnumber <= 255)
    doMask = kTRUE;

  return doMask;
}

Double_t Get_ASIC_Number(Int_t TAnumber)
{
  // ASIC number defined as;
  //  ASIC for p-side 1 = Module + 0.
  //  ASIC for p-side 2 = Module + 0.5

  Double_t ASIC_number(0);

  return ASIC_number;
}
}
