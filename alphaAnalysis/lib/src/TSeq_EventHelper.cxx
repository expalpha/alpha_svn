#include "TSeq_EventHelper.h"


TSeq_EventHelper::TSeq_EventHelper()
{
  SetRunNumber(0);
}

TSeq_EventHelper::TSeq_EventHelper(Int_t RunNo)
{
  SetRunNumber(RunNo);
}

TSeq_EventHelper::~TSeq_EventHelper()
{
  fSeq.clear();
  fSeqNum.clear();
  fID.clear();
  fEventName.clear();
  fDescription.clear();
  fonCount.clear();
  //std::vector<Int_t> fStartIntCount;
  RunTime.clear();
}

void TSeq_EventHelper::AddTSeq_Event(TSeq_Event* Event, Double_t Time)
{
    fSeq.push_back(Event->GetSeq());
    fSeqNum.push_back(Event->GetSeqNum());
    fID.push_back(Event->GetID());
    fEventName.push_back(Event->GetEventName());
    fDescription.push_back(Event->GetDescription());
    fonCount.push_back(Event->GetonCount());

    RunTime.push_back(Time);
}
Double_t TSeq_EventHelper::GetRunTimeOfDump(TSeq_Event* seqEvent, Int_t seqOffset)
{
  //TString Seq=Event->GetSeq();
  Int_t SeqNum=seqEvent->GetSeqNum();
  Int_t ID=seqEvent->GetID();
  //TString EventName=Event->GetEventName();
  //TString Description.push_back(Event->GetDescription());
  Int_t onCount=seqEvent->GetonCount();

  for (UInt_t i=0; i<fSeq.size(); i++)
  {
    //sequencerTree->GetEntry(i);
    if (SeqNum!=fSeqNum[i])
      continue; 
    if (ID!=fID[i])
      continue;
    if (onCount!=fonCount[i])
      continue;
    return RunTime[i];
  }
  return -2.;
}
Double_t TSeq_EventHelper::GetRunTimeOfDump( const char *eventName, const char *description, Int_t repetition, Int_t offset, bool exact_match)
{
  char search_string[80];
  sprintf(search_string, "\"%s", description); // add a " before the name
  Int_t rep(0);

  for (UInt_t i=0; i<fSeq.size(); i++)
  {
    //sequencerTree->GetEntry(i);
    if (!fDescription[i].CompareTo(TString(""), TString::kIgnoreCase))
      continue; //Old bugfix, now resolved elsewhere {offset++;  continue;} //If event has no description... its likely a laserpulse and has no dump timestamp...
    if (fEventName[i].CompareTo(TString(eventName), TString::kIgnoreCase))
      continue;
    //      if(seqEvent->GetDescription().CompareTo(TString(description), TString::kIgnoreCase)) continue;

    if (exact_match)
    {
      if ((fDescription[i].CompareTo(TString(search_string) + "\"", TString::kIgnoreCase)))
        continue;
    }
    else
    {
      if (!(fDescription[i].BeginsWith(TString(search_string), TString::kIgnoreCase)))
        continue;
    }

    ++rep;
    if (rep < repetition)
      continue;

    Info("FindSequencerEvent", "Found matching Cached Event #%d %s %s",
         i, fEventName[i].Data(), fDescription[i].Data());
    return RunTime[i];
  }
  return -1.;
}
//void TSeq_EventHelper::Print()
//{
//}

