#include "../include/TSiliconPedestals.h"
#include <stdlib.h>

// TSiliconPedestals Class =============================================================
//
// Class representing the Si detector pedestals
//
// ===============================================================================

ClassImp(TSiliconPedestals);

TSiliconPedestals::TSiliconPedestals( int _run_number )
{ 
  run_number = _run_number; 
  for(int i=0; i<nVF48*48; i++) ped_adc_rms[i]=-99;
}

TSiliconPedestals::~TSiliconPedestals()
{
}

void TSiliconPedestals::Set_Ped_Adc_Rms( int _strips_id, double _ped_adc_rms )
{ 
  assert( _strips_id < nVF48*48 );
  ped_adc_rms[ _strips_id ] = _ped_adc_rms;
}

double TSiliconPedestals::Get_Ped_Adc_Rms( int _strips_id ) 
{
  assert( _strips_id < nVF48*48 ); 
  return ped_adc_rms[ _strips_id ];
}

void TSiliconPedestals::Print()
{
  printf("... Pedestal Values ....\n");
  printf("From run: %d\n",run_number);
  for( int i = 0; i<nVF48*48; i++)
   {
    printf(" %lf ",ped_adc_rms[i]);
   }
  printf("\n");
}

int TSiliconPedestals::Write_Channel_Pedestal( int vf48modnum, int chan, TF1* myGauss )
{
  int strips_id = vf48modnum*48 + chan;  
  double sigma = myGauss->GetParameter(2);
  
  Set_Ped_Adc_Rms( strips_id, sigma );

  return 0;
}

int TSiliconPedestals::Write_Pedestal_Tree( )
{
  char fname[128];
  sprintf(fname,"%s/alphaAnalysis/pedestals/si_pedestals.root",getenv("RELEASE"));
  // load pedestal tree
  TFile* ped_file = new TFile(fname, "UPDATE");
  if( !ped_file ) return 1;

  TTree* ped_tree = NULL;
  ped_tree = (TTree*) ped_file->Get("ped_tree");

  Int_t run_number;
  Float_t ped_adc[nVF48*48];

  if(ped_tree==NULL)
    {
      //create tree
      printf("\n\n\n Creating a new ped_tree.............. \n\n\n");
      ped_tree = new TTree( "ped_tree", "ped_tree" );
      ped_tree->Branch( "run_number", &run_number, "run_number/I" );
      ped_tree->Branch( "ped_adc", &ped_adc, "ped_adc[nVF48*48]/F" );
    }

  ped_tree->SetBranchAddress("run_number",&run_number);
  ped_tree->SetBranchAddress("ped_adc",&ped_adc); 

  run_number = Get_Run_Number();
  for(int i=0; i<nVF48*48; i++ ) ped_adc[i] = Get_Ped_Adc_Rms( i );
  
  ped_tree->Fill(); 
  ped_tree->Write("",TObject::kOverwrite);
  ped_file->Close();

  return 0;
}

int TSiliconPedestals::Read_Pedestal_Tree( )
{
  char fname[128];
  sprintf(fname,"%s/alphaAnalysis/pedestals/si_pedestals.root",getenv("RELEASE"));
  // load pedestal tree
  TFile* ped_file = new TFile(fname, "UPDATE");
  if( !ped_file ) return 1;

  TTree* ped_tree = NULL;
  ped_tree = (TTree*) ped_file->Get("ped_tree");

  if( !ped_tree )
   {
    printf("No pedestal tree! Using defaults\n");
    Set_Run_Number( -99 );
    for(int i=0; i<nVF48*48; i++ ) Set_Ped_Adc_Rms( i, -99. );
    return 2;
   }

  Int_t run_number;
  Float_t ped_adc[nVF48*48];

  ped_tree->SetBranchAddress("run_number",&run_number);
  ped_tree->SetBranchAddress("ped_adc",&ped_adc); 

  ped_tree->GetEntry( ped_tree->GetEntries() - 1 );
 
  Set_Run_Number( run_number );
  for(int i=0; i<nVF48*48; i++ ) Set_Ped_Adc_Rms( i, ped_adc[i] );
  
  ped_file->Close();

  return 0;
}

int TSiliconPedestals::Read_Pedestal_Tree( char * fname )
{
  // load pedestal tree
  TFile* ped_file = new TFile(fname, "OPEN");
  if( !ped_file ) return 1;

  printf("Using pedestal file: %s\n", fname);

  TTree* ped_tree = NULL;
  ped_tree = (TTree*) ped_file->Get("ped_tree");

  if( !ped_tree )
   {
    printf("No pedestal tree! Using defaults\n");
    Set_Run_Number( -99 );
    for(int i=0; i<nVF48*48; i++ ) Set_Ped_Adc_Rms( i, -99. );
    return 2;
   }

  Int_t run_number;
  Float_t ped_adc[nVF48*48];

  ped_tree->SetBranchAddress("run_number",&run_number);
  ped_tree->SetBranchAddress("ped_adc",&ped_adc); 

  ped_tree->GetEntry( ped_tree->GetEntries() - 1 );
 
  Set_Run_Number( run_number );
  for(int i=0; i<nVF48*48; i++ ) Set_Ped_Adc_Rms( i, ped_adc[i] );
  
  ped_file->Close();

  return 0;
}
