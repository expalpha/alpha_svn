#include "../include/TSiliconStripRMS.h"

// TSiliconStripRMS Class =========================================================================================
//
// Class representing the RMS of a single silicon strip for sum0 events.
//
// JWS 02/07/2008
//
// ==========================================================================================================

ClassImp(TSiliconStripRMS);

TSiliconStripRMS::TSiliconStripRMS()
{
}

TSiliconStripRMS::TSiliconStripRMS( Int_t _ModuleNumber, Int_t _ASICNumber, Int_t _StripNumber )
{
  ModuleNumber = _ModuleNumber;
  ASICNumber = _ASICNumber;
  StripNumber = _StripNumber;

  sum0 = 0;
  sum1 = 0.;
  sum2 = 0.;
}

TSiliconStripRMS::~TSiliconStripRMS()
{
}

Double_t TSiliconStripRMS::GetStripRMS()
{ 
  Double_t rms = -1.;
  if (sum0 >0)
  rms = ( (Double_t) sum2 / (Double_t) sum0 ) - ( sum1 / (Double_t) sum0 ) * ( sum1 / (Double_t) sum0 );
  if(rms>0.) rms = sqrt(rms);

  return rms;
}

void TSiliconStripRMS::AddADCValue( Double_t _ADC )
{
  if (_ADC < 1025) {
    sum0 ++;
    sum1 += _ADC;
    sum2 += (_ADC * _ADC);
  }
}

void TSiliconStripRMS::AddADCValue( Double_t _ADC, Double_t rawADC )
{
  if (rawADC < 1025) {
  sum0 ++;
  sum1 += _ADC;
  sum2 += (_ADC * _ADC);
  rawadc += rawADC;
  }
}

