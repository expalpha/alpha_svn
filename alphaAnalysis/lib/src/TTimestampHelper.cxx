#include "TTimestampHelper.h"
#include "assert.h"

// TTimestampHelper Class =========================================================================================
//
// Class used as a cache of SIS and VF48 timestamps. This cache saves 
// excessive file IO by avoiding the need to scan the file for event
// timestamps
//
// JTKM 16/07/2018
//
// ==========================================================================================================


using namespace std;

ClassImp(TTimestampHelper);

TTimestampHelper::TTimestampHelper()
{
  runNumber=0;
}

void TTimestampHelper::ClearVectors()
{
  for (Int_t i=0; i<64; i++)
  {
    SISTS[i].clear();
  }
  VF48TS.clear();
}

void TTimestampHelper::ClearSISVectorsExcept(Int_t ChannelToKeep)
{
  for (Int_t i=0; i<64; i++)
  {
    if (ChannelToKeep == i) continue;
    SISTS[i].clear();
  }
  if (ChannelToKeep == -1 ) return;
  VF48TS.clear();
}

void TTimestampHelper::AddAtEnd(TTimestampHelper* addme)
{
  assert(runNumber==addme->GetRunNumber());
  for (Int_t i=0; i<64; i++)
  {
    //if (addme->GetSISEvents(i))
    if (addme->GetSISEvents(i)<=0) continue;
    std::vector<Double_t> tmp=addme->GetSISTSVector(i);
    SISTS[i].insert(SISTS[i].end(),tmp.begin(),tmp.end());
  }
  //if (addme->GetVF48Events())
  if (addme->GetVF48Events()<=0) return;
  std::vector<Double_t> tmp=addme->GetVF48TSVector();
  VF48TS.insert(VF48TS.end(),tmp.begin(),tmp.end());
}

TTimestampHelper::~TTimestampHelper()
{
  //Delete vectors here
}


Int_t TTimestampHelper::GetFirstVF48EventBefore(Double_t Timestamp)
{
  //Replace this with a binary search once this object has been demonstrated to help
  Int_t entry;
  for (entry=0; entry<GetVF48Events(); entry++)
  {
    if (Timestamp<VF48TS.at(entry)) break;
  }
  if (entry>0) return entry-1;
  return 0;
}
Int_t TTimestampHelper::GetFirstSISEventBefore(Int_t SisCh,Double_t Timestamp)
{
  //Replace this with a binary search once this object has been demonstrated to help
  Int_t entry;
  for (entry=0; entry<GetSISEvents(SisCh); entry++)
  {
    if (Timestamp<SISTS[SisCh].at(entry)) break;
  }
  if (entry>0) return entry-1;
  return 0;
}

Double_t TTimestampHelper::GetTimeOfSISCount(Int_t SisCh,Int_t Entry)
{
  Double_t time=-999; 
  if (Entry==0) std::cout <<"TTimestampHelper::GetTimeOfSISCount error. Please count SIS count from 1"<<std::endl;
  if (GetSISEvents(SisCh)>SISTS[SisCh].at(Entry-1));
    time= SISTS[SisCh].at(Entry-1);
  return time;
}

void TTimestampHelper::Print(Int_t Verbosity)
{
  cout <<"Timestamp helper for Run "<< runNumber<<endl;
   // getters
  cout <<"VF48 Events: "<< GetVF48Events()<<endl;
  for (Int_t i=0; i<64; i++)
    cout <<"SIS["<<i<<"] Events: "<< GetSISEvents(i) <<endl;
}
