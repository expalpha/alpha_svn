#include "TUtilities.h"

// TUtilities Class =========================================================================================
//
// Class collecting togther low level functions used in alphaAnalysis. 
//
// JWS 25/06/2010
//
// ==========================================================================================================


ClassImp(TUtilities);

TUtilities::TUtilities()
{
}

TUtilities::~TUtilities()
{
}


TH1D* TUtilities::Get_TH1D( TDirectory* dir, char* name, int nbinsx, double xlow, double xup ){
  return Get_TH1D( dir, name, name, nbinsx, xlow, xup );
}

TH1D* TUtilities::Get_TH1D( TDirectory* dir, char* name, char* title, int nbinsx, double xlow, double xup ){
  
  TH1D* the_Hist = NULL;
  if (dir) the_Hist = (TH1D*) dir->FindObject(name);
  
  if (!the_Hist)
    {
      the_Hist = new TH1D(name, title, nbinsx, xlow, xup);
      if (dir) dir->Add(the_Hist);
    }
  
  return the_Hist;  
}

TH2D* TUtilities::Get_TH2D( TDirectory* dir, char* name, int nbinsx, double xlow, double xup, int nbinsy, double ylow, double yup ){
 return Get_TH2D( dir, name, name, nbinsx, xlow, xup, nbinsy, ylow, yup );
}

TH2D* TUtilities::Get_TH2D( TDirectory* dir, char* name, char* title, int nbinsx, double xlow, double xup, int nbinsy, double ylow, double yup )
{
  TH2D* the_Hist = NULL;
  if (dir) the_Hist = (TH2D*) dir->FindObject(name);

  if (!the_Hist)
    {
      the_Hist = new TH2D(name, title, nbinsx, xlow, xup, nbinsy, ylow, yup);
      if (dir) dir->Add(the_Hist);
    }

  return the_Hist;
}

TGraphErrors* TUtilities::Make_TGraphErrors( TDirectory* dir, char* name, Int_t n,  Double_t* x, Double_t* y, Double_t* ex, Double_t* ey )
{
  TGraphErrors* the_Graph = NULL;
  if (dir) the_Graph = (TGraphErrors*) dir->FindObject(name);

  if (!the_Graph)
    {
      the_Graph = new TGraphErrors( n, x, y, ex, ey );
      the_Graph->SetName( name );
      the_Graph->SetTitle( name );
      if (dir) dir->Add(the_Graph);
    }

  return the_Graph;
}

TGraph* TUtilities::Make_TGraph( TDirectory* dir, char* name, TH1* hist )
{
  TGraph* the_Graph = NULL;
  if (dir) the_Graph = (TGraph*) dir->FindObject(name);

  if (!the_Graph)
    {
      the_Graph = new TGraph( hist);
      if (dir) dir->Add(the_Graph);
    }

  return the_Graph;
}


TDirectory* TUtilities::Get_Directory( const char* name, TFile* location )
{
  TDirectory* dir = NULL;

  if (location)
    dir = (TDirectory*)location->Get( name );
  
  //  location->ls();
  if (location && !dir)
    dir = location->mkdir( name );

  return dir;
}

TDirectory* TUtilities::Get_Sub_Directory( const char* sub_name, const char* dir_name, TFile* location )
{
  TDirectory* dir = Get_Directory( dir_name, location );

  TDirectory* sub_dir = NULL;

  if (location)
    sub_dir = (TDirectory*)dir->Get( sub_name );
  
  //  location->ls();
  if (location && !sub_dir)
    sub_dir = dir->mkdir( sub_name );

  return sub_dir;
}
