#include "TalphaStripsReport.h"

// TAnalysis Class =========================================================================================
//
// Class collecting togther very basic version and QOD report data. 
//
// JTKM 7/12/14
//
// ==========================================================================================================


using namespace std;

ClassImp(TalphaStripsReport);

TalphaStripsReport::TalphaStripsReport()
{
  Histos1D=new TObjArray(0);
  Histos2D=new TObjArray(0);
  StripSettings=new TObjArray();
  SetVF48Events(0, 0);
  
  nSideQuietThresh=2;
  nSideNoiseyThresh=30;
  pSideQuietThresh=1;
  pSideNoiseyThresh=30;
  
}


TalphaStripsReport::~TalphaStripsReport()
{
  SetRunNumber(-1);
  Histos1D->SetOwner(kTRUE);
  Histos2D->SetOwner(kTRUE);
  StripSettings->SetOwner(kTRUE);
  Histos1D->Delete();
  delete Histos1D;
  Histos2D->Delete();
  delete Histos2D;
  StripSettings->Delete();
  delete StripSettings;
}

void TalphaStripsReport::PrintHistos2D(Int_t Verbosity)
{
  for (Int_t i=0; i<Histos2D->GetEntries();i++)
  {
    cout << "HistoName:"<<Histos2D->At(i)->GetName()<<endl;
  }
  return;
}

void TalphaStripsReport::PrintQuietStrips(Int_t Verbosity)
{

  std::cout <<"\t\t\tN-side\t\tP-side"<<std::endl;
  std::cout<<"  Mean RMS \t\t"<< GetnSideRMSMean() << "\t\t"<< GetpSideRMSMean()<<endl;
  std::cout <<"  Quiet strips \t\t"<<GetnSideQuietCount();
  std::cout <<"\t\t"<<GetpSideQuietCount()<<std::endl;
  std::cout <<"  Noisy strips \t\t"<<GetnSideNoisyCount();
  std::cout <<"\t\t"<<GetpSideNoisyCount()<<std::endl;
  
}

void TalphaStripsReport::Print(Int_t Verbosity)
{
  std::cout <<std::endl<<std::endl;
  std::cout <<" ===========alphaStrip Report - Run "<< runNumber << "==========="<<std::endl;
  //std::cout <<"VF48 Events used in alphaStrips:"<<   GoodVF48Events <<" good "<< BadVF48Events <<" bad"<<std::endl;
  std::cout <<"  VF48 Events:\t"<< GetGoodVF48Events() <<" good\t"<< GetBadVF48Events() <<" bad"<<std::endl;
  PrintQuietStrips(Verbosity);
  
  std::cout<<"  alphaStrips run on host: "<<HostName<<" with "<<std::endl;
  std::cout<<"  \t\tSVN Version: "<< SVNalphaStripsRevision<<" on: "<< GetAnalysisDateDay() << "/" << GetAnalysisFileDateMonth() << "/" << GetAnalysisFileDateYear() <<endl;

  
  std::cout <<" ==================================================="<<std::endl;
}
