#!/bin/bash

echo "//TSiliconLibVersion.h: I am a file that should not be committed to the SVN!"> include/LibVersion.h
echo "//                      I am created by the makefile">> include/LibVersion.h
echo "#ifndef _TSiliconLibVersion_">> include/LibVersion.h
echo "#define _TSiliconLibVersion_">> include/LibVersion.h
echo "#include \"TString.h\"">> include/LibVersion.h

#Record the SVN only number:
echo -n "TString _LibSVNRevision_=\"" >> include/LibVersion.h
svnversion -n ${RELEASE} >> include/LibVersion.h
echo "\";"  >> include/LibVersion.h

#Record the full SVN info
echo -n "TString _LibSVNFull_=\"">> include/LibVersion.h
echo -n `svn info ${RELEASE} | sed -E ':a;N;$!ba;s/\r{0,1}\n/\\n/g' ` >> include/LibVersion.h
echo  "\";">> include/LibVersion.h
if [ "$RPLATFORM" == "macosx64" ]; then
  echo 'unsigned int gLibTime='`stat include/LibVersion.h | awk -F ' ' '{print $2}'` ';'>> include/LibVersion.h
else
  echo 'unsigned int gLibTime='`stat include/LibVersion.h -c %Y`';'>> include/LibVersion.h
fi


echo "#endif" >> include/LibVersion.h
