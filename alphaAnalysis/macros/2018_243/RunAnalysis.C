#include "PlotMVA.C+"
//#include "PlotDumps.C+"


void RunOneAnalysis(Int_t runNumber,Int_t Freq, Int_t IsCstate, Int_t repetition)
{
  #if MVA
  
  #endif
    Double_t ActualFreq[9];
    if (runNumber==57208)
    {
      ActualFreq[0]=-100;
      ActualFreq[1]= -25;
      ActualFreq[2]= -12.5;
      ActualFreq[3]=  -0.;
      ActualFreq[4]=  12.5;
      ActualFreq[5]=  25;
      ActualFreq[6]=  37.5;
      ActualFreq[7]=  50;
      ActualFreq[8]= 100;
    }
    if (runNumber==57195 || runNumber==57181 )
    {
      ActualFreq[0]=-200;
      ActualFreq[1]=-100;
      ActualFreq[2]= -50;
      ActualFreq[3]= -25;
      ActualFreq[4]=   0.;
      ActualFreq[5]=  25;
      ActualFreq[6]=  50;
      ActualFreq[7]= 100;
      ActualFreq[8]= 200;
    }
    
  if (!IsCstate) cout <<"D State!"<<endl;
  //TAlphaPlot* a=new TAlphaPlot();  
  Int_t RealFreq=-1;
  for (Int_t j=0; j<2; j++) // List number
  {
    //for (Int_t i=0; i<50; i++) //Repetition of dump
    {
      int i = repetition;
      Int_t ListNo=j;
      if (j==0) RealFreq=Freq;
      if (j==1) RealFreq=8-Freq;
      if (IsCstate) ListNo=j+2;
      //Dump name (uses Freq)
      TString string="243 List ";
      string+=ListNo;
      string+=" Freq ";
      string+=Freq;
      //Nickname (uses Real Freq)
      TString NickName="TrueFreq ";
      NickName+=RealFreq;
      if (IsCstate && !(runNumber==57208)) NickName+=" Cstate ";
      if (!IsCstate) NickName+=" Dstate ";
      NickName+=" rep: ";
      NickName+=i;

      double first_dump=TRootUtils::MatchEventToTime(runNumber,"startDump","243 List",1);

      //if (RunNo)
      TString CSVName="";
      CSVName+="\"";
      CSVName+=string;
      CSVName+="\"(";
      CSVName+=i;
      CSVName+="),";
      CSVName+=RealFreq;
      CSVName+=",";
      CSVName+=ActualFreq[RealFreq];
      CSVName+=",";
      CSVName+=ListNo;
      CSVName+=",";
      CSVName+=IsCstate;
      CSVName+=",";
      CSVName+=i;
      CSVName+=",";
      CSVName+=first_dump;
      CSVName+=",";
      cout << string <<"\t\t really is \t\t: "<< NickName << " ("<<ActualFreq[RealFreq]<<")"<<endl;
      Double_t tmin=TRootUtils::MatchEventToTime(runNumber,"startDump",string,i+1);
      if (tmin<0)
      {
        cout <<string<< " for rep " << i+1 << "has badness"<<endl;
        continue;
      }
      Double_t tmax=TRootUtils::MatchEventToTime(runNumber,"stopDump",string,i+1);
      Double_t tnext=-1.;
      Int_t dump=0;
      while (tnext<tmax)
      {
        tnext=TRootUtils::MatchEventToTime(runNumber,"startDump","243 List",dump);
        //Catch the last dump case:
        if (tnext<0.) tnext=MatchEventToTime(runNumber,"startDump","Microwave Dump");
        dump++;
      }
      //Double_t nextbin=Get_RunTime_of_Count(runNumber,28,1,tmax,tmax+10.);
      //cout <<"start: "<<tmin<<"\tstop: "<<tmax<<"\t next after"<<tnext<<endl;
      cout <<"start: "<<tmin<<"\tstop: "<<tmax-tmin<<"\t next after"<<tnext-tmin<<endl;
      grfcut=0.1867;
      TRootUtils::SetZCut(-10.,10.);
      PrintTimeGateWindow(runNumber,tmin,tmax,CSVName);
      PrintDetectorQOD(runNumber,tmin,tmax,CSVName);
      
      TString PostBinName="AfterBin";
      PostBinName+=CSVName;
      PrintTimeGateWindow(runNumber,tmax,tnext,PostBinName);
      PrintDetectorQOD(runNumber,tmax,tnext,PostBinName);
      
      TString HoldAfterBinName="LifetimeAfterBin";
      HoldAfterBinName+=CSVName;
      grfcut=0.2550;
      TRootUtils::SetZCut(-25.,25.);
      
      PrintTimeGateWindow(runNumber,tmax,tnext,HoldAfterBinName);
      PrintDetectorQOD(runNumber,tmax,tnext,HoldAfterBinName);
      
      //PrintDumpGateWindow(runNumber,string,i+1);
      //a->AddToTAlphaPlot(GetPlotVertices(runNumber,string,i+1));
    }
  }
}

void RunAnalysis(Int_t runNumber)
{
  int reps=50;
  if (runNumber==57181) reps=49;
  for (Int_t Freq=0; Freq<9;Freq++)
  {
    for (Int_t IsCstate=0; IsCstate<2; IsCstate++)
    {
      for (Int_t rep=0; rep<reps; rep++)
      {
        RunOneAnalysis(runNumber,Freq,IsCstate,rep);
      }
    }
  }
  return;
  //return a->Canvas();
}
/*void RunOne(Int_t runNumber, Int_t freq, Bool_t state)
{
//  for (Int_t j=0; j<2; j++){
Run(runNumber,freq,state);
TString Name="";
if (state==0) Name+="DState_2reps_Freq";
else Name+="CState_2reps_Freq";
Name+=freq;
cout <<freq<<endl;
//    SaveCanvas(runNumber,Name);
//  }
}*/
/*void RunAll(Int_t runNumber)
{
for (Int_t i=0; i<9;i++){
for (Int_t j=0; j<2; j++){
gc=Run(runNumber,i,(Bool_t)j);
TString Name="";
if (j==0) Name+="DState_50reps_Freq";
else Name+="CState_50reps_Freq";
Name+=i;
SaveCanvas(runNumber,Name);
}
}
return;
}*/


void RunAnalysis()
{
RunAnalysis(57208);
RunAnalysis(57195);
RunAnalysis(57181);
}
