#!/bin/bash
################################################################
#  BASIC ANALYSIS SCRIPT FOR USING ALL CPU CORES ON A MACHINE  #
#                                                              #
#  Usage:                                                      #
#    ./run_parallel.sh listfile.txt                            #
################################################################

#Check myconfig settings
if [ ${#RELEASE} -gt 2 ]; then
  echo "myconfig sourced... good"
else
  echo "Please source myconfig.sh"
  exit
fi




#Set up analysis 'job'
job()
{

  local Args=${1}
  #local Freq=${2}
  #local IsCsate=${3}
  cd $RELEASE/alphaAnalysis/macros
  echo ".L 2018_243/RunAnalysis.C
  SetTMVA(\"BDT\",\"243_2018\");
  grfcut=0.1867;
  TRootUtils::SetZCut(-10.,10.);
  RunOneAnalysis(${Args});
  .q
  " | root -l -b &> 2018_243/logs/${Args}.log
}


#Fancy process bar:
function print_progress ()
{
Job=$1
Total=$2
echo " "
echo "Job $Job of $Total at `date` "

# Process data
    let _progress=(${1}*100/${2}*100)/100
    let _done=(${_progress}*4)/10
    let _left=40-$_done
# Build progressbar string lengths
    _fill=$(printf "%${_done}s")
    _empty=$(printf "%${_left}s")
printf "\rProgress : [${_fill// /#}${_empty// /-}] ${_progress}%%"
echo ""
}



#One can override 'maxjobs' if you want to set the number of CPUs manually
maxjobs=`nproc`
maxjobs=12

#List file given as first arguement
rm ${1}_full 
for RUNNO in `cat ${1}`  ; do
  for Freq in `seq 0 8`; do
    for IsCstate in 0 1; do 
      for rep in `seq 0 49`; do
        if [ ${RUNNO} -eq 57181 ]; then
          #Run 57181 only has 49 laser repetitions
          if [ ${rep} -eq 49 ]; then
            continue
          fi
        fi
        echo "${RUNNO},${Freq},${IsCstate},${rep}" >> ${1}_full 
      done
    done
  done
done
LISTFILE=${1}_full


NUMOFLINES=`cat $LISTFILE | wc -l` #$(wc -l < "$LISTFILE")
JobCount=0

while IFS='' read -r RUNNO || [[ -n ${RUNNO} ]]; do
	if [ $maxjobs -gt 1 ]
	then
		jobcnt=(`jobs -p`)
		#echo "Jobcount ${jobcnt[@]}"
		if [ ${#jobcnt[@]} -lt $maxjobs ] ; then
			echo "Starting Job with ${RUNNO} "
			JobCount=$((JobCount+1))
			print_progress $JobCount $NUMOFLINES
			job "${RUNNO}" &
		else
			echo "waiting"
			while [ ${#jobcnt[@]} -ge $maxjobs ]
			do
				sleep 30
				echo "$maxjobs jobs running... sleeping 30s"
				jobcnt=(`jobs -p`)
			done
			echo "Running strips $RUNNO "
			JobCount=$((JobCount+1))
			print_progress $JobCount $NUMOFLINES
			job "${RUNNO}" &
		fi
    else
		job  "${RUNNO}" 
    fi
done < "$LISTFILE"
echo "waiting for jubs to finish"
wait

echo "done..."
