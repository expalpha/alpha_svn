cd $RELEASE/alphaAnalysis/macros/

#Laser cooling:
ZCUT=25
RFCUT=0.2550
CLASS="243_2018"
#Run 57181 ( no cooling time)

echo ".L 2018_243/StackingLife.C
  SetTMVA(\"BDT\",\"${CLASS}\");
  SetRFCut(${RFCUT});
  gZcutMin=-${ZCUT};
  gZcutMax=${ZCUT};
  StackingLife(57181);
  .q
" | root -l -b &

#Run 57195:
#"Lyman Alpha Laser Cooling" 	    36826.3 	    44026.3 	    7200.03	290209	1140	799	447	0	69985	7478	
#   "Lyman Alpha No Laser" 	    44026.4 	    44028.1 	    1.64564	72	0	0	0	0	10	1	
#"Lyman Alpha Laser Cooling" 	    44028.1 	    51228.1 	    7200.03	289125	1043	701	396	0	70514	7214	
#   "Lyman Alpha No Laser" 	    51228.2 	    51229.9 	    1.70262	66	0	0	0	0	12	3	
#"Lyman Alpha Laser Cooling" 	    51229.9 	    58429.9 	    7200.03	288961	1074	694	399	0	70569	6875	


echo ".L 2018_243/StackingLife.C
  SetTMVA(\"BDT\",\"${CLASS}\");
  SetRFCut(${RFCUT});
  gZcutMin=-${ZCUT};
  gZcutMax=${ZCUT};
  StackingLife(57195);
  .q
" | root -l -b &

echo ".L PlotMVA.C+
  SetTMVA(\"BDT\",\"${CLASS}\");
  SetRFCut(${RFCUT});
  gZcutMin=-${ZCUT};
  gZcutMax=${ZCUT};
PrintTimeGateWindow(57195,36826.3,58429.9,\"Laser cooling\");
.q
" | root -l -b
echo ".L PlotMVA.C+
  SetTMVA(\"BDT\",\"${CLASS}\");
  SetRFCut(${RFCUT});
  gZcutMin=-${ZCUT};
  gZcutMax=${ZCUT};
PrintDetectorQOD(57195,36826.3,58429.9,\"Laser cooling\");
.q
" | root -l -b


#Run 57208
#"Laser cooling while stacking start" 	    120.188 	    120.298 	       0.11	9	0	0	0	0	3	0	
#"Laser cooling while stacking stop" 	    32896.3 	    32896.4 	       0.11	5	0	0	0	0	0	0	
#   "Lyman Alpha No Laser" 	    32956.6 	      32963 	    6.40283	256	1	1	0	0	52	5	
#"Lyman Alpha Laser Cooling" 	      32963 	    40163.1 	    7200.03	293453	1133	769	446	0	71085	7292	
#   "Lyman Alpha No Laser" 	    40163.2 	    40164.4 	    1.23124	52	0	0	0	0	11	0	
#"Lyman Alpha Laser Cooling" 	    40164.4 	    47364.4 	    7200.03	292556	1065	752	403	0	71005	7390	
#   "Lyman Alpha No Laser" 	    47364.5 	    47366.2 	    1.69835	81	0	0	0	0	13	1	
#"Lyman Alpha Laser Cooling" 	    47366.2 	    54566.3 	    7200.03	292249	1099	769	388	0	71051	7217	


echo ".L 2018_243/StackingLife.C
  SetTMVA(\"BDT\",\"${CLASS}\");
  SetRFCut(${RFCUT});
  gZcutMin=-${ZCUT};
  gZcutMax=${ZCUT};
  StackingLife(57208);
  .q
" | root -l -b &


echo ".L PlotMVA.C+
  SetTMVA(\"BDT\",\"${CLASS}\");
  SetRFCut(${RFCUT});
  gZcutMin=-${ZCUT};
  gZcutMax=${ZCUT};
PrintTimeGateWindow(57208,32963,54566.3,\"Laser cooling\");
.q
" | root -l -b
echo ".L PlotMVA.C+
  SetTMVA(\"BDT\",\"${CLASS}\");
  SetRFCut(${RFCUT});
  gZcutMin=-${ZCUT};
  gZcutMax=${ZCUT};
PrintDetectorQOD(57208,32963,54566.3,\"Laser cooling\");
.q
" | root -l -b
