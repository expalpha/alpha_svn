#!/bin/bash

cd ${RELEASE}/alphaAnalysis/macros

CLASS="243_2018"



  #FRD
  NAME="FRD"
  RFCUT=0.1410
  ZCUT=25
  #Analyse ${i} ${RFCUT} ${NAME} ${ZCUT}
  echo ".L PlotMVA.C+
    SetTMVA(\"BDT\",\"${CLASS}\");
    SetRFCut(${RFCUT});
    gZcutMin=-${ZCUT};
    gZcutMax=${ZCUT};
    PrintDetectorQOD(57208,61934.9,61934.9+2.,\"Recovered-FastRampDown\");
 .q
  " | root -l -b
  echo ".L PlotMVA.C+
    SetTMVA(\"BDT\",\"${CLASS}\");
    SetRFCut(${RFCUT});
    gZcutMin=-${ZCUT};
    gZcutMax=${ZCUT};
    PrintTimeGateWindow(57208,61934.9,61934.9+2.,\"Recovered-FastRampDown\");
    .q
  " | root -l -b



for i in  57181 57195 ; do

  #Microwave
  NAME="Microwave"
  RFCUT=0.1608
  ZCUT=10

  echo ".L PlotMVA.C+
    SetTMVA(\"BDT\",\"${CLASS}\");
    SetRFCut(${RFCUT});
    gZcutMin=-${ZCUT};
    gZcutMax=${ZCUT};
    PrintDetectorQOD(${i},\"Microwave\");
    .q
  " | root -l -b
  echo ".L PlotMVA.C+
    SetTMVA(\"BDT\",\"${CLASS}\");
    SetRFCut(${RFCUT});
    gZcutMin=-${ZCUT};
    gZcutMax=${ZCUT};
    PrintMicrowaveSweep(${i});
    .q
  " | root -l -b
  echo ".L PlotMVA.C+
    SetTMVA(\"BDT\",\"${CLASS}\");
    SetRFCut(${RFCUT});
    gZcutMin=-${ZCUT};
    gZcutMax=${ZCUT};
    PrintDumpGateWindow(${i},\"Microwave\");
    .q
  " | root -l -b
done

for i in  57181 57195 ; do

  #FRD
  NAME="FRD"
  RFCUT=0.1410
  ZCUT=25
  echo ".L PlotMVA.C+
    SetTMVA(\"BDT\",\"${CLASS}\");
    SetRFCut(${RFCUT});
    gZcutMin=-${ZCUT};
    gZcutMax=${ZCUT};
    PrintDetectorQOD(${i},\"FastRampDown\");
    .q
  " | root -l -b
  echo ".L PlotMVA.C+
    SetTMVA(\"BDT\",\"${CLASS}\");
    SetRFCut(${RFCUT});
    gZcutMin=-${ZCUT};
    gZcutMax=${ZCUT};
    PrintDumpGateWindow(${i},\"FastRampDown\");
    .q
  " | root -l -b
done
