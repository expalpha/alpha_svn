#!/bin/bash

cd ${RELEASE}/alphaAnalysis/macros

#Extra cosmics?
#57185
#57197

CLASS="243_2018"


function RunCosmics {
  i=${1}
  RFCUT="${2}"
  NAME="${3}"
  ZCUT="${4}"
  echo ".L PlotMVA.C+
    SetTMVA(\"BDT\",\"${CLASS}\");
    SetRFCut(${RFCUT});
    gZcutMin=-${ZCUT};
    gZcutMax=${ZCUT};
    PrintDetectorQOD(${i},0.,-1.,\"${NAME}-Cosmic\");
    .q
    " | root -l -b
}

function Analyse {
  i=${1}
  RFCUT="${2}"
  NAME="${3}"
  ZCUT="${4}"

  echo ".L PlotMVA.C+
  SetTMVA(\"BDT\",\"${CLASS}\");
  SetRFCut(${RFCUT});
  gZcutMin=-${ZCUT};
  gZcutMax=${ZCUT};
  PreCatchingCosmicBaseline(${i},\"${NAME}-PreCatchCosmics\");
  .q
  " | root -l -b &
  sleep 2
  
  echo ".L PlotMVA.C+
  SetTMVA(\"BDT\",\"${CLASS}\");
  SetRFCut(${RFCUT});
  gZcutMin=-${ZCUT};
  gZcutMax=${ZCUT};
  PostTrappingCosmicBaselineLong(${i},\"${NAME}-PostTrappingCosmics\");
  .q
  " | root -l -b &
  sleep 2
  
  STACK=150
  if [ ${i} -eq 57208 ]; then
    STACK=130
  fi
  for mix in `seq 2 2 ${STACK}`; do
    echo ".L PlotMVA.C+
    SetTMVA(\"BDT\",\"${CLASS}\");
    SetRFCut(${RFCUT});
    gZcutMin=-25;
    gZcutMax=25;
    PrintDetectorQOD(${i},\"Mixing\",${mix},0,\"${NAME}-Mixing-${mix}\");
    .q
    " | root -l -b
  done

}

#for i in 57208; do
#for i in 57195 57181 57208; do
for i in `cat 2018_243/ExtaCosmics.list`; do
  #243 spectrum:
  NAME="243"
  RFCUT=0.1867
  ZCUT=10
  RunCosmics ${i} ${RFCUT} ${NAME} ${ZCUT} 

  #HBar hold
  NAME="Lifetime"
  RFCUT=0.2550
  ZCUT=25
  RunCosmics ${i} ${RFCUT} ${NAME} ${ZCUT} &

  #FRD
  NAME="FRD"
  RFCUT=0.1410
  ZCUT=25
  RunCosmics ${i} ${RFCUT} ${NAME} ${ZCUT} &

  NAME="Microwave"
  RFCUT=0.1608
  ZCUT=10
  RunCosmics ${i} ${RFCUT} ${NAME} ${ZCUT} &


done

for i in  57181 57195 57208 ; do
  #243 spectrum:
  NAME="243"
  RFCUT=0.1867
  ZCUT=10
  Analyse ${i} ${RFCUT} ${NAME} ${ZCUT} &
  
  #FRD
  NAME="FRD"
  RFCUT=0.1410
  ZCUT=25
  Analyse ${i} ${RFCUT} ${NAME} ${ZCUT} &
  
  #Microwave
  NAME="Microwave"
  RFCUT=0.1608
  ZCUT=10
  Analyse ${i} ${RFCUT} ${NAME} ${ZCUT} &
  
  #HBar hold
  NAME="Lifetime"
  RFCUT=0.2550
  ZCUT=25
  Analyse ${i} ${RFCUT} ${NAME} ${ZCUT} &
  
done
