// AlphaAnalysis Utilties for interfacing with the tree file
// Split off from Plot_Vertices.cpp, etc... by Eoin, Dec 12 2008
// use by adding an #include "AlphaAnalysisUtilities.cpp" line in the macro

// SIS Triggers ==================================================================================

#include <TH1D.h>
#include "../lib/include/SIS_Channels.h.alpha1"


TH1D* Sum_SIS_Triggers( const Int_t runs, Int_t* run_numbers, Int_t start_trigger_channel, Int_t start_trigger_repetition, Int_t end_trigger_channel, Int_t end_trigger_repetition )
{
  TH1D* sum_of_plots;
  TH1D* a_plot;
  sum_of_plots = Get_SIS_Triggers( run_numbers[0], start_trigger_channel, start_trigger_repetition, end_trigger_channel, end_trigger_repetition );
  for( Int_t i=1; i<runs; i++ )
    {
      sum_of_plots->Add( Get_SIS_Triggers( run_numbers[i], start_trigger_channel, start_trigger_repetition, end_trigger_channel, end_trigger_repetition ) );
    }

  return sum_of_plots;
}

TH1D* Sum_SIS_Triggers( const Int_t runs, Int_t* run_numbers, Int_t* start_trigger_channel, Int_t* start_trigger_repetition, Int_t* end_trigger_channel, Int_t* end_trigger_repetition ,Int_t SisChannel)
{
  TH1D* sum_of_plots;
  TH1D* a_plot;

  sum_of_plots = Get_SIS_Triggers( run_numbers[0], start_trigger_channel[0], start_trigger_repetition[0]
                                           , end_trigger_channel[0], end_trigger_repetition[0] ,SisChannel);

  for( Int_t i=1; i<runs; i++ )
    {
      sum_of_plots->Add( Get_SIS_Triggers( run_numbers[i], start_trigger_channel[i], start_trigger_repetition[i], end_trigger_channel[i], end_trigger_repetition[i]
                                                         , SisChannel ) );
    }

  return sum_of_plots;
}

TH1D* Sum_SIS_Triggers( const Int_t runs, Int_t* run_numbers, Int_t* start_trigger_channel, Int_t* start_trigger_repetition, Int_t* end_trigger_channel, Int_t* end_trigger_repetition ,Int_t SisChannel ,char* Plot_Title ,Int_t binning = 1000 , Int_t debug = 0 )
{
    TH1D* sum_of_plots;
    TH1D* a_plot;

    sum_of_plots = Get_SIS_Triggers( run_numbers[0], start_trigger_channel[0], start_trigger_repetition[0]
            , end_trigger_channel[0], end_trigger_repetition[0] ,SisChannel , Plot_Title , binning , debug );

    for( Int_t i=1; i<runs; i++ )
    {
        sum_of_plots->Add( Get_SIS_Triggers( run_numbers[i], start_trigger_channel[i], start_trigger_repetition[i]
                , end_trigger_channel[i], end_trigger_repetition[i]
                        , SisChannel , Plot_Title , binning , debug ) );
    }

    return sum_of_plots;
}
TH1D* Get_SIS_Triggers( Int_t run_number, Int_t start_trigger_channel, Int_t start_trigger_repetition, Int_t end_trigger_channel, Int_t end_trigger_repetition  )
{
  Double_t start_time = Find_SIS_Channel_Trigger_Time( run_number, start_trigger_channel, start_trigger_repetition );
  Double_t end_time = Find_SIS_Channel_Trigger_Time( run_number, end_trigger_channel, end_trigger_repetition );

  TTree* DET_weed = Get_Sis_Det_Weed( run_number );
  TSisEvent* sisEvent = new TSisEvent();
  DET_weed->SetBranchAddress("SisEvent", &sisEvent );

  char plot_title[80];
  sprintf( plot_title, "Quench time(s)" );
  TH1D* the_plot = new TH1D( plot_title, plot_title, 10, 0, 0.001 );
  the_plot->GetXaxis()->SetTitle("time /s");
  the_plot->GetYaxis()->SetTitle("events");

  Int_t sum_counts(0);

  for ( int i = 0 ; i < DET_weed->GetEntries() ; i++ )
    {
      DET_weed->GetEntry(i) ;

      // selection cuts
      if( sisEvent->GetRunTime() < start_time || sisEvent->GetRunTime() > end_time) continue;
      //if( sisEvent->GetChannel() != 51 ) continue;
      //if( sisEvent->GetChannel() != 45 ) continue;
      if( sisEvent->GetChannel() != 48 ) continue;


      // ... fill histogram
      the_plot->Fill( sisEvent->GetRunTime()-start_time, sisEvent->GetCountsInChannel()  );
     //if ( debug!=0 ) printf( "...added event with time %f and counts %d \n", sisEvent->GetRunTime(), sisEvent->GetCountsInChannel() );

      sum_counts+=sisEvent->GetCountsInChannel();

    }//loop over vertex tree

 printf("sum_counts = %d \n", sum_counts );

  return the_plot;
}

TH1D* Get_SIS_Triggers( Int_t run_number, Int_t start_trigger_channel, Int_t start_trigger_repetition, Int_t end_trigger_channel, Int_t end_trigger_repetition ,Int_t SisChannel )
{
  Double_t start_time = Find_SIS_Channel_Trigger_Time( run_number, start_trigger_channel, start_trigger_repetition );
  Double_t end_time = Find_SIS_Channel_Trigger_Time( run_number, end_trigger_channel, end_trigger_repetition );

  TTree* DET_weed = Get_Sis_Det_Weed( run_number );
  TSisEvent* sisEvent = new TSisEvent();
  DET_weed->SetBranchAddress("SisEvent", &sisEvent );

  char plot_title[80];
  sprintf( plot_title, "Dump " );
  TH1D* the_plot = new TH1D( plot_title, plot_title, 1000 , 0.0, end_time - start_time );
  the_plot->GetXaxis()->SetTitle("time /s");
  the_plot->GetYaxis()->SetTitle("events");

  Int_t sum_counts(0);

  for ( int i = 0 ; i < DET_weed->GetEntries() ; i++ )
    {
      DET_weed->GetEntry(i) ;

      // selection cuts
      if( sisEvent->GetRunTime() < start_time || sisEvent->GetRunTime() > end_time) continue;
      //if( sisEvent->GetChannel() != 51 ) continue;
      //if( sisEvent->GetChannel() != 45 ) continue;
      if( sisEvent->GetChannel() != SisChannel ) continue;


      // ... fill histogram
      the_plot->Fill( sisEvent->GetRunTime()-start_time, sisEvent->GetCountsInChannel()  );
      //if ( debug!= 0 ) printf( "...added event with time %f and counts %d \n", sisEvent->GetRunTime(), sisEvent->GetCountsInChannel() );

      sum_counts+=sisEvent->GetCountsInChannel();

    }//loop over vertex tree


    printf("run %d : sum_counts = %d \n", run_number , sum_counts );
  return the_plot;
}

TH1D* Get_SIS_Triggers( Int_t run_number, Int_t start_trigger_channel, Int_t start_trigger_repetition, Int_t end_trigger_channel, Int_t end_trigger_repetition ,Int_t SisChannel,char* Plot_Title , Int_t binning = 1000 , Int_t debug = 0 )
{
    Double_t start_time = Find_SIS_Channel_Trigger_Time( run_number, start_trigger_channel, start_trigger_repetition );
    Double_t end_time = Find_SIS_Channel_Trigger_Time( run_number, end_trigger_channel, end_trigger_repetition );

    TTree* DET_weed = Get_Sis_Det_Weed( run_number );
    TSisEvent* sisEvent = new TSisEvent();
    DET_weed->SetBranchAddress("SisEvent", &sisEvent );

    char plot_title[80];
    sprintf( plot_title, Plot_Title );
    TH1D* the_plot = new TH1D( plot_title, plot_title, (end_time - start_time)*binning , 0.0, end_time - start_time );
    the_plot->GetXaxis()->SetTitle("time /s");
    the_plot->GetYaxis()->SetTitle("events");

    Int_t sum_counts(0);

    for ( int i = 0 ; i < DET_weed->GetEntries() ; i++ )
    {
        DET_weed->GetEntry(i) ;

      // selection cuts
        if( sisEvent->GetRunTime() < start_time || sisEvent->GetRunTime() > end_time) continue;
      //if( sisEvent->GetChannel() != 51 ) continue;
      //if( sisEvent->GetChannel() != 45 ) continue;
        if( sisEvent->GetChannel() != SisChannel ) continue;


      // ... fill histogram
        the_plot->Fill( sisEvent->GetRunTime()-start_time, sisEvent->GetCountsInChannel()  );
        //if ( debug!= 0 )printf( "...added event with time %f and counts %d \n", sisEvent->GetRunTime(), sisEvent->GetCountsInChannel() );

        sum_counts+=sisEvent->GetCountsInChannel();

    }//loop over vertex tree

    printf("%s raw counts = %d \n",Plot_Title , sum_counts );

    return the_plot;
}

TH1D* Sum_SIS_Triggers( const Int_t runs, Int_t* run_numbers, Int_t* start_trigger_channel, Int_t* start_trigger_repetition, Int_t* end_trigger_channel, Int_t* end_trigger_repetition, Double_t tmin , Double_t tmax )
{
  TH1D* sum_of_plots;
  TH1D* a_plot;

  sum_of_plots = Get_SIS_Triggers( run_numbers[0], start_trigger_channel, start_trigger_repetition, end_trigger_channel, end_trigger_repetition , tmin,tmax,);

  for( Int_t i=1; i<runs; i++ )
    {
      sum_of_plots->Add( Get_SIS_Triggers( run_numbers[i], start_trigger_channel, start_trigger_repetition, end_trigger_channel, end_trigger_repetition,tmin,tmax ) );
    }

  return sum_of_plots;
}

Int_t Plot_Backgrounds(TString fileName )
{
    FILE* myFile =NULL;
    myFile = new FILE();
    myFile = fopen( fileName , "r");


    const Int_t max_run_numbers = 10000 ;
    Int_t run_numbers[max_run_numbers] ;
    Int_t runs = 0 ;

    if( myFile == NULL )
    {
        printf("... text file %s not found.\n", fileName );
        return -1;
    }
    else
    {
        do
        {
            fscanf(myFile ,"%d \n", &run_numbers[runs]  );
            runs++ ;
        }while( !feof(myFile) );
    }

    const Int_t Number_Runs = runs ;
    Double_t Time_After_AD[Number_Runs];

    Int_t Deg_Total_Counts[Number_Runs];
    Int_t Trap_Total_Counts[Number_Runs];
    Int_t Downstream_Total_Counts[Number_Runs];
    Int_t Dump_Numbers[Number_Runs];
    Int_t Silicon_Total_Counts[Number_Runs];

    TH1D* Trap_PMT[Number_Runs] ;
    TH1D* Downstream_PMT[Number_Runs];
    TH1D* Deg_PMT[Number_Runs];
    TH1D* Silicon[Number_Runs];

    Sum_Background_Triggers( runs , run_numbers , 2 , Time_After_AD , Deg_Total_Counts , Dump_Numbers ) ;

    fclose(myFile);

    FILE* outputFile ;
    outputFile = new FILE();
    outputFile = fopen("Background_output.txt" , "w");

    for ( int i = 0 ; i < Number_Runs ; i++ )
    {
        Deg_PMT[i]        = Get_SIS_Triggers(run_numbers[i] , 30 , Dump_Numbers[i] , 31 , Dump_Numbers[i] , 2 );
        Downstream_PMT[i] = Get_SIS_Triggers(run_numbers[i] , 30 , Dump_Numbers[i] , 31 , Dump_Numbers[i] , 5 );
        Trap_PMT[i]       = Get_SIS_Triggers(run_numbers[i] , 30 , Dump_Numbers[i] , 31 , Dump_Numbers[i] , 3 );
        Silicon[i]       = Get_SIS_Triggers(run_numbers[i] , 30 , Dump_Numbers[i] , 31 , Dump_Numbers[i] , 51 );

        Trap_Total_Counts[i] = Trap_PMT[i]->Integral();

        Downstream_Total_Counts[i] = Downstream_PMT[i]->Integral();

        Deg_Total_Counts[i] = Deg_PMT[i]->Integral() ;

        Silicon_Total_Counts[i] = Silicon[i]->Integral() ;

        fprintf(myFile, "%d %d %d %d %d %f \n" , run_numbers[i] , Deg_Total_Counts[i] ,Trap_Total_Counts[i] ,Downstream_Total_Counts[i] , Silicon_Total_Counts[i] , Time_After_AD[i] );
    }
    fclose(outputFile);

    return 0 ;
}

TH1D* Get_SIS_Triggers( Int_t run_number, Int_t start_trigger_channel, Int_t start_trigger_repetition, Int_t end_trigger_channel, Int_t end_trigger_repetition, Double_t tmin , Double_t tmax, Int_t debug = 0 )
{
  Double_t start_time = Find_SIS_Channel_Trigger_Time( run_number, start_trigger_channel, start_trigger_repetition ) + tmin;
  Double_t end_time = Find_SIS_Channel_Trigger_Time( run_number, start_trigger_channel, start_trigger_repetition ) + tmax;

  TTree* DET_weed = Get_Sis_Det_Weed( run_number );
  TSisEvent* sisEvent = new TSisEvent();
  DET_weed->SetBranchAddress("SisEvent", &sisEvent );

  char plot_title[80];
  sprintf( plot_title, "Quench time(s)" );
  TH1D* the_plot = new TH1D( plot_title, plot_title, 10, 0, 0.001 );
  the_plot->GetXaxis()->SetTitle("time /s");
  the_plot->GetYaxis()->SetTitle("events");

  Int_t sum_counts(0);

  for ( int i = 0 ; i < DET_weed->GetEntries() ; i++ )
    {
      DET_weed->GetEntry(i) ;

      // selection cuts
      if( sisEvent->GetRunTime() < start_time || sisEvent->GetRunTime() > end_time) continue;
      //if( sisEvent->GetChannel() != 51 ) continue;
      //if( sisEvent->GetChannel() != 45 ) continue;
      if( sisEvent->GetChannel() != 48 ) continue;


      // ... fill histogram
      the_plot->Fill( sisEvent->GetRunTime()-start_time, sisEvent->GetCountsInChannel()  );
     // if ( debug!=0 ) printf( "...added event with time %f and counts %d \n", sisEvent->GetRunTime(), sisEvent->GetCountsInChannel() );

      sum_counts+=sisEvent->GetCountsInChannel();

    }//loop over vertex tree

 // printf("sum_counts = %d \n", sum_counts );

  return the_plot;
}

void Sum_Background_Triggers(const Int_t runs , Int_t* run_numbers , Int_t SisChannel , Double_t&* time_before_AD , Int_t&* Total_Counts , Int_t&* Dump_Numbers)
{
    Get_Background_Triggers(run_numbers[0] , SisChannel , time_before_AD[0] , Total_Counts[0] , Dump_Numbers[0]) ;

    for ( int i = 1 ; i < runs ; i++ )
    {
        Get_Background_Triggers(run_numbers[i] , SisChannel  ,time_before_AD[i] , Total_Counts[i], Dump_Numbers[i] ) ;
            }



}

Int_t Count_SIS_Triggers(Int_t run_number, Int_t SIS_Channel)
{
  TTree* SIS_Tree = Get_Sis_Tree( run_number, SIS_Channel );
  TSisEvent* sisEvent = new TSisEvent();
  SIS_Tree->SetBranchAddress("SisEvent", &sisEvent );

  Int_t sum_counts(0);

  for ( Int_t i = 0 ; i < SIS_Tree->GetEntries() ; i++ )
    {
      SIS_Tree->GetEntry(i) ;
      if( sisEvent->GetChannel() != SIS_Channel ) continue;

      sum_counts++;
    }

    return sum_counts;
}

Int_t Count_SIS(Int_t run_number, Int_t SIS_Channel, Double_t tmin=0.0, Double_t tmax=99999.)
{
    TTree* SIS_Tree = Get_Sis_Tree( run_number );
    TSisEvent* sisEvent = new TSisEvent();
    SIS_Tree->SetBranchAddress("SisEvent", &sisEvent );

    Int_t sum_counts(0);

    for ( Int_t i = 0 ; i < SIS_Tree->GetEntries() ; i++ )
    {
        SIS_Tree->GetEntry(i) ;
        if( sisEvent->GetRunTime() < tmin || sisEvent->GetRunTime() > tmax ) continue;
        if( sisEvent->GetChannel() !=    SIS_Channel ) continue;
            sum_counts+=sisEvent->GetCountsInChannel();
    }

    return sum_counts;
}

Int_t GetIntegralBetweenTriggers( Int_t run_number, Int_t SIS_channel, Int_t start_trigger_channel, Int_t start_trigger_repetition, Int_t end_trigger_channel, Int_t end_trigger_repetition )
{

  Int_t total(0);
  Int_t i = 0;
  Double_t start_time = Find_SIS_Channel_Trigger_Time( run_number, start_trigger_channel, start_trigger_repetition );
  Double_t end_time = Find_SIS_Channel_Trigger_Time( run_number, end_trigger_channel, 1 );

  if(end_time<start_time)
  {
      while(end_time<start_time && end_time!=999.)
      {
        i++;
        end_time = Find_SIS_Channel_Trigger_Time( run_number, end_trigger_channel, i );
      }
  }
  if(end_time == 999. || start_time == 999.)
  {
      printf("\nCould not resolve a dump for run %d\n",run_number);
      return -1;
  }

  TTree* detWeed = Get_Sis_Tree( run_number, SIS_channel );
  if( detWeed == NULL ) gROOT->Error();
  TSisEvent* detEvent = new TSisEvent();
  detWeed->SetBranchAddress("SisEvent", &detEvent );

  //loop over the detWeed
  for(i=0; i<detWeed->GetEntries(); i++ )
    {
      detWeed->GetEntry(i);
      if( detEvent->GetRunTime() < start_time ) continue;
      if( detEvent->GetRunTime() > end_time ) continue;
      //if( detEvent->GetChannel() != SIS_channel ) continue;

      total+=detEvent->GetCountsInChannel();
    }//loop over the detWeed

  return total;
}

Int_t Get_Integral_Between_Triggers_Fast( Int_t run_number, Int_t SIS_channel, Int_t start_trigger_channel, Int_t start_trigger_repetition, Int_t end_trigger_channel, Int_t end_trigger_repetition )
{

  Int_t total(0);
  Int_t subtotal(1);
  Int_t i(1);
  Double_t start_time = Find_SIS_Channel_Trigger_Time( run_number, start_trigger_channel, start_trigger_repetition );
  Double_t end_time = Find_SIS_Channel_Trigger_Time( run_number, end_trigger_channel, 1 );
  char formatstr[500];

  if(end_time<start_time)
  {
      while(end_time<start_time && end_time!=999.)
      {
        i++;
        end_time = Find_SIS_Channel_Trigger_Time( run_number, end_trigger_channel, i );
      }
  }
  if(end_time == 999. || start_time == 999.)
  {
      printf("\nCould not resolve a dump for run %d\n",run_number);
      return -1;
  }

  TTree* detWeed = Get_Sis_Tree( run_number, SIS_channel );
  if( detWeed == NULL )
    {
        printf("Could not open tree for run %d\n",run_number);
        return -1;
    }


    i=1;
    while(subtotal>0)
    {
        sprintf(formatstr,"RunTime>=%f && RunTime<=%f && Channel==%d && CountsInChannel>=%d",start_time,end_time,SIS_channel,i); //Make the format string
        subtotal = detWeed->GetEntries(formatstr);
        total+=subtotal;
        i++;
    }


  return total;
}

void Get_Background_Triggers(Int_t run_number, Int_t SisChannel , Double_t& Time_before_AD , Int_t& Total_Counts , Int_t& dump_Number)
{
    // Find background dump
    Double_t start_time[100] ;
    Double_t end_time[100]  ;
    Double_t dump_time = 0.00 ;
    Int_t dumps = 0 ;

    TTree* TS_weed = Get_Sis_TS_Weed( run_number );
    TSisEvent* sisTSEvent = new TSisEvent();
    TS_weed->SetBranchAddress("SisEvent", &sisTSEvent );

    Double_t AD_Time[1000]  ;
    Int_t spill = 0 ;

    for ( int i = 0 ; i < TS_weed->GetEntries() ; i++ )
    {
        TS_weed->GetEntry(i) ;

        if ( sisTSEvent->GetChannel() ==30 || sisTSEvent->GetChannel() == 31 )
        {
            if ( sisTSEvent->GetChannel() == 30 && sisTSEvent->GetCountsInChannel() > 0  )
            {
                start_time[dumps] = sisTSEvent->GetRunTime();
            }

            if( sisTSEvent->GetChannel() == 31 && sisTSEvent->GetCountsInChannel() > 0 )
            {
                end_time[dumps] = sisTSEvent->GetRunTime();
                dumps++ ;
            }
        }
        else continue ;


    }

    for ( i = 0 ; i < dumps-1 ; i++ )
    {/*
        printf( "Length of dump is %f \n" , end_time[i] - start_time[i] );*/

        if ( (Int_t)(end_time[i] - start_time[i] ) == 10 )
        {
            dumpNumber = i + 1 ;
            dump_time = end_time[i] - start_time[i] ;
        }
    }

    //Find first AD injection before the dump
    for ( int i = 0 ; i < TS_weed->GetEntries() ; i++ )
    {
        TS_weed->GetEntry(i) ;

        if ( sisTSEvent->GetChannel() == 1 && sisTSEvent->GetRunTime() < start_time[dumpNumber - 1] )
        {
            AD_Time[spill] = sisTSEvent->GetRunTime();
            spill ++;
        }

        if ( sisTSEvent->GetRunTime() > start_time[dumpNumber -1 ]) break ;
    }

    if ( spill!=0 )
    {

        Time_before_AD = start_time[dumpNumber-1] - AD_Time[spill - 1 ] ;
        printf( " For run %d found background : dump %d , length is %f \n AD injection is at %f , %f seconds before background \n" ,run_number , dumpNumber
                , dump_time , AD_Time[spill - 1]
                , Time_before_AD );
    }
    else
    {

        Time_before_AD = start_time[dumpNumber-1] - AD_Time[spill ] ;
        printf( " For run %d found background : dump %d , length is %f \n AD injection is at %f , %f seconds before background \n" ,run_number , dumpNumber
                , dump_time , AD_Time[spill ]
                        , Time_before_AD );

    }


}

void Get_Background_Triggers(Int_t run_number, Int_t SisChannel , Double_t& Time_before_AD , Int_t& Total_Counts , Int_t& dumpNumber, Double_t& dump_time)
{
    // Find background dump
    Double_t start_time[100] ;
    Double_t end_time[100]  ;
    Double_t dump_time = 0.00 ;
    Int_t dumps = 0 ;

    TTree* TS_weed = Get_Sis_TS_Weed( run_number );
    TSisEvent* sisTSEvent = new TSisEvent();
    TS_weed->SetBranchAddress("SisEvent", &sisTSEvent );

    Double_t AD_Time[1000]  ;
    Int_t spill = 0 ;

    for ( int i = 0 ; i < TS_weed->GetEntries() ; i++ )
    {
        TS_weed->GetEntry(i) ;

        if ( sisTSEvent->GetChannel() ==30 || sisTSEvent->GetChannel() == 31 )
        {
            if ( sisTSEvent->GetChannel() == 30 && sisTSEvent->GetCountsInChannel() > 0  )
            {
                start_time[dumps] = sisTSEvent->GetRunTime();
            }

            if( sisTSEvent->GetChannel() == 31 && sisTSEvent->GetCountsInChannel() > 0 )
            {
                end_time[dumps] = sisTSEvent->GetRunTime();
                dumps++ ;
            }
        }
        else continue ;


    }

    for ( i = 0 ; i < dumps-1 ; i++ )
    {/*
        printf( "Length of dump is %f \n" , end_time[i] - start_time[i] );*/

        if ( (Int_t)(end_time[i] - start_time[i] ) == 10 )
        {
            dumpNumber = i + 1 ;
            dump_time = end_time[i] - start_time[i] ;
        }
    }

    //Find first AD injection before the dump
    for ( int i = 0 ; i < TS_weed->GetEntries() ; i++ )
    {
        TS_weed->GetEntry(i) ;

        if ( sisTSEvent->GetChannel() == 1 && sisTSEvent->GetRunTime() < start_time[dumpNumber - 1] )
        {
            AD_Time[spill] = sisTSEvent->GetRunTime();
            spill ++;
        }

        if ( sisTSEvent->GetRunTime() > start_time[dumpNumber -1 ]) break ;
    }

    if ( spill!=0 )
    {

        Time_before_AD = start_time[dumpNumber-1] - AD_Time[spill - 1 ] ;
        printf( " For run %d found background : dump %d , length is %f \n AD injection is at %f , %f seconds before background \n" ,run_number , dumpNumber
                , dump_time , AD_Time[spill - 1]
                , Time_before_AD );
    }
    else
    {

        Time_before_AD = start_time[dumpNumber-1] - AD_Time[spill ] ;
        printf( " For run %d found background : dump %d , length is %f \n AD injection is at %f , %f seconds before background \n" ,run_number , dumpNumber
                , dump_time , AD_Time[spill ]
                        , Time_before_AD );

    }


}
// General Utilities =========================================================================================

Double_t Find_SIS_Channel_Trigger_Time( Int_t run_number, Int_t trigger_channel, Int_t trigger_repetition_number , Int_t debug = 0  )
{
  TTree* TS_weed = Get_Sis_Tree( run_number, trigger_channel );
  TSisEvent* sisTSEvent = new TSisEvent();
  TS_weed->SetBranchAddress("SisEvent", &sisTSEvent );

  Double_t trigger_time = 999.;
  Int_t count_trigger_repetitions = 1;

  for ( int i = 0 ; i < TS_weed->GetEntries() ; i++ )
    {
      TS_weed->GetEntry(i) ;

      if ( sisTSEvent->GetChannel() == trigger_channel && sisTSEvent->GetCountsInChannel()==1 /*&& sisTSEvent->GetRunTime()>30*/ )
        {
          if( trigger_repetition_number == count_trigger_repetitions )
          {
            trigger_time = sisTSEvent->GetRunTime();
            break;
          }
          count_trigger_repetitions++;
        }
    }

 if (trigger_time==999.) Warning("Find_SIS_Channel_Trigger_Time","\033[33mRun:%d; Trigger:%d:%d returning 999 (trigger not found)\033[00m",run_number, trigger_channel, trigger_repetition_number);
 return trigger_time;
}


Int_t Find_SIS_Channel_Trigger_LVCounter( Int_t run_number, Int_t trigger_channel, Int_t trigger_repetition_number , Int_t debug = 0  )
{
  TTree* TS_weed = Get_Sis_Tree( run_number, trigger_channel );
  TSisEvent* sisTSEvent = new TSisEvent();
  TS_weed->SetBranchAddress("SisEvent", &sisTSEvent );

  Int_t trigger_counter(0);
  Int_t count_trigger_repetitions = 1;

  for ( int i = 0 ; i < TS_weed->GetEntries() ; i++ )
    {
      TS_weed->GetEntry(i) ;

      if ( sisTSEvent->GetChannel() == trigger_channel && sisTSEvent->GetCountsInChannel()==1 /*&& sisTSEvent->GetRunTime()>30*/ )
        {
          if( trigger_repetition_number == count_trigger_repetitions )
          {
            trigger_counter = sisTSEvent->GetLabVIEWCounter();
            break;
          }
          count_trigger_repetitions++;
        }
    }
 return trigger_counter;
}

Int_t Find_SIS_Channel_Trigger_VertexCounter( Int_t run_number, Int_t trigger_channel, Int_t trigger_repetition_number , Int_t debug = 0  )
{
  TTree* TS_weed = Get_Sis_Tree( run_number, trigger_channel );
  TSisEvent* sisTSEvent = new TSisEvent();
  TS_weed->SetBranchAddress("SisEvent", &sisTSEvent );

  Int_t trigger_counter(0);
  Int_t count_trigger_repetitions = 1;

  for ( int i = 0 ; i < TS_weed->GetEntries() ; i++ )
    {
      TS_weed->GetEntry(i) ;

      if ( sisTSEvent->GetChannel() == trigger_channel && sisTSEvent->GetCountsInChannel()==1 /*&& sisTSEvent->GetRunTime()>30*/ )
        {
          if( trigger_repetition_number == count_trigger_repetitions )
          {
            trigger_counter = sisTSEvent->GetVertexCounter();
            break;
          }
          count_trigger_repetitions++;
        }
    }
 return trigger_counter;
}



Int_t Find_SIS_Channel_Trigger_Index( Int_t run_number, Int_t trigger_channel, Int_t trigger_repetition_number , Int_t debug = 0  )
{
  TTree* TS_weed = Get_Sis_Tree( run_number);
  TSisEvent* sisTSEvent = new TSisEvent();
  TS_weed->SetBranchAddress("SisEvent", &sisTSEvent );

  //Int_t trigger_counter(0);
  Int_t count_trigger_repetitions = 1;
  Int_t i;

  for ( i = 0 ; i < TS_weed->GetEntries() ; i++ )
    {
      TS_weed->GetEntry(i) ;

      if ( sisTSEvent->GetChannel() == trigger_channel && sisTSEvent->GetCountsInChannel()==1 /*&& sisTSEvent->GetRunTime()>30*/ )
        {
          if( trigger_repetition_number == count_trigger_repetitions )
          {
            //trigger_counter = sisTSEvent->GetLabVIEWCounter();
            break;
          }
          count_trigger_repetitions++;
        }
    }
 return i;
}


void MakeBlueBackground( TH2D* the_plot )
{
  // zphi background color
  Int_t numXbins = the_plot->GetXaxis()->GetNbins();
  Int_t numYbins = the_plot->GetYaxis()->GetNbins();

  for(Int_t x=0; x<=numXbins; x++ )
    {
      for(Int_t y=0; y<=numYbins; y++)
        {
          if( the_plot->GetBinContent( x, y ) == 0. ) the_plot->SetBinContent( x, y, 1e-7 );
        }
    }
}

void AddIntegralText( TH1D* the_plot )
{
  Int_t the_integral = (Int_t) the_plot->Integral();

  TString the_text("Integral = ");
  the_text+=the_integral;
  TPaveLabel* the_label = new TPaveLabel(0.74, 0.93, .98, .98, the_text , "NDC" );
  the_label->SetTextSize(0.45);
  the_label->Draw();
}

void AddIntegralText( TH1D* the_plot ,Int_t Color)
{
  Int_t the_integral = (Int_t) the_plot->Integral();

  TString the_text("Integral = ");
  the_text+=the_integral;
  TPaveLabel* the_label = new TPaveLabel(0.74, 0.93, .98, .98, the_text , "NDC" );
  the_label->SetTextSize(0.5);
  the_label->SetTextColor(Color + (Color>4) ) ;
  the_label->Draw("same");
}

void AddIntegralText( TH1D* the_plot ,Int_t runNumber )
{
  Int_t the_integral = (Int_t) the_plot->Integral();

  TString the_text("Run ");
  the_text += runNumber ;
  the_text += " Integral: " ;
  the_text +=  the_integral ;
  TPaveLabel* the_label = new TPaveLabel(0.74, 0.93, .98, .98, the_text , "NDC" );
  the_label->SetTextSize(0.5);
  the_label->Draw();
}

void AddIntegralText( TH1D* the_plot ,Int_t runNumber,Int_t Color,Int_t rep)
{
  Int_t the_integral = (Int_t) the_plot->Integral();

  TString the_text("Run ");
  the_text += runNumber ;
  the_text += " Integral: " ;
  the_text +=  the_integral ;

  TPaveLabel* the_label = new TPaveLabel(0.74 , 0.93 - 0.06*rep, .98, .98-0.06*rep, the_text , "NDC" );
  the_label->SetTextSize(0.5);
  the_label->SetTextColor(Color + (Color>4) ) ;
  the_label->Draw("same");
}

void AddRandomText( TH1D* the_plot, char* the_text, Int_t Color, Double_t textSize = 0.45, Int_t rep =0)
{
 TPaveLabel* the_label = new TPaveLabel(0.74 , 0.93 - 0.06*rep, .98, .98-0.06*rep, the_text , "NDC" );
  the_label->SetTextSize(textSize);
  the_label->SetTextColor(Color + (Color>4) ) ;
  the_label->Draw();
}

void AddRandomText(char* the_text, Int_t Color, Double_t textSize = 0.45, Int_t rep =0)
{
 TPaveLabel* the_label = new TPaveLabel(0.74 , 0.93 - 0.06*rep, .98, .98-0.06*rep, the_text , "NDC" );
  the_label->SetTextSize(textSize);
  the_label->SetTextColor(Color + (Color>4) ) ;
  the_label->Draw();
}

void AddRandomTextWidth(char* the_text, Int_t Color, Double_t textSize = 0.45, Double_t width=0.24, Int_t rep =0)
{
 TPaveLabel* the_label = new TPaveLabel(0.98-width , 0.93 - 0.06*rep, .98, .98-0.06*rep, the_text , "NDC" );
  the_label->SetTextSize(textSize);
  the_label->SetTextColor(Color + (Color>4) ) ;
  the_label->Draw();
}

void AddIntegralText( TH2D* the_plot )
{
  Int_t the_integral = (Int_t) the_plot->Integral();

  TString the_text("Integral = ");
  the_text+=the_integral;
  // TPaveLabel* the_label = new TPaveLabel(0.54, 0.74, .78, .78, the_text , "NDC" );
  TPaveLabel* the_label = new TPaveLabel(0.74, 0.93, .98, .98, the_text , "NDC" );
  the_label->SetTextSize(0.45);
  the_label->Draw();
}

TTree* Get_Vtx_Tree( Int_t run_number )
{
  //load file
  TFile* root_file = Get_File( run_number );

  TTree* vtx_tree = NULL;
  vtx_tree = (TTree*) root_file->Get("gSiliconTree");
  if( vtx_tree==NULL ){
    Error("Get_Vtx_Tree","\033[31mVertex Tree for run number %d not found\033[00m", run_number);
    //gSystem->Exit(1);
    vtx_tree->GetName(); // This is to crash the CINT interface  instead of exiting (deliberately)
  }
  return vtx_tree;
}

TTree* Get_TTC_Tree( Int_t run_number )
{
  //load file
  TFile* root_file = Get_File( run_number );

  TTree* ttc_tree = NULL;
  ttc_tree = (TTree*) root_file->Get("TTC Tree");
  if( ttc_tree==NULL ){
    Error("Get_TTC_Tree","\033[31mTTC Tree for run number %d not found\033[00m", run_number);
    //gSystem->Exit(1);
    ttc_tree ->GetName(); // This is to crash the CINT interface  instead of exiting (deliberately)
  }
  return ttc_tree;
}

TTree* Get_Sis_Tree( Int_t run_number )
{
  //load file
  TFile* root_file = Get_File( run_number );

  TTree* sis_tree = NULL;
  sis_tree = (TTree*) root_file->Get("Sis Tree");
  if( sis_tree==NULL ){
    Error("Get_Sis_Tree","\033[31mSIS Tree for run number %d not found\033[00m", run_number);
    //gSystem->Exit(1);
    sis_tree->GetName(); // This is to crash the CINT interface  instead of exiting (deliberately)
  }

  return sis_tree;
}

TTree* Get_Sis_Tree(Int_t run_number, Int_t channel)
{
    TFile* theFile = Get_File(run_number);

    TString treeName = "gSisTree";
    if(channel<10) treeName+="0";
    treeName+=channel;
    printf("SIS tree data name %s\n", treeName.Data());

    TTree* theTree = (TTree*)theFile->Get(treeName.Data());

    if(theTree==NULL)
    {
        Error("Get_Sis_Tree","\033[31mCould not get a tree named %s for run %d\033[00m", treeName.Data(), run_number);
        theTree->GetName(); // This is to crash the CINT interface  instead of exiting (deliberately)
        return NULL;
    }

    return theTree;

}

TTree* Get_Sis_TS_Weed( Int_t run_number )
{
  //load file
  TFile* root_file = Get_File( run_number );


  TTree* sis_tree = NULL;
  sis_tree = (TTree*) root_file->Get("SisTSTrigger Weed");
  if( sis_tree==NULL ){
    Error("Get_Sis_TS_Weed","\033[31mSIS TS Weed for run number %d not found\033[00m", run_number);
    //gSystem->Exit(1);
    sis_tree->GetName();// This is to crash the CINT interface  instead of exiting (deliberately)
  }
  return sis_tree;
}

TTree* Get_Sis_Det_Weed( Int_t run_number )
{
  //load file
  TFile* root_file = Get_File( run_number );

  TTree* sis_tree = NULL;
  sis_tree = (TTree*) root_file->Get("SisDetTrigger Weed");
  if( sis_tree==NULL ){
    Error("Get_Sis_Det_Weed","\033[31mSIS Det Weed for run number %d not found\033[00m", run_number);
    //gSystem->Exit(1);
    sis_tree->GetName();// This is to crash the CINT interface  instead of exiting (deliberately)
  }

  return sis_tree;
}

TTree* Get_LabVIEW_Tree( Int_t run_number )
{
  //load file
  TFile* root_file = Get_File( run_number );

  TTree* labview_tree = NULL;
  labview_tree = (TTree*) root_file->Get("gLabVIEWTree");
  if( labview_tree==NULL ){
    Error("Get_LabVIEW_Tree","\033[31mLabview tree for run number %d not found\033[00m", run_number);
    //gSystem->Exit(1);
    labview_tree->GetName(); // This is to crash the CINT interface  instead of exiting (deliberately)
  }

  return labview_tree;
}

TTree* GetTreeByChannel(Int_t run_number, Int_t DetectorChannel)
{
    return Get_Sis_Tree(run_number, DetectorChannel);
}

TFile* Get_File(Int_t run_number)
{
  TFile * f = NULL;

  TString file_name("/home/alpha/alphaSoftware2012/alphaAnalysis/data/tree");
  if (run_number<10000) file_name+="0";
  file_name+=run_number;
  file_name+=("offline.root");

  f = (TFile*)gROOT->GetListOfFiles()->FindObject(file_name);

  if(f==NULL)
    {
      f = new TFile( file_name.Data(), "READ" );
       printf("Successfully loaded %s \n", file_name.Data() );
    }

  f->SetBit(kMustCleanup);

  if(!f->IsOpen())
    {
      Error("Get_File", "\033[33mCould not open tree file for run %d\033[00m", run_number);
      //gSystem->Exit(1);
      TObject* dum=NULL;
      dum->GetName(); // This is to crash the CINT interface  instead of exiting (deliberately)
    }

  return f;
}

TH1D* Get_TH1D( TDirectory* dir, char* name, int nbinsx=10, double xlow=0., double xup=10. )
{
  TH1D* the_Hist = NULL;
  if (dir) the_Hist = (TH1D*) dir->Get(name);

  if (!the_Hist)
    {
      the_Hist = new TH1D(name, name, nbinsx, xlow, xup);
      if (dir) dir->Add(the_Hist);
    }

  return the_Hist;
}

TH2D* Get_TH2D( TDirectory* dir, char* name, int nbinsx, double xlow, double xup, int nbinsy, double ylow, double yup, Bool_t new_plot )
{
  new_plot = false;

  TH2D* the_Hist = NULL;
  if (dir) the_Hist = (TH2D*) dir->Get(name);

  if (!the_Hist)
    {
      the_Hist = new TH2D(name, name, nbinsx, xlow, xup, nbinsy, ylow, yup);
      if (dir) dir->Add(the_Hist);
      new_plot = true;
    }

  return the_Hist;
}

TDirectory* Get_Directory( char* name, TFile* location )
{
  TDirectory* dir = NULL;

  if (location)
    dir = (TDirectory*)location->Get( name );

  //  location->ls();
  if (location && !dir)
    dir = location->mkdir( name );

  return dir;
}

TDirectory* Get_Sub_Directory( char* sub_name, char* dir_name, TFile* location )
{
  TDirectory* dir = Get_Directory( dir_name, location );

  TDirectory* sub_dir = NULL;

  if (location)
    sub_dir = (TDirectory*)dir->Get( sub_name );

  //  location->ls();
  if (location && !sub_dir)
    sub_dir = dir->mkdir( sub_name );

  return sub_dir;
}

Int_t Close_Open_Files()
{
    //Closees any open files whose paths match a pattern. Good if we've lost the pointer to a file
    //EB 12/2008

    //Note - this will kill any open histograms that were generated from that file

    TSeqCollection* file_list = gROOT->GetListOfFiles();
    char pattern[20] = "offline.root"; // This is the substring we'll look for. Only data files should match it
    Int_t j = file_list->GetEntries();

    if(j>0)
    {
        for (Int_t i = j-1; i>=0; i--)
        {
            if(strstr(file_list->At(i)->GetName(),pattern)==NULL) continue;

            file_list->At(i)->Execute("Close","");
            j--;
        }
    }

    return j;
}

Int_t Close_File(Int_t runNumber)
{
    char filename[50];
    TSeqCollection* fileList = gROOT->GetListOfFiles();
    sprintf(filename, "tree%05doffline.root", runNumber);

    TString matchName;

    for (Int_t i=0; i<fileList->GetEntries(); i++)
    {
        matchName=fileList->At(i)->GetName();
        if(matchName.Contains(filename)) ((TFile*)(fileList->At(i)))->Close();
    }

    return 0;
}

void set_plot_style_fire()
{
    const Int_t NRGBs = 4;
    const Int_t NCont = 255;

    Double_t stops[NRGBs] = {0,0.33,0.66,1}; //{ 0.00, 0.34, 0.61, 0.84, 1.00 };
    Double_t red[NRGBs]   = {0,1,1,1};//{ 0.00, 0.00, 0.87, 1.00, 0.51 };
    Double_t green[NRGBs] = {0,0,1,1};//{ 0.00, 0.81, 1.00, 0.20, 0.00 };
    Double_t blue[NRGBs]  = {0,0,0,1};//{ 0.51, 1.00, 0.12, 0.00, 0.00 };
    TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
    gStyle->SetNumberContours(NCont);
}

TTree *GetAlphaStripTree(Int_t runNumber){
  TFile* root_file = Get_File(runNumber);
  TTree* alphaStrip_tree = NULL;
  alphaStrip_tree = (TTree*) root_file->Get("Strip Tree");
  if(alphaStrip_tree==NULL ){
    Error("getAlphaStripTree","\033[31mStrip tree for run number %d not found\033[00m", runNumber);
    //gSystem->Exit(1);
    alphaStrip_tree->GetName(); // This is to crash the CINT interface  instead of exiting (deliberately)
  }
  return alphaStrip_tree;
}

Int_t GetAlphaStripRun(Int_t runNumber){
  TTree* alphaStripTree = GetAlphaStripTree(runNumber);
  Int_t stripRunNumber;
  alphaStripTree->SetBranchAddress("stripRun", &stripRunNumber);
  alphaStripTree->GetEntry(0);
  return stripRunNumber;
}

TString GetAlphaStripFileName(Int_t runNumber){
  TTree* alphaStripTree = GetAlphaStripTree(runNumber);
  TObjString *stripFileName;
  TString fileName;
  alphaStripTree->SetBranchAddress("stripRunFile", &stripFileName);
  alphaStripTree->GetEntry(0);
  fileName = stripFileName->GetString();
  return fileName;
}

Int_t FileToRuns(char* filename, Int_t* runNumber=NULL, Int_t* dumps=NULL, TObjArray* legendText=NULL)
{
        TString* TSfilename = new TString(filename);
        if(TSfilename->Contains(".filelist"))
        {
            Info("FileToRuns","processing file %s as a filelist", filename);

            TObjArray* filelistnames = new TObjArray();
            Int_t nFiles =  FileToSubFiles(filename, filelistnames);

            Int_t nTotalRuns(0),  nRuns;
            for(Int_t i=0; i<nFiles; i++)
            {
                nRuns = FileToRuns(((TString*)filelistnames->At(i))->Data(), runNumber, dumps);

                nTotalRuns+=nRuns;
                if(runNumber)runNumber=runNumber+nRuns;
                if(dumps)dumps=dumps+nRuns;
            }

            CheckForDuplicates(nTotalRuns, runNumber-nTotalRuns);
            Info("FileToRuns", "Read %d runs from %s", i, filename);
            return nTotalRuns;
        }

    Int_t i(0), nRead;
    TString* label;

    FILE* myFile = fopen(filename, "r");
    if(myFile==NULL)
        {
                Error("PlotVertices", "\033[31mCould not open text file %s for reading\033[00m", filename);
                //gSystem->Exit(1);
                TObject* dum=NULL;
                dum->GetName();// This is to crash the CINT interface  instead of exiting (deliberately)
        }

    char textLine[100];

    Int_t run, dump;

    while(true)
    {
        fgets(textLine, 100, myFile);
        nRead = sscanf(textLine, "%d %d %s", &run, &dump, text);
        if(runNumber) runNumber[i] = run;
        if(dumps) dumps[i] = dumps;
        if(nRead<3 && runNumber) sprintf(text, "%d", runNumber[i]);
        if(nRead<2 && dumps) dumps[i]=0;
        label = new TString(text);
        if(legendText)legendText->Add((TObject*)label);

        i++;
        if(feof(myFile)) break;
    }
    i--;

    Info("FileToRuns", "Read %d runs from %s", i, filename);

    fclose(myFile);
    CheckForDuplicates(i, runNumber);
    return i;
}

Int_t FileToSubFiles(char* filename, TObjArray* subFilenames, TObjArray* legendText==NULL)
{
        FILE* myFile = fopen(filename, "r");

        if(myFile==NULL)
        {
                Error("FileToSubFiles", "\033[31mCould not open file %s for reading\033[00m", filename);
                //gSystem->Exit(1);
                TObject* dum=NULL;
                dum->GetName();// This is to crash the CINT interface  instead of exiting (deliberately)
        }

        char textLine[500];
        char thisFilename[500];
        char thisLabel[500];
        Int_t i(0), nRead;
        TString* myString;

        while(!feof(myFile))
        {
            fgets(textLine, 500, myFile);
            nRead = sscanf(textLine, "%s %s", thisFilename, thisLabel);
            myString = new TString(thisFilename);
            subFilenames->Add((TObject*)myString);
            if(legendText==NULL)
            {
            }
            else
            {
                if(nRead>1) myString = new TString(thisLabel);
                legendText->Add((TObject*)myString);
            }

            i++;
        }
        i--;

        Info("FileToSubFiles", "Read %d files from %s", i, filename);

        fclose(myFile);

        return i;

}


Int_t CheckFile(char* filename)
{
    Int_t runs[10000], dump[10000];
    Int_t nFiles = FileToRuns(filename, runs, dump);

    for(Int_t i=0; i<nFiles; i++)
    {
        if(!CheckFile(runs[i])) Error("", "\033[31mTree file for run %d doesn't exist\033[00m", runs[i]);
        Close_File(runs[i]);
    }
}

Int_t CheckFile(Int_t run_number)
{

  TFile * f = NULL;

  TString file_name("/home/alpha/alphaSoftware2012/alphaAnalysis/data/tree");
  if (run_number<10000) file_name+="0";
  file_name+=run_number;
  file_name+=("offline.root");

  f = (TFile*)gROOT->GetListOfFiles()->FindObject(file_name);

  if(f==NULL)
  {
    f = new TFile( file_name );

     if(!f->IsOpen())
        {
          TString file_name("/home/alpha/alphaSoftware2012/alphaAnalysis/data/tree");
          if (run_number<10000) file_name+="0";
          file_name+=run_number;
          //  file_name+=("offline.root");
          file_name+=(".root");
          f = new TFile( file_name );
        }
  }

  f->SetBit(kMustCleanup);

  Int_t retVal;
  if(!f->IsOpen()) retVal=0;
  else retVal=1;

  //f->Close();

  return retVal;
}

Bool_t CheckForDuplicates(Int_t nRuns, Int_t* runs)
{
    Bool_t check(kFALSE);

    for(Int_t i=0; i<nRuns; i++)
    {
        for(Int_t j=i+1; j<nRuns; j++)
        {
            if(runs[i]==runs[j])
            {
                Warning("CheckForDuplicates", "\033[33mrun %d is duplicated\033[00m", runs[i]);
                check = kTRUE;
            }
        }
    }
    return check;
}

