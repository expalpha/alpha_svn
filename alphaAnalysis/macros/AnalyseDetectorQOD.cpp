// ====================================================================
//
// alphaAnalysis macros for the extraction of detector signal gain
//
// JWS : 25th May 2010
//
// ====================================================================

#include "AlphaAnalysisUtilities.cpp"

void Make_Gain_v_Hold_Time( )
{
  gROOT->Reset();

  TMultiGraph* gain_hold_mgraph = new TMultiGraph();
  gain_hold_mgraph->SetTitle("Detector gain");

  TGraphErrors* p_gain_graph = Get_Gain_v_Hold_Time( 1 );
  TGraphErrors* n_gain_graph = Get_Gain_v_Hold_Time( 0 );

  gain_hold_mgraph->Add( p_gain_graph, "p" );
  gain_hold_mgraph->Add( n_gain_graph, "p" );

  TCanvas* canvas = new TCanvas("canvas", "canvas", 800, 600);

  TPad* graphPad = new TPad("graphPad", "graphPad",0.,0., 0.8, 1.); // xlow, ylow, xup, yup
  graphPad->Draw();
  graphPad->cd();
  gain_hold_mgraph->Draw("A");
  graphPad->Update();
  gain_hold_mgraph->GetHistogram()->SetXTitle("hold time / ns"); 
  graphPad->Update();

  canvas->cd();
  TPad* legendPad = new TPad("legend", "legend", 0.8, 0., 1., 1.);
  legendPad->Draw();
  legendPad->cd();

  TLegend* legend = new TLegend(0.,0.6,1.,1.);
  legend->AddEntry( p_gain_graph, "p-side", "p");
  legend->AddEntry( n_gain_graph, "n-side", "p");
  legend->SetTextSize(0.10);
  legend->SetBorderSize(0);
  legend->Draw();
}

TGraphErrors* Get_Gain_v_Hold_Time( Int_t p_side == 1 )
{
  const Int_t num_runs = 9;
  Int_t run_numbers[ num_runs ] = {16461, 16483, 16485, 16489, 16490, 16507, 16412, 16522, 16616 };
  Double_t hold_delay[ num_runs ] = {240, 180, 360, 420, 480, 120, 300, 60, 720 };

  Double_t gain_means[ num_runs ];
  Double_t gain_rmss[ num_runs ];

  Double_t gain_mean(0.);
  Double_t gain_rms(0.);
  Double_t hold_delay_errors[ num_runs ];

  Int_t rc(0);

  for( Int_t i=0; i<num_runs; i++ )
    {
      rc = Get_Detector_Gain_Mean_RMS( run_numbers[i], p_side, gain_mean, gain_rms );
      if( rc!= 0 ) exit(0);
      gain_means[ i ] = gain_mean;
      gain_rmss[ i ] = gain_rms;         
      hold_delay_errors[ i ] = 0.;
    }

  TGraphErrors* gr = new TGraphErrors( num_runs, hold_delay, gain_means, hold_delay_errors, gain_rmss );
  gr->SetTitle("Detector gain");
  gr->GetXaxis()->SetTitle("Hold delay / ns");
  gr->SetMarkerStyle(21);
  if( p_side == 1 ) gr->SetMarkerColor(2);
  else gr->SetMarkerColor(4);
  
  return gr;
}


Int_t Get_Detector_Gain_Mean_RMS( Int_t run_number, Int_t p_side == 1, Double_t & gain_mean, Double_t & gain_rms )
{
  TH1D* detector_gain = Get_Detector_SignalGains( run_number, p_side );
  if( detector_gain == NULL)
    {
      printf("Failed to retrieve TH1D* from Get_Detector_SignalsGains...");
      return -1;
    }
  
 TF1* gaus = new TF1("gaus","gaus",0.,50.); 
 detector_gain->Fit("gaus","","",0.,50.);

 gain_mean = gaus->GetParameter(1);
 gain_rms = gaus->GetParameter(2);

 return 0;
}


TH1D* Get_Detector_SignalMeans( Int_t run_number, Int_t p_side == 1 )
{
  TH1D* hist = new TH1D("signalmeans","signalmeans",100,-0.5,511.5);

  for(Int_t isil=1; isil<=60; isil++)
    {
      if( isil==10 ) continue;

      if( p_side==1 ) 
        {
          hist->Fill( fabs( CalculateSignalMean( run_number, isil, 3 ) ) );
          hist->Fill( fabs( CalculateSignalMean( run_number, isil, 4 ) ) );
        }
      else
        {
          hist->Fill( fabs( CalculateSignalMean( run_number, isil, 1 ) ) );
          hist->Fill( fabs( CalculateSignalMean( run_number, isil, 2 ) ) );
        }
    }

  return hist;
}

TH1D* Get_Detector_SignalGains( Int_t run_number, Int_t p_side = 1 )
{
  TH1D* hist = new TH1D("signalmeans","signalmeans",50,0,50);

  for(Int_t isil=1; isil<=60; isil++)
    {
      if( isil==10 ) continue;

      if( p_side==1 ) 
        {
          hist->Fill( fabs( CalculateSignalGain( run_number, isil, 3 ) ) );
          hist->Fill( fabs( CalculateSignalGain( run_number, isil, 4 ) ) );
        }
      else
        {
          hist->Fill( fabs( CalculateSignalGain( run_number, isil, 1 ) ) );
          hist->Fill( fabs( CalculateSignalGain( run_number, isil, 2 ) ) );
        }
    }

  return hist;
}


void Plot_Module_SignalMeans( Int_t run_number, Int_t p_side == 1 )
{
  TCanvas* c_signalMeans = new TCanvas( "SignalMeans","SignalMeans",600,400);
  c_signalMeans->cd();
  c_signalMeans->SetLogy(0);

  TGraph* graph = Get_Module_SignalMeans( run_number, p_side );
  graph->SetMarkerStyle(2);
  if( p_side==1 ) graph->SetMarkerColor(2);
  else graph->SetMarkerColor(4);
  graph->Draw("AP");
}

TGraph* Get_Module_SignalMeans( Int_t run_number, Int_t p_side == 1 )
{
  Double_t signal_means[120];
  Double_t asic_numbers[120];
  Int_t index(0);
  
  Int_t asic_i, asic_j;
  if( p_side == 1 )
    {
      asic_i=3;
      asic_j=4;
    }
  else
    {
      asic_i=1;
      asic_j=2;
    }
    
  for( Int_t isil=1; isil<=60; isil++ )
    {
      if( isil==10 ) continue;

      // asic 1
      asic_numbers[ index ] = isil;
      signal_means[ index ] = fabs( CalculateSignalMean( run_number, isil, asic_i ) );
      index++;
      printf("%f \t %f \n", asic_numbers[ index ], signal_means[ index ] );   

      // asic 2
      asic_numbers[ index ]  = isil + 0.5;
      signal_means[ index ] = fabs( CalculateSignalMean( run_number, isil, asic_j ) );   
      index++;
      printf("%f \t %f \n", asic_numbers[ index ], signal_means[ index ] );   
    }
      
  TGraph* gr = new TGraph( index, asic_numbers, signal_means );

  return gr;
}

Double_t CalculateSignalMean( Int_t run_number, Int_t module_number, Int_t asic_number )
{
  Double_t signal_mean(-999.);

  // load ADC spectrum 
  TFile* f = Get_File( run_number );
  TDirectory* dir = Get_Sub_Directory( "ADC", "QOD", f );
  // dir->ls();

  char name[256];
  sprintf( name, "Module# %d ASIC# %d : ADC Spectrum", module_number, asic_number );

  TH1D* hist = Get_TH1D( dir, name );

//   TCanvas* c1 = new TCanvas("c1","c1");
//   c1->SetLogy();
//   hist->Draw();
  
  // fit pedestal
  TF1* gaus = new TF1("gaus","gaus",-50.,50.); 
  hist->Fit("gaus","","",-10.,10.);
  Double_t ped_mean = gaus->GetParameter(1);
  Double_t ped_sigma = gaus->GetParameter(2);

  // n-side : ASIC 1 & 2 (VF48 polarity bug -> +ve signal)
  // p-side : ASIC 3 & 4 (VF48 polarity bug -> -ve signal)

  Double_t threshold;
  Double_t num_sigma=5.;
  if( asic_number==1 || asic_number==2 ) threshold = ped_mean + num_sigma*ped_sigma;
  if( asic_number==3 || asic_number==4 ) threshold = ped_mean - num_sigma*ped_sigma;

  printf("mean = %f \t sigma = %f \t threshold = %f \n", ped_mean, ped_sigma, threshold ); 

  Double_t Sum0(0.);
  Double_t Sum1(0.);

  // integrate ADC spectra 
  for( Int_t i=0; i<hist->GetNbinsX(); i++ )
    {
      Double_t adc = hist->GetBinCenter(i);
      if( (asic_number==1 || asic_number==2 ) && adc < threshold ) continue;
      if( (asic_number==3 || asic_number==4 ) && adc > threshold ) continue;
      
      Sum0 += hist->GetBinContent(i);
      Sum1 += hist->GetBinContent(i)*adc; 
    }

  signal_mean = Sum1/Sum0;

  return signal_mean;
}

void Plot_Module_SignalGains( Int_t run_number, Int_t p_side == 1 )
{
  TCanvas* c_signalGains = new TCanvas( "SignalGains","SignalGains",600,400);
  c_signalGains->cd();
  c_signalGains->SetLogy(0);

  TGraph* graph = Get_Module_SignalGains( run_number, p_side );
  graph->SetMarkerStyle(2);
  if( p_side==1 ) graph->SetMarkerColor(2);
  else graph->SetMarkerColor(4);
  graph->Draw("AP");
}

TGraph* Get_Module_SignalGains( Int_t run_number, Int_t p_side == 1 )
{
  Double_t signal_gains[120];
  Double_t asic_numbers[120];
  Int_t index(0);
  
  Int_t asic_i, asic_j;
  if( p_side == 1 )
    {
      asic_i=3;
      asic_j=4;
    }
  else
    {
      asic_i=1;
      asic_j=2;
    }
    
  for( Int_t isil=1; isil<=60; isil++ )
    {
      if( isil==10 ) continue;

      // asic 1
      asic_numbers[ index ] = isil;
      signal_gains[ index ] = fabs( CalculateSignalGain( run_number, isil, asic_i ) );
      index++;
      printf("%f \t %f \n", asic_numbers[ index ], signal_gains[ index ] );   

      // asic 2
      asic_numbers[ index ]  = isil + 0.5;
      signal_gains[ index ] = fabs( CalculateSignalGain( run_number, isil, asic_j ) );   
      index++;
      printf("%f \t %f \n", asic_numbers[ index ], signal_gains[ index ] );   
    }
      
  TGraph* gr = new TGraph( index, asic_numbers, signal_gains );

  return gr;
}

Double_t CalculateSignalGain( Int_t run_number, Int_t module_number, Int_t asic_number )
{
  Double_t signal_mean(-999.);

  // load ADC spectrum 
  TFile* f = Get_File( run_number );
  TDirectory* dir = Get_Sub_Directory( "ADC", "QOD", f );
  // dir->ls();

  char name[256];
  sprintf( name, "Module# %d ASIC# %d : ADC Spectrum", module_number, asic_number );

  TH1D* hist = Get_TH1D( dir, name );

//   TCanvas* c1 = new TCanvas("c1","c1");
//   c1->SetLogy();
//   hist->Draw();
  
  // fit pedestal
  TF1* gaus = new TF1("gaus","gaus",-50.,50.); 
  hist->Fit("gaus","","",-10.,10.);
  Double_t ped_mean = gaus->GetParameter(1);
  Double_t ped_sigma = gaus->GetParameter(2);

  // n-side : ASIC 1 & 2 (VF48 polarity bug -> +ve signal)
  // p-side : ASIC 3 & 4 (VF48 polarity bug -> -ve signal)

  Double_t threshold;
  Double_t num_sigma=5.;
  if( asic_number==1 || asic_number==2 ) threshold = ped_mean + num_sigma*ped_sigma;
  if( asic_number==3 || asic_number==4 ) threshold = ped_mean - num_sigma*ped_sigma;

  printf("mean = %f \t sigma = %f \t threshold = %f \n", ped_mean, ped_sigma, threshold ); 

  Double_t Sum0(0.);
  Double_t Sum1(0.);

  // integrate ADC spectra 
  for( Int_t i=0; i<hist->GetNbinsX(); i++ )
    {
      Double_t adc = hist->GetBinCenter(i);
      if( (asic_number==1 || asic_number==2 ) && adc < threshold ) continue;
      if( (asic_number==3 || asic_number==4 ) && adc > threshold ) continue;
      
      Sum0 += hist->GetBinContent(i);
      Sum1 += hist->GetBinContent(i)*adc; 
    }

  signal_mean = Sum1/Sum0;

  // close the files
  f->Close();

  return signal_mean/ped_sigma;
}

//////////////////////////////////////////////////////////////////////////////////////
// Strip Gain
//////////////////////////////////////////////////////////////////////////////////////

Double_t CalculateStripGain( Int_t run_number, Int_t asic_number, Int_t strip_number )
{
  Double_t signal_mean(-999.);

  // load ADC spectrum 
  TFile* f = Get_File( run_number );
  TDirectory* dir = Get_Sub_Directory( "ADC", "QOD", f );
  // dir->ls();

  char name[256];
  sprintf( name, "ASIC# %d Strip %d : ADC Spectrum", asic_number, strip_number );

  TH1D* hist = Get_TH1D( dir, name );

//   TCanvas* c1 = new TCanvas("c1","c1");
//   c1->SetLogy();
//   hist->Draw();
  
  // fit pedestal
  TF1* gaus = new TF1("gaus","gaus",-50.,50.); 
  hist->Fit("gaus","","",-10.,10.);
  Double_t ped_mean = gaus->GetParameter(1);
  Double_t ped_sigma = gaus->GetParameter(2);

  // n-side : ASIC 1 & 2 (VF48 polarity bug -> +ve signal)
  // p-side : ASIC 3 & 4 (VF48 polarity bug -> -ve signal)

  Double_t threshold;
  Double_t num_sigma=5.;
  if( asic_number==1 || asic_number==2 ) threshold = ped_mean + num_sigma*ped_sigma;
  if( asic_number==3 || asic_number==4 ) threshold = ped_mean - num_sigma*ped_sigma;

  printf("mean = %f \t sigma = %f \t threshold = %f \n", ped_mean, ped_sigma, threshold ); 

  Double_t Sum0(0.);
  Double_t Sum1(0.);

  // integrate ADC spectra 
  for( Int_t i=0; i<hist->GetNbinsX(); i++ )
    {
      Double_t adc = hist->GetBinCenter(i);
      if( (asic_number==1 || asic_number==2 ) && adc < threshold ) continue;
      if( (asic_number==3 || asic_number==4 ) && adc > threshold ) continue;
      
      Sum0 += hist->GetBinContent(i);
      Sum1 += hist->GetBinContent(i)*adc; 
    }

  signal_mean = Sum1/Sum0;

  // close the files
  f->Close();

  return signal_mean/ped_sigma;
}

TGraph* Get_StripGains( Int_t run_number, Int_t asic_number )
{
  Double_t gain[128];
  Double_t strip[128]; 

  for(Int_t s=0; s<128; s++ )
    {
      gain[s] = fabs( CalculateStripGain( run_number, asic_number, s ) );
      strip[s] = (Double_t) s + 1.; //set strip numbering from 1,..,128
    }

  TGraph* graph = new TGraph( 128, strip, gain );
  graph->SetMarkerStyle(2);
  if( asic_number==1 || asic_number==2 ) graph->SetMarkerColor(2);
  else graph->SetMarkerColor(4);
  graph->GetXaxis()->SetRangeUser(0.,130.);
  graph->GetXaxis()->SetTitle("strip number");
  graph->GetYaxis()->SetRangeUser(0.,25.);
  char title[256];
  sprintf( title, "Strip gain : Run Number %d ASIC %d ", run_number, asic_number );
  graph->SetTitle(title);

  return graph;
}

void Plot_StripGains( Int_t run_number )
{
  TCanvas* c_stripGains = new TCanvas( "StripGains","StripGains",1200,800);
  c_stripGains->cd();
  c_stripGains->SetLogy(0);
  c_stripGains->Divide(2,2);
  c_stripGains->cd(1);

  // asic 1 (n-side)
  TGraph* asic1 = Get_StripGains( run_number, 1 );
  asic1->Draw("AP");

  c_stripGains->cd(2);
  // asic 2 (n-side)
  TGraph* asic2 = Get_StripGains( run_number, 2 );
  asic2->Draw("AP");

  c_stripGains->cd(3);
  // asic 3 (p-side)
  TGraph* asic3 = Get_StripGains( run_number, 3 );
  asic3->Draw("AP");

  c_stripGains->cd(4);
  // asic 4 (p-side)
  TGraph* asic4 = Get_StripGains( run_number, 4 );
  asic4->Draw("AP");

}

//////////////////////////////////////////////////////////////////////////////////////
// Occupancy
//////////////////////////////////////////////////////////////////////////////////////

TH1D* Get_N_Side_Occupancy( Int_t run_number )
{
  TCanvas* c_occ = new TCanvas( "Occupancies","Occupancies",1000,600);
  
  TH1D* occ = new TH1D( "n-side strip occupancy","n-side strip occupancy", 512, 0.5, 512.5 ); 
  occ->GetXaxis()->SetTitle("strip number");

  // n-side ( short strips )
  
  // AD-end
  TH1D* ASIC_1_AD = Get_Occupancy( run_number, 1, kTRUE );
  TH1D* ASIC_2_AD = Get_Occupancy( run_number, 2, kTRUE );
  // e+-end
  TH1D* ASIC_1_pos = Get_Occupancy( run_number, 1, kFALSE );
  TH1D* ASIC_2_pos = Get_Occupancy( run_number, 2, kFALSE );
  
  for( Int_t i=1; i<=128; i++ )
    {
      occ->Fill( i, ASIC_2_AD->GetBinContent( i ) );
      occ->Fill( i+128, ASIC_1_AD->GetBinContent( i ) );
      occ->Fill( i+256, ASIC_1_pos->GetBinContent( 129-i ) );
      occ->Fill( i+384, ASIC_2_pos->GetBinContent( 129-i ) );
    }
  
  return occ;
}

TH1D* Get_P_Side_Occupancy( Int_t run_number, Bool_t AD_end = kTRUE )
{
  TCanvas* c_occ = new TCanvas( "Occupancies","Occupancies",1000,600);

  Int_t offset(0);
  if(!AD_end) offset+=256;
  
  TH1D* occ = new TH1D( "p-side strip occupancy","p-side strip occupancy", 256, 0.5+offset, 256.5+offset ); 
  occ->GetXaxis()->SetTitle("strip number");

  // p-side ( long strips )
  
  TH1D* ASIC_3 = Get_Occupancy( run_number, 3, AD_end );
  TH1D* ASIC_4 = Get_Occupancy( run_number, 4, AD_end );

  for( Int_t i=1; i<=128; i++ )
    {
      occ->Fill( i+offset, ASIC_3->GetBinContent( i ) );
      occ->Fill( i+128+offset, ASIC_4->GetBinContent( 129-i ) );
    }

  return occ;
}


TH1D* Get_Occupancy( Int_t run_number, Int_t asic, Bool_t AD_end = kTRUE )
{ 
  TFile* f = Get_File( run_number );
  TDirectory* dir = Get_Sub_Directory( "Occupancy", "QOD", f );

  char name[256];
  if ( AD_end ) sprintf( name, "Modules in the AD end : ASIC# %d : Occupancy", asic );
  else sprintf( name, "Modules in the e+ end : ASIC# %d : Occupancy", asic );

  TH1D* hist = Get_TH1D( dir, name );

  return hist;
}
