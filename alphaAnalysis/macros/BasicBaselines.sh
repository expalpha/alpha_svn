#!/bin/bash

#######################################
# Contents:
# Lifetime
# Background
# Mixing
# Vacuum (RCT Hold)
# FastRampDown (vertex and excel trapping sheet)
# FastRampDown Detector QOD
# SequenceQOD
#
#######################################

RUNNO=${1}

cd ${RELEASE}/alphaAnalysis/macros

#Basic analysis report:
echo ".L PlotDumps.C+
PrintAnalysisReport(${RUNNO})
.q
"|root -l -b | tail -n 17 &> R${RUNNO}-AnalysisReport.txt

#Lifetime
echo ".L PlotDumps.C+
if (IsDumpInATM(${RUNNO}, \"startDump\", \"Lifetime\") || IsDumpInRCT(${RUNNO}, \"startDump\", \"Lifetime\")) {
    PrintDetectorQOD(${RUNNO},\"Lifetime\");
  } else {
    cout << \" Lifetime not in ATOM trap...  \" << endl; 
  }
.q
" | root -l -b

#Background
echo ".L PlotDumps.C+
AntiDumpCosmicBaseline(${RUNNO}, 1000.)
.q
"| root -l -b 


#Mixing
for i in `seq 1 300`; do
  MIXSTATUS=`echo ".L PlotDumps.C+
if (IsDumpInATM(${RUNNO}, \"startDump\", \"Mixing\",${i}) >0) {
PrintDetectorQOD($RUNNO,\"Mixing\",${i});
}
.q
" | root -l -b 2>&1`
  MIXERROR=`echo "$MIXSTATUS" | grep "Could not find a match for event"`
  if [ ${#MIXERROR} -ge 3 ]; then
    echo "No more mixing dumps... breaking loop"
    break
  fi
done

#Vacuum
echo ".L PlotDumps.C+
gProgressBars=kFALSE;
TrappingVacuumBaseline($RUNNO)
.q
" | root -b -l

#FastRampDown
echo ".L PlotDumps.C+
TString savFolder=\"~/AnalysisOutput/${RUNNO}\";
Int_t runNumber=${RUNNO}; 
Double_t start=MatchEventToTime(runNumber,\"startDump\",\"FastRampDown\",1,0);
Double_t stop=MatchEventToTime(runNumber,\"stopDump\",\"FastRampDown\",1,0);
if ((stop-start)<100) {
  PrintTrappingReportForExcel(runNumber,PrintQuenchGateWindow($RUNNO));
}else { cout << \"FastRampDown abnormally long (\" << stop-start << \"s), skipping listing Si Events\"<< endl;} 
.q
" | root -l -b	

 echo ".L PlotDumps.C+
gAnnounce=kFALSE;
gProgressBars=kFALSE;
PrintDetectorQOD(${RUNNO},\"FastRampDown\");
.q
" | root -b -l 

 echo ".L PlotDumps.C+
gAnnounce=kFALSE;
gProgressBars=kFALSE;
PrintDetectorQOD(${RUNNO},\"Quench\");
.q
" | root -b -l 

 echo ".L PlotDumps.C+
gAnnounce=kFALSE;
PrintQuenchGateWindow(${RUNNO});
.q
" | root -b -l

#Sequence QOD


echo ".L PlotDumps.C+
gProgressBars=kFALSE;
PrintSequenceQOD(${RUNNO})
.q
" | root -l -b 


