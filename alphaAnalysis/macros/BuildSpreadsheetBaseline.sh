#!/bin/bash

#Concatinate spreadsheed based baselines... skip runs already in spreadsheet






case `hostname` in
alphasvnchecker*)
  echo "Hello, we are alphasvnchecker... building special baselines..."
  for j in `echo "38032 39993 40258"`; do
    #echo "j: $j"
    OUTPUT="${j}_SVN_Summary.csv"
    #cat *-${j}.csv > ${OUTPUT}
    for i in `ls *-R${j}.csv`; do
      SVN=`awk -F"," '{print $2}' $i | tail -n 1`
      if [ `awk -F"," '{print $2}' $OUTPUT | grep "$SVN" | wc -l` -eq 0 ]; then
        echo "adding $SVN to $OUTPUT"
        #if [ ! -f "$OUTPUT" ]; then
        #  echo "New file, adding title"
        #  grep "Run" $i | head -n 1 > $OUTPUT
        #fi
        #grep -v "Run" $i >> $OUTPUT
        cat ${i} >> ${OUTPUT}
      else
        echo "$RUNNO already in file"
      fi
    done
  done
  #scp *Summary.csv alpha@alphadaq:~/joseph/JosephBaselines/
  scp *Summary.csv alpha@alphadaq:~/public_html/JosephBaselines/
  exit
  ;;
*)
    echo "Not on alphasvnchecker... not running special cases"
    
    ;;
esac


#Quenchdata (special as its in a subfolder): 
#FastRampDown
#I collect the CSV files from PrintTimeGateWindow()
for j in ` echo "FastRampDown Quench"`; do
  for i in `ls ${j}CSV/${j}Data_Run[0-9][0-9][0-9][0-9][0-9].csv`; do
    QUENCHOUTPUT="${j}_VertexSummary.csv"
    RUNNO=`awk -F"," '{print $1}' $i | tail -n 1`
    if [ `awk -F"," '{print $1}' $QUENCHOUTPUT | grep "$RUNNO" | wc -l` -eq 0 ]; then
      echo "adding $RUNNO to $QUENCHOUTPUT"
      if [ ! -f "$QUENCHOUTPUT" ]; then
        echo "New file, adding title"
        grep Run $i | head -n 1 > $QUENCHOUTPUT
      fi
      grep -v "Run" $i >> $QUENCHOUTPUT
    else
      echo "$RUNNO already in file"
    fi
  done
done

#Trapping spreadsheet
rm TrappingAutoSpreadsheetFiles/TrappingSpreadsheetSummary.csv
for i in `ls -tr TrappingAutoSpreadsheetFiles/TrappingHash*.csv`; do
cat ${i} >>  TrappingAutoSpreadsheetFiles/TrappingSpreadsheetSummary.csv
echo "
" >> TrappingAutoSpreadsheetFiles/TrappingSpreadsheetSummary.csv
done




#FastRampDown
#TrappingLifetime
#Lifetime
#I collect the CSV files from PrintDetectorQOD()
for j in `echo "FastRampDown Mixing Lifetime Cosmic TrappingLife PostTrappingBackground PostTrappingBackgroundLong 243_CC_DD AntiDumpCosmic"`; do
  #echo "j: $j"
  OUTPUT="${j}_Summary.csv"
  for i in `ls ${j}-R[0-9][0-9][0-9][0-9][0-9].csv`; do
    #echo "i: $i"
    #OUTPUT="${j}_Summary.csv"
    RUNNO=`awk -F"," '{print $1}' $i | tail -n 1`
    if [ `awk -F"," '{print $1}' $OUTPUT | grep "$RUNNO" | wc -l` -eq 0 ]; then
      echo "adding $RUNNO to $OUTPUT"
      if [ ! -f "$OUTPUT" ]; then
        echo "New file, adding title"
        grep "Run" $i | head -n 1 > $OUTPUT
      fi
      grep -v "Run" $i >> $OUTPUT
    else
      echo "$RUNNO already in file"
    fi
  done
done

scp *Summary.csv TrappingAutoSpreadsheetFiles/TrappingSpreadsheetSummary.csv alpha@alphadaq:~/public_html/JosephBaselines/
#scp *Summary.csv alpha@alphadaq:~/joseph/JosephBaselines/
