#include "AlphaAnalysisUtilities.cpp"
//#include "Find_LabVIEW_Events.cpp"
#include "TSpline.h"
#include "PlotMixing.cpp"

//Definitions of constants

#define NORM_TRANSFORMER 3.0
#define MAX_RAMP_POINTS 200000
#define EV_TO_K 11604.5059
#define FITMOVEPOINTS 11 // The number of points over which to move when doing thes fit scans

gStyle->SetOptStat(1000000);

// Global variables which contain settings which might need to be changed

Double_t gHISTO_RESOLUTION = 0.00002; //Resolution in Seconds
Double_t gENERGY_HISTO_RESOLUTION = 0.001; //Resolution in Volts
Double_t RAMP_START_DELAY = 0.001;
Double_t RAMP_END_DELAY = 0.001;
Int_t gFitFailed(0);
Int_t gIntegrateHistos(0);
Int_t gDisablePrintfing(0);
TString gFitOpts("0LI");
char drawOpts[50]="HIST";
char text[500];

/*
Eoin Butler, Jul 2009

CODERS, PLEASE NOTE!
Routines which the user should run and which produce plots should start with Plot...
Variable names should be immediately obvious - do not use obscure abbreviations
*/


//Declaration of things which need to be passed between functions, but which sometimes get lost

TH1D* gLastHistoPointer;
Int_t gHistoCounter(1);



//Plot Displayers

void PlotDump(Int_t runNumber, Int_t ChainNumber, Int_t DumpNumber, Int_t DetectorChannel)
{
    TH1D* theHist = GetDumpHisto(runNumber, ChainNumber, DumpNumber, DetectorChannel);
    FormatHisto(theHist);

}

void PlotDump(Int_t runNumber, Int_t ChainNumber, Int_t DumpNumber, Int_t DetectorChannel, Double_t startTime, Double_t endTime)
{
    TH1D* theHist = GetDumpHisto(runNumber, ChainNumber, DumpNumber, DetectorChannel);
    FormatHisto(theHist);
    if(startTime!=0. || endTime!=0.)
    {
        Int_t startBin(1), endBin(0);
        endBin=theHist->GetNbinsX();
        if(startTime!=0.) startBin=TimeToBinNumber(theHist, startTime);
        if(endTime!=0.) endBin=TimeToBinNumber(theHist, endTime);

        theHist->GetXaxis()->SetRange(startBin, endBin);

        sprintf(text, "Integral %d", theHist->Integral(startBin, endBin));
        AddRandomText(text, 1, 0.45, 0);
    }

    Int_t max = theHist->GetMaximumBin();
    std::cout << "Run: " << runNumber << std::endl;
    std::cout << "Mean:  " << theHist->GetMean() << " s"<<std::endl;
    //std::cout << max << std::endl;
    std::cout << "Maximum: " << theHist->GetBinCenter(theHist->GetMaximumBin()) << " s" << std::endl;

}

void PlotDump(Int_t runNumber, char* dumpName, Int_t DetectorChannel, Int_t dumpRep=1, Double_t startTime=0., Double_t endTime=0.)
{
    Int_t dumpNumber = MatchDumpNameToDumpNumber(runNumber, dumpName, dumpRep);
    PlotDump(runNumber, 1, dumpNumber, DetectorChannel, startTime, endTime);
}

TH1D* PlotEnergyDump(Int_t runNumber, char* voltageFile, Int_t ChainNumber, Int_t DumpNumber, Int_t DetectorChannel, Int_t PlotLog=0)
{
    char voltageFileTemp[500];
    strcpy(voltageFileTemp, voltageFile);

    TString* voltageFilename = new TString(voltageFile);
    if(!(voltageFilename->CompareTo("AUTO", TString::kIgnoreCase)))
    {
        TString* foundName = FindDumpFileEvent(runNumber, DumpNumber);
        sprintf(voltageFileTemp, "%s", foundName->Data());
    }

    TH1D* theHist = GetEnergyDumpHisto(runNumber, voltageFileTemp, ChainNumber, DumpNumber, DetectorChannel);

    TCanvas* c = new TCanvas("EnergyPlots","EnergyPlots");

    TPad* p = new TPad("thepad","thepad",0,0,1,1);
    p->SetLogy(PlotLog);
    p->SetFillStyle(4000);
    p->Draw();
    p->cd();

    if(gIntegrateHistos)
    {
        theHist->SetTitle(TString(theHist->GetTitle()).Prepend("Integrated "));
        Double_t tempError;

        for(Int_t i = (theHist->GetNbinsX()-2); i>=0; i--)
        {
        tempError = theHist->GetBinError(i);
        theHist->SetBinContent(i, theHist->GetBinContent(i)+theHist->GetBinContent(i+1));
        theHist->SetBinError(i,sqrt(tempError**2+(theHist->GetBinError(i+1))**2));
        }

        gLastHistoPointer = theHist;
    }

        FormatHisto(theHist);

    AddRandomTextWidth( voltageFileTemp, 4, 0.45, 0.5, 1);
    if(gIntegrateHistos) AddRandomText("Integrated", 3, 0.6, 7);

    sprintf(text, "Underflow: %d", theHist->GetBinContent(0));
    AddRandomText(text, 1, 0.45, 2);

    sprintf(text, "Res: %.3g mV", gENERGY_HISTO_RESOLUTION*1000.);
    AddRandomText(text, 1, 0.45, 4);

    sprintf(text, "Mean: %.3g V", theHist->GetMean());
    AddRandomText(text, 4, 0.45, 8);
    sprintf(text, "Peak: %.3g V", theHist->GetBinCenter(theHist->GetMaximumBin()));
    AddRandomText(text, 4, 0.45, 9);

    return theHist;
}

TH1D* PlotEnergyDump(Int_t runNumber, char* voltageFile, char* dumpName, Int_t detectorChannel, Int_t PlotLog=0)
{
    Int_t dumpNumber = MatchDumpNameToDumpNumber(runNumber, dumpName);
    return PlotEnergyDump(runNumber, voltageFile, 1, dumpNumber, detectorChannel, PlotLog);
}

void PlotEnergyDumpBack(Int_t runNumber, char* voltageFile, Int_t ChainNumber, Int_t DumpNumber, Int_t DetectorChannel, Double_t Background=-1.)
{
    TH1D* theHist = GetDumpHisto(runNumber, ChainNumber, DumpNumber, DetectorChannel);

    if(Background<0.)
    {
        TH1D* BackHisto = GetBackgroundHisto(runNumber, ChainNumber, DetectorChannel, 1 );
        Double_t Background = BackHisto->Integral()/BackHisto->GetXaxis()->GetXmax();
    }

    Info("PlotEnergyDumpBack","\033[32mBackground rate :%lf\033[00m", Background);

    TF1* backgroundFxn = new TF1("background", "1", 0, 100000);
    theHist->Add(backgroundFxn, -Background*gHISTO_RESOLUTION);

    theHist = GetEnergyHistoFromTimeHisto(theHist, voltageFile);

    sprintf(text,"Energy: Run %d, Chain %d, Dump %d, Channel %d", runNumber, ChainNumber, DumpNumber, DetectorChannel);

    FormatHisto(theHist);
    sprintf(text, "Background Subtracted %.1lf Hz", Background);
    AddRandomText(text, 2, 0.33, 2);
    AddRandomText(voltageFile, 4, 0.3, 1);
    AddIntegralText(theHist);

    theHist->SetTitle(text);
}

TH1D* PlotEnergyDumpLog(Int_t runNumber, char* voltageFile, Int_t ChainNumber, Int_t DumpNumber, Int_t DetectorChannel)
{
    return PlotEnergyDump(runNumber, voltageFile, ChainNumber, DumpNumber, DetectorChannel, 1);
}

TH1D* PlotEnergyDumpLog(Int_t runNumber, char* voltageFile, Int_t ChainNumber, Int_t DumpNumber, Int_t DetectorChannel, Double_t fitMin, Double_t fitMax)
{
    TH1D* histo = PlotEnergyDumpLog(runNumber, voltageFile, ChainNumber, DumpNumber, DetectorChannel);

    TF1* fitFunction = FitExponential(histo,fitMin, fitMax);
    fitFunction->SetLineColor(gHistoCounter+1);
    fitFunction->Draw("same");

    if(gFitFailed>0) AddRandomText(gLastHistoPointer, "Warning: Fit Failed", 2, 0.33, 8);
    sprintf(text, "Chi2: %.2lf, NDF: %d", fitFunction->GetChisquare(), fitFunction->GetNDF());
    AddRandomText(text, 1, 0.5, 3);
    gFitFailed=0;

    Double_t Temperature, TemperatureError;
    CalculateTemperatureFromFit(fitFunction, &Temperature, &TemperatureError);

    sprintf(text, "T: %.2lf +/- %.2lf K",Temperature, TemperatureError);
    TText* tempLabel = new TText(0.6, 0.6, text);
    tempLabel->SetNDC();
    tempLabel->SetTextColor(gHistoCounter+1);
    tempLabel->Draw();

    //ExtractHisto(runNumber, voltageFile);

    return histo;
}

TH1D* PlotEnergyDumpLog(Int_t runNumber, char* voltageFile, char* dumpName, Int_t detectorChannel)
{
    Int_t dumpNumber = MatchDumpNameToDumpNumber(runNumber, dumpName);
    return PlotEnergyDumpLog(runNumber, voltageFile, 1, dumpNumber, detectorChannel);
}

TH1D* PlotEnergyDumpLog(Int_t runNumber, char* voltageFile, char* dumpName, Int_t detectorChannel, Double_t fitMin, Double_t fitMax)
{
    Int_t dumpNumber = MatchDumpNameToDumpNumber(runNumber, dumpName);
    return PlotEnergyDumpLog(runNumber, voltageFile, 1, dumpNumber, detectorChannel, fitMin, fitMax);
}

PlotEnergyDumpOverlay(char* runFilePath, char* voltageMapPath, char* dumpName, Int_t detector, Int_t PlotLog=0)
{
    Int_t runNumber[1000], dumps[1000];
    Int_t nRuns;
    TObjArray* legendText = new TObjArray();
    nRuns = FileToRuns(runFilePath, runNumber, dumps, legendText);

    TCanvas* c = new TCanvas("EnergyPlots","EnergyPlots");

    TPad* graphs = new TPad("thepad","thepad",0.,0.,0.7,1.);
    graphs->SetLogy(PlotLog);
    graphs->SetFillStyle(4000); //transparent
    graphs->Draw();

    TPad* legend = new TPad("legend", "legend", 0.7, 0., 1., 1.);
    //legend->SetFillStyle(4000); //transparent
    legend->Draw();
    legend->cd();

    TText* runListText = new TText();

    for(Int_t i=0; i<nRuns; i++)
    {
        runListText->SetTextSize(0.1);
        runListText->SetTextColor(i+1+(i>3));

        runListText->DrawText(0.1, 0.95-0.04*i, ((TString)legendText->At(i)).Data());

        dumps[i]=MatchDumpNameToDumpNumber(runNumber[i], dumpName);
    }

    graphs->cd();
    PlotEnergyDumpOverlay(nRuns, runNumber, dumps, voltageMapPath, detector);


}

void PlotEnergyDumpOverlay (Int_t nRuns, Int_t* runNumber, Int_t* dumpNumbers, char* voltageFile, Int_t detector)
{
    gStyle->SetOptStat(goptStat);
    TH1D *dumpHistogram, *timeHistogram;
    Double_t transformerValue;

    for(Int_t i=0; i<nRuns; i++)
    {
        timeHistogram=GetDumpHisto(runNumber[i], 1, dumpNumbers[i], detector);

        dumpHistogram=GetEnergyHistoFromTimeHisto(timeHistogram, voltageFile);
        delete timeHistogram;

        transformerValue=GetTransformerValue(runNumber[i]);
        if(TRANSFORMER_NORMALISED>0. && transformerValue>0.) dumpHistogram->Scale(TRANSFORMER_NORMALISED/transformerValue);

        sprintf(text,"Overlay of %d runs", nRuns);
        dumpHistogram->SetTitle(text);

        dumpHistogram->SetLineColor(i+1+(i>3));
        if(i==0) dumpHistogram->DrawCopy();
        else dumpHistogram->DrawCopy("SAME");

        Int_t mixingIntegral = dumpHistogram->Integral();
        sprintf(text,"Integral: %d", mixingIntegral);
        AddRandomText(text, i+1, 0.7, i+1);

        delete dumpHistogram;
    }
}

void PlotDumpNested(Int_t runNumber, char* dumpName, Int_t channel, Int_t dumpRep=1, Double_t startTime=0., Double_t endTime=0.)
{
    // This macro allows for the possibility that you have nested dumps, so it looks for the stopDump Count as well as the startDump
    TH1D* dumpHisto = GetDumpHistoNested(runNumber, dumpName, channel, dumpRep, startTime, endTime);
    FormatHisto(dumpHisto);
}

ExtractHisto(char* outFilePath)
{
    if(gLastHistoPointer==NULL)
    {
         Error("ExtractHisto","\033[31mThere is no histogram to extract\033[00m");
        return;
    }

    FILE* outFile = fopen(outFilePath,"w");
    if(outFile==NULL)
    {
        Error("ExtractHisto","\033[31mCould not open output file %s\033[00m", outfilePath);
        return;
    }

    printf("Saving to %s\n", outFilePath);

    for (Int_t i=0; i<gLastHistoPointer->GetNbinsX(); i++)
    {
        fprintf(outFile,"%g\t%g\n", gLastHistoPointer->GetBinCenter(i), gLastHistoPointer->GetBinContent(i));
    }

    fclose(outFile);
}

ExtractHisto(Int_t runNumber, char* voltageFile)
{
    char *voltageFileName;
    char * pch;

    pch = strtok(voltageFile, " /");
    printf("voltageFile: %s\n", voltageFile);
    while(pch!=NULL)
    {
        printf("voltageFile: %s\n", voltageFile);
        voltageFileName = pch;
        printf("voltageFile: %s\n", voltageFile);
        pch = strtok(NULL, " /");
    }

    char tempstring[200];

    sprintf(tempstring, "energyhistograms/energyoutput%d_%s_%.2lfRes.txt", runNumber, voltageFileName, gENERGY_HISTO_RESOLUTION*1000);

    TString outputFileName(tempstring);

    ExtractHisto(outputFileName);
}

//Histogram Getters

TH1D* GetHistoByTimes(Int_t runNumber, Int_t DetectorChannel, Double_t StartTime, Double_t EndTime)
{
    //Info("GetHistoByTimes", "(runNumber=%d DetectorChannel=%d StartTime=%lf EndTime=%lf)", runNumber, DetectorChannel, StartTime, EndTime);
    TTree* theTree = Get_Sis_Tree(runNumber, DetectorChannel);
    TSisEvent* sisEvent = new TSisEvent();
    theTree->SetBranchAddress("SisEvent", &sisEvent );

    TH1D* theHist = (TH1D*)gDirectory->Get("Dump Histo");
    if(theHist!=NULL)
    {
        //Info("debug", "deleting histo");
        delete theHist;
    }

    sprintf(text, "Run %d, Channel %d - RunTime>=%.6lf && RunTime<=%.6lf", runNumber, DetectorChannel, StartTime, EndTime);
    theHist = new TH1D("Dump Histo", text, ceil((EndTime-StartTime)/gHISTO_RESOLUTION+1.), 0, EndTime-StartTime);

    sprintf(text, "RunTime>=%.6lf && RunTime<=%.6lf", StartTime, EndTime);
    theTree->Draw(">>timeList", text);
    TEventList* timeList = (TEventList*)gDirectory->Get("timeList");
    if(timeList==NULL) return theHist; //the list is empty

    //for(Int_t i =0; i<theTree->GetEntries();i++)
    for(Int_t i=0; i<timeList->GetN();i++)
    {
        theTree->GetEntry(timeList->GetEntry(i));
        if(sisEvent->GetChannel()!=DetectorChannel) continue;
        if(sisEvent->GetRunTime()<StartTime || sisEvent->GetRunTime()>EndTime) continue;
        theHist->Fill(sisEvent->GetRunTime()-StartTime,sisEvent->GetCountsInChannel());
    }

    theHist->GetYaxis()->SetRange(0, 1);

    gLastHistoPointer = theHist;
    delete theTree;
    delete timeList;
    return theHist;
}

TH1D* GetDumpHisto(Int_t runNumber, Int_t ChainNumber, Int_t DumpNumber, Int_t DetectorChannel, Double_t startCutTime=0., Double_t endCutTime=0.)
{
    Double_t startTime, endTime;

    if(DumpNumber==-1)
    {
        startTime=0.;
        endTime = Get_Total_RunTime(runNumber);
    }
    else
    {
        GetDumpTimes(runNumber, ChainNumber, DumpNumber, &startTime, &endTime);
    }

    //Info("GetDumpHisto", "Dump Found %.3lf - %.3lf [%.3lf]", startTime, endTime, endTime-startTime);

    if(endCutTime!=0.) endTime=startTime+endCutTime;
    if(startCutTime!=0.) startTime+=startCutTime;
    TH1D* theHist = GetHistoByTimes(runNumber, DetectorChannel, startTime, endTime );
    char histTitle[200];
    sprintf(histTitle,"Run %d, Chain %d, Dump %d, Detector %d", runNumber, ChainNumber, DumpNumber, DetectorChannel);

    theHist->SetTitle(histTitle);
    theHist->SetName(histTitle);

    sprintf(text,"Dump Histogram Retrieved: Run %d, Chain %d, Dump %d, Channel %d: %.3lf - %.3lf [%.3lf]: Integral %.1lf",runNumber, ChainNumber, DumpNumber, DetectorChannel, startTime, endTime, endTime-startTime, theHist->Integral());
    if(!gDisablePrintfing) Info("GetDumpHisto", text);

    #ifdef NORMALISE_TIME_HISTOS
        Warning("GetDumpHisto", "\033[35mNormalising Time Axis\033[00m");
        theHist->Scale(0.001);
        theHist->GetXaxis()->SetLimits(0., 1.);
    #endif

    gLastHistoPointer = theHist;
    return theHist;
}

TH1D* GetDumpHistoNested(Int_t runNumber, char* dumpName, Int_t DetectorChannel, Int_t dumpRep=1, Double_t startCutTime=0., Double_t endCutTime=0.)
{
    // This macro allows for the possibility that you have nested dumps, so it looks for the stopDump Count as well as the startDump
    Int_t startCount = MatchDumpNameToDumpNumber(runNumber, dumpName, dumpRep);
    Int_t stopCount = MatchDumpNameToDumpNumber(runNumber, dumpName, dumpRep, 1);

    Double_t startTime = Get_RunTime_of_Count(runNumber, SIS_DUMP_START, startCount);
    Double_t endTime = Get_RunTime_of_Count(runNumber, SIS_DUMP_STOP, stopCount);

    TH1D* theHist = GetHistoByTimes(runNumber, DetectorChannel, startTime, endTime );

    char histTitle[200];
    sprintf(histTitle,"Run %d, %s, Detector %d", runNumber, dumpName, DetectorChannel);

    theHist->SetTitle(histTitle);
    theHist->SetName(histTitle);

    sprintf(text,"Dump Histogram Retrieved: Run %d, %s: %.3lf - %.3lf [%.3lf]: Integral %.1lf",runNumber, dumpName, startTime, endTime, endTime-startTime, theHist->Integral());
    if(!gDisablePrintfing) Info("GetDumpHisto", text);

    #ifdef NORMALISE_TIME_HISTOS
        Warning("GetDumpHisto", "\033[35mNormalising Time Axis\033[00m");
        theHist->Scale(0.001);
        theHist->GetXaxis()->SetLimits(0., 1.);
    #endif

    if(startCutTime!=0. || endCutTime!=0.)
    {
        if(endCutTime!=0.)
        {
            theHist->GetXaxis()->SetLimits(startCutTime, endCutTime);
            theHist->GetXaxis()->SetRangeUser(startCutTime, endCutTime);
        }
        else
        {
            theHist->GetXaxis()->SetLimits(startCutTime, theHist->GetXaxis()->GetXmax());
            theHist->GetXaxis()->SetRangeUser(startCutTime, theHist->GetXaxis()->GetXmax());
        }
    }

    gLastHistoPointer = theHist;
    return theHist;
}

TH1D* GetBackgroundHisto(Int_t runNumber, Int_t ChainNumber, Int_t DetectorChannel, Int_t BackgroundNumber=1)
{
    Double_t startTime;
    Double_t endTime;

    GetBackgroundTimes(runNumber, ChainNumber, BackgroundNumber, &startTime, &endTime);

    printf("Background Found %.3lf - %.3lf [%.3lf]\n", startTime, endTime, endTime-startTime);

    TH1D* theHist = GetHistoByTimes(runNumber, DetectorChannel, startTime, endTime );

    char histTitle[200];
    sprintf(histTitle,"Run %d, Chain %d, Background %d, Detector %d", runNumber, ChainNumber, BackgroundNumber, DetectorChannel);

    theHist->SetTitle(histTitle);
    theHist->SetName(histTitle);

    printf("Run %d, Chain %d, Background %d: Integral %lf\n",runNumber, ChainNumber, BackgroundNumber, theHist->Integral());

    gLastHistoPointer = theHist;
    return theHist;
}

TH1D* GetEnergyDumpHisto(Int_t runNumber, char* voltageFile, Int_t ChainNumber, Int_t DumpNumber, Int_t DetectorChannel)
{
    char voltageFileTemp[500];
    strcpy(voltageFileTemp, voltageFile);

    TString* voltageFilename = new TString(voltageFile);

    TH1D* timeHisto = GetDumpHisto (runNumber, ChainNumber, DumpNumber, DetectorChannel);

    if(!(voltageFilename->CompareTo("AUTO", TString::kIgnoreCase)))
    {
        TString* foundName = FindDumpFileEvent(runNumber, DumpNumber);
        sprintf(voltageFileTemp, "%s", foundName->Data());
    }

    sprintf(text, "Energy: Run %d, Chain %d, Dump %d, Chan %d", runNumber, ChainNumber, DumpNumber, DetectorChannel);

    TH1D* energyHisto = GetEnergyHistoFromTimeHisto(timeHisto, voltageFileTemp);
    energyHisto->SetTitle(text);
    energyHisto->Sumw2();

    gLastHistoPointer = energyHisto;
    return energyHisto;
}

TH1D* GetEnergyHistoFromTimeHisto(TH1D* timeHisto, char* voltageFile)
{
    TSpline5* rampVoltage = GetSplineFromVoltageRampFile(voltageFile);

    TH1D* energyHisto = new TH1D("histoTitle", "histoTitle", (Int_t)((rampVoltage->Eval(0.)-0.)/gENERGY_HISTO_RESOLUTION)+1, 0., rampVoltage->Eval(0.));

    Double_t thisVoltage, thisTimeAsFraction;
    Int_t assignedBin, dumpFinished(0);
    char text[200];


    for (Int_t i =1; i<=timeHisto->GetNbinsX(); i++)
    {
        thisTimeAsFraction = (timeHisto->GetBinCenter(i)-RAMP_START_DELAY)/(timeHisto->GetBinCenter(timeHisto->GetNbinsX()-1)-RAMP_START_DELAY-RAMP_END_DELAY);

        thisVoltage = rampVoltage->Eval(thisTimeAsFraction);


        if(dumpFinished)
        {
             energyHisto->AddBinContent(0, timeHisto->GetBinContent(i));
             assignedBin=0;
        }

        else assignedBin = energyHisto->Fill(thisVoltage,timeHisto->GetBinContent(i));

        if(thisVoltage<0. && thisTimeAsFraction>0.)
        {
            if(dumpFinished==0)
            {
                sprintf(text, "Dump finished at t=%lf", timeHisto->GetBinCenter(i)-RAMP_START_DELAY);
                Info("GetEnergyHistoFromTimeHisto", text);
            }
            dumpFinished=1;
        }

    }

    energyHisto->SetXTitle("Energy [eV]");
    energyHisto->SetYTitle("Counts");

    gLastHistoPointer = energyHisto;
    return energyHisto;
}

//Energy Stuff

Int_t GetVoltageRampFromFile(char* fileName, Double_t* step, Double_t* potential)
{

    FILE *voltageFile = fopen(fileName,"r");

    Int_t nPoints;

    if(voltageFile==NULL)
    {
        Error("GetVoltageRampFromFile","\033[31mCould not open voltage ramp file %s\033[00m", fileName);
        //gSystem->Exit(1);
        voltageFile->Kick(); // This is to crash the CINT interface  instead of exiting (deliberately)
    }

    char buffer[500];

    for(nPoints=0; nPoints<MAX_RAMP_POINTS; nPoints++)
    {
        fgets(buffer, 500, voltageFile);
        sscanf(buffer, "%lf\t%lf", &step[nPoints], &potential[nPoints]);
        if(feof(voltageFile)) break;
    }

    Info("GetVoltageRampFromFile", "Read %d points from voltage file \"%s\"", nPoints, fileName);
    fclose(voltageFile);

    step = (Double_t*)realloc(step, nPoints*sizeof(Double_t));
    potential = (Double_t*)realloc(potential, nPoints*sizeof(Double_t));

    for(Int_t i=0; i<nPoints; i++) step[i] = step[i]/step[nPoints-1];

    return nPoints;
}

TSpline5* GetSplineFromVoltageRampFile(char* fileName)
{
    Double_t* step = (Double_t*)malloc(MAX_RAMP_POINTS*sizeof(Double_t));
    Double_t* potential = (Double_t*)malloc(MAX_RAMP_POINTS*sizeof(Double_t));

    Int_t nPoints = GetVoltageRampFromFile(fileName, step, potential);

    TSpline5* spline = GetSplineFromVoltageRamp(nPoints, step, potential);

    return spline;
}

TSpline5* GetSplineFromVoltageRamp(Int_t nPoints, Double_t* step, Double_t* potential)
{
    TSpline5* spline = new TSpline5("Voltage Ramp", step, potential, nPoints);
    return spline;
}

TF1* FitExponential(TH1D* EnergyHistogram, Double_t fitMin, Double_t fitMax)
{
    TF1* fitFunction = new TF1("fitFunction", "expo", fitMin, fitMax);
    fitFunction->SetNpx(1000);
    gFitFailed=0;

    Int_t binMin = EnergyHistogram->GetXaxis()->FindBin(fitMin);
    Int_t binMax = EnergyHistogram->GetXaxis()->FindBin(fitMax);

    Bool_t zeroBin(kFALSE);

    for(Int_t i=binMin; i<=binMax; i++)
    {
        if(EnergyHistogram->GetBinContent(i)==0)
        {
            zeroBin = kTRUE;
            break;
        }
    }

    if(zeroBin)
    {
        AddRandomTextWidth("Warning: Empty Bins in range: Use coarser resolution", 2, 0.33, 0.3, 8);
    }

    Double_t nMin = EnergyHistogram->GetBinContent(binMin);
    Double_t nMax = EnergyHistogram->GetBinContent(binMax);

    fitFunction->SetParameter(1, (log(nMin)-log(nMax) )/(fitMin-fitMax));
    fitFunction->SetParameter(0, log(nMin)-fitFunction->GetParameter(1)*fitMin);

    Info("", "%e %e", fitMin, fitMax);
    Info("Guess", " [0]: %e [1]: %e", fitFunction->GetParameter(0), fitFunction->GetParameter(1));

    if(EnergyHistogram->Fit("fitFunction", gFitOpts.Data(), "", fitMin, fitMax)->Status()>0) gFitFailed=1;

    fitFunction->GetXaxis()->SetRangeUser(fitMin, fitMax);

    Info("FitExponential","\tProbability : %lf", fitFunction->GetProb());
    Info("FitExponential","\tChi-square : %lf\t NDF : %d", fitFunction->GetChisquare(), fitFunction->GetNDF());


    return fitFunction;
}

// Utilities

TString* FindDumpFileEvent(Int_t runNumber, Int_t dumpNumber)
{
    Double_t startTime, endTime, eventTime;
    Int_t i, seqOffset;

    GetDumpTimes(runNumber, 1, dumpNumber, &startTime, &endTime);

    TTree* sequencerTree = GetSequencerTree(runNumber);
    TSeq_Event* seqEvent = new TSeq_Event();
    sequencerTree->SetBranchAddress("SequenceEvent", &seqEvent);

    for(i=0; i<sequencerTree->GetEntries(); i++)
    {
        sequencerTree->GetEntry(i);

        if(seqEvent->GetEventName().CompareTo("dumpFile", TString::kIgnoreCase)) continue;

        for(seqOffset=0; seqOffset<=MATCH_SEQUENCE_TOLERANCE; seqOffset++)
        {
            eventTime = Get_RunTime_of_SequencerEvent(runNumber, seqEvent, seqOffset);

            if(eventTime>=startTime && eventTime<=endTime)
            {
                Info("FindDumpFileEvent", "Found dump file \033[34m%s\033[00m for runNumber %d, dumpNumber %d", seqEvent->GetDescription().Data(), runNumber, dumpNumber);
                TString* description = new TString(seqEvent->GetDescription().Data());
                sprintf(text, "\"");
                description->Remove(TString::kBoth,text[0]);
                delete seqEvent;
                delete sequencerTree;
                return description;
            }
        }
    }

    Error("FindDumpFileEvent", "\033[31mCould not find dump file for runNumber %d, dumpNumber %d\033[00m", runNumber, dumpNumber);
    //gSystem->Exit(1);
    description=NULL;
    description->Kick(); // This is to crash the CINT interface  instead of exiting (deliberately)
}

void FindDumps(Int_t runNumber)
{
    PrintChainsAndDumps(runNumber);
}

FindDumps(Int_t runNumber, Int_t detectorChannel)
{
    gDisablePrintfing=1;

    Int_t ChainCount(1), DumpCount(1), moreDumps(1);
    Double_t DumpStartTime, DumpEndTime;
    TH1D* theHist;
    Int_t numChains = Count_SIS_Triggers(runNumber, SIS2_PBAR_SEQ_START);

    for(ChainCount=1; ChainCount<=numChains+1; ChainCount++)
    {
        printf("--------------- Chain %d ---------------\n",ChainCount);

        DumpCount=0;
        moreDumps=1;
        while(moreDumps>0)
        {
            DumpCount++;
            GetDumpTimes(runNumber, ChainCount, DumpCount, &DumpStartTime, &DumpEndTime);

            if(DumpStartTime==-1. || DumpEndTime==-1.) moreDumps=0;
            else
            {
                printf("Dump %d\t%7.3lf - %7.3lf [%7.3lf]\t", DumpCount, DumpStartTime, DumpEndTime, DumpEndTime-DumpStartTime);

                    theHist=GetDumpHisto(runNumber, ChainCount, DumpCount, detectorChannel);
                    if(theHist!=NULL)
                    {
                        printf("\t%5d", theHist->Integral());
                    }
                    else
                    {
                        printf("\t-");
                    }

                printf("\n");
            }
        }
    }

}

void FindDumps(Int_t runNumber, Int_t NumChannels, Int_t* Channels)
{
    gDisablePrintfing=1;

    Int_t ChainCount(1), DumpCount(1), moreDumps(1);
    Double_t DumpStartTime, DumpEndTime;
    TH1D* theHist;
    Int_t numChains = Count_SIS_Triggers(runNumber, SIS2_PBAR_SEQ_START);

    for(ChainCount=0; ChainCount<=numChains+1; ChainCount++)
    {
        printf("--------------- Chain %d ---------------\n",ChainCount);

        DumpCount=0;
        moreDumps=1;
        while(moreDumps>0)
        {
            DumpCount++;
            GetDumpTimes(runNumber, ChainCount, DumpCount, &DumpStartTime, &DumpEndTime);

            if(DumpStartTime==-1. || DumpEndTime==-1.) moreDumps=0;
            else
            {
                printf("Dump %d\t%7.3lf - %7.3lf [%7.3lf]\t", DumpCount, DumpStartTime, DumpEndTime, DumpEndTime-DumpStartTime);

                for(Int_t j=0; j<NumChannels; j++)
                {
                    theHist=GetDumpHisto(runNumber, ChainCount, DumpCount, Channels[j]);
                    if(theHist!=NULL)
                    {
                        printf("\t%5d", theHist->Integral());
                    }
                    else
                    {
                        printf("\t-");
                    }
                }
                printf("\n");
            }
        }
    }
}


void FindDumps_TimeStamp(Int_t runNumber)
{
    gDisablePrintfing=1;
    static const int FindDumpsNumChannels=4;
    Int_t Channels[FindDumpsNumChannels] = { SIS_PMT_DEG_OR, SIS_PMT_TRAP_OR, SIS_PMT_DOWN_OR, SIS2_TTC_SI_TRIG };

    Int_t ChainCount(1), DumpCount(1), moreDumps(1);
    Double_t DumpStartTime, DumpEndTime, ChainStartTime, mixCounts;
    TH1D* theHist;
    Int_t numChains = Count_SIS_Triggers(runNumber, SIS2_PBAR_SEQ_START);
    Double_t timestampCounts;

    Int_t numMixChains = Count_SIS_Triggers(runNumber, SIS2_MIX_SEQ_START);
    Double_t* TimestampsAtStartOfMix = (Double_t*)malloc(numMixChains*sizeof(Double_t));


    for(ChainCount=0; ChainCount<numMixChains; ChainCount++)
    {
        TimestampsAtStartOfMix[ChainCount]=Count_SIS(runNumber,  SIS2_MIX_SEQ_TIME, 0., Find_SIS_Channel_Trigger_Time(runNumber, SIS2_MIX_SEQ_START, ChainCount+1) );
    }

    for(ChainCount=0; ChainCount<=numChains; ChainCount++)
    {
        printf("--------------- Chain %d ---------------\n",ChainCount);

        ChainStartTime = Get_RunTime_of_Count(runNumber, SIS2_PBAR_SEQ_START, ChainCount+1);
        if(ChainStartTime<0.) ChainStartTime=1.e99;
        DumpCount=0;
        moreDumps=1;
        while(moreDumps>0)
        {
            DumpCount++;
            GetDumpTimes(runNumber, ChainCount, DumpCount, &DumpStartTime, &DumpEndTime);

            if(DumpStartTime<=0. || DumpEndTime<=0. || DumpStartTime >ChainStartTime) moreDumps=0;
            else
            {
                printf("Dump %d\t%7.3lf - %7.3lf [%7.3lf]\t", DumpCount, DumpStartTime, DumpEndTime, DumpEndTime-DumpStartTime);

                printf("[%9.1lf-%9.1lf]\t", Count_SIS(runNumber, SIS2_PBAR_SEQ_TIME, 0, DumpStartTime), Count_SIS(runNumber, SIS2_PBAR_SEQ_TIME, 0, DumpEndTime));

                mixCounts = TimestampsAtStartOfMix[(Int_t)(Count_SIS(runNumber, SIS2_MIX_SEQ_START, 0, DumpStartTime)-1.)];


                printf("[%9.1lf-%9.1lf] {%.0lf}\t", Count_SIS(runNumber, SIS2_MIX_SEQ_TIME, 0, DumpStartTime)-mixCounts, Count_SIS(runNumber, SIS2_MIX_SEQ_TIME, 0, DumpEndTime)-mixCounts, Count_SIS(runNumber, SIS2_MIX_SEQ_START, 0, DumpStartTime)-1.);


                for(Int_t j=0; j<FindDumpsNumChannels; j++)
                {
                    theHist = GetDumpHisto(runNumber, ChainCount, DumpCount, Channels[j]);
                    printf("%d\t", theHist->Integral());
                    //theHist->Delete();
                }

                printf("\n");
            }
        }
    }
}

void PrintChainsAndDumps(Int_t runNumber)
{
    gDisablePrintfing=1;
        static const int FindDumpsNumChannels=3;
    Int_t Channels[FindDumpsNumChannels] = { SIS_PMT_TRAP_OR, SIS_PMT_5_AND_6, SIS_PMT_7_AND_8 };

    Int_t ChainCount(1), DumpCount(1), moreDumps(1);
    Double_t DumpStartTime, DumpEndTime, ChainStartTime;
    TH1D* theHist;
    Int_t numChains = Count_SIS_Triggers(runNumber, SIS2_PBAR_SEQ_START);

    for(ChainCount=0; ChainCount<=numChains; ChainCount++)
    {
        printf("--------------- Chain %d ---------------\n",ChainCount);

        ChainStartTime = Get_RunTime_of_Count(runNumber, SIS2_PBAR_SEQ_START, ChainCount+1);
        if(ChainStartTime<0.) ChainStartTime=1.e99;
        DumpCount=0;
        moreDumps=1;
        while(moreDumps>0)
        {
            DumpCount++;
            GetDumpTimes(runNumber, ChainCount, DumpCount, &DumpStartTime, &DumpEndTime);

            if(DumpStartTime<=0. || DumpEndTime<=0. || DumpStartTime >ChainStartTime) moreDumps=0;
            else
            {
                printf("Dump %d\t%7.3lf - %7.3lf [%7.3lf]\t", DumpCount, DumpStartTime, DumpEndTime, DumpEndTime-DumpStartTime);

                for(Int_t j=0; j<FindDumpsNumChannels; j++)
                {
                    theHist = GetDumpHisto(runNumber, ChainCount, DumpCount, Channels[j]);
                    printf("%d\t", theHist->Integral());
                    //theHist->Delete();
                }

                printf("\n");
            }
        }
    }
}

void FindAllDumps(Int_t runNumber)
{
    gDisablePrintfing=1;
    static const int FindDumpsNumChannels=4;
    Int_t Channels[FindDumpsNumChannels] = { SIS_PMT_DEG_OR, SIS_PMT_TRAP_OR, SIS_PMT_DOWN_OR, SIS2_TTC_SI_TRIG };

    Double_t DumpStartTime, DumpEndTime;
    TH1D* theHist;

    Int_t nDumps = Count_SIS_Triggers(runNumber, SIS2_PBAR_DUMP_START);
    gDisablePrintfing=1;
    for(Int_t i=0; i<nDumps; i++)
    {
        GetDumpTimes(runNumber, 0, i+1, &DumpStartTime, &DumpEndTime);

        printf("Dump %d\t%7.3lf - %7.3lf [%7.3lf]\t", i+1, DumpStartTime, DumpEndTime, DumpEndTime-DumpStartTime);

        for(Int_t j=0; j<FindDumpsNumChannels; j++)
        {
            theHist = GetDumpHisto(runNumber, 0, i+1, Channels[j]);
            printf("%d\t", theHist->Integral());
            delete theHist;
        }

        printf("\n");

    }
    gDisablePrintfing=0;

}

void PrintDetectorChannels()
{
    printf("Detector SIS Channels\n");
    printf("PMTs\n");
    printf("\tPMT_DEG_OR: %d\n",SIS_PMT_DEG_OR);
    printf("\tPMT_TRAP_OR: %d\n",SIS_PMT_TRAP_OR);
    printf("\tPMT_DOWN_OR: %d\n",SIS_PMT_DOWN_OR);
    printf("\tPMT_DEG_AND: %d\n",SIS_PMT_DEG_AND);
    printf("Silicon\n");
    printf("\tSilicon Trigger: %d\n",SIS2_TTC_SI_TRIG);
    printf("\tSilicon TTC 3: %d\n", SIS2_TTC_TRIG3);
    printf("\tSilicon TTC 6: %d\n", SIS2_TTC_TRIG6);
    printf("\tSilicon Readout: %d\n", SIS2_SI_RO);
}

void SetTimeResolution(Double_t resolution)
{
    gHISTO_RESOLUTION = resolution;
}

void SetEnergyResolution(Double_t resolution)
{
    gENERGY_HISTO_RESOLUTION = resolution;
}

void SetIntegrateFlag(Int_t IntegrateFlag=1)
{
     gIntegrateHistos=IntegrateFlag;
}

void SetFitOpts(char* fitOpts)
{
    gFitOpts = TString(fitOpts).Prepend("0");
}

void SetDumpDelays(Double_t startDelay=-1., Double_t endDelay=-1.)
{
    if(startDelay>=0.) RAMP_START_DELAY = startDelay;
    if(endDelay>=0.) RAMP_END_DELAY = endDelay;
}

// Low-level stuff

Double_t CalculateTemperatureFromFit(TF1* fitFunction, Double_t* Temperature, Double_t* TemperatureError)
{
    Temperature[0] = -EV_TO_K/(fitFunction->GetParameter(1));
    TemperatureError[0] = -Temperature[0]*(fitFunction->GetParError(1))/(fitFunction->GetParameter(1));

    return Temperature[0];
}

Double_t CalculateTemperatureFromFit(TF1* fitFunction)
{
    Temperature = -EV_TO_K/(fitFunction->GetParameter(1));
    return Temperature;
}

void CloseAllCanvases()
{
    TSeqCollection* canvasList=gROOT->GetListOfCanvases();
    TCanvas* c;

    for(Int_t i=0; i<canvasList->GetEntries(); i++);
    {
        c=(TCanvas*) canvasList->First();
        if(c!=NULL) c->Close();
    }
}

void FormatHisto(TH1D* theHist, Bool_t showInt = kTRUE)
{
    theHist->SetLineColor(gHistoCounter);
    theHist->SetStats(kFALSE);
    theHist->SetMarkerColor(gHistoCounter+(gHistoCounter==1));
    theHist->SetMarkerStyle(7);


    if(gHistoCounter>1) sprintf(text, "SAME %s", drawOpts);
    else sprintf(text, "%s", drawOpts);

    theHist->DrawCopy(text);
    if(showInt) AddIntegralText(theHist, gHistoCounter);
    
}

void WriteLegend(Int_t runNumber)
{
    TString label("Run ");
    label+=runNumber;

    TText* tempLabel = new TText(0.1, 1.-gHistoCounter*0.04, label);
    tempLabel->SetNDC();
    tempLabel->SetTextColor(gHistoCounter+1);
    tempLabel->SetTextSize(0.07);
    tempLabel->Draw();
}

void WriteLegend(char* LegendText)
{

    TText* tempLabel = new TText(0.1, 1.-gHistoCounter*0.04, LegendText);
    tempLabel->SetNDC();
    tempLabel->SetTextColor(gHistoCounter+1);
    tempLabel->SetTextSize(0.07);
    tempLabel->Draw();

}

void GetBackgroundTimes(Int_t RunNumber, Int_t ChainNumber, Int_t BackgroundNumber, Double_t* startTime, Double_t* endTime )
{
    // Get the start of chain runTime
    Double_t startChainTime(0.);
    if(chainNumber>0) startChainTime = Get_RunTime_of_Count(runNumber, SIS2_PBAR_SEQ_START, chainNumber);

    startTime[0] = Get_RunTime_of_Count(runNumber, SIS_BACKGROUND_START, dumpNumber, startChainTime, 9e99);
    endTime[0] = Get_RunTime_of_Count(runNumber, SIS_BACKGROUND_STOP, dumpNumber, startChainTime, 9e99);
}

void GetDumpTimes(Int_t runNumber, Int_t chainNumber, Int_t dumpNumber, Double_t* startTime, Double_t* endTime )
{
    if(dumpNumber==-1) // It's a "WHOLE_RUN"
    {
        startTime[0]=0.;
        endTime[0] = Get_Total_RunTime(runNumber);
        return;
    }

    // Get the start of chain runTime
    Double_t startChainTime(0.);
    if(chainNumber>0) startChainTime = Get_RunTime_of_Count(runNumber, SIS2_PBAR_SEQ_START, chainNumber);

    cout<<"chain "<<chainNumber<<"\t time "<<startChainTime<<endl;
    startTime[0] = Get_RunTime_of_Count(runNumber, SIS_DUMP_START, dumpNumber, startChainTime, 9e9);
    endTime[0] = Get_RunTime_of_Count(runNumber, SIS_DUMP_STOP, dumpNumber, startChainTime, 9e9);
}

Int_t TimeToBinNumber(TH1D* histo, Double_t time)
{
    Int_t i;
    for(i=1; i<=histo->GetNbinsX(); i++)
    {
        if(histo->GetBinCenter(i)>time) break;

    }

    if(i==histo->GetNbinsX())
    {
        Error("TimeToBinNumber", "Could not match time to bin number");
        return -1;
    }

    if(histo->GetBinCenter(i)-time>time-histo->GetBinCenter(i-1)) i--;

    return i;
}

void SplitFilename(char* voltageFile, char* output)
{
    char *voltageFileName;
    char *pch;

    pch = strtok(voltageFile, " /");
    //printf("voltageFile: %s\n", voltageFile);
    while(pch!=NULL)
    {
        //printf("voltageFile: %s\n", voltageFile);
        voltageFileName = pch;
        //printf("voltageFile: %s\n", voltageFile);
        pch = strtok(NULL, " /");
    }

    strcpy(output, voltageFileName);
    return;

}

void SaveCanvasc1(char* filename){
  sprintf(text, "plots/%s.png",filename);

  c1->SaveAs(text);

}

void SaveCanvasEnergy(char* filename){
  sprintf(text, "plots/%s.png",filename);

  EnergyPlots->SaveAs(text);

}
