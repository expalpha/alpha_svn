//
// ExtractQuenchEventNumbers.cpp
// JWS 25/02/2010
//
// AGP 25/10/2010 new function to plot events over time - plotHistoEvent used when PrintQuenchEventNumbers() and PrintPreQuenchBackgroundEventNumbers() are called.
// Plots are desactivated using:PrintQuenchEventNumbers(runNumber,0) and PrintPreQuenchBackgroundEventNumbers(runNumber,0)
// EB: 16/10/2011 if plot==2, then the histograms are stacked
//
//AGP 29/10/2010 summed plots: void PrintQuenchEventNumbers(char *filename) and PrintPreQuenchBackgroundEventNumbers(char *filename)
//
// EB 16/10/2011 plot override options -- binary number
// bit 0 : don't draw new canvas
// bit 1 : don't draw legend
// bit 2 : don't add TPave at side with run numbers.

#include "PlotMixing.cpp"
#include <vector>

Int_t gNBinsPreQuench = 200;
Int_t gNBinsQuench = 200;

Int_t gPlotPCOnly(0);

void SetNBinsQuench(Int_t nBins){
  gNBinsQuench = nBins;
}

void SetNBinsPreQuench(Int_t nBins){
  gNBinsPreQuench = nBins;
}

Bool_t gUSE_TSRUNTIME(kFALSE);

Int_t PrintPreQuenchAndQuenchEventNumbers( Int_t run_number )
{
  printf("\nQuench *********************************************************************************\n\n");

  PrintQuenchEventNumbers( run_number );

  printf("\nPreQuench Background ********************************************************************\n\n");

  PrintPreQuenchBackgroundEventNumbers( run_number );

  printf("\n");
}

Int_t DumpQuenchEvents(Int_t run_number)
{
  TTree* sis_quench_trigger_tree =  Get_Sis_Tree( run_number, 6 );
  if( sis_quench_trigger_tree == NULL ) return -1;
  if( sis_quench_trigger_tree->GetEntries()<1)
  {
     Error("", "No Quench Trigger found in run %d", run_number);
     return 1;
  }
  Double_t sis_quench_trigger_time = Get_RunTime_of_Count( sis_quench_trigger_tree, 1 );

  TTree* sis_quench_stop_trigger_tree =  Get_Sis_Tree( run_number, 7 );
  if( sis_quench_stop_trigger_tree == NULL ) return -1;
  Double_t sis_quench_stop_trigger_time = Get_RunTime_of_Count( sis_quench_stop_trigger_tree, 1 );


// creating file to save quench information
  sprintf(text, "./../events/run%d_info",run_number);
  ofstream fileInfo(text);


  printf("%f -> %f [%f s] \n", sis_quench_trigger_time, sis_quench_stop_trigger_time, (sis_quench_stop_trigger_time - sis_quench_trigger_time) );
  fileInfo <<"\n\n";
  sprintf(text,"%f -> %f [%f s] \n", sis_quench_trigger_time, sis_quench_stop_trigger_time, (sis_quench_stop_trigger_time - sis_quench_trigger_time) );
  fileInfo << text;

  Double_t quench_period(999.);
  if( sis_quench_stop_trigger_time>0. ) quench_period = sis_quench_stop_trigger_time - sis_quench_trigger_time;

  TTree* sil_tree = Get_Vtx_Tree( run_number );
  if( sil_tree == NULL ) return -1;
  TSiliconEvent* sil_event = new TSiliconEvent();
  sil_tree->SetBranchAddress("SiliconEvent", &sil_event);

  TVertexCut* cutCheck = new TVertexCut();
  cutCheck->SetIsActive(1);
  cutCheck->SetName("Trapping");

//creating file to record the events number:
  sprintf(text, "./../events/run%d",run_number);
  ofstream fileEvents(text);



  printf("Run   Event#  Time(ms) NRawHits NHits NTracks NVertices Residual  X      Y      Z_sim   R     Phi_sim    Pass? \n");
  printf("-----------------------------------------------------------------------------------------------------------\n");

  fileInfo << "Run   Event#  Time(ms) NRawHits NHits NTracks NVertices Residual  X      Y      Z_sim   R     Phi_sim    Pass? \n";
  fileInfo << "-----------------------------------------------------------------------------------------------------------\n";

   for( Int_t i=0; i<sil_tree->GetEntries(); i++ )
    {
      sil_event->ClearEvent();
      sil_tree->GetEntry(i);
      if( sil_event->GetRunTime() < sis_quench_trigger_time ) continue;
      if( sil_event->GetRunTime() > sis_quench_stop_trigger_time && sis_quench_stop_trigger_time!=-999. ) break;

      fileEvents << sil_event->GetVF48NEvent() <<"\n"; //event number into the file

      printf("%5d  %4d \t%3.2lf \t%d \t%d \t%d \t%d \t",
             sil_event->GetRunNumber(),
             sil_event->GetVF48NEvent(),
             (sil_event->GetRunTime()-sis_quench_trigger_time)*1000.,
             sil_event->GetNRawHits(),
             sil_event->GetNHits(),
             sil_event->GetNTracks(),
             sil_event->GetNVertices());

      sprintf(text,"%5d  %4d \t%3.2lf \t%d \t%d \t%d \t%d \t",
             sil_event->GetRunNumber(),
             sil_event->GetVF48NEvent(),
             (sil_event->GetRunTime()-sis_quench_trigger_time)*1000.,
             sil_event->GetNRawHits(),
             sil_event->GetNHits(),
             sil_event->GetNTracks(),
              sil_event->GetNVertices());
      fileInfo << text;

      TVector3 * v = sil_event->GetVertex();
      if(sil_event->GetNVertices()>0 )
        {
          printf("%.2lf\t%6.2lf\t%6.2lf\t%6.2lf\t%3.2lf\t%3.1lf", sil_event->GetResidual(), v->X(),v->Y(),-1.*(v->Z()*10.-24.05),v->XYvector().Mod(),v->Phi()*180/TMath::Pi()+6.9);
          sprintf(text,"%.2lf\t%6.2lf\t%6.2lf\t%6.2lf\t%3.2lf\t%3.1lf", sil_event->GetResidual(), v->X(),v->Y(),-1.*(v->Z()*10.-24.05),v->XYvector().Mod(),v->Phi()*180/TMath::Pi()+6.9);
          fileInfo << text;
        }
      else
        {
         printf("- \t  -\t-\t-\t-\t-");
         sprintf(text,"- \t  -\t-\t-\t-\t-");
         fileInfo << text;

        }
        printf("\t%d", cutCheck->CheckInRange(sil_event));
        printf("\n");
        sprintf(text,"\t%d", cutCheck->CheckInRange(sil_event));
        fileInfo << text << "\n";

      //      printf("VF48NEvent = %d \t RunTime = %f \t TSRunTime = %f \t VF48Timestamp = %f \s \t TimeAfterQuench = %f ms \t NHits = %d \t NVertices = %d \n", sil_event->GetVF48NEvent(), sil_event->GetRunTime(), sil_event->GetTSRunTime(), sil_event->GetVF48Timestamp(), (sil_event->GetRunTime()-sis_quench_trigger_time)*1000.,  sil_event->GetNHits(), sil_event->GetNVertices() );
    }
   printf("\n");
   fileInfo << "\n";
   fileInfo.close();
   fileEvents.close();
}

void PrintQuenchEventNumbers( Int_t run_number, Int_t plot,TH1D *timePC=NULL, TH1D *timeFResC=NULL, TH1D *timeFRadC=NULL, TH1D *timeFResRadC=NULL,TH1D *timeNoVertex=NULL,vector<Int_t> listRuns=NULL, char *filename=NULL){

  TTree* sis_quench_trigger_tree =  Get_Sis_Tree( run_number, 6 );
  if( sis_quench_trigger_tree == NULL ) return -1;
  if( sis_quench_trigger_tree->GetEntries()<1)
  {
     Error("", "No Quench Trigger found in run %d", run_number);
     return 1;
  }
  Double_t sis_quench_trigger_time = Get_RunTime_of_Count( sis_quench_trigger_tree, 1 );

  TTree* sis_quench_stop_trigger_tree =  Get_Sis_Tree( run_number, 7 );
  if( sis_quench_stop_trigger_tree == NULL ) return -1;
  Double_t sis_quench_stop_trigger_time = Get_RunTime_of_Count( sis_quench_stop_trigger_tree, 1 );

  printf("%f -> %f [%f s] \n\n", sis_quench_trigger_time, sis_quench_stop_trigger_time, (sis_quench_stop_trigger_time - sis_quench_trigger_time) );
  Double_t quench_period(999.);
  if( sis_quench_stop_trigger_time>0. ) quench_period = sis_quench_stop_trigger_time - sis_quench_trigger_time;


  /// loop test
  // PrintEventNumbers( run_number, pre_quench_background_start_time, (quench_start_time- 0.00999 ), plot);
  //  PrintEventNumbers(run_number,sis_quench_trigger_time, sis_quench_stop_trigger_time,plot,2000);
  if(timePC==NULL && timeFResC==NULL && timeFRadC==NULL && timeFResRadC==NULL && timeNoVertex==NULL){

    TH1D *timePC = new TH1D(Form("h6_%d",run_number),"",gNBinsQuench, 0,(sis_quench_stop_trigger_time-sis_quench_trigger_time));
    TH1D *timeFResC = new TH1D(Form("h7_%d",run_number),"",gNBinsQuench, 0,(sis_quench_stop_trigger_time-sis_quench_trigger_time));
    TH1D *timeFRadC = new TH1D(Form("h8_%d",run_number),"",gNBinsQuench, 0,(sis_quench_stop_trigger_time-sis_quench_trigger_time));
    TH1D *timeFResRadC = new TH1D(Form("h9_%d",run_number),"",gNBinsQuench,0,(sis_quench_stop_trigger_time-sis_quench_trigger_time));
    TH1D *timeNoVertex = new TH1D(Form("h10_%d",run_number),"",gNBinsQuench, 0,(sis_quench_stop_trigger_time-sis_quench_trigger_time));



  }
  // recall 10ms RO deadtime before quench
  PrintEventNumbers(run_number, sis_quench_trigger_time, sis_quench_stop_trigger_time,plot,"QuenchEvents",timePC,timeFResC,timeFRadC,timeFResRadC,timeNoVertex, listRuns, filename);

}

Int_t PrintQuenchEventNumbers( char* filename, Int_t plot = 1){

  Int_t runNumber[1000], dumps[1000];
  Int_t nRuns = FileToRuns(filename, runNumber, dumps);

  Int_t run_number = runNumber[0];
  TTree* sis_quench_trigger_tree =  Get_Sis_Tree( run_number, 6 );
  if( sis_quench_trigger_tree == NULL ) return -1;
  if( sis_quench_trigger_tree->GetEntries()<1)
  {
     Error("", "No Quench Trigger found in run %d", runNumber);
     return 1;
  }
  Double_t sis_quench_trigger_time = Get_RunTime_of_Count( sis_quench_trigger_tree, 1 );

  TTree* sis_quench_stop_trigger_tree =  Get_Sis_Tree( run_number, 7 );
  if( sis_quench_stop_trigger_tree == NULL ) return -1;
  Double_t sis_quench_stop_trigger_time = Get_RunTime_of_Count( sis_quench_stop_trigger_tree, 1 );

  printf("%f -> %f [%f s] \n\n", sis_quench_trigger_time, sis_quench_stop_trigger_time, (sis_quench_stop_trigger_time - sis_quench_trigger_time) );
  Double_t quench_period(999.);
  if( sis_quench_stop_trigger_time>0. ) quench_period = sis_quench_stop_trigger_time - sis_quench_trigger_time;

  TH1D *timePC = new TH1D(Form("h6_%d",run_number),"",gNBinsQuench, 0,(sis_quench_stop_trigger_time-sis_quench_trigger_time));
  TH1D *timeFResC = new TH1D(Form("h7_%d",run_number),"",gNBinsQuench, 0,(sis_quench_stop_trigger_time-sis_quench_trigger_time));
  TH1D *timeFRadC = new TH1D(Form("h8_%d",run_number),"",gNBinsQuench, 0,(sis_quench_stop_trigger_time-sis_quench_trigger_time));
  TH1D *timeFResRadC = new TH1D(Form("h9_%d",run_number),"",gNBinsQuench,0,(sis_quench_stop_trigger_time-sis_quench_trigger_time));
  TH1D *timeNoVertex = new TH1D(Form("h10_%d",run_number),"",gNBinsQuench, 0,(sis_quench_stop_trigger_time-sis_quench_trigger_time));

  vector<Int_t> listRuns;

  for (Int_t i=0;i<nRuns;i++)
    {
      Int_t run = runNumber[i];
      listRuns.push_back(run);
      if(i==(nRuns-1))
        {

          PrintQuenchEventNumbers(run,plot,timePC,timeFResC,timeFRadC,timeFResRadC,timeNoVertex,listRuns, filename);
        }
      else
        {
          PrintQuenchEventNumbers(run,0,timePC,timeFResC,timeFRadC,timeFResRadC,timeNoVertex);
        }
    }
  return 0;
}

void PrintPreQuenchBackgroundEventNumbers( Int_t run_number, Int_t plot, TH1D *timePC=NULL, TH1D *timeFResC=NULL, TH1D *timeFRadC=NULL, TH1D *timeFResRadC=NULL,TH1D *timeNoVertex=NULL,vector<Int_t> listRuns=NULL, char *filename=NULL){

  Double_t quench_start_time = Get_RunTime_of_Count( run_number, SIS_QUENCH_START, 1 );
  if( quench_start_time==-999.)
  {
     Error("", "No Quench Trigger found in run %d", run_number);
     return 1;
  }

  for( Int_t i=1; i<=Get_Total_Sis_Counts( run_number, 30 ); i++ )
    {
      if( fabs( quench_start_time - Get_RunTime_of_Count( run_number, 30, i ) ) < 0.001 )
        {
          // we've hit the quench start of dump flag
          break;
        }
      Double_t pre_quench_background_start_time = Get_RunTime_of_Count( run_number, 30, i );
    }//loop over

  if(timePC==NULL && timeFResC==NULL && timeFRadC==NULL && timeFResRadC==NULL && timeNoVertex==NULL){

    TH1D *timePC = new TH1D(Form("h1_%d",run_number),"",gNBinsPreQuench, 0,(quench_start_time-pre_quench_background_start_time));
    TH1D *timeFResC = new TH1D(Form("h2_%d", run_number),"",gNBinsPreQuench, 0,(quench_start_time-pre_quench_background_start_time));
    TH1D *timeFRadC = new TH1D(Form("h3_%d",run_number),"",gNBinsPreQuench, 0,(quench_start_time-pre_quench_background_start_time));
    TH1D *timeFResRadC = new TH1D(Form("h4_%d",run_number),"",gNBinsPreQuench, 0,(quench_start_time-pre_quench_background_start_time));
    TH1D *timeNoVertex = new TH1D(Form("h5_%d",run_number),"",gNBinsPreQuench, 0,(quench_start_time-pre_quench_background_start_time));
  }
  // recall 10ms RO deadtime before quench

  //printf("Trying to PrintEventNumbers\n");

  if ( (quench_start_time- 0.00999 ) - pre_quench_background_start_time > 0.5 )
  {
  PrintEventNumbers(run_number, pre_quench_background_start_time, (quench_start_time- 0.00999 ), plot,"PreQuenchEvents",timePC,timeFResC,timeFRadC,timeFResRadC,timeNoVertex, listRuns, filename);
  }
  else
  {
    Info("", "There isn't a prequench hold time");
  }

}

Bool_t IsBackGroundDump(Int_t run_number, Int_t dump_number, Double_t quench_start_time=0)
{
    if(quench_start_time==0)
    {
        quench_start_time = Get_RunTime_of_Count( run_number, SIS_QUENCH_START, 1 );
    }

    Double_t start_time, end_time;

    GetDumpTimes(run_number, 1, dump_number, &start_time, &end_time);

    if(end_time > quench_start_time || end_time<0) return kFALSE;
    if(end_time -start_time < 1.) return kFALSE;

    // we know that it's before the quench. If there are any _short_ dumps (i.e < 1s) between this and the quench, then it's not a background (getto, I know)

    while(true)
    {
        dump_number++;
        GetDumpTimes(run_number, 1, dump_number, &start_time, &end_time);

        if(start_time>=quench_start_time) return kTRUE; // we've reached the quench without encountering a short dump! Yay!

        if(end_time-start_time < 1.) return kFALSE; // this is a short dump! And it's between the dump we were asked about and the quench dump! Therefore it's not a background!

        if(start_time <0 || end_time <0 ) return kFALSE; // this shouldn't happen, but check anyway;
    }
}

Int_t PlotManyPreQuenchBackgrounds( char* filename, Int_t plot = 1)
{
  Int_t runNumber[1000], dumps[1000];
  Int_t nRuns = FileToRuns(filename, runNumber, dumps);
  Int_t run_number = runNumber[0];
  Double_t quench_start_time = Get_RunTime_of_Count( run_number, SIS_QUENCH_START, 1 );
  Double_t arTrigger_time = Get_RunTime_of_Count( run_number, SIS2_AR_TRIGGER, Count_SIS_Triggers(run_number, SIS2_AR_TRIGGER)); // for reasons passing comprehension, this channel is or-ed with something else... We only want the _last_ trigger (usually. I think)

  Int_t nDumps = Count_SIS_Triggers(run_number, SIS_DUMP_START);
  Int_t nDumps_toplot(0), nPlot(0);
  Double_t start_time, end_time;
  for (Int_t i=1; i<=nDumps; i++)
    {
      // looking for _long_ dumps between the mixing dump and the quench dump
      if(IsBackGroundDump(run_number, i, quench_start_time))
        {
          nDumps_toplot++;
        }
    }

  cout << "Found " << nDumps_toplot << " Dumps to plot in Run " << run_number << endl;

  if(nDumps_toplot==0)
    {
      Info("PrintManyPreQuenchBackgrounds", "no dumps to plot");
      return;
    }

  TString title="";
  title+=filename;
  title+="_PreQuenchBackgrounds";
  TCanvas* c1 = new TCanvas("c1", title, 1200, 300*nDumps_toplot+300*(nDumps_toplot==1));
  c1->cd();
  TPad* c = new TPad("c", "c", 0., 0., 1.0, 1.);
  c->Draw();
  c->Divide(1,nDumps_toplot, 0.001, 0.001);

  TH1D *timePC, *timeFResC, *timeRadC, *timeFResRadC, *timeNoVertex;
  Double_t dump_time;
  TString dump_name;

  vector<Int_t> listRuns;
  listRuns.clear();
  for (Int_t i=1; i<=nDumps; i++)
    {
      // looking for _long_ dumps between the mixing dump and the quench dump
      GetDumpTimes(run_number, 1, i, &start_time, &end_time);
      if(!IsBackGroundDump(run_number, i, quench_start_time))  continue;
      listRuns.push_back(run_number);
      nPlot++;
      c->cd(nPlot);

      dump_time = end_time-start_time;

      // the gNBinsPreQuench * dump_time/100. is to keep the bin size constant if the length of the dumps is different

      timePC = new TH1D(Form("h1_%d_%d",run_number,nPlot),"",gNBinsPreQuench * dump_time/100., 0, dump_time);
      if(nDumps_toplot>1)
        {
          timePC->SetTitleSize(0.06, "XY");
          timePC->SetLabelSize(0.06, "XY");
          timePC->SetTitleOffset(+0.7, "XY");
        }
      timeFResC  = (TH1D*)timePC->Clone(Form("h2_%d_%d",run_number,nPlot));
      timeFRadC  = (TH1D*)timePC->Clone(Form("h3_%d_%d",run_number,nPlot));
      timeFResRadC  = (TH1D*)timePC->Clone(Form("h4_%d_%d",run_number,nPlot));
      timeNoVertex  = (TH1D*)timePC->Clone(Form("h5_%d_%d",run_number,nPlot));

      dump_name = MatchDumpNumberToDumpName(run_number, i);
      cout << "Created histograms for plot: " << nPlot << " run: " << run_number << " dumptime: " << dump_time << endl;
      PrintEventNumbers(run_number, start_time, end_time, 0, Form("%s", dump_name.Data()),timePC,timeFResC,timeFRadC,timeFResRadC,timeNoVertex);

      for (Int_t j=1;j<nRuns;j++)
      {
        Int_t run = runNumber[j];
        listRuns.push_back(run);
        if(j==(nRuns-1))
        {
          cout << "Adding final run: " << run << " plot: " << nPlot << endl;
          GetDumpTimes(run, 1, i, &start_time, &end_time);
          PrintEventNumbers(run, start_time, end_time, plot, Form("%s", dump_name.Data()),timePC,timeFResC,timeFRadC,timeFResRadC,timeNoVertex, listRuns, filename, 1+2*0+4*0);
        }
        else
        {
          GetDumpTimes(run, 1, i, &start_time, &end_time);
          cout << "Adding run: " << run << " plot: " << nPlot << endl;
          PrintEventNumbers(run, start_time, end_time, 0, Form("%s", dump_name.Data()),timePC,timeFResC,timeFRadC,timeFResRadC,timeNoVertex);
        }
      }

      listRuns.clear();
    }

   TString imageName="plots/";
  imageName+=title;
  if(gPlotPCOnly) imageName+="_PC";
  imageName+=".pdf";
  c1->SaveAs(imageName);
  cout << "Saved: " << imageName << endl;

  return 0;
}

void PlotManyPreQuenchBackgrounds( Int_t run_number, Int_t plot, TH1D *timePC=NULL, TH1D *timeFResC=NULL, TH1D *timeFRadC=NULL, TH1D *timeFResRadC=NULL,TH1D *timeNoVertex=NULL,vector<Int_t> listRuns=NULL, char *filename=NULL){

    Double_t quench_start_time = Get_RunTime_of_Count( run_number, SIS_QUENCH_START, 1 );
    Double_t arTrigger_time = Get_RunTime_of_Count( run_number, SIS2_AR_TRIGGER, Count_SIS_Triggers(run_number, SIS2_AR_TRIGGER)); // for reasons passing comprehension, this channel is or-ed with something else... We only want the _last_ trigger (usually. I think)

    Int_t nDumps = Count_SIS_Triggers(run_number, SIS_DUMP_START);
    Int_t nDumps_toplot(0), nPlot(0);
    Double_t start_time, end_time;
    for (Int_t i=1; i<=nDumps; i++)
    {
        // looking for _long_ dumps between the mixing dump and the quench dump
        if(IsBackGroundDump(run_number, i, quench_start_time))
        {
            nDumps_toplot++;
        }

    }

    if(nDumps_toplot==0)
      {
        Info("PrintManyPreQuenchBackgrounds", "no dumps to plot");
        return;
      }

    if (plot == 2)
      {
        TString title="run";
        title+=run_number;
        title+="_PreQuenchBackgrounds";
        TCanvas* c1 = new TCanvas("c1", title, 1200, 300*nDumps_toplot+300*(nDumps_toplot==1));
        c1->cd();
        TPad* c = new TPad("c", "c", 0., 0., 1.0, 1.);
        c->Draw();
        c->Divide(1,nDumps_toplot, 0.001, 0.001);

        TH1D *timePC, *timeFResC, *timeRadC, *timeFResRadC, *timeNoVertex;
      }
    Double_t dump_time;
    TString dump_name;


      for (Int_t i=1; i<=nDumps; i++)
        {
          // looking for _long_ dumps between the mixing dump and the quench dump
          GetDumpTimes(run_number, 1, i, &start_time, &end_time);
          if(!IsBackGroundDump(run_number, i, quench_start_time)) continue;

          nPlot++;
          c->cd(nPlot);

          dump_time = end_time-start_time;

          // the gNBinsPreQuench * dump_time/100. is to keep the bin size constant if the length of the dumps is different
          if (plot == 2)
            {
              timePC = new TH1D(Form("h1_%d_%d",run_number,nPlot),"",gNBinsPreQuench * dump_time/100., 0, dump_time);
              if(nDumps_toplot>1)P
                {
                  timePC->SetTitleSize(0.06, "XY");
                  timePC->SetLabelSize(0.06, "XY");
                  timePC->SetTitleOffset(+0.7, "XY");
                }
              timeFResC  = (TH1D*)timePC->Clone(Form("h2_%d_%d",run_number,nPlot));
              timeFRadC  = (TH1D*)timePC->Clone(Form("h3_%d_%d",run_number,nPlot));
              timeFResRadC  = (TH1D*)timePC->Clone(Form("h4_%d_%d",run_number,nPlot));
              timeNoVertex  = (TH1D*)timePC->Clone(Form("h5_%d_%d",run_number,nPlot));
            }
          dump_name = MatchDumpNumberToDumpName(run_number, i);
        cout << " Doing plot: " << nPlot << " Dump: " << i << endl;
        PrintEventNumbers(run_number, start_time, end_time, plot, Form("%s", dump_name.Data()),timePC,timeFResC,timeFRadC,timeFResRadC,timeNoVertex, listRuns, filename, 1+2*0+4);

        if(plot==2)
          {
            //c->cd(1);
          }
      }

    if(plot == 2)
      {
        TString imageName="plots/";
        imageName+=title;
        imageName+=".pdf";
        c1->SaveAs(imageName);
      }
}

Int_t PrintPreQuenchBackgroundEventNumbers( char * filename){

  Int_t runNumber[1000], dumps[1000];
  Int_t nRuns = FileToRuns(filename, runNumber, dumps);

  Int_t run_number = runNumber[0];
  Double_t quench_start_time = Get_RunTime_of_Count( run_number, SIS_QUENCH_START, 1 );
  if( quench_start_time==-999.)
  {
     Error("", "No Quench Trigger found in run %d", run_number);
     return 1;
  }


  for( Int_t i=1; i<=Get_Total_Sis_Counts( run_number, 30 ); i++ )
    {
      if( fabs( quench_start_time - Get_RunTime_of_Count( run_number, 30, i ) ) < 0.001 )
        {
          // we've hit the quench start of dump flag
          break;
        }
      Double_t pre_quench_background_start_time = Get_RunTime_of_Count( run_number, 30, i );
    }//loop over


  TH1D *timePC = new TH1D(Form("h1_%d",run_number),"",gNBinsPreQuench, 0,(quench_start_time-pre_quench_background_start_time));
  TH1D *timeFResC = new TH1D(Form("h2_%d", run_number),"",gNBinsPreQuench, 0,(quench_start_time-pre_quench_background_start_time));
  TH1D *timeFRadC = new TH1D(Form("h3_%d",run_number),"",gNBinsPreQuench, 0,(quench_start_time-pre_quench_background_start_time));
  TH1D *timeFResRadC = new TH1D(Form("h4_%d",run_number),"",gNBinsPreQuench, 0,(quench_start_time-pre_quench_background_start_time));
  TH1D *timeNoVertex = new TH1D(Form("h5_%d",run_number),"",gNBinsPreQuench, 0,(quench_start_time-pre_quench_background_start_time));

  vector<Int_t> listRuns;
  Int_t plot;

  for (Int_t i=0;i<nRuns;i++){
    Int_t run = runNumber[i];
    listRuns.push_back(run);
    if(i==(nRuns-1)){
      plot=1;
      PrintPreQuenchBackgroundEventNumbers(run,plot,timePC,timeFResC,timeFRadC,timeFResRadC,timeNoVertex,listRuns,filename);
    }
    else{
      plot=0;
      PrintPreQuenchBackgroundEventNumbers(run,plot,timePC,timeFResC,timeFRadC,timeFResRadC,timeNoVertex);
    }
  }
  return 0;
}

Int_t PrintEventNumbers( Int_t run_number, Double_t sis_quench_trigger_time=0., Double_t sis_quench_stop_trigger_time=99999., Int_t plot, char* plotTitle,TH1D *timePC=NULL, TH1D *timeFResC=NULL, TH1D *timeFRadC=NULL, TH1D *timeFResRadC=NULL,TH1D *timeNoVertex=NULL, vector<Int_t> listRuns=NULL, char *filename, Int_t plotOpts = 0){

  //printf("%f -> %f [%f s] \n\n", sis_quench_trigger_time, sis_quench_stop_trigger_time, (sis_quench_stop_trigger_time - sis_quench_trigger_time) );

   TString title(plotTitle);


  TTree* sil_tree = Get_Vtx_Tree( run_number );
  if( sil_tree == NULL ) return -1;
  TSiliconEvent* sil_event = new TSiliconEvent();
  sil_tree->SetBranchAddress("SiliconEvent", &sil_event);


  vector<Int_t> preQuenchPassed;

  TVertexCut* cutCheck = new TVertexCut();


  cutCheck->SetIsActive(1);

  cutCheck->SetName("Trapping");

  // adding information to the file ./../events/runxxx_info

  sprintf(text, "./../events/run%d_info",run_number);
  ofstream fileInfo(text,ios::app);

  char cutTest[100];
  sprintf(cutTest, "RunTime>=%lf && RunTime<=%lf", sis_quench_trigger_time,sis_quench_stop_trigger_time);
  sil_tree->Draw(">>eventList", cutTest);
  TEventList* eventList = (TEventList*)gDirectory->Get("eventList");


   for(Int_t j=0; j<eventList->GetN(); j++) // less memory intensive getting of data -- Eoin
  //for( Int_t i=0; i<sil_tree->GetEntries(); i++ )
    {
        Int_t i = eventList->GetEntry(j);
      //printf("Entry %d\n\n", i);
      sil_event->ClearEvent();
      sil_tree->GetEntry(i);

    if(gUSE_TSRUNTIME)
      {
          // for debugging purposes EB
         if( sil_event->GetTSRunTime() < sis_quench_trigger_time ) continue;
      if( sil_event->GetTSRunTime() > sis_quench_stop_trigger_time && sis_quench_stop_trigger_time!=-999. ) break;
      }
      else
      {
          if( sil_event->GetRunTime() < sis_quench_trigger_time ) continue;
         if( sil_event->GetRunTime() > sis_quench_stop_trigger_time && sis_quench_stop_trigger_time!=-999. ) break;
     }

      //      printf("VF48NEvent = %d \t RunTime = %f \t TSRunTime = %f \t VF48Timestamp = %f \s \t TimeAfterQuench = %f ms \t NHits = %d \t NVertices = %d \n", sil_event->GetVF48NEvent(), sil_event->GetRunTime(), sil_event->GetTSRunTime(), sil_event->GetVF48Timestamp(), (sil_event->GetRunTime()-sis_quench_trigger_time)*1000.,  sil_event->GetNHits(), sil_event->GetNVertices() );

      Double_t simz = 0;
      if( sil_event->GetNVertices() )
        simz = -1.*(sil_event->GetVertex()->Z()*10.-24.05);

      printf("\n\033[44m\033[1;37mVF48NEvent = %d \t VF48Time = %4.3f \t TimeAfterQuench = %3.3f ms\033[00m \n\tNHits = %d \t NVertices = %d\n" ,
             sil_event->GetVF48NEvent(),
             sil_event->GetVF48Timestamp(),
             (sil_event->GetRunTime()-sis_quench_trigger_time)*1000.,
             sil_event->GetNHits(),
             sil_event->GetNVertices());
      sprintf(text,"\nVF48NEvent = %d \t VF48Time = %4.3f \t TimeAfterQuench = %3.3f ms \n\tNHits = %d \t NVertices = %d\n" ,
             sil_event->GetVF48NEvent(),
             sil_event->GetVF48Timestamp(),
             (sil_event->GetRunTime()-sis_quench_trigger_time)*1000.,
             sil_event->GetNHits(),
             sil_event->GetNVertices());
      fileInfo << text;
      if( sil_event->GetNVertices() == 0 )
        {
          printf("\033[1;31m\tNo Vertex\033[00m\n");
          timeNoVertex->Fill(sil_event->GetRunTime()-sis_quench_trigger_time);
          sprintf(text,"\tNo Vertex\n");
          fileInfo << text;
          continue;
        }

      printf("\tVertex.Z() \t= %3.3fcm \n\tSimulation Z \t= %3.3lfmm\n",
             sil_event->GetVertex()->Z(),
             simz );
      sprintf(text,"\tVertex.Z() \t= %3.3fcm \n\tSimulation Z \t= %3.3lfmm\n",
             sil_event->GetVertex()->Z(),
             simz );
      fileInfo << text;

      TVector3 * v = sil_event->GetVertex();
      Double_t R = v->XYvector().Mod();

      printf("\tSimulation Phi \t= %3.1fdeg\n", v->Phi()*180/TMath::Pi()+6.9);
      sprintf(text,"\tSimulation Phi \t= %3.1fdeg\n", v->Phi()*180/TMath::Pi()+6.9);
      fileInfo << text;
      Bool_t passed = kTRUE;
      Int_t fRad = 0;
      Int_t fRes = 0;

      printf("\tR = %lfcm\n",R);
      sprintf(text,"\tR = %lfcm\n",R);
      fileInfo << text;
      if(R>4)
        {
          printf("\t\033[1;31m--- FAILED RADIUS CUT ---\033[00m\n");
          passed = kFALSE;
          sprintf(text,"\t--- FAILED RADIUS CUT ---\n");
          fileInfo << text;
          fRad++;
        }

      printf("\tNTracks = %d\n", sil_event->GetNTracks() );
      printf("\tResidual = %lf\n", sil_event->GetResidual() );

      sprintf(text,"\tNTracks = %d\n", sil_event->GetNTracks() );
      fileInfo << text;
      sprintf(text,"\tResidual = %lf\n", sil_event->GetResidual() );
      fileInfo << text;

      if( sil_event->GetNTracks() == 2 && sil_event->GetResidual() < 2. )
        {
          printf("\t\033[1;31m--- FAILED RESIDUAL CUT ---\033[00m\n");
          passed = kFALSE;
          sprintf(text,"\t--- FAILED RESIDUAL CUT ---\n");
          fileInfo << text;
          fRes++;
        }

      if( sil_event->GetNTracks() > 2 && sil_event->GetResidual() < 0.05 )
        {
          printf("\t\033[1;31m-- FAILED RESIDUAL CUT ---\033[00m\n");
          passed = kFALSE;
          sprintf(text,"\t-- FAILED RESIDUAL CUT ---\n");
          fileInfo << text;
          fRes++;
        }

      if(passed)
        {
          printf("\t\033[1;32m--- PASSED CUTS ---\033[00m\n");
          timePC->Fill(sil_event->GetRunTime()-sis_quench_trigger_time);

          sprintf(text,"\t\--- PASSED CUTS ---\n");
          fileInfo << text;
          if(title.CompareTo("PreQuenchEvents")==0){
            //std::cout << "aaqui" << std::endl;
            preQuenchPassed.push_back(i);
          }
        }

      if((fRes>0) && (fRad>0))
        timeFResRadC->Fill(sil_event->GetRunTime()-sis_quench_trigger_time);
      if((fRes>0) && (fRad==0))
        timeFResC->Fill(sil_event->GetRunTime()-sis_quench_trigger_time);
      if((fRad>0) && (fRes==0))
        timeFRadC->Fill(sil_event->GetRunTime()-sis_quench_trigger_time);

      //printf("TVertexCut result = %d \n", myCut->CheckIn2009Cuts( sil_event ) );

    }//loop over sil_tree entries

  if(strcmp(title, "PreQuenchEvents")==0){
    printf("\nPre-Quench Passed Cuts information:\nRun   Event#  Time(s) \tNRawHits NHits NTracks NVertices Residual  X      Y      Z_sim   R     Phi_sim    Pass? \n");
    printf("-----------------------------------------------------------------------------------------------------------\n");
    for (Int_t j = 0; j<preQuenchPassed.size(); j++){

      sil_event->ClearEvent();
      sil_tree->GetEntry(preQuenchPassed[j]);

      printf("%5d  %4d \t%3.2lf \t%d \t%d \t%d \t%d \t",
             sil_event->GetRunNumber(),
             sil_event->GetVF48NEvent(),
             (sil_event->GetRunTime()-sis_quench_trigger_time),
             sil_event->GetNRawHits(),
             sil_event->GetNHits(),
             sil_event->GetNTracks(),
             sil_event->GetNVertices());
      TVector3 * v = sil_event->GetVertex();
      if(sil_event->GetNVertices()>0 )
        {
          printf("%.2lf\t%6.2lf\t%6.2lf\t%6.2lf\t%3.2lf\t%3.1lf", sil_event->GetResidual(), v->X(),v->Y(),-1.*(v->Z()*10.-24.05),v->XYvector().Mod(),v->Phi()*180/TMath::Pi()+6.9);
        }
      else
        {
          printf("- \t  -\t-\t-\t-\t-");

        }
      printf("\t%d", cutCheck->CheckInRange(sil_event));
      printf("\n");
    }
 }

  if(plot>0){
    sprintf(text, "./../events/%s_info",filename);
    ofstream fileInfoSummary(text,ios::app);
    fileInfoSummary << "Passed cuts summary for " << title << "\n";
    fileInfoSummary << "Number of runs: " << listRuns.size()<< "\n";
    fileInfoSummary << "Number of pass cuts: " << timePC->GetEntries() << "\n";
    fileInfoSummary.close();
    fileInfo.close();
  }
  if((timePC->GetEntries()==0) && (timeFResC->GetEntries()==0) &&(timeFRadC->GetEntries()==0) &&(timeFResRadC->GetEntries()==0) && (timeNoVertex->GetEntries()==0))
    plot=0;

  if(plot>0)
    PlotHistoEvent(sis_quench_trigger_time, sis_quench_stop_trigger_time,title, run_number, timePC,timeFResC, timeFRadC, timeFResRadC, timeNoVertex, listRuns, filename, plotOpts, (plot==2));
     return 0;
}

void PlotHistoEvent(Double_t start_time, Double_t stop_time,  TString plotName, Int_t runNumber, TH1D *timePC=NULL, TH1D *timeFResC=NULL, TH1D *timeFRadC=NULL, TH1D *timeFResRadC=NULL,TH1D *timeNoVertex=NULL, vector<Int_t> listRuns=NULL, char *filename=NULL, Int_t plotOpts = 0, Bool_t stack = kFALSE){

gStyle->SetLineStyleString(11,"4 100000");

  if(listRuns == NULL){
    listRuns.push_back(runNumber);
  }
  //TString title=plotName;
  TString title="run";
  if(filename==NULL)
    title+=runNumber;
  else
    title=filename;

  title+="_";
  title+=plotName;

  // EB interpret the plot options
  Bool_t drawRunList = (plotOpts%4==plotOpts);
  plotOpts-=!(plotOpts%4==plotOpts)*4;
  Bool_t drawLegend = (plotOpts%2==plotOpts);
  plotOpts-=!(plotOpts%2==plotOpts)*2;
  Bool_t newCanvas = (plotOpts%1==plotOpts);
  plotOpts-=!(plotOpts%1==plotOpts)*1;


  if(newCanvas) TCanvas* c = new TCanvas("c",title,1000,600);
  else TPad* c = gPad;

  Float_t maxBin=0;
  Int_t total = timePC->GetEntries()+timeFResC->GetEntries()+timeFRadC->GetEntries()+timeFResRadC->GetEntries()+timeNoVertex->GetEntries();
  TString header = "Total Events = ";
  header+=total;
  header+=" -- ";
  header+=listRuns.size();
  header+=" runs";
  Float_t rate = total/(stop_time - start_time)/listRuns.size();
  Float_t drate =  TMath::Sqrt(total)/(stop_time - start_time)/listRuns.size();
  sprintf(text," [ %.02f +/- %.02f Hz] ",rate,drate);
  //header+=" [";
  header+= text;
  //header+=" Hz]";
  Double_t ww = (Double_t) gPad->GetWw();
  Double_t wh = (Double_t) gPad->GetWh();
  if(newCanvas)
  {
      leg = new TLegend(0.67,0.84,1,1);
  }
  else
  {
    leg = new TLegend((ww-450.)/ww, (wh-600.)/wh, (ww-50)/ww, (wh-0.)/wh);
  }
  leg->SetHeader(header);

  timeNoVertex->SetFillColor(kGray+2);
  timeNoVertex->SetLineColor(kGray+2);
  timeNoVertex->SetLineStyle(11);

  TString legNV = "No Vertex = ";
  legNV+=timeNoVertex->GetEntries();

  if(maxBin < timeNoVertex->GetMaximum())
    maxBin = timeNoVertex->GetMaximum();

  //passed cuts
  if(timePC!=NULL){
    timePC-> SetFillColor(2);
    timePC-> SetLineColor(2);
    timePC->SetLineStyle(11);
    rate = (timePC->GetEntries()/(stop_time-start_time))/listRuns.size()*1000;
    drate = (TMath::Sqrt(timePC->GetEntries())/(stop_time-start_time))/listRuns.size()*1000;
    TString legPC = "Passed Cuts = ";
    legPC+=timePC->GetEntries();
    sprintf(text," [ %.02f +/- %.02f mHz] ",rate,drate);
    legPC+=text;
    leg->AddEntry(timePC,legPC,"f");
    if(maxBin < timePC->GetMaximum())
      maxBin = timePC->GetMaximum();

  }

//failed residual and radius cuts
  if(timeFResRadC!=NULL){
    timeFResRadC->SetFillColor(kCyan+2);
    timeFResRadC->SetLineColor(kCyan+2);
    timeFResRadC->SetLineStyle(11);


    TString legFResRad = "Failed Residual & Radius Cuts = ";
    legFResRad+=timeFResRadC->GetEntries();

    rate = (timeFResRadC->GetEntries()/(stop_time-start_time))/listRuns.size();
    drate = (TMath::Sqrt(timeFResRadC->GetEntries())/(stop_time-start_time))/listRuns.size();
    sprintf(text," [ %.02f +/- %.02f Hz] ",rate,drate);
    legFResRad+=text;
    leg->AddEntry(timeFResRadC,legFResRad,"f");

    if(maxBin < timeFResRadC->GetMaximum())
      maxBin = timeFResRadC->GetMaximum();

  }


  //failed radius cuts
  if(timeFRadC!=NULL){
    timeFRadC->SetFillColor(kGreen-7);
    timeFRadC->SetLineColor(kGreen-7);
    timeFRadC->SetLineStyle(11);

    TString legFRad = "Failed Radius Cuts = ";
    legFRad+=timeFRadC->GetEntries();
    rate = (timeFRadC->GetEntries()/(stop_time-start_time))/listRuns.size();
    drate = (TMath::Sqrt(timeFRadC->GetEntries())/(stop_time-start_time))/listRuns.size();
    sprintf(text," [ %.02f +/- %.02f Hz] ",rate,drate);
    legFRad+=text;
    leg->AddEntry(timeFRadC,legFRad,"f");

    if(maxBin < timeFRadC->GetMaximum())
      maxBin = timeFRadC->GetMaximum();

  }

 //failed residual cuts
  if(timeFResC!=NULL){
    timeFResC-> SetFillColor(4);
    timeFResC-> SetLineColor(4);
    timeFResC->SetLineStyle(11);

    TString legFResC = "Failed Residual Cuts = ";
    legFResC+=timeFResC->GetEntries();
    rate = (timeFResC->GetEntries()/(stop_time-start_time))/listRuns.size();
    drate = (TMath::Sqrt(timeFResC->GetEntries())/(stop_time-start_time))/listRuns.size();
    sprintf(text," [ %.02f +/- %.02f Hz] ",rate,drate);
    legFResC+=text;
    leg->AddEntry(timeFResC,legFResC,"f");

   if(maxBin < timeFResC->GetMaximum())
      maxBin = timeFResC->GetMaximum();

  }

  if (stack)
  {
  THStack* hstack = new THStack("hstacks", "");
  hstack->Add(timePC);
  hstack->Add(timeFRadC);
  hstack->Add(timeFResC);
  hstack->Add(timeFResRadC);
  hstack->Add(timeNoVertex);

  hstack->Draw("");

  hstack->GetXaxis()->SetTitle("Time [s]");
  hstack->GetYaxis()->SetTitle("Counts");

  hstack->Draw(""); // draw again to get the titles
  if(gPlotPCOnly) hstack->SetMaximum(ceil(timePC->GetMaximum()*1.5));

   AddRandomTextWidth("Histograms are STACKED", kRed, 0.6, 0.2, 6);
 }
 else
 {
  timeNoVertex->Draw();
  if(timeFResRadC!=NULL) timeFResRadC->Draw("same");
  if(timeFRadC!=NULL) timeFRadC->Draw("same");
  if(timeFResC!=NULL) timeFResC->Draw("same");
  if(timePC!=NULL) timePC->Draw("same");//Passed cuts: last histogram to be plot
 }





  if(timeNoVertex!=NULL){
    rate = (timeNoVertex->GetEntries()/(stop_time-start_time))/listRuns.size();
    drate = (TMath::Sqrt(timeNoVertex->GetEntries())/(stop_time-start_time))/listRuns.size();
    sprintf(text," [ %.02f +/- %.02f Hz] ",rate,drate);
    legNV+=text;
    leg->AddEntry(timeNoVertex,legNV,"f"); //last in the legend
  }
  timeNoVertex->SetStats(0);
  timeNoVertex->GetXaxis()->SetTitle("Time [s]");
  maxBin = maxBin+0.75;
  timeNoVertex->GetYaxis()->SetRangeUser(0,maxBin);

  TH1D *timeVertex = new TH1D(Form("h6_%d_vertex",runNumber),"",timePC->GetNbinsX(), 0, (stop_time-start_time));
  if(timePC != NULL)
    timeVertex->Add(timePC);
  if(timeFResC != NULL)
    timeVertex->Add(timeFResC);
  if(timeFRadC != NULL)
    timeVertex->Add(timeFRadC);
  if(timeFResRadC != NULL)
    timeVertex->Add(timeFResRadC);
  TString legV= " Vertex = ";
  // std::cout << "ici: " << timeVertex->GetEntries()<< std::endl;
  legV+=timeVertex->GetEntries();
  rate = (timeVertex->GetEntries()/(stop_time-start_time))/listRuns.size();
  drate = (TMath::Sqrt(timeVertex->GetEntries())/(stop_time-start_time))/listRuns.size();
  sprintf(text," [ %.02f +/- %.02f Hz] ",rate,drate);
  legV+=text;
  leg->AddEntry(timeVertex,legV,"");


  TPaveText * pt2 = new TPaveText(0.0,0.91,0.5,1,"NDC");
  pt2->SetFillColor(0);
  pt2->AddText(0.5,0.5,title);
  pt2->Draw();

  leg->SetFillColor(0);
  c->SetFillColor(0);
  c->SetBorderMode(0);
  c->SetFrameBorderMode(0);
  if(drawLegend) leg->Draw();

  if(listRuns!=NULL){
    TPaveText * pt3 = new TPaveText(0.91,0.1,1,0.83,"NDC");
    pt3->SetFillColor(0);
    TString hRuns=" ";
    hRuns+=listRuns.size();
    hRuns+=" runs:";
    pt3->AddText(hRuns);
    if(listRuns.size()<31){
      for( Int_t i=0;i<listRuns.size();i++){
          TString runs = " ";
          runs+= listRuns[i];
          pt3->AddText(runs);

        }
             }
      else{
        for( Int_t i= 0; i < 13; i++){
            TString runs = " ";
            runs+= listRuns[i];
            pt3->AddText(runs);

          }
               pt3->AddText("  -  ");
             pt3->AddText("  -  ");
             pt3->AddText("  -  ");

             for(Int_t i=(listRuns.size()-13);i<listRuns.size();i++){
            TString runs = " ";
            runs+= listRuns[i];
            pt3->AddText(runs);

          }
              }
    if(drawRunList) pt3->Draw();
  }

  TString imageName="plots/";
  //imageName+=plotName;
  //imageName+=runNumber;
  imageName+=title;
  imageName+=".png";
  if(newCanvas) c->SaveAs(imageName);

}

Int_t PrintQuenchEventNumbers( Int_t run_number )
{
  TTree* sis_quench_trigger_tree =  Get_Sis_Tree( run_number, 6 );
  if( sis_quench_trigger_tree == NULL ) return -1;
  if(sis_quench_trigger_tree->GetEntries()<1)
  {
      Error("", "No quench trigger");
      return -1;
   }

  Double_t sis_quench_trigger_time = Get_RunTime_of_Count( sis_quench_trigger_tree, 1 );

  TTree* sis_quench_stop_trigger_tree =  Get_Sis_Tree( run_number, 7 );
  if( sis_quench_stop_trigger_tree == NULL ) return -1;
  Double_t sis_quench_stop_trigger_time = Get_RunTime_of_Count( sis_quench_stop_trigger_tree, 1 );

  printf("%f -> %f [%f s] \n\n", sis_quench_trigger_time, sis_quench_stop_trigger_time, (sis_quench_stop_trigger_time - sis_quench_trigger_time) );
  Double_t quench_period(999.);
  if( sis_quench_stop_trigger_time>0. ) quench_period = sis_quench_stop_trigger_time - sis_quench_trigger_time;

  TTree* sil_tree = Get_Vtx_Tree( run_number );
  if( sil_tree == NULL ) return -1;
  TSiliconEvent* sil_event = new TSiliconEvent();
  sil_tree->SetBranchAddress("SiliconEvent", &sil_event);

  for( Int_t i=0; i<sil_tree->GetEntries(); i++ )
    {
      sil_event->ClearEvent();
      sil_tree->GetEntry(i);

            if(gUSE_TSRUNTIME)
      {
          // for debugging purposes EB
                if( sil_event->GetTSRunTime() < sis_quench_trigger_time ) continue;
                if( sil_event->GetTSRunTime() > sis_quench_stop_trigger_time && sis_quench_stop_trigger_time!=-999. ) break;
      }
      else
      {
                if( sil_event->GetRunTime() < sis_quench_trigger_time ) continue;
      if( sil_event->GetRunTime() > sis_quench_stop_trigger_time && sis_quench_stop_trigger_time!=-999. ) break;
     }

      if( sil_event->GetRunTime() < sis_quench_trigger_time ) continue;
      if( sil_event->GetRunTime() > sis_quench_stop_trigger_time && sis_quench_stop_trigger_time!=-999. ) break;

      //      printf("VF48NEvent = %d \t RunTime = %f \t TSRunTime = %f \t VF48Timestamp = %f \s \t TimeAfterQuench = %f ms \t NHits = %d \t NVertices = %d \n", sil_event->GetVF48NEvent(), sil_event->GetRunTime(), sil_event->GetTSRunTime(), sil_event->GetVF48Timestamp(), (sil_event->GetRunTime()-sis_quench_trigger_time)*1000.,  sil_event->GetNHits(), sil_event->GetNVertices() );

      Double_t simz = 0;
      if( sil_event->GetNVertices() )
        simz = -1.*(sil_event->GetVertex()->Z()*10.-24.05);

      printf("\033[44m\033[1;37mVF48NEvent = %d \t VF48Time = %4.3f \t TimeAfterQuench = %3.3f ms\033[00m \n\tNHits = %d \t NVertices = %d\n" ,
             sil_event->GetVF48NEvent(),
             sil_event->GetVF48Timestamp(),
             (sil_event->GetRunTime()-sis_quench_trigger_time)*1000.,
             sil_event->GetNHits(),
             sil_event->GetNVertices());

      if( sil_event->GetNVertices() == 0 )
        {
          printf("\033[1;31m\tNo Vertex\033[00m\n");
          continue;
        }

      printf("\tVertex.Z() \t= %3.3fcm \n\tSimulation Z \t= %3.3lfmm\n",
             sil_event->GetVertex()->Z(),
             simz );

      TVector3 * v = sil_event->GetVertex();
      Double_t R = v->XYvector().Mod();

      printf("\tSimulation Phi \t= %3.1fdeg\n", v->Phi()*180/TMath::Pi()+6.9);

      Bool_t passed = kTRUE;

      printf("\tR = %lfcm\n",R);
      if(R>4)
        {
          printf("\t\033[1;31m--- FAILED RADIUS CUT ---\033[00m\n");
          passed = kFALSE;
              }

      printf("\tNTracks = %d\n", sil_event->GetNTracks() );
      printf("\tResidual = %lf\n", sil_event->GetResidual() );

      if( sil_event->GetNTracks() == 2 && sil_event->GetResidual() < 2. )
        {
          printf("\t\033[1;31m--- FAILED RESIDUAL CUT ---\033[00m\n");
          passed = kFALSE;
        }

      if( sil_event->GetNTracks() > 2 && sil_event->GetResidual() < 0.05 )
        {
          printf("\t\033[1;31m-- FAILED RESIDUAL CUT ---\033[00m\n");
          passed = kFALSE;
        }
      if(passed)
        {
          printf("\t\033[1;32m--- PASSED CUTS ---\033[00m\n");
        }


      //printf("TVertexCut result = %d \n", myCut->CheckIn2009Cuts( sil_event ) );

    }//loop over sil_tree entries

  return 0;
}

Int_t PrintPreQuenchBackgroundEventNumbers( Int_t run_number )
{
  Double_t quench_start_time = Get_RunTime_of_Count( run_number, 6, 1 );

  for( Int_t i=1; i<=Get_Total_Sis_Counts( run_number, 30 ); i++ )
        {
          if( fabs( quench_start_time - Get_RunTime_of_Count( run_number, 30, i ) ) < 0.001 )
            {
              // we've hit the quench start of dump flag
              break;
            }
          Double_t pre_quench_background_start_time = Get_RunTime_of_Count( run_number, 30, i );
        }//loop over

  // recall 10ms RO deadtime before quench
  PrintEventNumbers( run_number, pre_quench_background_start_time, (quench_start_time- 0.00999 ) );

  return 0;
}

Int_t PrintEventNumbers( Int_t run_number, Double_t start_time=0., Double_t stop_time=99999. )
{
  printf("%f -> %f [%f s] \n\n", start_time, stop_time, (stop_time - start_time) );

  TTree* sil_tree = Get_Vtx_Tree( run_number );
  if( sil_tree == NULL ) return -1;
  TSiliconEvent* sil_event = new TSiliconEvent();
  sil_tree->SetBranchAddress("SiliconEvent", &sil_event);

  for( Int_t i=0; i<sil_tree->GetEntries(); i++ )
    {
      sil_event->ClearEvent();
      sil_tree->GetEntry(i);

      if(gUSE_TSRUNTIME)
      {
          // for debugging purposes EB
          if( sil_event->GetTSRunTime() < start_time ) continue;
          if( sil_event->GetTSRunTime() > stop_time ) break;
      }
      else
      {
          if( sil_event->GetRunTime() < start_time ) continue;
          if( sil_event->GetRunTime() > stop_time ) break;
     }


      Double_t simz = 0;
      if( sil_event->GetNVertices() )
        simz = -1.*(sil_event->GetVertex()->Z()*10.-24.05);

      printf("VF48NEvent = %d \t TimeAfterDumpStart = %3.3f ms \t NHits = %d \t NVertices = %d\n" ,
             sil_event->GetVF48NEvent(),
             (sil_event->GetRunTime()-start_time)*1000.,
             sil_event->GetNHits(),
             sil_event->GetNVertices());

      if( sil_event->GetNVertices() == 0 )
        {
          printf("   No Vertex\n");
          continue;
        }

      printf("   Vertex.Z() = %3.3fcm \n   Simulation Z = %3.3lfmm\n",
             sil_event->GetVertex()->Z(),
             simz );

      TVector3 * v = sil_event->GetVertex();
      Double_t R = v->XYvector().Mod();

      printf("   R = %lfcm\n",R);

      if( R<4 )
        {
          printf("   NTracks = %d\n", sil_event->GetNTracks() );
          if( sil_event->GetNTracks() == 2 )
            {
              printf("   Residual = %lf\n", sil_event->GetResidual() );
              if( sil_event->GetResidual() > 2 )
                {
                  printf("   --- PASSED CUTS ---\n");
                }
              else
                printf("   --- FAILED RESIDUAL CUT ---\n");
            }
          else if( sil_event->GetNTracks() > 2 )
            {
              printf("   Residual = %lf\n", sil_event->GetResidual() );
              if( sil_event->GetResidual() > 0.05 )
                {
                  printf("   --- PASSED CUTS ---\n");
                }
              else
                printf("   --- FAILED RESIDUAL CUT ---\n");
            }
        }
      else
        {
          printf("   --- FAILED RADIUS CUT ---\n");
        }
    }

  return 0;
}
