#include "PlotMixing.cpp"
Bool_t debug(kFALSE);

void FindDumpsAndNames(Int_t runNumber)
{
    gDisablePrintfing=1;

    static const int FindDumpsNumChannels=3;
    Int_t Channels[FindDumpsNumChannels] = { SIS_PMT_TRAP_OR, SIS_PMT_5_AND_6, SIS_PMT_7_AND_8 };


    Int_t lastEvent(-1), bestMatch(-1);
    Double_t DumpStartTime, DumpStopTime;
    TString dumpName;

    Int_t numDumps = Count_SIS_Triggers(runNumber, SIS_DUMP_START);
    for(Int_t i=0; i<numDumps; i++)
    {
        dumpName = MatchDumpNumberToDumpName(runNumber, i+1,  &lastEvent);
        //Info("", "Got the matched event %d", lastEvent);
        GetDumpTimes(runNumber, 1, i+1, &DumpStartTime, &DumpStopTime);


        printf("Dump %2d %-25s    [%8.3lf - %8.3lf] = %8.3lfs\t", i+1, dumpName.Data(), DumpStartTime, DumpStopTime, DumpStopTime-DumpStartTime);

        for(Int_t j=0; j<FindDumpsNumChannels; j++)
        {
            printf("%5d\t", GetDumpHisto(runNumber, 1, i+1, Channels[j])->Integral());
        }

        printf("\n");


    }

}

TString MatchDumpNumberToDumpName(Int_t runNumber, Int_t dumpNumber, Int_t* guess=NULL)
{
    TTree* sequencerTree = GetSequencerTree(runNumber);
    TSeq_Event* seqEvent = new TSeq_Event();
    sequencerTree->SetBranchAddress("SequenceEvent", &seqEvent);

    if(debug) Info("DEBUG", "sequencer Tree has %d entries", sequencerTree->GetEntries());


    Int_t bestMatch(-1), lastEvent(-1);
    Double_t bestMatchTime(999.), DumpStartTime, DumpStopTime, timeDiff;
    TString dumpName;
    Double_t MatchTolerance(10e-3);

    GetDumpTimes(runNumber, 1, dumpNumber, &DumpStartTime, &DumpStopTime);

    if(guess==0)
    {
        Int_t guess_l = (dumpNumber-1)*2-2+11*(dumpNumber>1); // -2 to make it think that it's the previous dump, +11 for the positron crap.
        guess = &guess_l;
    }

    if(guess>0)
    {
        if(debug)Info("DEBUG", "Guessing %d", *guess+2);
        sequencerTree->GetEntry(*guess+2);

        for(Int_t seqOff = 0; seqOff<2; seqOff++)
        {
            timeDiff = fabs(Get_RunTime_of_SequencerEvent(runNumber, seqEvent, seqOff) - DumpStartTime );

            if(debug)Info("DEBUG", "Guess timeDiff %lf", timeDiff);

            if(strcmp(seqEvent->GetEventName(),"startDump")==0 && timeDiff<=MatchTolerance)
            {
                if(debug) Info("DEBUG", "Fast Match!!");
                dumpName = seqEvent->GetDescription();
                dumpName.ReplaceAll("\n", " ");
                dumpName.ReplaceAll("\"", "");
                guess[0] = guess[0]+2;
                return dumpName;
            }
        }
    }


    bestMatch=-1;
    bestMatchTime = 9999.;
    for(Int_t j=0; j<sequencerTree->GetEntries(); j++)
    {
       sequencerTree->GetEntry(j);
       if(strcmp(seqEvent->GetEventName(),"startDump")!=0) continue;

       for(Int_t seqOff = 0; seqOff<2; seqOff++)
       {
          //if(debug) Info("DEBUG", "SequencerEvent %d name: %s", j, seqEvent->GetEventName());

          timeDiff = fabs(Get_RunTime_of_SequencerEvent(runNumber, seqEvent, seqOff) - DumpStartTime );
          //if(debug) Info("DEBUG", "Dump %d,  timediff %.6lf with seqEvent %d", dumpNumber, timeDiff, j);
          if( timeDiff<bestMatchTime )
          {
              //if(debug) Info("DEBUG", "setting bestMatch to %d", j);
              bestMatch = j;
              bestMatchTime = timeDiff;
          }

          if(bestMatchTime<=MatchTolerance) break;
       }

       if(bestMatchTime<=MatchTolerance) break;

    }

    if(debug) Info("DEBUG", "Dump %d,  timediff %.6lf with seqEvent %d", dumpNumber, bestMatchTime, bestMatch);
    if(bestMatchTime<=MatchTolerance)
    {
        sequencerTree->GetEntry(bestMatch);
        dumpName = seqEvent->GetDescription();
        if(guess>0)guess[0] = bestMatch;
    }
    else
    {
        dumpName = "???";
    }

   dumpName.ReplaceAll("\n", " ");
   dumpName.ReplaceAll("\"", "");
   return dumpName;
}

void GetSequencerEventInfo(Int_t runNumber)
{
    TTree* SeqTree = GetSequencerTree(runNumber);
    TSeq_Event* se = new TSeq_Event();
    SeqTree->SetBranchAddress("SequenceEvent", &se);

    Int_t dumpCnt(0);
    TString dumpDes;

    for(Int_t i=0; i<SeqTree->GetEntries(); i++)
    {
            SeqTree->GetEntry(i);
            if(strcmp(se->GetEventName().Data(), "startDump") != 0)
                continue;

            dumpCnt++;
            printf("Dump %2d - %-25s\t", dumpCnt, se->GetDescription().Data() );
            printf("%6d (%3d)\t", se->GetonCount(), se->getonState());
            dumpDes = se->GetDescription();

        for(Int_t j=0; j<SeqTree->GetEntries(); j++)
        {
                SeqTree->GetEntry(j);
                if(strcmp(se->GetEventName().Data(), "stopDump") != 0)
                    continue;

                if(strcmp(se->GetDescription().Data(), dumpDes.Data())==0)
                {
                    printf("%6d (%3d)\t", se->GetonCount(), se->getonState());
                    break;
                }
        }
        printf("\n");
    }



}
