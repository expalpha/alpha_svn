// Find Labview Events from the tree ======================================
// Eoin Butler Sep 25, 2008

#include <fstream>
#include <stdio.h>
#include "AlphaAnalysisUtilities.cpp"

//=====================================================================
// Macros
//=====================================================================

Double_t GetTransformerTotal(Int_t runNumber, Int_t nDumps, Int_t firstHotDump=1)
{

    Int_t spillNumber = 0;
    Int_t dumpNumber;
    Int_t dumpsFound = 0;
    Int_t offset;
    Int_t firstSpill;


    Double_t AD_time=0.;
    Double_t nextAD_time = 0.;
    Double_t start_time;
    Double_t dump_time;
    Double_t dump_end_time;
    Double_t Transformer;
    Double_t TransformerSum = 0.;

    Int_t dumpsFound=0;

    //Need to identify the spill which contains the first dump - this is assumed to be the first hot dump
    start_time = Find_SIS_Channel_Trigger_Time(runNumber, SIS_DUMP_START, firstHotDump);

    while(AD_time<start_time && AD_time!=999.)
    {
        spillNumber++;
        AD_time = Find_SIS_Channel_Trigger_Time(runNumber, SIS_AD, spillNumber);
    }

    if(AD_time==999.)
    {
        Error("GetTransformerTotal","Could not find enough AD Spills in this run");
        return NULL;
    }

    firstSpill = spillNumber - 1;
    spillNumber = firstSpill-1; //gets ++'ed in a minute
    dumpNumber = 0;
    dump_time = 0.;
    AD_time = Find_SIS_Channel_Trigger_Time(runNumber, SIS_AD, spillNumber);
    nextAD_time = Find_SIS_Channel_Trigger_Time(runNumber, SIS_AD, spillNumber+1); //put this here to try save some time
    while(dumpsFound<nDumps && AD_time !=999.)
    {
        spillNumber++;
        AD_time = nextAD_time;
        nextAD_time = Find_SIS_Channel_Trigger_Time(runNumber, SIS_AD, spillNumber+1);
        while(dump_time<nextAD_time )
        {
            while(dump_time<AD_time && dump_time!=999.)
            {
                dumpNumber++;
                dump_time = Find_SIS_Channel_Trigger_Time(runNumber, SIS_DUMP_START, dumpNumber);
            }
                if(dump_time==999.)
                {
                    Error("GetTransformerTotal","\nCould not find enough dumps\n");
                    return NULL;
                }
            offset = -1;
            dump_end_time = 0.;
            while(dump_end_time<dump_time && dump_end_time!=999.) // This allows for the possibility that the triggers may have gotten out of sync
            {
                offset++;
                dump_end_time = Find_SIS_Channel_Trigger_Time(runNumber, SIS_DUMP_STOP, dumpNumber+offset);
            }
                if(dump_time==999.)
                {
                    Error("GetTransformerTotal","Could not find enough dumps");
                    return NULL;
                }


            dumpNumber++;
            dump_time = Find_SIS_Channel_Trigger_Time(runNumber, SIS_DUMP_START, dumpNumber);
        }

        dumpsFound++;
        TransformerSum += LabVIEW_at_SIS(runNumber, SIS_AD, spillNumber, "ADE0", 6, 1 );


    }
    if(AD_time==999.)
    {
        Error("GetTransformerTotal","\nCould not find enough dumps\n");
        return NULL;
    }

    printf("Transformer total for run %d : %lf\n",runNumber, TransformerSum);
    return TransformerSum;
}


Double_t Avg_Transformer(Int_t run_number, Int_t number_spills, Int_t First_Spill=1)
{

    Double_t running_total=0.;
    Double_t this_time;
    Int_t num_caught=0;
    Double_t second_spill;

    Double_t first_dump = Find_SIS_Channel_Trigger_Time(run_number, 30, First_Spill);

    Int_t i =0;

    while (second_spill<first_dump)
        {
        i++;
         second_spill = Find_SIS_Channel_Trigger_Time(run_number, 1, i); //This is the AD Spill time
        }

    for (int num_spill=First_Spill ; num_spill<First_Spill+number_spills ; num_spill++ )
    {

        this_time=LabVIEW_at_SIS(run_number,1,num_spill,"ADE0",6,1);
        num_caught++ ;
        if ( this_time < 0.5 )
        {
           printf("Missed shot %d Transfo: %3lf\n",num_spill, this_time);
           continue ;
        }
        printf("Caught shot %d Transfo: %3lf\n",num_spill, this_time);
        running_total+=this_time;
    }


    printf ("Average transformer number for run %d for %d spills is : %3lf \n" , run_number , num_caught , running_total/num_caught);
    return running_total/num_caught;
}

Double_t Sum_Transformer(Int_t run_number, Int_t number_spills, Int_t First_Spill=1)
{

    Double_t running_total=0.;
    Double_t this_time;
    Int_t num_caught=0;
    Double_t second_spill;

    Double_t first_dump = Find_SIS_Channel_Trigger_Time(run_number, 30, First_Spill);

    Int_t i =0;

    while (second_spill<first_dump)
        {
        i++;
         second_spill = Find_SIS_Channel_Trigger_Time(run_number, 1, i); //This is the AD Spill time
        }

    for (int num_spill=First_Spill ; num_spill<First_Spill+number_spills ; num_spill++ )
    {

        this_time=LabVIEW_at_SIS(run_number,1,num_spill,"ADE0",6,1);
        num_caught++ ;
        if ( this_time < 0.5 )
        {
           printf("Missed shot %d Transfo: %3lf\n",num_spill, this_time);
           //continue ;
        }
        printf("Caught shot %d Transfo: %3lf\n",num_spill, this_time);
        running_total+=this_time;
    }


    printf ("Total transformer number for run %d for %d spills is : %3lf \n" , run_number , num_caught , running_total/num_caught);
    return running_total;
}

//=====================================================================

// global variables

Double_t LabVIEW_at_SIS(Int_t run_number, Int_t SIS_channel, Int_t repetition, TString Bank, Int_t Array_number, Int_t Interp_Option )
{
    /*
    Int_t run_number = 8942;
    Int_t SIS_channel = 28;
    Int_t repetition = 1;

    TString Bank("LRUS");
    Int_t Array_number = 2;

    Int_t Interp_Option = 0; // -1 = before, 1 = after, 0 = lin_interp
    */



    if(repetition<1) //allow for 0 or negative numbers to count backwards.
    {
        Int_t times = Count_SIS_Triggers(run_number, SIS_channel);
        repetition += times;
    }

    Double_t trigger_time = Find_SIS_Channel_Trigger_Time( run_number, SIS_channel, repetition );

    if(trigger_time==999. || trigger_time<=0.)
    {
        printf("!!! Could not Find SIS Trigger\n");
        return -1.;
    }

    return GetLabviewEvent(run_number, trigger_time, Bank, Array_number, Interp_Option);


}

Double_t GetLabviewEvent(Int_t run_number, Double_t runTime, TString Bank, Int_t Array_number, Int_t Interp_Option)
{
        Double_t before_val, after_val, before_time, after_time;
    Int_t found_before=0;
    Int_t found_after=0;

    TTree* labVIEW_tree = Get_LabVIEW_Tree(run_number);
    TLabVIEWEvent* labVIEW_event = new TLabVIEWEvent();

    labVIEW_tree->SetBranchAddress("LabVIEWEvent", &labVIEW_event);

    //printf("..found %d LabVIEW entries\n", labVIEW_tree->GetEntries());

    for(Int_t i=0; i<labVIEW_tree->GetEntries(); i++)
    {
        labVIEW_tree->GetEntry(i);

        if(labVIEW_event->GetArrayNumber()!=Array_number) continue;
        if(strncmp(labVIEW_event->GetBankName(),Bank.Data(),4)!=0) continue;

        if(labVIEW_event->GetTime() > runTime)
        {
            after_val = labVIEW_event->GetValue();
            after_time = labVIEW_event->GetTime();
            found_after = 1;
            break;
        }
        else
        {
            before_val = labVIEW_event->GetValue();
            before_time = labVIEW_event->GetTime();
            found_before = 1;
        }

    }

    labVIEW_tree->Delete();

        if(found_before != 1 && (Interp_Option==0 || Interp_Option==-1))
        {
            printf("!!! Could not find a value BEFORE this trigger");
            return -1.;
        }
        if(found_after != 1 && (Interp_Option==0 || Interp_Option==1))
        {
            printf("!!! Could not find a value AFTER this trigger");
            return -1.;
        }

        switch(Interp_Option)
        {
            case -1: //before
                return before_val;
                break;
            case 1: //after
                return after_val;
                break;
            case 0: //linear interpolation
                return ((runTime-before_time)*before_val+(after_time-runTime)*after_val)/(after_time-before_time);
                break;
            case 2: //nearest
                if(found_before==1 && found_after==1)
                {
                    if(runTime-before_time<after_time-runTime)
                        return before_val;
                    else
                        return after_val;
                    break;
                }
                else if(found_before==1)
                {
                    return before_val;
                    break;
                }
                else if(found_after==1)
                {
                    return after_val;
                    break;
                }
                else
                {
                    printf("Could not find any values");
                    return -1;
                    break;
                }
            default:
                printf("!!! Invalid Option Value %d\n",Interp_Option);
                return -1.;
                break;
        }
}

Double_t GetLabviewEvent(TTree* labVIEW_tree, Double_t runTime, TString Bank, Int_t Array_number, Int_t Interp_Option)
{
        Double_t before_val, after_val, before_time, after_time;
    Int_t found_before=0;
    Int_t found_after=0;

    //TTree* labVIEW_tree = Get_LabVIEW_Tree(run_number);
    TLabVIEWEvent* labVIEW_event = new TLabVIEWEvent();

    labVIEW_tree->SetBranchAddress("LabVIEWEvent", &labVIEW_event);

    //printf("..found %d LabVIEW entries\n", labVIEW_tree->GetEntries());

    for(Int_t i=0; i<labVIEW_tree->GetEntries(); i++)
    {
        labVIEW_tree->GetEntry(i);

        if(labVIEW_event->GetArrayNumber()!=Array_number) continue;
        if(strncmp(labVIEW_event->GetBankName(),Bank.Data(),4)!=0) continue;

        if(labVIEW_event->GetTime() > runTime)
        {
            after_val = labVIEW_event->GetValue();
            after_time = labVIEW_event->GetTime();
            found_after = 1;
            break;
        }
        else
        {
            before_val = labVIEW_event->GetValue();
            before_time = labVIEW_event->GetTime();
            found_before = 1;
        }

    }


        if(found_before != 1 && (Interp_Option==0 || Interp_Option==-1))
        {
            printf("!!! Could not find a value BEFORE this trigger");
            return -1.;
        }
        if(found_after != 1 && (Interp_Option==0 || Interp_Option==1))
        {
            printf("!!! Could not find a value AFTER this trigger");
            return -1.;
        }

        switch(Interp_Option)
        {
            case -1: //before
                return before_val;
                break;
            case 1: //after
                return after_val;
                break;
            case 0: //linear interpolation
                return ((runTime-before_time)*before_val+(after_time-runTime)*after_val)/(after_time-before_time);
                break;
            case 2: //nearest
                if(found_before==1 && found_after==1)
                {
                    if(runTime-before_time<after_time-runTime)
                        return before_val;
                    else
                        return after_val;
                    break;
                }
                else if(found_before==1)
                {
                    return before_val;
                    break;
                }
                else if(found_after==1)
                {
                    return after_val;
                    break;
                }
                else
                {
                    printf("Could not find any values");
                    return -1;
                    break;
                }
            default:
                printf("!!! Invalid Option Value %d\n",Interp_Option);
                return -1.;
                break;
        }
}

Int_t Extract_Bank_Array(Int_t run_number, TString bank, Int_t array_num, Double_t *time, Double_t *LVData)
{
    TTree* labVIEW_tree = new TTree();
    labVIEW_tree = Get_LabVIEW_Tree(run_number);
    TLabVIEWEvent* labVIEW_event = new TLabVIEWEvent();

    labVIEW_tree->SetBranchAddress("LabVIEWEvent", &labVIEW_event);

    Int_t k = -1;

    for (Int_t i=0; i<labVIEW_tree->GetEntries(); i++)
    {
        labVIEW_tree->GetEntry(i);

        if(labVIEW_event->GetBankName() != bank || labVIEW_event->GetArrayNumber() != array_num) continue;

        k++;
        time[k] = labVIEW_event->GetTime();
        LVData[k] = labVIEW_event->GetValue();
    }

    if(k>-1)
    {
        printf("Found %d events ",k+1);
    }
    else
    {
        printf("No events found ");
    }

    printf("in %s[%d]\n",bank.Data(),array_num);

    delete labVIEW_tree;
    delete labVIEW_event;

    return k+1;
}


Double_t FindExtremeinTimeRange(Int_t runNumber, TString bank, Int_t arrayIndex, Double_t timeMin, Double_t timeMax, Int_t max)
{
    Double_t time[10000], data[10000];
    Int_t nVals = Extract_Bank_Array(runNumber, bank, arrayIndex, time, data);

    Double_t extremeVal(9e99);
    if(max==1) extremeVal=0;

    for(Int_t i=0; i<nVals; i++)
    {
        if(time[i]<timeMin || time[i]>timeMax) continue;

        if(max==1 && data[i]>extremeVal) extremeVal=data[i];
        if(max!=1 && data[i]<extremeVal) extremeVal=data[i];
    }

    return extremeVal;
}

Double_t FindExtremeinTimeRangeNonZero(Int_t runNumber, TString bank, Int_t arrayIndex, Double_t timeMin, Double_t timeMax, Int_t max)
{
  Double_t time[10000], data[10000];
  Int_t nVals = Extract_Bank_Array(runNumber, bank, arrayIndex, time, data);

  Double_t extremeVal(9e99);
  if(max==1) extremeVal=0;

  for(Int_t i=0; i<nVals; i++)
    {
      if(time[i]<timeMin || time[i]>timeMax) continue;

      if(max==1 && data[i]>extremeVal) extremeVal=data[i];
      if(max!=1 && data[i]<extremeVal && data[i]>0) extremeVal=data[i];
    }

  return extremeVal;
}



Double_t FindChange(Int_t runNumber, TString bank, Int_t arrayIndex, Double_t tMin=0., Double_t minChange=0., Int_t rep=1)
{
    Double_t time[10000], LVData[10000];
    Int_t nVals = Extract_Bank_Array(runNumber, bank, arrayIndex, time, LVData);
    Int_t count(0), i(0);
    Double_t lastVal, diff;
    minChange=fabs(minChange);

    for(i=0; i<nVals; i++)
    {
        if(time[i]<tMin || i==0)
        {
            lastVal = LVData[i];
            continue;
        }
        diff = fabs(lastVal-LVData[i]);
        else
        {
            if(diff<=minChange) continue;
        }
        else
        {
            lastVal=LVData[i];
            count++;
            if(count==rep) break;
        }
    }

    if(count==rep)
    {
        return lastVal;
    }
    else
    {
        Warning("FindChange", "Run %d, bank %s[%d] - Could not find %d changes after time %d", runNumber, bank.Data(), arrayIndex, rep, tMin);
        return -9e99;
    }
}

TGraph* PlotBank(Int_t runNumber, TString bank, Int_t array_num, char* opts = NULL, Int_t Colour = 1 )
{
    static const int n = 10000;
    Double_t time[n], data[n];

    Int_t nvals = Extract_Bank_Array(runNumber, bank, array_num, time, data);

    TGraph* g = new TGraph(nvals, time, data);

    char title[500];
    sprintf(title, "Run %d - Labview %s[%d]", runNumber, bank.Data(), array_num);

    TString* opt = new TString(opts);

    if(opts == NULL)
    {
        g->Draw("AP");
    }
    else
    {
        char optstring[100];
        if (opt->Contains("SAME"))
        {
            sprintf(optstring, "%sSAME", optstring);
        }
        else
        {
            sprintf(optstring, "%sA", optstring);
        }

        if (opt->Contains("Y+")) sprintf(optstring, "%sY+", optstring);
        if (opt->Contains("X+")) sprintf(optstring, "%sX+", optstring);


        if(opt->Contains("STEP"))
        {

            if(opt->Contains("STEP0"))
            {
                memmove(time+1, time, nvals*sizeof(Double_t));
                memmove(data+1, data, nvals*sizeof(Double_t));
                time[0] = 0.;
                data[0] = 0.;
                nvals++;
                g->Delete();
                g = new TGraph(nvals, time, data);
            }

            if(!opt->Contains("SAME"))
            {
                sprintf( optstring, "%sP", optstring);
                g->Draw(optstring);
            }

            if(opt->Contains("Y+") || opt->Contains("X+"))
            {
                 sprintf( optstring, "%sAP", optstring);
                 g->Draw(optstring);
            }

            TLine* line = new TLine();
            line->SetLineColor(Colour);
            TMarker* marker = new TMarker();
            marker->SetMarkerColor(Colour);
            marker->SetMarkerStyle(kFullCircle);

            for(Int_t i=0; i<nvals-1; i++)
            {
                //marker->DrawMarker(time[i], data[i]);
                line->DrawLine(time[i], data[i], time[i+1], data[i]);
                marker->DrawMarker(time[i+1], data[i]);
                line->DrawLine(time[i+1], data[i], time[i+1], data[i+1]);
            }

            marker->DrawMarker(time[nvals-1], data[nvals-1]);


        }
        else
        {
            if(opt->Contains("L"))  sprintf(optstring, "%sL", optstring);
            if(opt->Contains("P"))  sprintf(optstring, "%sP", optstring);
            if(strlen(optstring)==0) sprintf(optstring, "%s", opts);

            g->Draw(optstring);
        }


    }

    g->GetYaxis()->SetTitle("Variable value");
    g->GetXaxis()->SetTitle("Time [s]");
    g->SetLineColor(Colour);
    g->SetMarkerStyle(20);
    g->SetMarkerColor(Colour);
    char title[100];
    sprintf(title, "Run %d, Labview %s[%d]", runNumber, bank.Data(), array_num);
    g->SetTitle(title);

    return g;
}
