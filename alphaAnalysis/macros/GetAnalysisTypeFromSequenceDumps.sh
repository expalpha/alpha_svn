#!/bin/bash
DEBUG=0
# GetAnalysisTypeFromSequenceDumps:
# I am a bash tool to identify the type of analysis a run will require. 
# I return a run 'type' as a short string, this type can then be used for
# automatic analysis. I depend on the PrintSequenceQOD() function in 
# PlotDumps. 
#
## FUNCTION USAGE:
# source GetAnalysisTypeFromSequenceDumps.sh 43000
# or
# source GetAnalysisTypeFromSequenceDumps.sh R43000_SequenceQOD.log
#
# returned is a 'Code phrase' use to identify run type


##NOTES: Cases to evaluate
#CODE			Description
#BasicCosmic	Cosmic (no pbars or positrons)
#DedCosmic		Dedicated Cosmic (no sequence run at all)
#MIXING			Mixing only (no quench or FRD)
#			Trapping:
#QUENCH			Quench run
#FRD			FastRampDown
#			Lifetime:
#Lifetime		Catching Trap || RCT & Atom Trap (Silicon)

function MakeQOD {
  RUNNO=$1
  echo ".L PlotDumps.C+
  gProgressBars=kFALSE;
  PrintSequenceQOD(${RUNNO})
  .q
  " | root -l -b
}
##################################################################
# Check input arguement, if (integer) run QOD else continue
##################################################################

SeqQODFile=$1
case ${SeqQODFile#[-+]} in
  *[!0-9]* ) 
    if [ $DEBUG -eq 1 ]; then echo "Not a number"; fi
  ;;
  * ) 
    if [ $DEBUG -eq 1 ]; then echo "Valid number"; fi
    if [ $DEBUG -eq 0 ]; 
    then MakeQOD $SeqQODFile &> /dev/null ;
    else MakeQOD $SeqQODFile ;  
    fi
    SeqQODFile="AutoPlots/$(date +%Y%m%d)/R${RUNNO}_SequenceQOD.log"
  ;;
esac
if [ $DEBUG -eq 1 ]; then echo "$SeqQODFile"; fi
if [ -s "$SeqQODFile" ]; then
  if [ $DEBUG -eq 1 ]; then echo "QODFile has some contents"; fi
else
  echo "QODFileEmpty"
  return 0
fi

#AM I NEEDED!?
#cp AutoPlots/$(date +%Y%m%d)/R${RUNNO}_SequenceQOD.log ~/AnalysisOutput/${RUNNO}/R${RUNNO}_SequenceQOD.log
#SequenceQOD=`grep Error ~/AnalysisOutput/${RUNNO}/R${RUNNO}_SequenceQOD.log`
#ELOG_SUBJECT="$ELOG_SUBJECT $SequenceQOD"
#echo "[code]" > ~/AnalysisOutput/R${RUNNO}.Elog.txt
#cat AutoPlots/$(date +%Y%m%d)/R${RUNNO}_SequenceQOD.log >> ~/AnalysisOutput/R${RUNNO}.Elog.txt
#echo "[/code]" >> ~/AnalysisOutput/R${RUNNO}.Elog.txt


#Test for dumps... insert more if you want them here:
MIXING=`grep "Mixing" $SeqQODFile | grep -v "Warning" | grep -v "Error" `
QUENCH=`grep "Quench" $SeqQODFile | grep -v "Warning" | grep -v "Error" `
FRD=`grep "FRD"  $SeqQODFile | grep -v "Warning" | grep -v "Error" `
FASTRAMPDOWN=`grep "FastRampDown" $SeqQODFile | grep -v "Warning" | grep -v "Error" `
PBAR=`grep "Pbar" $SeqQODFile`
LIFETIME=`grep "Lifetime" $SeqQODFile`
ANYDUMP=`grep "\." $SeqQODFile`
ALWAYSEMPTY=`grep "A string like me shouldnt exist..." $SeqQODFile`
POSITRONS=`grep Positrons $SeqQODFile`

if [ $DEBUG -eq 1 ]; then 
  echo "MIXING:$MIXING"
  echo "QUENCH:$QUENCH"
  echo "FRD:$FRD"
  echo "FASTRAMPDOWN:$FASTRAMPDOWN"
  echo "ANYDUMP:$ANYDUMP"
  echo "ALWAYSEMPTY:$ALWAYSEMPTY"
  echo "PBAR:$PBAR"
  echo "POSITRONS:$POSITRONS"
fi



#IF QUENCH OR FRD and NOT MIXING OR PBAR... treat as cosmic...

#If there is a fast ramp down dump...
if [ -n "$FASTRAMPDOWN" ] && [ -n "$MIXING" ]; then
  echo "FRD"
  if [ $DEBUG -eq 1 ]; then echo "FastRampDown dump found..."; fi
  return 0
fi

#If there is a fast ramp down dump...
if [ -n "$QUENCH" ] && [ -n "$MIXING" ]; then
  echo "QUENCH"
  if [ $DEBUG -eq 1 ]; then echo "Quench dump found"; fi
  return 0
fi

#If there is evidence we have a trapping like run
if [ -n "$MIXING" ]; then #|| [ -n "$QUENCH" ] || [ -n "$FASTRAMPDOWN" ]; then
  echo "MIXING"
  if [ $DEBUG -eq 1 ]; then echo "Mixing dump found... "; fi
  return 0
fi

if [ -n "$LIFETIME" ]; then #|| [ -n "$QUENCH" ] || [ -n "$FASTRAMPDOWN" ]; then
  echo "LIFETIME"
  if [ $DEBUG -eq 1 ]; then echo "Lifetime dump found... "; fi
  return 0
fi


if  [ ${#PBAR} -lt 3 ] && [ ${#POSITRONS} -lt 3 ] && [ ${#MIXING} -lt 3 ]; then
  #echo "No dumps mentioning Pbar, Positron or Mixing in run... Assuming its a cosmic run... "
  if [ ${#ANYDUMP} -lt 3 ]; then
##Move these lines back to autoanalysis...
#		echo "No dumps at all... we can be patient... waiting for alphaStrips to finish"
#		wait $STRIPS_PID
#		export STRIPFILES=${RELEASE}/alphaStrips/data/${RUNNO}/
    echo "DedCosmic"
    if [ $DEBUG -eq 1 ]; then echo "No dumps found in sequence... i am a dedicated cosmic run"; fi
    return 0
  else
    echo "BasicCosmic"
    if [ $DEBUG -eq 1 ]; then echo "BASIC COSMIC RUN"; fi
    return 0
  fi
  else
    echo "Not mix/trapping... has polutants"
    return 0
fi

