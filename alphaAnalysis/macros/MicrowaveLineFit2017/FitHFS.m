close all;
clear variables;

format LONG;

cb = [];
%fileID = fopen('Nov8_All_corrected_directTimes.txt');
%fileID = fopen('Nov8_AltCrit.txt');
%fileID = fopen('Nov7_AltCrit.txt');
%fileID = fopen('Nov29.txt');
%fileID = fopen('Nov30.txt');
RFCUT='0.15';
ONSET='2_1_0_-1.csv';

LISTA={'20171107_20kHz_128s.list' '20171108_194s.list' '20171130_128s.list' };
LISTB={'20171107_20kHz_194s.list' '20171108_262s.list' '20171130_280s.list' };
%Loop over the 6 run lists in LISTS
for ListPair=[1 2 3]
    clearvars -global -except ListPair LISTB LISTA ONSET RFCUT cb 
    All=zeros(0,6);
    %Loop over short and long wait (LISTA and LISTB)
    for j=1:2
        for STATE={'state0' 'state1'}
            %Get List Name
            if j==1
                RunList=LISTA{ListPair};
            else
                RunList=LISTB{ListPair};
            end
            %Build filename of CSV
            filename=['MATLAB/' RunList '_BDT_' RFCUT '_' STATE{1} '_' ONSET]; %disp(filename);
            %Open file
            fileID = fopen(filename);
            %Parse 6 columns in CSV:
                %Run Number, 
                %Wait time, 
                %State (0 or 1 for C or D), 
                %UNIX time, 
                %Time since first Microwave dump, 
                %Onset Frequency?
            MaxEntries=Inf;
            if (ListPair==2)
                %Only use the first 4 runs 
                MaxEntries=4;
            end
            data=fscanf(fileID,'%d %f %d %f %f %f',[6 MaxEntries]);
            All=[ All; data'];
            fclose(fileID);
        end
    end
    %Check all hold times look normal:
    %All(:,2);
    
    %Overwrite potentially buggy column 5 with UNIX timestamps (with first
    %entry as 0 time)
    All(:,5)=All(:,4)-All(1,4);
    %Change time from seconds to hours
    All(:,4)=All(:,4)/3600;
    All(:,5)=All(:,5)/3600;

    %Auto identify the runs with shorter or longer delays
    clear c_noDelInd c_DelInd d_noDelInd d_DelInd
    %Shorter hold C state:
    c_noDelInd = find( abs(All(:,2) - All(1,2)) < 1 & All(:,3)==0);
    %Longer hold (Delayed) D state:
    c_DelInd   = find( abs(All(:,2) - All(1,2)) > 1 & All(:,3)==0);
    %Shorter hold C state:
    d_noDelInd = find( abs(All(:,2) - All(1,2)) < 1 & All(:,3)==1);
    %Longer hold (Delayed) D state:
    d_DelInd   = find( abs(All(:,2) - All(1,2)) > 1 & All(:,3)==1);

    
    psuedo = 0; %something is biasing the psuedodata to smaller splittings.
    % If anything I think the real bias is the opposite. Need to add some noise
    % that doesn't scale with binSize.
    
    if psuedo==0
        cbx = All(c_noDelInd,5);
        cby = All(c_noDelInd,6);
        cbDelx = All(c_DelInd,5);
        cbDely = All(c_DelInd,6);
        
        dax =  All(d_noDelInd,5);
        day =  All(d_noDelInd,6);
        daDelx = All(d_DelInd,5);
        daDely = All(d_DelInd,6);
    else
        base = 28.24;
        hfs = 1.420406;
        binSize = 40E-6;
        t1 = 0.5;
        t2 = 64;
        drift1 = -96E-6;
        drift2 = 0.23E-6;
        Npoints = 5;
        cby = [];
        cbDely = [];
        day = [];
        daDely = [];
        cbx = [];
        cbDelx = [];
        dax = [];
        daDelx = [];
        
        binNoise = round(randn())*binSize/2; %meant to estimate the effect of
        % using using MVA>1 to catch the onset which can statistically fluctuate
        basicNoise = 5; %magnetic or whatever actual fluctuations, regardless
        % of binsize
        
        for i=1:Npoints
            cbx(i) = 2*t1*(i-1)+randn()/3600;
            cbDelx(i) = t1 + 2*t1*(i-1) + t2/3600 + randn()/3600 ;
            dax(i) = cbx(i)+ t2/3600;
            daDelx(i) = cbDelx(i) + t2/3600;
            
            cby(i) = round((base + drift1*cbx(i) + binNoise + basicNoise )/binSize)*binSize;
            cbDely(i) = round((base + drift1*cbDelx(i) + drift2*t2 +  binNoise + basicNoise )/binSize)*binSize;
            day(i) = round((base+hfs + drift1*dax(i) +  binNoise)/binSize + basicNoise)*binSize;
            daDely(i) = round((base + drift1*daDelx(i)+hfs + drift2*t2 +  binNoise + basicNoise )/binSize)*binSize;
            
        end
        
    end
    
    
    
    xdata = horzcat(cbx,cbDelx,dax,daDelx);
    ydata = horzcat(cby,cbDely,day,daDely);
    
    %Set variables for legend position
    MeanFrequency=mean(All(:,6));
    MinCBFrequency=min(min([cby,cbDely]))-5E-5;
    MinDAFrequency=min(min([day,daDely]))-5E-5;
    %fun = @(a,xdata) horzcat(a(1)*cbx+a(2),a(1)*cbDelx+a(3),a(1)*dax+a(4),a(1)*daDelx+a(5))
    
    %a(1) -- Carlsberg decay
    %a(2) -- c no delay intercept
    %a(3) -- c-drift
    %a(4) -- Drift corrected HFS
    %a(5) -- d-drift
    
    %Fit time:
    fun = @(a,xdata) horzcat(a(1)*cbx+a(2),a(1)*cbDelx+a(3)+a(2),a(1)*dax+ ...
        a(4)+a(3)+a(2),a(1)*daDelx+a(5)+a(4)+a(3)+a(2));
    x0 = [90E-6;28;20E-6;1.42;20E-6];
    %x = lsqcurvefit(fun,x0,xdata,ydata)
    [x,resnorm,resid,exitflag,output,lambda,J] = lsqcurvefit(fun,x0,xdata,ydata);
    
    
    ci = nlparci(x,resid,'jacobian',J);
    
    %Declare the variables that summarise the fit with meaningful names in
    %MHZ
    CarlsbergDrift = x(1)*1E3;
    CarlsDriftError = (ci(1,2) - x(1))*1E3;
    cbDrift = x(3)*1E3;
    cbDrifterror = (ci(3,2)-x(3))*1E3;
    daDrift = x(5)*1E3;
    daDrifterror = (ci(5,2)-x(5))*1E3;
    HFS = x(4)*1E3;
    HFSError = (ci(4,2) - x(4))*1E3;
    
    figure() 
    subplot(2,1,1)
    scatter(cbx,cby,'filled','MarkerFaceColor','black');
    title('CB Tranisiton'); 
    hold on;
    scatter(cbDelx,cbDely,'filled','MarkerFaceColor','blue');
    
    xpts = [0:0.1:6];
    cbfit = x(1)*xpts + x(2);
    %cbfitm = x(1)*xpts+ci(2,1);
    %cbfitp = x(1)*xpts+ci(2,2);
    cbDelfit = x(1)*xpts + x(2)+x(3);
    
    
    plot(xpts,cbfit,'Color','black');
    %plot(xpts,cbfitm)
    %plot(xpts,cbfitp)
    plot(xpts,cbDelfit,'Color','blue');
    
    xlabel('Time (hours)')
    ylabel('Frequency (GHz)')
    FONTSIZE=12;
    ax = gca;
    ax.FontSize = FONTSIZE;
    legend('Seq A c onset','Seq B c onset')

    
    text(0.2,MinCBFrequency+.0001,['Carlsberg drift = ' ...
        num2str( CarlsbergDrift*1E3) ' +/- ' num2str( CarlsDriftError*1E3) 'kHz/hr'])
    text(0.2,MinCBFrequency,['Sweep drift = ' num2str(cbDrift*1E3) ' +/- ' ...
        num2str(cbDrifterror*1E3) ' kHz (over ' ...
        num2str(max(All(:,2))-min(All(:,2))) ' s)'])
    text(0.2,MinCBFrequency-0.0001,['HFS: ' num2str(HFS) ' +/- ' ...
        num2str(HFSError) ' MHz'],'FontSize',11)
    
    drawnow
    subplot(2,1,2)
    %figure()
    
    scatter(dax,day,'filled','MarkerFaceColor','red');
    title('DA Tranisiton')
    hold on;
    scatter(daDelx,daDely,'filled','MarkerFaceColor',[0.466000000000000  ...
        0.674000000000000   0.188000000000000]);
    
    xpts = [0:0.1:6];
    da = x(1)*xpts + x(2)+x(3)+x(4);
    daDelfit = x(1)*xpts + x(2)+x(3)+x(4)+x(5);
    plot(xpts,da,'Color','red')
    plot(xpts,daDelfit,'Color',[0.466000000000000   0.674000000000000  ...
        0.188000000000000] )
    
    xlabel('Time (hours)')
    ylabel('Frequency (GHz)')
    FONTSIZE=12;
    ax = gca;
    ax.FontSize = FONTSIZE;
    legend('Seq A d onset','Seq B d onset')
    text(0.2,MinDAFrequency,['Sweep drift = ' num2str(daDrift*1E3) ' +/- ' ...
        num2str(daDrifterror*1E3) ' kHz (over ' ...
        num2str(max(All(:,2))-min(All(:,2))) ' s)'])
    %text(1.5,MinDAFrequency-0.0001,RunList,'FontSize',11)
    
    
    annotation('textbox', [0 0.9 1 0.1], ...
    'String', [LISTA{ListPair} ' and ' LISTB{ListPair}], ...
    'EdgeColor', 'none', ...
    'HorizontalAlignment', 'center','Interpreter', 'none')
    drawnow
    saveas(gcf,['HFSSplitting_' RFCUT '_' ONSET '_Day' num2str(ListPair) '.png'])
    %Results in MHz
    if psuedo==0
    disp([ 'Lists: ' LISTA{ListPair} '  ' LISTB{ListPair} ])
    end
    disp([ 'CarlsbergDrift: ' num2str(CarlsbergDrift) char(9) '+/- ' num2str(CarlsDriftError) ' Mhz' ])
    disp([ 'C-B drift:      ' num2str(cbDrift) char(9) '+/- ' num2str(cbDrifterror) ' Mhz'])
    disp([ 'D-A drift:      ' num2str(daDrift) char(9) '+/- ' num2str(daDrifterror) ' Mhz'])
    disp([ 'HFS:            ' num2str(HFS) char(9) '+/- ' num2str(HFSError) ' Mhz'])    
    stdHFSError = HFSError/1.96;
    disp([ 'Std Error:' stdHFSError])
    
    
end

