

LIST=${1}

#For a given run list, I get the frequencies from a copy of the elog:
#MicrowaveLog
#
#I summarise all runs I've even been given into one master Tab Separated
#File (FrequencyRunList.csv)
for i in `cat ${LIST}`; do
  echo "${i}:
"
  #Old useless version (does only first bin)
  #grep ${i} ElogScrape.csv |  tr '<BR>' '\n' | grep -e 15.000000 -e 12.000000 | head -n 1 | awk 'BEGIN {FS="\t"}; {print $2}'
  #echo  ${i} `grep ${i} ElogScrape.csv |  tr '<BR>' '\n' | grep -e 15.000000 -e 12.000000 | head -n 1 | awk 'BEGIN {FS="\t"}; {print $2}'` >> FrequencyRunList.data

  #MicrowaveLog folder is a copy from the Elog (raw files)

  grep -A 4 "Run: ${i}" MicrowaveLog/* | tr '<BR>' '\n' | grep -e 12.000000 -e 15.000000 -e 18.000000 | tail -n 64

  echo "${i}	-1	-1	-1"> R${i}.data
  grep -A 4 "Run: ${i}" MicrowaveLog/* | tr '<BR>' '\n' | grep -e 12.000000 -e 15.000000 -e 18.000000 | tail -n 64 >>R${i}.data

  echo "${i} done"
done

paste -d'\t' R*.data > FrequencyRunList.csv

ROWS=`cat FrequencyRunList.csv | wc -l`
COLUMNS=`ls R*.data | wc -l`
echo "const Int_t gnRuns=${COLUMNS};
Double_t FreqRunList[${ROWS}][${COLUMNS}*4]={"> FrequencyRunList.h

while read line; do
  echo "{${line}}," | tr '\t' ',' >> FrequencyRunList.h
done < FrequencyRunList.csv
echo "};" >> FrequencyRunList.h

#echo "FreqRunList={
#for line in 
#" `cat FrequencyRunList.csv | tr '\t' ',' | tr '\r' '\} \n \{'` "}" > FrequencyRunList.h
