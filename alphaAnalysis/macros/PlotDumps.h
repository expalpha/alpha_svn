//
#include "Rtypes.h"
#if MVA==2
#include "TMVA/Tools.h"
#include "TMVA/Reader.h"
#include "TMVA/MethodCuts.h"
#endif

#include "TObject.h"
#include "TH1D.h"
#include "TFile.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TError.h"
#include "TStyle.h"
#include "TTree.h"
#include "TPaveText.h"
#include "TSpline.h"
#include "TF1.h"
#include "TEventList.h"
#include "TROOT.h"
#include "THStack.h"
#include "TSystem.h"
#include "TMultiGraph.h"
#include "TLegend.h"
#include "TGraphErrors.h"
//#include "TAlphaEventStrip.h"
/*#include "TAlphaEvent.h"
*/

#include "TAlphaPlot.h"


#include "TSeq_Event.h"
#include "TSisEvent.h"
#include "TSiliconEvent.h"
#include "TSISChannels.h"
#include "TTCEvent.h"
#include "TLabVIEWEvent.h"
//#include "../lib/include/SIS_Channels.h.alpha2"
#include "TVF48SiMap.h"
#include "TAnalysisReport.h"


#include <iostream>
#include "TRootUtils.h"
#include <iomanip>
using namespace std;
using namespace TRootUtils;
#include <fstream>
#include "sqlite3.h"
//#include "Utils.h"
extern Bool_t gApplyCuts;//=kTRUE;
Int_t gNbin=100;
extern Double_t gZcutMax;//=999.;
extern Double_t gZcutMin;//=-999.;
extern Int_t gLegendDetail;//=1;
extern TString gTreePostfix;
extern TString gTreePrefix;
//Bool_t gApplyCuts;
//Int_t gNbin;
//Int_t gLegendDetail;
#if MVA == 2
//Eventually kill there globals:
extern TMVA::Reader *reader;
extern Double_t grfcut ;
extern std::vector<TString> gTMVAVarsString;
extern std::vector<TString> gTMVAVarsType;
extern std::vector<TString> gTMVASpectatorsString;
extern std::vector<TString> gTMVASpectatorsType;

extern Float_t gTMVAVarFloat[10000];
extern Int_t gTMVAVarInt[10000];
extern Float_t gTMVASpectatorsFloat[10000];
extern Int_t gTMVASpectatorsInt[10000];
extern TString gmethodName;
extern TString gdir;
extern TString prefix;
//Global floats for reader:
 Int_t nhits, nCT, nGT; 
  Float_t rnhits, rnCT, rnGT,Fnhitsasym;
  Float_t  Fresidual, Fr,FS0rawPerp, FS0axisrawZ,Fphi_S0axisraw,Ftracksdca;
  Float_t Fcurvemin,Fcurvemean,Fcurvesign,Flambdamin,Flambdamean,Fphip,Fthetap;
//Global doubles for dumper file:  
  Double_t nhitsasym;
  Double_t  residual, r,S0rawPerp, S0axisrawZ,phi_S0axisraw,tracksdca;
  Double_t curvemin,curvemean,curvesign,lambdamin,lambdamean,phip,thetap;




  
void SetTMVAVarsLoop(TXMLEngine* xml, XMLNodePointer_t node, Int_t level, Int_t VarNum, Int_t NodeType)
{
	
	  
   // this function display all accessible information about xml node and its children
   
  if (strncmp(xml->GetNodeName(node),"Variables",9)==0) NodeType=1;
  if (strncmp(xml->GetNodeName(node),"Spectators",10)==0) NodeType=2;
   
  if ( NodeType==2) 
  {
    printf("%*c node: %s\n",level,' ', xml->GetNodeName(node));

    // display namespace
    XMLNsPointer_t ns = xml->GetNS(node);
    XMLAttrPointer_t attr = xml->GetFirstAttr(node);
    while (attr!=0) {
      if (NodeType==2 && strncmp(xml->GetAttrName(attr),"Type",4)==0)
      {
        printf("%*c attr: %s value: %s\n",level+2,' ', xml->GetAttrName(attr), xml->GetAttrValue(attr));
        gTMVASpectatorsType.push_back(xml->GetAttrValue(attr));
      }        
      if (NodeType==2 && strncmp(xml->GetAttrName(attr),"Expression",10)==0)
      {
        printf("%*c attr: %s value: %s\n",level+2,' ', xml->GetAttrName(attr), xml->GetAttrValue(attr));
        gTMVASpectatorsString.push_back(xml->GetAttrValue(attr));
      }
      attr = xml->GetNextAttr(attr);
     }
  }



   if ( NodeType==1)
   {
   printf("%*c node: %s\n",level,' ', xml->GetNodeName(node));
   
   // display namespace
   XMLNsPointer_t ns = xml->GetNS(node);
   XMLAttrPointer_t attr = xml->GetFirstAttr(node);
   while (attr!=0) {
     if (NodeType==1 && strncmp(xml->GetAttrName(attr),"Type",4)==0)
       {
         printf("%*c attr: %s value: %s\n",level+2,' ', xml->GetAttrName(attr), xml->GetAttrValue(attr));
         gTMVAVarsType.push_back(xml->GetAttrValue(attr));
       }        
     if (NodeType==1 && strncmp(xml->GetAttrName(attr),"Expression",10)==0)
       {printf("%*c attr: %s value: %s\n",level+2,' ', xml->GetAttrName(attr), xml->GetAttrValue(attr));
        gTMVAVarsString.push_back(xml->GetAttrValue(attr));
	   }
       attr = xml->GetNextAttr(attr);
   }
  }
  const char* content = xml->GetNodeContent(node);
  XMLNodePointer_t child = xml->GetChild(node);
  while (child!=0) {
     SetTMVAVarsLoop(xml, child, level+2, VarNum,NodeType);
     child = xml->GetNext(child);
  }
}
void SetTMVAVars(TString xmlfile)
{
   TXMLEngine* xml = new TXMLEngine;
   // Now try to parse xml file
   // Only file with restricted xml syntax are supported
   XMLDocPointer_t xmldoc = xml->ParseFile(xmlfile);
   if (xmldoc==0) {
      delete xml;
      return;
   }
   // take access to main node
   XMLNodePointer_t mainnode = xml->DocGetRootElement(xmldoc);
   // display recursively all nodes and subnodes
   SetTMVAVarsLoop(xml, mainnode, 1,0, 0);
   // Release memory before exit
   xml->FreeDoc(xmldoc);
   delete xml;


  
  for (UInt_t i=0; i<gTMVASpectatorsString.size(); i++)
  {
	std::cout <<gTMVASpectatorsString[i]<<std::endl;
  if ( gTMVASpectatorsType[i] == 'I' )
    {
      reader->AddSpectator( gTMVASpectatorsString[i],&gTMVASpectatorsInt[i]);
    }
    else if ( gTMVASpectatorsType[i] == 'F' )
    {
      reader->AddSpectator( gTMVASpectatorsString[i],&gTMVASpectatorsFloat[i]);
    }
    else 
    {
      std::cout <<gTMVASpectatorsString[i] << "Has an unsupported data type of "\
      << gTMVASpectatorsType[i] <<std::endl;
    }
  }

  
  for (UInt_t i=0; i<gTMVAVarsString.size(); i++)
  {
	std::cout <<gTMVAVarsString[i]<<std::endl;
  if ( gTMVAVarsType[i] == 'I' )
    {
      reader->AddVariable( gTMVAVarsString[i],&gTMVAVarInt[i]);
    }
    else if ( gTMVAVarsType[i] == 'F' )
    {
      reader->AddVariable( gTMVAVarsString[i],&gTMVAVarFloat[i]);
    }
    else 
    {
      std::cout <<gTMVAVarsString[i] << "Has an unsupported data type of "\
      << gTMVAVarsType[i] <<std::endl;
    }
  }

}

void SetMVAMethod(TString Method)
{
  if (!reader) reader = new TMVA::Reader( "!Color:!Silent" );
  gmethodName = TString(Method) + TString(" method");
  TString weightfile = gdir + prefix + TString("_") + TString(Method) + TString(".weights.xml");
  SetTMVAVars(weightfile);
  reader->BookMVA( gmethodName, weightfile ); 
}
void SetMVAPath(TString dataset="dataset")
{ 
	gdir="";
	gdir+="../../alphaMVA/tmva/";
	gdir+=dataset;
	gdir+="/weights/";
	// TString dir="alphaMVA_19/weights/"  
}
void SetTMVAVars()
{
	//Create a for loop to auto populate this
/*   reader->AddVariable( "nhits",&rnhits );
   reader->AddVariable( "residual",&Fresidual);
   reader->AddVariable( "r",&Fr);
   reader->AddVariable( "S0rawPerp", &FS0rawPerp );
   reader->AddVariable( "S0axisrawZ", &FS0axisrawZ );
   reader->AddVariable( "phi_S0axisraw",&Fphi_S0axisraw);
   reader->AddVariable( "nCT",  &rnCT );
   reader->AddVariable( "nGT",  &rnGT );
   reader->AddVariable( "nhitsasym",&Fnhitsasym);  
   reader->AddVariable( "tracksdca",&Ftracksdca);
   reader->AddVariable( "curvemin",&Fcurvemin);
   reader->AddVariable( "curvemean",&Fcurvemean);
   reader->AddVariable( "lambdamin",&Flambdamin); 
   reader->AddVariable( "lambdamean",&Flambdamean);
   reader->AddVariable( "curvesign",&Fcurvesign);
   reader->AddVariable( "phip",&Fphip);
   reader->AddVariable( "thetap",&Fthetap);*/
}
void SetTMVA(TString USE="BDTF",TString path="OnlineMVA")
{
  //if (USE.CompareTo("PyKeras")==0) TMVA::PyMethodBase::PyInitialize();
  if (!reader) reader = new TMVA::Reader( "!Color:!Silent" );
  //~4mHz Background (42% efficiency)
  grfcut=0.398139;
  //45mHz Background (72% efficiency)
  //grfcut=0.230254;
  //100mHz Background (78% efficiency)
  //grfcut=0.163; 
  //SetTMVAVars();//Pass path here eventually (to auto detect vars)
  SetMVAPath(path);
  SetMVAMethod(USE);

}
#endif

void SetRFCut(Double_t RFcut);
void AntiDumpSingleWindow(Int_t runNumber, Double_t &tmin, Double_t &tmax, Bool_t Verbose=kFALSE);
void AntiSeqSingleWindow(Int_t runNumber, Double_t &tmin, Double_t &tmax);
TCanvas* PlotMVA(TString ifilename, Double_t tmin, Double_t tmax, TString TimeAxis="SiRunTime");
TCanvas* PlotMVA(TObjArray hh);
TObjArray GetMVAHisto(TString ifilename, Double_t tmin, Double_t tmax, TString TimeAxis="SiRunTime", Bool_t zeroTime=kTRUE);

void DumpBank(Int_t runNumber, TString Bank, Int_t Array_number, const char* description, Int_t repetition=1, Int_t offset=0);
void DumpBank(Int_t runNumber, TString Bank, Int_t Array_number, Double_t start, Double_t stop, const char* FileName);


TString GetLaserReportForExcel(Int_t runNumber);

//TCanvas*  PlotLyVertices(Int_t runNumber);
TCanvas* PlotLaserVerticesMultiDump_SeqPulseTiming(Int_t runNumber, const char* description, Double_t ObservationStart =0, Double_t ObservationStop = 0.01,Int_t LimitDumpRepetition = 1, Int_t PlotAxis=0, bool exact_match=true);
TAlphaPlot* GetLaserVerticesMultiDump_SeqPulseTiming(Int_t runNumber, const char* description, Double_t ObservationStart =0, Double_t ObservationStop = 0.01,Int_t LimitDumpRepetition = 1, Int_t offset = 0, Int_t PlotAxis=0, bool exact_match=true);
TCanvas* PlotLaserVerticesMultiDump_SeqPulseTiming(const char* runfile, const char* description, Double_t ObservationStart, Double_t ObservationStop, Int_t LimitDumpRepetition);
TAlphaPlot* GetLaserVerticesMultiDump_SeqPulseTiming(const char* runfile, const char* description, Double_t ObservationStart, Double_t ObservationStop, Int_t LimitDumpRepetition, Int_t PlotAxis=0, bool exact_match=true);
int PrintVerticies(Int_t runNumber, Double_t QpulseTimes[], int pulseCount, Double_t ObservationStart, Double_t ObservationStop);

//void SavePlotVertices(const char* runfile, const char* description, Bool_t SavePlot=1, Int_t repetition=1, Int_t offset=0, Double_t tmax=-1);
TCanvas* PlotQuenchFlagVertices(const char* runfile, Int_t repetition=1, Double_t tmax=-1);
TCanvas* PlotQuenchFlagVertices(Int_t runNumber, Int_t repetition=1, Double_t tmax=-1);
TCanvas* PlotMixingFlagVertices(const char* runfile, Int_t repetition=1, Double_t tmax=-1, Double_t startOffset=0.);
TCanvas* PlotMixingFlagVertices(Int_t runNumber, Int_t repetition=1, Double_t tmax=-1, Double_t startOffset=0.);
//TCanvas* PlotSISFlagVertices(const char* runfile, Int_t SIS_Channels[], Int_t repetition=1, Double_t tmax=-1);
TCanvas* PlotSISFlagVertices(Int_t runNumber, Int_t SIS_Channel, Int_t repetition=1, Double_t tmax=-1, Double_t toffset=0.);





TCanvas* PlotVertices(Int_t runNumber, Double_t tmin=0., Double_t tmax=-1.);
TCanvas* PlotVertices(TObjArray hh);
TCanvas* PlotVertices(Int_t runNumber, const char* description, Int_t repetition=1, Int_t offset=0, Double_t tmax=-1);
TCanvas* PlotVertices(const char* runfile, const char* description, Int_t repetition=1, Int_t offset=0, Double_t tmax=-1.);
TCanvas* PlotVertices(const char* runfile, Int_t dumpNumber, Int_t seq, Int_t offset=0, Double_t tmax=-1);
TCanvas* PlotVertices(const char* runfile, const char* description1, Int_t repetition1, Int_t offset1, const char* description2, Int_t repetition2, Int_t offset2, Double_t tmax=-1);

TAlphaPlot* LoadTAlphaPlot(TString file);
TAlphaPlot* GetPlotVertices(Int_t runNumber, Double_t tmin=0., Double_t tmax=-1.);
TAlphaPlot* GetPlotVertices(Int_t runNumber, const char* description, Int_t repetition=1, Int_t offset=0, Double_t tmax=-1);

TH1D* PlotSIScounts(Int_t runNumber, Int_t channel, Double_t tmin, Double_t tmax, Bool_t zeroTime=kTRUE);
TH1D* PlotSIScounts(Int_t runNumber, Int_t channel, const char* description, Int_t PulseChan, Double_t tmin, Double_t tmax, Bool_t zeroTime=kTRUE);
TH1D* PlotSIScounts(Int_t runNumber, Int_t channel, Double_t runtmin, Double_t runtmax, Int_t PulseChan, Double_t tmin, Double_t tmax, Bool_t zeroTime=kTRUE);
TH1D*  PlotSIScounts(Int_t runNumber, Int_t channel, Double_t runtmin, Double_t runtmax, Double_t QpulseTimes[], Int_t pulseCount, Double_t tmin,Double_t tmax, Bool_t zeroTime=kTRUE);
TH1D* PlotSIScounts(Int_t runNumner, Int_t channel, const char* description, Int_t repetition=1, Int_t offset=0);
TH1D* PlotSIScounts(char* runfile, Int_t channel, const char* description, Int_t repetition=1, Int_t offset=0);

TCanvas* Plot_ATOM_AND( Int_t runNumber, const char* description, Int_t repetition= 1, Int_t offset= 0);
TCanvas* Plot_ATOM_AND(Int_t runNumber, Double_t tmin, Double_t tmax);

TCanvas* Plot_CATCH_STICK(Int_t runNumber, const char* description, Int_t repetition=1, Int_t offset=0 );
TCanvas* Plot_CATCH_STICK(Int_t runNumber, Double_t tmin, Double_t tmax);

Double_t FitEnergyDump(Double_t Emin, Double_t Emax);
TCanvas* PlotEnergyDump(Int_t runNumber, Int_t binNumber=-1);//, Int_t repetition = 1, Int_t offset=0);
TCanvas* PlotEnergyDump(Int_t runNumber, const char* dumpFile, const char* dumpDescription, const char* channel, Int_t binNumber=2000, Int_t repetition = 1, Int_t offset=0);
TCanvas* PlotEnergyDump(Int_t runNumber, const char* dumpFile, const char* dumpDescription, Int_t det, Int_t binNumber=2000,  Int_t repetition = 1, Int_t offset=0, Double_t EnergyRangeFactor=10);

Double_t GetBackgroundIntegral(Int_t runNumber, Double_t& Nbkg);
Double_t CalculateHistoIntegral(TH1* histo, Double_t& xmin, Double_t& xmax);
TCanvas* PlotPbarLifetimeGen(Int_t runNumber, Int_t chan, Double_t tmin=0, Double_t tmax=-1,Double_t tbkgmin=0.,Double_t tbkgmax=0., Double_t bkgratefix=-1);
Double_t RoundToBinTop(TH1* histo, Double_t& time);
Double_t RoundToBinBottom(TH1* histo, Double_t& time);

//QOD
void GetDetectorTemp_atRunTime(Int_t runNumber, Double_t runTime, Double_t &EURT1, Double_t &EURT2);

TCanvas* DeltaT_VF48Timestamp(Int_t runNumber, const char* description, Double_t MaxRange=0.2, Int_t repetition=1, Int_t offset=0 );
TCanvas* DeltaT_VF48Timestamp(Int_t runNumber, Double_t tmin, Double_t tmax, Double_t MaxRange=0.2);


void PrintDetectorQOD(Int_t runNumber, const char* description, Int_t repetition=1, Int_t offset=0, TString CSVOutputName="");
void PrintDetectorQOD(Int_t runNumber, Double_t tmin=0., Double_t tmax=-1.,  TString CSVOutputName="");

void SaveHistogram(Int_t runNumber, TString dir, TString plot_name, TString plot_title, TString savSubFolder="");
void SaveCanvas();
void SaveCanvas(Int_t runNumber, const char* Description ="");
void SaveCanvas(TString Description);

TCanvas* PlotGeneralMultiplicityQOD(Int_t runNumber,const char* description,Int_t repetition=1,Int_t offset=0);
TCanvas* PlotGeneralMultiplicityQOD(Int_t runNumber,Double_t tmin=0., Double_t tmax=-1.);

//TCanvas* PlotGeneralMultipicityHisto(Int_t runNumber,const char* description,Int_t repetition=1,Int_t offset=0);

TCanvas* PlotGeneralMultiplicityHisto(Int_t runNumber,Double_t tmin=0., Double_t tmax=-1.);
TH1D** GetGeneralMultiplicityHisto(Int_t runNumber, Double_t tmin=0., Double_t tmax=-1.);
TH1D* PlotNHitHisto(Int_t runNumber,const char* description,Int_t repetition,Int_t offset,Bool_t RAWHits=kFALSE);
TH1D* PlotNHitHisto(Int_t runNumber,Double_t tmin=0., Double_t tmax=-1., Bool_t RAWHits=kFALSE);
TCanvas* PlotOccupancyAgainstTime(Int_t runNumber,const char* description,Int_t repetition=1,Int_t offset=0);
TCanvas* PlotOccupancyAgainstTime_fixedBinWidth(Int_t runNumber,Double_t tmin=0,Double_t tmax=-1, Double_t BinWidth=10);
TCanvas* PlotOccupancyAgainstTime(Int_t runNumber,Double_t tmin=0,Double_t tmax=-1);
TH2D** GetOccupancyAgainstTime(Int_t runNumber,const char* description,Int_t repetition=1,Int_t offset=0);
//TH2D** GetOccupancyAgainstTime(Int_t runNumber,Double_t tmin=0,Double_t tmax=-1);
TH2D** GetOccupancyAgainstTime(Int_t runNumber,Double_t tmin=0,Double_t tmax=-1);
TCanvas* PerStripOccupancy(const char* runfile);
TCanvas* PerStripOccupancy(Int_t runNumber);
TCanvas* PerModuleOccupancy(const char* runfile);
TCanvas* PerModuleOccupancy(Int_t runNumber);
TH1D* SeriesStripOccupancy(Int_t runNumber, Int_t ASICNumber, Int_t moduleNumber);
TH1D* SeriesStripOccupancy(const char* runfile, Int_t ASICNumber, Int_t moduleNumber);
Int_t SeriesOccupancy(Int_t runNumber, Int_t ASICNumber, Int_t moduleNumber);
Int_t SeriesOccupancy(const char* runfile, Int_t ASICNumber, Int_t moduleNumber);

//Int_t GetCountsInModule(Int_t runNumber, Int_t ModuleNumber, Double_t tmin, Double_t tmax);

void SaveStripMeanCompareHistogramsSummary(const char* runfile, TString saveName);
void SaveStripMeanCompareHistogramsSummary(Int_t runNumber, TString saveName);
TCanvas* stripMeanCompareHistogramsSummary(const char* runfile);
TCanvas* stripMeanCompareHistogramsSummary(Int_t runNumber);
TH2D* stripMeanCompareHistograms(const char* runfile, Int_t moduleNumber, Int_t ASICNumber);

TH2D* stripMeanCompareHistograms( Int_t runNumber, Int_t moduleNumber, Int_t ASICNumber);

//Double_t* GetPulseTimes_SeqPulseTiming(Int_t runNumber, Int_t PulsesInDump, Double_t ObservationStart, Double_t ObservationStop);
//void PlotMultiVert(char* runList, Int_t ch, const char* description, Int_t repetition=1, Int_t offset =0);
//void PlotMultiVert(Int_t runNumber,  Int_t ch,const char* description, Int_t repetition=1, Int_t offset=0 );
//void PlotMultiVert(Int_t runNumber, Int_t ch=-1,  Double_t tmin=0., Double_t tmax=-1.);
void PlotMultiSIS(Int_t runNumber,  Int_t ch, const char* description, Int_t repetition=1, Int_t offset=0 );
void PlotMultiSIS(Int_t runNumber, Int_t ch,  Double_t tmin, Double_t tmax);

TGraph* PlotBank(Int_t runNumber, TString bank, Int_t array_num, const char* description, Int_t repetition=1, Int_t offset=0, const char* opts = NULL, Int_t Colour = 1 );
TGraph* PlotBank(Int_t runNumber, TString bank, Int_t array_num, Double_t tmin=0., Double_t tmax=-1., const char* opts = NULL, Int_t Colour = 1 );
TGraph* GraphBank(Int_t runNumber, TString Bank, Int_t Array_number, Double_t start=0., Double_t stop=-1.);

Int_t LoadRampFile(const char* filename, Double_t* x, Double_t* y);

void ListSeriesInDatabase();
TString FindQWPInDatabase( int runNumber);
void AddSeriesToDatabase(const char* filename);
void AddSeriesToDatabase(int runNumber, TString SeriesName, TString QWP);

TString FindSeriesInDatabase(int runNumber);
TH1I** GetTTC_by_layer_simple(Int_t runNumber, Double_t tmin, Double_t tmax);

TCanvas* PlotAllSISChannels(Int_t runNumber, Double_t tmin=0., Double_t tmax=-1.);


Double_t GetSIScoinicence(Int_t runNumber, Double_t tmin=0, Double_t tmax=-1.);
void PrintADInjectionClockSync_YMXC(Int_t runNumber);
void GetADInjectionClockSync_YMXC(Int_t runNumber, Double_t &slope, Double_t &intercept, Int_t &nsyncs);
TGraph* PlotADinjectionSISvsVF48(Int_t runNumber);




TSpline5* InterpolateVoltageRamp(const char* filename);

Int_t ReturnQuenchVF48Numbers( Int_t runNumber, Double_t endTime, Int_t& first_VF48, Int_t& last_VF48, Int_t NoTalk=0);

Int_t SaveQuenchVF48Events( Int_t runNumber, Double_t endTime=-1,  Bool_t OnlyPassedCuts=kFALSE  );
Int_t PrintQuenchGateWindow( Int_t runNumber, Double_t endTime=-1, Bool_t PassedCutsOnly=kTRUE);
Int_t PrintDumpGateWindow( Int_t runNumber, const char* description, Int_t repetition=1, Int_t offset=0, Bool_t PassedCutsOnly=kTRUE);
Int_t PrintTimeGateWindow( Int_t runNumber, Double_t tmin, Double_t tmax, TString TitleString, Bool_t PassedCutsOnly=kTRUE);
Int_t PlotExtendedLaserDumpTimes(Int_t runNumber,Int_t nList,Int_t nFreq,Int_t nRep, Bool_t PassCutsOnly);
Int_t PassedCuts(Int_t runNumber, const char* description, Int_t repetition=1, Int_t offset=0);
Int_t PassedCuts(Int_t runNumber, Double_t start, Double_t stop);

TCanvas* PlotDump(Int_t runNumber, const char* description, const char* SISChannel, Int_t repetition = 1, Int_t offset=0);
TCanvas* PlotDump(Int_t runNumber, const char* description, Int_t SISChannel, Int_t repetition = 1, Int_t offset=0);
TGraph* Get_SiMod_TA_Events( Int_t runNumber, Double_t start_time=0., Double_t end_time=99999. );
TGraph* Get_SiMod_TA_Events( Int_t runNumber, const char* description, Int_t repetition = 1, Int_t offset=0 );
TGraph* Get_SiMod_TA_Events( const char* runfile, const char* description, Int_t repetition=1, Int_t offset=0 );
TH1D* Get_TA_Events( Int_t runNumber, Double_t start_time=0., Double_t end_time=99999. );
TH1D* Get_TA_Events( Int_t runNumber, const char* description, Int_t repetition = 1, Int_t offset=0 );
TCanvas* Plot_SiMod_TA_Events( const char* runfile, const char* description, Int_t repetition=1, Int_t offset=0 );
TCanvas* Plot_SiMod_TA_Events( Int_t runNumber, Double_t start_time=0., Double_t end_time=99999. );
TCanvas* Plot_SiMod_TA_Events( Int_t runNumber, const char* description, Int_t repetition = 1, Int_t offset=0 );
TCanvas* Plot_TA_Events( Int_t runNumber, Double_t start_time=0., Double_t end_time=99999. );
TCanvas* Plot_TA_Events( Int_t runNumber, const char* description, Int_t repetition=1, Int_t offset=0 );
TCanvas* Plot_TA_Events( const char* runfile ,const char* description, Int_t repetition=1, Int_t offset=0  );

TCanvas* PlotPbarLifetime(Double_t tmin=-1., Double_t tmax=-1.);
TCanvas* PbarLifetimeDisplay(Int_t runNumber,const char* description="Lifetime", Int_t repetition=1, Int_t offset=0, Bool_t backgroundSubtract=kTRUE);
TCanvas* PbarLifetimeDisplay(Int_t runNumber,Double_t tmin, Double_t tmax, Bool_t backgroundSubtract=kTRUE);
//TCanvas* PlotPbarLifetime(Int_t runNumber, Double_t tmin, Double_t tmax);
//New since 04/May/2016 JamesT
TCanvas* Plot_CATCH_OR_With_ADVeto(Int_t runNumber, Double_t tmin, Double_t tmax);
TCanvas* Plot_CATCH_AND_With_ADVeto(Int_t runNumber, Double_t tmin, Double_t tmax);
TH1D* PlotSIScountsWithVeto(Int_t runNumber, Int_t channel, vector<Int_t> VetoChan, Double_t tmin, Double_t tmax, Bool_t zeroTime);
//TArray* Get_RunTime_of_Pulse(Int_t runNumber, vector<Int_t> VetoChan, Int_t tmin, Int_t tmax);
TCanvas* Plot_CATCH_Lifetime_With_ADVeto(Int_t runNumber);
Double_t GetBackgroundIntegralWithADVeto(Int_t runNumber, Double_t& Nbkg);
TCanvas* PlotPbarLifetimeGenWithADVeto(Int_t runNumber, Int_t chan, Double_t tmin, Double_t tmax,Double_t tbkgmin,Double_t tbkgmax, Double_t bkgrate_fix);
TCanvas* PlotPbarLifetimeLongWithADVeto(Int_t runNumber, Double_t tmin=0, Double_t tmax=-1,Double_t tbkgmin=0.,Double_t tbkgmax=0., Double_t bkgrate_fix=-1);
//TCanvas* Plot_CATCH_OR_With_ADVeto( Int_t runNumber, const char* description, Int_t repetition, Int_t offset );
//TCanvas* Plot_CATCH_Lifetime_With_ADVeto_v2(Int_t runNumber);
//TCanvas* Plot_CATCH_Background(Int_t runNumber);
//Double_t FindBackgroundTime(Int_t runNumber, Double_t& tmin, Double_t& tmax);
//TCanvas* Plot_CATCH_OR_Lifetime(Int_t runNumber);


void PrintSequenceQOD(const char* runfile);
Int_t PrintSequenceQOD(Int_t runNumber, Int_t offset = 0);
TGraph* Get121Power(Int_t irunnumber, TString idescription, Int_t irepetition, Int_t ioffset);
