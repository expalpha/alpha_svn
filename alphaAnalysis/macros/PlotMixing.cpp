/*
 Macro for highly automated production of online mixing analysis plots

 Eoin Butler, Sep 2, 2009
*/

/*
 * To do
 * More intelligently handle chains, sequences being aborted, etc
 * Add/average/overlay mixing plots
 * Cuts on vertices
 * Add positrons
 */

/* Art Olin Sept 13
 * Change rdens hist to filling with 1/r weights.
 *After invoking hist->Sumw2() errors are calculated correctly
 * Added fitting code (ringfits.cpp, radfits.cpp) to fit gas and
 * wall annihilation rates
 *Added call PlotVertices(file, dumpName) to sum runs
*/

#include "AlphaAnalysisUtilities.cpp"
#include "EnergyDumps.cpp"
#include "Find_LabVIEW_Events.cpp"
#include "TVertexCut.cpp"
#include "ExtractQuenchEventNumbers.cpp"
#include "PlotQuench.cpp"
//#include "PlotMixing_AverageOverlay.cpp"
#include <TF1.h>
//#include "checkBackground.cpp"
//#include "../lib/include/SIS_Channels.h.alpha1"

// The number of bins for the time plots in the mixing graphs
Int_t NUMBER_BINS(100);
#define OCTUPOLE_RES 100

// The descriptions for the dumps
#define MIXING_NAME "Mixing"
#define LEFT_NAME "Left"
#define RIGHT_NAME "Right"
#define HOT_NAME "Hot Dump"
#define QUENCH_NAME "Quench"

// Which channels to use for the PMT plots for the left and right dumps - will always als plot the Silicon triggers
#define LEFT_DET SIS_PMT_DEG_OR
#define RIGHT_DET SIS_PMT_DOWN_OR

//The transformer value to scale to (Set to 0.0 to disable normalisation)
#define TRANSFORMER_NORMALISED 0.0

#define DO_VERTICES // Uncomment this line to also produce vertex plots
#define ALLOW_VERTEX_CUTS //Uncomment this line to allow for cuts on the vertices
//#define RINGFIT // Uncomment to perform fits on the ring
//#define NORMALISE_TIME_HISTOS
//#define NORMALISE_Z_DISTS

#define FREE_TREES
#define SPEEDY_COUNTS_MAX 2

Bool_t gSAVEONLYVERTICES(kFALSE); // When uncommented, only events which have vertices will be saved by SaveVertices(), otherwise all are saved

#define QUENCH_ENDOFFSET_LONG 1.0
#define QUENCH_STARTOFFSET_LONG -1.0
#define QUENCH_TIMERES_LONG 20.e-3
#define QUENCH_ENDOFFSET_SHORT 0.1
#define QUENCH_STARTOFFSET_SHORT -0.1
#define QUENCH_TIMERES_SHORT 2.e-3

#define RO_IN_VERTS
#define MIXING_LOG

// The indices of the histograms in the vertex TObjArray
#define Z_INDEX 0
#define ZPHI_INDEX 1
#define ZT_INDEX 2
#define XY_INDEX 3
#define T_INDEX 4
#define R_INDEX 5
#define RDENS_INDEX 6
#define ZR_INDEX 7
#define PHI_INDEX 8
#define TR_INDEX 9


// The resolution of the vertex plots (used to determine the binning needed)
Double_t VERTEX_RES_Z(0.4);
Double_t VERTEX_RES_PHI(10.);
Double_t VERTEX_RES_TRANSVERSE(0.2); // For x-y
Double_t VERTEX_RES_RADIAL(0.1);

Double_t Z_MIN(-20.);
Double_t Z_MAX(20.);
Double_t R_MAX(5.);

TString* gVerticesFile;

#define VERTEX_TITLE_SIZE 0.05

#define MATCH_TIME_TOLERANCE 0.0015
#define MATCH_SEQUENCE_TOLERANCE 2

#define ALLOWFILELIST

char text[500];
Int_t goptStat(00000000)
gStyle->SetOptStat(goptStat);
TVertexCut* gCut = new TVertexCut();
Int_t gNormaliseOverlay(0);
char drawOpts[50] = "E1P";

TCanvas* gActiveCanvas;

#define timing_info
#ifdef timing_info
    TStopwatch* myStopwatch = new TStopwatch();
#endif

// ==================== PLOT MIXING ======================================================================

void PlotMixing (Int_t runNumber)
{
    Int_t mixingDump = MatchDumpNameToDumpNumber(runNumber, MIXING_NAME);

    TSeq_Event* seqEvent = FindSequencerEvent(runNumber, "startDump", "Quench");
    //if(seqEvent==NULL)
    //{
        Int_t leftDump = MatchDumpNameToDumpNumber(runNumber, LEFT_NAME);
        Int_t rightDump = MatchDumpNameToDumpNumber(runNumber, RIGHT_NAME);
    //}
    //else
    //{
    //  Int_t leftDump(0);
    //  Int_t rightDump(0);
    //
       if(seqEvent) PlotQuench(runNumber);
    //}

    /*
    if(leftDump<mixingDump || FindSequencerEvent(runNumber, "startDump", MIXING_NAME)==NULL)
    {
        sprintf(text, "Detected that this is probably a dump before mixing");
        Warning("PlotMixing", text);
        PlotBeforeMixing(runNumber, 1, leftDump, rightDump);
    }
    else
    */
    PlotMixing(runNumber, 1, mixingDump, leftDump, rightDump);


}

void PlotMixing (Int_t runNumber, Int_t ChainNumber, Int_t mixingDump, Int_t leftDump, Int_t rightDump, Double_t transformerValue=0.)
{
    gStyle->SetOptStat(goptStat);
    sprintf(text, "Mixing Plots");
    if(leftDump>0 || rightDump>0) TCanvas* c= new TCanvas(text, text, 800, 840);
    else TCanvas* c= new TCanvas(text, text, 800, 480);

    if(transformerValue==0. && TRANSFORMER_NORMALISED>0.) // then automatically find the transformer value
    {
        transformerValue=GetTransformerValue(runNumber);
    }

    // Mixing Histogram

    if(leftDump>0 || rightDump>0) TPad* mixingOverPad = new TPad("MixingOver", "MixingOver", 0., 0.5, 1., 1.);
    else TPad* mixingOverPad = new TPad("MixingOver", "MixingOver", 0., 0., 1., 1.);

    mixingOverPad->Draw();
    mixingOverPad->cd();

    TPad* mixingPad = new TPad("Mixing", "Mixing", 0., 0., 1., 1.);
    mixingPad->Draw();
    mixingPad->cd();

    #ifdef MIXING_LOG
        mixingPad->SetLogy(1);
    #endif

    Int_t ttc_detectors[2] = {61, 48};

    PlotMixingDump(runNumber, ChainNumber, mixingDump, SIS2_TTC_SI_TRIG, transformerValue); // Si>1
    //PlotMixingDump(runNumber, ChainNumber, mixingDump, 39, transformerValue, 3);
    PlotMixingDump(runNumber, ChainNumber, mixingDump, 45, transformerValue, 2); // Readout
    PlotMixingDump(runNumber, ChainNumber, mixingDump, 56, transformerValue, 4); // T3 OR T6
    //ShowPositronNumber(runNumber, mixingDump);


    if(leftDump>0  || rightDump>0)
    {
        c->cd();
        TPad* dumpOverPad = new TPad("DumpOver", "DumpOver", 0., 0., 1., 0.5);
        dumpOverPad->Draw();
        dumpOverPad->cd();

        //Left Dump
        TPad* leftDumpPad1= new TPad("LeftDump", "LeftDump", 0., 0.5, 0.5, 1.0);
        leftDumpPad1->Draw();
        TPad* leftDumpPad2= new TPad("LeftDump2", "LeftDump2", 0., 0., 0.5, 0.5);
        leftDumpPad2->Draw();

        if(leftDump>0.)
        {
            leftDumpPad1->cd();
            PlotLeftDump(runNumber, ChainNumber, leftDump, LEFT_DET, transformerValue);
            AddRandomText("PMT", kRed, 1.);
            leftDumpPad2->cd();
            PlotLeftDump(runNumber, ChainNumber, leftDump, SIS2_TTC_SI_TRIG, transformerValue);
            AddRandomText("Si>1", kRed, 1.);
            gLastHistoPointer->SetLineColor(kRed);
        }

        //Right Dump
        dumpOverPad->cd();
        TPad* rightDumpPad1= new TPad("RightDump", "RightDump", 0.5, 0.5, 1.0, 1.0);
        rightDumpPad1->Draw();
        TPad* rightDumpPad2= new TPad("RightDump2", "RightDump2", 0.5, 0., 1.0, 0.5);
        rightDumpPad2->Draw();

        if(rightDump>0.)
        {
            rightDumpPad1->cd();
            PlotRightDump(runNumber, ChainNumber, rightDump, RIGHT_DET, transformerValue);
            AddRandomText("PMT", kRed, 1.);
            rightDumpPad2->cd();
            PlotRightDump(runNumber, ChainNumber, rightDump, SIS2_TTC_SI_TRIG, transformerValue);
            AddRandomText("Si>1", kRed, 1.);
            gLastHistoPointer->SetLineColor(kRed);
        }
    }

    #ifdef DO_VERTICES
        PlotVertices(runNumber, mixingDump);
        sprintf(text, "/home/alpha/alphaSoftware2012/alphaAnalysis/macrosA1/plots/run%d_vertex_%s.png", runNumber, MIXING_NAME);
        gActiveCanvas->SaveAs(text);
    #endif

    sprintf(text, "/home/alpha/alphaSoftware2012/alphaAnalysis/macrosA1/plots/run%d_mixing.png", runNumber);
    c->SaveAs(text);
}

void PlotBeforeMixing (Int_t runNumber)
{
    Int_t leftDump = MatchDumpNameToDumpNumber(runNumber, LEFT_NAME);
    Int_t rightDump = MatchDumpNameToDumpNumber(runNumber, RIGHT_NAME);

    PlotBeforeMixing(runNumber, 1, leftDump, rightDump);
}

void PlotBeforeMixing (Int_t runNumber, Int_t ChainNumber, Int_t leftDump, Int_t rightDump, Double_t transformerValue=0.)
{
    gStyle->SetOptStat(goptStat);
    sprintf(text, "Mixing Plots");
    TCanvas* c= new TCanvas(text, text, 800, 400);

    if(transformerValue==0. && TRANSFORMER_NORMALISED>0.) // then automatically find the transformer value
    {
        transformerValue=GetTransformerValue(runNumber);
    }

    //Left Dump
    c->cd();
    TPad* leftDumpPad1 = new TPad("LeftDump", "LeftDump", 0., 0.5, 0.5, 1);
    leftDumpPad1->Draw();
    leftDumpPad1->cd();
    PlotLeftDump(runNumber, ChainNumber, leftDump, LEFT_DET, transformerValue);
    c->cd();
    TPad* leftDumpPad2 = new TPad("LeftDump", "LeftDump", 0., 0., 0.5, 0.5);
    leftDumpPad2->Draw();
    leftDumpPad2->cd();
    PlotLeftDump(runNumber, ChainNumber, leftDump, SIS2_TTC_SI_TRIG, transformerValue);

    //Right Dump
    c->cd();
    TPad* rightDumpPad1 = new TPad("RightDump", "RightDump", 0.5, 0.5, 1., 1);
    rightDumpPad1->Draw();
    rightDumpPad1->cd();

    PlotRightDump(runNumber, ChainNumber, rightDump, RIGHT_DET, transformerValue);
    c->cd();
    TPad* rightDumpPad2 = new TPad("RightDump", "RightDump", 0.5, 0., 1., 0.5);
    rightDumpPad2->Draw();
    rightDumpPad2->cd();
    PlotRightDump(runNumber, ChainNumber, rightDump, SIS2_TTC_SI_TRIG, transformerValue);

    sprintf(text, "TRAFO %.2lf normalised to %.2lf\n", transformerValue, TRANSFORMER_NORMALISED);
    if(TRANSFORMER_NORMALISED>0.) AddRandomTextWidth(text, 4, 0.5, 0.7, 0);
}

void PlotTriggers(Int_t runNumber)
{
    Int_t mixingDump = MatchDumpNameToDumpNumber(runNumber, MIXING_NAME);
    PlotTriggers(runNumber, 1, mixingDump);
}

void PlotTriggers(Int_t runNumber, Int_t ChainNumber, Int_t mixingDump, Double_t transformerValue=0.)
{
    gStyle->SetOptStat(goptStat);
    sprintf(text, "Mixing Plots", runNumber);
    TCanvas* c= new TCanvas(text, text, 800, 600);

    if(transformerValue==0. && TRANSFORMER_NORMALISED>0.) // then automatically find the transformer value
    {
        transformerValue=GetTransformerValue(runNumber);
    }

    // Mixing Histogram

    TPad* mixingPad = new TPad("Mixing", "Mixing", 0, 0., 1., 1.);
    mixingPad->Draw();
    mixingPad->cd();

    PlotMixingDump(runNumber, ChainNumber, mixingDump, SIS2_TTC_SI_TRIG, transformerValue);

}

void PlotQuench(Int_t runNumber, Int_t rep=1)
{
    sprintf(text,"Quench Triggers");
    gStyle->SetOptStat(goptStat);
    TCanvas* quenchCanvas = new TCanvas(text,text,1200,600);

    Double_t startTime = Get_RunTime_of_Count(runNumber, SIS_QUENCH_START, rep);
    TVirtualPad* currentPad;
    TH1D* quenchHisto;
    Double_t zeroTime;

    quenchCanvas->cd();
    quenchCanvas->Divide(3,2, 0.005, 0.005);

    currentPad = quenchCanvas->cd(1);
    SetTimeResolution(QUENCH_TIMERES_LONG);
    quenchHisto = GetHistoByTimes(runNumber, SIS2_TTC_SI_TRIG,  startTime+QUENCH_STARTOFFSET_LONG, startTime+QUENCH_ENDOFFSET_LONG);
    sprintf(text, "Si>1, Run %d", runNumber);
    quenchHisto->SetTitle(text);
    quenchHisto->GetXaxis()->SetLimits(QUENCH_STARTOFFSET_LONG, QUENCH_ENDOFFSET_LONG);
    quenchHisto->SetLineColor(2);
    if(quenchHisto->Integral()>0.) quenchHisto->GetYaxis()->SetRangeUser(0.1, quenchHisto->GetMaximum()*3);
    else quenchHisto->GetYaxis()->SetRangeUser(0.5, 1);
    currentPad->SetLogy(1);
    quenchHisto->GetXaxis()->SetTitle("Time [s]");
    sprintf(text, "Counts / %.0lf ms", QUENCH_TIMERES_LONG*1000);
    quenchHisto->GetYaxis()->SetTitle(text);
    quenchHisto->DrawCopy();
    zeroTime = TimeToBinNumber(quenchHisto, 0.);
    sprintf(text, "t<0: %d", quenchHisto->Integral(1, zeroTime));
    AddRandomText(text,1, 0.7, 0);
    sprintf(text, "t>0: %d", quenchHisto->Integral(zeroTime+1, quenchHisto->GetNbinsX()));
    AddRandomText(text,1, 0.7, 1);

    currentPad = quenchCanvas->cd(2);
    SetTimeResolution(QUENCH_TIMERES_LONG);
    quenchHisto = GetHistoByTimes(runNumber, 61,  startTime+QUENCH_STARTOFFSET_LONG, startTime+QUENCH_ENDOFFSET_LONG);
    quenchHisto->SetName("quenchHisto");
    quenchHisto->Add(GetHistoByTimes(runNumber, 48,  startTime+QUENCH_STARTOFFSET_LONG, startTime+QUENCH_ENDOFFSET_LONG));
    sprintf(text, "T3 + T6, Run %d", runNumber);
    quenchHisto->SetTitle(text);
    quenchHisto->GetXaxis()->SetLimits(QUENCH_STARTOFFSET_LONG, QUENCH_ENDOFFSET_LONG);
    quenchHisto->SetLineColor(4);
    if(quenchHisto->Integral()>0.) quenchHisto->GetYaxis()->SetRangeUser(0.1, quenchHisto->GetMaximum()*3);
    else quenchHisto->GetYaxis()->SetRangeUser(0.5, 1);
    currentPad->SetLogy(1);
    quenchHisto->GetXaxis()->SetTitle("Time [s]");
    sprintf(text, "Counts / %.0lf ms", QUENCH_TIMERES_LONG*1000);
    quenchHisto->GetYaxis()->SetTitle(text);
    quenchHisto->DrawCopy();
    zeroTime = TimeToBinNumber(quenchHisto, 0.);
    sprintf(text, "t<0: %d", quenchHisto->Integral(1, zeroTime));
    AddRandomText(text,1, 0.7, 0);
    sprintf(text, "t>0: %d", quenchHisto->Integral(zeroTime+1, quenchHisto->GetNbinsX()));
    AddRandomText(text,1, 0.7, 1);
    delete quenchHisto;

    currentPad = quenchCanvas->cd(3);
    SetTimeResolution(QUENCH_TIMERES_LONG);
    quenchHisto = GetHistoByTimes(runNumber, 39,  startTime+QUENCH_STARTOFFSET_LONG, startTime+QUENCH_ENDOFFSET_LONG);
    sprintf(text, "SAP, Run %d", runNumber);
    quenchHisto->SetTitle(text);
    quenchHisto->GetXaxis()->SetLimits(QUENCH_STARTOFFSET_LONG, QUENCH_ENDOFFSET_LONG);
    quenchHisto->SetLineColor(51);
    if(quenchHisto->Integral()>0.) quenchHisto->GetYaxis()->SetRangeUser(0.1, quenchHisto->GetMaximum()*3);
    else quenchHisto->GetYaxis()->SetRangeUser(0.5, 1);
    currentPad->SetLogy(1);
    quenchHisto->GetXaxis()->SetTitle("Time [s]");
    sprintf(text, "Counts / %.0lf ms", QUENCH_TIMERES_LONG*1000);
    quenchHisto->GetYaxis()->SetTitle(text);
    quenchHisto->DrawCopy();
    zeroTime = TimeToBinNumber(quenchHisto, 0.);
    sprintf(text, "t<0: %d", quenchHisto->Integral(1, zeroTime));
    AddRandomText(text,1, 0.7, 0);
    sprintf(text, "t>0: %d", quenchHisto->Integral(zeroTime+1, quenchHisto->GetNbinsX()));
    AddRandomText(text,1, 0.7, 1);

    currentPad = quenchCanvas->cd(4);
    SetTimeResolution(QUENCH_TIMERES_SHORT);
    quenchHisto = GetHistoByTimes(runNumber, SIS2_TTC_SI_TRIG,  startTime+QUENCH_STARTOFFSET_SHORT, startTime+QUENCH_ENDOFFSET_SHORT);
    sprintf(text, "Si>1, Run %d", runNumber);
    quenchHisto->SetTitle(text);
    quenchHisto->GetXaxis()->SetLimits(QUENCH_STARTOFFSET_SHORT, QUENCH_ENDOFFSET_SHORT);
    quenchHisto->SetLineColor(2);
    quenchHisto->GetYaxis()->SetRangeUser(0., quenchHisto->GetMaximum()*1.1);
    quenchHisto->GetXaxis()->SetTitle("Time [s]");
    sprintf(text, "Counts / %.1lf ms", QUENCH_TIMERES_SHORT*1000);
    quenchHisto->GetYaxis()->SetTitle(text);
    quenchHisto->DrawCopy();
    zeroTime = TimeToBinNumber(quenchHisto, 0.);
    sprintf(text, "t<0: %d", quenchHisto->Integral(1, zeroTime));
    AddRandomText(text,1, 0.7, 0);
    sprintf(text, "t>0: %d", quenchHisto->Integral(zeroTime+1, quenchHisto->GetNbinsX()));
    AddRandomText(text,1, 0.7, 1);

    currentPad = quenchCanvas->cd(5);
    SetTimeResolution(QUENCH_TIMERES_SHORT);
    quenchHisto = GetHistoByTimes(runNumber, 61,  startTime+QUENCH_STARTOFFSET_SHORT, startTime+QUENCH_ENDOFFSET_SHORT);
    quenchHisto->SetName("quenchHisto");
    quenchHisto->Add(GetHistoByTimes(runNumber, 48,  startTime+QUENCH_STARTOFFSET_SHORT, startTime+QUENCH_ENDOFFSET_SHORT));
    sprintf(text, "T3 + T6, Run %d", runNumber);
    quenchHisto->SetTitle(text);
    quenchHisto->GetXaxis()->SetLimits(QUENCH_STARTOFFSET_SHORT, QUENCH_ENDOFFSET_SHORT);
    quenchHisto->SetLineColor(4);
    quenchHisto->GetYaxis()->SetRangeUser(0., quenchHisto->GetMaximum()*1.1);
    quenchHisto->GetXaxis()->SetTitle("Time [s]");
    sprintf(text, "Counts / %.1lf ms", QUENCH_TIMERES_SHORT*1000);
    quenchHisto->GetYaxis()->SetTitle(text);
    quenchHisto->DrawCopy();
    zeroTime = TimeToBinNumber(quenchHisto, 0.);
    sprintf(text, "t<0: %d", quenchHisto->Integral(1, zeroTime));
    AddRandomText(text,1, 0.7, 0);
    sprintf(text, "t>0: %d", quenchHisto->Integral(zeroTime+1, quenchHisto->GetNbinsX()));
    AddRandomText(text,1, 0.7, 1);
    delete quenchHisto;

    currentPad = quenchCanvas->cd(6);
    SetTimeResolution(QUENCH_TIMERES_SHORT);
    quenchHisto = GetHistoByTimes(runNumber, 39,  startTime+QUENCH_STARTOFFSET_SHORT, startTime+QUENCH_ENDOFFSET_SHORT);
    sprintf(text, "SAP, Run %d", runNumber);
    quenchHisto->SetTitle(text);
    quenchHisto->GetXaxis()->SetLimits(QUENCH_STARTOFFSET_SHORT, QUENCH_ENDOFFSET_SHORT);
    quenchHisto->SetLineColor(51);
    quenchHisto->GetYaxis()->SetRangeUser(0., quenchHisto->GetMaximum()*1.1);
    quenchHisto->GetXaxis()->SetTitle("Time [s]");
    sprintf(text, "Counts / %.1lf ms", QUENCH_TIMERES_SHORT*1000);
    quenchHisto->GetYaxis()->SetTitle(text);
    quenchHisto->DrawCopy();
    zeroTime = TimeToBinNumber(quenchHisto, 0.);
    sprintf(text, "t<0: %d", quenchHisto->Integral(1, zeroTime));
    AddRandomText(text,1, 0.7, 0);
    sprintf(text, "t>0: %d", quenchHisto->Integral(zeroTime+1, quenchHisto->GetNbinsX()));
    AddRandomText(text,1, 0.7, 1);

    sprintf(text, "/home/alpha/alphaSoftware2012/alphaAnalysis/macrosA1/plots/run%d_quench.png", runNumber);
    quenchCanvas->SaveAs(text);
}

void PlotQuench(char* filename)
{
    sprintf(text,"Quench Triggers");
    gStyle->SetOptStat(goptStat);
    TCanvas* overcanvas = new TCanvas(text, text, 1200, 650);
    overcanvas->Draw();
    overcanvas->cd();
    Int_t runNumber[500], dumpNumber[500];
    Double_t startTime[500];
    Int_t zeroTime;
    Int_t nRuns = FileToRuns(filename, runNumber, dumpNumber);

    TPad* label = new TPad("label","label",0, 0., 1. ,0.1);
    label->Draw();
    label->cd();
    TText* labelText = new TText();
    labelText->SetTextSize(0.3);
    sprintf(text, "%s [%d Runs]", filename, nRuns);
    labelText->DrawText(0.05, 0.3, text);

    overcanvas->cd();
    TPad* quenchCanvas = new TPad(text,text,0, 0.1, 1. ,1.);
    quenchCanvas->Draw();


    for(Int_t i=0; i<nRuns; i++)
    {
        startTime[i] = Get_RunTime_of_Count(runNumber[i], SIS_QUENCH_START,1);
    }

    TVirtualPad* currentPad;
    TH1D* quenchHisto;

    quenchCanvas->cd();
    quenchCanvas->Divide(3,2, 0.005, 0.005);

    currentPad = quenchCanvas->cd(1);
    SetTimeResolution(QUENCH_TIMERES_LONG);
    for(Int_t i=0; i<nRuns; i++)
    {
        if(i==0)
        {
            quenchHisto = GetHistoByTimes(runNumber[i], SIS2_TTC_SI_TRIG,  startTime[i]+QUENCH_STARTOFFSET_LONG, startTime[i]+QUENCH_ENDOFFSET_LONG);
            quenchHisto->SetName("quenchHisto");
        }

        else quenchHisto->Add(GetHistoByTimes(runNumber[i], SIS2_TTC_SI_TRIG,  startTime[i]+QUENCH_STARTOFFSET_LONG, startTime[i]+QUENCH_ENDOFFSET_LONG));
    }
    sprintf(text, "Si>1");
    quenchHisto->SetTitle(text);
    quenchHisto->GetXaxis()->SetLimits(QUENCH_STARTOFFSET_LONG, QUENCH_ENDOFFSET_LONG);
    quenchHisto->SetLineColor(2);
    if(quenchHisto->Integral()>0.) quenchHisto->GetYaxis()->SetRangeUser(0.1, quenchHisto->GetMaximum()*3);
    else quenchHisto->GetYaxis()->SetRangeUser(0.5, 1);
    currentPad->SetLogy(1);
    quenchHisto->GetXaxis()->SetTitle("Time [s]");
    sprintf(text, "Counts / %.0lf ms", QUENCH_TIMERES_LONG*1000);
    quenchHisto->GetYaxis()->SetTitle(text);
    quenchHisto->DrawCopy();
    zeroTime = TimeToBinNumber(quenchHisto, 0.);
    sprintf(text, "t<0: %d", quenchHisto->Integral(1, zeroTime));
    AddRandomText(text,1, 0.7, 0);
    sprintf(text, "t>0: %d", quenchHisto->Integral(zeroTime+1, quenchHisto->GetNbinsX()));
    AddRandomText(text,1, 0.7, 1);
    delete quenchHisto;

    currentPad = quenchCanvas->cd(2);
    SetTimeResolution(QUENCH_TIMERES_LONG);
    for(Int_t i=0; i<nRuns; i++)
    {
        if(i==0)
        {
            quenchHisto = GetHistoByTimes(runNumber[i], 61,  startTime[i]+QUENCH_STARTOFFSET_LONG, startTime[i]+QUENCH_ENDOFFSET_LONG);
            quenchHisto->SetName("QuenchHisto");
        }

        else quenchHisto->Add(GetHistoByTimes(runNumber[i], 61,  startTime[i]+QUENCH_STARTOFFSET_LONG, startTime[i]+QUENCH_ENDOFFSET_LONG));

        quenchHisto->Add(GetHistoByTimes(runNumber[i], 48,  startTime[i]+QUENCH_STARTOFFSET_LONG, startTime[i]+QUENCH_ENDOFFSET_LONG));
    }
    sprintf(text, "T3 + T6");
    quenchHisto->SetTitle(text);
    quenchHisto->GetXaxis()->SetLimits(QUENCH_STARTOFFSET_LONG, QUENCH_ENDOFFSET_LONG);
    quenchHisto->SetLineColor(4);
    if(quenchHisto->Integral()>0.) quenchHisto->GetYaxis()->SetRangeUser(0.1, quenchHisto->GetMaximum()*3);
    else quenchHisto->GetYaxis()->SetRangeUser(0.5, 1);
    currentPad->SetLogy(1);
    quenchHisto->GetXaxis()->SetTitle("Time [s]");
    sprintf(text, "Counts / %.0lf ms", QUENCH_TIMERES_LONG*1000);
    quenchHisto->GetYaxis()->SetTitle(text);
    quenchHisto->DrawCopy();
    zeroTime = TimeToBinNumber(quenchHisto, 0.);
    sprintf(text, "t<0: %d", quenchHisto->Integral(1, zeroTime));
    AddRandomText(text,1, 0.7, 0);
    sprintf(text, "t>0: %d", quenchHisto->Integral(zeroTime+1, quenchHisto->GetNbinsX()));
    AddRandomText(text,1, 0.7, 1);
    delete quenchHisto;

    currentPad = quenchCanvas->cd(3);
    SetTimeResolution(QUENCH_TIMERES_LONG);
    for(Int_t i=0; i<nRuns; i++)
    {
        if(i==0)
        {
            quenchHisto = GetHistoByTimes(runNumber[i], 39,  startTime[i]+QUENCH_STARTOFFSET_LONG, startTime[i]+QUENCH_ENDOFFSET_LONG);
            quenchHisto->SetName("QuenchHisto");
        }
        else quenchHisto->Add(GetHistoByTimes(runNumber[i], 39,  startTime[i]+QUENCH_STARTOFFSET_LONG, startTime[i]+QUENCH_ENDOFFSET_LONG));
    }
    sprintf(text, "SAP");
    quenchHisto->SetTitle(text);
    quenchHisto->GetXaxis()->SetLimits(QUENCH_STARTOFFSET_LONG, QUENCH_ENDOFFSET_LONG);
    quenchHisto->SetLineColor(51);
    if(quenchHisto->Integral()>0.) quenchHisto->GetYaxis()->SetRangeUser(0.1, quenchHisto->GetMaximum()*3);
    else quenchHisto->GetYaxis()->SetRangeUser(0.5, 1);
    currentPad->SetLogy(1);
    quenchHisto->GetXaxis()->SetTitle("Time [s]");
    sprintf(text, "Counts / %.0lf ms", QUENCH_TIMERES_LONG*1000);
    quenchHisto->GetYaxis()->SetTitle(text);
    quenchHisto->DrawCopy();
    zeroTime = TimeToBinNumber(quenchHisto, 0.);
    sprintf(text, "t<0: %d", quenchHisto->Integral(1, zeroTime));
    AddRandomText(text,1, 0.7, 0);
    sprintf(text, "t>0: %d", quenchHisto->Integral(zeroTime+1, quenchHisto->GetNbinsX()));
    AddRandomText(text,1, 0.7, 1);
    delete quenchHisto;

    currentPad = quenchCanvas->cd(4);
    SetTimeResolution(QUENCH_TIMERES_SHORT);
    for(Int_t i=0; i<nRuns; i++)
    {
        if(i==0)
        {
            quenchHisto = GetHistoByTimes(runNumber[i], SIS2_TTC_SI_TRIG,  startTime[i]+QUENCH_STARTOFFSET_SHORT, startTime[i]+QUENCH_ENDOFFSET_SHORT);
            quenchHisto->SetName("quenchHisto");
        }

        else quenchHisto->Add(GetHistoByTimes(runNumber[i], SIS2_TTC_SI_TRIG,  startTime[i]+QUENCH_STARTOFFSET_SHORT, startTime[i]+QUENCH_ENDOFFSET_SHORT));
    }
    sprintf(text, "Si>1");
    quenchHisto->SetTitle(text);
    quenchHisto->GetXaxis()->SetLimits(QUENCH_STARTOFFSET_SHORT, QUENCH_ENDOFFSET_SHORT);
    quenchHisto->SetLineColor(2);
    quenchHisto->GetYaxis()->SetRangeUser(0., quenchHisto->GetMaximum()*1.1);
    quenchHisto->GetXaxis()->SetTitle("Time [s]");
    sprintf(text, "Counts / %.1lf ms", QUENCH_TIMERES_SHORT*1000);
    quenchHisto->GetYaxis()->SetTitle(text);
    quenchHisto->DrawCopy();
    zeroTime = TimeToBinNumber(quenchHisto, 0.);
    sprintf(text, "t<0: %d", quenchHisto->Integral(1, zeroTime));
    AddRandomText(text,1, 0.7, 0);
    sprintf(text, "t>0: %d", quenchHisto->Integral(zeroTime+1, quenchHisto->GetNbinsX()));
    AddRandomText(text,1, 0.7, 1);
    delete quenchHisto;

    currentPad = quenchCanvas->cd(5);
    SetTimeResolution(QUENCH_TIMERES_SHORT);
    for(Int_t i=0; i<nRuns; i++)
    {
        if(i==0)
        {
            quenchHisto = GetHistoByTimes(runNumber[i], 61,  startTime[i]+QUENCH_STARTOFFSET_SHORT, startTime[i]+QUENCH_ENDOFFSET_SHORT);
            quenchHisto->SetName("quenchHisto");
        }

        else quenchHisto->Add(GetHistoByTimes(runNumber[i], 61,  startTime[i]+QUENCH_STARTOFFSET_SHORT, startTime[i]+QUENCH_ENDOFFSET_SHORT));

        quenchHisto->Add(GetHistoByTimes(runNumber[i], 48,  startTime[i]+QUENCH_STARTOFFSET_SHORT, startTime[i]+QUENCH_ENDOFFSET_SHORT));
    }
    sprintf(text, "T3 + T6");
    quenchHisto->SetTitle(text);
    quenchHisto->GetXaxis()->SetLimits(QUENCH_STARTOFFSET_SHORT, QUENCH_ENDOFFSET_SHORT);
    quenchHisto->SetLineColor(4);
    quenchHisto->GetYaxis()->SetRangeUser(0., quenchHisto->GetMaximum()*1.1);
    quenchHisto->GetXaxis()->SetTitle("Time [s]");
    sprintf(text, "Counts / %.1lf ms", QUENCH_TIMERES_SHORT*1000);
    quenchHisto->GetYaxis()->SetTitle(text);
    quenchHisto->DrawCopy();
    zeroTime = TimeToBinNumber(quenchHisto, 0.);
    sprintf(text, "t<0: %d", quenchHisto->Integral(1, zeroTime));
    AddRandomText(text,1, 0.7, 0);
    sprintf(text, "t>0: %d", quenchHisto->Integral(zeroTime+1, quenchHisto->GetNbinsX()));
    AddRandomText(text,1, 0.7, 1);
    delete quenchHisto;

    currentPad = quenchCanvas->cd(6);
    SetTimeResolution(QUENCH_TIMERES_SHORT);
    for(Int_t i=0; i<nRuns; i++)
    {
        if(i==0)
        {
            quenchHisto = GetHistoByTimes(runNumber[i], 39,  startTime[i]+QUENCH_STARTOFFSET_SHORT, startTime[i]+QUENCH_ENDOFFSET_SHORT);
            quenchHisto->SetName("quenchHisto");
        }
        else quenchHisto->Add(GetHistoByTimes(runNumber[i], 39,  startTime[i]+QUENCH_STARTOFFSET_SHORT, startTime[i]+QUENCH_ENDOFFSET_SHORT));
    }
    sprintf(text, "SAP");
    quenchHisto->SetTitle(text);
    quenchHisto->GetXaxis()->SetLimits(QUENCH_STARTOFFSET_SHORT, QUENCH_ENDOFFSET_SHORT);
    quenchHisto->SetLineColor(51);
    quenchHisto->GetYaxis()->SetRangeUser(0., quenchHisto->GetMaximum()*1.1);
    quenchHisto->GetXaxis()->SetTitle("Time [s]");
    sprintf(text, "Counts / %.1lf ms", QUENCH_TIMERES_SHORT*1000);
    quenchHisto->GetYaxis()->SetTitle(text);
    quenchHisto->DrawCopy();
    zeroTime = TimeToBinNumber(quenchHisto, 0.);
    sprintf(text, "t<0: %d", quenchHisto->Integral(1, zeroTime));
    AddRandomText(text,1, 0.7, 0);
    sprintf(text, "t>0: %d", quenchHisto->Integral(zeroTime+1, quenchHisto->GetNbinsX()));
    AddRandomText(text,1, 0.7, 1);
    delete quenchHisto;

    SplitFilename(filename, text);
    sprintf(text, "plots/%s.png", text);
    overcanvas->SaveAs(text);
}

void ShowPositronNumber(Int_t runNumber, Int_t mixDump=0)
{
    if(mixDump==0)
    {
        mixDump = MatchDumpNameToDumpNumber(runNumber, MIX_NAME);
    }

    Double_t startTime, endTime;
    GetDumpTimes(runNumber, 1, mixDump, &startTime, &endTime);

    Double_t posDump = GetLabviewAtTime(runNumber, "LFC4", 1, endTime, 1);
    sprintf(text, "e+ = %.3g", posDump);

    AddRandomText(text, 2, 0.7, 0);
}

// ==================== PLOT VERTICES ======================================================================

void PlotVertices(Int_t runNumber)
{
    PlotVertices(runNumber, MIXING_NAME);
}

void PlotVertices(Int_t runNumber, char* dumpName, Int_t dumpRep=1)
{
    Int_t dumpNumber = MatchDumpNameToDumpNumber(runNumber, dumpName, dumpRep);
    PlotVertices(runNumber, dumpNumber);

    sprintf(text, "/home/alpha/alphaSoftware2012/alphaAnalysis/macrosA1/plots/run%d_vertex_%s.png", runNumber, dumpName);
    gActiveCanvas->SaveAs(text);

}

void PlotVertices(Int_t runNumber, Int_t dump, Double_t offsetStart, Double_t length=0.)
{
    gStyle->SetOptStat(goptStat);
    sprintf(text, "Vertex Plots", runNumber);
    if(gCut->GetIsActive()>0)
    {
        TCanvas* overCanvas = new TCanvas(text,text,1200,650);
        TPad* vertexCanvas = new TPad("vertexCanvasPad", "vertexCanvasPad",0.,0.1, 1., 1.);
        TPad* listOfRuns = new TPad("listOfRunsPad", "listOfRunsPad", 0., 0, 1., 0.1);
        vertexCanvas->Draw();
        listOfRuns->Draw();
        listOfRuns->cd();
        TText* cutListText = new TText();
        cutListText->SetTextSize(0.3);
        cutListText->SetTextColor(2);
        cutListText->DrawText(0.03, 0.7, gCut->GetCutString().Data());
        gActiveCanvas=overCanvas;
    }
    else
    {
        TCanvas* vertexCanvas = new TCanvas(text,text,1200,600);
        gActiveCanvas=vertexCanvas;
    }

    Double_t startTime, endTime;
    GetDumpTimes(runNumber, 1, dump, &startTime, &endTime);
    if(length!=0.) endTime=startTime+length-offsetStart;
    TObjArray* vertexPlots = GetVertices(runNumber, startTime-offsetStart, endTime);

    //Z Projection of Vertices
    vertexCanvas->cd();
    TPad* ZPad = new TPad("Z", "Z", 0, 0., 0.25 ,0.5);
    ZPad->Draw();
    ZPad->cd();
    TH1D* zVert = (TH1D*)vertexPlots->At(Z_INDEX);
    zVert->DrawCopy();
    DrawTrapPosition(zVert);
    sprintf(text, "Integral: %d", zVert->Integral());
    AddRandomText(text, 1, 0.6);
    delete zVert;

    //Z-Phi Distribution
    vertexCanvas->cd();
    TPad* ZPhiPad = new TPad("ZPhi", "ZPhi", 0.25, 0., 0.5, 0.5);
    ZPhiPad->Draw();
    ZPhiPad->cd();
    TH2D* zPhiVert =(TH2D*) vertexPlots->At(ZPHI_INDEX);
    MakeBlueBackground(zPhiVert);
    zPhiVert->DrawCopy("COLZ");
    DrawTrapEdges(zPhiVert);
    sprintf(text, "Integral: %d", zPhiVert->Integral());
    AddRandomText(text, 1, 0.6);
    delete zPhiVert;

    //Z-t Distribution
    vertexCanvas->cd();
    TPad* ZTPad = new TPad("ZT", "ZT", 0.5, 0, 0.75, 0.5);
    ZTPad->Draw();
    ZTPad->cd();
    TH2D* zTVert =(TH2D*) vertexPlots->At(ZT_INDEX);
    MakeBlueBackground(zTVert);
    zTVert->DrawCopy("COLZ");
    DrawTrapEdges(zTVert);
    sprintf(text, "Integral: %d", zTVert->Integral());
    AddRandomText(text, 1, 0.6);
    delete zTVert;

    //Z-R Distribution
    vertexCanvas->cd();
    TPad* ZRPad = new TPad("ZR", "ZR", 0.75, 0.5, 1.0, 1.);
    ZRPad->Draw();
    ZRPad->cd();
    TH2D* zRVert =(TH2D*) vertexPlots->At(ZR_INDEX);
        zRVert->GetYaxis()->SetRangeUser(0.2, 2.2); // This is to supress bins at small r dragging the Z scale very high.
        zRVert->SetMaximum(zRVert->GetMaximum());
        zRVert->GetYaxis()->UnZoom();
    MakeBlueBackground(zRVert);
    zRVert->DrawCopy("COLZ");
    DrawTrapEdges(zRVert);
    delete zRVert;

    //X-Y Distribution
    vertexCanvas->cd();
    TPad* XYPad = new TPad("XY", "XY", 0., 0.5, 0.25, 1.);
    XYPad->Draw();
    XYPad->cd();
    TH2D* xYVert =(TH2D*) vertexPlots->At(XY_INDEX);
    MakeBlueBackground(xYVert);
    xYVert->DrawCopy("COLZ");
    sprintf(text, "Integral: %d", xYVert->Integral());
    AddRandomText(text, 1, 0.6);
    #ifdef RINGFIT
     ringfit(xYVert);
    #endif
    delete xYVert;

    //T Distribution
    vertexCanvas->cd();
    TPad* TimePad = new TPad("Time", "Time", 0.25, 0.5, 0.5, 1);
    TimePad->Draw();
    TimePad->cd();
    TH1D* tVert =(TH1D*) vertexPlots->At(T_INDEX);
    tVert->GetYaxis()->SetRangeUser(0, 1.1*tVert->GetMaximum());
    tVert->GetYaxis()->SetTitle("Rate [Hz]");

    Double_t vInt = tVert->Integral();
    tVert->Scale(1./tVert->GetXaxis()->GetBinWidth(1));


    #ifdef RO_IN_VERTS
        TH1D* roTrig = GetHistoByTimes(runNumber, 45, startTime-offsetStart, endTime);
        roTrig = RebinHistoToNBins(roTrig);
        roTrig->Draw();
        roTrig->GetYaxis()->SetRangeUser(0., roTrig->GetMaximum());
        Double_t rInt = roTrig->Integral();
        roTrig->Scale(1./roTrig->GetXaxis()->GetBinWidth(1));
        roTrig->SetLineColor(2);
        roTrig->DrawCopy();
        tVert->DrawCopy("SAME");
        sprintf(text, "Readouts: %d", rInt);
        AddRandomText(text, 2, 0.6, 1);
        delete roTrig;
    #else
        tVert->DrawCopy();
    #endif

    sprintf(text, "Vertices: %d", vInt);
    AddRandomText(text, 1, 0.6);
    delete tVert;

    //RDens Distribution
    vertexCanvas->cd();
    TPad* RDensPad = new TPad("RDens", "RDens", 0.5, 0.5, 0.75, 1);
    RDensPad->Draw();
    RDensPad->cd();
    TH1D* rDensVert =(TH1D*) vertexPlots->At(RDENS_INDEX);
    rDensVert->DrawCopy();
    Double_t par[4];
    Double_t perr[4];
    Int_t ierr = radfit(rDensVert, par,  perr);
    rDensVert->DrawCopy();
    if (ierr ==0) {
    sprintf(text,"Entries  %i \n", rDensVert->GetEntries());
    AddRandomText(text, 1, 0.5,2);
    sprintf(text,"Central  %6.0f+-%5.0f \n", par[0], perr[0]);
    AddRandomText(text, 1, 0.5,3);
    sprintf(text,"Wall   %6.0f+-%5.0f \n",par[1],perr[1]);
    AddRandomText(text, 1, 0.5,4);
    sprintf(text,"Const  %6.0f+-%5.0f \n", par[2], perr[2]);
    AddRandomText(text, 1, 0.5,5);
    } else {
    sprintf(text,"Fit did not Converge \n");
    AddRandomText(text, 1, 0.5,3);
    }
    delete rDensVert;

    //Phi Distribution
    vertexCanvas->cd();
    TPad* PhiPad = new TPad("Phi", "Phi", 0.75, 0., 1., 0.5);
    PhiPad->Draw();
    PhiPad->cd();
    TH1D* phiVert =(TH1D*) vertexPlots->At(PHI_INDEX);
    phiVert->SetFillColor(1);
    phiVert->DrawCopy("hbar");
    sprintf(text, "Integral: %.0lf", phiVert->Integral());
    AddRandomText(text, 1, 0.6);
    delete phiVert;


    delete vertexPlots;
}

void PlotVertices(Int_t runNumber, char* dumpName, Double_t offsetStart, Double_t length, Int_t dumpRep=1)
{
    Int_t dumpNumber = MatchDumpNameToDumpNumber(runNumber, dumpName, dumpRep);
    PlotVertices(runNumber, dumpNumber, offsetStart, length);
}

void PlotVertices(Int_t runNumber, Int_t dump)
{
    PlotVertices(runNumber, dump, 0., 0.);
}

void PlotVertices(Int_t* runNumber, Int_t* dumps, Int_t nRuns)
{
    gStyle->SetOptStat(goptStat);
    sprintf(text, "Vertex Plots");
    TCanvas* overCanvas = new TCanvas(text, text, 1200, 650);
    TPad* listOfRuns = new TPad("listOfRunsPad", "listOfRunsPad", 0., 0, 1., 0.1);
    TPad* vertexCanvas = new TPad("vertexCanvasPad", "vertexCanvasPad",0.,0.1, 1., 1.);
    vertexCanvas->Draw();
    listOfRuns->Draw();
    listOfRuns->cd();
    TString runList;

    if(nRuns<15)
    {
        for(Int_t i=0; i<nRuns; i++)
        {
            runList+=runNumber[i];
            runList+=", ";
        }
    }
    else
    {
        for(Int_t i=0; i<14; i++)
        {
            runList+=runNumber[i];
            runList+=", ";
        }
            runList+="... (";
            runList+=nRuns;
            runList+=" runs)";
    }

    if(gVerticesFile!=NULL)
    {
        TText* runFileText = new TText();
        runFileText->SetTextSize(0.25);
        runFileText->SetTextColor(4);
        runFileText->DrawText(0.03, 0.75, gVerticesFile->Data());
        gVerticesFile=NULL;
    }

    TText* runListText = new TText();
    runListText->SetTextSize(0.25);
    runListText->DrawText(0.03, 0.5, runList.Data());
    TText* cutListText = new TText();
    cutListText->SetTextSize(0.25);
    cutListText->SetTextColor(2);
    cutListText->DrawText(0.03, 0.1, gCut->GetCutString().Data());

    TObjArray* thisVertices;

    for(Int_t i=0; i<nRuns; i++)
    {
        //Info("DEBUG", "i: %d nRuns: %d", i, nRuns);
        //Info("DEBUG", "running run %d dump %d", runNumber[i], dumps[i]);
        if(dumps[i]==0) dumps[i]=MatchDumpNameToDumpNumber(runNumber[i], MIXING_NAME);

        thisVertices=GetVertices(runNumber[i], dumps[i]);

        if(i==0)
        {
            TH1D* zVertices = (TH1D*)thisVertices->At(Z_INDEX)->Clone();
            TH2D* zPhiVertices = (TH2D*)thisVertices->At(ZPHI_INDEX)->Clone();
            TH2D* zTVertices = (TH2D*)thisVertices->At(ZT_INDEX)->Clone();
            TH2D* xYVertices = (TH2D*)thisVertices->At(XY_INDEX)->Clone();
            TH1D* tVertices = (TH1D*)thisVertices->At(T_INDEX)->Clone();
            TH1D* rDensVertices = (TH1D*)thisVertices->At(RDENS_INDEX)->Clone();
            TH2D* zRVertices =(TH2D*)thisVertices->At(ZR_INDEX)->Clone();
            TH1D* phiVertices=(TH1D*)thisVertices->At(PHI_INDEX)->Clone();
        }
        else
        {
            zVertices->Add((TH1D*)thisVertices->At(Z_INDEX));
            zPhiVertices->Add((TH2D*)thisVertices->At(ZPHI_INDEX));
            zTVertices->Add((TH2D*)thisVertices->At(ZT_INDEX));
            xYVertices->Add((TH2D*)thisVertices->At(XY_INDEX));
            tVertices->Add((TH1D*)thisVertices->At(T_INDEX));
            rDensVertices->Add((TH1D*)thisVertices->At(RDENS_INDEX));
            zRVertices->Add((TH2D*)thisVertices->At(ZR_INDEX));
            phiVertices->Add((TH1D*)thisVertices->At(PHI_INDEX));
        }
        thisVertices->Delete();

        //Info("DEBUG", "finished run %d dump %d", runNumber[i], dumps[i]);
    }



    vertexCanvas->cd();
    //Z Projection of Vertices
    TPad* ZPad = new TPad("Z", "Z", 0, 0., 0.25 ,0.5);
    ZPad->Draw();
    ZPad->cd();
    zVertices->SetTitle("Z-distribution");
    zVertices->DrawCopy("");
    DrawTrapPosition(zVertices);
    sprintf(text, "Integral: %d", zVertices->Integral());
    AddRandomText(text, 1, 0.6);
    delete zVertices;


    //Z-Phi Distribution
    vertexCanvas->cd();
    TPad* ZPhiPad = new TPad("ZPhi", "ZPhi", 0.25, 0., 0.5, 0.5);
    ZPhiPad->Draw();
    ZPhiPad->cd();
    zPhiVertices->SetTitle("Z-#phi distribution");
    MakeBlueBackground(zPhiVertices);
    zPhiVertices->DrawCopy("COLZ");
    DrawTrapEdges(zPhiVertices);
    sprintf(text, "Integral: %d", zPhiVertices->Integral());
    AddRandomText(text, 1, 0.6);
    delete zPhiVertices;

    //Z-t Distribution
    vertexCanvas->cd();
    TPad* ZTPad = new TPad("ZT", "ZT", 0.5, 0, 0.75, 0.5);
    ZTPad->Draw();
    ZTPad->cd();
    zTVertices->SetTitle("Z-T distribution");
    MakeBlueBackground(zTVertices);
    zTVertices->DrawCopy("COLZ");
    DrawTrapEdges(zTVertices);
    sprintf(text, "Integral: %d", zTVertices->Integral());
    AddRandomText(text, 1, 0.6);
    delete zTVertices;

    //Z-R Distribution
    vertexCanvas->cd();
    TPad* ZRPad = new TPad("ZR", "ZR", 0.75, 0.5, 1.0, 1.);
    ZRPad->Draw();
    ZRPad->cd();
    zRVertices->SetTitle("Z-R Density distribution");
    MakeBlueBackground(zRVertices);
        zRVertices->GetYaxis()->SetRangeUser(0.2, 2.2); // This is to supress bins at small r dragging the Z scale very high.
        zRVertices->SetMaximum(zRVertices->GetMaximum());
        zRVertices->GetYaxis()->UnZoom();
    zRVertices->DrawCopy("COLZ");
    DrawTrapEdges(zRVertices);
    delete zRVertices;

    //X-Y Distribution
    vertexCanvas->cd();
    TPad* XYPad = new TPad("XY", "XY", 0., 0.5, 0.25, 1.);
    XYPad->Draw();
    XYPad->cd();
    xYVertices->SetTitle("XY distribution");
    MakeBlueBackground(xYVertices);
    xYVertices->DrawCopy("COLZ");
    sprintf(text, "Integral: %d", xYVertices->Integral());
    AddRandomText(text, 1, 0.6);
    #ifdef RINGFIT
        ringfit(xYVertices);
    #endif
    delete xYVertices;

    //T Distribution
    vertexCanvas->cd();
    TPad* TimePad = new TPad("Time", "Time", 0.25, 0.5, 0.5, 1);
    TimePad->Draw();
    TimePad->cd();
    tVertices->SetTitle("T-distribution");
    tVertices->GetYaxis()->SetRangeUser(0., tVertices->GetMaximum());
    //#ifdef RO_IN_VERTS
        TH1D* roTrig2;
        for(Int_t i=0; i<nRuns; i++)
        {
            cout << "boo" << endl; // EB: This is magic. I don't know why...

            if(i==0)
            {
                TH1D* roTrig = GetDumpHisto(runNumber[i], 0, dumps[i], 45);
                roTrig->SetName("roTrig");
                roTrig->SetTitle("Time distribution");
            }
            else
            {
                roTrig2 = GetDumpHisto(runNumber[i], 0, dumps[i], 45);
                roTrig->Add(roTrig2);
            }
        }
        roTrig = RebinHistoToNBins(roTrig);
        roTrig->GetYaxis()->SetRange(0, roTrig->GetNbinsY());
        roTrig->SetLineColor(2);
        roTrig->DrawCopy("");
        tVertices->DrawCopy("SAME");

        sprintf(text, "Readouts: %d", roTrig->Integral());
        AddRandomText(text, 2, 0.6, 1);
        delete roTrig;
    #else
        tVertices->DrawCopy();
    #endif

    sprintf(text, "Vertices: %d", tVertices->Integral());
    AddRandomText(text, 1, 0.6);
    delete tVertices;

    //RDens Distribution
    vertexCanvas->cd();
    TPad* RDensPad = new TPad("RDens", "RDens", 0.5, 0.5, 0.75, 1);
    RDensPad->Draw();
    RDensPad->cd();
    rDensVertices->SetTitle("R Density distribution");
    Double_t par[4];
    Double_t perr[4];
    Int_t ierr = radfit(rDensVertices, par,  perr);
    rDensVertices->DrawCopy();
        if (ierr ==0) {
    sprintf(text,"Entries  %i \n", rDensVertices->GetEntries());
    AddRandomText(text, 1, 0.5,2);
    sprintf(text,"Central  %6.0f+-%5.0f \n", par[0], perr[0]);
    AddRandomText(text, 1, 0.5,3);
    sprintf(text,"Wall   %6.0f+-%5.0f \n",par[1],perr[1]);
    AddRandomText(text, 1, 0.5,4);
    sprintf(text,"Const  %6.0f+-%5.0f \n", par[2], perr[2]);
    AddRandomText(text, 1, 0.5,5);
    } else {
    sprintf(text,"Fit did not Converge \n");
    AddRandomText(text, 1, 0.5,3);
}

    delete rDensVertices;

    //Phi Distribution
    vertexCanvas->cd();
    TPad* PhiPad = new TPad("Phi", "Phi", 0.75, 0., 1., 0.5);
    PhiPad->Draw();
    PhiPad->cd();
    phiVertices->SetFillColor(1);
    phiVertices->DrawCopy("hbar");
    sprintf(text, "Integral: %.0lf", phiVertices->Integral());
    AddRandomText(text, 1, 0.6);
    delete phiVertices;

}

void PlotVertices(char* filename)
{
    /*
     * The file needs to be formatted in two columns - a run number and an optional dump number
     * if the dump number is 0 or not specified, will try to automatically find the mixing dump for that sequence
     */

    TString* Tfilename = new TString(filename);
    if(Tfilename->EndsWith(".root"))
    {
        PlotSavedVertices(filename);
        return;
    }

     Int_t runNumber[1000], dumps[1000];
     Int_t nRuns = FileToRuns(filename, runNumber, dumps);

    gVerticesFile = Tfilename;
    PlotVertices(runNumber, dumps, nRuns);
    AddRandomTextWidth(filename, 4, 0.5, 0.5,  0);

}


void PlotVertices(char* filename, Int_t dumpNumber)
{
     Int_t runNumber[1000], dumps[1000];
     Int_t nRuns = FileToRuns(filename, runNumber, dumps);
     for (int i=0;i<nRuns;i++){
      dumps[i] =  dumpNumber;
    }
    gVerticesFile = new TString(filename);
    PlotVertices(runNumber, dumps, nRuns);
    AddRandomTextWidth(filename, 4, 0.5, 0.5,  0);
}



void PlotVertices(char* filename, char* dumpName, Int_t dumpRep=1)
{
     Int_t runNumber[1000], dumps[1000];
     Int_t nRuns = FileToRuns(filename, runNumber);
     for (int i=0;i<nRuns;i++){
      dumps[i] =  MatchDumpNameToDumpNumber(runNumber[i], dumpName, dumpRep);
    }
    gVerticesFile = new TString(filename);
    PlotVertices(runNumber, dumps, nRuns);
    AddRandomTextWidth(filename, 4, 0.5, 0.5,  0);
}

void PlotRunVertices(Int_t runNumber)
{
    PlotVertices(runNumber, -1); // dump number of -1 indicates to take the whole run
}

void PlotRunVertices(char* filename)
{
    PlotVertices(fileName, "WHOLE_RUN");
}

void SaveVertices(char* filename)
{
    SaveVertices(filename, MIXING_NAME);
}

void SaveVertices(char* filename, char* dumpname, Double_t startOffset=0., Double_t length=0., Int_t dumpRep=1)
{
    /*
        This saves the vertices in a tree so that they can be u ed alter without the overhead of re-extracting them from the trees

    */
    Int_t runNumber[1000], dumps[1000];
    Int_t nRuns = FileToRuns(filename, runNumber, dumps);
    Int_t nVertices, nVerticesTotal(0);
    Double_t maxTime(0.), startTime, endTime;

    sprintf(text, "%s_vertices_%s.root", filename, dumpname);
    TFile* outfile = new TFile(text, "RECREATE");
    TTree* vertices = new TTree("vertices", filename);
    TSiliconEvent* siliconEvent = new TSiliconEvent();
    vertices->Branch("vertices", &siliconEvent);

    for (Int_t i=0; i<nRuns; i++)
    {
        siliconEvent->ClearEvent();
        if(dumps[i]==0) dumps[i]=MatchDumpNameToDumpNumber(runNumber[i], dumpname);
        GetDumpTimes(runNumber[i], 1, dumps[i], &startTime, &endTime);
        startTime+=startOffset;
        if(length!=0.) endTime = startTime+length;
        nVertices=GetVerticesRaw(runNumber[i], startTime, endTime, vertices, &maxTime);
        Info("SaveVertices", "Done with run %d, saved %d vertices", runNumber[i], nVertices);
        nVerticesTotal+=nVertices;
        outfile->cd();
        vertices->Write();
        Close_File(runNumber[i]);
    }
    Info("SaveVertices", "Done with all %d runs, saved %d vertices", nRuns, nVerticesTotal);

    // Need to save the maximum time as a TObject so we can make the time axes later.
    outfile->cd();
    TVector2* maxContainer = new TVector2(maxTime, maxTime);
    maxContainer->Write();

    outfile->Write();
    outfile->Close();
}

// ==================== PLOTDUMPS with special names ======================================================================

void PlotLeftDump(Int_t runNumber, Int_t chainNumber, Int_t dumpNumber, Int_t detector, Double_t transformerValue = 0., Int_t rep=1)
{
    gStyle->SetOptStat(goptStat);
    TH1D* leftHistogram = GetDumpHisto(runNumber, chainNumber, dumpNumber, detector);

    if(TRANSFORMER_NORMALISED>0. && transformerValue>0.)
    {
        leftHistogram->Scale(TRANSFORMER_NORMALISED/transformerValue);
    }
    Int_t leftIntegral = leftHistogram->Integral();
    leftHistogram=RebinHistoToNBins(leftHistogram);

    leftHistogram->SetLineColor(rep+(rep>4));
    sprintf(text, "%s", drawOpts);
    leftHistogram->Draw(text);

    sprintf(text,"Integral: %d", leftIntegral);
    AddRandomText(text, rep, 1., rep);
    sprintf(text, "Left Dump [%d]",dumpNumber);
    leftHistogram->SetTitle(text);
}

void PlotRightDump(Int_t runNumber, Int_t chainNumber, Int_t dumpNumber, Int_t detector, Double_t transformerValue = 0., Int_t rep=1)
{
    gStyle->SetOptStat(goptStat);
    TH1D* rightHistogram = GetDumpHisto(runNumber, chainNumber, dumpNumber, detector);

    if(TRANSFORMER_NORMALISED>0. && transformerValue>0.) rightHistogram->Scale(TRANSFORMER_NORMALISED/transformerValue);
    Int_t rightIntegral = rightHistogram->Integral();
    rightHistogram = RebinHistoToNBins(rightHistogram);

    rightHistogram->SetLineColor(rep+(rep>4));
    sprintf(text, "%s", drawOpts);
    rightHistogram->Draw(text);

    sprintf(text,"Integral: %d", rightIntegral);
    AddRandomText(text, rep, 1., rep);
    sprintf(text, "Right Dump [%d]",dumpNumber);
    rightHistogram->SetTitle(text);
}

void PlotMixingDump(Int_t runNumber, Int_t chainNumber, Int_t mixingDump, Int_t detector=SIS2_TTC_SI_TRIG, Double_t transformerValue=0., Int_t rep=1)
{
    gStyle->SetOptStat(goptStat);
    TH1D* mixingHistogram = GetDumpHisto(runNumber, chainNumber, mixingDump, detector);
    if(TRANSFORMER_NORMALISED>0. && transformerValue>0.) mixingHistogram->Scale(TRANSFORMER_NORMALISED/transformerValue);

    Int_t mixingIntegral = mixingHistogram->Integral();
    ScaleAndRebinHistogram(mixingHistogram);

    #ifdef MIXING_LOG
        mixingHistogram->GetYaxis()->SetRangeUser(0.5*NUMBER_BINS/mixingHistogram->GetXaxis()->GetXmax(), 2*mixingHistogram->GetMaximum());
    #endif

    mixingHistogram->SetLineColor(rep+(rep>4));
    sprintf(text, "Mixing, Run %d", runNumber);
    mixingHistogram->SetTitle(text);
    if(rep>1)sprintf(text, "%sSAME", drawOpts);
    else sprintf(text, "%s", drawOpts);
    mixingHistogram->DrawCopy(text);
    sprintf(text, "TRAFO %.2lf normalised to %.2lf\n", transformerValue, TRANSFORMER_NORMALISED);
    if(TRANSFORMER_NORMALISED>0.) AddRandomText(text, 4, 0.5, 0);

    if (detector==51) sprintf(text,"Si>1, Integral: %d", mixingIntegral);
    else if(detector==45) sprintf(text,"RO, Integral: %d", mixingIntegral);
    else if(detector==39) sprintf(text,"SAP, Integral: %d", mixingIntegral);
    else if(detector==56) sprintf(text,"T3 OR T6, Integral: %d", mixingIntegral);
    else sprintf(text,"Chan %d, Integral: %d", detector, mixingIntegral);
    AddRandomText(text, rep, 0.7, rep);
}

// ==================== PLOT __ VERTICES ======================================================================

void PlotZVertices(Int_t runNumber)
{
    Int_t mixingDump = MatchDumpNameToDumpNumber(runNumber, MIXING_NAME);
    PlotZVertices(runNumber, mixingDump);
}

void PlotZVertices(Int_t runNumber, Int_t dumpNumber, Int_t rep=1)
{
    gStyle->SetOptStat(goptStat);
    TH1D* zVertices = GetZVertices(runNumber, dumpNumber);
    zVertices->SetLineColor(rep+(rep>4));
    if(rep==1)
    {
        zVertices->DrawCopy();
        DrawTrapPosition(zVertices);
    }
    else
        zVertices->DrawCopy("SAME");
    sprintf(text, "Integral: %d", zVertices->Integral());
    AddRandomText(text, 1, 0.6);
    delete zVertices;
}

 void PlotZVertices(Int_t* runNumber, Int_t* dumps, Int_t nRuns, Int_t rep=1)
{
    gStyle->SetOptStat(goptStat);
    TH1D *zVertices, *thisVertices;


    for(Int_t i=0; i<nRuns; i++)
    {
        if(dumps[i]==0) dumps[i]=MatchDumpNameToDumpNumber(runNumber[i], MIXING_NAME);

        if(i==0)
            zVertices = GetZVertices(runNumber[i], dumps[i]);
        else
        {
            thisVertices=GetZVertices(runNumber[i], dumps[i]);
            zVertices->Add(thisVertices);
            thisVertices->Delete();
        }
    }



    if(gNormaliseOverlay)zVertices->Scale(1./zVertices->Integral());



    zVertices->SetTitle("Z-distribution");
    zVertices->SetLineColor(rep+(rep>4));
    if(rep==1)
    {
        zVertices->DrawCopy();
        DrawTrapPosition(zVertices);
    }
    else
        zVertices->DrawCopy("SAME");
    sprintf(text, "Integral: %d", zVertices->Integral());
    AddRandomText(text, 1, 0.6);
    delete zVertices;
}

void PlotZVertices(char* filename, Int_t rep=1)
{
    /*
     * The file needs to be formatted in two columns - a run number and an optional dump number
     * if the dump number is 0 or not specified, will try to automatically find the mixing dump for that sequence
     */

     Int_t runNumber[1000], dumps[1000];
     Int_t nRuns = FileToRuns(filename, runNumber, dumps);

    PlotZVertices(runNumber, dumps, nRuns, rep);
}

void PlotZVerticesOverlay(char* filename)
{
    gStyle->SetOptStat(goptStat);
    sprintf(text, "Vertex Plots: %s", filename);
    TCanvas* overCanvas = new TCanvas(text, text, 600, 480);
    TPad* legendPad = new TPad("legend", "legend", 0.8, 0., 1., 1.);
    legendPad->Draw();
    legendPad->cd();

    //Need to put legend text here
    TObjArray* filenames = new TObjArray();
    TObjArray* legendText = new TObjArray();

    Int_t nFiles = FileToSubFiles(filename, filenames, legendText);
    Int_t nRuns, runs[1000], dumps[1000];
    TText* runListText = new TText();

    gNormaliseOverlay = kTRUE;

    for(Int_t i=0; i<nFiles; i++)
    {
        nRuns = FileToRuns(((TString*)filenames->At(i))->Data(), runs, dumps);
        sprintf(text, "%s [%d]", ((TString*)legendText->At(i))->Data(), nRuns);

        runListText->SetTextSize(0.1);
        runListText->SetTextColor(i+1+(i>3));

        runListText->DrawText(0.1, 0.95-0.03*i, text);
    }

    overCanvas->cd();
    TPad* c= new TPad(text, text, 0., 0., 0.8, 1.0);
    c->Draw();
    c->cd();

    // Mixing Histogram

    TPad* mixingPad = new TPad("Mixing", "Mixing", 0., 0., 1., 1.);
    mixingPad->Draw();
    mixingPad->cd();
    TH1D* zVertices;

    for(Int_t i=0; i<nFiles; i++)
    {
        PlotZVertices(((TString*)filenames->At(i))->Data(), i+1);
    }

}

void PlotZPhiVertices(Int_t runNumber)
{
    Int_t mixingDump = MatchDumpNameToDumpNumber(runNumber, MIXING_NAME);
    PlotZPhiVertices(runNumber, mixingDump);
}

void PlotZPhiVertices(Int_t runNumber, Int_t dumpNumber)
{
    gStyle->SetOptStat(goptStat);
    TH2D* zPhiVertices = GetZPhiVertices(runNumber, dumpNumber);
    MakeBlueBackground(zPhiVertices);
    zPhiVertices->DrawCopy("COLZ");
    sprintf(text, "Integral: %d", zPhiVertices->Integral());
    AddRandomText(text, 1, 0.6);
    delete zPhiVertices;
}

void PlotZPhiVertices(Int_t* runNumber, Int_t* dumps, Int_t nRuns)
{
    gStyle->SetOptStat(goptStat);
    TH2D *zPhiVertices, *thisVertices;

    for(Int_t i=0; i<nRuns; i++)
    {
        if(dumps[i]==0) dumps[i]=MatchDumpNameToDumpNumber(runNumber[i], MIXING_NAME);

        if(i==0)
            zPhiVertices = GetZPhiVertices(runNumber[i], dumps[i]);
        else
        {
            thisVertices=GetZPhiVertices(runNumber[i], dumps[i]);
            zPhiVertices->Add(thisVertices);
            thisVertices->Delete();
        }
    }

    zPhiVertices->SetTitle("Z-#phi distribution");
    MakeBlueBackground(zPhiVertices);
    zPhiVertices->DrawCopy("COLZ");
    sprintf(text, "Integral: %d", zPhiVertices->Integral());
    AddRandomText(text, 1, 0.6);
    delete zPhiVertices;
}

void PlotZPhiVertices(char* filename)
{
    /*
     * The file needs to be formatted in two columns - a run number and an optional dump number
     * if the dump number is 0 or not specified, will try to automatically find the mixing dump for that sequence
     */

     Int_t runNumber[1000], dumps[1000];
     Int_t nRuns = FileToRuns(filename, runNumber, dumps);

    PlotZPhiVertices(runNumber, dumps, nRuns);
}

void PlotZRVertices(Int_t runNumber)
{
    Int_t mixingDump = MatchDumpNameToDumpNumber(runNumber, MIXING_NAME);
    PlotZRVertices(runNumber, mixingDump);
}

void PlotZRVertices(Int_t runNumber, Int_t dumpNumber)
{
    gStyle->SetOptStat(goptStat);
    TH2D* zRVertices = GetZRVertices(runNumber, dumpNumber);
    MakeBlueBackground(zRVertices);
    zRVertices->DrawCopy("COLZ");
    sprintf(text, "Integral: %d", zRVertices->Integral());
    AddRandomText(text, 1, 0.6);
    delete zRVertices;
}

void PlotZRVertices(Int_t* runNumber, Int_t* dumps, Int_t nRuns)
{
    gStyle->SetOptStat(goptStat);
    TH2D *zRVertices, *thisVertices;

    for(Int_t i=0; i<nRuns; i++)
    {
        if(dumps[i]==0) dumps[i]=MatchDumpNameToDumpNumber(runNumber[i], MIXING_NAME);

        if(i==0)
            zRVertices = GetZRVertices(runNumber[i], dumps[i]);
        else
        {
            thisVertices=GetZRVertices(runNumber[i], dumps[i]);
            zRVertices->Add(thisVertices);
            thisVertices->Delete();
        }
    }

    zRVertices->SetTitle("Z-R distribution");
    MakeBlueBackground(zRVertices);
    zRVertices->DrawCopy("COLZ");
    sprintf(text, "Integral: %d", zRVertices->Integral());
    AddRandomText(text, 1, 0.6);
    delete zRVertices;
}

void PlotZRVertices(char* filename)
{
    /*
     * The file needs to be formatted in two columns - a run number and an optional dump number
     * if the dump number is 0 or not specified, will try to automatically find the mixing dump for that sequence
     */

     Int_t runNumber[1000], dumps[1000];
     Int_t nRuns = FileToRuns(filename, runNumber, dumps);

    PlotZRVertices(runNumber, dumps, nRuns);
}

void PlotZTVertices(Int_t runNumber)
{
    Int_t mixingDump = MatchDumpNameToDumpNumber(runNumber, MIXING_NAME);
    PlotZTVertices(runNumber, mixingDump);
}

void PlotZTVertices(Int_t runNumber, Int_t dumpNumber)
{
    gStyle->SetOptStat(goptStat);
    TH2D* zTVertices = GetZTVertices(runNumber, dumpNumber);
    MakeBlueBackground(zTVertices);
    zTVertices->DrawCopy("COLZ");
    sprintf(text, "Integral: %d", zTVertices->Integral());
    AddRandomText(text, 1, 0.6);
    delete zTVertices;

}

void PlotZTVertices(Int_t* runNumber, Int_t* dumps, Int_t nRuns)
{
    gStyle->SetOptStat(goptStat);
    TH2D *zTVertices, *thisVertices;

    for(Int_t i=0; i<nRuns; i++)
    {
        if(dumps[i]==0) dumps[i]=MatchDumpNameToDumpNumber(runNumber[i], MIXING_NAME);

        if(i==0)
            zTVertices = GetZTVertices(runNumber[i], dumps[i]);
        else
        {
            thisVertices=GetZTVertices(runNumber[i], dumps[i]);
            zTVertices->Add(thisVertices);
            thisVertices->Delete();
        }
    }

    zTVertices->SetTitle("Z-T distribution");
    MakeBlueBackground(zTVertices);
    zTVertices->DrawCopy("COLZ");
    sprintf(text, "Integral: %d", zTVertices->Integral());
    AddRandomText(text, 1, 0.6);
    delete zTVertices;
}

void PlotZTVertices(char* filename)
{
    /*
     * The file needs to be formatted in two columns - a run number and an optional dump number
     * if the dump number is 0 or not specified, will try to automatically find the mixing dump for that sequence
     */

     Int_t runNumber[1000], dumps[1000];
     Int_t nRuns = FileToRuns(filename, runNumber, dumps);

    PlotZTVertices(runNumber, dumps, nRuns);
}

void PlotXYVertices(Int_t runNumber)
{
    Int_t mixingDump = MatchDumpNameToDumpNumber(runNumber, MIXING_NAME);
    PlotXYVertices(runNumber, mixingDump);
}

void PlotXYVertices(Int_t runNumber, Int_t dumpNumber)
{
    gStyle->SetOptStat(goptStat);
    TH2D* xYVertices = GetXYVertices(runNumber, dumpNumber);
    MakeBlueBackground(xYVertices);
    xYVertices->DrawCopy("COLZ");
    sprintf(text, "Integral: %d", xYVertices->Integral());
    AddRandomText(text, 1, 0.6);
    #ifdef RINGFIT
     ringfit(xYVertices);
    #endif
    delete xYVertices;
}

void PlotXYVertices(Int_t* runNumber, Int_t* dumps, Int_t nRuns)
{
    TH2D *XYVertices, *thisVertices;
    gStyle->SetOptStat(goptStat);
    for(Int_t i=0; i<nRuns; i++)
    {
        if(dumps[i]==0) dumps[i]=MatchDumpNameToDumpNumber(runNumber[i], MIXING_NAME);

        if(i==0)
            XYVertices = GetXYVertices(runNumber[i], dumps[i]);
        else
        {
            thisVertices=GetXYVertices(runNumber[i], dumps[i]);
            XYVertices->Add(thisVertices);
            thisVertices->Delete();
        }
    }

    XYVertices->SetTitle("XY distribution");
    MakeBlueBackground(XYVertices);
    XYVertices->DrawCopy("COLZ");
    sprintf(text, "Integral: %d", XYVertices->Integral());
    AddRandomText(text, 1, 0.6);
    #ifdef RINGFIT
        ringfit(XYVertices);
    #endif
    delete XYVertices;
}

void PlotXYVertices(char* filename)
{
    /*
     * The file needs to be formatted in two columns - a run number and an optional dump number
     * if the dump number is 0 or not specified, will try to automatically find the mixing dump for that sequence
     */

     Int_t runNumber[1000], dumps[1000];
     Int_t nRuns = FileToRuns(filename, runNumber, dumps);

    PlotXYVertices(runNumber, dumps, nRuns);
}

void PlotTimeVertices(Int_t runNumber)
{
    Int_t mixingDump = MatchDumpNameToDumpNumber(runNumber, MIXING_NAME);
    PlotTimeVertices(runNumber, mixingDump);
}

void PlotTimeVertices(Int_t runNumber, Int_t dumpNumber)
{
    gStyle->SetOptStat(goptStat);
    TH1D* timeVertices = GetTimeVertices(runNumber, dumpNumber);
    timeVertices->DrawCopy();
    sprintf(text, "Integral: %d", timeVertices->Integral());
    AddRandomText(text, 1, 0.6);
    delete timeVertices;
}

void PlotTimeVertices(Int_t* runNumber, Int_t* dumps, Int_t nRuns)
{
    TH1D *timeVertices, *thisVertices;
    gStyle->SetOptStat(goptStat);
    for(Int_t i=0; i<nRuns; i++)
    {
        if(dumps[i]==0) dumps[i]=MatchDumpNameToDumpNumber(runNumber[i], MIXING_NAME);

        if(i==0)
            timeVertices = GetTimeVertices(runNumber[i], dumps[i]);
        else
        {
            thisVertices=GetTimeVertices(runNumber[i], dumps[i]);
            timeVertices->Add(thisVertices);
            thisVertices->Delete();
        }
    }

    timeVertices->SetTitle("T-distribution");
    timeVertices->DrawCopy();
    sprintf(text, "Integral: %d", timeVertices->Integral());
    AddRandomText(text, 1, 0.6);
    delete timeVertices;
}

void PlotTimeVertices(char* filename)
{
    /*
     * The file needs to be formatted in two columns - a run number and an optional dump number
     * if the dump number is 0 or not specified, will try to automatically find the mixing dump for that sequence
     */

     Int_t runNumber[1000], dumps[1000];
     Int_t nRuns = FileToRuns(filename, runNumber, dumps);

    PlotTimeVertices(runNumber, dumps, nRuns);
}

void PlotRVertices(Int_t runNumber)
{
    Int_t mixingDump = MatchDumpNameToDumpNumber(runNumber, MIXING_NAME);
    PlotRVertices(runNumber, mixingDump);
}

void PlotRVertices(Int_t runNumber, Int_t dumpNumber)
{
    gStyle->SetOptStat(goptStat);
    TH1D* rVertices = GetRVertices(runNumber, dumpNumber);
    rVertices->DrawCopy();
    sprintf(text, "Integral: %d", rVertices->Integral());
    AddRandomText(text, 1, 0.6);
    delete rVertices;
}

void PlotRVertices(Int_t* runNumber, Int_t* dumps, Int_t nRuns)
{

    TH1D *rVertices, *thisVertices;
    gStyle->SetOptStat(goptStat);
    for(Int_t i=0; i<nRuns; i++)
    {
        if(dumps[i]==0) dumps[i]=MatchDumpNameToDumpNumber(runNumber[i], MIXING_NAME);

        if(i==0)
            rVertices = GetRVertices(runNumber[i], dumps[i]);
        else
        {
            thisVertices=GetRVertices(runNumber[i], dumps[i]);
            rVertices->Add(thisVertices);
            thisVertices->Delete();
        }
    }

    rVertices->SetTitle("R-distribution");
    rVertices->DrawCopy();
    sprintf(text, "Integral: %d", rVertices->Integral());
    AddRandomText(text, 1, 0.6);
    delete rVertices;
}

void PlotRVertices(char* filename)
{
    /*
     * The file needs to be formatted in two columns - a run number and an optional dump number
     * if the dump number is 0 or not specified, will try to automatically find the mixing dump for that sequence
     */

     Int_t runNumber[1000], dumps[1000];
     Int_t nRuns = FileToRuns(filename, runNumber, dumps);

    PlotRVertices(runNumber, dumps, nRuns);
}

void PlotRDensVertices(Int_t runNumber)
{
    Int_t mixingDump = MatchDumpNameToDumpNumber(runNumber, MIXING_NAME);
    PlotRDensVertices(runNumber, mixingDump);
}

void PlotRDensVertices(Int_t runNumber, Int_t dumpNumber)
{
    gStyle->SetOptStat(goptStat);
    TH1D* rVertices = GetRDensVertices(runNumber, dumpNumber);
    rVertices->DrawCopy();
    sprintf(text, "Entries: %d", rVertices->GetEntries());
    AddRandomText(text, 1, 0.6);
    delete rVertices;
}

void PlotRDensVertices(Int_t* runNumber, Int_t* dumps, Int_t nRuns)
{

    TH1D *rVertices, *thisVertices;
    gStyle->SetOptStat(goptStat);
    for(Int_t i=0; i<nRuns; i++)
    {
        if(dumps[i]==0) dumps[i]=MatchDumpNameToDumpNumber(runNumber[i], MIXING_NAME);

        if(i==0)
            rVertices = GetRDensVertices(runNumber[i], dumps[i]);
        else
        {
            thisVertices=GetRDensVertices(runNumber[i], dumps[i]);
            rVertices->Add(thisVertices);
            thisVertices->Delete();
        }
    }

    rVertices->SetTitle("R Density distribution");
    rVertices->DrawCopy();
    delete rVertices;
}

void PlotRDensVertices(char* filename)
{
    /*
     * The file needs to be formatted in two columns - a run number and an optional dump number
     * if the dump number is 0 or not specified, will try to automatically find the mixing dump for that sequence
     */

     Int_t runNumber[1000], dumps[1000];
     Int_t nRuns = FileToRuns(filename, runNumber, dumps);

    PlotRDensVertices(runNumber, dumps, nRuns);
}

// PlotSavedVertices

void PlotSavedVertices(char* rootfilename)
{
    gStyle->SetOptStat(goptStat);
    sprintf(text, "Vertex Plots");
        TCanvas* overCanvas = new TCanvas(text, text, 1600, 600);
    TPad* listOfRuns = new TPad("listOfRunsPad", "listOfRunsPad", 0., 0, 1., 0.1);
    TPad* vertexCanvas = new TPad("vertexCanvasPad", "vertexCanvasPad",0.,0.1, 1., 1.);
    vertexCanvas->Draw();
    listOfRuns->Draw();
    listOfRuns->cd();

    TText* runListText = new TText();
    runListText->SetTextSize(0.3);
    runListText->DrawText(0.03, 0.7, rootfilename);
    TText* cutListText = new TText();
    cutListText->SetTextSize(0.3);
    cutListText->SetTextColor(2);
    cutListText->DrawText(0.03, 0.3, gCut->GetCutString().Data());

    TObjArray* vertexPlots = GetVerticesFromRootFile(rootfilename);

    //Z Projection of Vertices
    vertexCanvas->cd();
    TPad* ZPad = new TPad("Z", "Z", 0, 0., 0.2 ,0.5);
    ZPad->Draw();
    ZPad->cd();
    TH1D* zVert = (TH1D*)vertexPlots->At(Z_INDEX);
    zVert->DrawCopy();
    DrawTrapPosition(zVert);
    sprintf(text, "Integral: %d", zVert->Integral());
    AddRandomText(text, 1, 0.6);
    delete zVert;

    //Z-Phi Distribution
    vertexCanvas->cd();
    TPad* ZPhiPad = new TPad("ZPhi", "ZPhi", 0.2, 0., 0.4, 0.5);
    ZPhiPad->Draw();
    ZPhiPad->cd();
    TH2D* zPhiVert =(TH2D*) vertexPlots->At(ZPHI_INDEX);
    MakeBlueBackground(zPhiVert);
    zPhiVert->DrawCopy("COLZ");
    DrawTrapEdges(zPhiVert);
    sprintf(text, "Integral: %d", zPhiVert->Integral());
    AddRandomText(text, 1, 0.6);
    delete zPhiVert;

    //Z-t Distribution
    vertexCanvas->cd();
    TPad* ZTPad = new TPad("ZT", "ZT", 0.4, 0, 0.6, 0.5);
    ZTPad->Draw();
    ZTPad->cd();
    TH2D* zTVert =(TH2D*) vertexPlots->At(ZT_INDEX);
    MakeBlueBackground(zTVert);
    zTVert->DrawCopy("COLZ");
    DrawTrapEdges(zTVert);
    sprintf(text, "Integral: %d", zTVert->Integral());
    AddRandomText(text, 1, 0.6);
    delete zTVert;

    //Z-R Distribution
    vertexCanvas->cd();
    TPad* ZRPad = new TPad("ZR", "ZR", 0.6, 0.5, 0.8, 1.);
    ZRPad->Draw();
    ZRPad->cd();
    TH2D* zRVert =(TH2D*) vertexPlots->At(ZR_INDEX);
    MakeBlueBackground(zRVert);
    zRVert->DrawCopy("COLZ");
    DrawTrapEdges(zRVert);
    delete zRVert;

    //X-Y Distribution
    vertexCanvas->cd();
    TPad* XYPad = new TPad("XY", "XY", 0., 0.5, 0.2, 1.);
    XYPad->Draw();
    XYPad->cd();
    TH2D* xYVert =(TH2D*) vertexPlots->At(XY_INDEX);
    MakeBlueBackground(xYVert);
    xYVert->DrawCopy("COLZ");
    sprintf(text, "Integral: %d", xYVert->Integral());
    AddRandomText(text, 1, 0.6);
    #ifdef RINGFIT
     ringfit(xYVert);
    #endif
    delete xYVert;

    //T Distribution
    vertexCanvas->cd();
    TPad* TimePad = new TPad("Time", "Time", 0.2, 0.5, 0.4, 1);
    TimePad->Draw();
    TimePad->cd();
    TH1D* tVert =(TH1D*) vertexPlots->At(T_INDEX);
    tVert->DrawCopy();
    sprintf(text, "Integral: %d", tVert->Integral());
    AddRandomText(text, 1, 0.6);
    delete tVert;

    //RDens Distribution
    vertexCanvas->cd();
    TPad* RDensPad = new TPad("RDens", "RDens", 0.4, 0.5, 0.6, 1);
    RDensPad->Draw();
    RDensPad->cd();
    TH1D* rDensVert =(TH1D*) vertexPlots->At(RDENS_INDEX);
    #ifdef RINGFIT
        Double_t par[4];
        Double_t perr[4];
        Int_t ierr = radfit(rDensVert, par,  perr);
        if (ierr ==0) {
        sprintf(text,"Entries  %i \n", rDensVert->GetEntries());
        AddRandomText(text, 1, 0.5,2);
        sprintf(text,"Central  %6.0f+-%5.0f \n", par[0], perr[0]);
        AddRandomText(text, 1, 0.5,3);
        sprintf(text,"Wall   %6.0f+-%5.0f \n",par[1],perr[1]);
        AddRandomText(text, 1, 0.5,4);
        sprintf(text,"Const  %6.0f+-%5.0f \n", par[2], perr[2]);
        AddRandomText(text, 1, 0.5,5);
        } else {
        sprintf(text,"Fit did not Converge \n");
        AddRandomText(text, 1, 0.5,3);
        }
    #endif
     rDensVert->DrawCopy();

    delete rDensVert;

    //Phi Distribution
    vertexCanvas->cd();
    TPad* PhiPad = new TPad("Phi", "Phi", 0.6, 0., 0.8, 0.5);
    PhiPad->Draw();
    PhiPad->cd();
    TH1D* phiVert =(TH1D*) vertexPlots->At(PHI_INDEX);
    phiVert->SetFillColor(1);
    phiVert->DrawCopy("hbar");
    sprintf(text, "Integral: %.0lf", phiVert->Integral());
    AddRandomText(text, 1, 0.6);
    delete phiVert;

    //T-R
        vertexCanvas->cd();
    TPad* TRPad = new TPad("TR", "TR", 0.8, 0.5, 1.0, 1.);
    TRPad->Draw();
    TRPad->cd();
    TH2D* tRVert =(TH2D*) vertexPlots->At(TR_INDEX);
    MakeBlueBackground(tRVert);
    tRVert->DrawCopy("COLZ");
    delete tRVert;

    delete vertexPlots;

    Close_Open_Files();
}

// ==================== PLOT OCTUPOLE ======================================================================

void PlotOctupole(Int_t runNumber, Double_t octupoleCurrent, Double_t trapLength)
{
    Int_t octupoleDump = MatchDumpNameToDumpNumber(runNumber, "Octupole");

    PlotOctupole(runNumber, octupoleDump, octupoleCurrent, trapLength);
}

void PlotOctupole(Int_t runNumber, Int_t dumpNumber, Double_t octupoleCurrent, Double_t trapLength)
{
    gStyle->SetOptStat(goptStat);
    TH1D* timeHisto = GetDumpHisto(runNumber, 1, dumpNumber, SIS2_TTC_SI_TRIG);

    TH1D* radialHisto = GetOctupoleRadialHistoFromTimeHisto(timeHisto, octupoleCurrent, timeHisto->GetXaxis()->GetXmax(), trapLength);

    TCanvas* octupoleCanvas = new TCanvas("Octupole Radial Profile", "Octupole Radial Profile", 800, 600);
    octupoleCanvas->Draw();
    sprintf(text,"Octupole ramp, run %d, dump %d", runNumber, dumpNumber, trapLength);
    radialHisto->SetTitle(text);
    radialHisto->Draw();

    sprintf(text,"Integral: %d", timeHisto->Integral());
    AddRandomText(text, 1, 0.5, 0);
    sprintf(text,"Trap Length %.1lfmm", trapLength);
    AddRandomText(text, 4, 0.5, 1);

    delete timeHisto;
}

// ==================== VERTEX CUT ======================================================================

TVertexCut* DefineCut(Double_t zMin, Double_t zMax, Double_t rMin, Double_t rMax, Double_t tMin, Double_t tMax, Double_t phiStart, Double_t phiEnd)
{
    gCut->DefineCut(zMin, zMax, rMin, rMax, tMin, tMax, phiStart, phiEnd);

    return gCut;
}

TVertexCut* ClearCut(TVertexCut* cut = gCut)
{
    cut->DefineCut(0., 0., 0., 0.,0., 0.,0., 0., 0., 0., 0., 0.);
    cut->SetIsActive(0);
}

TVertexCut* SetCut(TVertexCut* cut)
{
    gCut=cut;
}

TVertexCut* SetCut(Double_t zMin, Double_t zMax, Double_t rMin, Double_t rMax, Double_t tMin, Double_t tMax, Double_t phiStart, Double_t phiEnd)
{
    gCut->DefineCut(zMin, zMax, rMin, rMax, tMin, tMax, phiStart, phiEnd);

    return gCut;
}

TVertexCut* SetCutZ(Double_t zMin, Double_t zMax, Int_t reset=0, TVertexCut* cut = NULL)
{

    if(cut==NULL) cut=gCut;
    if(reset==1) ClearCut(cut);
    cut->SetzMin(zMin);
    cut->SetzMax(zMax);
    cut->SetIsActive(1);

    return cut;
}

TVertexCut* SetCutR(Double_t rMin, Double_t rMax, Int_t reset=0, TVertexCut* cut = NULL)
{
    if(cut==NULL) cut=gCut;
    if(reset==1) ClearCut(cut);
    cut->SetrMin(rMin);
    cut->SetrMax(rMax);
    cut->SetIsActive(1);

    return cut;
}

TVertexCut* SetCutT(Double_t tMin, Double_t tMax, Int_t reset=0, TVertexCut* cut = NULL)
{
    if(cut==NULL) cut=gCut;
    if(reset==1) ClearCut(cut);
    cut->SettMin(tMin);
    cut->SettMax(tMax);
    cut->SetIsActive(1);

    return cut;
}

TVertexCut* SetCutPhi(Double_t phiStart, Double_t phiEnd, Int_t reset=0, TVertexCut* cut = NULL)
{
    if(cut==NULL) cut=gCut;
    if(reset==1) ClearCut(cut);
    cut->SetphiStart(phiStart);
    cut->SetphiEnd(phiEnd);
    cut->SetIsActive(1);

    return cut;
}

TVertexCut* SetCutNTracksRange(Int_t nTracksMin, Int_t nTracksMax, Int_t reset=0, TVertexCut* cut = NULL)
{
    if(cut==NULL) cut=gCut;
    if(reset==1) ClearCut(cut);
    cut->SetnTracksMin(nTracksMin);
    cut->SetnTracksMax(nTracksMax);
    cut->SetIsActive(1);

    return cut;
}

TVertexCut* SetCutResidual(Double_t residualMin, Double_t residualMax, Int_t reset=0, TVertexCut* cut=NULL)
{
    if(cut==NULL) cut=gCut;
    if(reset==1) ClearCut(cut);
    cut->SetResidualMin(residualMin);
    cut->SetResidualMax(residualMax);
    cut->SetIsActive(1);

    return cut;
}

TVertexCut* SetCutNTracks(Int_t nTracks, Int_t reset=0, TVertexCut* cut = NULL)
{
    if(cut==NULL) cut=gCut;
    if(reset==1) ClearCut(cut);
    cut->SetnTracksMin(nTracks);
    cut->SetnTracksMax(nTracks);
    cut->SetIsActive(1);

    return cut;
}

void PrintCuts(TVertexCut* cut=gCut)
{
    cut->PrintCuts();
    return;
}

// ==================== TRANSFORMER VALUE ======================================================================

Double_t GetTransformerValue(Int_t runNumber, Int_t* nStacks=NULL)
{

    Int_t numberStacks(0);
    TSeq_Event* seqEvent;
    Double_t transformerSum(0.), hotDumpTime, spillNumber, tafo;

    while(true)
    {
        seqEvent = FindSequencerEvent(runNumber, "startDump", HOT_NAME, numberStacks+1);
        if(seqEvent==NULL) break;

        numberStacks++;
        hotDumpTime =  Get_RunTime_of_SequencerEvent(runNumber, seqEvent);

        spillNumber = Get_Total_Sis_Counts(runNumber, SIS_AD, 0., hotDumpTime); // This is the spill number of the hot dump

        // The transformer value at the next AD trigger should be the one we're looking for

        tafo = LabVIEW_at_SIS(runNumber, SIS_AD, spillNumber+1, "ADE0", 6 , -1);

        if(tafo<0.3) tafo=0.;

        transformerSum+=tafo;
    }

    //Info("GetTransformerValue", "Last Error was not an Error!!");

    Info("GetTransformerValue", "\033[32mFound %d stacks, total TransformerNumber %.3lf\033[00m", numberStacks, transformerSum);
    if(nStacks!=NULL) nStacks[0] = numberStacks;
    return transformerSum;
}

// ==================== GET VERTICES ======================================================================

TH1D* GetZVertices(Int_t runNumber, Int_t dumpNumber, TVertexCut* cut=NULL)
{
    TObjArray* vertices = GetVertices(runNumber, dumpNumber, cut);
    return (TH1D*) vertices->At(Z_INDEX);
}

TH2D* GetZPhiVertices(Int_t runNumber, Int_t dumpNumber, TVertexCut* cut=NULL)
{
    TObjArray* vertices = GetVertices(runNumber, dumpNumber, cut);
    return (TH2D*) vertices->At(ZPHI_INDEX);
}

TH2D* GetZTVertices(Int_t runNumber, Int_t dumpNumber, TVertexCut* cut=NULL)
{
    TObjArray* vertices = GetVertices(runNumber, dumpNumber, cut);
    return (TH2D*) vertices->At(ZT_INDEX);
}

TH2D* GetXYVertices(Int_t runNumber, Int_t dumpNumber, TVertexCut* cut=NULL)
{
    TObjArray* vertices = GetVertices(runNumber, dumpNumber, cut);
    return (TH2D*) vertices->At(XY_INDEX);
}

TH2D* GetZRVertices(Int_t runNumber, Int_t dumpNumber, TVertexCut* cut=NULL)
{
    TObjArray* vertices = GetVertices(runNumber, dumpNumber, cut);
    return (TH2D*) vertices->At(ZR_INDEX);
}

TH1D* GetTimeVertices(Int_t runNumber, Int_t dumpNumber, TVertexCut* cut=NULL)
{
    TObjArray* vertices = GetVertices(runNumber, dumpNumber, cut);
    return (TH1D*) vertices->At(T_INDEX);
}

TH1D* GetRVertices(Int_t runNumber, Int_t dumpNumber, TVertexCut* cut=NULL)
{
    TObjArray* vertices = GetVertices(runNumber, dumpNumber, cut);
    return (TH1D*) vertices->At(R_INDEX);
}

TH1D* GetRDensVertices(Int_t runNumber, Int_t dumpNumber, TVertexCut* cut=NULL)
{
    TObjArray* vertices = GetVertices(runNumber, dumpNumber, cut);
    return (TH1D*) vertices->At(RDENS_INDEX);
}

TObjArray* GetVertices(Int_t runNumber, Int_t dumpNumber, TVertexCut* cut=NULL)
{
    Double_t startTime, endTime;
    GetDumpTimes(runNumber, 1, dumpNumber, &startTime, &endTime);
    return GetVertices(runNumber, startTime, endTime, cut);
}

TObjArray* GetVertices(Int_t runNumber, Double_t startTime, Double_t endTime, TVertexCut* cut=NULL)
{
    if(cut==NULL) cut=gCut;

    Double_t r, phi;

    Double_t startMixTime = startTime;
    Double_t endMixTime = endTime;

    TObjArray* vertexPlots = new TObjArray();

    TH1::AddDirectory(kFALSE);
    TH2::AddDirectory(kFALSE);

    #ifdef ALLOW_VERTEX_CUTS
        if(cut->GetIsActive())
        {
            if(cut->GettMax()!=0.) endTime = startTime+cut->GettMax();
            startTime+=cut->GettMin();
        }
    #endif

    TTree* vertexTree = Get_Vtx_Tree(runNumber);
    TSiliconEvent* siliconEvent = new TSiliconEvent();
    vertexTree->SetBranchAddress("SiliconEvent", &siliconEvent );
    TVector3* vtx;

    sprintf(text, "Z Distribution, Run: %d", runNumber);
    TH1D* zVert = new TH1D( text, text, (Z_MAX-Z_MIN)/VERTEX_RES_Z, Z_MIN, Z_MAX );
    zVert->GetXaxis()->SetTitle("z [cm]");
    zVert->GetYaxis()->SetTitle("events");
    zVert->GetXaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    zVert->GetYaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    vertexPlots->Add(zVert);

    sprintf(text, "Z-#phi Distribution, Run: %d", runNumber);
    TH2D* zPhiVert = new TH2D( text, text, (Z_MAX-Z_MIN)/VERTEX_RES_Z, Z_MIN, Z_MAX, 360./VERTEX_RES_PHI, -180., 180.);
    zPhiVert->GetXaxis()->SetTitle("z [cm]");
    zPhiVert->GetYaxis()->SetTitle("#phi [deg]");
    zPhiVert->GetXaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    zPhiVert->GetYaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    vertexPlots->Add(zPhiVert);

    sprintf(text, "Z-T Distribution, Run: %d", runNumber);
    TH2D* zTVert = new TH2D( text, text, (Z_MAX-Z_MIN)/VERTEX_RES_Z, Z_MIN, Z_MAX, NUMBER_BINS, 0., endMixTime-startMixTime);
    zTVert->GetXaxis()->SetTitle("z [cm]");
    zTVert->GetYaxis()->SetTitle("t [s]");
    zTVert->GetXaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    zTVert->GetYaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    vertexPlots->Add(zTVert);

    sprintf(text, "X-Y Distribution, Run: %d", runNumber);
    TH2D* xYVert = new TH2D( text, text, 10./VERTEX_RES_TRANSVERSE, -5, 5., 10./VERTEX_RES_TRANSVERSE, -5., 5.);
    xYVert->GetXaxis()->SetTitle("x [cm]");
    xYVert->GetYaxis()->SetTitle("y [cm]");
    xYVert->GetXaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    xYVert->GetYaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    vertexPlots->Add(xYVert);

    sprintf(text, "T Distribution, Run: %d", runNumber);
    TH1D* tVert = new TH1D( text, text, NUMBER_BINS, 0., endMixTime-startMixTime);
    tVert->GetXaxis()->SetTitle("t [s]");
    tVert->GetYaxis()->SetTitle("events");
    tVert->GetXaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    tVert->GetYaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    vertexPlots->Add(tVert);

    sprintf(text, "R Distribution, Run: %d", runNumber);
    TH1D* rVert = new TH1D( text, text, R_MAX/VERTEX_RES_RADIAL, 0, R_MAX );
    rVert->GetXaxis()->SetTitle("r [cm]");
    rVert->GetYaxis()->SetTitle("events");
    rVert->GetXaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    rVert->GetYaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    vertexPlots->Add(rVert);

    sprintf(text, "R Density Distribution, Run: %d", runNumber);
    TH1D* rDensVert = new TH1D( text, text, R_MAX/VERTEX_RES_RADIAL, 0, R_MAX );
    rDensVert->GetXaxis()->SetTitle("r [cm]");
    rDensVert->GetYaxis()->SetTitle("events/area [arb]");
    rDensVert->GetXaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    rDensVert->GetYaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    vertexPlots->Add(rDensVert);
    rDensVert->Sumw2();

    sprintf(text, "Z-R Density Distribution, Run: %d", runNumber);
    TH2D* zRVert = new TH2D( text, text, (Z_MAX-Z_MIN)/VERTEX_RES_Z, Z_MIN, Z_MAX, R_MAX/VERTEX_RES_RADIAL, 0., R_MAX);
    zRVert->GetXaxis()->SetTitle("z [cm]");
    zRVert->GetYaxis()->SetTitle("r [cm]");
    zRVert->GetXaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    zRVert->GetYaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    vertexPlots->Add(zRVert);

    sprintf(text, "Phi Distribution, Run: %d", runNumber);
    TH1D* phiVert = new TH1D( text, text, 360./VERTEX_RES_PHI, -180., 180.);
    phiVert->GetXaxis()->SetTitle("phi [deg]");
    phiVert->GetYaxis()->SetTitle("events");
    phiVert->GetXaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    phiVert->GetYaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    vertexPlots->Add(phiVert);

    sprintf(text, "RunTime>=%lf && RunTime<=%lf", startTime,endTime);
    vertexTree->Draw(">>eventList", text);
    TEventList* eventList = (TEventList*)gDirectory->Get("eventList");

    if(eventList==NULL)
    {
        //Warning("", "eventList is NULL");
        delete vertexTree;
        return vertexPlots;
    }
    if(eventList->GetN()==0)
    {
        //Warning("", "no entries in eventList");
        delete eventList;
        delete vertexTree;
        return vertexPlots;
    }

    for(Int_t i=0; i<eventList->GetN(); i++)
    {
        siliconEvent->ClearEvent();
        vertexTree->GetEntry(eventList->GetEntry(i));
        if( siliconEvent->GetNVertices()==0 ) continue;

        vtx = siliconEvent->GetVertex();
        r = sqrt(vtx->X()**2+vtx->Y()**2);
        phi =  (vtx->Phi()/3.14159)*180.;

        #ifdef ALLOW_VERTEX_CUTS
                if(!(cut->CheckInRange(siliconEvent))) continue;
        #endif

         zVert->Fill(-1.*vtx->Z());
         zPhiVert->Fill(-1.*vtx->Z(), phi);
         zTVert->Fill(-1.*vtx->Z(), siliconEvent->GetRunTime()-startMixTime);
         xYVert->Fill(vtx->X(), vtx->Y());
         tVert->Fill(siliconEvent->GetRunTime()-startMixTime);
         rVert->Fill(r);
         if(r>0) rDensVert->Fill(r,1/r);
         if(r>0) zRVert->Fill(-1.*vtx->Z(), r, 1/r);
         phiVert->Fill(phi);

    }
    zVert->GetYaxis()->SetRangeUser(0., zVert->GetMaximum());
    tVert->GetYaxis()->SetRangeUser(0., tVert->GetMaximum());
    rDensVert->GetYaxis()->SetRangeUser(0., rDensVert->GetMaximum());
    rVert->GetYaxis()->SetRangeUser(0., rVert->GetMaximum());
    phiVert->GetYaxis()->SetRangeUser(0., phiVert->GetMaximum());

    delete siliconEvent;
    delete vertexTree;
    delete eventList;

    return vertexPlots;
}

TObjArray* GetVerticesFromRootFile(char* rootFileName, TVertexCut* cut=NULL)
{
    if(cut==NULL) cut=gCut;
    TFile* f = new TFile(rootFileName);
    TTree* vertexTree = (TTree*)f->Get("vertices");

    TSiliconEvent* siliconEvent = new TSiliconEvent();
    vertexTree->SetBranchAddress("vertices", &siliconEvent );
    TVector3* vtx;
    TVector2* maxTime = (TVector2*)f->Get("TVector2;1"); // The maximum time should be stored as a TVector2 in the root file. We need this to construct the axes of the histograms
    Double_t endTime = maxTime->X();
    Double_t startTime(0.), r, phi;

    Double_t startMixTime = startTime;
    Double_t endMixTime = endTime;

    TObjArray* vertexPlots = new TObjArray();

    //TH1::AddDirectory(kFALSE);
    //TH2::AddDirectory(kFALSE);

    #ifdef ALLOW_VERTEX_CUTS
        if(cut->GetIsActive())
        {
            if(cut->GettMax()!=0.) endTime = startTime+cut->GettMax();
            startTime+=cut->GettMin();
        }
    #endif

    sprintf(text, "Z Distribution");
        TH1D* zVert = new TH1D( text, text, (Z_MAX-Z_MIN)/VERTEX_RES_Z, Z_MIN, Z_MAX );
    zVert->GetXaxis()->SetTitle("z [cm]");
    zVert->GetYaxis()->SetTitle("events");
    zVert->GetXaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    zVert->GetYaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    vertexPlots->Add(zVert);

    sprintf(text, "Z-#phi Distribution");
    TH2D* zPhiVert = new TH2D( text, text, (Z_MAX-Z_MIN)/VERTEX_RES_Z, Z_MIN, Z_MAX, 360./VERTEX_RES_PHI, -180., 180.);
    zPhiVert->GetXaxis()->SetTitle("z [cm]");
    zPhiVert->GetYaxis()->SetTitle("#phi [deg]");
    zPhiVert->GetXaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    zPhiVert->GetYaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    vertexPlots->Add(zPhiVert);

    sprintf(text, "Z-T Distribution");
    TH2D* zTVert = new TH2D( text, text, (Z_MAX-Z_MIN)/VERTEX_RES_Z, Z_MIN, Z_MAX, NUMBER_BINS, 0., endMixTime-startMixTime);
    zTVert->GetXaxis()->SetTitle("z [cm]");
    zTVert->GetYaxis()->SetTitle("t [s]");
    zTVert->GetXaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    zTVert->GetYaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    vertexPlots->Add(zTVert);

    sprintf(text, "X-Y Distribution");
    TH2D* xYVert = new TH2D( text, text, 10./VERTEX_RES_TRANSVERSE, -5, 5., 10./VERTEX_RES_TRANSVERSE, -5., 5.);
    xYVert->GetXaxis()->SetTitle("x [cm]");
    xYVert->GetYaxis()->SetTitle("y [cm]");
    xYVert->GetXaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    xYVert->GetYaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    vertexPlots->Add(xYVert);

    sprintf(text, "T Distribution");
    TH1D* tVert = new TH1D( text, text, NUMBER_BINS, 0., endMixTime-startMixTime);
    tVert->GetXaxis()->SetTitle("t [s]");
    tVert->GetYaxis()->SetTitle("events");
    tVert->GetXaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    tVert->GetYaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    vertexPlots->Add(tVert);

    sprintf(text, "R Distribution");
    TH1D* rVert = new TH1D( text, text, R_MAX/VERTEX_RES_RADIAL, 0, 8 );
    rVert->GetXaxis()->SetTitle("r [cm]");
    rVert->GetYaxis()->SetTitle("events");
    rVert->GetXaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    rVert->GetYaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    vertexPlots->Add(rVert);

    sprintf(text, "R Density Distribution");
    TH1D* rDensVert = new TH1D( text, text, R_MAX/VERTEX_RES_RADIAL, 0, R_MAX );
    rDensVert->GetXaxis()->SetTitle("r [cm]");
    rDensVert->GetYaxis()->SetTitle("events/area [arb]");
    rDensVert->GetXaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    rDensVert->GetYaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    vertexPlots->Add(rDensVert);
    rDensVert->Sumw2();

    sprintf(text, "Z-r Density Distribution");
    TH2D* zRVert = new TH2D( text, text, (Z_MAX-Z_MIN)/VERTEX_RES_Z, Z_MIN, Z_MAX, R_MAX/VERTEX_RES_RADIAL, 0., R_MAX);
    zRVert->GetXaxis()->SetTitle("z [cm]");
    zRVert->GetYaxis()->SetTitle("r [cm]");
    zRVert->GetXaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    zRVert->GetYaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    vertexPlots->Add(zRVert);

    sprintf(text, "Phi Distribution, Run: %d");
    TH1D* phiVert = new TH1D( text, text, 360./VERTEX_RES_PHI, -180., 180.);
    phiVert->GetXaxis()->SetTitle("phi [deg]");
    phiVert->GetYaxis()->SetTitle("events");
    phiVert->GetXaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    phiVert->GetYaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    vertexPlots->Add(phiVert);

    sprintf(text, "r-T Distribution");
    TH2D* rTVert = new TH2D( text, text, (R_MAX)/VERTEX_RES_RADIAL, 0., R_MAX, NUMBER_BINS, 0., endMixTime-startMixTime);
    rTVert->GetXaxis()->SetTitle("r [cm]");
    rTVert->GetYaxis()->SetTitle("t [s]");
    rTVert->GetXaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    rTVert->GetYaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    vertexPlots->Add(rTVert);

    sprintf(text, "#phi-Z Distribution");
    TH2D* phiZVert = new TH2D( text, text, 360./VERTEX_RES_PHI, -180., 180., (Z_MAX-Z_MIN)/VERTEX_RES_Z, Z_MIN, Z_MAX);
    phiZVert->GetYaxis()->SetTitle("z [cm]");
    phiZVert->GetXaxis()->SetTitle("#phi [deg]");
    phiZVert->GetXaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    phiZVert->GetYaxis()->SetTitleSize(VERTEX_TITLE_SIZE);
    vertexPlots->Add(phiZVert);

    for(Int_t i=0; i<vertexTree->GetEntries(); i++)
    {
        siliconEvent->ClearEvent();
        vertexTree->GetEntry(i);
        if( siliconEvent->GetNVertices()==0 ) continue;

        if ((siliconEvent->GetRunTime()<startTime)|| siliconEvent->GetRunTime()>endTime) continue;

        vtx = siliconEvent->GetVertex();
        r = sqrt(vtx->X()**2+vtx->Y()**2);
        phi =  (vtx->Phi()/3.14159)*180.;

        #ifdef ALLOW_VERTEX_CUTS
                if(!(cut->CheckInRange(siliconEvent))) continue;
        #endif

         zVert->Fill(-1.*vtx->Z());
         zPhiVert->Fill(-1.*vtx->Z(), phi);
         zTVert->Fill(-1.*vtx->Z(), siliconEvent->GetRunTime()-startMixTime);
         xYVert->Fill(vtx->X(), vtx->Y());
         tVert->Fill(siliconEvent->GetRunTime()-startMixTime);
         rVert->Fill(r);
         if(r>0)rDensVert->Fill(r,1/r);
         if(r>0)zRVert->Fill(-1.*vtx->Z(), r, 1/r);
         rTVert->Fill(r, siliconEvent->GetRunTime()-startMixTime);
         phiVert->Fill(phi);
         phiZVert->Fill(phi, -1.*vtx->Z());
    }
    zVert->GetYaxis()->SetRangeUser(0., zVert->GetMaximum());
    tVert->GetYaxis()->SetRangeUser(0., tVert->GetMaximum());
    rDensVert->GetYaxis()->SetRangeUser(0., rDensVert->GetMaximum());
    rVert->GetYaxis()->SetRangeUser(0., rVert->GetMaximum());
    phiVert->GetYaxis()->SetRangeUser(0., phiVert->GetMaximum());
    siliconEvent->Delete();
    vertexTree->Delete();

    return vertexPlots;
}


Int_t GetVerticesRaw(Int_t runNumber, Int_t dumpNumber, TTree* vertices, Double_t* maxTime)
{
    /*
        This function is for getting the silicon events (probably for storage in a tree or root file
    */

    Double_t startTime, endTime;
    if(dumpNumber==-1)
    {
        //Get the vertices for the whole run
        startTime = 0.;
        endTime = Get_Total_RunTime(runNumber);
    }
    else
    {
        GetDumpTimes(runNumber, 1, dumpNumber, &startTime, &endTime );
    }

    return GetVerticesRaw(runNumber, startTime, endTime, vertices, maxTime);
}


Int_t GetVerticesRaw(Int_t runNumber, Double_t startTime, Double_t endTime, TTree* vertices, Double_t* maxTime)
{
    /*
        This function is for getting the silicon events (probably for storage in a tree or root file
    */

    Double_t time;
    Int_t n(0);

    TTree* vertexTree = Get_Vtx_Tree(runNumber);

    sprintf(text, "RunTime>=%lf && RunTime<=%lf", startTime,endTime);
    vertexTree->Draw(">>eventList", text);
    TEventList* eventList = (TEventList*)gDirectory->Get("eventList");

    if(eventList==NULL)
    {
        //Warning("", "eventList is NULL");
        delete vertexTree;
        return 0;
    }
    if(eventList->GetN()==0)
    {
        //Warning("", "no entries in eventList");
        delete eventList;
        delete vertexTree;
        return 0;
    }

    TSiliconEvent* siliconEvent = new TSiliconEvent();
    vertexTree->SetBranchAddress("SiliconEvent", &siliconEvent );
    vertices->SetBranchAddress("vertices", &siliconEvent);

    for(Int_t i=0; i<eventList->GetN(); i++)
    {
        siliconEvent->ClearEvent();
        vertexTree->GetEntry(eventList->GetEntry(i));
        if(gSAVEONLYVERTICES) if( siliconEvent->GetNVertices()==0 ) continue;
        if(!gCut->CheckInRange(siliconEvent)) continue;
        time = siliconEvent->GetRunTime();
        if( time>endTime || time<startTime) continue;
        siliconEvent->SetRunTime(time-startTime);

        vertices->Fill();
        n++;
        if(time-startTime>maxTime[0]) maxTime[0] = time-startTime;
    }

    delete eventList;
    delete siliconEvent;
    delete vertexTree;
    return n;
}

void SaveZVertices(char* rootFileName, char* outFileName, TVertexCut* cut=NULL)
{
    if(cut==NULL) cut = gCut;
    TObjArray* vertices = GetVerticesFromRootFile(rootFileName, cut);

    TFile* f = new TFile(outFileName, "RECREATE");
    TH1D* zVertices = vertices->At(0);
    zVertices->SetName("vertexDistribution");
    f->cd();
    zVertices->Write();
    f->Close();

}

// ==================== OCTUPOLE RAMP TOOLS ======================================================================

TSpline5* GetCriticalRadiusSpline(Double_t octupoleCurrent, Double_t trapLength)
{
    Double_t time[OCTUPOLE_RES], RCrit[OCTUPOLE_RES], current;

    for(Int_t i=0; i<OCTUPOLE_RES; i++)
    {
        time[i] = (Double_t)i/(OCTUPOLE_RES-1.);
        current = octupoleCurrent*i/(OCTUPOLE_RES-1);

        RCrit[i] = GetRCrit(current, trapLength);
    }

    TSpline5* rCritSpline = new TSpline5("Critical Radius", time, RCrit, OCTUPOLE_RES);
    return rCritSpline;
}

TH1D* GetOctupoleRadialHistoFromTimeHisto(TH1D* timeHisto, Double_t octupoleCurrent, Double_t rampTime, Double_t trapLength)
{
    TSpline5* criticalRadius = GetCriticalRadiusSpline(octupoleCurrent, trapLength);

    TH1D* radialHisto = new TH1D("histoTitle", "histoTitle", OCTUPOLE_RES, 0., criticalRadius->Eval(0.));

    Double_t thisRadius, thisTimeAsFraction;
    Int_t assignedBin, dumpFinished(0);

    for (Int_t i =1; i<=timeHisto->GetNbinsX(); i++)
    {
        thisTimeAsFraction = (timeHisto->GetBinCenter(i)-RAMP_START_DELAY)/(timeHisto->GetBinCenter(timeHisto->GetNbinsX()-1)-RAMP_START_DELAY-RAMP_END_DELAY);
        thisRadius = criticalRadius->Eval(thisTimeAsFraction);

        if(thisTimeAsFraction<0.)
        {
            assignedBin = radialHisto->AddBinContent(0,timeHisto->GetBinContent(i));
        }
        elseif(thisTimeAsFraction>0.6)
        {
//          assignedBin = radialHisto->AddBinContent(radialHisto->GetNbinsX()+1,timeHisto->GetBinContent(i));
        }
        else assignedBin = radialHisto->Fill(thisRadius,timeHisto->GetBinContent(i));

        //printf("%.3lf %.3lf\n", thisRadius, timeHisto->GetBinContent(i));
    }

    for (Int_t i=1; i<=radialHisto->GetNbinsX(); i++)
    {
        radialHisto->SetBinContent(i, radialHisto->GetBinContent(i)/radialHisto->GetBinCenter(i));
    }

    radialHisto->SetXTitle("Radius [mm]");
    radialHisto->SetYTitle("Counts/area [arb]");

    return radialHisto;
}

double GetRCrit(double current,double length)
{

// Stolen from Niels

  double r_wall = 22; // wall location (for calculation)
  double Bsol = 1.04; // axial B-field on axis in trap center
  double Bw_fac;

  Bw_fac = 1.5/900; // in units of Tesla/Ampere

  return r_wall/sqrt(1+Bw_fac*current/Bsol*(length/r_wall));
}

// ==================== RESCALE/REBIN TOOLS ======================================================================

void ScaleAndRebinHistogram(TH1D* histo)
{
    histo = RebinHistoToNBins(histo);
    histo->Scale(1./histo->GetBinWidth(1));
    histo->SetYTitle("Rate [Hz]");
    histo->GetYaxis()->SetRangeUser(0., histo->GetMaximum());
}

TH1D* RebinHistoToNBins(TH1D* histo, Int_t newBins = 0)
{
    if (newBins==0) newBins=NUMBER_BINS;
    Int_t numBins = histo->GetNbinsX();
    if(numBins>newBins && (Int_t)numBins/newBins>1 )
    {
        histo->Rebin((Int_t)numBins/newBins);
    }
    histo->GetYaxis()->SetRangeUser(0., histo->GetMaximum());
    return histo;

    /*
    TH1D* newhisto = new TH1D(histo->GetName(), histo->GetTitle(), newBins, histo->GetXaxis()->GetXmin(), histo->GetXaxis()->GetXmin());

    for(Int_t i=1; i<=histo->GetNbinsX(); i++)
    {
        newhisto->Fill(histo->GetBinCenter(i), histo->GetBinContent(i));
    }

    newhisto->SetXTitle("Time [s]");
    newhisto->SetYTitle("Counts");

    delete histo;
    return newhisto;
    */

}

// ==================== GET DUMP HISTO ======================================================================

TH1D* GetDumpHisto(Int_t runNumber, char* description, Int_t channel, Int_t dumpRep=1, Double_t startCut=0., Double_t endCut=0.)
{
    Int_t dumpNumber = MatchDumpNameToDumpNumber(runNumber, description, dumpRep);
    TH1D* dumpHisto = GetDumpHisto(runNumber, 0, dumpNumber, channel, startCut, endCut);

    return dumpHisto;
}

// ==================== SEQUENCER EVENT TOOLS ======================================================================

TSeq_Event* FindSequencerEvent(Int_t runNumber, char* eventName, char* description, Int_t repetition = 1)
{
    // return the repetition(th) sequencer Event which matches eventName and CONTAINS the text in description
    TTree* sequencerTree = GetSequencerTree(runNumber);
    TSeq_Event* seqEvent = new TSeq_Event();
    sequencerTree->SetBranchAddress("SequenceEvent", &seqEvent);

    sprintf(text, "\"%s", description); // add a " before the name

    Int_t rep(0);
    for (Int_t i=0; i<sequencerTree->GetEntries(); i++)
    {
        sequencerTree->GetEntry(i);
        if(seqEvent->GetEventName().CompareTo(TString(eventName), TString::kIgnoreCase)) continue;

        if(!(seqEvent->GetDescription().BeginsWith(TString(text), TString::kIgnoreCase))) continue;

        rep++;
        if(rep<repetition) continue;

        //Info("FindSequencerEvent", "Found matching Event #%d %s %s", i, seqEvent->GetEventName().Data(), seqEvent->GetDescription().Data());
        delete sequencerTree;
        return seqEvent;
    }

    delete sequencerTree;
    Error("FindSequencerEvent", "\033[33mCould not find a match for event: %s\t %s\033[00m", eventName, description);

    return NULL;
}

Double_t MatchEventToTime(Int_t runNumber, char* eventName, char* description, Int_t repetition = 1, Int_t offset=0)
{
    TSeq_Event* seqEvent = FindSequencerEvent(runNumber, eventName, description, repetition);
    if(seqEvent==NULL)
    {
        Error("MatchEventToTime", "\033[33mCould not find sequncer event %s (%s) in run %d\033[00m", eventName, description, runNumber);
        return -1;
    }

    Double_t runTime = Get_RunTime_of_SequencerEvent(runNumber, seqEvent, offset);

    delete seqEvent;
    return runTime;

}

Int_t MatchDumpNameToDumpNumber(Int_t runNumber, char* description, Int_t repetition = 1, Int_t stop=0)
{
    //Info("", "Called MatchDumpNameToDumpNumber(runNumber = %d, description = %s, repetition = %d, stop = %d", runNumber, description, repetition, stop);

    myStopwatch->Start();
    Int_t seqOffset(0), dumpNumber(0), bestDumpNumber(0);
    Double_t bestTimeDiff(1000);

    if(strcmp(description, "WHOLE_RUN")==0)
        return -1;

    while(true)
    {
    if(stop) Double_t timeOfStart = MatchEventToTime(runNumber, "stopDump", description, repetition, seqOffset);
    else Double_t timeOfStart = MatchEventToTime(runNumber, "startDump", description, repetition, seqOffset);

    if(stop) TTree* startOfDumps = Get_Sis_Tree(runNumber, SIS_DUMP_STOP);
    else TTree* startOfDumps = Get_Sis_Tree(runNumber, SIS_DUMP_START);
    TSisEvent* sisEvent = new TSisEvent;
    startOfDumps->SetBranchAddress("SisEvent", &sisEvent );

    for(dumpNumber=0; dumpNumber<startOfDumps->GetEntries(); dumpNumber++)
    {
        startOfDumps->GetEntry(dumpNumber);

        if(sisEvent->GetRunTime()>timeOfStart) break;
    }

    if(dumpNumber==startOfDumps->GetEntries()) dumpNumber--;

    Double_t timeAfter = sisEvent->GetRunTime();

    startOfDumps->GetEntry(dumpNumber-1);
    Double_t timeBefore = sisEvent->GetRunTime();
    Double_t timeDiff = timeAfter-timeOfStart;

    if((timeAfter-timeOfStart)>(timeOfStart-timeBefore))
    {
        dumpNumber--;
        timeDiff = fabs(timeOfStart-timeBefore);
    }

    dumpNumber++; // Dumps are supposed to be numbered starting at 1, not 0

    if(timeDiff>MATCH_TIME_TOLERANCE)
    {
        sprintf(text, "Run %d, Dump %d -> Time difference is large : %.3f (%.3lf)", runNumber, dumpNumber, timeDiff, MATCH_TIME_TOLERANCE);
        Warning("MatchDumpNameToCounts", text);
    }


    if(bestTimeDiff>timeDiff)
    {
        bestTimeDiff=timeDiff;
        bestDumpNumber=dumpNumber;
    }

    if(bestTimeDiff<MATCH_TIME_TOLERANCE || seqOffset>MATCH_SEQUENCE_TOLERANCE || (dumpNumber==0 && seqOffset>0)) break; // allow retrying the sequence this many times (a common failure mode)
    if(seqOffset==0)seqOffset=-1;
    else if(seqOffset==-1) seqOffset=1;
    else seqOffset++;


    }

    if(stop)Info("MatchDumpNameToCounts", "Run %d : Matched END OF dump name \"%s\" to count %d, timeOffset %.3lf in %.3lf s", runNumber, description, bestDumpNumber, bestTimeDiff, myStopwatch->RealTime());
    else Info("MatchDumpNameToCounts", "Run %d : Matched dump name \"%s\" to dump number %d, timeOffset %.3lf in %.3lf s", runNumber, description, bestDumpNumber, bestTimeDiff, myStopwatch->RealTime());

    delete startOfDumps;
    delete sisEvent;

    return bestDumpNumber;
}

void PrintSequencerEventTable(Int_t runNumber)
{
    TTree* sequencerTree = GetSequencerTree(runNumber);
    TSeq_Event* seqEvent = new TSeq_Event();
    sequencerTree->SetBranchAddress("SequenceEvent", &seqEvent);

    printf("entryNo\teventName\tDescription\tonCount\tonState\tSeq\tSeqN\n");
    printf("===========================================================\n");
    for (Int_t i=0; i<sequencerTree->GetEntries(); i++)
    {
        sequencerTree->GetEntry(i);
        printf("%d\t%s\t%s\t%d\t%d\t%s\t%d\n", i, seqEvent->GetEventName().Data(), seqEvent->GetDescription().Data(), seqEvent->GetonCount(), seqEvent->getonState(), seqEvent->GetSeq().Data(), seqEvent->GetSeqNum());
    }
}

void PrintSequencerTimeStampSummary(Int_t run_number)
{
    Int_t i, counts;
    Double_t startTime, stopTime;

    // Pbar Sequencer

    Int_t pbarStarts = Get_Total_Sis_Counts(run_number, SIS2_PBAR_SEQ_START);
    printf("\nPbar Sequencer: Total sequences: %d, Total Counts: %d\n", pbarStarts, Get_Total_Sis_Counts(run_number, SIS2_PBAR_SEQ_TIME));
    printf("Seq\tTime\tTimeStamps\n");

    stopTime = Get_RunTime_of_PbarSeq_Start(run_number, 1);
    counts = Get_Total_Sis_Counts(run_number, SIS2_PBAR_SEQ_TIME, 0., stopTime);
    printf("%d\t%.1f\t%d\n", 0, 0., counts);

    for(i=1; i<pbarStarts; i++)
    {
        startTime = Get_RunTime_of_PbarSeq_Start(run_number, i);
        stopTime = Get_RunTime_of_PbarSeq_Start(run_number, i+1);
        counts = Get_Total_Sis_Counts(run_number, SIS2_PBAR_SEQ_TIME, startTime, stopTime);

        printf("%d\t%.1f\t%d\n", i, startTime, counts);
    }

    startTime = Get_RunTime_of_PbarSeq_Start(run_number, i);
    counts = Get_Total_Sis_Counts(run_number, SIS2_PBAR_SEQ_TIME, startTime, 1e7);
    printf("%d\t%.1f\t%d\n", i, startTime, counts);

    // Mix Sequencer

    Int_t mixStarts = Get_Total_Sis_Counts(run_number, SIS2_MIX_SEQ_START);
    printf("\nMix Sequencer: Total sequences: %d, Total Counts: %d\n", mixStarts, Get_Total_Sis_Counts(run_number, SIS2_MIX_SEQ_TIME));

    printf("Seq\tTime\tTimeStamps\n");

    stopTime = Get_RunTime_of_MixSeq_Start(run_number, 1);
    counts = Get_Total_Sis_Counts(run_number, SIS2_MIX_SEQ_TIME, 0., stopTime);
    printf("%d\t%.1f\t%d\n", 0, 0., counts);

    for(i=1; i<mixStarts; i++)
    {
        startTime = Get_RunTime_of_MixSeq_Start(run_number, i);
        stopTime = Get_RunTime_of_MixSeq_Start(run_number, i+1);
        counts = Get_Total_Sis_Counts(run_number, SIS2_MIX_SEQ_TIME, startTime, stopTime);

        printf("%d\t%.1f\t%d\n", i, startTime, counts);
    }

    startTime = Get_RunTime_of_MixSeq_Start(run_number, i);
    counts = Get_Total_Sis_Counts(run_number, SIS2_MIX_SEQ_TIME, startTime, 1e7);
    printf("%d\t%.1f\t%d\n", i, startTime, counts);
}

void FindReadouts(Int_t runNumber, char* dumpName, Double_t lengthOverride=0.)
{
    Int_t dumpNumber = MatchDumpNameToDumpNumber(runNumber, dumpName);
    FindReadouts(runNumber, dumpNumber, lengthOverride);
}

void FindReadouts(Int_t runNumber, Int_t dumpNumber, Double_t lengthOverride=0.)
{
    Double_t startTime, endTime;

    GetDumpTimes(runNumber, 0, dumpNumber, &startTime, &endTime);

    if(lengthOverride!=0.) endTime = startTime+lengthOverride;

    if(lengthOverride<0)
    {
        endTime=startTime;
        startTime=endTime+lengthOverride;
        startTime=endTime+lengthOverride;
    }

    TTree* vtxTree = Get_Vtx_Tree(runNumber);

    sprintf(text, "RunTime>%.6lf && RunTime<%.6lf", startTime, endTime);
    vtxTree->Scan("RunNumber:RunTime:VF48NEvent:NVertices:NHits:NRawHits:NTracks", text);

    vtxTree->Delete();
}

void FindReadouts(char* filename, char* dumpName, Double_t lengthOverride=0.)
{
    Int_t runNumber[500], dumps[500];
    Int_t nRuns = FileToRuns(filename, runNumber, dumps);

    for(Int_t i=0; i<nRuns; i++)
    {
        dumps[i] = MatchDumpNameToDumpNumber(runNumber[i], dumpName);
    }

    for(Int_t i=0; i<nRuns; i++)
    {
        FindReadouts(runNumber[i], dumps[i], lengthOverride);
    }
}

TTree* GetSequencerTree(Int_t runNumber)
{
    TFile* runFile = Get_File(runNumber);
    printf( "%s",runFile->GetName());

    TTree* sequencerTree = (TTree*) runFile->Get("gSeqTree");
    //    if(sequencerTree==NULL) {
    //   TTree* sequencerTree = (TTree*) runFile->Get("Sequencer Event Tree");
    //    }
    if(sequencerTree !=NULL) printf("Seq tre is not null\n");
    if(sequencerTree==NULL)
    {
      //    Error("GetSequencerTree", "\033[31mTree file for run %d not found\033[00m", runNumber);
        //gSystem->Exit(1);
        sequencerTree->GetName(); // This is to crash the CINT interface  instead of exiting (deliberately)
    }


    return sequencerTree;
}

// ==================== FITTING ROUTINES (ART OLIN) ======================================================================

Double_t ringfun(Double_t *x, Double_t *par) {
// Art Olin
  Double_t r;
  const Double_t sigma=0.6;
  Double_t sigmasq;
  Double_t wall_rad=2.2;
  r= sqrt(x[0]*x[0] +x[1]*x[1]);
  sigmasq  = sigma*sigma;
  Double_t result = (par[0]*exp(-0.5*r*r/sigmasq) +par[1]*exp(-0.5*(r-wall_rad)*(r-wall_rad)/sigmasq)/(2*3.14159*wall_rad))/(sigma*sqrt(2*3.14159))+par[2]/100;
  result *=(VERTEX_RES_TRANSVERSE*VERTEX_RES_TRANSVERSE);
  return result;
}

TF2* ringfit(TH2D* top_level_xyvertices) {

 // Art Olin

   TH2D *xyvertices = (TH2D*) top_level_xyvertices);

   // draw a frame to define the range
   // TH2F *hr = c1->DrawFrame(-5,5,-5,5);
   // hr->SetXTitle("X   cm");
   // hr->SetYTitle("Y   cm");
   // c1->GetFrame()->SetFillColor(21);
   // c1->GetFrame()->SetBorderSize(12);

   const Int_t npar = 3;

   //Radial gaussians to fit
   TF2  *xy2 = new TF2("xy2",ringfun,-5,5,-5,5,npar);
   double cent = xyvertices->GetBinContent(xyvertices->FindBin(0.2,0.2));
   double wall = xyvertices->GetBinContent(xyvertices->FindBin(2.3,0.));
   cent = (int)cent ?  cent : 1;
   wall = (int)wall ? wall : 1.;
   Info("ringfit", "Starting values ampcent %i %f ampwall %i %f",xyvertices->FindBin(0.2,0.2),cent,xyvertices->FindBin(2.3,0.),wall);
   Info("ringfit", "Entries %f Integral %f chisq %f", xyvertices->GetEntries(), xyvertices->Integral(), xy2->GetChisquare());

   xy2->SetParameters(cent,wall,0.4,0.);

   if( xyvertices->Fit(xy2,"LN") !=0) {
     Warning("ringfit", "\033[31mRing fitting failed\033[00m");
     return;
   }

   Double_t apar[npar] , aperr[npar];
   for (int i=0; i <npar;i++) {
   apar[i] = xy2->TF2::GetParameter(i);
   aperr[i] = xy2->TF2::GetParError(i);
   }
   Info("ringfit", "Central Volume %10.8f +- %10.8f Ring Volume  %10.8f +-  %10.8f  Const %10.8f +- %10.8f", apar[0], aperr[0],apar[1],aperr[1],apar[2],aperr[2]);

 #ifdef DRAW_RING_FIT_PLOT
   TCanvas *c1 = new TCanvas("c1","ring fits",200,10,700,500);
   gStyle->SetOptStat(kTRUE);
   gStyle->SetPalette(1);

   c1->SetFillColor(42);
   c1->SetGrid();
   xyvertices->SetMarkerColor(kBlue);
   xyvertices->SetMarkerStyle(21);
    //   xy2->Draw("colz");
   xyvertices->DrawCopy("lego");

   xy2->SetLineColor(2);
   xy2->Draw("same");

   sprintf(text,"Central vol %6.2f +- %6.2f \n", apar[0], aperr[0]);
   AddRandomText(text, 1, 0.6,3);
   sprintf(text,"wall vol  %6.2f +-  %6.2f \n",apar[1],aperr[1]);
   AddRandomText(text, 1, 0.6,4);
   sprintf(text,"Const  %6.2f +- %6.2f \n", apar[2], aperr[2]);
   AddRandomText(text, 1, 0.6,5);
#endif
   return xy2;

}

Double_t radfun( Double_t *x, Double_t *par){
  Double_t r;
  Double_t wall_rad=2.2;
  r= x[0];
  Double_t result =par[0]*TMath::Gaus(r, 0.0,par[3],1)+par[1]*
TMath::Gaus(r, wall_rad, par[3],1) + par[2];

  return result;
}

Int_t radfit(TH1D* rvertices, Double_t *par,Double_t* perr) {
   //Radial gaussians to fit
   Double_t sigma=0.6;
   Double_t const pi = 3.141592653;
   Double_t wall_rad = 2.2;
   TF1  *r2 = new TF1("r2",radfun,0.,8.,4);

   Info("radfit", "Starting values area_cent %i %f area_wall %i %f", rvertices->FindBin(0.2), rvertices->GetBinContent(rvertices->FindBin(0.5)),rvertices->FindBin(2.3),rvertices->GetBinContent(rvertices->FindBin(2.3)));
   r2->SetParameters(rvertices->GetBinContent(rvertices->FindBin(0.2)),rvertices->GetBinContent(rvertices->FindBin(2.3)),1.0,sigma);
    //Comment following line to fit sigma.
   r2->FixParameter(3,sigma);
   Int_t result = rvertices->Fit(r2,"LL");
    if( result!=0) Warning("Radfit", "\033[31mRadial fitting failed\033[00m");


   TF1 *fit1 = (TF1*)rvertices->FindObject("r2");
   for (int i=0; i <4;i++) {
   par[i] = r2->TF1::GetParameter(i );
   perr[i] = r2->TF1::GetParError(i);
   }
   Info("RadFit", "Amplitudes 0 %f +- %f 1 %f +- %f 2 %f +- %f X2 %f", par[0], perr[0],par[1],perr[1],par[2], perr[2], TF1::r2->GetChisquare());
   //These fits are to a Gaussian radial density N(r)/r, different from the XY plots fitting N(r)
   //To get the counts from the fit one needs to integrate N(r)for each radfun
   //term, producing the coefficients below
   par[0]*=par[3]/sqrt(2*pi)/VERTEX_RES_RADIAL;
   par[1]*=(wall_rad+par[3]/sqrt(2*pi))/VERTEX_RES_RADIAL;
   par[2]*=8.0/VERTEX_RES_RADIAL;
   perr[0]*=par[3]/sqrt(2*pi)/VERTEX_RES_RADIAL;
   perr[1]*=(wall_rad+par[3]/sqrt(2*pi))/VERTEX_RES_RADIAL;
   perr[2]*=8.0/VERTEX_RES_RADIAL;
   Info("Radfit","Central area %10.6f +- %10.6f wall area  %10.6f +-  %10.6f  Const %10.6f +- %10.6f  Chisq  %10.6f", par[0], perr[0],par[1],perr[1],par[2], perr[2], TF1::r2->GetChisquare());

   return result;
}

// ==================== GET RUN TIME ======================================================================
// Most of the Get_RunTime macros stolen from James (with several modifications)

Double_t Get_RunTime_of_SequencerEvent(Int_t run_number, char* eventName, char* eventDescription, Int_t repetition=1)
{
    TSeq_Event* event = FindSequencerEvent(run_number, eventName, eventDescription, repetition);
    return Get_RunTime_of_SequencerEvent(run_number, event);

}

Double_t Get_RunTime_of_SequencerEvent(Int_t run_number, TSeq_Event* seqEvent, Int_t seqOffset=0)
{
    Int_t SEQ_TIMESTAMP_CHANNEL, SEQ_START_CHANNEL;

    if(!(seqEvent->GetSeq().CompareTo("PBAR", TString::kIgnoreCase)))
    {
            SEQ_TIMESTAMP_CHANNEL = SIS2_PBAR_SEQ_TIME;
            SEQ_START_CHANNEL = SIS2_PBAR_SEQ_START;
    }
    else
    {
            SEQ_TIMESTAMP_CHANNEL = SIS2_MIX_SEQ_TIME;
            SEQ_START_CHANNEL = SIS2_MIX_SEQ_START;
    }

        Double_t timeOfSequenceStart = Get_RunTime_of_Count(run_number, SEQ_START_CHANNEL, seqEvent->GetSeqNum()+seqOffset);

        //Int_t cumulativeCount = Get_Total_Sis_Counts_Speedy(run_number, SEQ_TIMESTAMP_CHANNEL, 0, timeOfSequenceStart); // this is from all the sequences in the chain which came before

        //cumulativeCount += seqEvent->GetonCount();

        Double_t time = Get_RunTime_of_Count(run_number, SEQ_TIMESTAMP_CHANNEL, seqEvent->GetonCount(), timeOfSequenceStart, 99999.);

        return time;
}

Double_t Get_RunTime_of_Count( TTree* tree, Int_t count )
{
  TSisEvent* sis_event = new TSisEvent();
  tree->SetBranchAddress("SisEvent", &sis_event );
  Int_t counts(0);
  Double_t time(-999);

   for( Int_t i=0; i<tree->GetEntries(); i++ )
    {
      tree->GetEntry(i);
      counts += sis_event->GetCountsInChannel();
      if( counts >= count )
        {
            time = sis_event->GetRunTime();
            break;
        }
    }

     #ifdef FREE_TREES
        delete sis_event;
        delete tree;
    #endif

   return time;
}

Double_t Get_RunTime_of_Count( TTree* tree, Int_t count, Double_t tmin, Double_t tmax )
{
    TSisEvent* sis_event = new TSisEvent();
    tree->SetBranchAddress("SisEvent", &sis_event );
    Int_t counts(0);

    sprintf(text, "RunTime>=%.6lf && RunTime<=%.6lf", tmin, tmax);
    Long64_t drawCounts = tree->Draw(">>countList", text);
    TEventList* countList = (TEventList*)gDirectory->Get("countList");

    if(countList==NULL)
    {
      //AO
      //Error("Get_RunTime_of_Count", "Eventlist for %s, count %d (%.3lf s,->%.3lf s) is NULL", tree->GetName(), count, tmin, tmax);
      printf("Get_RunTime_of_Count", "Eventlist for %s, count %d (%.3lf s,->%.3lf s) is NULL", tree->GetName(), count, tmin, tmax);
       return 0;
   }

    Double_t time;

    for( Int_t i=0; i<countList->GetN(); i++ )
    {
      tree->GetEntry(countList->GetEntry(i));
      counts += sis_event->GetCountsInChannel();
      if( counts >= count )
        {
            time = sis_event->GetRunTime();
            break;
        }
    }
     #ifdef FREE_TREES
        delete countList;
        delete sis_event;
        delete tree;
    #endif

   return time;
}

Double_t Get_RunTime_of_Count(Int_t run_number, Int_t channel, Int_t count)
{
    TTree* tree = Get_Sis_Tree( run_number, channel );
    if (tree==NULL) return -1.;

    Double_t run_time(-999.);
    run_time = Get_RunTime_of_Count( tree, count );

    if(run_time == -999.) Error("Get_RunTime_of_Count", "\033[33mCould not find runTime of trigger count %d in run %d, channel %d\033[00m", count, run_number, channel);

    return run_time;
}

Double_t Get_RunTime_of_Count(Int_t run_number, Int_t channel, Int_t count, Double_t tmin, Double_t tmax)
{
    TTree* tree = Get_Sis_Tree( run_number, channel );
    if (tree==NULL) return -1.;

    Double_t run_time(-999.);
    run_time = Get_RunTime_of_Count( tree, count, tmin, tmax );

    if(run_time == -999.) Error("Get_RunTime_of_Count", "\033[33mCould not find runTime of trigger count %d in run %d, channel %d\033[00m", count, run_number, channel);

    return run_time;

}

Int_t Get_Total_Sis_Counts( Int_t run_number, Int_t channel )
{
  TTree* tree = Get_Sis_Tree( run_number, channel );
  if(!tree) return -1;

  TSisEvent* sis_event = new TSisEvent;
  tree->SetBranchAddress("SisEvent", &sis_event );
  Int_t counts(0);

  for( Int_t i=0; i<tree->GetEntries(); i++ )
    {
      tree->GetEntry(i);
      counts += sis_event->GetCountsInChannel();
    }

    #ifdef FREE_TREES
    delete tree;
    #endif

    return counts;
}

Int_t Get_Total_Sis_Counts( Int_t run_number, Int_t channel, Double_t run_time_min, Double_t run_time_max )
{
  TTree* tree = Get_Sis_Tree( run_number, channel );
  if(!tree) return -1;

  sprintf(text, "RunTime>=%.6lf && RunTime<=%.6lf", run_time_min, run_time_max);
  tree->Draw(">>timeList", text);

  TEventList* entriesInRange = (TEventList*)gDirectory->Get("timeList");

  if(entriesInRange->GetN()==0)
  {
    delete entriesInRange;
    return 0;
  }

  TSisEvent* sis_event = new TSisEvent;
  tree->SetBranchAddress("SisEvent", &sis_event );

    Int_t counts(0), i(0);

    for(i=0; i<entriesInRange->GetN(); i++)
    {
      tree->GetEntry(entriesInRange->GetEntry(i));
      counts += sis_event->GetCountsInChannel();
    }
    delete entriesInRange;
    return counts;

    #ifdef FREE_TREES
        delete tree;
    #endif
}

Int_t Get_Total_Sis_Counts_Speedy(Int_t run_number, Int_t channel, Double_t run_time_min, Double_t run_time_max )
{
    TTree* tree = Get_Sis_Tree( run_number, channel );
    if(!tree) return -1;

    TSisEvent* sis_event = new TSisEvent;
    tree->SetBranchAddress("SisEvent", &sis_event );
    TEventList* entriesInRange;
    Int_t i(0), count(0);

    for(i=1; i<=SPEEDY_COUNTS_MAX; i++)
    {
        sprintf(text, "RunTime>=%.6lf && RunTime<=%.6lf && CountsInChannel==%d", run_time_min, run_time_max, i);
        tree->Draw(">>timeList", text);
        entriesInRange = (TEventList*)gDirectory->Get("timeList");
        count+=entriesInRange->GetN()*i;

    }

    sprintf(text, "RunTime>=%.6lf && RunTime<=%.6lf && CountsInChannel>=%d", run_time_min, run_time_max, i);
    tree->Draw(">>timeList", text);
    entriesInRange = (TEventList*)gDirectory->Get("timeList");

    for(i=0; i<entriesInRange->GetN(); i++)
    {
      tree->GetEntry(entriesInRange->GetEntry(i));
      count += sis_event->GetCountsInChannel();
    }

    delete entriesInRange;
    delete tree;
    return count;
}

Double_t Get_Total_RunTime(Int_t run_number)
{
    TTree* tree = Get_Sis_Tree(run_number,16); //50MhZ Clock
    if(!tree)
    {
        Error("Get_Total_RunTime", "\033[31mCould not open tree for run %d\033[00m", run_number);
        //gSystem->Exit(1);
        tree->GetName(); // This is to crash the CINT interface  instead of exiting (deliberately)
    }

    TSisEvent* sis_event = new TSisEvent;
    tree->SetBranchAddress("SisEvent", &sis_event );

    tree->GetEntry(tree->GetEntries()-1);
    Double_t totalTime = sis_event->GetRunTime();

    #ifdef FREE_TREES
    delete sis_event;
    delete tree;
    #endif

    return totalTime;
}

// ==================== LABVIEW STUFF ======================================================================
Double_t GetLabviewAtTime(Int_t runNumber, char* bankName, Int_t bankIndex, Double_t time, Int_t searchOption=1)
{
    /* Search Option
        0 = nearest to time
        1 = first after time
        -1 = first before time
    */

    TTree* lvTree = Get_LabVIEW_Tree(runNumber);
    TLabVIEWEvent* lvEvent = new TLabVIEWEvent();
    lvTree->SetBranchAddress("LabVIEWEvent", &lvEvent);

    Int_t lastFound(0), i, selectedEvent(0);
    Int_t totalEntries = lvTree->GetEntries();

    for(i=0; i<totalEntries; i++)
    {
            lvTree->GetEntry(i);
            if(lvEvent->GetArrayNumber()!=bankIndex) continue;
            if((lvEvent->GetBankName().CompareTo(bankName, TString::kIgnoreCase))) continue;
        //  Info("Debug", "Found matching event at t=%.3lf", lvEvent->GetTime());
            if(lvEvent->GetTime()>time) break;
            lastFound=i;
    }

    if(lastFound==0 && i==totalEntries)
    {
        Error("GetLabviewAtTime", "\033[33mCould not find matching event for %s[%d]\033[00m", bankName, bankIndex);
        return 0;
    }
    else if(i==totalEntries) // no Event after this time
    {
        selectedEvent=lastFound;
    }
    else if(lastFound==0) //no Event before this time
    {
        selectedEvent=i;
    }
    else
    {
        if(searchOption==0)
        {
            lvTree->GetEntry(lastFound);
            Double_t timeBefore = lvEvent->GetTime();
            lvTree->GetEntry(i);
            Double_t timeAfter = lvEvent->GetTime();

            if((timeAfter-time)<(time-timeBefore)) selectedEvent=i;
            else selectedEvent=lastFound;
        }
        else if(searchOption==-1)
        {
            selectedEvent=lastFound;
        }
        else
        {
            selectedEvent=i;
        }

    }

    lvTree->GetEntry(selectedEvent);
    Double_t selectedValue = lvEvent->GetValue();
    Info("GetLabviewAtTime","\033[32mFound event %s[%d] at time %.1f = %g\033[00m", bankName, bankIndex, lvEvent->GetTime(), selectedValue);

    delete lvTree;
    delete lvEvent;

    return selectedValue;

}

void PrintLabviewEvents(Int_t runNumber, char* bankName, Int_t bankIndex)
{
    TTree* lvTree = Get_LabVIEW_Tree(runNumber);
    TLabVIEWEvent* lvEvent = new TLabVIEWEvent();
    lvTree->SetBranchAddress("LabVIEWEvent", &lvEvent);

    for(Int_t i=0; i<lvTree->GetEntries(); i++)
    {
            lvTree->GetEntry(i);
            if(lvEvent->GetArrayNumber()!=bankIndex) continue;
            if((lvEvent->GetBankName().CompareTo(bankName, TString::kIgnoreCase))) continue;

            printf("%d %.1lf \t%s[%d]: %.3g\n", i, lvEvent->GetTime(), lvEvent->GetBankName().Data(), lvEvent->GetArrayNumber(), lvEvent->GetValue());
    }


}

// ==================== MISC ======================================================================

void SetDrawOpts(char* newOpts)
{
    sprintf(drawOpts, "%s", newOpts);
}

Int_t SetNormaliseFlag(Int_t normalise=0)
{
    gNormaliseOverlay=normalise;
    return gNormaliseOverlay;
}

void SetVertexRes(char* command=NULL)
{
    if(command==NULL)
    {
        VERTEX_RES_Z=0.25;
        VERTEX_RES_RADIAL=0.1;
        VERTEX_RES_TRANSVERSE=0.2;
        VERTEX_RES_PHI=5.;
        NUMBER_BINS=100;
    }
    else if(strcmp(command, "default")==0)
    {
        VERTEX_RES_Z=0.25;
        VERTEX_RES_RADIAL=0.1;
        VERTEX_RES_TRANSVERSE=0.2;
        VERTEX_RES_PHI=5.;
        NUMBER_BINS=100;
    }
    else if(strcmp(command, "hires")==0)
    {
        VERTEX_RES_Z=0.1;
        VERTEX_RES_RADIAL=0.1;
        VERTEX_RES_TRANSVERSE=0.1;
        VERTEX_RES_PHI=5;
        NUMBER_BINS=200;
    }
    else if(strcmp(command, "lores")==0)
    {
        VERTEX_RES_Z=1.;
        VERTEX_RES_RADIAL=0.5;
        VERTEX_RES_TRANSVERSE=0.5;
        VERTEX_RES_PHI=30;
        NUMBER_BINS=50;
    }
}

TH1D* NormaliseHistoTimeAxis(TH1D* histo, Double_t ignoreStart=0., Double_t ignoreEnd=0., Int_t ConserveArea=0)
{
    Double_t totalTime = histo->GetXaxis()->GetXmax()-histo->GetXaxis()->GetXmin();
    Double_t renormalisedTime=totalTime-ignoreStart-ignoreEnd;

    histo = RebinHistoToNBins(histo, NUMBER_BINS*10); // speeds it up A LOT

    Double_t normalised;

    TH1D* newHisto = new TH1D(histo->GetName(), histo->GetTitle(), NUMBER_BINS, 0., 1.);

    for(Int_t i=1; i<=histo->GetNbinsX(); i++)
    {
        normalised = (histo->GetBinCenter(i)-ignoreStart)/renormalisedTime;
        newHisto->Fill(normalised, histo->GetBinContent(i));
    }

    newHisto->SetYTitle("Counts");
    newHisto->SetXTitle("Normalised Time");

    if(ConserveArea)
    {
        newHisto->Scale(renormalisedTime);
        newHisto->SetYTitle("Normalised Rate");
    }



    delete histo;
    return newHisto;
}

void PlotHistoByTimes(Int_t runNumber, Int_t DetectorChannel, Double_t StartTime, Double_t EndTime)
{
  gStyle->SetOptStat(00000000);
  sprintf(text, "Run %d, Channel %d : start time = %f and end time = %f", runNumber, DetectorChannel, StartTime, EndTime);
  TCanvas* c= new TCanvas(text, text, 800, 600);

  TH1D* histo =  GetHistoByTimes(runNumber, DetectorChannel, StartTime, EndTime);
  histo->SetTitle( text );

  c->cd();
  histo->Draw();
  AddIntegralText( histo );
}

int DrawTrapEdges(TH2D* theHist)
{
  double height = 0.9*(theHist->GetYaxis()->GetXmax()-theHist->GetYaxis()->GetXmin())+theHist->GetYaxis()->GetXmin();
  double heightscale = 1.0;
    double textoffset(1.8);
  double textsize(0.03);

  double offset(8.); // global trap offset relative to silicon position

  TLine* l12 = new TLine( 3.2 + offset,theHist->GetYaxis()->GetXmin(), 3.2 + offset, theHist->GetYaxis()->GetXmax() ); // lower edge (LE) E25
  l12->SetLineColor(10);
  l12->SetLineStyle(3);
  l12->Draw();
   TLatex* t4 = new TLatex( 1.2 + offset + textoffset, height*heightscale, "E24" );
  t4->SetTextSize(textsize);
  t4->SetTextColor(10);
  t4->SetTextAngle(90);
  t4->Draw();

  TLine* l10b = new TLine(-24.4 + offset,theHist->GetYaxis()->GetXmin(), -24.4 + offset, theHist->GetYaxis()->GetXmax() ); // lower edge (LE) E12
  l10b->SetLineColor(10);
  l10b->SetLineStyle(3);
  l10b->Draw();
  TLatex* t2 = new TLatex( -24.4 + offset + textoffset, height*heightscale, "E12" );
  t2->SetTextSize(textsize);
  t2->SetTextColor(10);
  t2->SetTextAngle(90);
  t2->Draw();

  TLine* l6 = new TLine( -10.4 + offset,theHist->GetYaxis()->GetXmin(),-10.4 + offset, theHist->GetYaxis()->GetXmax() ); // lower edge (LE) E18
  l6->SetLineColor(10);
  l6->SetLineStyle(3);
  l6->Draw();

  return 0;



}

int DrawTrapPosition(TH1D * theHist)
{
  double height = theHist->GetMaximum();
  double heightscale = 0.9;

  double offset(8.); // global trap offset relative to silicon position
  double textoffset(1.8);
  double textsize(0.03);

  TLatex* t1 = new TLatex( -1.4 + offset + textoffset, height*heightscale, "E23" );
  t1->SetTextSize(textsize);
  t1->SetTextColor(2);
  t1->SetTextAngle(90);
  t1->Draw();
  TLine* l1 = new TLine( -1.4 + offset, 0, -1.4 + offset, height ); // lower edge (LE) E23
  l1->SetLineColor(2);
  l1->SetLineStyle(3);
  l1->Draw();
  TLatex* t2 = new TLatex( -3.4 + offset + textoffset, height*heightscale, "E22" );
  t2->SetTextSize(textsize);
  t2->SetTextColor(2);
  t2->SetTextAngle(90);
  t2->Draw();
  TLine* l2 = new TLine( -3.4 + offset, 0, -3.4 + offset, height ); // lower edge (LE) E22
  l2->SetLineColor(2);
  l2->SetLineStyle(3);
  l2->Draw();
  TLatex* t3 = new TLatex( -5.4 + offset + textoffset, height*heightscale, "E21" );
  t3->SetTextSize(textsize);
  t3->SetTextColor(2);
  t3->SetTextAngle(90);
  t3->Draw();
  TLine* l3 = new TLine( -5.4 + offset, 0, -5.4 + offset, height ); // lower edge (LE) E21
  l3->SetLineColor(2);
  l3->SetLineStyle(3);
  l3->Draw();
  TLatex* t4 = new TLatex( -7.4 + offset + textoffset, height*heightscale, "E20" );
  t4->SetTextSize(textsize);
  t4->SetTextColor(2);
  t4->SetTextAngle(90);
  t4->Draw();
  TLine* l4 = new TLine( -7.4 + offset, 0, -7.4 + offset, height ); // lower edge (LE) E20
  l4->SetLineColor(2);
  l4->SetLineStyle(3);
  l4->Draw();
  TLatex* t5 = new TLatex( -9.4 + offset + textoffset, height*heightscale, "E19" );
  t5->SetTextSize(textsize);
  t5->SetTextColor(2);
  t5->SetTextAngle(90);
  t5->Draw();
  TLine* l5 = new TLine( -9.4 + offset, 0, -9.4 + offset, height ); // lower edge (LE) E19
  l5->SetLineColor(2);
  l5->SetLineStyle(3);
  l5->Draw();
  TLatex* t6 = new TLatex( -11.4 + offset + textoffset, height*heightscale, "E18" );
  t6->SetTextSize(textsize);
  t6->SetTextColor(2);
  t6->SetTextAngle(90);
  t6->Draw();
  TLine* l6 = new TLine( -11.4 + offset, 0, -11.4 + offset, height ); // lower edge (LE) E18
  l6->SetLineColor(2);
  l6->SetLineStyle(3);
  l6->Draw();
  TLatex* t7 = new TLatex( -13.4 + offset + textoffset, height*heightscale, "E17" );
  t7->SetTextSize(textsize);
  t7->SetTextColor(2);
  t7->SetTextAngle(90);
  t7->Draw();
  TLine* l7 = new TLine( -13.4 + offset, 0, -13.4 + offset, height ); // lower edge (LE) E17
  l7->SetLineColor(2);
  l7->SetLineStyle(3);
  l7->Draw();
  TLatex* t8 = new TLatex( -15.4 + offset + textoffset, height*heightscale, "E16" );
  t8->SetTextSize(textsize);
  t8->SetTextColor(2);
  t8->SetTextAngle(90);
  t8->Draw();
  TLine* l8 = new TLine( -15.4 + offset, 0, -15.4 + offset, height ); // lower edge (LE) E16
  l8->SetLineColor(2);
  l8->SetLineStyle(3);
  l8->Draw();
  TLatex* t9 = new TLatex( -17.4 + offset + textoffset, height*heightscale, "E15" );
  t9->SetTextSize(textsize);
  t9->SetTextColor(2);
  t9->SetTextAngle(90);
  t9->Draw();
  TLine* l9 = new TLine( -17.4 + offset, 0, -17.4 + offset, height ); // lower edge (LE) E15
  l9->SetLineColor(2);
  l9->SetLineStyle(3);
  l9->Draw();
  TLatex* t10 = new TLatex( -19.4 + offset + textoffset, height*heightscale, "E14" );
  t10->SetTextSize(textsize);
  t10->SetTextColor(2);
  t10->SetTextAngle(90);
  t10->Draw();
  TLine* l10 = new TLine( -19.4 + offset, 0, -19.4 + offset, height ); // lower edge (LE) E14
  l10->SetLineColor(2);
  l10->SetLineStyle(3);
  l10->Draw();
  TLatex* t10a = new TLatex( -21.4 + offset + textoffset, height*heightscale, "E13" );
  t10a->SetTextSize(textsize);
  t10a->SetTextColor(2);
  t10a->SetTextAngle(90);
  t10a->Draw();
  TLine* l10a = new TLine( -21.4 + offset, 0, -21.4 + offset, height ); // lower edge (LE) E14
  l10a->SetLineColor(2);
  l10a->SetLineStyle(3);
  l10a->Draw();
  TLatex* t10b = new TLatex( -24.4 + offset + textoffset, height*heightscale, "E12" );
  t10b->SetTextSize(textsize);
  t10b->SetTextColor(2);
  t10b->SetTextAngle(90);
  t10b->Draw();
  TLine* l10b = new TLine( -24.4 + offset, 0, -24.4 + offset, height ); // lower edge (LE) E14
  l10b->SetLineColor(2);
  l10b->SetLineStyle(3);
  l10b->Draw();
  TLatex* t11 = new TLatex( 0.6 + offset + textoffset, height*heightscale, "E24" );
  t11->SetTextSize(textsize);
  t11->SetTextColor(2);
  t11->SetTextAngle(90);
  t11->Draw();
  TLine* l11 = new TLine( 0.6 + offset, 0, 0.6 + offset, height ); // lower edge (LE) E24
  l11->SetLineColor(2);
  l11->SetLineStyle(3);
  l11->Draw();
  TLatex* t12 = new TLatex( 3.2 + offset + textoffset, height*heightscale, "E25" );
  t12->SetTextSize(textsize);
  t12->SetTextColor(2);
  t12->SetTextAngle(90);
  t12->Draw();
  TLine* l12 = new TLine( 3.2 + offset, 0, 3.2 + offset, height ); // lower edge (LE) E25
  l12->SetLineColor(2);
  l12->SetLineStyle(3);
  l12->Draw();
  TLatex* t13 = new TLatex( 5.9 + offset + textoffset, height*heightscale, "E26" );
  t13->SetTextSize(textsize);
  t13->SetTextColor(2);
  t13->SetTextAngle(90);
  t13->Draw();
  TLine* l13 = new TLine( 5.9 + offset, 0, 5.9 + offset, height ); // lower edge (LE) E26
  l13->SetLineColor(2);
  l13->SetLineStyle(3);
  l13->Draw();
  TLatex* t14 = new TLatex( 8.1 + offset + textoffset, height*heightscale, "E27" );
  t14->SetTextSize(textsize);
  t14->SetTextColor(2);
  t14->SetTextAngle(90);
  t14->Draw();
  TLine* l14 = new TLine( 8.1 + offset, 0, 8.1 + offset, height ); // lower edge (LE) E27
  l14->SetLineColor(2);
  l14->SetLineStyle(3);
  l14->Draw();
  TLatex* t15 = new TLatex( 11.0 + offset + textoffset, height*heightscale, "E28" );
  t15->SetTextSize(textsize);
  t15->SetTextColor(2);
  t15->SetTextAngle(90);
//  t15->Draw();
  TLine* l15 = new TLine( 11.0 + offset, 0, 11.0 + offset, height ); // lower edge (LE) E28
  l15->SetLineColor(2);
  l15->SetLineStyle(3);
  l15->Draw();
  TLatex* t16 = new TLatex( 13.9 + offset + textoffset, height*heightscale, "E29" );
  t16->SetTextSize(textsize);
  t16->SetTextColor(2);
  t16->SetTextAngle(90);
 //t16->Draw();
  TLine* l16 = new TLine( 13.9 + offset, 0, 13.9 + offset, height ); // lower edge (LE) E29
  l16->SetLineColor(2);
  l16->SetLineStyle(3);
 // l16->Draw();
  TLatex* t17 = new TLatex( 16.1 + offset + textoffset, height*heightscale, "E30" );
  t17->SetTextSize(textsize);
  t17->SetTextColor(2);
  t17->SetTextAngle(90);
//  t17->Draw();
  TLine* l17 = new TLine( 16.1 + offset, 0, 16.1 + offset, height ); // lower edge (LE) E30
  l17->SetLineColor(2);
  l17->SetLineStyle(3);
 // l17->Draw();
  TLatex* t18 = new TLatex( 17.6 + offset + textoffset, height*heightscale, "E31" );
  t18->SetTextSize(textsize);
  t18->SetTextColor(2);
  t18->SetTextAngle(90);
//  t18->Draw();
  TLine* l18 = new TLine( 17.6 + offset, 0, 17.6 + offset, height ); // lower edge (LE) E31
  l18->SetLineColor(2);
  l18->SetLineStyle(3);
//  l18->Draw();
  TLatex* t19 = new TLatex( 19.1 + offset + textoffset, height*heightscale, "E32" );
  t19->SetTextSize(textsize);
  t19->SetTextColor(2);
  t19->SetTextAngle(90);
//  t19->Draw();
  TLine* l19 = new TLine( 19.1 + offset, 0, 19.1 + offset, height ); // lower edge (LE) E32
  l19->SetLineColor(2);
  l19->SetLineStyle(3);
//  l19->Draw();
  TLatex* t03 = new TLatex( -34.3 + offset + textoffset, height*heightscale, "E9" );
  t03->SetTextSize(0.03);
  t03->SetTextColor(2);
  t03->SetTextAngle(90);
  //t03->Draw();
  TLine* l03 = new TLine( -34.3 + offset, 0, -34.3 + offset, height ); // lower edge (LE) E09
  l03->SetLineColor(2);
  l03->SetLineStyle(3);
 // l03->Draw();
  TLatex* t02 = new TLatex( -29.3 + offset + textoffset, height*heightscale, "E10" );
  t02->SetTextSize(0.03);
  t02->SetTextColor(2);
  t02->SetTextAngle(90);
  //t02->Draw();
  TLine* l02 = new TLine( -29.3 + offset, 0, -29.3 + offset, height ); // lower edge (LE) E10
  l02->SetLineColor(2);
  l02->SetLineStyle(3);
  //l02->Draw();
  TLatex* t01 = new TLatex( -27.1 + offset + textoffset, height*heightscale, "E11" );
  t01->SetTextSize(0.03);
  t01->SetTextColor(2);
  t01->SetTextAngle(90);
  t01->Draw();
  TLine* l01 = new TLine( -27.1 + offset, 0, -27.1 + offset, height ); // lower edge (LE) E11
  l01->SetLineColor(2);
  l01->SetLineStyle(3);
  l01->Draw();


  return 0;
}
void MakeBlueBackground( TH2D* the_plot )
{
  // zphi background color
  Int_t numXbins = the_plot->GetXaxis()->GetNbins();
  Int_t numYbins = the_plot->GetYaxis()->GetNbins();

  for(Int_t x=0; x<=numXbins; x++ )
    {
      for(Int_t y=0; y<=numYbins; y++)
        {
          if( the_plot->GetBinContent( x, y ) == 0. ) the_plot->SetBinContent( x, y, 1e-7 );
        }
    }
}

Int_t SetTimeBins(Int_t nBins)
{
    return SetNBins(nBins);
}

void PlotMixingDumpAdd(Int_t runNumber, Int_t chainNumber, Int_t mixingDump, Int_t nDetectors, Int_t* detectors, Double_t transformerValue=0., Int_t rep=1)
{
    gStyle->SetOptStat(goptStat);
    for(Int_t i=0; i<nDetectors; i++)
    {
        if(i==0) TH1D* mixingHistogram = GetDumpHisto(runNumber, chainNumber, mixingDump, detectors[i]);
        else mixingHistogram->Add(GetDumpHisto(runNumber, chainNumber, mixingDump, detectors[i]));
    }
    if(TRANSFORMER_NORMALISED>0. && transformerValue>0.) mixingHistogram->Scale(TRANSFORMER_NORMALISED/transformerValue);

    Int_t mixingIntegral = mixingHistogram->Integral();
    ScaleAndRebinHistogram(mixingHistogram);

    #ifdef MIXING_LOG
        mixingHistogram->GetYaxis()->SetRangeUser(0.5, mixingHistogram->GetMaximum());
    #endif

    mixingHistogram->SetLineColor(rep+(rep>4));
    sprintf(text, "Mixing, Run %d", runNumber);
    mixingHistogram->SetTitle(text);
    sprintf(text, "%sSAME", drawOpts);
    mixingHistogram->DrawCopy(text);

    sprintf(text, "TRAFO %.2lf normalised to %.2lf\n", transformerValue, TRANSFORMER_NORMALISED);
    if(TRANSFORMER_NORMALISED>0.) AddRandomText(text, 4, 0.5, 0);

    if(nDetectors==2)
    {
        if((detectors[0]==61 && detectors[1]==48) || (detectors[1]==61 && detectors[0]==48)) sprintf(text, "TTC 3+6: %d", mixingIntegral);
        else sprintf(text,"Integral: %d", mixingIntegral);
    }
    else sprintf(text,"Integral: %d", mixingIntegral);
    AddRandomText(text, rep, 0.7, rep);

}


Int_t SetNBins(Int_t NBins)
{
    NUMBER_BINS=NBins;
}
