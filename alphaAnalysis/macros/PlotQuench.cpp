#include "PlotMixing.cpp"

Int_t PrintQuenchGateWindow( Int_t runNumber )
{
  TTree* sis_quench_trigger_tree =  Get_Sis_Tree( runNumber, 6 );
  //TTree* sis_quench_trigger_tree =  Get_Sis_Tree( runNumber, 28 );
  if( sis_quench_trigger_tree == NULL ) return -1;

  if(sis_quench_trigger_tree->GetEntries()<1) return -1;

  Double_t sis_quench_trigger_time = Get_RunTime_of_Count( sis_quench_trigger_tree, 1 );

  TTree* sis_tree = Get_Sis_Tree( runNumber, 51 );
  if( sis_tree == NULL ) return -1;
  TSisEvent* sis_event = new TSisEvent();
  sis_tree->SetBranchAddress("SisEvent", &sis_event );

  printf("SIS Events ******************************************************\n");

  for( Int_t i=0; i<sis_tree->GetEntries(); i++ )
    {
      sis_event->ClearSisEvent();
      sis_tree->GetEntry(i);

      if(sis_event->GetRunTime() < sis_quench_trigger_time ) continue;
      if( ((sis_event->GetRunTime() - sis_quench_trigger_time)*1000.) > 100. ) break;

      printf("Sis trigger %d at (runtime - quench trigger time) = %4.3f ms \t with %d count(s) \n",
             sis_event->GetChannel(),
             (sis_event->GetRunTime() - sis_quench_trigger_time)*1000.,
             sis_event->GetCountsInChannel() );
    }

  printf("\n");
  printf("Silicon Events **************************************************\n");

  TTree* sil_tree = Get_Vtx_Tree( runNumber );
 TVector3* vertex;
  if( sil_tree == NULL ) return -1;
  TSiliconEvent* sil_event = new TSiliconEvent();
  sil_tree->SetBranchAddress("SiliconEvent", &sil_event);

   for( Int_t i=0; i<sil_tree->GetEntries(); i++ )
    {
     sil_event->ClearEvent();
      sil_tree->GetEntry(i);
      if( sil_event->GetRunTime() < sis_quench_trigger_time ) continue;
      if( ((sil_event->GetRunTime() - sis_quench_trigger_time)*1000.) > 100. ) break;

      printf("Silicon event at (runtime - quench trigger time)/ms = %4.3f, VF48NEvent = %d, VF48Timestamp = %4.5f, Vertices = %d, NRawHits = %d, NHits = %d\n",
             (sil_event->GetRunTime() - sis_quench_trigger_time)*1000.,
             sil_event->GetVF48NEvent(),
             sil_event->GetVF48Timestamp(),
             sil_event->GetNVertices(),
             sil_event->GetNRawHits(),
             sil_event->GetNHits());
    if(sil_event->GetNVertices()>0)
    {
    vertex = sil_event->GetVertex();
    printf("Vertex (%d tracks) at X: %.2lf, Y:%.2lf, Z:%.2lf, R:%.2lf, Phi:%.1lf, Residual %.3lf\n", sil_event->GetNTracks(), vertex->X(), vertex->Y(), -vertex->Z(), sqrt(vertex->X()**2+vertex->Y()**2), vertex->Phi()/3.14159*180., sil_event->GetResidual());
    }
    }

  printf("\n");
  printf("TTC Events *****************************************************\n");

  TTree* ttc_tree = Get_TTC_Tree( runNumber );
  TTCEvent* ttc_event = new TTCEvent();
  ttc_tree->SetBranchAddress( "TTCEvent", &ttc_event );

for(Int_t i=0; i<ttc_tree->GetEntries(); i++)
    {
      ttc_event->ClearTTCEvent();
      ttc_tree->GetEntry(i);
      if( ttc_event->GetRunTime() < sis_quench_trigger_time ) continue;
      if( (ttc_event->GetRunTime() - sis_quench_trigger_time)*1000. > 100. ) break;
      printf("TTC event at (runtime - quench trigger time)/ms = %4.3f \n", (ttc_event->GetRunTime() - sis_quench_trigger_time)*1000 );
    }

  for(Int_t i=0; i<ttc_tree->GetEntries(); i++)
    {
      ttc_tree->GetEntry(i);
      if( ttc_event->GetRunTime() < sis_quench_trigger_time ) continue;
      if( (ttc_event->GetRunTime() - sis_quench_trigger_time)*1000. > 100. ) break;
      GetNumberOfTAs( ttc_event );
    }

  return 0;
}

Int_t GetNumberOfTAs( Int_t runNumber )
{
  Int_t count_TAs(0);

  TTree* ttc_tree = Get_TTC_Tree( runNumber );

  TTCEvent* ttc_event = new TTCEvent();
  ttc_tree->SetBranchAddress( "TTCEvent", &ttc_event );

  //  for(Int_t i=0; i<ttc_tree->GetEntries(); i++)
  for(Int_t i=0; i<1; i++)
    {
      ttc_tree->GetEntry(i);
      GetNumberOfTAs( ttc_event );
    }



  return count_TAs;

}

Int_t GetNumberOfTAs( TTCEvent* ttc_event )
{
  // recall; one TTCEvent / FPGA

  Int_t count_TAs(0);

  Int_t TAs[16];

  printf("TAs at time = %f\n", ttc_event->GetRunTime() );

  // loop over banks
  for( Int_t BankNumber=0; BankNumber <8; BankNumber++ )
    {
      uint16_t latch = ttc_event->GetLatch( BankNumber );

      uint16_t compare = 1;
      int counter = 0 ;
      do
        {
          if ( latch & compare  )
            {
              TAs[counter] = 1  ;
            }
          else     TAs[counter] = 0 ;

          compare = compare << 1 ;

          //printf("Latch = %04x, Counter = %d, TA = %d \n", latch, counter, TAs[counter] );

          counter++ ;
        }while(counter < 16 ) ;


      Int_t TAnumber;
      for( Int_t TA=0; TA<16; TA++ )
        {
          TAnumber = TA + 16*BankNumber + 128*ttc_event->GetFPGA();
          if( TA==0 || TA==3 || TA ==4 || TA ==7 || TA==8 || TA==11 || TA==12 || TA==15 ) continue;
          if( TAs[TA]==1 ) printf("%d \t ", TAnumber );
          //if( TAs[TA]==1 ) thePlot->Fill( TAnumber );
          count_TAs++;
        }

      for(Int_t i=0; i<16; i++ ) TAs[i]=0;
    }//loop over banks

  printf("\n\n");

  return count_TAs;
}
