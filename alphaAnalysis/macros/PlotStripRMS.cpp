// Note : For Module # input Modules are # from 1,...,60.

TGraph* PlotStripRMS( Int_t run_number, Int_t module_number, Int_t asic_number )
{
  TString file("../data/alphaStrips");
  file+=(run_number);
  file+=("offline.root");
  
  TFile* f = new TFile(file);

  TTree* t = (TTree*) f->Get("alphaStrip Tree");
  Int_t stripNumber;
  Float_t stripMean;
  Float_t stripRMS;
  Float_t stripMeanSubRMS;
  t->SetBranchAddress( "stripNumber",     &stripNumber );
  t->SetBranchAddress( "stripMean",       &stripMean );
  //  t->SetBranchAddress( "stripRMS",        &stripRMS );
  t->SetBranchAddress( "stripMeanSubRMS",        &stripRMS );
  //  t->SetBranchAddress( "stripMeanSubRMS", &stripMeanSubRMS );

  Float_t strip_numbers[128];
  for(Int_t i=0; i<128; i++) strip_numbers[i]= (Float_t)i+1.;
  Float_t strip_rms[128];

  Int_t first_strip = 512*(module_number-1) + 128*(asic_number-1) + 0;
  Int_t last_strip = 512*(module_number-1) + 128*(asic_number-1) + 128;
  
  for(Int_t i=0; i<t->GetEntries(); i++)
    {
      t->GetEntry(i);
      if( stripNumber < first_strip ) continue;
      if( stripNumber >= last_strip ) break; 

      //      printf( "%d \t %f \t %f \t %f \n", stripNumber, stripMean, stripRMS, stripMeanSubRMS );
     
      strip_rms[ (stripNumber - first_strip) ] = stripRMS;
    }
  
  TGraph* graph = new TGraph( 128, strip_numbers, strip_rms );
  graph->GetYaxis()->SetRangeUser(0.,30.);
  graph->GetXaxis()->SetRangeUser(0.,130.);
  graph->GetXaxis()->SetTitle("strip number");
  graph->SetMarkerStyle(2);
  
  char title[80];
  sprintf( title, "Run %d Module %d Asic %d", run_number, module_number, asic_number );
  graph->SetTitle(title);
  
  return graph;
}

// TGraph* PlotStripMeanSubRMS( Int_t run_number, Int_t module_number, Int_t asic_number )
// {
//   TString file("../data/alphaStrips");
//   file+=(run_number);
//   file+=("offline.root");

//   TFile* f = new TFile(file);
//   TTree* t = (TTree*) f->Get("alphaStrip Tree");

//   Float_t siliconRMS = 0.;
//   t->SetBranchAddress("stripMeanSubRMS", &siliconRMS );
//   Double_t strip_numbers[128];
//   for(Int_t i=0; i<128; i++) strip_numbers[i]= (Double_t)i+1.;
//   Double_t strip_rms[128];

//   if( asic_number > 3 )
//     {
//       Error("PlotStripMeanSumRMS","asic_number should be < 3");
//       return (TGraph*) NULL;
//     }

//   for(Int_t i=0; i<128; i++)
//     {
//       Int_t stripNumber = i + 128*(asic_number-0) + 512*(module_number-1);

//       t->GetEntry(stripNumber);
//       strip_rms[i]=siliconRMS;
//     }

//   TGraph* graph = new TGraph( 128, strip_numbers, strip_rms );
//   graph->GetYaxis()->SetRangeUser(0.,50.);
//   graph->GetXaxis()->SetRangeUser(0.,130.);
//   graph->SetMarkerStyle(2);

//   char title[80];
//   sprintf( title, "Run %d Module %d Asic %d", run_number, module_number, asic_number );
//   graph->SetTitle(title);

//   return graph;
// }

// TGraph* PlotMeans( Int_t run_number, Int_t module_number, Int_t asic_number )
// {
//   TString file("../data/alphaStrips");
//   file+=(run_number);
//   file+=("offline.root");

//   TFile* f = GetFile(file);
//   if(!f) return (TGraph*) NULL;

//   TTree* t = (TTree*) f->Get("alphaStrip Tree");

//   Float_t siliconMean = 0.;
//   t->SetBranchAddress("stripMean", &siliconMean );
//   Double_t strip_numbers[128];
//   for(Int_t i=0; i<128; i++) strip_numbers[i]= (Double_t)i+1.;
//   Double_t strip_means[128];

//   if( asic_number > 3 )
//     {
//       Error("PlotStripMeans","asic_number should be < 3");
//       return (TGraph*) NULL;
//     }

//   for(Int_t i=0; i<128; i++)
//     {
//       Int_t stripNumber = i + 128*(asic_number-0) + 512*(module_number-1);

//       t->GetEntry(stripNumber);
//       strip_means[i]=siliconMean;
//     }

//   TGraph* graph = new TGraph( 128, strip_numbers, strip_means );
//   graph->GetYaxis()->SetRangeUser(-50.,50.);
//   graph->GetXaxis()->SetRangeUser(0.,130.);
//   graph->SetMarkerStyle(2);

//   char title[80];
//   sprintf( title, "Run %d Module %d Asic %d", run_number, module_number, asic_number );
//   graph->SetTitle(title);

//   f->Close();

//   return graph;
// }

TFile * GetFile(const char * filename)
{
  // Check for open files
  Int_t nfiles = gROOT->GetListOfFiles()->GetEntries();

  for( Int_t i = 0; i < nfiles; i++ )
    {
      TFile * f = (TFile*)gROOT->GetListOfFiles()->At(i);
      if( strcmp( filename, f->GetName() ) == 0 ) // success
        {
          return f;
        }
    }

  // nope, have to open it
  
  TFile * f = new TFile( filename );
  if( !f->IsOpen() )
    {
      delete f;
      return (TFile*) NULL;
    }
  else
    return f;

  // epic failure
  delete f;
  return (TFile*) NULL;
}

Double_t GetStripMean( Int_t run_number, Int_t module_number, Int_t asic_number, Int_t strip )
{
  TString file("../data/alphaStrips");
  file+=(run_number);
  file+=("offline.root");

  TFile* f = GetFile(file);
  if( !f )
    return -9999.;

  TTree* t = (TTree*) f->Get("alphaStrip Tree");
  if( !t )
    {
      printf("runnumber: %d\n",run_number);
      return -9999.;
    }

  Float_t stripMean = 0.;
  t->SetBranchAddress("stripMean", &stripMean );
  
  if( asic_number > 3 )
    {
      Error("PlotStripMeans","asic_number should be < 3");
      return (TGraph*) NULL;
    }

  Int_t stripNumber = strip + 128*(asic_number-0) + 512*(module_number-1);
  t->GetEntry(stripNumber);

  return stripMean;
}

Double_t GetStripMeanError( Int_t run_number, Int_t module_number, Int_t asic_number, Int_t strip )
{
  TString file("../data/alphaStrips");
  file+=(run_number);
  file+=("offline.root");

  TFile* f = GetFile(file);
  if( !f )
    return -9999.;

  TTree* t = (TTree*) f->Get("SiliconStripRMS Tree");
  if( !t )
    {
      printf("runnumber: %d\n",run_number);
      return -9999.;
    }

  TSiliconStripRMS* siliconRMS = new TSiliconStripRMS();
  t->SetBranchAddress("SiliconStripRMS", &siliconRMS );

  Double_t stripRMS = 0.;
  Double_t n = 0.;

  if( asic_number > 3 )
    {
      Error("PlotStripMeanError","asic_number should be < 3");
      return (TGraph*) NULL;
    }

  Int_t stripNumber = strip + 128*(asic_number-0) + 512*(module_number-1);
  t->GetEntry(stripNumber);

  stripRMS = siliconRMS->GetStripRMS();
  n = siliconRMS->GetNumberEvents();
  if(n==0) return 0;

  return stripRMS/sqrt((double)n);
}

Double_t Mean( TArrayD * array )
{
  Double_t sum = 0;
  for( Int_t i=0; i<array->GetSize(); i++ )
    {
      Double_t element = array->At(i);
      sum += element;
    }
  return sum/((Double_t)array->GetSize());
}

Double_t StdDev( TArrayD * array )
{
  Int_t sum0 = 0;
  Double_t sum1 = 0.;
  Double_t sum2 = 0.;

  for( Int_t i=0; i<array->GetSize(); i++ )
    {
      Double_t element = array->At(i);
      sum0 ++;
      sum1 += element;
      sum2 += element*element;
    }
  Double_t var = ( (Double_t) sum2 / (Double_t) sum0 ) - ( sum1 / (Double_t) sum0 ) * ( sum1 / (Double_t) sum0 );

  Double_t sd =0;
  if( var > 0. ) sd = sqrt(var);
  
  return sd;
}

void Get_Mean_ASIC_RMS( Int_t run_number, Int_t module_number, Int_t asic_number, Double_t & ASIC_Mean, Double_t & ASIC_RMS )
{
  TString file("../data/alphaStrips");
  file+=(run_number);
  file+=("offline.root");
  
  TFile* f = GetFile(file);
  if( !f )
    return -9999.;

  TTree* t = (TTree*) f->Get("alphaStrip Tree");
  Int_t stripNumber;
  Float_t stripRMS;
  Float_t stripMeanSubRMS;
  t->SetBranchAddress( "stripNumber",     &stripNumber );
  t->SetBranchAddress( "stripMeanSubRMS",        &stripRMS );

  Int_t first_strip = 512*(module_number-1) + 128*(asic_number-1) + 0;
  Int_t last_strip = 512*(module_number-1) + 128*(asic_number-1) + 128;

  Double_t sum0 = 0.;
  Double_t sum1 = 0.;
  Double_t sum2 = 0.;
  Double_t ASIC_Var = 0.;

  for(Int_t i=0; i<t->GetEntries(); i++)
    {
      t->GetEntry(i);
      if( stripNumber < first_strip ) continue;
      if( stripNumber >= last_strip ) break; 

      sum0+=1.;
      sum1+=stripRMS;
      sum2+=(stripRMS*stripRMS);
    }

  ASIC_Mean = sum1/sum0;
  ASIC_Var  = sum2/sum0 - (ASIC_Mean*ASIC_Mean);
  if( ASIC_Var > 0 ) ASIC_RMS = sqrt( ASIC_Var );

}

void Get_Noisey_Strips( Int_t run_number, Int_t module_number, Int_t asic_number)
{
  Double_t ASIC_Mean(0.);
  Double_t ASIC_RMS(0.);

  Get_Mean_ASIC_RMS( run_number, module_number, asic_number, ASIC_Mean, ASIC_RMS );

  TString file("../data/alphaStrips");
  file+=(run_number);
  file+=("offline.root");
  
  TFile* f = GetFile(file);
  if( !f )
    return -9999.;

  TTree* t = (TTree*) f->Get("alphaStrip Tree");
  if( !t )
    return -9999.;

  Int_t stripNumber;
  Float_t stripMean;
  Float_t stripRMS;
  t->SetBranchAddress( "stripNumber",     &stripNumber );
  t->SetBranchAddress( "stripMean",       &stripMean );
  t->SetBranchAddress( "stripMeanSubRMS", &stripRMS );

  Int_t first_strip = 512*(module_number-1) + 128*(asic_number-1) + 0;
  Int_t last_strip = 512*(module_number-1) + 128*(asic_number-1) + 128;

  for(Int_t i=0; i<t->GetEntries(); i++)
    {
      t->GetEntry(i);
      if( stripNumber < first_strip ) continue;
      if( stripNumber >= last_strip ) break; 

      if(  (stripRMS - ASIC_Mean)/ASIC_RMS  > 5. ) printf("Module_number %d\t\t ASIC_number %d\t Strip %d is noisey : %f \t\t %f \t %f \n", module_number, asic_number, (stripNumber - first_strip), stripRMS, ASIC_Mean, ASIC_RMS );
    }

  if(t) delete t;
}

void Get_Noisey_Strips( Int_t run_number )
{
  for( Int_t module_number=1; module_number<=60; module_number++ )
    {
      if( module_number==10 ) continue;

      for( Int_t asic_number=1; asic_number<=4; asic_number++ )
        {
          Get_Noisey_Strips( run_number, module_number, asic_number );
        }
    }
}

// TGraphErrors * PlotMeanOverRuns( Int_t srun, Int_t erun, Int_t module_number, Int_t asic_number, Int_t strip )
// {
//   Int_t n = erun - srun;
//   if( n < 0 )
//     {
//       Error("PlotMeanOverRuns","Crappy run choices");
//       return (TGraph*) NULL;
//     }

//   Int_t tot = 0;
//   TGraphErrors* graph = new TGraphErrors();

//   for( Int_t i = srun; i <= erun; i++ )
//     {
//       Double_t mean = GetStripMean( i, module_number, asic_number, strip );
//       Double_t err = GetStripMeanError( i, module_number, asic_number, strip );
//       if(( mean > -9999. ) && ( mean < 9999. ) )
//         {
//           graph->SetPoint(tot, i, mean );
//           graph->SetPointError(tot, 0, err );
//           tot++;
//         }
//     }
  
//   graph->GetYaxis()->SetRangeUser(-50.,50.);
//   graph->GetXaxis()->SetRangeUser(srun,erun);
//   graph->SetMarkerStyle(2);

//   char title[80];
//   sprintf( title, "Module %d Asic %d Strip %d", module_number, asic_number, strip );
//   graph->SetTitle(title);

//   return graph;
// }

// TGraphErrors * PlotModuleMeanOverRuns( Int_t srun, Int_t erun, Int_t module_number, Int_t asic_number )
// {
//   Int_t n = erun - srun;
//   if( n < 0 )
//     {
//       Error("PlotModuleMeanOverRuns","Crappy run choices");
//       return (TGraph*) NULL;
//     }

//   Int_t tot[128];
//   TArrayD * mean_array[128];
//   for( Int_t i = 0; i<128; i++ )
//     {
//       mean_array[i] = new TArrayD();
//       tot[i] = 0;
//     }

//   for( Int_t i = srun; i <= erun; i++ )
//     {
//       for( Int_t s = 0; s<128; s++ )
//         {
//           Double_t mean = GetStripMean( i, module_number, asic_number, s );
//           if(( mean > -9999. ) && ( mean < 9999. ) )
//             {
//               mean_array[s]->Set(tot[s]+1);
//               mean_array[s]->SetAt(mean, tot[s]);
//               tot[s]++;
//             }
//         }
//     }

//   Double_t means[128];
//   Double_t sd[128];
//   Double_t strip[128];
//   for( Int_t s=0; s<128; s++ )
//     {
//       strip[s] = s;
//       means[s] = Mean( mean_array[s] );
//       sd[s] = StdDev( mean_array[s] );//sqrt(tot[s]);
//     }
  
//   for( Int_t i = 0; i<128; i++ )
//     {
//       delete mean_array[i];
//     }

//   TGraphErrors * graph = new TGraphErrors( 128, strip, means, 0, sd );
//   graph->GetYaxis()->SetRangeUser(-50.,50.);
//   graph->GetXaxis()->SetRangeUser(0,128);
//   graph->SetMarkerStyle(2);

//   char title[80];
//   sprintf( title, "Module %d Asic %d", module_number, asic_number );
//   graph->SetTitle(title);

//   return graph;
// }

void PlotStripRMS( Int_t run_number, Int_t module_number, TCanvas* &pad )
{
  //  TCanvas* c = new TCanvas("c","c",1000,800);
  pad->Divide(2,2);
  
  pad->cd(1);
  TGraph* a1 = PlotStripRMS( run_number, module_number, 1 );
  a1->SetMarkerColor(2);
  a1->Draw("APL");
  
  pad->cd(2);
  TGraph* a2 = PlotStripRMS( run_number, module_number, 2 );
  a2->SetMarkerColor(2);
  a2->Draw("APL");
  
  pad->cd(3);
  TGraph* a3 = PlotStripRMS( run_number, module_number, 3 );
  a3->SetMarkerColor(4);
  a3->Draw("APL");

  pad->cd(4);
  TGraph* a4 = PlotStripRMS( run_number, module_number, 4 );
  a4->SetMarkerColor(4);
  a4->Draw("APL");
}


void PlotStripRMS( Int_t run_number, Int_t module_number, TVirtualPad* &pad )
{
  //  TCanvas* c = new TCanvas("c","c",1000,800);
  pad->Divide(2,2);
  
  pad->cd(1);
  TGraph* a1 = PlotStripRMS( run_number, module_number, 1 );
  a1->SetMarkerColor(2);
  a1->Draw("APL");
  
  pad->cd(2);
  TGraph* a2 = PlotStripRMS( run_number, module_number, 2 );
  a2->SetMarkerColor(2);
  a2->Draw("APL");
  
  pad->cd(3);
  TGraph* a3 = PlotStripRMS( run_number, module_number, 3 );
  a3->SetMarkerColor(4);
  a3->Draw("APL");

  pad->cd(4);
  TGraph* a4 = PlotStripRMS( run_number, module_number, 4 );
  a4->SetMarkerColor(4);
  a4->Draw("APL");
}

void PlotStripRMS( Int_t run_number, Int_t module_number )
 {
   TCanvas* c = new TCanvas("c","c",1000,800);
   PlotStripRMS( run_number, module_number, c );
 }

void PlotStripRMS( Int_t run_number  )
 {
   TCanvas* c1 = new TCanvas("SiMod1:16","SiMod1:16",1000,800);
   c1->Divide(4,4);
   for(Int_t i=1; i<=16; i++ )
     {
       //c1->cd(i);
       TVirtualPad* p = c1->cd(i);
       PlotStripRMS( run_number, i, p );
     }
   
   TCanvas* c2 = new TCanvas("SiMod17:30","SiMod17:30",1000,800);
   c2->Divide(4,4);
   for(Int_t i=17; i<=30; i++ )
     {
       TVirtualPad* p = c2->cd(i-16);
       PlotStripRMS( run_number, i, p );
     }
   
   TCanvas* c3 = new TCanvas("SiMod31:46","SiMod31:46",1000,800);
   c3->Divide(4,4);
   for(Int_t i=31; i<=46; i++ )
     {
       TVirtualPad* p = c3->cd(i-30);
       PlotStripRMS( run_number, i, p);
     }
   
   TCanvas* c4 = new TCanvas("SiMod47:60","SiMod47:60",1000,800);
   c4->Divide(4,4);
   for(Int_t i=47; i<=60; i++ )
     {
       TVirtualPad* p = c4->cd(i-46);
       PlotStripRMS( run_number, i, p );
       }   
 }

// void PlotStripMeanSubRMS( Int_t run_number, Int_t module_number, TVirtualPad* & pad )
// {
//   TCanvas* c = new TCanvas("c","c",1000,800);
//   pad->Divide(2,2);
  
//   pad->cd(1);
//   TGraph* a1 = PlotStripMeanSubRMS( run_number, module_number, 0 );
//   a1->SetMarkerColor(2);
//   a1->Draw("APL");
    
//   pad->cd(2);
//   TGraph* a2 = PlotStripMeanSubRMS( run_number, module_number, 1 );
//   a2->SetMarkerColor(2);
//   a2->Draw("APL");

//   pad->cd(3);
//   TGraph* a3 = PlotStripMeanSubRMS( run_number, module_number, 2 );
//   a3->SetMarkerColor(4);
//   a3->Draw("APL");

//   pad->cd(4);
//   TGraph* a4 = PlotStripMeanSubRMS( run_number, module_number, 3 );
//   a4->SetMarkerColor(4);
//   a4->Draw("APL");
// }

// void PlotStripMeanSubRMS( Int_t run_number, Int_t module_number )
// {
//   TCanvas* c = new TCanvas("c","c",1000,800);
//   PlotStripMeanSubRMS( run_number, module_number, c );
// }

// void PlotStripMeanSubRMS( Int_t run_number  )
// {
//   TCanvas* c1 = new TCanvas("SiMod1:16","SiMod1:16",1000,800);
//   c1->Divide(4,4);
//   for(Int_t i=1; i<=16; i++ )
//     {
//       TVirtualPad* p = c1->cd(i);
//       PlotStripMeanSubRMS( run_number, i, p );
//     }

//   TCanvas* c2 = new TCanvas("SiMod17:30","SiMod17:30",1000,800);
//   c2->Divide(4,4);
//   for(Int_t i=17; i<=30; i++ )
//     {
//       TVirtualPad* p = c2->cd(i-16);
//       PlotStripMeanSubRMS( run_number, i, p );
//     }

//   TCanvas* c3 = new TCanvas("SiMod31:46","SiMod31:46",1000,800);
//   c3->Divide(4,4);
//   for(Int_t i=31; i<=46; i++ )
//     {
//       TVirtualPad* p = c3->cd(i-30);
//       PlotStripMeanSubRMS( run_number, i, p );
//     }

//   TCanvas* c4 = new TCanvas("SiMod47:60","SiMod47:60",1000,800);
//   c4->Divide(4,4);
//   for(Int_t i=47; i<=60; i++ )
//     {
//       TVirtualPad* p = c4->cd(i-46);
//       PlotStripMeanSubRMS( run_number, i, p );
//     }

//}

// TH1D* PlotRMS( Int_t run_number, Int_t asic_number )
// {
//   TString file("../data/sistripanalyser");
//   file+=(run_number);
//   file+=("offline.root");

//   TFile* thefile = new TFile(file);

//   TTree* t = (TTree*) thefile->Get("SiliconStripRMS Tree");

//   char plot_title[80];
//   sprintf( plot_title, "Run %d Asic %d", run_number, asic_number );


//   TH1D* h_qod = new TH1D( plot_title, plot_title, 500,0.,100.);

//   TSiliconStripRMS* siliconRMS = new TSiliconStripRMS;
//   t->SetBranchAddress("SiliconStripRMS", &siliconRMS );

//   for(Int_t i=0; i<t->GetEntries(); i++)
//     {
//       t->GetEntry(i);
//       if( siliconRMS->GetASICNumber() != asic_number ) continue;
//       h_qod->Fill( siliconRMS->GetStripRMS() );      
//     }

//   return h_qod;
// }

