#include "TVertexCut.h"

ClassImp(TVertexCut)

// Constructor

TVertexCut::TVertexCut()
{
	active=0;
	zMin=0.;
	zMax=0.;
	rMin=0.;
	rMax=0.;
	tMin=0.;
	tMax=0.;
	residualMin=0.;
	residualMax=0.;
	nTracksMin=0;
	nTracksMax=0;
	phiStart=0.;
	phiEnd=0.;
	
	fname = (char*)malloc(100*sizeof(char));
	sprintf(fname, "Cut");

    
}

TVertexCut::TVertexCut(Double_t _zMin, Double_t _zMax, Double_t _rMin, Double_t _rMax, Double_t _tMin, Double_t _tMax, Double_t _phiStart, Double_t _phiEnd, Double_t _residualMin, Double_t _residualMax, Int_t _tracksMin, Int_t _tracksMax)
{
	TVertexCut();

	zMin=_zMin;
	zMax=_zMax;
	rMin=_rMin;
	rMax=_rMax;
	tMin=_tMin;
	tMax=_tMax;
	residualMin=_residualMin;
	residualMax=_residualMax;
	nTracksMin=_tracksMin;
	nTracksMax=_tracksMax;
	phiStart=_phiStart;
	phiEnd=_phiEnd;
	
	active=1;
}

TVertexCut::~TVertexCut()
{
}

void TVertexCut::SetName(char* name)
{
	fname=name;
}

const char* TVertexCut::GetName()
{
	return fname;
}


//Functions

Int_t TVertexCut::CheckInRange(TSiliconEvent* ts)
{
	if(active==0) 
	{
		//Warning("TVertexCut::CheckInRange", "Cut is not active");
		return 1;
	}
	
	if(ts->GetNVertices()<1) return 0;
	
	if(this->GetName()!=NULL)
	{
		if(strcmp(this->GetName(), "Trapping")==0) // This is an override to do the canonical cuts
		{
			//Info("TVertexCut::CheckInRange","Name 'Trapping' detected - using Trapping Cuts only");
			
			if(ts->GetNTracks()==2)
			{
				this->DefineCut(0., 0., 0., 4., 0., 0., 0., 0., 2., 0., 2, 2);
				//this->DefineCut(0., 0., 0., 0., 0., 0., 0., 0., 2., 0., 2, 2);
				if(ts->GetResidual()<2.) return 0;
				return this->CheckInRange(ts->GetVertex());
			}
			
			if(ts->GetNTracks()>2)
			{
				this->DefineCut(0., 0., 0., 4., 0., 0., 0., 0., 0.05, 0., 3, 0);
				//this->DefineCut(0., 0., 0., 0., 0., 0., 0., 0., 0.05, 0., 3, 0);
				if(ts->GetResidual()<0.05) return 0;
				return this->CheckInRange(ts->GetVertex());
			}
			
			return 0;
		
		}
	}
	
	if((ts->GetNTracks()<nTracksMin)||(ts->GetNTracks()>nTracksMax && nTracksMax!=0)) return 0;
	if( (ts->GetResidual()<residualMin && residualMin>0.) || (ts->GetResidual()>residualMax && residualMax!=0.)) return 0;
	if( (ts->GetRunTime()<tMin && tMin>0.) || (ts->GetRunTime()>tMax && tMax>0.) ) return 0;
	
	if(CheckInRange(ts->GetVertex())==0) return 0;
	
	return 1;	
}

Int_t TVertexCut::CheckInRange(TVector3* vtx)
	{
		// return 1 if in range, 0 otherwise;
		// this function can't check time;
		
		if(active==0) return 1;
		
		Double_t phi, r;
		
		r = sqrt(pow(vtx->X(),2.)+pow(vtx->Y(),2.));
		phi =  (vtx->Phi()/3.14159)*180.;
		
		
		// need to set z->-z
		//if(((-vtx->Z()<zMin)||(-vtx->Z()>zMax)) && zMin!=zMax) return 0; // out of range
		if(((vtx->Z()<zMin)||(vtx->Z()>zMax)) && zMin!=zMax) return 0; // out of range
		if((r<rMin)||(r>rMax && rMax!=0.)) return 0; // out of range
		if(!DeterminePhiCut(phi)) return 0; // out of range
		
	
		return 1; // in range
	}

Int_t TVertexCut::DeterminePhiCut(Double_t phi)
{
	//Return 1 if accept, 0 if reject
	
	if(phiStart==phiEnd) return 1;

	if(phiStart<phiEnd)
	{
		// This is the easy case
		if(phi>phiStart && phi<phiEnd) return 1;
		return 0;
	}
	
		// This crosses the 180 deg line
		

		if(phi<=phiStart) phi+=360.;
		if(phi>phiStart && phi<phiEnd+360.) return 1;
		return 0;

}
	
Int_t TVertexCut::DefineCut(Double_t _zMin, Double_t _zMax, Double_t _rMin, Double_t _rMax, Double_t _tMin, Double_t _tMax, Double_t _phiStart, Double_t _phiEnd, Double_t _residualMin, Double_t _residualMax, Int_t _tracksMin, Int_t _tracksMax)
{
	zMin=_zMin;
	zMax=_zMax;
	rMin=_rMin;
	rMax=_rMax;
	tMin=_tMin;
	tMax=_tMax;
	residualMin=_residualMin;
	residualMax=_residualMax;
	nTracksMin=_tracksMin;
	nTracksMax=_tracksMax;
	phiStart=_phiStart;
	phiEnd=_phiEnd;

	active=1;
	return active;
}
		
Int_t TVertexCut::PrintCuts()
{
	printf("zMin:\t\t%.3lf\tzMax:\t\t%.3lf\n", zMin, zMax);
	printf("rMin:\t\t%.3lf\trMax:\t\t%.3lf\n", rMin, rMax);
	printf("tMin:\t\t%.3lf\ttMax:\t\t%.3lf\n", tMin, tMax);
	printf("phiStart:\t%.3lf\tphiEnd:\t\t%.3lf\n",phiStart, phiEnd);
	printf("nTracksMin:\t\t%.d\tnTracksMax:\t\t%d\n", nTracksMin, nTracksMax);
	printf("residualMin:\t\t%.3lf\tresidualMax:\t\t%.3lf\n", residualMin, residualMax);
	
	return 1;
}

TString TVertexCut::GetCutString()
{
	char temp[50];
	TString cutString = " ";
	
	if(active==0) return cutString;
	
	if(strcmp(this->GetName(), "Trapping")==0) 
	{
		cutString = "[(nTracks==2 && Res>2.0) OR (nTracks>2 && Res>0.05)] AND R<4";
		return cutString;
	}
	
	if(zMin!=zMax) 
	{
		sprintf(temp,"%.2lf < Z < %.2lf ", zMin, zMax);
		cutString+=temp;
	}
	
	if(tMin!=tMax) 
	{
		if(tMin!=0.)
		{
			sprintf(temp,"%.2lf <", tMin);
			cutString+=temp;
		}
		
		cutString+=" t ";
		
		if(tMax!=0.)
		{
			sprintf(temp,"< %.2lf ", tMax);
			cutString+=temp;
		}
	}
	
	if(rMin!=rMax) 
	{
		if(rMin!=0.)
		{
			sprintf(temp,"%.2lf <", rMin);
			cutString+=temp;
		}
		
		cutString+=" r ";
		
		if(rMax!=0.)
		{
			sprintf(temp,"< %.2lf ", rMax);
			cutString+=temp;
		}
	}
	
	if(phiStart!=phiEnd)
	{
		sprintf(temp,"Phi: %.2lf -> %.2lf ", phiStart, phiEnd);
		cutString+=temp;
	}
	
	if(nTracksMin==nTracksMax && nTracksMin!=0)
	{
		sprintf(temp, "nTracks==%d ", nTracksMin);
		cutString+=temp;
	}
	else
	{
		if(nTracksMin!=0)
		{
			sprintf(temp, "%d < ", nTracksMin);
			cutString+=temp;
		}
		
		if(nTracksMin!=0 || nTracksMax!=0)
		{
			sprintf(temp, "nTracks ");
			cutString+=temp;
		}
		
		if(nTracksMax!=0)
		{
			sprintf(temp, "< %d ", nTracksMax);
			cutString+=temp;
		}
	}
	
	if(residualMin!=residualMax)
		{
			if(residualMin!=0.)
			{
				sprintf(temp, "%.3lf <", residualMin);
				cutString+=temp;
			}
			sprintf(temp, " residual ");
			cutString+=temp;
			if(residualMax!=0.)
			{
				sprintf(temp, " < %.3lf ", residualMax);
				cutString+=temp;
			}
			
		}

	
	return cutString;
}
//Getters

	Double_t TVertexCut::GetzMin()
	{
		return zMin;
	}
	Double_t TVertexCut::GetzMax()
	{
		return zMax;
	}
	Double_t TVertexCut::GettMin()
	{	
		return tMin;
	}
	Double_t TVertexCut::GettMax()
	{
		return tMax;
	}
	Double_t TVertexCut::GetrMin()
	{
		return rMin;
	}
	Double_t TVertexCut::GetrMax()
	{	
		return rMax;
	}
	Int_t TVertexCut::GetnTracksMin()
	{
		return nTracksMin;
	}
	Int_t TVertexCut::GetnTracksMax()
	{
		return nTracksMax;
	}
	Double_t TVertexCut::GetphiStart()
	{
		return phiStart;
	}
	Double_t TVertexCut::GetphiEnd()
	{
		return phiEnd;
	}
	Double_t TVertexCut::GetResidualMin()
	{
		return residualMin;
	}
	Double_t TVertexCut::GetResidualMax()
	{
		return residualMax;
	}	
	Int_t TVertexCut::GetIsActive()
	{
		return active;
	}
	
//Setters

	Double_t TVertexCut::SetzMin(Double_t _zMin)
	{
		zMin=_zMin;
		return zMin;
	}
	Double_t TVertexCut::SetzMax(Double_t _zMax)
	{
		zMax=_zMax;
		return zMax;
	}
	Double_t TVertexCut::SettMin(Double_t _tMin)
	{
		tMin=_tMin;
		return tMin;
	}
	Double_t TVertexCut::SettMax(Double_t _tMax)
	{
		tMax=_tMax;
		return tMax;
	}
	Double_t TVertexCut::SetrMin(Double_t _rMin)
	{
		rMin=_rMin;
		return rMin;
	}
	Double_t TVertexCut::SetrMax(Double_t _rMax)
	{
		rMax=_rMax;
		return rMax;
	}
	Double_t TVertexCut::SetphiStart(Double_t _phiMin)
	{
		phiStart=_phiMin;
		return phiStart;
	}
    Double_t TVertexCut::SetphiEnd(Double_t _phiMax)
    {
      phiEnd=_phiMax;
      return phiEnd;
    }
   	Double_t TVertexCut::SetResidualMin(Double_t _residualMin)
	{
		residualMin = _residualMin;
		return residualMin;
	}
    Double_t TVertexCut::SetResidualMax(Double_t _residualMax)
    {
		residualMax = _residualMax;
		return residualMax;
    }
    Int_t TVertexCut::SetnTracksMin(Int_t _nTracksMin)
	{
		nTracksMin=_nTracksMin;
		return nTracksMin;
	}
	Int_t TVertexCut::SetnTracksMax(Int_t _nTracksMax)
	{
		nTracksMax=_nTracksMax;
		return nTracksMax;
	}
	
	Int_t TVertexCut::SetIsActive(Int_t isActive)
	{
		if(isActive>0) active=1;
		else active=0;
		
		return active;
	}
