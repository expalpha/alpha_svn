#include "TROOT.h"
#include "TObject.h"
class TVertexCut;

class TVertexCut : public TObject
{
  ClassDef(TVertexCut,1)
  
  private:
  
  Int_t active; // Active Y/N
  Double_t zMin, zMax;
  Double_t tMin, tMax;
  Double_t rMin, rMax;
  Double_t phiStart, phiEnd;
  Int_t nTracksMin, nTracksMax;
  Double_t residualMin, residualMax;
  
  Bool_t xyMaskInActive;
  Bool_t xyMaskOutActive;
  Double_t xMaskMin;
  Double_t xMaskMax;
  Double_t yMaskMin;
  Double_t yMaskMax;
  
  char* fname;
  
  Int_t DeterminePhiCut(Double_t);
  
public:
  
  TVertexCut();
  TVertexCut(Double_t, Double_t, Double_t, Double_t, Double_t, Double_t, Double_t, Double_t, Int_t, Int_t, Double_t, Double_t);
  ~TVertexCut();
  
  //Getters
  Double_t GetzMin();
  Double_t GetzMax();
  Double_t GettMin();
  Double_t GettMax();
  Double_t GetrMin();
  Double_t GetrMax();
  Int_t GetnTracksMin();
  Int_t GetnTracksMax();
  Double_t GetResidualMax();
  Double_t GetResidualMin();
  Double_t GetphiStart();
  Double_t GetphiEnd();
  Int_t GetIsActive();
  
  const char* GetName();
  
  //Setters
  Double_t SetzMin(Double_t);
  Double_t SetzMax(Double_t);
  Double_t SettMin(Double_t);
  Double_t SettMax(Double_t);
  Double_t SetrMin(Double_t);
  Double_t SetrMax(Double_t);
  Int_t SetnTracksMin(Int_t);
  Int_t SetnTracksMax(Int_t);
  Double_t SetResidualMin(Double_t);
  Double_t SetResidualMax(Double_t);
  Double_t SetphiStart(Double_t);
  Double_t SetphiEnd(Double_t);
  
  void SetName(char*);

  Int_t SetIsActive(Int_t);
	
  Int_t CheckInRange(TVector3*);
  Int_t CheckInRange(TSiliconEvent*);
  Int_t DefineCut(Double_t, Double_t, Double_t, Double_t, Double_t, Double_t, Double_t, Double_t, Double_t, Double_t, Int_t, Int_t);
  Int_t PrintCuts();
  TString GetCutString();
	
};
