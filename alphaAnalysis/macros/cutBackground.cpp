#include "PlotMixing.cpp"

Bool_t CHECK_MAGNETS(kFALSE);
Bool_t WRITE_LOG(kFALSE);

void checkBackground(Int_t runNumber)
{
	// for convenience ;)
	char treefile[200];
	sprintf(treefile, "../data/tree%doffline.root", runNumber);
	checkBackground(treefile);
}

void checkBackground(char* treefile)
{
	TFile* fid = new TFile(treefile, "READ");
	if(!fid)
	{
		Error("", "File %s does not exist", treefile);
		assert(false);
	}
	
	TTree* tVtx = (TTree*)fid->Get("gSiliconTree");
	TSiliconEvent* sE = new TSiliconEvent();
	tVtx->SetBranchAddress("SiliconEvent", &sE);

	gCut->SetIsActive(kTRUE);
	gCut->SetName("Trapping");
	//gCut->SetnTracksMin(3.);
	
	Int_t nAccepted(0), nVertices(0);
	Double_t vetoWindow(0.5);
	
	TTree* tAD = (TTree*)fid->Get("gSisTree01");
	TTree* tADin = (TTree*)fid->Get("gSisTree52");
	
	Int_t i;
	Double_t octCurrent, usmCurrent, dsmCurrent;
	
	if(CHECK_MAGNETS)
	{
		TTree* lvTree = (TTree*)fid->Get("LabVIEW Tree");
	}
	
	tVtx->GetEntry(0);
	char logfilename[20];
	sprintf(logfilename, "cosmiccut%d.log", sE->GetRunNumber());
	
	if(WRITE_LOG) FILE* fid_log = fopen(logfilename, "w");
	
	for(i=0; i<tVtx->GetEntries()-1; i++)
	{
		sE->ClearEvent();
		tVtx->GetEntry(i);
		
		if(WRITE_LOG) fprintf(fid_log, "%d\t%d\t%d\t%.2lf\t%.2lf\t", sE->GetVF48NEvent(), sE->GetNVertices(), sE->GetNTracks(), sE->GetVertex()->XYvector().Mod(), sE->GetResidual());
		
		if(checkVeto(tAD, sE->GetRunTime(), vetoWindow) || checkVeto(tADin, sE->GetRunTime(), vetoWindow)) 
		{
			if(WRITE_LOG)  fprintf(fid_log, "VETO\n");
			continue;
		}

		nVertices+=sE->GetNVertices();
						
		if(!gCut->CheckInRange(sE)){
			if(WRITE_LOG)  fprintf(fid_log, "FAIL\n");
			continue;
		}
		if(WRITE_LOG)  fprintf(fid_log, "PASS\n");
		nAccepted++;
		
		if(CHECK_MAGNETS)
		{
			octCurrent = GetLabviewEvent(lvTree, sE->GetRunTime()+1., "BNLM", 26, 1);
			usmCurrent = GetLabviewEvent(lvTree, sE->GetRunTime()+1., "BNLM", 17, 1);
			dsmCurrent = GetLabviewEvent(lvTree, sE->GetRunTime()+1., "BNLM", 35, 1);
			
			if(octCurrent<800.) 
			{
				Error("", "OCT current dropped to %.2lf at runTime = %.0lf s, discarding rest of run", octCurrent, sE->GetRunTime());
				break;
			}
			
			if(usmCurrent<550.) 
			{
				Error("", "USM current dropped to %.2lf at runTime = %.0lf s, discarding rest of run", usmCurrent,sE->GetRunTime());
				break;
			}
			
			if(dsmCurrent<550.) 
			{
				Error("", "DSM current dropped to %.2lf at runTime = %.0lf s, discarding rest of run", dsmCurrent,sE->GetRunTime());
				break;
			}
		}
	}
	
	sE->ClearEvent();
	tVtx->GetEntry(0);
	Double_t startTime = sE->GetTSRunTime();
	sE->ClearEvent();
	tVtx->GetEntry(i);
	Double_t endTime = sE->GetTSRunTime();
	sE->ClearEvent();
	Double_t totalTime =  endTime-startTime;
	
	Double_t vetoedTime = ((Double_t)CountCounts(tAD, endTime))*vetoWindow*2.;
	vetoedTime += ((Double_t)CountCounts(tADin, endTime))*vetoWindow*2.;
	totalTime-=vetoedTime;
	
	TTree* tSi = (TTree*) fid->Get("gSisTree51");
	Int_t nSi = CountCounts(tSi, endTime);
	
	TTree* tT3T6 = (TTree*) fid->Get("gSisTree56");
	Int_t nT3T6 = CountCounts(tT3T6, endTime);
	
	printf("%.3lf - %.3lf [%.3lf (%.3lf vetoed) ==> %.3lf]\n", startTime, endTime, endTime-startTime, vetoedTime, totalTime);
	printf("Si>1 \t\t %d \t [(%.3lf \t+-\t %.3lf)\tHz]\n", nSi, (Double_t)nSi/totalTime, sqrt((Double_t)nSi)/totalTime);
	printf("T3T6 \t\t %d \t [(%.3lf \t+-\t %.3lf)\tHz]\n", nT3T6, (Double_t)nT3T6/totalTime, sqrt((Double_t)nT3T6)/totalTime);
	printf("Readouts \t %d \t [(%.3lf \t+-\t %.3lf)\tHz]\n", i, (Double_t)i/totalTime, sqrt((Double_t)i)/totalTime);
	printf("Vertices \t %d \t [(%.3lf \t+-\t %.3lf)\tHz]\t(%.3lf\%)\n", nVertices, (Double_t)nVertices/totalTime, sqrt((Double_t)nVertices)/totalTime, (Double_t)nVertices/(Double_t)tVtx->GetEntries());
	printf("Accepted \t %d \t [(%.3lf \t+-\t %.3lf)\tHz]\t(%.3lf\%)\n", nAccepted, (Double_t)nAccepted/totalTime, sqrt((Double_t)nAccepted)/totalTime, (Double_t)nAccepted/(Double_t)tVtx->GetEntries());

	delete tVtx;
	delete tAD;
	delete tSi;
	delete tT3T6;
	delete tADin;
	delete sE;
	fid->Close();
	if(WRITE_LOG)  fclose(fid_log);
	
	
	
}

Int_t CountCounts(TTree* sistree, Double_t tmax=9999999.)
{
	TSisEvent* sisE = new TSisEvent();
	sistree->SetBranchAddress("SisEvent", &sisE);
	
	Int_t nCount(0);
	
	for(Int_t i=0; i<sistree->GetEntries(); i++)
	{
		sisE->Clear();
		sistree->GetEntry(i);
		if(sisE->GetRunTime()>tmax) break;
		nCount+=sisE->GetCountsInChannel();
	}

		sistree->SetBranchAddress("SisEvent", NULL);
		delete sisE;
		return nCount;
		
}

Bool_t checkVeto(TTree* vetoTree, Double_t runTime, Double_t window)
{
	TSisEvent* sE = new TSisEvent();
	vetoTree->SetBranchAddress("SisEvent", &sE);
	
	Bool_t result(kFALSE)
	
	if(vetoTree->GetEntries()==0) Warning("checkVeto", "vetoTree has zero entries");
	
	for(Int_t i=0; i<vetoTree->GetEntries(); i++)
	{
		sE->Clear();
		vetoTree->GetEntry(i);
		if(fabs(sE->GetRunTime()-runTime)<window) 
		{
			result = kTRUE;
			break;
		}
		
	}
	
	vetoTree->SetBranchAddress("SisEvent", NULL);
	delete sE;
	
	return result;
}
