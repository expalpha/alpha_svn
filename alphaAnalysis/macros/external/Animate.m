%function r(X,Y,Z,FaceAalpha,varargin)
%Joseph McKENNA: Animate vertex plots

%clear all
function AnimateFast(runNumber,LoadFolder,OutputName)

close all
set(0,'DefaultFigureVisible','off')

SAVE=1;
DRAW3D=1;
YRES=720;
XRES=1920;
limz=220;
scale_factor=1;%4/3;

passed_cuts=1;
%make 3d animation of data
% run=28493; %microwave off run
%Series 25: 3 reco events: 21231
%%
%SINGLE=0;
%parfor SINGLE=[1 2 3 4]% 0 2]
for SINGLE=[5 ]
    
    %total_passed_cuts=0;
    mkdir(OutputName);
    %microwave on 29324
    %load reco file
    home = pwd;
    %filename=(['../2011_results/time_and_vertex/alphaanalysis_vertex_' num2str(run) '.csv']);
    data_long=[];
    %        for i=1:length(run)
    %filename=([ FOLDER_LOAD '/MixingDataRun' num2str(run(i)) '.csv']);
    filename=([ LoadFolder 'Data_Run' num2str(runNumber) '.csv']);
    disp(['Filename:' filename])
    %            [data] = csvread(filename,2,0);
    %            disp(['Reco data loaded...']);
    data=importdata(filename,',',1,0);
    data_long=data.data;
    %            data_long=[data_long; data];
    %        end
    close all
    %  data_long=sortrows(data_long,4);
    %  time=data_long(:,4)/1000;
    time_column=-1;
    x_column=-1;
    y_column=-1;
    z_column=-1;
    Pass_column=-1;
    for i=[1:size(data.textdata,2)]
        %Time:
        if (strcmp(data.textdata(1,i),'Time from given start (ms)')==1)
            time_column=i;
        end
        %X
        if (strcmp(data.textdata(1,i),'X')==1)
            x_column=i;
        end
        %Y
        if (strcmp(data.textdata(1,i),'Y')==1)
            y_column=i;
        end
        %Z
        if (strcmp(data.textdata(1,i),'Z')==1)
            z_column=i;
        end
        %Pass
        if (strcmp(data.textdata(1,i),'PassedCut')==1)
            Pass_column=i;
        end
        %MVA
        if (strcmp(data.textdata(1,i),'PassMVA')==1)
            Pass_column=i;
        end
        
    end
    offset=size(data.textdata,2) - size(data_long,2);
    disp(['Offset: ' num2str(offset) ])
    if (time_column < 0 )
    time_column=12;
    x_column=16;
    y_column=17;
    z_column=18;
    Pass_column=22;
    offset=0;
    end
    disp(['Time column ' num2str(time_column)]);
    disp(['x column ' num2str(x_column)]);
    disp(['y column ' num2str(y_column)]);
    disp(['z column ' num2str(z_column)]);
    disp(['Pass column ' num2str(Pass_column)]);
    %data_long=sortrows(data_long,time_column);
    time=data_long(:,time_column-offset)/1000;
    X=data_long(:,x_column-offset)*10;%mm
    Y=data_long(:,y_column-offset)*10;%mm
    Z=data_long(:,z_column-offset)*10;%mm
    pass=data_long(:,Pass_column-offset);
    
    %DELETE BEFORE MIXING
    index=[1:length(time)];
    start_point=1%min(index(time>=401.703));%409.528));%Run 37770
    %time=time(start_point:length(time));
    time=time(start_point:length(time));
    X=X(start_point:length(X));
    Y=Y(start_point:length(Y));
    Z=Z(start_point:length(Z));
    pass=pass(start_point:length(pass));
    
    if passed_cuts==0
        for i=1:length(X)
            if X(i)~=0 && Y(i)~=0 && Z(i)~=0
                pass(i)=1;
            end
        end
    end
    
    % hold on
    SPEEDFACTOR=1
    %timestep=0.1*(1/60)%0.1;%timestep=0.01;
    timestep=SPEEDFACTOR*(1/60)%0.1;%timestep=0.01;
    decay_constant=SPEEDFACTOR/2
    last_draw=0;%529.19%0;
    frames=1;
    
    
    %time(:)=time(:)-time(1);
    
    start=0;%446.380;%;0;
    last_draw=start;
    stop=1.1*max(time)%2.1;%1.1;
    %Xtime(1)=start;
    %YCounts(1)=0;
    %index=1:length(time);
    %while (last_draw<stop
    %a=1
    %if (a==0)
    parfor m=1:(round((stop-start)/timestep))
        t=start+(m-1)*timestep;
        total_passed_cuts=sum(pass(1:sum(time<t))==1);
        hold off
        close
        figure('GraphicsSmoothing','on','Visible','off')
        set(gcf,'Position',[50 50 XRES YRES])
        
        f=gcf;
        %% SUBPLOT 1
        global_limit=40;
        subplot(1,2,1)
        ax = gca;
        outerpos= ax.OuterPosition;
        ti = ax.TightInset;
        left = outerpos(1)+ti(1);
        bottom = outerpos(2)+ti(2);
        ax_width = outerpos(3) -ti(1) -ti(3);
        ax_height = outerpos(4)-ti(2)-ti(4);
        ax.Position = [left bottom ax_width ax_height];
        axis equal;
        zlim([-scale_factor*global_limit scale_factor*global_limit])
        ylim([-scale_factor*global_limit scale_factor*global_limit])
        xlim([-limz limz])
        hold on
        if DRAW3D
            EVENTS_DRAWN=0;
            %                for i=sum(time<=start)+1:sum(time<stop)%j%length(time)
            %total_passed_cuts=sum(pass(1:sum(time<stop)));
            for i=1:sum(time<stop)%j%length(time)
                if time(i)-start<=t && pass(i)>=1 %&& time(i)-start+timestep*10>last_draw %&& X(i)~=0 %
                    %disp('blue');
                    Aalpha=visible2(time(i),t+start,decay_constant);
                    if Aalpha>0.001
                        scatter3sph(Z(i),scale_factor*X(i),scale_factor*Y(i),...
                            Aalpha,'color',[0 0 1],'size',1.2+0.5*Aalpha); %blue
                    end
                end
                if time(i)-start<=t+2*timestep && time(i)-start>t && pass(i)>=1 % && X(i)~=0 %
                    %disp('yellow');
                    EVENTS_DRAWN=EVENTS_DRAWN+1;
                    Aalpha=0.7;%visible2(time(i),last_draw+start,decay_constant);
                    scatter3sph(Z(i),scale_factor*X(i),scale_factor*Y(i),...
                        Aalpha,'color',[0.8 0 0],'size',1.2+0.5*Aalpha); %[1 1 0]yellow
                    hold on
                    %                        total_passed_cuts=total_passed_cuts+1;
                end
            end
            
            light('Position',[0 0 1],'Style','infinit','Color',[1 1 1]);
            lighting gouraud
            light('Position',[0 0 1],'Style','infinit','Color',[1 1 1]);
            
            %view(30,15)
            
        end
        if t>=start && t-timestep<=stop
            R=[44.55/2 44.55/2];
            N=100;
            [CX,CY,CZ] = cylinder(R,N);
            testsubject = surf(600*(CZ-0.5),scale_factor*CY,scale_factor*CX);
            set(testsubject,'FaceColor',[0.7 0.7 0],'FaceAlpha',0.5,'EdgeColor','none',...
                'DiffuseStrength',1,'AmbientStrength',1)
        end
        grid off
        view(-30-3+(10*sin(t*0.7/SPEEDFACTOR)),30)
        %view(-30-3+(15*sin(last_draw*0.1)),30)
        
        %view(-30,30)
        xlabel('Z (mm)')
        ylabel('X (mm)')
        zlabel('Y (mm)')
        nums=['-40';
            '-20';
            '  0';
            ' 20';
            ' 40'];
        
        
        set(gca,'YTick',[-scale_factor*40:scale_factor*20:scale_factor*40])
        set(gca,'ZTick',[-scale_factor*40:scale_factor*20:scale_factor*40])
        set(gca,'YTickLabel',nums)
        set(gca,'ZTickLabel',nums)
        %             view(-30+2.5*cos(last_draw*12),30+2.5*sin(last_draw*12))
        %             annotation('textbox', [.65 .2 .1 .1],'String',['t = ' num2str(last_draw) ' s'])
        event_type={' '};
        
        %Xtime(frames)=last_draw;
        %YCounts(frames)=sum(pass(time(:)<last_draw));
        
        
        
        %% SUBPLOT 2
        %time(i)-start<=last_draw+timestep && time(i)-start>last_draw
        subplot(1,2,2)
        
        ax = gca;
        outerpos= ax.OuterPosition;
        ti = ax.TightInset;
        left = outerpos(1)+ti(1);
        bottom = outerpos(2)+ti(2);
        ax_width = outerpos(3) -ti(1) -ti(3);
        ax_height = outerpos(4)-ti(2)-ti(4);
        ax.Position = [left bottom ax_width ax_height];
        llimit= scale_factor*global_limit;
        rrange=[-llimit:llimit/16:llimit];
        xa=X(pass(time(:)<t+timestep)>=1);
        %total_passed_cuts=sum(xa)
        ya=Y(pass(time(:)<t+timestep)>=1);
        za=Z(pass(time(:)<t+timestep)>=1);
        if isempty(xa)
            xa=-9999;
        end
        if isempty(ya)
            ya=-9999;
        end
        if isempty(za)
            za=-9999;
        end
        n=hist3([xa ya],'Edges',{rrange/scale_factor,rrange/scale_factor});
        xyslice=imagesc(rrange, rrange, n);
        C = xyslice.CData;
        
        n2=hist3([ya za],'Edges',{rrange/scale_factor,limz*[-300:4:300]/300});
        zyslice=imagesc(limz*[-300:60:300]/300, rrange, n2);
        C2 = zyslice.CData;
        
        n3=hist3([xa za],'Edges',{rrange/scale_factor,limz*[-300:4:300]/300});
        zxslice=imagesc(limz*[-300:60:300]/300, rrange, n3);
        C3 = zxslice.CData;
        
        cla reset
        
        XX = [limz limz; limz limz];
        YY = [-llimit -llimit; llimit llimit];
        ZZ = [-llimit llimit; -llimit llimit];
        surface(XX,YY,ZZ,rot90(fliplr(C')),'FaceColor','texturemap','FaceAlpha',0.7,'EdgeColor','white','CDataMapping','scaled')
        
        
        XX = [-limz -limz; limz limz];
        YY = llimit*[1 1; 1 1]; %all same number
        ZZ = [-llimit llimit; -llimit llimit];
        
        surface(XX,YY,ZZ,rot90(fliplr(C2)),'FaceColor','texturemap','FaceAlpha',0.7,'EdgeColor','white','CDataMapping','scaled')
        XX = [-limz -limz; limz limz];
        YY = [-llimit llimit; -llimit llimit];
        ZZ = -llimit*[1 1; 1 1]; %all same number
        
        surface(XX,YY,ZZ,rot90(fliplr(C3)),'FaceColor','texturemap','FaceAlpha',0.7,'EdgeColor','white','CDataMapping','scaled')
        
        view(-30-3+(10*sin(last_draw*0.7/SPEEDFACTOR)),30)
        
        %alpha(xyslice,0.7)
        colormap(jet)
        c=colorbar('south');
        
        caxis auto
        caxis([0 (max(caxis)+1)]);
        set(c,'XTick',[0:1:max(caxis)]);
        if passed_cuts==1
            ylabel(c,'Detected antihydrogen')
        else
            ylabel(c,'Verticies')
        end
        axis equal;
        
        xlabel('Z (mm)')
        ylabel('X (mm)')
        zlabel('Y (mm)')
        nums=['-40';
            '-20';
            '  0';
            ' 20';
            ' 40'];
        zlim([-scale_factor*global_limit scale_factor*global_limit]*1.01)
        ylim([-scale_factor*global_limit scale_factor*global_limit]*1.01)
        xlim([-limz limz]*1.01)
        
        
        set(gca,'YTick',[-scale_factor*40:scale_factor*20:scale_factor*40])
        set(gca,'ZTick',[-scale_factor*40:scale_factor*20:scale_factor*40])
        set(gca,'YTickLabel',nums)
        set(gca,'ZTickLabel',nums)
        %        light('Position',[0 -0.5 1],'Style','infinit','Color',[1 1 1]);
        %        lighting gouraud
        
        
        view(-30-3+(10*sin(t*0.7/SPEEDFACTOR)),30)
        
        %% DRAW NOW
        if passed_cuts==1
            ITEM_COUNTED='Detected Antihydrogen';
        else
            ITEM_COUNTED='Verticies';
        end
        mtit(['T: ' num2str(0.01*round(100*(t+start)),'%.2f') ...
            's  ' ITEM_COUNTED ': ' num2str(total_passed_cuts)],...
            'fontsize',32,...
            'xoff',0,'yoff',-0.08);
        %set(gcf,'Position',[50 50 1280 720])
        %set(f,'units','pixel');
        %set(f,'papersize',[1280 720]);
        set(gcf,'Renderer','painters');
        f.InvertHardcopy='off';
        f.PaperUnits='points';
        f.PaperPosition=[0 0 XRES YRES];
        f.PaperPositionMode='manual';
        drawnow
        %                last_draw=last_draw+timestep;
        %frames=frames+1;
        
        %saveas(f, [OUTPUT_PRENAME '/0.01SecStep_' num2str(last_draw) '_PassCut.jpg'])
        if SAVE
            %print(gcf,['-r',num2str(300)], '-dpng', [OUTPUT_PRENAME '/0.01SecStep_' num2str(last_draw) '_PassCut.jpg']);
            %    myaa([OUTPUT_PRENAME '/0.01SecStep_' num2str(last_draw) '_PassCut.jpg'],2,1) %8,4)
            %    saveas(f,[OUTPUT_PRENAME '/0.01SecStep_' num2str(last_draw) '_PassCut.png'],'-dpng','-r0') %8,4)
            print([OutputName '/' num2str(m,'%05.f') '.png'],'-dpng','-r0') %8,4)
            %print([OutputName '/' num2str(m,'%05.f') '-' num2str(timestep) 'Step.png'],'-dpng','-r0') %8,4)
            %                print([OutputName '/' num2str(m,'%05.f') '-0.01SecStep_' num2str(t) '_PassCut.png'],'-dpng','-r0') %8,4)
        end
        close
        
    end
   % end
    myVideo = VideoWriter([OutputName '/myfile.avi'],'MPEG-4');
    %myVideo = VideoWriter([OutputName '/myfile.avi', 'Uncompressed AVI');
    myVideo.FrameRate = 60;  % Default 30
    myVideo.Quality = 100;    % Default 75
    open(myVideo);
    
    for i=1:1000000
        try
            %disp([OutputName '/' num2str(i,'%05.f') '.png']);
            frame = imread([OutputName '/' num2str(i,'%05.f') '.png']);
            writeVideo(myVideo, frame);
            disp(['Frame: ' num2str(i) 'done']);
        catch
            break
        end
        
    end
    close(myVideo);
    
end



