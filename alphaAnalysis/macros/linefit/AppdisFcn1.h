#ifndef MN_AppdisFcn1_H_
#define MN_AppdisFcn1_H_
#include "Minuit2/FCNBase.h"
#include <vector>
namespace ROOT {
    namespace Minuit2 {
class AppdisFcn1 : public  FCNBase {
public:
  AppdisFcn1(const std::vector<double>& measa,
                const std::vector<double>& measd,
                const std::vector<double>& freq,
                const std::vector<double>& mvara,
                const std::vector<double>& mvard,
                const std::vector<double>& uwvar,
                const std::vector<double>& powvar) :
                theMeasa(measa),theMeasd(measd),theFreq(freq),theMvara(mvara),
                  theMvard(mvard), theUwvar(uwvar),
                thePowvar(powvar), theErrorDef(1.) {}

                ~AppdisFcn1() {}

  virtual double Up() const {return theErrorDef;}
  virtual double operator()(const std::vector<double>&) const;
  std::vector<double> Measa() const {return theMeasa;}
  std::vector<double> Measd() const {return theMeasd;}
  std::vector<double> Freq() const {return theFreq;}
  std::vector<double> Mvara() const {return theMvara;}
  std::vector<double> Mvard() const {return theMvard;}
  std::vector<double> Uwvar() const {return theUwvar;}
  std::vector<double> Powvar() const {return thePowvar;}
  void setErrorDef(double def) {theErrorDef = def;}
private:
 std::vector<double> theMeasa;
 std::vector<double> theMeasd;
 std::vector<double> theFreq;
 std::vector<double> theMvara;
 std::vector<double> theMvard;
 std::vector<double> theUwvar;
 std::vector<double> thePowvar;
 double theErrorDef;
};
    }
    }

#endif //MN_AppdisFcn1_H_
