#ifndef MN_AppearanceFcn_H_
#define MN_AppearanceFcn_H_
#include "Minuit2/FCNBase.h"
#include <vector>
namespace ROOT {
    namespace Minuit2 {
class AppearanceFcn : public  FCNBase {
public:
  AppearanceFcn(const std::vector<double>& meas,
                const std::vector<double>& freq,
                const std::vector<double>& mvar,
                const std::vector<double>& uwvar,
                const std::vector<double>& powvar) :
                theMeas(meas),theFreq(freq),theMvar(mvar), theUwvar(uwvar),
                thePowvar(powvar), theErrorDef(1.) {}

                ~AppearanceFcn() {}
  double sl_sr() const {return sl_sigmar;}
  double sl_gr() const  {return sl_gammar;}
  double sl_gb() const  {return sl_gammab;}
  double sl_NP() const  {return sl_NPower;}
  double sl_off() const {return sl_offset;}
  double NP0()   const  {return NPower0;}

  virtual double Up() const {return theErrorDef;}
  virtual double operator()(const std::vector<double>&) const;
  std::vector<double> Meas() const {return theMeas;}
  std::vector<double> Freq() const {return theFreq;}
  std::vector<double> Mvar() const {return theMvar;}
  std::vector<double> Uwvar() const {return theUwvar;}
  std::vector<double> Powvar() const {return thePowvar;}
  void setErrorDef(double def) {theErrorDef = def;}
private:
 std::vector<double> theMeas;
 std::vector<double> theFreq;
 std::vector<double> theMvar;
 std::vector<double> theUwvar;
 std::vector<double> thePowvar;
 double theErrorDef;
 // Power interpolation constants for lineshape from sims
 const double sl_offset=3.98;
 const double sl_sigmar=21.9;
 const double sl_gammar=-7.11;
 const double sl_gammab=67.72;
 const double sl_NPower=0.6412;
 const double NPower0 = 0.5131;
};
    }
    }

#endif //MN_AppearanceFcn_H_
