#ifndef MN_AppearanceFunction_H_
#define MN_AppearanceFunction_H_
#define _USE_MATH_DEFINES
#include <math.h>
#include <TMath.h>
#include <TRandom.h>
namespace ROOT { 
  namespace Minuit2 {
    class AppearanceFunction {
    public:
    AppearanceFunction(double peak,double sigmar, double gammar, double gammab) :
      the_peak(peak), the_sigmar(sigmar), the_gammar(gammar),the_gammab(gammab) {}
      ~AppearanceFunction() {}
      double c() const {return the_peak+the_offset;}
      double sr() const {return the_sigmar; }
      double gr() const {return the_gammar;}
      double gb() const {return the_gammab;}
      TRandom gRand= TRandom(11111);
      double operator()(double x) const {
        return ( x<c())?TMath::Voigt((x-c()),sr(),gr())/TMath::Voigt(0.,sr(),gr()):(0.25*gb()*gb()/((x-c())*(x-c()) + gb()*gb()*0.25));
      }
    private:
      double the_peak;
      double the_sigmar;
      double the_gammar;
      double the_gammab;
      //      double the_offset=gRand.Uniform(-10.,10.);
      double the_offset=0.;
    };
  }
}
#endif // MN_AppearanceFunction_H_
