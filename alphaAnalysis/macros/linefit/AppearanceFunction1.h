#ifndef MN_AppearanceFunction1_H_
#define MN_AppearanceFunction1_H_
#define _USE_MATH_DEFINES
#include <math.h>
#include <TMath.h>
#include <TRandom.h>
namespace ROOT { 
  namespace Minuit2 {

    class AppearanceFunction1 {
    public:
    AppearanceFunction1(double peak,double sigmar, double gammar, double gammab, double lb, double NAtom, double NPowera,double NPowerd, double bgd ) :
      the_peak(peak), the_sigmar(sigmar), the_gammar(gammar),the_gammab(gammab),the_lb(lb), the_NAtom(NAtom), the_NPowera(NPowera),the_NPowerd(NPowerd),the_bgd(bgd) {}
      ~AppearanceFunction1() {}
      double c() const {return the_peak + the_offset;}
      double sr() const {return the_sigmar;}
      double gr() const {return the_gammar;}
      double gb() const {return the_gammab;}
      double lb() const {return the_lb;}
      double na() const {return the_NAtom;}
      double npa() const {return the_NPowera;}
      double npd() const {return the_NPowerd;}
      double bg() const {return the_bgd;}
      TRandom gRand= TRandom(99999);
      double operator()(double x) const {
        return ( x<c())?TMath::Voigt((x-c()),sr(),gr())/TMath::Voigt(0.,sr(),gr()):((x<lb()+c())?(0.25*gb()*gb()/((x-c())*(x-c()) + gb()*gb()*0.25)):0.25*gb()*gb()*exp(-2.0*lb()*(x-c()-lb())/(0.25*gb()*gb()+lb()*lb()))/(0.25*gb()*gb()+lb()*lb()));
      }
    private:
      double the_peak;
      double the_sigmar;
      double the_gammar;
      double the_gammab;
      double the_lb;
      double the_NAtom;
      double the_NPowera;
      double the_NPowerd;
      double the_bgd;
      double the_offset=gRand.Uniform(-10.,10.);
    };
  }
}
#endif // MN_AppearanceFunction1_H_
