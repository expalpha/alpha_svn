#ifndef MN_TheFcn_H_
#define MN_TheFcn_H_
#include "Minuit2/FCNBase.h"
#include <vector>
namespace ROOT {
    namespace Minuit2 {
class TheFcn : public  FCNBase {
public:
  TheFcn(const std::vector<double>& measa,
                const std::vector<double>& measd,
                const std::vector<double>& freq,
                const std::vector<double>& mvara,
                const std::vector<double>& mvard,
                const std::vector<double>& uwvar,
                const std::vector<double>& powvar) :
                theMeasa(measa),theMeasd(measd),theFreq(freq),theMvara(mvara),
                  theMvard(mvard), theUwvar(uwvar),
                thePowvar(powvar), theErrorDef(1.) {}

                ~TheFcn() {}
  double sl_sr() const {return sl_sigmar;}
  double sl_gr() const  {return sl_gammar;}
  double sl_gb() const  {return sl_gammab;}
  double sl_NP() const  {return sl_NPower;}
  double sl_off() const {return sl_offset;}
  double NP0()   const  {return NPower0;}
  double disbgd() const {return FRDbgd;}
  void SetNSets(int ns)  {NSets=ns;}
  void SetNFreq(int nf)  {NFreq=nf;}
  void SetApmode( int mode)  {Apmode=mode;}

  virtual double Up() const {return theErrorDef;}
  virtual double operator()(const std::vector<double>&) const;
  std::vector<double> Measa() const {return theMeasa;}
  std::vector<double> Measd() const {return theMeasd;}
  std::vector<double> Freq() const {return theFreq;}
  std::vector<double> Mvara() const {return theMvara;}
  std::vector<double> Mvard() const {return theMvard;}
  std::vector<double> Uwvar() const {return theUwvar;}
  std::vector<double> Powvar() const {return thePowvar;}
  void setErrorDef(double def) {theErrorDef = def;}
private:
 std::vector<double> theMeasa;
 std::vector<double> theMeasd;
 std::vector<double> theFreq;
 std::vector<double> theMvara;
 std::vector<double> theMvard;
 std::vector<double> theUwvar;
 std::vector<double> thePowvar;
 double theErrorDef;
 // Power interpolation constants for lineshape from sims
 const double sl_offset=3.98;
 const double sl_sigmar=21.9;
 const double sl_gammar=-7.11;
 const double sl_gammab=67.72;
 const double sl_NPower=0.6412;
 const double NPower0 = 0.5107;
 const double FRDbgd = 6.423;
 int NSets=4;
 int NFreq=14;
 int Apmode=4;//1=appearance 2=disappearance 3=combined 4=sim
};
    }
    }

#endif //MN_TheFcn_H_
