function out=Voigt_Lorentz_exponential(a,b,gamma1,sigma1,c2,d2,x)

%calculate factors for grafting on exponential
e2=(2*(d2-b))/((d2-b)^2+c2); 
a2=(a*c2*exp(e2*d2))/((d2-b)^2+c2);


out=a*real(fadf(((x-b)+1i*gamma1)/(sigma1*sqrt(2))))...
    ./real(fadf((1i*gamma1)/(sigma1*sqrt(2)))).*(x<b)...
    +a*c2./((x-b).^2+c2).*(x>=b).*(x<d2)+a2.*exp(-e2.*x).*(x>=d2);
end
