function out=fit4powerfun(P1,P2,P3,P4,offset,x)

set1=powerfun(P1,offset,x).*(x<1e3);
set2=powerfun(P2,offset,x-1400).*(x>1e3).*(x<2e3);
set3=powerfun(P3,offset,x-2400).*(x>2e3).*(x<3e3);
set4=powerfun(P4,offset,x-3400).*(x>3e3);
out = set1+set2+set3+set4;
end

