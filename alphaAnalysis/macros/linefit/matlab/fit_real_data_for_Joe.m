%% combination power fit (4 powers, one offset):
clear; close all; clc
load josephdata.mat


atoms=A(:,[1 3 5 7]);
atomerrors=A(:,[2 4 6 8]);
clear A
L=atoms(:,1);
M=atoms(:,2);
S=atoms(:,3);
N=atoms(:,4);
Lerr=atomerrors(:,1);
Merr=atomerrors(:,2);
Serr=atomerrors(:,3);
Nerr=atomerrors(:,4);


%change in calculation at 243nm:
deltaf=523/2/1000;%kHz
detuns_1=[-200 -100 0 100]-deltaf;
detuns_2=[-200 -25 0 25]-deltaf;
detuns_3=[-200 0 50 200]-deltaf;
detuns_4=[-200 -50 0 25]-deltaf;

%   L   M   S   N   (:,1 2 3 4)
%  -200 -100 0 100 -200 -25 0 25 -200 0 50 200 -200 -50 0 25
%   O1    C  B  A   O2   D  E  F  O3  G  I  H   O4   L  K  J

disappearance_1 = (S(1)-S(1:4)); %set1
disappearance_2 = (S(5)-S(5:8)); %set2
disappearance_3 = (S(9)-S(9:12)); %set3
disappearance_4 = (S(13)-S(13:16)); %set4

appearance_1= L(1:4);
appearance_2= L(5:8);
appearance_3= L(9:12);
appearance_4= L(13:16);

err_disappearance_1 = sqrt(Serr(1)^2+Serr(1:4).^2); %set1
err_disappearance_2 = sqrt(Serr(5)^2+Serr(5:8).^2); %set2
err_disappearance_3 = sqrt(Serr(9)^2+Serr(9:12).^2); %set3
err_disappearance_4 = sqrt(Serr(13)^2+Serr(13:16).^2); %set4

err_appearance_1= Lerr(1:4);
err_appearance_2= Lerr(5:8);
err_appearance_3= Lerr(9:12);
err_appearance_4= Lerr(13:16);

ff=linspace(-300,300,1000);
FONTSIZE=16;
MARKERSIZE=10;


total_1=mean(N(1:4))/2;
total_2=mean(N(5:8))/2;
total_3=mean(N(9:12))/2;
total_4=mean(N(13:16))/2;


%for amplitude permutation study:
% total_1=mean(N(1:4)-Nerr(1:4))/2;
% total_2=mean(N(5:8)+Nerr(5:8))/2;
% total_3=mean(N(9:12)+Nerr(9:12))/2;
% total_4=mean(N(13:16)+Nerr(13:16))/2;


normfac=0.9382; %from off res [700 800 900 1000 1250]mW dd only

% make 4 domains for the fit. 
%Go to ratios for the fit (divide through by atoms)
xx=[detuns_1 detuns_1 detuns_2+1400 detuns_2+1400 detuns_3+2400 detuns_3+2400 detuns_4+3400 detuns_4+3400]';
S=[disappearance_1./total_1 ; appearance_1./total_1; disappearance_2./total_2 ; appearance_2./total_2; disappearance_3./total_3 ; appearance_3./total_3; disappearance_4./total_4 ; appearance_4./total_4].*normfac;
weights=1./([err_disappearance_1./total_1 ; err_appearance_1./total_1 ; err_disappearance_2./total_2 ; err_appearance_2./total_2 ; err_disappearance_3./total_3 ; err_appearance_3./total_3 ; err_disappearance_4./total_4 ; err_appearance_4./total_4].*normfac).^2;
fitfun= @(P1,P2,P3,P4,b,x) fit4powerfun(P1./1000,P2./1000,P3./1000,P4./1000,b,x);
fopt=fitoptions('Method','NonlinearLeastSquares','StartPoint',[900 900 900 900 0],...
   'Lower',[700 700 700 700 -10],'Upper',[1200 1200 1200 1200 10],'TolFun',1e-8,'TolX',1e-8,'Weights',weights);
[fit_combi,gof]=fit(xx,S,fitfun,fopt)


fig5h=figure('Position',[100 100 800 600]); hold on;
ax5=gca; 
errorbar(detuns_1,disappearance_1,err_disappearance_1,err_disappearance_1,'bs','Markersize',MARKERSIZE)
errorbar(detuns_1,appearance_1,err_appearance_1,err_appearance_1,'ro','Markersize',MARKERSIZE)
plot(ff,fitfun(fit_combi.P1,fit_combi.P2,fit_combi.P3,fit_combi.P4,fit_combi.b,ff).*total_1./normfac,'k')
set(ax5,'fontsize',FONTSIZE,'xlim',[-300 300])
xlabel('detuning (kHz)')
ylabel('atoms')
legend('Disappearance','Appearance','Fit')

fig6h=figure('Position',[100 100 800 600]); hold on;
ax6=gca;
errorbar(detuns_2,disappearance_2,err_disappearance_2,err_disappearance_2,'bs','Markersize',MARKERSIZE)
errorbar(detuns_2,appearance_2,err_appearance_2,err_appearance_2,'ro','Markersize',MARKERSIZE)
plot(ff,fitfun(fit_combi.P1,fit_combi.P2,fit_combi.P3,fit_combi.P4,fit_combi.b,ff+1400).*total_2./normfac,'k')
set(ax6,'fontsize',FONTSIZE,'xlim',[-300 300])
xlabel('detuning (kHz)')
ylabel('atoms')
legend('Disappearance','Appearance','Fit')

fig7h=figure('Position',[100 100 800 600]); hold on;
ax7=gca;
errorbar(detuns_3,disappearance_3,err_disappearance_3,err_disappearance_3,'bs','Markersize',MARKERSIZE)
errorbar(detuns_3,appearance_3,err_appearance_3,err_appearance_3,'ro','Markersize',MARKERSIZE)
plot(ff,fitfun(fit_combi.P1,fit_combi.P2,fit_combi.P3,fit_combi.P4,fit_combi.b,ff+2400).*total_3./normfac,'k')
set(ax7,'fontsize',FONTSIZE,'xlim',[-300 300])
xlabel('detuning (kHz)')
ylabel('atoms')
legend('Disappearance','Appearance','Fit')

fig8h=figure('Position',[100 100 800 600]); hold on;
ax8=gca;
errorbar(detuns_4,disappearance_4,err_disappearance_4,err_disappearance_4,'bs','Markersize',MARKERSIZE)
errorbar(detuns_4,appearance_4,err_appearance_4,err_appearance_4,'ro','Markersize',MARKERSIZE)
plot(ff,fitfun(fit_combi.P1,fit_combi.P2,fit_combi.P3,fit_combi.P4,fit_combi.b,ff+3400).*total_4./normfac,'k')
set(ax8,'fontsize',FONTSIZE,'xlim',[-300 300])
xlabel('detuning (kHz)')
ylabel('atoms')
legend('Disappearance','Appearance','Fit')

confidence=confint(fit_combi);
disp(['P1 = ' num2str(fit_combi.P1) ' +/- ' num2str((confidence(2,1)-confidence(1,1))/4) ])
disp(['P2 = ' num2str(fit_combi.P2) ' +/- ' num2str((confidence(2,2)-confidence(1,2))/4) ])
disp(['P3 = ' num2str(fit_combi.P3) ' +/- ' num2str((confidence(2,3)-confidence(1,3))/4) ])
disp(['P4 = ' num2str(fit_combi.P4) ' +/- ' num2str((confidence(2,4)-confidence(1,4))/4) ])
disp(['offset = ' num2str(fit_combi.b) ' +/- ' num2str((confidence(2,5)-confidence(1,5))/4) ' (kHz @ 243nm)'])
disp(['offset = ' num2str(fit_combi.b*2) ' +/- ' num2str((confidence(2,5)-confidence(1,5))/2) ' (kHz @ 121nm)'])

