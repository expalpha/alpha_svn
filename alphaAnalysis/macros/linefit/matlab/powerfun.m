function out=powerfun(P,offset,x)
%load 	
load powerfit_params.mat
% should be good for powers between 700mW and 1250mW
% a = 0.6332.*P - 0.1277;
% b = -30.9.*P.^3 + 89.33.*P.^2 - 78.57.*P +24.57;
% c = -2736.*P.^2 + 8244.*P - 3260;
% d = 158.1.*P.^3 - 1103.*P.^2 +1843.*P -681.4;
% gamma = 204.1.*P.^3 - 641.3.*P.^2 + 643.6.*P - 187.3;
% sigma = -181.9.*P.^3 + 529.7.*P.^2 - 468.9.*P + 142.1;
a_val=a(P);
b_val=b(P);
c_val=c(P);
d_val=d(P);
gamma_val=gamma_param(P);
sigma_val=sigma_param(P);

out=Voigt_Lorentz_exponential(a_val,b_val+offset,gamma_val,sigma_val,c_val,d_val,x);
end

 