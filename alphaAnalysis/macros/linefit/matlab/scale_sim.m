clear; close all; clc

%cd /Users/Orum/cernbox/ALPHA/Simulation/width_fix/flat_2017/

pre='xv1s2s_';
mid='mW_300s_';
post='kHz_10kHz_50K_10K_200um_';
detuns= [-200 -100 -50 -25 -12 -5 0 5 12 25 50 100 200 300]; %kHz %add -5 kHz
powers=[700 800 900 1000 1100 1250];

Tquench=300;
dions=zeros(length(powers),length(detuns));
flippedd=zeros(length(powers),length(detuns));
inquenchd=zeros(length(powers),length(detuns));
lostd=zeros(length(powers),length(detuns));
totald=zeros(length(powers),length(detuns));


Nfiles=0;
for powerj=1:length(powers)
    for j=1:length(detuns)
        dum=[];
        for jj=0:19
            try
                dum=[dum; importdata([pre num2str(powers(powerj)) mid num2str(detuns(j)) post num2str(jj) '.dat'])];
                Nfiles=Nfiles+1;
            catch
%                 disp(['detun = ' num2str(detuns(j)) 'kHz with seed = ' num2str(jj) ' not found'])
            end
        end
        
        tim=dum(:,3);
        channel=dum(:,2); % 0= 1S_L 2=flip 3=ion
        state=dum(:,1); % 0=d-d 1=c-c
        dstarted=sum(state==0);
        if sum(isnan(dum(:)))
            disp(['There are NaNs: powerj = ' num2str(powerj) ', j = ' num2str(j)])
        end
        
        dions(powerj,j)=sum(channel==3 & state==0 & tim<Tquench)/dstarted;
        flippedd(powerj,j)=sum(channel==2 & tim<Tquench & state==0)/dstarted;
        inquenchd(powerj,j)=sum(tim>=Tquench & state==0)/dstarted;
        lostd(powerj,j)=sum(channel==0 & tim<Tquench & state==0)/dstarted;
        totald(powerj,j)=dstarted;
    end
end
%%
close all; clc

errs=sqrt(totald.*(dions+flippedd).*(1-dions-flippedd)) ./ totald;

FONTSIZE=16;
MARKERSIZE=5;

% fig1h=figure('Position',[100 100 800 600]); hold on;
% ax1=gca;
linfits=zeros(2,length(detuns));
pp=linspace(powers(1),powers(end),10);


for j=1:length(detuns)
    fig1h=figure('Position',[100 100 800 600]); hold on;
    errorbar(powers,flippedd(:,j)+dions(:,j),errs(:,j),errs(:,j),'k.')
    set(gca,'fontsize',FONTSIZE)
    title(num2str(detuns(j)))
    P=polyfit(powers',flippedd(:,j)+dions(:,j),1);
    plot(pp,polyval(P,pp),'k')
    FN=['/Users/Orum/Dropbox/ALPHA/Analysis17/sim_interp/sim_interp_' num2str(detuns(j)) 'kHz.png'];
%     saveas(fig1h,FN,'png')
%     pause(1)
    linfits(:,j)=P;
end

% cd /Users/Orum/cernbox/ALPHA/Simulation/width_fix/
% save('sim_lin_fits.mat','detuns','linfits')

%% plot 3D sim
close all; clc
figure
hold on
for j=1:length(powers)
    plot3(powers(j).*ones(size(detuns)),detuns,flippedd(j,:)+dions(j,:),'b-x')
end

grid on
xlabel('power (mW)')
ylabel('detuning (kHz)')
zlabel('ions + spin-flip')

%% surface plot with sim:
close all; clc
cd /Users/Orum/cernbox/ALPHA/Simulation/width_fix/
load sim_lin_fits.mat

Ps=700:10:1250;
detunlength=200;
Z=zeros(length(Ps),detunlength);

for jj=1:length(Ps)

power=Ps(jj);

sim_scaled=zeros(1,length(detuns));
for j=1:length(detuns)
    sim_scaled(j)=polyval(linfits(:,j),power);
end

ff=linspace(-200,300,detunlength);

Artfitfun= @(a,b,gamma1,sigma1,c2,d2,x) Voigt_Lorentz_exponential(a,b,gamma1,sigma1,c2,d2,x);
fopt=fitoptions('Method','NonlinearLeastSquares','StartPoint',[max(sim_scaled) 7 9 35 2000 150],...
   'Lower',[0 -20 0  0 0 50 ],'Upper',[1 15 inf inf 10000 300 ]);
[Artfit,gof]=fit(detuns',sim_scaled',Artfitfun,fopt);
Artfun=@(x) Artfitfun(Artfit.a,Artfit.b,Artfit.gamma1,Artfit.sigma1,Artfit.c2,Artfit.d2,x);
Z(jj,:)=Artfun(ff);

end
%% do the plot for extended data:
close all; clc
figure('Position',[100 100 1000 800])


hold on
for j=1:length(powers)
    %plot a smooth curve
    power=powers(j);
    sim_scaled=zeros(1,length(detuns));
    for jj=1:length(detuns)
        sim_scaled(jj)=polyval(linfits(:,jj),power);
    end
    Artfitfun= @(a,b,gamma1,sigma1,c2,d2,x) Voigt_Lorentz_exponential(a,b,gamma1,sigma1,c2,d2,x);
    fopt=fitoptions('Method','NonlinearLeastSquares','StartPoint',[max(sim_scaled) 7 9 35 2000 150],...
        'Lower',[0 -20 0  0 0 50 ],'Upper',[1 15 inf inf 10000 300 ]);
    [Artfit,gof]=fit(detuns',sim_scaled',Artfitfun,fopt);
    Artfun=@(x) Artfitfun(Artfit.a,Artfit.b,Artfit.gamma1,Artfit.sigma1,Artfit.c2,Artfit.d2,x);
    plot3(power.*ones(size(ff)),ff,Artfun(ff),'r','linewidth',1.1)
    %plot simulation points:
    plot3(powers(j).*ones(size(detuns)),detuns,flippedd(j,:)+dions(j,:),'kx','linewidth',1,'markersize',15)
end
% plot3(powers(j).*ones(size(detuns)),detuns,flippedd(j,:)+dions(j,:),'kx','linewidth',1,'markersize',15)
surf(Ps,ff,Z')



alpha 0.6
shading flat
set(gca,'fontsize',15,'xlim',[700 1250],'Ydir','reverse')
grid on
xlabel('Power (mW)')
ylabel('Detuning (kHz)')
zlabel('Signal a.u.')
view(-50,30)
legend('Interpolated curve','Simulation','Location','Best')

% set(gca,'color','none')
% cd /Users/Orum/Dropbox/ALPHA/Analysis17
% export_fig simulation_interp_3D.pdf -transparent

%% fit at arbitrary power
close all; clear; clc
cd /Users/Orum/cernbox/ALPHA/Simulation/width_fix/
load sim_lin_fits.mat

Ps=700:10:1250;

for jj=1:length(Ps)

power=Ps(jj);

sim_scaled=zeros(1,length(detuns));
for j=1:length(detuns)
    sim_scaled(j)=polyval(linfits(:,j),power);
end

% plot(detuns,sim_scaled,'bx')
% hold on
ff=linspace(-200,300,500);

Artfitfun= @(a,b,gamma1,sigma1,c2,d2,x) Voigt_Lorentz_exponential(a,b,gamma1,sigma1,c2,d2,x);
fopt=fitoptions('Method','NonlinearLeastSquares','StartPoint',[max(sim_scaled) 7 9 35 2000 130],...
   'Lower',[0 -20 0  0 0 50 ],'Upper',[1 15 inf inf 10000 250 ]...
   ,'TolFun',1e-8,'TolX',1e-8,'MaxIter',800,'MaxFunEvals',1200);
[Artfit,gof]=fit(detuns',sim_scaled',Artfitfun,fopt)
Artfun=@(x) Artfitfun(Artfit.a,Artfit.b,Artfit.gamma1,Artfit.sigma1,Artfit.c2,Artfit.d2,x);
% plot(ff,Artfun(ff),'b')
as(jj)=Artfit.a;
bs(jj)=Artfit.b;
gammas(jj)=Artfit.gamma1;
sigmas(jj)=Artfit.sigma1;
cs(jj)=Artfit.c2;
ds(jj)=Artfit.d2;

% xlabel('power (mW)')

end
%% parameterize by power
close all; clc
cd /Users/orum/Dropbox/ALPHA/Analysis17/
 
figure
hold on
plot(Ps,as,'x')
P_a=polyfit(Ps./1000,as,1);
a = @(P) polyval(P_a,P);
plot(Ps,a(Ps./1000))
title('a')

figure
hold on
plot(Ps,bs,'x')
P_b=polyfit(Ps./1000,bs,4);
b = @(P) polyval(P_b,P);
plot(Ps,b(Ps./1000))
title('b')

figure
hold on
plot(Ps,gammas,'x')
P_gamma=polyfit(Ps./1000,gammas,4);
gamma_param = @(P) polyval(P_gamma,P);
plot(Ps,gamma_param(Ps./1000))
title('gamma')

figure
hold on
plot(Ps,sigmas,'x')
P_sigma=polyfit(Ps./1000,sigmas,4);
sigma_param = @(P) polyval(P_sigma,P);
plot(Ps,sigma_param(Ps./1000))
title('sigma')

figure
hold on
plot(Ps,cs,'x')
P_c=polyfit(Ps./1000,cs,2);
c = @(P) polyval(P_c,P);
plot(Ps,c(Ps./1000))
title('c')

figure
hold on
plot(Ps,ds,'x')
P_d=polyfit(Ps./1000,ds,6);
d = @(P) polyval(P_d,P);
plot(Ps,d(Ps./1000))
title('d')

fG=2.*sigmas.*sqrt(2*log(2));
fL=2.*gammas;
fV=0.5346.*fL+sqrt(0.2166.*fL.^2+fG.^2);
FWHM_Lorentz=2.*sqrt(cs);
FWHMs=(fV+FWHM_Lorentz)./2;
figure
hold on
plot(Ps,FWHMs,'x')
title('FWHM')

% xlabel('power (mW)')

% cd /Users/Orum/cernbox/ALPHA/Simulation/width_fix/
% save('powerfit_params.mat','a','b','c','d','gamma_param','sigma_param')

%% test full lineshapes
clear; close all; clc
cd /Users/Orum/cernbox/ALPHA/Simulation/width_fix/
load sim_lin_fits.mat
load powerfit_params.mat
cd /Users/orum/Dropbox/ALPHA/Analysis17/

power=1000;
figure('Position',[200 150 800 600])
sim_scaled=zeros(1,length(detuns));
for j=1:length(detuns)
    sim_scaled(j)=polyval(linfits(:,j),power);
end

plot(detuns,sim_scaled,'bx')
hold on
ff=linspace(-300,300,500);

Artfitfun= @(a,b,gamma1,sigma1,c2,d2,x) Voigt_Lorentz_exponential(a,b,gamma1,sigma1,c2,d2,x);
fopt=fitoptions('Method','NonlinearLeastSquares','StartPoint',[max(sim_scaled) 7 9 35 2000 150],...
   'Lower',[0 -20 0  0 0 50 ],'Upper',[1 15 inf inf 10000 300 ]);
[Artfit,gof]=fit(detuns',sim_scaled',Artfitfun,fopt);
Artfun=@(x) Artfitfun(Artfit.a,Artfit.b,Artfit.gamma1,Artfit.sigma1,Artfit.c2,Artfit.d2,x);
plot(ff,Artfun(ff),'b')

fitfun_power = @(P,x) powerfun(P./1000,0,x);
fopt=fitoptions('Method','NonlinearLeastSquares','StartPoint',[900],...
   'Lower',[700],'Upper',[1250]);
[powerfit,gof]=fit(detuns',sim_scaled',fitfun_power,fopt)
plot(ff,powerfun(powerfit.P/1000,0,ff),'r')

