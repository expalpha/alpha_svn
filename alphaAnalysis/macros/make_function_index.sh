#!/bin/bash

CMD=`ctags -u -f - --c-kinds=-v $1`
#CMD=`ctags -u -f - --c-kinds=-v -x /%s/\t/%s $1`
#CMD=`etags -f - --no-globals $1`
IFS=$'\n'

echo "// ******************************************************* "> $1.idx
echo "// Automatically Generated Function Index for $1">> $1.idx
echo "// ******************************************************* ">> $1.idx
for LINE in $CMD; do
    echo "// $LINE">> $1.idx
done
