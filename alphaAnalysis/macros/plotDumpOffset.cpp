#include "PlotMixing.cpp"

TH1D*  PlotAroundDump (Int_t runNumber, char* dumpName, Int_t channel, Double_t offset, Double_t length, Int_t dumpRep=1, Int_t plotRep=1)
{
	Int_t dumpNumber;

	dumpNumber = MatchDumpNameToDumpNumber(runNumber, dumpName, dumpRep);

	TH1D* h = PlotAroundTrigger(runNumber, SIS_DUMP_START, dumpNumber, channel, offset, length, plotRep);

	return h;
}


TH1D* PlotAroundTrigger(Int_t runNumber, Int_t refChannel, Int_t trigRep, Int_t channel, Double_t offset, Double_t length, Int_t plotRep=1)
{
	Double_t refTime = Get_RunTime_of_Count(runNumber, refChannel, trigRep);
	TH1D* h = GetHistoByTimes(runNumber, channel, refTime+offset, refTime+offset+length);
	Double_t theIntegral = h->Integral();
	h->GetXaxis()->SetLimits(offset, offset+length);
	h->SetXTitle("Time [s]");

	gHistoCounter = plotRep;
	FormatHisto(h);
	sprintf(text, "Integral = %.0lf", theIntegral);
	AddRandomText(text, plotRep+1, 0.45, plotRep-1);
	
	return h;
	
}


void PlotAroundDumpOverlay (char* filename, char* dumpName, Int_t channel, Double_t offset, Double_t length)
{
	Int_t runNumber[500], dumpNumber[500];
	
	Int_t nRuns = FileToRuns(filename, runNumber, dumpNumber);
	
	for(Int_t i=0; i<nRuns; i++)
	{
		gHistoCounter = i+1;
		plotAroundDump(runNumber[i], dumpName, channel, offset, length, i+1);
	}

}

void PlotAroundTriggerOverlay (char* filename, Int_t refChannel, Int_t trigRep, Int_t channel, Double_t offset, Double_t length)
{
	Int_t runNumber[500], dumpNumber[500];
	Int_t nRuns = FileToRuns(filename, runNumber, dumpNumber);
	
	for(Int_t i=0; i<nRuns; i++)
	{
		gHistoCounter = i+1;
		plotAroundTrigger(runNumber[i], refChannel, trigRep, channel, offset, length, i+1);
	}

}


void PlotAroundDumpAveraged(char* filename, char* dumpName, Int_t channel, Double_t offset, Double_t length, Int_t dumpRep=1,Int_t plotRep=1)
{
	Double_t startTime, endTime;
	Int_t runNumber[1000], dumpNumber, dumps[1000];
	
	Int_t nRuns = FileToRuns(filename, runNumber, dumps);
	
	TH1D *thisHisto, *histo;
	
	for(Int_t i=0; i<nRuns; i++)
	{

		dumpNumber = MatchDumpNameToDumpNumber(runNumber[i], dumpName, dumpRep);
     
		GetDumpTimes(runNumber[i], 1, dumpNumber, &startTime, &endTime);

		thisHisto = GetHistoByTimes(runNumber[i], channel,  startTime+offset, startTime+offset+length);
		thisHisto->GetXaxis()->SetLimits(offset, offset+length);
	
		if(i==0)histo = (TH1D*)thisHisto->Clone();
		else histo->Add(thisHisto);
	
		//thisHisto->Delete();

	}
	
	
	gHistoCounter = plotRep;
	gLastHistoPointer = histo;
	sprintf(text, "Histo");
	histo->SetYTitle("Counts");
	histo->Scale(1./nRuns);
	histo->SetTitle(text);
	Double_t theIntegral = histo->Integral();
	
	histo->SetXTitle("Time [s]");
	FormatHisto(histo, kFALSE);	

	sprintf(text, "Integral = %.0lf", theIntegral);
	AddRandomText(text, plotRep, 0.45, plotRep);	
	
} 

void PlotAroundTriggerAveraged(char* filename, Int_t refChannel, Int_t trigRep, Int_t channel, Double_t offset, Double_t length, Int_t plotRep=1)
{
	Double_t refTime;
	Int_t runNumber[1000], dumpNumber, dumps[1000];
	
	Int_t nRuns = FileToRuns(filename, runNumber, dumps);
	
	TH1D *thisHisto, *histo;
	
	for(Int_t i=0; i<nRuns; i++)
	{

		refTime = Get_RunTime_of_Count(runNumber[i], refChannel, trigRep);

		thisHisto = GetHistoByTimes(runNumber[i], channel, refTime+offset, refTime+offset+length);
		thisHisto->GetXaxis()->SetLimits(offset, offset+length);
	
		if(i==0)histo = (TH1D*)thisHisto->Clone();
		else histo->Add(thisHisto);
	
		//delete thisHisto;

	}
	
	
	gHistoCounter = plotRep;
	gLastHistoPointer = histo;
	sprintf(text, "Histo");
	histo->SetYTitle("Counts");
	histo->Scale(1./nRuns);
	histo->SetTitle(text);
	
	Double_t theIntegral = histo->Integral();
	histo->SetXTitle("Time [s]");
	FormatHisto(histo, kFALSE);	
	sprintf(text, "Integral = %.0lf", theIntegral);
	AddRandomText(text, plotRep, 0.45, plotRep);	
	
} 

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//ANDREA CUSTOM
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

void PlotAroundDump (char* filename, Int_t channel, Double_t offset, Double_t length, char* text="histo", Int_t dumpAdd=0)
{
	Double_t startTime, endTime;
	Int_t runNumber[1000], dumpNumber, dumps[1000];
	
	Int_t nRuns = FileToRuns(filename, runNumber, dumps);
	
	TH1D *thisHisto, *histo;
	
	for(Int_t i=0; i<nRuns; i++)
	{

	dumpNumber = dumps[i];
    	dumpNumber = dumpNumber + dumpAdd;
	GetDumpTimes(runNumber[i], 1, dumpNumber, &startTime, &endTime);

	thisHisto = GetHistoByTimes(runNumber[i], channel,  startTime+offset, startTime+offset+length);
	Info("", "Run %d, [%.3lf -> %.3lf], Chan %d : Integral %.1lf", runNumber[i], startTime+offset, startTime+offset+length, channel, thisHisto->Integral());
	thisHisto->GetXaxis()->SetLimits(offset, offset+length);
	thisHisto->Sumw2();
	
	if(i==0)histo = (TH1D*)thisHisto->Clone();
	else histo->Add(thisHisto);
	
	delete thisHisto;

	}
	
	gLastHistoPointer = histo;
	//sprintf(text, "Histo");
	histo->SetYTitle("Counts");
	//histo->SetYTitle("Rate [Hz]");
	//histo->Scale(1./gHISTO_RESOLUTION);
	//histo->Scale(1./nRuns);
	histo->SetTitle(text);
	histo->SetName(text);
	histo->SetXTitle("Time [s]");
	FormatHisto(histo);		

    
} 


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//EOIN CUSTOM
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

void showPlot()
{
	gStyle->SetOptStat(00000000);
	TFile* f = new TFile("mix_214valid.root");
	TH1D* histo = (TH1D*)f->Get("MixingHisto");
	
	TCanvas* c = new TCanvas("plots", "plots");
	c->Divide(1,3);
	//c->Divide(1,2);
	c->cd(1);
	
	Double_t startAR(3e-3), histHeight, ARLength(1.081e-3);
	
	Int_t rebin=2;
	histo->Rebin(rebin);
	histo->Scale(1./histo->GetBinWidth(1));
	
	histo->GetXaxis()->SetRangeUser(0., 5e-3);
	histo->SetMarkerStyle(7);
	histo->SetMarkerColor(2);
	histo->Draw("p");
	histo->GetYaxis()->SetTitle("Rate [Hz]");
	histo->GetXaxis()->SetTitle("Time [s]");
	histo->SetTitle("Mixing Triggers");
	
	histHeight = histo->GetMaximum();
		
	TLine* startARl = new TLine(startAR, 0., startAR, histHeight);
	startARl->SetLineColor(4);
	startARl->SetLineStyle(2);
	startARl->Draw();
	TLine* stopARl = new TLine(startAR+ARLength, 0., startAR+ARLength, histHeight);
	stopARl->SetLineColor(4);
	stopARl->SetLineStyle(2);
	stopARl->Draw();
	
	c->cd(3);
	Int_t nARPoints=1000;
	Double_t time[1000], freq[1000];
	Double_t maxTime=5e-3;
	Double_t ARstartFreq(320e3), ARstopFreq(200e3), ARon(10./320e3), ARoff(10./200e3);
	for(Int_t i=0; i<nARPoints; i++)
	{
		time[i] = maxTime/(nARPoints-1)*(Double_t)i;
		if(time[i]<startAR)
		{
			freq[i]=0.;
			continue;
		}
		if(time[i]<startAR+ARon)
		{
			freq[i] = ARstartFreq;
			continue;
		}
		if(time[i]>startAR+ARLength)
		{
			freq[i]=0.;
			continue;
		}
		if(time[i]>startAR+ARLength-ARoff)
		{
			freq[i]=ARstopFreq;
		}
		freq[i] = (time[i]-startAR-ARon)/(ARLength-ARon-ARoff)*(ARstopFreq-ARstartFreq)+ARstartFreq;
	}
	
	TGraph* ARGraph = new TGraph(nARPoints, time, freq);
	ARGraph->SetLineColor(2);
	ARGraph->Draw("AL");
	ARGraph->GetXaxis()->SetRangeUser(0., 5e-3);
	ARGraph->SetTitle("AR Chirp");
	ARGraph->GetYaxis()->SetTitle("Frequency [Hz]");
	ARGraph->GetXaxis()->SetTitle("Time [s]");
	
	/*
	c->cd(3);
	TH1D* freqInj = new TH1D("FreqInj","FreqInj", 100, 180e3, 350e3);
	for (Int_t i=1; i<histo->GetNbinsX()+1; i++)
	{
		freqInj->Fill(ARGraph->Eval(histo->GetBinCenter(i)), histo->GetBinContent(i)*histo->GetBinWidth(i));
	}
	freqInj->SetMarkerStyle(7);
	freqInj->Draw("P");
	
	TCanvas* cc = new TCanvas();
	cc->cd();
	*/
	c->cd(2);
	/*
	FILE* mcpFile = fopen("20091129_ARInject_MCPTrace.dat","r");
	
	Double_t timeM[50000], signalM[50000];
	Double_t tempTime, tempSignal, timeOffset;
	Int_t counter(1);
	char line[1000];
	Int_t stat;
	
	timeM[0] = 0.;
	signalM[0]=0.;
	
	while(!feof(mcpFile))
	{
		//fgets(line, 990, mcpFile);
		fscanf(mcpFile,"%lf %lf", &tempTime, &tempSignal);
		//stat = sscanf(line,"%lf %lf", &tempTime, &tempSignal);
		//printf("%d : %d\n", counter, stat);
		if(counter==1) timeOffset = tempTime;
		timeM[counter] = tempTime-timeOffset+3e-3-1e-4;
		signalM[counter] = tempSignal;
		//printf("%d\t%lf\t%lf\n", counter, tempTime, tempSignal);
		//printf("%d\t%lf\t%lf\n", counter, timeM[counter], signalM[counter]);
		counter++;
		//printf("%s\n", line);
	}
	
	TGraph* mcpGraph = new TGraph(counter, timeM, signalM);
	//mcpGraph->GetXaxis()->SetLimits(-3e-3, timeM[counter]-3e-3);
	mcpGraph->GetXaxis()->SetRangeUser(0, 5e-3);
	mcpGraph->GetYaxis()->SetRangeUser(0, 2500);
	mcpGraph->Draw("AL");
	*/
	SetDrawOpts("HIST");
	plotAroundDump(15528, "Left", 37, -2.9e-3, 5e-3);
	
	TH1D* theHist = gLastHistoPointer;
	for(Int_t i = 2; i<theHist->GetNbinsX()+1; i++) 
	{
		if(theHist->GetBinCenter(i)<1.25e-3)theHist->SetBinContent(i, theHist->GetBinContent(i)*theHist->GetBinWidth(i)+theHist->GetBinContent(i-1));
		else theHist->SetBinContent(i,0);
	}
	theHist->SetTitle("Injection");
	theHist->SetMarkerColor(4);
	theHist->GetYaxis()->SetTitle("Integrated Counts");
	theHist->GetXaxis()->SetLimits(0, 5e-3);
	theHist->Draw("P");
	
}

void plotMixingExample()
{
	gHistoCounter=1;
	SetTimeResolution(5.e-4);
	plotAroundDump(13526, "Mixing", SIS2_TTC_SI_TRIG, -.02, .04,1);
	gHistoCounter++;
//	plotAroundDump(13526, "Mixing", SIS_PMT_DEG_OR, -.02, .04, 2);
	gHistoCounter++;
//	plotAroundDump(13526, "Mixing", SIS_PMT_DOWN_OR, -.02, .04, 3);
	
	Double_t height(1.5e6), textOffset(1e-3);
	
	Double_t mixTime(0.);
	TLine* mixLine = new TLine(mixTime, 0., mixTime, height);
	mixLine->SetLineColor(1);
	mixLine->SetLineStyle(2);
	mixLine->SetLineWidth(1);
	mixLine->Draw();
	
	TText* mixText = new TText(mixTime+textOffset, height*0.85, "Start Mixing");
	mixText->SetTextColor(2);
	mixText->SetTextSize(0.033);
	mixText->SetTextAngle(45.);
	mixText->Draw();
	
	
	Double_t injectTime(-7e-3);
	TLine* injectLine = new TLine(injectTime, 0., injectTime, height);
	injectLine->SetLineColor(1);
	injectLine->SetLineStyle(2);
	injectLine->SetLineWidth(1);
	injectLine->Draw();
	
	TText* injectText = new TText(injectTime+textOffset, height*0.85, "Start Injection");
	injectText->SetTextColor(2);
	injectText->SetTextSize(0.033);
	injectText->SetTextAngle(45.);
	injectText->Draw();
	
	Double_t injectDoneTime(-4e-3);
	TLine* injectDoneLine = new TLine(injectDoneTime, 0., injectDoneTime, height);
	injectDoneLine->SetLineColor(1);
	injectDoneLine->SetLineStyle(2);
	injectDoneLine->SetLineWidth(1);
	injectDoneLine->Draw();
	
	TText* injectDoneText = new TText(injectDoneTime+textOffset, height*0.85, "Injection Done");
	injectDoneText->SetTextColor(2);
	injectDoneText->SetTextSize(0.033);
	injectDoneText->SetTextAngle(45.);
	injectDoneText->Draw();
}

savePlot()
{
	TFile* f = new TFile("mix_214valid.root", "RECREATE");
	
	TH1D* h = GetAveragedDumpHisto("run_info/20091208_series2_214valid.runs", "Mixing", 1, 51);
	h->SetName("MixingHisto");
	f->cd();
	h->Write();
	f->Close();
}

void PlotDumpZoom (Int_t runNumber, char* dumpName,Int_t channel, Double_t offset, Double_t length, Double_t timeResolution = 0.001, Int_t rep=1)
{
  
	Double_t startTime, endTime;
	Int_t dumpNumber;
    

	dumpNumber = MatchDumpNameToDumpNumber(runNumber, dumpName, rep);
    GetDumpTimes(runNumber, 1, dumpNumber, &startTime, &endTime);

    TCanvas *c1 = new TCanvas();
    c1->Divide(1,2);
    c1->cd(1);
    SetTimeResolution(0.01);
    TH1D* histoFull = GetHistoByTimes(runNumber, channel,  startTime, endTime);
    Double_t fullIntegral = histoFull->Integral();
    //
    histoFull->DrawCopy();
    histoFull->SetXTitle("Time [s]");
	//histo = RebinHistoToNBins(histo, NUMBER_BINS);
	//histo->SetYTitle("Rate [Hz]");
	//histo->Scale(1./histo->GetBinWidth(1));
	//FormatHisto(histoFull);
    histoFull->GetXaxis()->SetLimits(0, endTime-startTime);
	FormatHisto(histoFull);
	sprintf(text, "Integral = %.0lf", fullIntegral);
	AddRandomText(text, rep+1, 0.45, rep-1);
    
    TLine *line1 = new TLine(offset,0,offset, 200);
    line1->SetLineColor(kRed);
    line1->SetLineStyle(2);
    line1->Draw();


    TLine *line2 = new TLine(offset+length,0,offset+length, 200);
    line2->SetLineColor(kRed);
    line2->SetLineStyle(2);
    line2->Draw();


	//SetDrawOpts("HIST");
    
    c1->cd(2);
    SetTimeResolution(timeResolution);
	TH1D* histo = GetHistoByTimes(runNumber, channel,  startTime+offset, startTime+offset+length);
	
	Double_t theIntegral = histo->Integral();
	histo->GetXaxis()->SetLimits(offset, offset+length);
	histo->SetXTitle("Time [s]");
	
	FormatHisto(histo);
	sprintf(text, "Integral = %.0lf", theIntegral);
	AddRandomText(text, rep+1, 0.45, rep-1);
	

    //SetTimeResolution(0.005);
    TCanvas *c2 = new TCanvas();
    c2->Divide(1,3);
    c2->cd(1);
    TH1D* histoDeg = GetHistoByTimes(runNumber, 2, startTime+offset, startTime+offset+length);
    Double_t degIntegral = histoDeg->Integral();
    //
    histoDeg->DrawCopy();
    histoDeg->SetXTitle("Time [s]");
    histoDeg->GetXaxis()->SetLimits(offset, offset+length);
	FormatHisto(histoDeg);
	sprintf(text, "Integral = %.0lf", degIntegral);
	AddRandomText(text, rep+1, 0.45, rep-1);
    line1->Draw();
    line2->Draw();
    
    c2->cd(2);
    TH1D* histoTrap = GetHistoByTimes(runNumber, 3,startTime+offset, startTime+offset+length);
    Double_t trapIntegral = histoTrap->Integral();
    //
    histoTrap->DrawCopy();
    // histoTrap->SetXTitle("Time [s]");
    histoTrap->GetXaxis()->SetLimits(offset, offset+length);
	FormatHisto(histoTrap);
	//sprintf(text, "Integral = %.0lf", dtrapIntegral);
	//AddRandomText(text, rep+1, 0.45, rep-1);
    line1->Draw();
    line2->Draw();

    c2->cd(3);
    TH1D* histoDown = GetHistoByTimes(runNumber, 5,startTime+offset, startTime+offset+length);
    Double_t downIntegral = histoDown->Integral();
    //
    histoDown->DrawCopy();
    //histoDown->SetXTitle("Time [s]");
    histoDown->GetXaxis()->SetLimits(offset, offset+length);
	FormatHisto(histoDown);
	//sprintf(text, "Integral = %.0lf", downIntegral);
	//AddRandomText(text, rep+1, 0.45, rep-1);
    line1->Draw();
    line2->Draw();
	
}
