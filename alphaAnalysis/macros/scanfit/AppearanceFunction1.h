#ifndef MN_AppearanceFunction_H_
#define MN_AppearanceFunction_H_
#define _USE_MATH_DEFINES
#include <math.h>
#include <TMath.h>
#include <TRandom.h>
namespace ROOT { 
  namespace Minuit2 {
    class AppearanceFunction {
    public:
      AppearanceFunction(double peak,double sigmar, double gammar, double gammab,double lb) :
        the_peak(peak), the_sigmar(sigmar), the_gammar(gammar),the_gammab(gammab),the_lb(lb) {}
      ~AppearanceFunction() {}
      double c() const {return the_peak+the_offset;}
      double sr() const {return the_sigmar; }
      double gr() const {return the_gammar;}
      double gb() const {return the_gammab;}
      double lb() const {return the_lb;}
      TRandom gRand= TRandom(571810);  //sim
      // TRandom gRand= TRandom(57181);
      double operator()(double x) const {
        return ( x<c())?TMath::Voigt((x-c()),sr(),gr())/TMath::Voigt(0.,sr(),gr()):((x<lb()+c())?(0.25*gb()*gb()/((x-c())*(x-c()) + gb()*gb()*0.25)):0.25*gb()*gb()*exp(-2.0*lb()*(x-c()-lb())/(0.25*gb()*gb()+lb()*lb()))/(0.25*gb()*gb()+lb()*lb()));
      }
    private:
      double the_peak;
      double the_sigmar;
      double the_gammar;
      double the_gammab;
      double the_lb;
      double the_offset=gRand.Uniform(-10.,10.);
      /* double the_offset=0.; */
    };
  }
}
#endif // MN_AppearanceFunction_H_
