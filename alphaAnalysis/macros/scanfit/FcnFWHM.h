#ifndef MN_FcnFWHM_H_
#define MN_FcnFWHM_H_
#include "Minuit2/FCNBase.h"
#include <vector>
namespace ROOT {
  namespace Minuit2 {
    class FcnFWHM : public  FCNBase {
    public:
      FcnFWHM(const std::vector<double>& freq,
              const std::vector<double>& meas,
              const std::vector<double>& mvar):
        theFreq(freq),theMeas(meas),theMvar(mvar),
        theErrorDef(1.) {}  
      ~FcnFWHM() {}
      /* double sl_sr() const {return sl_sigmar;} */
      /* double sl_gr() const  {return sl_gammar;} */
      /* double sl_gb() const  {return sl_gammab;} */
      /* double sl_NP() const  {return sl_NPower;} */
      /* double sl_off() const {return sl_offset;} */
      void SetNFreq(int nf)  {NFreq=nf;}
      Int_t GetNSim()  const {return NSim;}
      virtual double Up() const {return theErrorDef;}
      virtual double operator()(const std::vector<double>&) const;

      void setErrorDef(double def) {theErrorDef = def;}
     private:
      const std::vector<double> theFreq;
      const std::vector<double> theMeas;
      const std::vector<double> theMvar;
      double theErrorDef;
      int  NFreq=9;
      int const  NSim=7;
    };
  }
}

#endif //MN_FcnFWHM_H_
