#ifndef MN_FcnFreq_H_
#define MN_FcnFreq_H_
#include "Minuit2/FCNBase.h"
#include <vector>
namespace ROOT {
  namespace Minuit2 {
    class FcnFreq : public  FCNBase {
    public:
      FcnFreq(const std::vector<double>& freq,
              const std::vector<double>& meas,
              const std::vector<double>& mvar):
        theFreq(freq),theMeas(meas),theMvar(mvar),
        theErrorDef(1.) {}

      ~FcnFreq() {}
      /* double sl_sr() const {return sl_sigmar;} */
      /* double sl_gr() const  {return sl_gammar;} */
      /* double sl_gb() const  {return sl_gammab;} */
      /* double sl_NP() const  {return sl_NPower;} */
      /* double sl_off() const {return sl_offset;} */

       void SetNFreq(int nf)  {NFreq=nf;}
      virtual double Up() const {return theErrorDef;}
      virtual double operator()(const std::vector<double>&) const;
      std::vector<double> Freq() const {return theFreq;}
      std::vector<double> Meas() const {return theMeas;}
      std::vector<double> Mvar() const {return theMvar;}
      void setErrorDef(double def) {theErrorDef = def;}
    private:
      std::vector<double> theFreq;
      std::vector<double> theMeas;
      std::vector<double> theMvar;
      double theErrorDef;
      int NFreq=9;
    };
  }
}

#endif //MN_FcnFreq_H_
