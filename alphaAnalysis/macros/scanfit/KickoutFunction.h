#ifndef MN_KickoutFunction_H_
#define MN_KickoutFunction_H_
#define _USE_MATH_DEFINES
#include <math.h>
#include <TMath.h>
#include <TRandom.h>
namespace ROOT { 
  namespace Minuit2 {
    class KickoutFunction {
    public:
      KickoutFunction(double lam1, double a1, double lam2, double a2) :
        the_lam1(lam1), the_a1(a1),the_lam2(lam2), the_a2(a2) {
TRandom gRand= TRandom(571950);//sim
the_offset=gRand.Uniform(-10.,10.);
}
      ~KickoutFunction() {}
      double lam_1() const {return the_lam1;}
      double a_1() const {return the_a1; }
      double lam_2() const {return the_lam2;}
      double a_2() const {return the_a2;}
      //      TRandom gRand= TRandom(57208);
     double operator()(double x) const {
       return ( a_1()*exp(-lam_1()*(x))+a_2()*exp(-lam_2()*(x)));
      }
    private:
      double the_lam1;
      double the_a1;
      double the_lam2;
      double the_a2;
      double the_offset;
      /* double the_offset=0.; */
    };
  }
}
#endif // MN_KickoutFunction_H_
