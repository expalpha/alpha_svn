#ifndef MN_KickoutFunctionDD_H_
#define MN_KickoutFunctionDD_H_
#define _USE_MATH_DEFINES
#include <math.h>
#include <TMath.h>
#include <TRandom.h>
namespace ROOT { 
  namespace Minuit2 {
    class KickoutFunctiondd {

    public:
      KickoutFunctiondd(double N0, double lamd, double leak, double lamg, double bkg)        
      {
        the_N0=N0;
        the_lamd=lamd;
        the_leak=leak;
        the_lamg=lamg;
        the_bkg=bkg;
        TheFcn fFcn;
        gamma=fFcn.GetGamma();
        NRep=fFcn.GetNRep();
        N_d[0]=N0;
        N_c[0]=N0;
        ndd[0]=0.;
        ndark[0]=0.;
        darkfrac[0]=fFcn.GetDarkfrac(0);
        darkfrac[1]=fFcn.GetDarkfrac(1);
        for (int rep=1;rep<NRep;rep++) {
          // here are the recurrence defs of Nd(rep), Nc(rep), ndd(rep) 
        cas=((rep+1)%2);
        N_d[rep] =N0*exp(-(lamd+lamg*(1+darkfrac[cas]))*pow(rep,1-gamma));
        N_c[rep]=N0*exp(-(lamg*(1+darkfrac[cas]))*rep);

        ndd[rep]=N_d[rep-1]*(1-exp(-(lamd+lamg)))*(1-gamma)*((rep>0)?pow(double(rep),-gamma):1.)
        +N_c[rep-1]*(1-exp(-(lamg)))+bkg;
        ncc[rep]=0;
        ndark[rep]=(N_d[rep-1]+N_c[rep-1])*(1.-exp(-lamg*darkfrac[cas]));
        ndark[rep]+=ndd[rep]*leak;
        ndd[rep]*=(1-leak);
        gdd[rep-1]=ndd[rep]-ndd[rep-1];
        gdark[rep-1]=ndark[rep]-ndark[rep-1];
        }
      } 
  
      ~KickoutFunctiondd(){}

      double N0() const {return the_N0;}
      double lamd() const {return the_lamd;}
      double leak() const {return the_leak; }
      double lamg() const {return the_lamg;}
      double bkg() const {return the_bkg;}
      double fndd(int rep) const {return ndd[rep];}
      double fndd(double x) const {return ndd[int(x)]+gdd[int(x)]*(x-int(x));}
      double fndd_(double* rep, double*) /*const*/ {
        return ndd[int(rep[0])];
      }
      double fndark(int rep) const {return ndark[rep];}
      double fndark(double x) const {return ndark[int(x)]+gdark[int(x)]*(x-int(x));}
      double fndark_(double* rep, double* ) {return ndark[int(rep[0])];}
      double fN_d(int rep) const {return N_d[rep];}
      double fN_c(int rep) const {return N_c[rep];}
    //      double operator()(double x) const {return {n_dd[int(x))]};     
    private:
      double the_N0;
      double the_lamd;
      double the_leak;
      double the_lamg;
      double the_bkg;
      int cas=0;
      double gamma=0.2;
      double darkfrac[2]{1.4,2.51111};
      double N_d[100];
      double N_c[100];
      double ndd[100];
      double ncc[100];
      double ndark[100];
      double gdd[100];
      double gcc[100];
      double gdark[100];
      int NRep=50;
    };
    }
}
#endif // MN_KickoutFunctionDD_H_
