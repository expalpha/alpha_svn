#ifndef MN_KickoutFunctionDD_H_
#define MN_KickoutFunctionDD_H_
#define _USE_MATH_DEFINES
#include <math.h>
#include <TMath.h>
#include <TRandom.h>
namespace ROOT { 
  namespace Minuit2 {
    class KickoutFunctiondd {

    public:
      KickoutFunctiondd(double N0, double lamd, double lamc, double lamg, double bkg, double _gamma)        
      {
        the_N0=N0;
        the_lamd=lamd;
        the_lamc=lamc;
        the_lamg=lamg;
        the_bkg=bkg;
        TheFcn fFcn;
        gamma=_gamma;
        NRep=fFcn.GetNRep();
        NRep=(NRep>99)?99:NRep;
        N_d[0]=N0;
        N_c[0]=N0;
        ndd[0]=0.;
        ndark[0]=0.;
        darkfrac[0]=fFcn.GetDarkfrac(0);
        darkfrac[1]=fFcn.GetDarkfrac(1);
        double tmpd,tmpc;
        for (int rep=1;rep<=NRep;rep++) {
          // here are the recurrence defs of Nd(rep), Nc(rep), ndd(rep) 
        cas=((rep-1)%2);
        tmpd=(rep>1)?N_d[rep-1]*exp(-(lamd*(pow(rep,gamma)-pow(rep-1, gamma))+lamg)):N0*exp(-lamd*gamma-lamg);// D states at end of laser pulse
        tmpc=N_c[rep-1]*exp(-lamg);
        ndd[rep]=N_d[rep-1]-tmpd+N_c[rep-1]-tmpc;
        N_d[rep] =tmpd*exp(-lamg*darkfrac[cas]);//D states at end of repetition
        N_c[rep]=tmpc*exp(-lamg*darkfrac[cas]);
        ndark[rep] =N_d[rep-1] -N_d[rep]+N_c[rep-1]-N_c[rep]-ndd[rep]  ;
        ncc[rep]=0;
        ndark[rep]+=ndd[rep]*lamc+bkg*darkfrac[cas]; //leak correction
        ndd[rep]*=(1-lamc)+bkg;
        }
      } 
  
      ~KickoutFunctiondd(){}

      double N0() const {return the_N0;}
      double lamd() const {return the_lamd;}
      double lamc() const {return the_lamc; }
      double lamg() const {return the_lamg;}
      double bkg() const {return the_bkg;}
      double fndd(int rep) const {return ndd[rep];}
      double fndd(double x) const {return ndd[int(x)];}
      double fndd_(double* rep, double*) /*const*/ {
        return ndd[int(rep[0]+0.5)];
      }
      double fndark(int rep) const {return ndark[rep];}
      double fndark(double x) const {return ndark[int(x)];}
      double fndark_(double* rep, double* ) {return ndark[int(rep[0]+0.5)];}
      double fN_d(int rep) const {return N_d[rep];}
      double fN_c(int rep) const {return N_c[rep];}
    //      double operator()(double x) const {return {n_dd[int(x))]};     
    private:
      double the_N0;
      double the_lamd;
      double the_lamc;
      double the_lamg;
      double the_bkg;
      int cas=0;
      double gamma;
      double darkfrac[2]{1.4,2.51111};
      double N_d[100];
      double N_c[100];
      double ndd[100];
      double ncc[100];
      double ndark[100];
      int NRep=99;
    };
    }
}
#endif // MN_KickoutFunctionDD_H_
