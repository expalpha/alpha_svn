#ifndef MN_KickoutFunctionP_H_
#define MN_KickoutFunctionP_H_
#define _USE_MATH_DEFINES
#include <math.h>
#include <TMath.h>
#include <TRandom.h>
namespace ROOT { 
  namespace Minuit2 {
    class KickoutFunctionp {

    public:
      KickoutFunctionp(double N0, double lamd, double lamc, double lamg, double bkg, double leak, double gammad,double gammac, double rncd)        
      {
        the_N0=N0;
        the_lamd=lamd;
        the_lamc=lamc;
        the_lamg=lamg;
        the_bkg=bkg;
        the_leak=leak;
        the_gammad=gammad;
        the_gammac=gammac;
        the_rncd=rncd;
        TheFcn fFcn;
        NRep=fFcn.GetNRep();
        NRep=(NRep>99)?99:NRep;
        N_d[0]=N0;
        N_c[0]=N0*rncd;
        ndd[0]=0.;
        ncc[0]=0.;
        ndark[0]=0.;
        darkfrac[0]=fFcn.GetDarkfrac(0);
        darkfrac[1]=fFcn.GetDarkfrac(1);
        double ldmpd,ldmpc; //Events remaining at end of laser pulse.
        int rep;
        for (int irep=1;irep<=2*NRep;irep++) {
          // here are the recurrence defs of N_d(irep), N_c(irep), ndd(irep/2+1) ncc(irep/2) 
          cas=((irep-1)%2);
          if((irep-1)%4<2){
            rep= irep/2+1;
            ldmpd=(irep>1)?N_d[irep-1]*exp(-(lamd*(pow(rep,gammad)-pow(rep-1, gammad))+lamg)):N_d[0]*exp(-lamd*gammad-lamg);
            ldmpc=N_c[irep-1]*exp(-lamg);
            ndd[rep]=N_d[irep-1]-ldmpd+N_c[irep-1]-ldmpc;
            N_d[irep] =ldmpd*exp(-lamg*darkfrac[cas]);
            N_c[irep]=ldmpc*exp(-lamg*darkfrac[cas]);
            ndark[irep] =N_d[irep-1] -N_d[irep]+N_c[irep-1]-N_c[irep]-ndd[rep] +bkg*darkfrac[cas] ;
            ndark[irep]+=ndd[rep]*leak; //leak correction
            ndd[rep]*=(1-leak)+bkg;
            
          }
          else {
            rep=irep/2;
            ldmpc=(irep>3)?N_c[irep-1]*exp(-(lamc*(pow(rep,gammac)-pow(rep-1, gammac))+lamg)):N_c[irep-1]*exp(-lamc*gammac-lamg);
            ldmpd=N_d[irep-1]*exp(-lamg);
            ncc[rep]=N_c[irep-1]-ldmpc+N_d[irep-1]-ldmpd;
            N_d[irep] =ldmpd*exp(-lamg*darkfrac[cas]);
            N_c[irep]=ldmpc*exp(-lamg*darkfrac[cas]);
            ndark[irep] =N_d[irep-1] -N_d[irep]+N_c[irep-1]-N_c[irep]-ncc[rep]  +bkg*darkfrac[cas];                 
            ndark[irep]+=ncc[rep]*leak; //leak correction
            ncc[rep]*=(1-leak)+bkg;
          }
        } 
      }
        ~KickoutFunctionp(){}

        double N0() const {return the_N0;}
        double lamd() const {return the_lamd;}
        double lamc() const {return the_lamc; }
        double lamg() const {return the_lamg;}
        double bkg() const {return the_bkg;}
        double leak() const {return the_leak;}
        double fndd(int rep) const {return ndd[rep];}
        double fndd(double x) const {return ndd[int(x)];}
        double fndd_(double* rep, double*) /*const*/ {
          return ndd[int(rep[0]+0.5)];
        }
        double fncc(int rep) const {return ncc[rep];}
        double fncc(double x) const {return ncc[int(x)];}
        double fncc_(double* rep, double*) /*const*/ {
          return ncc[int(rep[0]+0.5)];
        }
        double fndark(int rep) const {return ndark[rep];}
        double fndark(double x) const {return ndark[int(x)];}
        double fndark_(double* rep, double* ) {return ndark[int(rep[0]+0.5)];}
        double fN_d(int rep) const {return N_d[rep];}
        double fN_c(int rep) const {return N_c[rep];}
        //      double operator()(double x) const {return {n_dd[int(x))]};     
      private:
        double the_N0;
        double the_lamd;
        double the_lamc;
        double the_lamg;
        double the_bkg;
        double the_leak;
        int cas=0;
        double the_gammad;
        double the_gammac;
        double the_rncd;
        double darkfrac[2]{1.4,2.51111};
        double N_d[200];
        double N_c[200];
        double ndd[100];
        double ncc[100];
        double ndark[200];
        int NRep=99;
      };
    }
  }
#endif // MN_KickoutFunctionP_H_
