// @(#)root/minuit2:$Id$
// Template Authors: M. Winkler, F. James, L. Moneta, A. Zsenei   2003-2005
// Art Olin 2017
/**********************************************************************
 *                                                                    *
 * Copyright (c) 2005 LCG ROOT Math team,  CERN/PH-SFT                *
 *                                                                    *
 **********************************************************************/
//For 57195 and 57181 d-d and c-c irradiation
#define SIM 0
#include "TheFcnpL.h"
#include "KickoutFunctionp195.h"
#include <cassert>
#include <iostream>
namespace ROOT {
  namespace Minuit2 {

     double TheFcn::operator()(const std::vector<double>& par) const {
      assert(par.size() == 8);
      double loglikd=0.;
      double loglikc=0.;
      double loglikdark=0.;
      KickoutFunctionp kickoutd(par[0],par[1],par[2],par[3],par[4],par[5],par[6],par[7]);
      // std::cout<<"lamd\t"<<kickoutd.lamd()<<" lamc\t"<<kickoutd.lamc()<<" N0\t"<<kickoutd.N0()<<" N_d0\t"<<kickoutd.fN_d(0)<<std::endl;

      Double_t cu,fu,cuc, fuc, cudark, fudark(0);
      for ( int irep=1;irep<NRep+1;irep++) {
        cu=theMeasd[irep-1];
	     fu=kickoutd.fndd(irep);

        if (fu < 1.e-9) fu = 1.e-9;
        loglikd-=TMath::Log(TMath::PoissonI(cu,fu));
        cuc=theMeasc[irep-1];
	     fuc=kickoutd.fncc(irep);

        if (fuc < 1.e-9) fuc = 1.e-9;
        loglikc-=TMath::Log(TMath::PoissonI(cu,fu));
#if SIM==0
     // for ( int irep=0;irep<NRep-1;irep+=2) {
        cudark=theMeasdark[irep-1];
	//        fu=fudark*par[2]+kickoutdark(Double_t(irep))+par[4]*darkfrac;
        fudark=kickoutd.fndark(irep);

        if (fu < 1.e-9) fu = 1.e-9;
        loglikdark-=TMath::Log(TMath::PoissonI(cudark,fudark));
#endif
        // std::cout<<irep<<" cu\t"<<cu<<" fu\t"<<fu<<" chi2\t"<<chi2<<" N_D\t"<<kickoutd.fN_d(irep)<<" Kickoutd\t"
        //          <<kickoutd.fndd(irep)<<" Kickoutdark\t"<<kickoutd.fndark(irep)
        //          <<std::endl;
      }
//      std::cout<<"N0\t"<<kickoutd.N0()<<" lambdad "<<kickoutd.lamd()<<std::endl;
//      std::cout<<"darkfrac\t"<<GetDarkfrac(0)<<" : "<<GetDarkfrac(1)<<std::endl;

      return loglikd+ loglikc+loglikdark  ;
    }

  }  // namespace Minuit2

}  // namespace ROOT
