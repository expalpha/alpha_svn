#ifndef MN_TheFcn_H_
#define MN_TheFcn_H_
#include "Minuit2/FCNBase.h"
#include <vector>
namespace ROOT {
  namespace Minuit2 {
    class TheFcn : public  FCNBase {
    public:
      TheFcn(const std::vector<double>& measd,
             const std::vector<double>& measc,
             const std::vector<double>& measdark,
             const std::vector<double>& mvard,
             const std::vector<double>& mvarc,
             const std::vector<double>& mvardark):
        theMeasd(measd),theMeasc(measc),theMeasdark(measdark),theMvard(mvard),
        theMvarc(mvarc), theMvardark(mvardark), theErrorDef(0.5) {}
      TheFcn(){}

      ~TheFcn() {}
      void SetNSets(int ns)  {NSets=ns;}
      void SetNRep(int nrep)  {NRep=nrep;}
      int GetNRep() const {return NRep;} 
      void SetDarkfrac(double *dfrac) {darkfrac[0]=dfrac[0];darkfrac[1]=dfrac[1];}
      double GetDarkfrac( int i) const {return darkfrac[i];}
      // double GetGamma() {return gamma;}
      virtual double Up() const {return theErrorDef;}
      virtual double operator()(const std::vector<double>&) const;
      std::vector<double> Measd() const {return theMeasd;}
      std::vector<double> Measc() const {return theMeasc;}
      std::vector<double> Mvard() const {return theMvard;}
      std::vector<double> Mvarc() const {return theMvarc;}
      std::vector<double> Measdark() const {return theMeasdark;}
      std::vector<double> Mvardark() const {return theMvardark;}
      void setErrorDef(double def) {theErrorDef = def;}
    private:
      std::vector<double> theMeasd;
      std::vector<double> theMeasc;
      std::vector<double> theMvard;
      std::vector<double> theMvarc;
      std::vector<double> theMeasdark;
      std::vector<double> theMvardark;
      double theErrorDef;

      int NSets=2;
      int NRep=98.;   //for the fit
      double darkfrac[2]={0.2,13.6/18}; //57181, 195
      // double darkfrac[2]={1.4,2.511};// 57208
      // double gamma=0.86;
      // double gamma=0.35;
    };
  }
}
#endif //MN_TheFcn_H_
