//AO: This code is a script - you need to modify some parameters for the case you choose to fit.
//Version scanfitfreq - fits scanned frequencies
//Input is INFILE
//
#define SIM 1
#include "FcnFreq.h"
#include "AppearanceFunction2.h"
#include "Minuit2/FunctionMinimum.h"
#include "Minuit2/MnUserParameterState.h"
#include "Minuit2/MnPrint.h"
#include "Minuit2/MnMigrad.h"
#include "Minuit2/MnMinos.h"
#include "Minuit2/MnContours.h"
#include "Minuit2/MnPlot.h"
#include "Minuit2/MinosError.h"
#include "Minuit2/ContoursError.h"

#include <iostream>
#include "Riostream.h"
#include <TString.h>
#include <TROOT.h>
#include <TFile.h>
#include <TGraph.h>
#include <TMultiGraph.h>
#include <TGraphErrors.h>
#include <TSystem.h>
#include <TF1.h>
#include <TCanvas.h>
using namespace ROOT::Minuit2;
double appearanceplot(double* x, double* par)
{
  AppearanceFunction Fappearance(par[0], par[1], par[2], par[3],par[4] );
   // std::cout<<"Fappearance: "<<" rMVA\t"<<rMVA<<" par[5]\t"<<par[4]<<"\n";
   return par[5]*Fappearance(x[0])+par[6];
 }
  


int scanfitfreq() {
  TString dir = gSystem->UnixPathName(__FILE__);
  //  std::cout<<dir.Data()<<" "<<dir <<"\n";
  //  dir.ReplaceAll("linefit1p.C","");
  //  dir.ReplaceAll("/./","/");
  const char* _infile= getenv("INFILE");
  std::cout<<Form("%s/%s",dir.Data(),_infile)<<"\n";
  auto _f= new TFile(_infile);
  TCanvas* c1=(TCanvas*)_f->Get("c1");
  TGraphErrors* gd =(TGraphErrors*)c1->FindObject("gfreqdd");
  TGraphErrors* gc =(TGraphErrors*)c1->FindObject("gfreqcc"); 

  Int_t nfreq =gd->GetN();
  Int_t nlines=0;
  Double_t  freq_,data_;
  Double_t error_;
  std::vector<double> freq;
  std::vector<double> measc;
  std::vector<double> measd;
  std::vector<double> mvarc;
  std::vector<double> mvard;
  std::vector<double> merrorc;
  std::vector<double> merrord;
  std::vector<double> merrorx;
  for (int ifreq=0;ifreq<nfreq;ifreq++){
    gd->GetPoint(ifreq,  freq_ ,data_);
    if(freq_<1.e-7&&data_<1.e-7) continue;
    nlines++;
    freq.push_back(freq_);
    measd.push_back(data_);
    error_=gd->GetErrorY(ifreq)>0?gd->GetErrorY(ifreq):0.5;
    merrord.push_back(error_);
    mvard.push_back(error_*error_);
    // std::cout<<ifreq<<"\t"<<freq_<<"\t"<<data_<<"\t"<<error_;
    gc->GetPoint(ifreq, freq_ , data_);
    measc.push_back(data_);
    error_=gc->GetErrorY(ifreq)>0?gc->GetErrorY(ifreq):0.5;
    // std::cout<<"\t"<<freq_<<"\t"<<data_<<"\t"<<error_<<"\n";
    merrorc.push_back(error_);
    mvarc.push_back(error_*error_);
    merrorx.push_back(0.1);
  }
  // c1->cd(1);
  // gd->Draw("AP");
  // c1->cd(2);
  // gc->Draw("AP");
  FcnFreq fdFCN(freq, measd, mvard ); 
  fdFCN.SetNFreq(nlines);
  std::cout<<"nfreq "<<nfreq<<" nlines "<<nlines<<"\n";
  ///////////////////////////////////////////////////////////////////////////////////////////////////
  //Here the fit parameters and fitting strategy is done.  
  // demonstrate standard minimization using MIGRAD
  // create Minuit parameters with names
  MnUserParameters upar;
  //Oct 13 params cont function fit
//  upar.Add("peak",-3.233226463749, 0.9679);
//    //-1 sigma    upar.Add("sigmar",30.47,0.1);
//  upar.Add("sigmar", 12.44230497575, 1.9291);
//  upar.Add("gammar",35.5879014963, 3.7632);
//    //+1 sigma    upar.Add("gammab",85.28,0.1);
//  upar.Add("gammab", 74.45922579218, 3.981889914471);
//  upar.Add("lb",74.79662361014, 11.0147);
//  upar.Add("NAtom",  160., 0.005820);
//  //    upar.Add("bgd", 0.0, 0.1);
//  upar.Add("bgd", 21.4, 0.1);
//Nov 7 1700mW sim
//  upar.Add("peak",-7.693  , 0.54781);
//  upar.Add("sigmar", 0., 2.6761972481);
  //  upar.Add("gammar",41.08, 1.854541);
  //  upar.Add("gammab",  93.496, 3.71105827761);
//  upar.Add("gammar",15.,1.);
//  upar.Add("gammab",40,1.); //57195
//  upar.Add("sigmab",0., 11.0147);
// upar.Add("NAtom",  160., 0.005820);
// upar.Add("bgd", 21.4, 0.1);
    // upar.Add("NAtom", 0.25, 0.1);
    // upar.Add("bgd", 0., 0.1);
//Nov 7 1500mW sim bgd free vals
//  upar.Add("peak",-8.020453, 1.54781);

  upar.Add("peak",  5.2420, 0.1);
  upar.Add("sigmar", 0.09438145563759 , 2.6761972481);
  upar.Add("gammar",39.15576085102, 1.854541);
  upar.Add("gammab", 91.0018612437 , 3.71105827761);
  upar.Add("sigmab",5., 1.);
#if SIM==0
  upar.Add("NAtom",  150., 0.005820);
    upar.Add("bgd", 11., 0.1);
#else
    upar.Add("NAtom", 1500., 0.1);
    upar.Add("bgd", 0., 0.1);
#endif
    // background from dark period 57181 68. *2100/370 /(2*9) = 21.4 +-2.6 cts
    std::cout<<"StartPars: "<<upar.Value("NAtom")<<"\t"<<upar.Value("peak")<<"\t"<<upar.Value("sigmar")<<"\t"<<upar.Value("gammar")<<"\t"<<upar.Value("gammab")<<"\t"<<upar.Value("lb")<<"\t"<<upar.Value("bgd")<<"\n";
  // create MIGRAD minimizer
  MnMigrad migradd(fdFCN, upar);
#if SIM==0
  migradd.Fix("gammar");
 migradd.Fix("sigmar");
  migradd.Fix("gammab");
#endif
    // migradd.Fix("sigmar");
    // migradd.Fix("bgd");
//     migradd.Fix("sigmab");
  //End of parameter and strategy specifications
  std::cout<<"End of parameter and strategy specifications\n";
  //==================================================================================
  // minimize
  FunctionMinimum mind = migradd();
  upar =migradd.Parameters();

  //migrad.Release("gammab");
  //    migrad.Release("gammar");
  // migrad.Release("peak");

  //  min=migrad();
  // upar =migradd.Parameters();
  // Minos errors show no improvement
   if (0) {
    MnMinos minos(fdFCN, mind);
    std::pair<double,double> e;
    for (unsigned int i = 0; i < upar.Params().size(); ++i) {
      if(upar.Error(i) >0.) {
        e = minos(i);
        std::cout << "MINOS error  for " << i <<":" << upar.Name(i)<< " = " << upar.Error(i)<<"\t"<<e.first << "\t" << e.second << std::endl;
      }
    }
  }
  // output
  std::cout<<"minimum: "<<mind<<std::endl;
  std::cout<<"minpard: "<<upar.Value("NAtom")<<"\t"<<upar.Value("peak")<<"\t"<<upar.Value("sigmar")<<"\t"<<upar.Value("gammar")<<"\t"<<upar.Value("gammab")<<upar.Value("sigmab")<<"\t"<<upar.Value("bgd")<<"\n";
  TF1 *fdappearance = new TF1("plotappearance",appearanceplot,freq[0],freq[nlines-1],nlines);
  fdappearance->SetParameters(upar.Value(0),upar.Value(1),upar.Value(2),upar.Value(3),upar.Value(4),upar.Value(5),upar.Value(6));
  fdappearance->SetLineColor(kBlack);
  Double_t chi2d= gd->Chisquare(fdappearance);
  std::cout<<"Chisquared d->d\t"<<chi2d<<"\n"; 
  TCanvas* c2 =new TCanvas("c2","Appearance fits", 600.,1200.);
  c2->Divide(1,2);
  c2->cd(1);
  gd->Draw("AP");
  fdappearance->Draw("SAME");
  FcnFreq fcFCN(freq, measc, mvarc ); 
  fcFCN.SetNFreq(nlines);
  std::cout<<"nfreq "<<nfreq<<" nlines "<<nlines<<"\n";
  //Dec 20  1300mW 27181
  upar.SetValue("peak",2. );
  //  upar.SetValue("sigmar", 0.5036854142169);
  upar.SetValue("sigmar", 5.0);
  upar.SetValue("gammar", 38.69009836033 );
  upar.SetValue("gammab",91.4);
  upar.SetValue("sigmab",4.);
   //Nov 7 1500mW 27181
 // upar.SetValue("bgd",0.);
//  upar.SetValue("bgd",21.4);
// 1700mw values
//  upar.SetValue("peak", -9.368);
//  upar.SetValue("sigmar",  0.  );
//   upar.SetValue("gammar", 38.336 );
//   upar.SetValue("gammab",  99.1395);
//   upar.SetValue("sigmab", 0.0);
// upar.SetValue(" NAtom", 140.);
    // upar.SetValue("NAtom",.32);
  // upar.SetValue("bgd",0.);
// upar.SetValue("bgd",21.4);
#if SIM==0
  upar.SetValue("bgd", 13.);
  upar.SetValue("NAtom",55.);
#else
   upar.SetValue("bgd", 0.);
   upar.SetValue("NAtom",1300.);
#endif
  MnMigrad migradc(fcFCN, upar); 

#if SIM==0
   migradc.Fix("sigmar");
  migradc.Fix("gammar");
   migradc.Fix("sigmab");
   migradc.Fix("gammab");
    // migradc.Fix("bgd");
   // migradc.Release("bgd");
#else
   //  migradc.Fix("sigmab");
   // migradc.Fix("gammar");
   // migradc.Fix("bgd"); 
#endif
  FunctionMinimum minc = migradc();
  upar =migradc.Parameters();
  std::cout<<"minimum: "<<minc<<std::endl;
  std::cout<<"minparc: "<<upar.Value("NAtom")<<"\t"<<upar.Value("peak")<<"\t"<<upar.Value("sigmar")<<"\t"<<upar.Value("gammar")<<"\t"<<upar.Value("gammab")<<"\t"<<upar.Value("sigmab")<<"\t"<<upar.Value("bgd")<<"\n";
  TF1 *fcappearance = new TF1("plotappearance",appearanceplot,freq[0],freq[nlines-1],nlines);
  fcappearance->SetParameters(upar.Value(0),upar.Value(1),upar.Value(2),upar.Value(3),upar.Value(4),upar.Value(5),upar.Value(6));
  fcappearance->SetLineColor(kRed);
  Double_t chi2c= gc->Chisquare(fcappearance);
  std::cout<<"Chisquared c->c\t"<<chi2c<<"\n"; 

  c2->cd(2);
  gc->Draw("AP");
  fcappearance->Draw("SAME");

  return 0;
};
