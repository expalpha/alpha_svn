#!/usr/bin/perl -w

#Eoin, Jul 2010
# Uses http://stromberg.dnsalias.org/~strombrg/looper/ to execute alphaAnalysis jobs in parallel

my $threads = 6; # the number of parallel instances
#my $executable = "/home/alpha/alphaAnalysis/scripts/analyse-run-no-reco.perl";
my $executable = "\$RELEASE/alphaAnalysis/scripts/analyse-run.perl";

use Cwd 'abs_path';
#use File::chdir;

# Find target run list
$runListFilename = $ARGV[0];

# Open run list
open(my $runListFile,  "<", $runListFilename ) or die "Can't open runListFilename : $!";

my @runList = <$runListFile>;
chomp( @runList );

# now run looper
my $cmd = "~/eoin/bin/looper --max-concurrency $threads --parameterized-command \'$executable %s\' \'@runList\'";
print "$cmd \n";
system $cmd;

exit 0;
