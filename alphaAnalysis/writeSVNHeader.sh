#!/bin/bash

echo "//TAlphaAnalysisVersion.h: I am a file that should not be committed to the SVN!"> alphaAnalysisSVNLog.h
echo "//                         I am created by the makefile">> alphaAnalysisSVNLog.h
echo "#ifndef _TAlphaAnalysisVersion_">> alphaAnalysisSVNLog.h
echo "#define _TAlphaAnalysisVersion_">> alphaAnalysisSVNLog.h
echo "#include \"TString.h\"">> alphaAnalysisSVNLog.h

#Record the SVN only number:
echo -n "TString _AlphaAnalysisSVNRevision_=\"" >> alphaAnalysisSVNLog.h
svnversion -n ${RELEASE} >> alphaAnalysisSVNLog.h
echo "\";"  >> alphaAnalysisSVNLog.h

#Record the full SVN info
echo -n "TString _AlphaAnalysisSVNFull_=\"">> alphaAnalysisSVNLog.h
echo -n `svn info ${RELEASE} | sed -E ':a;N;$!ba;s/\r{0,1}\n/\\n/g' ` >> alphaAnalysisSVNLog.h
echo  "\";">>  alphaAnalysisSVNLog.h
if [ "$RPLATFORM" == "macosx64" ]; then
  echo 'unsigned int AlphaAnalysisCompileTime=' `stat alphaAnalysisSVNLog.h | awk -F ' ' '{print $2}'` ';'>>  alphaAnalysisSVNLog.h
else
  echo 'unsigned int AlphaAnalysisCompileTime='`stat alphaAnalysisSVNLog.h -c %Y`';'>> alphaAnalysisSVNLog.h
fi


echo "#endif" >> alphaAnalysisSVNLog.h
