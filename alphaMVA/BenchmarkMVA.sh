#!/bin/bash


echo "./BenchmarkMVA.sh Lists/All July_1_100 300_8_16 0.15534"
ListFile=$1
MVANAME=$2
ARCH=$3
RFCUT=$4
TYPE=$5


for i in `cat ${ListFile}`; do 

cd ${RELEASE}/alphaAnalysis/macros
#  echo ".L PlotDumps.C+
#gSPR=\"/alphaMVA/MVAStudies/${MVANAME}/randforest_${ARCH}.spr\" 
#grfcut=${RFCUT}
#PrintMVAPerformance($i,0.,-1.,\"${MVANAME}-${ARCH}\")
#.q
#" | root -l -b; done

echo ".L PlotDumps.C+
gSPR=\"/alphaMVA/MVAStudies/${MVANAME}/randforest_${ARCH}.spr\" 
grfcut=${RFCUT};
Double_t tmin = MatchEventToTime($i,\"startDump\",\"Mixing\",1,0);
Double_t tmax = MatchEventToTime($i,\"stopDump\",\"Mixing\",1,0);
PrintMVAPerformance($i,tmin,tmax,\"${MVANAME}-${ARCH}\");
.q
" | root -l -b; done



cat MVATitle.csv R*${MVANAME}-${ARCH}.csv > MVA${TYPE}Benchmark_${MVANAME}-${ARCH}.csv
