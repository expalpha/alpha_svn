#!/bin/bash
if [ -z $RELEASE ]; then
source ../../myconfig.sh
fi
#SearchString="Aug"
if [ -z $1 ]; then
echo "No argument given... Usage:
./ScanStudies SearchString
where Search string will is the name common to all studies run in MVAStudies folder
eg:
./ScanStudies Aug
"
exit
fi
SearchString="$1"

#ARR="10mHz"
rm $RELEASE/alphaMVA/MVAStudies/${SearchString}_CSVListOfStudies.csv
#touch $RELEASE/alphaMVA/MVAStudies/${SearchString}_CSVListOfStudies.csv
echo "StudyName,Rate Limit,MVASet,RF selection,Background Rate,Error,Fraction Rejected,Accepted Signal,Error" >> $RELEASE/alphaMVA/MVAStudies/${SearchString}_CSVListOfStudies.csv



for dir in `find *${SeachString}*/ -type d  -maxdepth 1`
#FINDLOOP=
#for dir in `find $RELEASE/alphaMVA/MVAStudies/* ${SeachString} -type d -maxdepth 1 | grep ${SeachString} `
do
  dir=${dir%*/}
  #echo ${dir##*/}
  cd $RELEASE/alphaMVA/MVAStudies/$dir/rfoutput    
  for dat in `ls *.dat`; do
    #echo $dat
    #cat $dat
    #echo -e "\n\n\n"
    RATE_LIMIT=$(grep "Rate Limit: " ../StudyDescription.txt)
    #echo $RATE_LIMIT
    RATE_LIMIT=${RATE_LIMIT#*Rate Limit: }
    #echo $RATE_LIMIT
    NF=${dat#*rfout_}
    #echo $NF
    CUT=$(grep "RF selection: " $dat)
    #echo $CUT
    CUT_V=${CUT#*RF selection: }
    #echo $CUT_V
    CUT_V=(${CUT_V//[a-z_>_-]/ })
    #echo $CUT_V
    RATE=$(grep -m2 "Background Rate: " $dat | tail -n1)
    #echo $RATE
    RATE_V=${RATE#*Background Rate: }
    #echo $RATE_V
    RATE_V=(${RATE_V//[a-z_-]/ })
    #echo $RATE_V
    RATE_E=${RATE#*(}
    RATE_E=(${RATE_E//[a-z_%]/ })
    #echo $RATE_E
    REJ=$(grep -m1 "Background Rejected: " $dat)
    #echo $REJ
    REJ_V=${REJ#*Background Rejected: }
    #echo $REJ_V
    ACC=$(grep -m1 "Signal Acceptance: " $dat)
    #echo $ACC
    ACC_V=${ACC#*Signal Acceptance: }
    #echo $ACC_V
    ACC_E=${ACC#*+/- }
    ACC_V=(${ACC_V//[a-z_%_-_/_+]/ })
    #echo $ACC_V
    #echo $ACC_E
    #echo "$patt $CUT $RATE $REJ $ACC "
    echo "${dir##*/},$RATE_LIMIT,$dat,$CUT_V,$RATE_V,$RATE_E,$REJ_V,$ACC_V,$ACC_E" >> $RELEASE/alphaMVA/MVAStudies/${SearchString}_CSVListOfStudies.csv
    #exit
  done
  for PRIM in `ls ../myPRIMoutput*.txt`; do
    CUT=$(grep "RF: " $PRIM)
  done
done
