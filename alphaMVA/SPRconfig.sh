#!/bin/bash


#if [ `root-config --version | head -c 1` -eq 6 ]; then
#  export SPRfolder="$HOME/packages/SPR-3.3.2_root6"
#else
#  export SPRfolder="$HOME/packages/SPR-3.3.2"
#fi
export SPRfolder="$HOME/packages/SPR-3.3.2"
export SPRinclude="$SPRfolder/include"
export SPRlib="$SPRfolder/lib"
export SPRbin="$SPRfolder/bin"
export PATH=$SPRbin:$PATH
export LD_LIBRARY_PATH=$SPRlib:$LD_LIBRARY_PATH

printenv | grep ^SPR
