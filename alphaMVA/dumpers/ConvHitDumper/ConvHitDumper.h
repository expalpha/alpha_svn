

#include "../../alphaAnalysis/lib/include/TSISChannels.h"
#include "../../alphaAnalysis/lib/include/TSeq_Event.h"
#include "../../alphaAnalysis/lib/include/TSisEvent.h"
#include "../../alphaAnalysis/lib/include/TSiliconEvent.h"
#include "../../alphavmc/include/TAlphaEvent.h"
#include "../../alphavmc/include/TAlphaEventVertex.h"

#include "Utils.h"

#include "TMatrixDEigen.h"
//#include "generalizedspher.h"

#include "TROOT.h"
#include "TSystem.h"
#include "TFile.h"
#include "TTree.h"
#include "TEventList.h"

#include <iostream>
#include <algorithm>
#include <vector>

enum RunPhase{ COSMICS, MIX, WAIT, QUENCH, ALL, UV, muW, BKG, STOCH};

void dumper(char* f_name = "data/tree19324_sub0offline.root", RunPhase phaseid = COSMICS, Int_t NTrack=-1);
//void dumper(char* f_name , RunPhase phaseid );
void libsToLoad();
void InsertValue (const char* name, Int_t* value);
void InsertValue (const char* name, UChar_t* value);
void InsertValue (const char* name, Bool_t* value);
void InsertValue (const char* name, Double_t* value);
void SaveEventVariablesASCII(TSiliconEvent* siliconEvent, Int_t runnumber, Int_t phaseid, Int_t entry);
void SaveEventVariables(TSiliconEvent* siliconEvent, TAlphaEvent* alphaEvent, 
			Int_t runnumber, Int_t phaseid, Int_t entry, Int_t whichsweep=-1,Int_t NTrack);
void AddAlphaEventVariables(TAlphaEvent* alphaEvent, Int_t runnumber, Int_t phaseid, Int_t entry);
void GetRunTimes(TTree* SISTree, std::vector<double>* timestamps);




