#!/bin/bash
#Tool to run dumper of a list of runs

echo "Usage:
To use EOS path, set 3rd arguement to 1
${0} RunList.list PHASE 1


eg for local files:
${0} RunList.list MIX2
eg for EOS files
${0} RunList.list MIX2 1
"

RunList=${1}
DUMP=${2}
UseEOS=${3}


for i in `cat ${RunList}`; do
  if [ ${UseEOS} -eq 1 ]; then
    ./dumper.exe ${EOS_MGM_URL}//eos/experiment/alpha/alphaTrees/${TREEPREFIX}${i}${TREEPOSTFIX} ${DUMP} --autoQOD
  else
    ./dumper.exe ${TREEFILES}/${TREEPREFIX}${i}${TREEPOSTFIX} ${DUMP} --autoQOD
  fi
done
