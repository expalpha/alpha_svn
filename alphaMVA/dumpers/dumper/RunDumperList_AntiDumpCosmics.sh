#!/bin/bash
#Tool to run dumper in 'antidump' cosmic mode with the aid of PlotDumps 
#on a list file

echo "Usage:
${0} RunList.list 


Warning I will go a run code in PlotDumps.C (in ${RELEASE}/alphaAnalysis/macros/)
"

RunList=${1}


for i in `cat ${RunList}`; do
eos cp /eos/experiment/alpha/alphaTrees/tree${i}offline.root ${TREEFILES}/
cd ${RELEASE}/alphaAnalysis/macros/  
echo ".L PlotDumps.C+
RunMVAAntiDumpCosmicDumper(${i},1000)
.q
"|root -l -b
rm ${TREEFILES}/tree${i}offline.root
done
