
#include "TMath.h"
#include <vector>
#include "TVector.h"
using namespace std;

void sphericity(vector<double> x, vector<double> y, vector<double> z, int r, TVector3** axis, TVector3** values);
