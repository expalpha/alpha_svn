#!/bin/sh

# merge.sh runlist.runs
echo Processing files for runlist $1 with prefix $2


prefix=$2
MVANAME=$3 #optional folder to put a single study in... useful for comparing MVA studies
#if MVANAME is unset... default (old) directories are used
# e.g.: prefix=quench (n.b.: minuscolo)

rm -f ${prefix}_listtomerge.tmp~ ${prefix}_root_files_with_no_trees.txt
 
for i in `cat $1`; do
  for j in `ls ${MVATREES}/${prefix}_*tree${i}offline*.root`; do
    if [ -e ${j} ]; then
      if [ `echo "TFile::Open(\"${j}\")->ls()" | root -l -b | grep TTree | wc -l` -gt 0 ]; then
        if [ `echo "TFile::Open(\"${j}\")->ReadKeys()" | root -l -b | grep -o '[0-9]' ` -gt 0 ]; then
          #Has tree inside
          echo "Found:${j}"
          echo "${j}" >> ${prefix}_listtomerge.tmp~
        else
          echo "Empty:${j}"
          echo "${j}" >> ${prefix}_root_files_with_empty_trees.txt
        fi
      else
        echo "NoTree:${j}"
        echo "${j}" >> ${prefix}_root_files_with_no_trees.txt
      fi
    else
      echo "DoesntExist:${j}"
    fi
  #done
#  ls $RELEASE/alphaMVA/randforest/seldata/${prefix}_tree${i}offline.root >> ${MVANAME}listtomerge.tmp~
  done
done

index=0

  while [ -a mergeroot/merged_${prefix}_$(basename $1)${index}.root ]; do
    ls mergeroot/merged_${prefix}_$(basename $1)${index}.root
    index=$((index+1))
  done
  echo $index
  #echo root -l -b -q merger.C'(0,1000,"listtomerge.tmp~","mergeroot/merged_'${prefix}'_'`basename $1`'",0,"selected_'${prefix}'")'
  # root -l -b -q merger.C'(0,1000,"listtomerge.tmp~","mergeroot/merged_'${prefix}'_'`basename $1`'",0,"selected_'${prefix}'")'
  root -l -b -q merger.C'(0,100000,"'${prefix}'_listtomerge.tmp~","'${MVATREES}'/merged_'${prefix}'_'`basename $1`'",'${index}',"selected_'${prefix}'")' > `basename $1`.log
