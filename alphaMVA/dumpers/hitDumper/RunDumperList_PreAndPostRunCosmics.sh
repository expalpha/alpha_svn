#!/bin/bash
#Tool to run dumper in 'antidump' cosmic mode with the aid of PlotDumps 
#on a list file



RunDumper()
{
	RUNNO=${1}
	cd ${RELEASE}/alphaAnalysis/macros/  
#Pre catch cosmic dumper:
echo ".L PlotDumps.C+
  Double_t tmin=0.;
  Double_t CosmicEnd=MatchEventToTime(${RUNNO},\"startDump\",\"Beam\",1,0)-1.;
  RunMVACosmicHitDumper(${RUNNO},tmin,CosmicEnd);
.q
"|root -l -b
#Post FRD cosmic dumper:
echo ".L PlotDumps.C+
Int_t runNumber=${RUNNO};
if (IsDumpInATM(runNumber,\"startDump\",\"Mixing\",1,0)){
  for (Int_t i = 0; i< 5; i++){
    Double_t Start=MatchEventToTime(runNumber,\"startDump\",\"Background\",i,0);
    Double_t MixingEnd=MatchEventToTime(runNumber,\"startDump\",\"Mixing\",1,0);
    Double_t RunEnd=GetTotalRunTime(runNumber);
    if (Start>MixingEnd){
      RunMVACosmicHitDumper(${RUNNO},Start,RunEnd);
    }
  }
}
.q
"|root -l -b


	
}


echo "Usage:
${0} RunList.list 


Warning I will go a run code in PlotDumps.C (in ${RELEASE}/alphaAnalysis/macros/)
"

RunList=${1}


for i in `cat ${RunList}`; do
  jobcnt=(`jobs -p`)

  if [ ${#jobcnt[@]} -lt 8 ] ; then
    echo "Starting Job with ${i} "
    RunDumper "${i}" &
  else
    echo "waiting"
    while [ ${#jobcnt[@]} -ge 8 ]
    do
      sleep 30
      echo "8 jobs running... sleeping 30s"
      jobcnt=(`jobs -p`)
    done
    echo "Running strips $i "
    RunDumper "${i}" &
  fi


done
