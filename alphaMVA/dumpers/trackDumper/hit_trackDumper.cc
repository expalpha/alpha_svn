/*
 *
 * How to compile this code:
 * g++ -Wno-deprecated -Wno-write-strings -std=c++11 `root-config --cflags`  `root-config --glibs`  -L$ROOTSYS/lib -lXMLParser -lXMLIO -lThread -Wl,-rpath,$ROOTSYS/lib/root -DUSE_ALPHAVMC -lVMC -lGeom -lEG  -L$RELEASE/alphaAnalysis/lib -lAlphaAnalysis -L$RELEASE/alphavmc/lib/tgt_linuxx8664gcc -lalphavmc  -I`root-config --incdir` -I$RELEASE/alphaAnalysis/lib/include -I$RELEASE/alphavmc/include -o dumper.exe dumper.cc

 * Instruct the program about the file name (full path) and the kind of data
 * you want to skim (i.e., cosmics, mixing events ...)
 * eg dumper.exe $TREEFILES/tree38627offline.root COSMICS
 * Files are output to $MVATREES
 * see enum RunPhase below for possible values
 * Only cosmics for now others to be done.
 * LD_LIBRARY_PATH must include
 * setenv  LD_LIBRARY_PATH $RELEASE/alphaAnalysis/lib:$RELEASE/alphavmc/lib/tgt_linuxx8664gcc:$ROOTSYS/lib
 *
 */

#include "TSISChannels.h"
#include "TSeq_Event.h"
#include "TSisEvent.h"
#include "TSiliconEvent.h"
#include "TAlphaEvent.h"
#include "TAlphaEventVertex.h"

//Unified library dev?
//#include "../../alphaAnalysis/macros/Utils.h"



#include "TMatrixDEigen.h"

#include "TROOT.h"
#include "TSystem.h"
#include "TFile.h"
#include "TTree.h"
#include "TEventList.h"
#include <iostream>
#include <algorithm>
#include <vector>

#define VERBOSE 1
#define FIXTIMING 0
#define DUMP_AS_FLOATS 1 //Experimental for TMVA (set to 0 for SPR): Save doubles as floats, save ints as floats

enum RunPhase{ COSMICS, MIX, WAIT, QUENCH, ALL, UV, muW, BKG, STOCH,MIX2,MIX3 };

Bool_t AllowOnlineAnalysis=kFALSE;
Int_t gMix3_rep=-1;

Int_t GlobalTrackLimit=50;
Int_t NTrackCut=-1;
//Select events by time window
Bool_t gUseTimeRange=kFALSE;
Double_t gTMinCut=0.;   //Default tim time zero
Double_t gTMaxCut=1E99; //Default max time cut infinite


Double_t gInterc;
//Double_t gSlope = 1.000008814;
Double_t gSlope = 1.;
Double_t gClockDiff = 0.;
Double_t prevtvf48;

TFile* f_curr;
TFile* f_fitData;

TTree* nt_fitData;


Double_t hold_phase_start_ts=0.;
Double_t hold_phase_stop_ts=0.;

Double_t stoch_phase_start_ts=0.;
Double_t stoch_phase_stop_ts=0.;

Double_t mix_phase_start_ts=0.;
Double_t mix_phase_stop_ts=0.;

Double_t wait_phase_start_ts=0.;
Double_t wait_phase_stop_ts=0.;

Double_t quench_phase_start_ts=0.;
Double_t quench_phase_stop_ts=0.;

Double_t bkg_phase_start_ts=0.;
Double_t bkg_phase_stop_ts=0.;

Double_t sweep_phase_latest_ts=0.;

Bool_t gAutoQOD=kFALSE;
TString QODType="";

//#include "Utils.cc"
#include "TRootUtils.h"

using namespace TRootUtils;
extern TSISChannels* gSISch;

//#include "../../alphaAnalysis/macros/Utils.C"
#include "generalizedspher.cc"
std::vector<Float_t*> FloatsToFree;

//Unified library dev
//TSISChannels* gSISch;

void InsertValue (const char* name, Int_t* value)
{
#if DUMP_AS_FLOATS //Experimental code for TMVA
  TString tmp;
  Float_t* Fvalue;
  Float_t Ftemp;
  if (!finite(Ftemp)) 
  {
    std::cout <<tmp <<" is not finite! ERROR!!!"<<endl;
    exit(13);
  }
  Ftemp=*value;
  Fvalue=new Float_t(Ftemp);
  FloatsToFree.push_back(Fvalue);
  
  //Exceptions for conversion to float:
  if ( strcmp(name,"run")==0 || strcmp(name,"entry")==0 )
  {
    tmp = TString::Format("%s/I",name);
    const char* type = tmp.Data();
    TBranch* b_variable = nt_fitData->GetBranch(name);
    if (!b_variable)
      nt_fitData->Branch(name,value,type);
    else
      nt_fitData->SetBranchAddress(name,value);
  }
  else
  {
    tmp = TString::Format("%s/F",name);
    const char* type = tmp.Data();
    TBranch* b_variable = nt_fitData->GetBranch(name);
    if (!b_variable)
      nt_fitData->Branch(name,Fvalue,type);
    else
      nt_fitData->SetBranchAddress(name,Fvalue);
  }
#else //Default behaviour for SPR:
  TString tmp = TString::Format("%s/I",name);
  const char* type = tmp.Data();
  //  std::cout<<"branch: "<<name<<"\t"<<type<<"\t";

  TBranch* b_variable = nt_fitData->GetBranch(name);
  if (!b_variable)
    {
      nt_fitData->Branch(name,value,type);
      //      std::cout<<"created!"<<std::endl;
    }
  else
    {
      nt_fitData->SetBranchAddress(name,value);
      //      std::cout<<"updated!"<<std::endl;
    }
#endif

}


void InsertValue (const char* name, UChar_t* value)
{
  TString tmp = TString::Format("%s/b",name);
  const char* type = tmp.Data();
  TBranch* b_variable = nt_fitData->GetBranch(name);
  if (!b_variable)
    nt_fitData->Branch(name,value,type);
  else
    nt_fitData->SetBranchAddress(name,value);

}

void InsertValue (const char* name, Bool_t* value)
{
#if DUMP_AS_FLOATS //Experimental code for TMVA
  TString tmp;
  Float_t* Fvalue;
  Float_t Ftemp;
  Ftemp=*value;
  Fvalue=new Float_t(Ftemp);
  FloatsToFree.push_back(Fvalue);
  tmp = TString::Format("%s/F",name);
  const char* type = tmp.Data();
  TBranch* b_variable = nt_fitData->GetBranch(name);
  if (!b_variable)
    nt_fitData->Branch(name,Fvalue,type);
  else
    nt_fitData->SetBranchAddress(name,Fvalue);
#else
  TString tmp = TString::Format("%s/O",name);
  const char* type = tmp.Data();
  TBranch* b_variable = nt_fitData->GetBranch(name);
  if (!b_variable)
    nt_fitData->Branch(name,value,type);
  else
    nt_fitData->SetBranchAddress(name,value);
#endif
}

// RunTime is saved as a double; should consider moving to float
// void InsertValue (const char* name, Float_t* value) {

void InsertValue (const char* name, Double_t* value)
{

#if DUMP_AS_FLOATS //Experimental code for TMVA
  TString tmp;
  tmp = TString::Format("%s/F",name);
  Float_t* Fvalue;
  Float_t Ftemp;
  Ftemp=*value;
  if (!finite(Ftemp)) 
  {
    std::cout <<tmp <<" is not finite! ERROR!!!"<<endl;
    exit(13);
  }
//  Fvalue=(Float_t*)value;
  Fvalue=new Float_t(Ftemp);
  FloatsToFree.push_back(Fvalue);
  const char* type = tmp.Data();
  TBranch* b_variable = nt_fitData->GetBranch(name);
  if (!b_variable)
    nt_fitData->Branch(name,Fvalue,type);
  else
    nt_fitData->SetBranchAddress(name,Fvalue);
#else //Default behaviour for SPR:
  TString tmp = TString::Format("%s/D",name);//  TString tmp = TString::Format("%s/F",name);
  const char* type = tmp.Data();
  TBranch* b_variable = nt_fitData->GetBranch(name);
  if (!b_variable)
    nt_fitData->Branch(name,value,type);
  else
    nt_fitData->SetBranchAddress(name,value);
#endif
}
void InsertValue (const char* name, Float_t* value)
{

  TString tmp = TString::Format("%s/F",name);//  TString tmp = TString::Format("%s/F",name);
  const char* type = tmp.Data();
  if (!finite(*value)) 
  {
    std::cout <<tmp <<" is not finite! ERROR!!!"<<endl;
    exit(13);
  }
  TBranch* b_variable = nt_fitData->GetBranch(name);
  if (!b_variable)
    nt_fitData->Branch(name,value,type);
  else
    nt_fitData->SetBranchAddress(name,value);

}
void InsertValue (const char* name, TString* value)
{
  TString tmp = TString::Format("%s/C",name);//  TString tmp = TString::Format("%s/F",name);
  const char* type = tmp.Data();
  TBranch* b_variable = nt_fitData->GetBranch(name);
  if (!b_variable)
    nt_fitData->Branch(name,(void*)value->Data(),type,2048);
  else
    nt_fitData->SetBranchAddress(name,(void*)value->Data());
}  


void SaveEventVariablesASCII(TSiliconEvent* siliconEvent, Int_t runnumber, Int_t phaseid, Int_t entry){

}

Double_t rad_cut = 4.20; // cm  Updated to 2014 capra optimization
Double_t res_cut_eq2 = 2.50; // cm^2
Double_t res_cut_gr2 = 0.08;

Double_t rad_cut_2012=4.0;
Double_t res_cut_eq2_2012=2.0;
Double_t res_cut_gr2_2012=0.05;
  Bool_t SkipCheck=kFALSE;
  Float_t NstripADC[72][256];
  Float_t NstripSig[72][256]; //ADC / RMS (significance)
  Float_t PstripADC[72][256];
  Float_t PstripSig[72][256]; //ADC / RMS (significance)
    Bool_t NVA[72][2];
  Bool_t PVA[72][2];
  Float_t NHitsPerASIC[72][2];
  Float_t PHitsPerASIC[72][2];
  Float_t NSumHitADCPerASIC[72][2];
  Float_t PSumHitADCPerASIC[72][2];

void SaveEventVariables(TSiliconEvent* siliconEvent, TAlphaEvent* alphaEvent,
                        Int_t runnumber, Int_t phaseid, Int_t entry, Double_t tfromQ, TString LaserDump_Name="",Int_t LaserDump_Num=-1)
{

  TVector3* vtx;
  Double_t r, z, phi, phioffset;
  Double_t tSIS, tfrommix, tfromquench, tfromwait, SiRunTime, tfromsweep, tfromstoch, tfromhold;
  Double_t tvf48, deltatvf48;
  Int_t ntracks, nvertices;
  Double_t residual;
  Double_t residualL;
  Double_t cosmChi2ProbNdof1; // def impropria, perche` i residui non vengono divisi per l'errore prima di sommarli
  Double_t dca, cosphi, dcar;
  Bool_t passcut;
  Bool_t passcut_2012;
  Bool_t HasVert;
  
  if (!SkipCheck)
  {
    for (Int_t i=0; i<72; i++)
    {
      for (Int_t j=0; j<2; j++)
      {

        TString name="NHitsPerASIC_"; name+=i; name+="_"; name+=j;
        TString tmp(name); tmp+="/F";
        const char* type = tmp.Data();
        nt_fitData->Branch(name,&NHitsPerASIC[i][j],type);

        name="PHitsPerASIC_"; name+=i; name+="_"; name+=j;
        tmp=name; tmp+="/F";
        type = tmp.Data();
        nt_fitData->Branch(name,&PHitsPerASIC[i][j],type);

        name="NSumHitADCPerASIC_"; name+=i; name+="_"; name+=j;
        tmp=name; tmp+="/F";
        type = tmp.Data();
        nt_fitData->Branch(name,&NSumHitADCPerASIC[i][j],type);

        name="PSumHitADCPerASIC_"; name+=i; name+="_"; name+=j;
        tmp=name; tmp+="/F";
        type = tmp.Data();
        nt_fitData->Branch(name,&PSumHitADCPerASIC[i][j],type);
      }
      for (Int_t j=0; j<256; j++)
      { 
        TString name="NstripADC_"; name+=i; name+="_"; name+=j; 
        TString tmp(name); tmp+="/F";
        const char* type = tmp.Data();
        //nt_fitData->Branch(name,&NstripADC[i][j],type);
        //InsertValue(tmp.Data(), &NstripADC[i][j],SkipCheck);
        name="PstripADC_"; name+=i; name+="_"; name+=j; 
        tmp=name; tmp+="/F";
        type = tmp.Data();
        //nt_fitData->Branch(name,&PstripADC[i][j],type);
        //InsertValue(tmp.Data(), &PstripADC[i][j],SkipCheck);
      }
    }
  }
  SkipCheck=kTRUE;
  

  //Make sure arrays are set to zero
  for (Int_t i=0; i<72; i++)
  {
    for (Int_t j=0; j<2; j++)
    {
      NHitsPerASIC[i][j]=0.;
      PHitsPerASIC[i][j]=0.;
      NSumHitADCPerASIC[i][j]=0;
      PSumHitADCPerASIC[i][j]=0;
      NVA[i][j]=kFALSE;
      PVA[i][j]=kFALSE;
	}
    for (Int_t j=0; j<256; j++)
    {
      NstripADC[i][j]=0.;
      NstripSig[i][j]=0.;
      PstripADC[i][j]=0.;
      PstripSig[i][j]=0.;
    }
  }
  Int_t ZFlip=0;//(siliconEvent->GetVF48NEvent() % 2);
  for (Int_t i=0;i<alphaEvent->GetNSil(); i++)
  {
    TAlphaEventSil *sil = alphaEvent->GetSil(i);
    Int_t SilNo=sil->GetSilNum();
    if (ZFlip)
    {
      if (SilNo>36) SilNo=SilNo-36;
      else SilNo+=36;
    }
    //if (!sil) continue;
    for (Int_t j=0; j<sil->GetNNClusters(); j++)
    { 
      TAlphaEventNCluster* Ncluster=sil->GetNCluster(j);
      for (Int_t k=0; k<Ncluster->GetNStrips(); k++)
      {
        TAlphaEventNStrip* NStrip=Ncluster->GetStrip(k);
        Int_t stripNo=     NStrip->GetStripNumber();

        Double_t stripADC= NStrip->GetADC();
        Double_t stripRMS= NStrip->GetStripRMS();

        if (stripNo<128)
        {
          NVA[SilNo][0+ZFlip]=kTRUE;
          NSumHitADCPerASIC[SilNo][0+ZFlip]=NSumHitADCPerASIC[SilNo][0+ZFlip]+(Float_t)stripRMS;
          NHitsPerASIC[SilNo][0+ZFlip]=NHitsPerASIC[i][0+ZFlip]+1;
        }
        else
        {
          NVA[SilNo][1-ZFlip]=kTRUE;
          NSumHitADCPerASIC[SilNo][1-ZFlip]=NSumHitADCPerASIC[SilNo][1-ZFlip]+(Float_t)stripRMS;
          NHitsPerASIC[SilNo][1-ZFlip]=NHitsPerASIC[SilNo][1-ZFlip]+1;
        }
        //cout <<"N:"<<stripNo <<":"<<stripADC<<":"<<stripRMS<<endl;
        NstripADC[SilNo][stripNo]=(Float_t)(stripADC);
        NstripSig[SilNo][stripNo]=(Float_t)(stripADC/stripRMS);
      }
    }

    
    for (Int_t j=0; j<sil->GetNPClusters(); j++)
    { 
      TAlphaEventPCluster* Pcluster=sil->GetPCluster(j);
      for (Int_t k=0; k<Pcluster->GetNStrips(); k++)
      {
        TAlphaEventPStrip* PStrip=Pcluster->GetStrip(k);
        Int_t stripNo=     PStrip->GetStripNumber();
        Double_t stripADC= PStrip->GetADC();
        Double_t stripRMS= PStrip->GetStripRMS();
        //cout <<"P:"<<stripNo <<":"<<stripADC<<":"<<stripRMS<<endl;
        PstripADC[SilNo][stripNo]=(Float_t)(stripADC);
        PstripSig[SilNo][stripNo]=(Float_t)(stripADC/stripRMS);
        
        if (stripNo<128)
        {
          PVA[SilNo][0+ZFlip]=kTRUE;
          PSumHitADCPerASIC[SilNo][0+ZFlip]=PSumHitADCPerASIC[SilNo][0+ZFlip]+(Float_t)stripADC;
          PHitsPerASIC[SilNo][0+ZFlip]++;
        }
        else
        {
          PVA[SilNo][1-ZFlip]=kTRUE;
          PSumHitADCPerASIC[SilNo][1-ZFlip]=PSumHitADCPerASIC[SilNo][1-ZFlip]+(Float_t)stripADC;
          PHitsPerASIC[SilNo][1-ZFlip]++;
        }
      }
    }
  }
 
  Int_t TTCEvents[4]={0};
  // Projection method vars
  TVector3* pvtx;
  Double_t phip, thetap, pseudorapidityp;
  Bool_t HasProjVert;
  
  // Cosmic Vector experiment
  TVector3* cVec;
  Double_t CosVecPerp, CosVecX, CosVecY,CosVecZ, phi_CosVec;
  for (Int_t j=0; j<4;j++)
  {
    TTCEvents[j]=siliconEvent->GetTTCCounter(j);
  }
  Int_t* ByLayer=TTC_Multiplicity_by_Layer( runnumber, TTCEvents) ;
  Int_t TTC_Inner=*ByLayer;
  Int_t TTC_Middle=*(ByLayer+1);
  Int_t TTC_Outer=*(ByLayer+2);  
  delete ByLayer;
  Int_t TTC_Total=TTC_Inner+TTC_Middle+TTC_Outer;
  Int_t TTC_Mean=(double)TTC_Total/3.;
  Int_t TTC_Var=
    ((double)TTC_Inner-TTC_Mean)*((double)TTC_Inner-TTC_Mean)+
    ((double)TTC_Middle-TTC_Mean)*((double)TTC_Middle-TTC_Mean)+
    ((double)TTC_Outer-TTC_Mean)*((double)TTC_Outer-TTC_Mean);
  TTC_Var=TTC_Var/3.;

  ntracks = siliconEvent->GetNTracks();
  HasVert=(siliconEvent->GetVertexType() & 1);
  HasProjVert=(siliconEvent->GetVertexType() & 2);
  
  nvertices = siliconEvent->GetNVertices();
  residual = siliconEvent->GetResidual();
  dca = siliconEvent->GetDCA();

  if (residual > 0.)
    residualL = TMath::Log(residual);
  else
    residualL = -99.;

  if (TMath::IsNaN(residualL))
    residualL = -99.;

  cosmChi2ProbNdof1 = TMath::Prob(residual,1);

  vtx = siliconEvent->GetVertex();
  pvtx = siliconEvent->GetProjVertex();
  cVec = siliconEvent->GetCosmicVector();
  
  
  
  
  if (nvertices > 0)
    {
      r = vtx->Perp();
      z = vtx->Z();
      phi = vtx->Phi();
      cosphi = TMath::Cos(phi);
      phi*=TMath::RadToDeg();
      phip = pvtx->Phi();
      phip*=TMath::RadToDeg();
      thetap= pvtx->Theta()*TMath::RadToDeg();
      pseudorapidityp=pvtx->PseudoRapidity();
      
      
      //Experimental Cosmic Vector
      
      CosVecPerp = cVec->Perp();
      CosVecX = cVec->X();
      CosVecY = cVec->Y();
      CosVecZ = cVec->Z();
      //cout <<CosVecX <<" : " << CosVecY <<" : " <<CosVecZ <<" : " <<endl;
      phi_CosVec = TMath::ACos(cVec->Y()/TMath::Sqrt(cVec->X()*cVec->X()+cVec->Y()*cVec->Y()));
      
    }
  else
    {
      r = -99.;
      z = -99.;
      phi = -9999.;
      cosphi = -9999.;
      phip=-9999.;
      thetap=-9999.;
      pseudorapidityp=-99.;
      
            
      CosVecPerp = -99;
      CosVecX = -99;
      CosVecY = -99;
      CosVecZ = -99;
      phi_CosVec = -99;
    }
  phioffset = 0.; // check
  // checked: very small from cosmic analysis

  // dca of a 2-tracks event failing the residual cut
  if(ntracks==2 && residual<res_cut_eq2)
    dcar=dca;
  else
    dcar=-99.;

  passcut = ( nvertices==1 && r<rad_cut &&
	    ( (ntracks==2 && residual>res_cut_eq2) || (ntracks>2 && residual > res_cut_gr2) )
	    );
  passcut_2012 = ( nvertices==1 && r<rad_cut_2012 &&
	    ( (ntracks==2 && residual>res_cut_eq2_2012) || (ntracks>2 && residual > res_cut_gr2_2012) )
	    );
	

  //  cout<<"\nEvent: "<<entry<<"\t pass: "<<passcut<<"\t";

  // Double_t xi= siliconEvent->GetVF48Timestamp();
  // Double_t yi= siliconEvent->GetRunTime();
  // SiRunTime = yi;

  //  Double_t toff = siliconEvent->GetRunTime() - siliconEvent->GetVF48Timestamp();

  // // actually, not necessary, since setruntime is run in the calling function dumpntuple() before passing siliconEvent to this one.
  // t = gInterc + xi*gSlope;

  //  siliconEvent->SetRunTime(t);

  SiRunTime = siliconEvent->GetRunTime();
  
  if (SiRunTime<gTMinCut) return;   //Default min time zero
  if (SiRunTime>gTMaxCut) return;   //Default max time cut infinite (1E99)

  
  //  std::cout<<" Saved in dumper "<<SiRunTime;
  // tfrommix = t - mix_phase_start_ts;
  // tfromquench = t - quench_phase_start_ts;
  // tfromwait = t - wait_phase_start_ts;
  // tfromsweep = t - sweep_phase_latest_ts;  

  tfrommix = SiRunTime - mix_phase_start_ts;
  tfromquench = SiRunTime - quench_phase_start_ts;
  tfromwait = SiRunTime - wait_phase_start_ts;
  tfromsweep = SiRunTime - sweep_phase_latest_ts;
  tfromstoch = SiRunTime - stoch_phase_start_ts;
  tfromhold = SiRunTime - hold_phase_start_ts;

  // testing
  tvf48 = siliconEvent->GetVF48Timestamp();
  deltatvf48 = SiRunTime - siliconEvent->GetTSRunTime();
#if FIXTIMING
  tSIS = gInterc + tvf48 * gSlope;
#else
  tSIS = siliconEvent->GetTSRunTime();
#endif
  // alpha event part
  TAlphaEventVertex* aevtx = alphaEvent->GetVertex();

  Double_t norm=0.;

  std::vector<double> velx;
  std::vector<double> vely;
  std::vector<double> velz;

  std::vector<double> velxraw;
  std::vector<double> velyraw;
  std::vector<double> velzraw;


  Int_t nAT =  alphaEvent->GetNHelices(); // all tracks
  Int_t nCT = 0;
  Int_t nraw = 0;
  
  Double_t AT_MeanHitSig=0; //Average hit significance
  Double_t CT_MeanHitSig=0; //Average hit significance
  Double_t AT_SumHitSig=0;
  Double_t CT_SumHitSig=0;
  Int_t AT_HitSigCounter=0;
  Int_t CT_HitSigCounter=0;
  for (int i = 0; i< nAT ; ++i)
    {
      TAlphaEventHelix* aehlx = alphaEvent->GetHelix(i);
      Double_t fc = aehlx->Getfc();
      Double_t fphi0 = aehlx->Getfphi();
      Double_t fLambda = aehlx->Getflambda();
      Double_t HitSignificance=aehlx->GetTotalHitSignificance();
      Double_t chi2=aehlx->GetChi2();
      Double_t fd0_trap=aehlx->Getfd0_trap();
      TString TrackName="ATtrack";
      TrackName+=i;
      TrackName+="_";
      for (Int_t ihit=0; ihit<3; ihit++)
      {
        TAlphaEventHit* aehlx_hit=aehlx->GetHit(ihit);
        Double_t HitSig=aehlx_hit->GetHitSignifance();
        TString HitName=TrackName;
        HitName+="hit";
        HitName+=ihit;
        HitName+="sig";
        InsertValue(HitName,&HitSig);
      }
      Int_t HelixStatus=aehlx->GetHelixStatus();
      InsertValue(TrackName+"status",&HelixStatus);
      InsertValue(TrackName+"fc",&fc);
      InsertValue(TrackName+"fphi0",&fphi0);
      InsertValue(TrackName+"fLambda",&fLambda);
      InsertValue(TrackName+"Sig",&HitSignificance);
      InsertValue(TrackName+"chi2",&chi2);
      InsertValue(TrackName+"fd0_trap",&fd0_trap);
      
      //Is it in vertex?
      
      Double_t s=0.; // calculates velx,y,z at POCA
      // special case for s = 0
      Int_t HelixHits=aehlx->GetNHits();
      for (int j=0; j<HelixHits; j++)
      {
        TAlphaEventHit* aehlx_hit=aehlx->GetHit(j);
        AT_SumHitSig+=aehlx_hit->GetHitSignifance();
        AT_HitSigCounter++;
      }
      
      // select good helices, after removal of duplicates
      
      // This seems to be true all the time... please check - Joe
      if (aehlx->GetHelixStatus()==1)
      {
        for (int j=0; j<HelixHits; j++)
        {
          TAlphaEventHit* aehlx_hit=aehlx->GetHit(j);
          CT_SumHitSig+=aehlx_hit->GetHitSignifance();
          CT_HitSigCounter++;
     
        }
        ++nraw; // == ntracks
        velxraw.push_back( - TMath::Sin(fphi0));
        velyraw.push_back( TMath::Cos(fphi0)) ;
        velzraw.push_back( fLambda );
        ++nCT;
      }

      //delete aehlx;
    }
  if (NTrackCut<0)
  { //Insert NULL values
    for (int i = nAT; i< GlobalTrackLimit; ++i)
    {
      Double_t fc = -99.;
      Double_t fphi0 = -99.;
      Double_t fLambda = -99.;
      Double_t HitSignificance=-99;
      Double_t chi2=-99;
      Double_t fd0_trap=-99;

      TString TrackName="ATtrack";
      TrackName+=i;
      TrackName+="_";
      for (Int_t ihit=0; ihit<3; ihit++)
      {
        Double_t HitSig=0;
        TString HitName=TrackName;
        HitName+="hit";
        HitName+=ihit;
        HitName+="sig";
        InsertValue(HitName,&HitSig);
      }
      Int_t HelixStatus=0;
      InsertValue(TrackName+"status",&HelixStatus);
      InsertValue(TrackName+"fc",&fc);
      InsertValue(TrackName+"fphi0",&fphi0);
      InsertValue(TrackName+"fLambda",&fLambda);
      InsertValue(TrackName+"Sig",&HitSignificance);
      InsertValue(TrackName+"chi2",&chi2);
      InsertValue(TrackName+"fd0_trap",&fd0_trap);
    }
  }
  if (AT_HitSigCounter>0)
    AT_MeanHitSig= AT_SumHitSig / (Double_t) AT_HitSigCounter;
  if (CT_HitSigCounter>0)
    CT_MeanHitSig= CT_SumHitSig / (Double_t) CT_HitSigCounter;

  Int_t nGTL = aevtx->GetNHelices();// tracks with vertex
  int nGT = 0;
  Double_t curvemin=9999.,curvemean=0.,lambdamin=9999.,lambdamean=0,curvesign=0;
  for (int i = 0; i< nGTL ; ++i)
    {
      TAlphaEventHelix* aehlx = aevtx->GetHelix(i);
      //    if(aehlx->GetHelixStatus()<0) continue;
      Double_t fc = aehlx->Getfc();
      Double_t fphi0 = aehlx->Getfphi();
      Double_t fLambda = aehlx->Getflambda();
      Double_t chi2=aehlx->GetChi2();
      Double_t fd0_trap=aehlx->Getfd0_trap();
      Double_t HitSignificance = aehlx->GetTotalHitSignificance();
      TString TrackName="GTtrack";
      TrackName+=i;
      TrackName+="_";
      for (Int_t ihit=0; ihit<3; ihit++)
      {
        TAlphaEventHit* aehlx_hit=aehlx->GetHit(ihit);
        Double_t HitSig=aehlx_hit->GetHitSignifance();
        TString HitName=TrackName;
        HitName+="hit";
        HitName+=ihit;
        HitName+="sig";
        InsertValue(HitName,&HitSig);
      }

      Int_t HelixStatus=aehlx->GetHelixStatus();
      InsertValue(TrackName+"status",&HelixStatus);
      InsertValue(TrackName+"fc",&fc);
      InsertValue(TrackName+"fphi0",&fphi0);
      InsertValue(TrackName+"fLambda",&fLambda);
      InsertValue(TrackName+"chi2",&chi2);
      InsertValue(TrackName+"fd0_trap",&fd0_trap);
      InsertValue(TrackName+"Sig",&HitSignificance);

      Double_t s=0.; // calculates velx,y,z at POCA
      // special case for s = 0
      velx.push_back( - TMath::Sin(fphi0) );
      vely.push_back( TMath::Cos(fphi0) ) ;
      velz.push_back( fLambda );

      // select good helices, after removal of duplicates
      if (aehlx->GetHelixStatus()==1)
        {
          ++nGT;
          curvemin= fabs(fc)>curvemin? curvemin:fabs(fc);
          lambdamin= fabs(fLambda)>lambdamin? lambdamin:fabs(fLambda);
          curvemean+=fabs(fc);
          lambdamean+=fabs(fLambda);
          curvesign+=(fc>0)?1:-1;
        }
      
      delete aehlx;
    }
  if(nGT>0){
    lambdamean/=nGT;
    curvemean/=nGT;
  }
  if (NTrackCut<0)
  { //Insert NULL values
    for (int i = nGT; i< GlobalTrackLimit; ++i)
    {
      Double_t fc = -99.;
      Double_t fphi0 = -99.;
      Double_t fLambda = -99.;
      Double_t HitSignificance=-99;
      Double_t chi2=-99;
      Double_t fd0_trap=-99;

      TString TrackName="GTtrack";
      TrackName+=i;
      TrackName+="_";
      for (Int_t ihit=0; ihit<3; ihit++)
      {
        Double_t HitSig=0;
        TString HitName=TrackName;
        HitName+="hit";
        HitName+=ihit;
        HitName+="sig";
        InsertValue(HitName,&HitSig);
      }
      Int_t HelixStatus=0;
      InsertValue(TrackName+"chi2",&chi2);
      InsertValue(TrackName+"fd0_trap",&fd0_trap);
      InsertValue(TrackName+"status",&HelixStatus);
      InsertValue(TrackName+"fc",&fc);
      InsertValue(TrackName+"fphi0",&fphi0);
      InsertValue(TrackName+"fLambda",&fLambda);
      InsertValue(TrackName+"Sig",&HitSignificance);
    }
  }
  //  cout<<"\t nAT: "<<nAT<<" nCT: "<<nCT<<" nraw: "<<nraw<<" nGTL: "<<nGTL<<" nGT: "<<nGT<<" Ntracks: "<<ntracks<<endl;


  TVector3* S0axis;
  TVector3* S0values;

  TVector3* S0axisraw;
  TVector3* S0valuesraw;

  Double_t S0 = -99.;
  Double_t S0raw = -99.;

  Double_t S0Perp = -99.;
  Double_t S0rawPerp = -99.;

  Double_t S0rawl1 = -99.;
  Double_t S0rawl2 = -99.;
  Double_t S0rawl3 = -99.;

  Double_t S0l1 = -99.;
  Double_t S0l2 = -99.;
  Double_t S0l3 = -99.;

  Double_t phi_S0axis = -99.;
  Double_t phi_absS0axis = -99.;

  Double_t phi_S0axisraw = -99.;
  Double_t phi_absS0axisraw = -99.;

  //   Double_t S0raw = -99.; // C, D, A

  Double_t S0axisrawX = -99.;
  Double_t S0axisrawY = -99.;
  Double_t S0axisrawZ = -99.;

  Double_t S0axisX = -99.;
  Double_t S0axisY = -99.;
  Double_t S0axisZ = -99.;

  TObjArray* hits=alphaEvent->GatherHits();
  Int_t nhits=hits->GetEntries();

  Int_t nhits_nomatch = -99.;
  Double_t yhitsasym = -99; //diff on y axis
  Double_t nhitsasym = -99.; // diff between hemispheres
  Double_t nhitsasymraw = -99.; // diff between hemispheres
  //AO test
  if(nhits != siliconEvent->GetNHits()) std::cout<<"Saved nhits alphaevent "<<entry<<"  = "<<nhits<< " sievent "<<  siliconEvent->GetNHits()<<std::endl;

  if(nraw>0)
    {
      nhitsasymraw = 0;

      sphericity(velxraw, velyraw, velzraw, 0, &S0axisraw, &S0valuesraw); // generalizedspher.h

      S0raw = 1.5*(S0valuesraw->Y()+S0valuesraw->Z());
      S0rawPerp = S0valuesraw->Perp();

      S0rawl1 = S0valuesraw->X();
      S0rawl2 = S0valuesraw->Y();
      S0rawl3 = S0valuesraw->Z();

      S0axisrawX = S0axisraw->X();
      S0axisrawY = S0axisraw->Y();
      S0axisrawZ = S0axisraw->Z();

      phi_S0axisraw = TMath::ACos(S0axisrawY/TMath::Sqrt(S0axisrawX*S0axisrawX+S0axisrawY*S0axisrawY));
      phi_absS0axisraw = TMath::ACos(TMath::Abs(S0axisrawY/TMath::Sqrt(S0axisrawX*S0axisrawX+S0axisrawY*S0axisrawY)));

      for (int i = 0; i< nhits ; ++i)
      {
        TAlphaEventHit* aehit = (TAlphaEventHit*) hits->At(i);
        TVector3* hitpos = new TVector3(aehit->X(),aehit->Y(),aehit->Z());
        Double_t dotprod = hitpos->Dot(*S0axisraw);
        if (dotprod>0.)
          ++nhitsasymraw;
        else if (dotprod<0.)
          --nhitsasymraw;
        delete hitpos;
        
      }
      nhitsasymraw/=nhits;
      delete S0axisraw;
      delete S0valuesraw;
    }

  if(nGTL>0)
    {
      // deg=1 => linearized sphericity; deg=2 => default non infrared-safe sphericity
      nhitsasym = 0;
      yhitsasym = 0;
      sphericity(velx, vely, velz, 0, &S0axis, &S0values); // generalizedspher.h

      S0 = 1.5*(S0values->Y()+S0values->Z());
      S0Perp = S0values->Perp();

      S0l1 = S0values->X();
      S0l2 = S0values->Y();
      S0l3 = S0values->Z();

      S0axisX = S0axis->X();
      S0axisY = S0axis->Y();
      S0axisZ = S0axis->Z();

      phi_S0axis = TMath::ACos(S0axisY/TMath::Sqrt(S0axisX*S0axisX+S0axisY*S0axisY));
      phi_absS0axis = TMath::ACos(TMath::Abs(S0axisY/TMath::Sqrt(S0axisX*S0axisX+S0axisY*S0axisY)));

      for (int i = 0; i< nhits ; ++i)
      {
        TAlphaEventHit* aehit = (TAlphaEventHit*) hits->At(i);
        TVector3* hitpos = new TVector3(aehit->X(),aehit->Y(),aehit->Z());
        Double_t ydotprod = hitpos->Dot(TVector3(0.,1.,0.));
        Double_t dotprod = hitpos->Dot(*S0axis);
        if (ydotprod>0.)
          ++yhitsasym;
        else if (ydotprod<0.)
          --yhitsasym;
         
        if (dotprod>0.)
          ++nhitsasym;
        else if (dotprod<0.)
         --nhitsasym;
        delete hitpos;
      }
      nhitsasym/=nhits;
    }


  InsertValue("nhits",&nhits);
  InsertValue("nhitsasym",&nhitsasym);
  InsertValue("yhitsasym",&yhitsasym);
  InsertValue("nhitsasymraw",&nhitsasymraw);
  InsertValue("curvemin",&curvemin);
  InsertValue("curvemean",&curvemean);
  InsertValue("curvesign",&curvesign);
  InsertValue("lambdamin",&lambdamin);
  InsertValue("lambdamean",&lambdamean);
  InsertValue("S0",&S0);
  InsertValue("S0raw",&S0raw);
  InsertValue("S0Perp",&S0Perp);
  InsertValue("S0rawPerp",&S0rawPerp);

  InsertValue("S0axisrawX",&S0axisrawX);
  InsertValue("S0axisrawY",&S0axisrawY);
  InsertValue("S0axisrawZ",&S0axisrawZ);

  InsertValue("S0axisX",&S0axisX);
  InsertValue("S0axisY",&S0axisY);
  InsertValue("S0axisZ",&S0axisZ);

  InsertValue("nAT",&nAT);
  InsertValue("nCT",&nCT);
  InsertValue("nGT",&nGT);
  InsertValue("nGTL",&nGTL);

  InsertValue("AT_MeanHitSig",&AT_MeanHitSig);
  InsertValue("CT_MeanHitSig",&CT_MeanHitSig);
  InsertValue("AT_SumHitSig",&AT_SumHitSig);
  InsertValue("CT_SumHitSig",&CT_SumHitSig);
  
  InsertValue("S0rawl1",&S0rawl1);
  InsertValue("S0rawl2",&S0rawl2);
  InsertValue("S0rawl3",&S0rawl3);

  InsertValue("S0l1",&S0l1);
  InsertValue("S0l2",&S0l2);
  InsertValue("S0l3",&S0l3);

  InsertValue("phi_S0axis",&phi_S0axis);
  InsertValue("phi_absS0axis",&phi_absS0axis);

  InsertValue("phi_S0axisraw",&phi_S0axisraw);
  InsertValue("phi_absS0axisraw",&phi_absS0axisraw);

  InsertValue("run",&runnumber);
  InsertValue("entry",&entry);
  InsertValue("phaseid",&phaseid);

  InsertValue("r",&r);
  InsertValue("z",&z);
  InsertValue("phi",&phi);
  InsertValue("phioffset",&phioffset);

  InsertValue("cosphi",&cosphi);
  InsertValue("phip",&phip);
  InsertValue("thetap",&thetap);
  InsertValue("pseudorapidityp",&pseudorapidityp);
  InsertValue("tvf48",&tvf48);
  InsertValue("deltatvf48",&deltatvf48);

  InsertValue("tSIS",&tSIS);
  InsertValue("SiRunTime",&SiRunTime);

  InsertValue("tfromQ",&tfromQ);

  InsertValue("tfrommix",&tfrommix);
  InsertValue("tfromquench",&tfromquench);
  InsertValue("tfromwait",&tfromwait);
  InsertValue("tfromstoch",&tfromstoch);

  InsertValue("ntracks",&ntracks);
  InsertValue("nvertices",&nvertices);
  InsertValue("residual",&residual);
  InsertValue("residualL",&residualL);
  InsertValue("cosmChi2ProbNdof1",&cosmChi2ProbNdof1);

  InsertValue("tracksdca",&dca);
  InsertValue("tracksdcacut",&dcar);

  InsertValue("passcut",&passcut);
  InsertValue("passcut_2012",&passcut_2012);


  InsertValue("HasVert",&HasVert);
  InsertValue("HasProjVert",&HasProjVert);

  InsertValue("LaserDumpName",&LaserDump_Name);
  InsertValue("tfromsweep",&tfromsweep);
  InsertValue("LaserDumpNum",&LaserDump_Num);

  //TTC
  InsertValue("TTC_Inner",&TTC_Inner);
  InsertValue("TTC_Middle",&TTC_Middle);
  InsertValue("TTC_Outer",&TTC_Outer);
  InsertValue("TTC_Total",&TTC_Total);
  InsertValue("TTC_Mean",&TTC_Mean);
  InsertValue("TTC_Var",&TTC_Var);

      
  InsertValue("CosVecPerp",&CosVecPerp);
  InsertValue("CosVecX",&CosVecX);
  InsertValue("CosVecY",&CosVecY);
  InsertValue("CosVecZ",&CosVecZ);
  InsertValue("phi_CosVec",&phi_CosVec);


  nt_fitData->Fill();
  alphaEvent->DeleteEvent(); 
  for(Int_t clear=0; clear<FloatsToFree.size(); clear++)
    if (FloatsToFree[clear]) 
    {
      delete FloatsToFree[clear];
      FloatsToFree[clear]=NULL;
    }
      
  //hits->SetOwner(kTRUE);
  //hits->Delete();
  delete hits;
  siliconEvent->Clear();

}


void AddAlphaEventVariables(TAlphaEvent* alphaEvent, Int_t runnumber, Int_t phaseid, Int_t entry)
{

  // insert custom code here

}


void GetRunTimes(TTree* SISTree, std::vector<double>* timestamps)
{
  TSisEvent* SISEvent = new TSisEvent();

  SISTree->SetBranchAddress("SisEvent", &SISEvent );
  Double_t runtime;
  for( Int_t i=0; i<SISTree->GetEntries(); ++i)
    {
      SISTree->GetEntry(i);
      runtime = SISEvent->GetRunTime();
      timestamps->push_back(runtime);
  }

  delete SISEvent;
}



Bool_t MIXAutoQOD(Int_t RunNo, Int_t NMix=1) //Dump repetition counts from 1
{
   cout <<"Running AutoQOD (Mixing "<<NMix<<")"<<endl;
   if (!gAutoQOD) return kTRUE;
   Double_t tmin=TRootUtils::MatchEventToTime(RunNo,"startDump","Mixing",NMix);
   Double_t tmax=TRootUtils::MatchEventToTime(RunNo,"stopDump","Mixing",NMix);
   std::vector<Bool_t> TestResult;
   std::vector<TString> TestName;
   Int_t TRIG=gSISch->GetChannel("IO32_TRIG_NOBUSY"); 
   Int_t READOUT=gSISch->GetChannel("IO32_TRIG"); 
   
   TestName.push_back("MixingTriggers>2000");
   if (TRootUtils::Count_SIS_Triggers(RunNo,TRIG,tmin,tmax) > 2000 )
     TestResult.push_back(kTRUE);
   else
     TestResult.push_back(kFALSE);
   
   
   TestName.push_back("MixingReadouts>500");
   if (TRootUtils::Count_SIS_Triggers(RunNo,READOUT,tmin,tmax) > 500 )
     TestResult.push_back(kTRUE);
   else
     TestResult.push_back(kFALSE);
   
   
   TestName.push_back("MixTime<1.1s");
   if (tmax-tmin<1.1 )
     TestResult.push_back(kTRUE);
   else
     TestResult.push_back(kFALSE);
   
   
   TestName.push_back("MixTime>1s");
   if (tmax-tmin>1. )
     TestResult.push_back(kTRUE);
   else
     TestResult.push_back(kFALSE);
   
   
   
   TTree* sil_tree = Get_Vtx_Tree( RunNo );
   TSiliconEvent* sil_event = new TSiliconEvent();
   sil_tree->SetBranchAddress("SiliconEvent", &sil_event);
   sil_tree->SetBranchStatus("SiliconModules",0);
   Int_t VF48Events=0;
   Int_t Verts=0;
   Int_t PassedCuts=0;
   for( Int_t i=0; i<sil_tree->GetEntries(); i++ )
   {
     sil_event->ClearEvent();
     sil_tree->GetEntry(i);
     if( sil_event->GetRunTime() <= tmin ) continue;
     if( sil_event->GetRunTime() > tmax ) break;
     VF48Events++;
     if(sil_event->GetNVertices()>0)
     {
       Verts++;
       if (TRootUtils::ApplyCuts( sil_event))
         PassedCuts++;
     }
   }
   delete sil_event;
   delete sil_tree;
   TestName.push_back("Vertex Rate>100");
   if ((Double_t)Verts/(tmax-tmin)> 100 )
     TestResult.push_back(kTRUE);
   else
     TestResult.push_back(kFALSE);
   
   
   TestName.push_back("Passed Cut Rate>100");
   if ((Double_t)PassedCuts/(tmax-tmin)> 100 )
     TestResult.push_back(kTRUE);
   else
     TestResult.push_back(kFALSE);
   
   
   
   TAnalysisReport* report=TRootUtils::GetAnalysisReport(RunNo);
   
   TestName.push_back("alphaAnalysis reached last mid file");
   if (report->GetStopTimeBinary()>100000000) 
     TestResult.push_back(kTRUE);
   else
     TestResult.push_back(kFALSE);
   
   TestName.push_back("SIS clock diff < 10");
   if (report->GetSISdiff()<10)
     TestResult.push_back(kTRUE);
   else
     TestResult.push_back(kFALSE);
     
   TestName.push_back("Nside Quite Strips < 20");
   TString StripString=report->GetStripFile();
   Ssiz_t StripStringPoint=StripString.Index("offline.root");
   if (StripStringPoint<0) StripStringPoint=StripString.Index(".root");
   Int_t StripNo=atoi(StripString(StripStringPoint-5,5).Data()); //Beware! I assume run numbers are 5 digits long...
   TalphaStripsReport* strips=TRootUtils::GetStripReport(StripNo);
   if (strips->GetnSideQuietCount()<20)
     TestResult.push_back(kTRUE);
   else
     TestResult.push_back(kFALSE);
   
   TestName.push_back("Pside Quite Strips < 20");
   if (strips->GetpSideQuietCount()<20)
     TestResult.push_back(kTRUE);
   else
     TestResult.push_back(kFALSE);
   
   
   TestName.push_back("Nside Noisy Strips < 20");
   if (strips->GetnSideNoisyCount()<20)
     TestResult.push_back(kTRUE);
   else
     TestResult.push_back(kFALSE);
   
   
   TestName.push_back("Pside Noisy Strips < 20");
   if (strips->GetpSideNoisyCount()<20)
     TestResult.push_back(kTRUE);
   else
     TestResult.push_back(kFALSE);
     
   cout <<"Result\tRunNo\tTest"<<endl;
   Bool_t QODPass=kTRUE;
   for (Int_t i=0; i<TestResult.size(); i++)
   {
     if (TestResult[i])
     {
       std::cout<<"pass\t";
     }
     else
     {
       QODPass=kFALSE;
       std::cout<<"fail\t";
     }
     std::cout <<RunNo<<"\t"<<TestName[i].Data()<<endl;
   }
   
   return QODPass;
}

Bool_t CosmicAutoQOD(Int_t RunNo, Double_t tmin, Double_t tmax) //Dump repetition counts from 1
{
   if (TRootUtils::GetTotalRunTime(RunNo)<tmax) tmax=TRootUtils::GetTotalRunTime(RunNo);
   cout <<"Running AutoQOD (Cosmic "<<tmin<<" - "<< tmax<<"s)"<<endl;
   if (!gAutoQOD) return kTRUE;
   std::vector<Bool_t> TestResult;
   std::vector<TString> TestName;
   Int_t TRIG=gSISch->GetChannel("IO32_TRIG_NOBUSY"); 
   Int_t READOUT=gSISch->GetChannel("IO32_TRIG"); 
   
   
   Double_t Triggers=(Double_t)TRootUtils::Count_SIS_Triggers(RunNo,TRIG,tmin,tmax);
   Double_t TriggersError=TMath::Sqrt((Double_t) Triggers);
   TestName.push_back("Triggers>9");
   if ((Triggers+TriggersError)/(tmax-tmin) > 9.)
     TestResult.push_back(kTRUE);
   else
     TestResult.push_back(kFALSE);
   
   TestName.push_back("Triggers<10");
   if ((Triggers-TriggersError)/(tmax-tmin) < 10.)
     TestResult.push_back(kTRUE);
   else
     TestResult.push_back(kFALSE);
   
   
   //TestName.push_back("TotalTime>1000s");
   //if (tmax-tmin> 1000.)
   //  TestResult.push_back(kTRUE);
   //else
   //  TestResult.push_back(kFALSE);
   
   Double_t Readouts=(Double_t)TRootUtils::Count_SIS_Triggers(RunNo,READOUT,tmin,tmax);
   Double_t ReadoutsError=TMath::Sqrt(Readouts);
   TestName.push_back("Readouts>9");
   if ((Readouts+ReadoutsError)/(tmax-tmin) > 9. )
     TestResult.push_back(kTRUE);
   else
     TestResult.push_back(kFALSE);
   
   TestName.push_back("Readouts<10");
   if ((Readouts-ReadoutsError)/(tmax-tmin) < 10. )
     TestResult.push_back(kTRUE);
   else
     TestResult.push_back(kFALSE);
   

   
   TestName.push_back("Start Dumps In ATM<5");
   if (TRootUtils::Count_SIS_Triggers(RunNo,gSISch->GetChannel("SIS_ATOM_DUMP_START"),tmin,tmax)< 5 )
     TestResult.push_back(kTRUE);
   else
     TestResult.push_back(kFALSE);

   TestName.push_back("Stop Dumps In ATM<5");
   if (TRootUtils::Count_SIS_Triggers(RunNo,gSISch->GetChannel("SIS_ATOM_DUMP_STOP"),tmin,tmax)< 5 )
     TestResult.push_back(kTRUE);
   else
     TestResult.push_back(kFALSE);
   
   
   TestName.push_back("Start Dumps In RCT<5");
   if (TRootUtils::Count_SIS_Triggers(RunNo,gSISch->GetChannel("SIS_RECATCH_DUMP_START"),tmin,tmax)< 5 )
     TestResult.push_back(kTRUE);
   else
     TestResult.push_back(kFALSE);

   TestName.push_back("Stop Dumps In RCT<5");
   if (TRootUtils::Count_SIS_Triggers(RunNo,gSISch->GetChannel("SIS_RECATCH_DUMP_STOP"),tmin,tmax)< 5 )
     TestResult.push_back(kTRUE);
   else
     TestResult.push_back(kFALSE);
   
   
   TTree* sil_tree = Get_Vtx_Tree( RunNo );
   TSiliconEvent* sil_event = new TSiliconEvent();
   sil_tree->SetBranchAddress("SiliconEvent", &sil_event);
   sil_tree->SetBranchStatus("SiliconModules",0);
   Int_t VF48Events=0;
   Int_t Verts=0;
   Int_t PassedCuts=0;
   for( Int_t i=0; i<sil_tree->GetEntries(); i++ )
   {
     sil_event->ClearEvent();
     sil_tree->GetEntry(i);
     if( sil_event->GetRunTime() <= tmin ) continue;
     if( sil_event->GetRunTime() > tmax ) break;
     VF48Events++;
     if(sil_event->GetNVertices()>0)
     {
       Verts++;
       if (TRootUtils::ApplyCuts( sil_event))
         PassedCuts++;
     }
   }
   delete sil_event;
   delete sil_tree;
   
   Double_t VF48EventError=TMath::Sqrt(VF48Events);
      TestName.push_back("VF48Events<10");
   if (((Double_t)VF48Events-VF48EventError)/(tmax-tmin) < 10. )
     TestResult.push_back(kTRUE);
   else
     TestResult.push_back(kFALSE);

   Double_t VertsError=TMath::Sqrt(Verts);
   
   TestName.push_back("Vertex Rate>5");
   if (((Double_t)Verts+VertsError)/(tmax-tmin)> 5 )
     TestResult.push_back(kTRUE);
   else
     TestResult.push_back(kFALSE);
   
   Double_t PassedCutsError=TMath::Sqrt(PassedCuts);
   TestName.push_back("Passed Cut Rate>0.04 ");
   if (((Double_t)PassedCuts+PassedCutsError)/(tmax-tmin)> 0.04 )
     TestResult.push_back(kTRUE);
   else
     TestResult.push_back(kFALSE);
   
   
   TestName.push_back("Passed Cut Rate<0.07");
   if (((Double_t)PassedCuts-PassedCutsError)/(tmax-tmin)< 0.07 )
     TestResult.push_back(kTRUE);
   else
     TestResult.push_back(kFALSE);
   
   TAnalysisReport* report=TRootUtils::GetAnalysisReport(RunNo);
   
   TestName.push_back("alphaAnalysis reached last mid file");
   if (report->GetStopTimeBinary()>100000000) 
     TestResult.push_back(kTRUE);
   else
     TestResult.push_back(kFALSE);
   
   TestName.push_back("SIS clock diff < 10");
   if (report->GetSISdiff()<10)
     TestResult.push_back(kTRUE);
   else
     TestResult.push_back(kFALSE);
     
   TestName.push_back("Nside Quite Strips < 20");
   TString StripString=report->GetStripFile();
   Ssiz_t StripStringPoint=StripString.Index("offline.root");
   if (StripStringPoint<0) StripStringPoint=StripString.Index(".root");
   Int_t StripNo=atoi(StripString(StripStringPoint-5,5).Data()); //Beware! I assume run numbers are 5 digits long...
   TalphaStripsReport* strips=TRootUtils::GetStripReport(StripNo);
   if (strips->GetnSideQuietCount()<20)
     TestResult.push_back(kTRUE);
   else
     TestResult.push_back(kFALSE);
   
   TestName.push_back("Pside Quite Strips < 20");
   if (strips->GetpSideQuietCount()<20)
     TestResult.push_back(kTRUE);
   else
     TestResult.push_back(kFALSE);
   
   
   TestName.push_back("Nside Noisy Strips < 20");
   if (strips->GetnSideNoisyCount()<20)
     TestResult.push_back(kTRUE);
   else
     TestResult.push_back(kFALSE);
   
   
   TestName.push_back("Pside Noisy Strips < 20");
   if (strips->GetpSideNoisyCount()<20)
     TestResult.push_back(kTRUE);
   else
     TestResult.push_back(kFALSE);
     
   cout <<"Result\tRunNo\tTest"<<endl;
   Bool_t QODPass=kTRUE;
   for (Int_t i=0; i<TestResult.size(); i++)
   {
     if (TestResult[i])
     {
       std::cout<<"pass\t";
     }
     else
     {
       QODPass=kFALSE;
       std::cout<<"fail\t";
     }
     std::cout <<RunNo<<"\t"<<TestName[i].Data()<<endl;
   }
   
   return QODPass;
}
//void RunAutoQOD(char* f_name = "data/tree42432offline.root")
//void COSMICAutoQOD(char* f_name = "data/tree42432offline.root")
/*{
  if(phaseid==MIX3 || phaseid==MIX2 || phaseid==MIX)
    MIXAutoQOD(f_name);
  else if (phase==COSMICS)
    COSMICAutoQOD(f_name);
  else
  {
    std::cout <<"BAD QOD TYPE... exiting"<<std::endl;
    exit(99);
  }
}*/


void dumper(char* f_name = "data/tree42432offline.root", RunPhase phaseid = COSMICS, Int_t NTrack=-1)
{
  std::cout << "processing file "<< f_name << " for runphase=" << phaseid << std::endl;
  if(0)  exit(123); // test

  TFile* f_curr = TFile::Open(f_name);
  // std::cout << f_curr <<"\t"<<f_curr->GetName() <<std::endl;
  
  // parse input file name and prepare output file name / get run number
  TString treename="selected";
 
  TString filename=getenv("MVATREES");
  filename+="/";
  if (NTrack>0)
    filename+=NTrack;
  else
    filename+="all";
  filename+="tracks_";
  //TString filename="seldata/";
  // std::cout<<treename<<std::endl;
  TString treename2="selected";
  // For lxplus
  //  TString filename = getenv("TMPDIR");
  // std::cout<<filename<<"\t"<<treename<<std::endl;

  TString basename(f_curr->GetName());
  basename.Remove(0,basename.Last('/')+1); // strip path name

  TString runname(basename.Data(),9); // tree#####
  runname.Remove(0,4); // remove "tree"

  Int_t runnumber = atoi(runname.Data());
  TAnalysisReport* Report=GetAnalysisReport(runnumber);
  	  TString StripString=Report->GetStripFile();
  	  delete Report;
	  Ssiz_t StripStringPoint=StripString.Index("offline.root");
	  if (StripStringPoint<0) StripStringPoint=StripString.Index(".root");
	  Int_t StripNo=atoi(StripString(StripStringPoint-5,5).Data()); //Beware! I assume run numbers are 5 digits long...
  if (StripNo!=runnumber)
  {
    cout <<"Strip file ("<<StripNo<<") from differnt run to alphaAnalysis ("<<runnumber<<")! Not good for high quality MVA" <<endl;
    if (!AllowOnlineAnalysis) exit(111);
  }
  else
  {
    cout <<"alphaAnalysis used same run for pedestals (alphaStrips) - good" <<endl;
  }
  // // std::cout<<runnumber<<std::endl;
  gSISch = new TSISChannels(runnumber);
  char dbName[255]; 
  sprintf(dbName,"%s/aux/main.db",getenv("RELEASE"));

  TSettings* gSettingsDB = new TSettings(dbName,runnumber);

  std::vector<Double_t>  sweep_phase_start;
  std::vector<Double_t>  sweep_phase_stop;
  std::vector<TString>   sweep_phase_dumpname;
  std::vector<Int_t>   sweep_phase_dumpnum;
  std::vector<Double_t> QFreq;

  std::vector<TString> DumpNames;
  std::vector<Double_t> PbarTempStarts,junk,PbarTempStops, PbarVetoStart, PbarVetoStop;

  mix_phase_start_ts=0.;
  mix_phase_stop_ts=0.;

  wait_phase_start_ts=0.;
  wait_phase_stop_ts=0.;

  quench_phase_start_ts=0.;
  quench_phase_stop_ts=0.;

  hold_phase_start_ts=0.;
  hold_phase_stop_ts=999999.;

  stoch_phase_start_ts=0.;
  stoch_phase_stop_ts=0.;

  bkg_phase_start_ts=0.;
  bkg_phase_stop_ts=0.;

  //No 2015 shutter signals
  // TTree* shutter_open_tree;
  // TTree* shutter_close_tree;
  TTree* Qpulse_tree;
  Int_t NQPulses=-1;
  Int_t iQPulse=-1;
  Int_t NShutterGates =0;
  Double_t tfromQ =-99.;
  Double_t timeOfLaserStart;
  Double_t timeOfLaserStop;
  TString LaserDump_Name("");
  Int_t LaserDump_Num=0;
  Int_t nrep(0);
  //  strcat(ofname,"seldata/");
  Double_t readstart=0.0, readstop=0.0;
  //  Phase-dependent initializations
  switch (phaseid)
    {
    case COSMICS:
      filename+="cosm_";
      treename+="_cosm";
      break;
    
    case MIX:
      filename+="mix_";
      treename+="_mix";
//      GetDumpTime(f_curr,(char*)"Mixing",mix_phase_start_ts,mix_phase_stop_ts);
      FindMixingGate(f_curr,mix_phase_start_ts,mix_phase_stop_ts);
      std::cout << "Mix Phase Start: " << mix_phase_start_ts <<  " s  ; ";
      std::cout << "Mix Phase Stop: " << mix_phase_stop_ts << " s  ; " ;
      std::cout << "Mix Phase Time: " << mix_phase_stop_ts - mix_phase_start_ts << " s  " << std::endl;
      readstart = mix_phase_start_ts;
      readstop = mix_phase_stop_ts;
      break;
    case MIX2:
      filename+="mix2_";
      treename+="_mix2";
//      GetDumpTime(f_curr,(char*)"Mixing",mix_phase_start_ts,mix_phase_stop_ts);
      FindMixingGate(f_curr,mix_phase_start_ts,mix_phase_stop_ts);
      std::cout << "Mix Phase Start: " << mix_phase_start_ts <<  " s  ; ";
      std::cout << "Mix Phase Stop: " << mix_phase_stop_ts << " s  ; " ;
      std::cout << "Mix Phase Time: " << mix_phase_stop_ts - mix_phase_start_ts << " s  " << std::endl;

      readstart = mix_phase_start_ts;
      readstop = mix_phase_stop_ts;
      break;
    case MIX3:
      filename+="mix3_";
      filename+=gMix3_rep;
      filename+="_";
      treename+="_mix3";
      readstart = 0.;
      readstop = 999999999.;
      break;
    case WAIT:
      filename+="wait_";
      treename+="_wait";
      //      Int_t repetition=1; // Get the 2nd (atm sequencer) hold dump
      GetDumpTime(f_curr,(char*)"Hold Dump",wait_phase_start_ts,wait_phase_stop_ts, 2);
      readstart = wait_phase_start_ts;
      readstop = wait_phase_stop_ts;
      // std::cout << "Hold Phase Start: " << wait_phase_start_ts <<  " s  ; ";
      // std::cout << "Hold Phase Stop: " << wait_phase_stop_ts << " s  ; " ;
      std::cout << "Hold  Time: " << wait_phase_stop_ts - wait_phase_start_ts << " s  " << std::endl;
      break;
    case QUENCH:
      filename+="quench_";
      treename+="_quench";
      //      GetDumpTime(f_curr,(char*)"Quench",quench_phase_start_ts,quench_phase_stop_ts);
      //      if(quench_phase_start_ts<1)  GetDumpTime(f_curr,(char*)"FastRampDown",quench_phase_start_ts,quench_phase_stop_ts);
      //Use QUENCH_FLAG sis channel
      FindQuenchGate(f_curr,quench_phase_start_ts,quench_phase_stop_ts);
      std::cout << "Quench/FRD Phase Start: " << quench_phase_start_ts <<  " s  ; ";
      std::cout << "Quench/FRD Phase Stop: " << quench_phase_stop_ts << " s  ; " ;
      std::cout << "Quench/FRD Phase Time: " << quench_phase_stop_ts - quench_phase_start_ts << " s  " << std::endl;
      readstart = quench_phase_start_ts;
      readstop = quench_phase_stop_ts;
      break;
    case BKG:
      GetDumpTime(f_curr,(char*)"Background Si2",bkg_phase_start_ts,bkg_phase_stop_ts);
      std::cout << "Bkg Phase Start: " << bkg_phase_start_ts <<  " s  ; ";
      std::cout << "Bkg Phase Stop: " << bkg_phase_stop_ts << " s  ; " ;
      std::cout << "Bkg Phase Time: " << bkg_phase_stop_ts - bkg_phase_start_ts << " s  " << std::endl;
      filename+="bkg_";
      treename+="_bkg";
      readstart = bkg_phase_start_ts;
      readstop = bkg_phase_stop_ts;
      break;
    case ALL:
      filename+="all_";
      treename+="_all";
      break;
    case muW:
      filename+="muW_";
      treename+="_muW";
      nrep=12;  //number of microwave dumps modify as needed
      GetDumpTime(f_curr,(char*)"Microwave Sweep",sweep_phase_start,sweep_phase_stop, sweep_phase_dumpname,nrep); 
      if (sweep_phase_start.size() >0){
        std::cout <<  "muW dumps found: " << sweep_phase_start.front() << " to  " << sweep_phase_stop.back() << std::endl;
        readstart=sweep_phase_start.front();
        readstop=sweep_phase_stop.back();
      }
      break;
    case STOCH:
      filename+="stoch_";
      treename+="_stoch";

      GetDumpTime(f_curr,(char*)"Stochastic Dump",stoch_phase_start_ts,stoch_phase_stop_ts);
      std::cout << "Stochastic Heating Start: " << stoch_phase_start_ts <<  " s  ; ";
      std::cout << "Stochastic Heating Stop: " << stoch_phase_stop_ts << " s  ; " ;
      std::cout << "Stochastic Heating Time: " << stoch_phase_stop_ts - stoch_phase_start_ts << " s  " << std::endl;
      readstart = stoch_phase_start_ts;
      readstop = stoch_phase_stop_ts;
      break;
    case UV:
      filename+="laser_";
      treename+="_laser";
      std::vector<TString> laserdumpname;
      //Get these names from the database
      //      TSettings* gSettingsDB;	
      std::cout <<"laserdumps: ";
      for (Int_t dumpnum=0;dumpnum<10;dumpnum++){
        TString temp("");
        temp=gSettingsDB->GetDumpName(runnumber,dumpnum);
        if (!temp.CompareTo("")) break;
        laserdumpname.push_back(temp);
        std::cout<<"  "<<dumpnum<<") "<<laserdumpname.back();
      }
      delete gSettingsDB;
      Int_t NDumpNames =laserdumpname.size();
      std::cout<<" NDumpNames:"<<NDumpNames<<"\n";
      GetDumpTime(f_curr,laserdumpname,sweep_phase_start,sweep_phase_stop, sweep_phase_dumpname);      
      NShutterGates=sweep_phase_start.size();
      std::cout<<"Gates "<<NShutterGates<<": "<<sweep_phase_dumpname.front()<<" "<< sweep_phase_start.front()<<" " << sweep_phase_start.back()<<" "<<sweep_phase_dumpname.back()<<" "<< sweep_phase_start.back()<<" "<<sweep_phase_stop.back()<<"\n";
      //Now sort the phases in time order
      Double_t tmptime;
      TString tmpname;
      bool swapped = kTRUE;
      while (swapped == kTRUE) {
        swapped=kFALSE;
        for (Int_t ishutter=1;ishutter<NShutterGates;ishutter++){
          if(sweep_phase_start[ishutter]<sweep_phase_start[ishutter-1]){
            tmptime=sweep_phase_start[ishutter];
            sweep_phase_start[ishutter]=sweep_phase_start[ishutter-1];
            sweep_phase_start[ishutter-1]=tmptime;
            tmptime=sweep_phase_stop[ishutter];
            sweep_phase_stop[ishutter]=sweep_phase_stop[ishutter-1];
            sweep_phase_stop[ishutter-1]=tmptime;
            tmpname=sweep_phase_dumpname[ishutter];
            sweep_phase_dumpname[ishutter]=sweep_phase_dumpname[ishutter-1];
            sweep_phase_dumpname[ishutter-1]=tmpname;
          }
        }
      }
      //Add the dump number to the sweep_phase;
      for (Int_t i=0;i<NShutterGates;i++)
      {
        for( Int_t dumpnum=0;dumpnum<NDumpNames;dumpnum++)
        {
          if (!sweep_phase_dumpname[i].CompareTo(laserdumpname[dumpnum]))
          {
            sweep_phase_dumpnum.push_back(dumpnum);
            //std::cout<< i <<"\t"<<sweep_phase_dumpname[i]<<"\t"<<dumpnum<<"\t"<<laserdumpname[dumpnum]<<std::endl;
          }
        }
        //std::cout<< i <<"\t"<<sweep_phase_dumpnum[i]<<std::endl;
      }
    
      timeOfLaserStart=sweep_phase_start.front();
      timeOfLaserStop=sweep_phase_stop.back();
      readstart =timeOfLaserStart;
      readstop = timeOfLaserStop;
      wait_phase_start_ts=timeOfLaserStart;
      wait_phase_stop_ts=timeOfLaserStop;
      std::cout<< "timeOfLaserStart "<< timeOfLaserStart<<" timeOfLaserStop  "<< timeOfLaserStop<<std::endl;
      //Get  Qpulse_tree NQPulses, QFreq
      // Assumes sweeps with equal numbers of laser pulses /ion ejections.
      //It will succeed unless there is a gap in pulses between the sweeps and the number of pulses/sweep are unequal.
      Int_t QPulse_channel=gSISch->GetChannel("SEQLASERPULSE"); 
      Qpulse_tree = GetSISTree(f_curr,QPulse_channel);
      NQPulses =  Qpulse_tree->GetEntries()/NShutterGates;
      for (Int_t i=0;i<NShutterGates;i++){
        QFreq.push_back ((NQPulses-1)/(GetRunTimeOfEntry(Qpulse_tree,NQPulses*(i+1)-1)-GetRunTimeOfEntry(Qpulse_tree,NQPulses*i)));
        std::cout <<laserdumpname[i]<<" Q pulser rate: "<<QFreq[i]<<" NQPulses "<<NQPulses<<" first time "<<GetRunTimeOfEntry(Qpulse_tree,NQPulses*i)<<std::endl;
      }
      iQPulse=0;
      break;   
    }//switch
    
    
    
  
  if (phaseid==COSMICS)  
  {
    TAnalysisReport* report=GetAnalysisReport(f_curr);
    Int_t RunNo=report->GetRunNumber();
    delete report;
    if (!CosmicAutoQOD(RunNo,gTMinCut,gTMaxCut)) exit(22); //Cosmic QOD check failed
  }
  
  filename.Append(basename.Data(),strlen(basename.Data())-5);
  if (gUseTimeRange)
  {
    filename+="_Time";
    filename+=gTMinCut;
    filename+="-";
    filename+=gTMaxCut;
  }
  filename+=".root";
  // std::cout << filename << std::endl;
  TFile* f_fitData = (TFile*) gROOT->FindObject(filename.Data());
  if (f_fitData) f_fitData->Close();
  gROOT->cd();
  f_fitData = new TFile(filename.Data(),"RECREATE");

  nt_fitData=new TTree(treename.Data(),treename.Data());
  // old code for microware sweeps. Should be recoded.
  // std::cout << nt_fitData->GetName() <<" in "<< f_fitData->GetName() << std::endl;

 
  // Double_t startsweep=0.;
  // Double_t stopsweep=0.;

  // for (int i=0; i<20; i++){
  //   int found = GetPhaseStartStop(sequencerTree,seqEvent,SISTrees,SISEvent,
  //                                 TString("\"Microwave Sweep"),&startsweep,&stopsweep,i);

  //   if (found < 0)
  //     break;

  //   sweep_phase_start.push_back(startsweep);
  //   sweep_phase_stop.push_back(stopsweep);

  //   cout << i << " " << found << " " << sweep_phase_start.at(i) << " " << sweep_phase_stop.at(i) << std::endl;

  // }
  //
  // GetPhaseStartStop(sequencerTree,seqEvent,SISTrees,SISEvent,
  //                   TString("\"Hold Before"),&hold_phase_start_ts,&hold_phase_stop_ts,i);
  // cout << "Hold: " << hold_phase_start_ts << " " << hold_phase_stop_ts << std::endl;
  
  //Calculate clock correction
  Int_t vf48ch =gSISch->GetChannel("SIS_VF48_CLOCK");
  if(vf48ch>1) 
    gClockDiff=GetClockCalib(f_curr);//chan defined for this run
#if FIXTIMING
  std::cout<<"gSlope = 1 - "<<gClockDiff<<std::endl;
  gSlope = 1.- gClockDiff;
#else
  gSlope=1.;
  gClockDiff=0.;
#endif


  std::vector<double> adtimestamps; // timestamps for AD injections/extractions

  int SIS_AD_IN=gSISch->GetChannel((char*)"SIS_DIX_ALPHA"),
    SIS_AD_EJ=gSISch->GetChannel((char*)"SIS_AD");

  GetRunTimes(GetSISTree(f_curr,SIS_AD_IN), &adtimestamps); // injections
  GetRunTimes(GetSISTree(f_curr,SIS_AD_EJ), &adtimestamps); // extractions

  std::sort(adtimestamps.begin(), adtimestamps.end()); // sort them
  
  if ( phaseid == MIX3 )
  {
    TAnalysisReport* report=GetAnalysisReport(f_curr);
    Int_t RunNo=report->GetRunNumber();
    delete report;
    if (!MIXAutoQOD(RunNo,gMix3_rep) ) exit(21); // fails... continue;
  }
  
  TTree* vertexTree = GetSiliconEvents(f_curr);
  TSiliconEvent* siliconEvent = new TSiliconEvent();
  Int_t eventID;
  vertexTree->SetBranchAddress("SiliconEvent", &siliconEvent );
  vertexTree->SetBranchStatus("SiliconModules",0);
  vertexTree->SetBranchAddress("eventID", &eventID );
  if( vertexTree == NULL )
    {
      std::cout << "vertexTree == NULL !!!!!" <<std::endl;
      return;
    }
  vertexTree->GetEntry(0);


  gInterc = siliconEvent->GetRunTime() - gSlope * siliconEvent->GetVF48Timestamp();
  std::cout << "Intercept:" << gInterc << std::endl;
  // read alphaevent tree
  TTree* alphaeventTree = GetAlphaEvents(f_curr);
  TAlphaEvent* alphaEvent = 0;
  Int_t alphaeventID;
  Double_t SiRunTime;
  alphaeventTree->SetBranchAddress("AlphaEvent", &alphaEvent );
  alphaeventTree->SetBranchAddress("eventID", &alphaeventID );
  //Reading the fprojp branch of alphaevent causes a segfault Aug 2016
  //Still there in April 2017 perhaps related to empty fprojp?
  //  alphaeventTree->SetDebug(3);
  alphaeventTree->SetBranchStatus("fprojp",0);


  // if cosmics, veto neighbours of ad events timestamps
  // read tree sequencially, and assume runtimes are properly ordered (!)
  //Histograms needed for MIX2
  Int_t nBins=100;
  TH1D* TrigHist;//=new TH1D("Triggers","Triggers",nBins,mix_phase_start_ts,mix_phase_stop_ts);
  TH1D* ReadHist;//=new TH1D("Reads","Reads",nBins,mix_phase_start_ts,mix_phase_stop_ts);
  if ( phaseid == COSMICS )
  {

    double ad_eps = 0.5;
    Int_t ad_ii_start=0; //AO to shorten this index loop
    Int_t aeentry=0;
    for(Int_t entry=0; entry<vertexTree->GetEntries(); ++entry)
    {
      siliconEvent->ClearEvent();
      if(entry%5000==0) std::cout<<entry<<std::endl;
      char aeselection[128];
      vertexTree->GetEntry(entry);
      //Skip all check when using a time range
      if (siliconEvent->GetRunTime()<gTMinCut) continue;
      if (siliconEvent->GetRunTime()>gTMaxCut) break;

      bool ad_veto = kFALSE;
      for(int ad_ii=ad_ii_start; ad_ii<adtimestamps.size(); ++ad_ii)
      {
        if ( fabs( siliconEvent->GetRunTime() - adtimestamps.at(ad_ii)) < ad_eps )
        {
          ad_veto=kTRUE;
          ad_ii_start=ad_ii;
          break;
        }
      }
      if(ad_veto) continue;
      //      cout << eventID << " : " << eventList->GetN() << std::endl;
      alphaEvent->DeleteEvent();
      alphaeventTree->GetEntry(aeentry);
      while (alphaeventID < eventID) 
      {
        std::cout<<"alpha and si eventID mismatch1 "<<alphaeventID<<"\t"<<eventID<<std::endl;
        alphaEvent->DeleteEvent();
        alphaeventTree->GetEntry(++aeentry);
      }
      if (alphaeventID == eventID)
      {
        SiRunTime = siliconEvent->GetRunTime();
        //	  std::cout<<"Siruntime correction: "<<SiRunTime;
        SiRunTime-=gClockDiff*SiRunTime;
        siliconEvent->SetRunTime(SiRunTime);
        //std::cout<<" to "<<SiRunTime;
        if (NTrack!=siliconEvent->GetNTracks() && NTrack>0) continue;
        if (siliconEvent->GetNHits()!=alphaEvent->GatherHits()->GetEntries()) std::cout<<"ID "<<eventID<<" NHitscosm:Si "<< siliconEvent->GetNHits()<<" alpha "<< alphaEvent->GatherHits()->GetEntries()<<std::endl; 
        SaveEventVariables(siliconEvent, alphaEvent, runnumber, phaseid, eventID,-1);
        //std::cout<<"alphaeventID="<<alphaeventID<<" aeentry="<<aeentry<<std::endl;
        aeentry++;
      }
      else
      {
        aeentry--;
        std::cout<<"alpha and si eventID mismatch "<<alphaeventID<<"\t"<<eventID<<std::endl;
        continue;
      }
    }// end cosmics entry loop
    //cout<<eventID<<std::endl;
  } // phaseid cosmics
  else
  {
    if (phaseid == MIX2 )
    {
      Int_t TRIG=gSISch->GetChannel("IO32_TRIG_NOBUSY"); 
      Int_t READOUT=gSISch->GetChannel("IO32_TRIG"); 
      TTree* Trig_tree = GetSISTree(f_curr,TRIG);
      TSisEvent* Trig_event = new TSisEvent();
      Trig_tree->SetBranchAddress("SisEvent", &Trig_event );
      TTree* Read_tree = GetSISTree(f_curr,READOUT);
      TSisEvent* Read_event = new TSisEvent();
      Read_tree->SetBranchAddress("SisEvent", &Read_event );

      Int_t NTrigs =  Trig_tree->GetEntries();
      Int_t NReads =  Read_tree->GetEntries();

      TrigHist=new TH1D("Triggers","Triggers",nBins,mix_phase_start_ts,mix_phase_stop_ts);
      ReadHist=new TH1D("Reads","Reads",nBins,mix_phase_start_ts,mix_phase_stop_ts);

      TAnalysisReport* report=GetAnalysisReport(f_curr);
      Int_t RunNo=report->GetRunNumber();
      delete report;
      if (!MIXAutoQOD(RunNo)) exit(21); //If QOD fails... continue;

      Double_t run_time;
      for (Int_t i=0;i<NTrigs;i++)
      {     
        Trig_tree->GetEntry(i);
        run_time = Trig_event->GetRunTime();
        if(run_time<=mix_phase_start_ts) continue;
        if(run_time>mix_phase_stop_ts) break;
        TrigHist->Fill(run_time,Trig_event->GetCountsInChannel());
      }
      for (Int_t i=0;i<NReads;i++)
      {      
        Read_tree->GetEntry(i);
        run_time = Read_event->GetRunTime();
        if(run_time<=mix_phase_start_ts) continue;
        if(run_time>mix_phase_stop_ts) break;
        ReadHist->Fill(run_time,Read_event->GetCountsInChannel());
      }
      bool StartFound=false; bool StopFound=false;
      Double_t tstep=(mix_phase_stop_ts-mix_phase_start_ts)/(double)nBins;
      for (Int_t i=1; i<nBins; i++) //skip underflow
      {
        Double_t trigs,reads;
        //cout << i <<":"<< (double)i*tstep<<"\t" << TrigHist->GetBinContent(i)<<"\t"<<ReadHist->GetBinContent(i) << std::endl;
        trigs=TrigHist->GetBinContent(i); reads=ReadHist->GetBinContent(i);
      } 
    }//MIX2
    if (phaseid==MIX3)
    {
      Int_t TRIG=gSISch->GetChannel("IO32_TRIG_NOBUSY"); 
      Int_t READOUT=gSISch->GetChannel("IO32_TRIG"); 

      Int_t MIXCHAN=gSISch->GetChannel("MIXING_FLAG");
      TTree* Mix_trig_tree = GetSISTree(f_curr,MIXCHAN);
      Int_t NMixings=Mix_trig_tree->GetEntries();
      if (gMix3_rep<1)
      {
        std::cout <<"Mixing repetition must be equal or greater than 1"<<std::endl;
      }
      if (gMix3_rep>NMixings)
      {
        std::cout<<"Mixing "<<gMix3_rep<<" does not exist... fail"<<std::endl;
        exit(55);
      }
      delete Mix_trig_tree;
      TAnalysisReport* report=GetAnalysisReport(f_curr);
      Int_t RunNo=report->GetRunNumber();
      delete report;
      //for (Int_t MixingNo=1; MixingNo<NMixings+1; MixingNo=MixingNo+2) // Use every other mixing (save others for future QOD)
      //{
        mix_phase_start_ts=TRootUtils::MatchEventToTime(RunNo,"startDump","Mixing",gMix3_rep);
        mix_phase_stop_ts=TRootUtils::MatchEventToTime(RunNo,"stopDump","Mixing",gMix3_rep);
        readstart = mix_phase_start_ts;
      readstop = mix_phase_stop_ts;
        
        TTree* Trig_tree = GetSISTree(f_curr,TRIG);
        TSisEvent* Trig_event = new TSisEvent();
      Trig_tree->SetBranchAddress("SisEvent", &Trig_event );
      TTree* Read_tree = GetSISTree(f_curr,READOUT);
      TSisEvent* Read_event = new TSisEvent();
      Read_tree->SetBranchAddress("SisEvent", &Read_event );

      Int_t NTrigs =  Trig_tree->GetEntries();
      Int_t NReads =  Read_tree->GetEntries();

      TrigHist=new TH1D("Triggers","Triggers",nBins,mix_phase_start_ts,mix_phase_stop_ts);
      ReadHist=new TH1D("Reads","Reads",nBins,mix_phase_start_ts,mix_phase_stop_ts);
      Double_t run_time;
      for (Int_t i=0;i<NTrigs;i++)
      {     
        Trig_tree->GetEntry(i);
        run_time = Trig_event->GetRunTime();
        if(run_time<=mix_phase_start_ts) continue;
        if(run_time>mix_phase_stop_ts) break;
        TrigHist->Fill(run_time,Trig_event->GetCountsInChannel());
      }
      for (Int_t i=0;i<NReads;i++)
      {
          Read_tree->GetEntry(i);
          run_time = Read_event->GetRunTime();
          if(run_time<=mix_phase_start_ts) continue;
          if(run_time>mix_phase_stop_ts) break;
          ReadHist->Fill(run_time,Read_event->GetCountsInChannel());
        }
        bool StartFound=false; bool StopFound=false;
        Double_t tstep=(mix_phase_stop_ts-mix_phase_start_ts)/(double)nBins;
        for (Int_t i=1; i<nBins; i++) //skip underflow
        {
          Double_t trigs,reads;
          //cout << i <<":"<< (double)i*tstep<<"\t" << TrigHist->GetBinContent(i)<<"\t"<<ReadHist->GetBinContent(i) << std::endl;
          trigs=TrigHist->GetBinContent(i); reads=ReadHist->GetBinContent(i);
        } 
      
    }//MIX3
    char selection[128];
    char aeselection[128];
    Int_t currentShutter=0;
    Double_t minQtime=-1;
    if (phaseid==UV) minQtime=1./QFreq[currentShutter];
    Double_t maxQtime=0.;
    Double_t temp;
    if (phaseid!=ALL) FindMixingGate(f_curr,mix_phase_start_ts,mix_phase_stop_ts);
    Int_t aeentry=0;
   for(Int_t i=0; i<vertexTree->GetEntries(); ++i)
   {  
      siliconEvent->ClearEvent();
      vertexTree->GetEntry(i);
      if (NTrack!=siliconEvent->GetNTracks() && NTrack>0) continue;
      if(i%2000==0) std::cout<<i<<"/"<<vertexTree->GetEntries()<<std::endl;

      SiRunTime = siliconEvent->GetRunTime();
      if ( phaseid==MIX2 || phaseid==MIX3 )
      { 
        TAxis *xaxis = TrigHist->GetXaxis();
        Int_t binx = xaxis->FindBin(SiRunTime);
        if (TrigHist->GetBinContent(binx)<2*ReadHist->GetBinContent(binx) || TrigHist->GetBinContent(binx)==0) continue;
        //else cout <<"OK bin:"<<TrigHist->GetBinContent(binx) <<">5*"<<ReadHist->GetBinContent(binx)<<"\t" << SiRunTime-mix_phase_start_ts<<"s"<<std::endl;
      }
      // std::cout<<"Siruntime correction: "<<SiRunTime;
      SiRunTime-=gClockDiff*SiRunTime;
      siliconEvent->SetRunTime(SiRunTime);
      //  	  std::cout<<" to "<<SiRunTime <<std::endl;
      sprintf(selection, "RunTime>=%lf && RunTime<=%lf", readstart,readstop);
      if( (SiRunTime < readstart || SiRunTime > readstop) && phaseid!=ALL )
        continue;
      //      cout << eventID << " : " << eventList->GetN() << std::endl;
      aeentry=i;
      alphaEvent->DeleteEvent();
      alphaeventTree->GetEntry(aeentry);
      //cout << alphaeventID << ": " << eventID << "  " << endl;
      while (alphaeventID < eventID) 
      {
        std::cout<<"alpha and si eventID mismatch "<<alphaeventID<<"\t"<<eventID<<std::endl;
        alphaEvent->DeleteEvent();
        alphaeventTree->GetEntry(++aeentry);
      }
      if (alphaeventID == eventID)
      {
        SiRunTime = siliconEvent->GetRunTime();
        //	  std::cout<<"Siruntime correction: "<<SiRunTime;
        SiRunTime-=gClockDiff*SiRunTime;
        siliconEvent->SetRunTime(SiRunTime);
        //std::cout<<" to "<<SiRunTime;
        TObjArray* hits=alphaEvent->GatherHits();
        Int_t AEnhits=hits->GetEntries();
        delete hits;
        if (siliconEvent->GetNHits()!=AEnhits) std::cout<<"ID "<<eventID<<" NHits7:Si "<< siliconEvent->GetNHits()<<" alpha "<< alphaEvent->GatherHits()->GetEntries()<<std::endl; 
        //std::cout<<"alphaeventID="<<alphaeventID<<" aeentry="<<aeentry<<std::endl;
        aeentry++;
      }
      else
      {
        aeentry--;
        std::cout<<"alpha and si eventID have no match1 "<<alphaeventID<<"\t"<<eventID<<std::endl;
        continue;
      }
      if ( phaseid == UV ) //Calculate the additional vars tfromQ, laserDumpName here
      //tfromQ calculated in interval [-.0005,1/QFreq -.0005]
      //Loop on shutter opens, test SiRunTime, find iQpulse
      //Get the shutter_open_tree, shutter_close_tree and NShutterGates, Qpulse_tree NQPulses, QFreq
      {
        if (siliconEvent->GetNHits()!=alphaEvent->GatherHits()->GetEntries()) std::cout<<"ID "<<eventID<<" NHits8:Si "<< siliconEvent->GetNHits()<<" alpha "<< alphaEvent->GatherHits()->GetEntries()<<std::endl; 
        for (Int_t iShutter=currentShutter;iShutter<NShutterGates;iShutter++)
        {
          //iQPulse = (SiRunTime-timeOfLaserStart)*QFreq[iShutter];
          tfromQ=SiRunTime-GetRunTimeOfEntry(Qpulse_tree,iQPulse);
          // std::cout<<"iQPulse1 "<<iQPulse<< " tfromQ " <<tfromQ<<std::endl;
          while (tfromQ<-0.005&&iQPulse>0) tfromQ=SiRunTime-GetRunTimeOfEntry(Qpulse_tree,--iQPulse);
          // std::cout<<"iQPulse2 "<<iQPulse<< " tfromQ " <<tfromQ<<std::endl;
          while(tfromQ>(1./QFreq[iShutter]-0.005)&&iQPulse<NQPulses*(iShutter+1)) tfromQ=SiRunTime -GetRunTimeOfEntry(Qpulse_tree,++iQPulse);
          temp=SiRunTime -GetRunTimeOfEntry(Qpulse_tree,iQPulse+1);
          // std::cout<<"iQPulse3 "<<iQPulse<< " tfromQ " <<tfromQ<<std::endl;
          tfromQ=(fabs(tfromQ)>fabs(temp)&&temp>-0.005)?temp:tfromQ;
          // std::cout<<"iQPulse4 "<<iQPulse<< " tfromQ " <<tfromQ<<std::endl;
          while (SiRunTime>sweep_phase_stop[iShutter])
          {
            // std::cout<<"Beyond last shutter "<<iShutter<<" tfromQ "<<tfromQ<<std::endl;
            iShutter++;
            minQtime=1./QFreq[iShutter];
            maxQtime=0.;
            if (iShutter>=NShutterGates)
            {
              sweep_phase_latest_ts =sweep_phase_start[NShutterGates-1];
              SaveEventVariables(siliconEvent, alphaEvent, runnumber, phaseid, eventID,-0.5, "");
              break;
            }
          }
          if (sweep_phase_start[iShutter] > SiRunTime)
          {
            // std::cout<<"Before shutter "<<iShutter<<" tfromQ "<<tfromQ<<"  timeOfLaserStart "<<timeOfLaserStart<<std::endl;
            sweep_phase_latest_ts =sweep_phase_start[0];
            SaveEventVariables(siliconEvent, alphaEvent, runnumber, phaseid, eventID,-0.5, "" );
            minQtime=1./QFreq[iShutter];
            maxQtime=0.;
            break;
          }
          else
          {
            // std::cout<<"Event in shutter  "<<iShutter<<"Dumpname "<<sweep_phase_dumpname[iShutter]<<" tfromQ "<<tfromQ<<std::endl;
            sweep_phase_latest_ts =sweep_phase_start[iShutter];
            SaveEventVariables(siliconEvent, alphaEvent, runnumber, phaseid, eventID, tfromQ, sweep_phase_dumpname[iShutter], sweep_phase_dumpnum[iShutter]);
            currentShutter=iShutter;
            if(iQPulse>0){
              minQtime=(tfromQ<minQtime)?tfromQ:minQtime;
              maxQtime=(tfromQ>maxQtime)?tfromQ:maxQtime;
            }
            break;
          }
        }//shutter loop
      // std::cout<<"minQtime "<<minQtime<<"\t maxQtime "<<maxQtime<<std::endl;
      }// phaseid = UV
      else 
      {
        TObjArray* hits=alphaEvent->GatherHits();
        Int_t AEnhits=hits->GetEntries();
        delete hits;
        if (siliconEvent->GetNHits()!=AEnhits) std::cout<<"ID "<<eventID<<" NHits7:Si "<< siliconEvent->GetNHits()<<" alpha "<< alphaEvent->GatherHits()->GetEntries()<<std::endl; 
        SaveEventVariables(siliconEvent, alphaEvent, runnumber, phaseid, eventID,-0.5, "" );
      }//phaseid       
    }//vertex loop
  }//Not cosmics
  delete siliconEvent;	    
  std::cout<<vertexTree->GetEntries()<<std::endl;
  delete gSISch;

  f_fitData->cd();
  //  nt_fitData->Write("",TObject::kOverwrite);
  nt_fitData->Write();
  f_fitData->Close();
  f_curr->cd();
  //  f_curr->Close();
  return;
}
void help()
{
	std::cout <<"Help printed here"<<std::endl;
	std::cout <<"Example arguments:"<<std::endl;
	std::cout <<"\t./trackDumper.exe ${TREEFILES}/tree50842offline.root COSMICS --ntracks=2 "<< std::endl;
	std::cout <<"--autoQOD"<<std::endl;
	
}
int main(int argc, char *argv[])
{

  // constructs an array for the commandline arguments
  std::vector<std::string> args;
  for (int i=0; i<argc; i++) {
    args.push_back(argv[i]);
    if (strcmp(argv[i],"-h")==0 || strcmp(argv[i],"--help")==0)
      help();
  }

  char filename[128];
  char phasechar[128];
  RunPhase phase;
  //Int_t NTrack=-1;
  Bool_t PHASE_SET=kFALSE;
  
  
  // parse command line options and sets the relevant global flags
  for (unsigned int i=1; i<args.size(); i++) // loop over the commandline options
  { 
    const char* arg = args[i].c_str();
    // printf("argv[%d] is %s\n",i,arg);
    if (!PHASE_SET) //Phase setting
    {
      if (strncmp(arg,"COSMICS",7)==0)
      {
        phase =COSMICS;
        PHASE_SET=kTRUE;
      }
      if (strncmp(arg,"MIX",3)==0)
      {
        phase =MIX;
        PHASE_SET=kTRUE;
      }
      if (strncmp(arg,"MIX2",4)==0)
      {
        phase =MIX2; //using Mixing when triggers > 2* readouts
        PHASE_SET=kTRUE;
      }
      if (strncmp(arg,"MIX3",4)==0)
      {
        phase =MIX3; //stacking
        gMix3_rep=std::atoi(args[i+1].c_str());
        i++;
        std::cout <<"Mixing repetition:"<< gMix3_rep<<std::endl;
        PHASE_SET=kTRUE;
      }
      if (strncmp(arg,"WAIT",4)==0)
      {
        phase =WAIT;
        PHASE_SET=kTRUE;
      }
      if (strncmp(arg,"QUENCH",6)==0)
      {
        phase =QUENCH;
        PHASE_SET=kTRUE;
      }
      if (strncmp(arg,"ALL",3)==0)
      {
        phase =ALL;
        PHASE_SET=kTRUE;
      }
      if (strncmp(arg,"UV",2)==0)
      {
        phase =UV;
        PHASE_SET=kTRUE;
      }
      if (strncmp(arg,"muW",3)==0)
      {
        phase =muW;
        PHASE_SET=kTRUE;
      }
      if (strncmp(arg,"BKG",3)==0)
      {
        phase =BKG;
        PHASE_SET=kTRUE;
      }
      if (strncmp(arg,"STOCH",5)==0)
      {
        phase =STOCH;
        PHASE_SET=kTRUE;
      }
    }  //Phase setting
    if (strncmp(arg,"--allowOnline",13)==0)
    {
      AllowOnlineAnalysis=kTRUE;
    }
    if (strncmp(arg,"--autoQOD",9)==0)
    {
      std::cout <<"Enabling autoQOD..."<<endl;
      gAutoQOD=kTRUE;
    }
    else if (strncmp(arg,"--usetimerange=",15)==0)
    {
      gUseTimeRange=kTRUE;
      sscanf(arg,
             "--usetimerange=%lg:%lg",
             &gTMinCut,&gTMaxCut);
      std::cout<< "selecting time range  " << gTMinCut << " : " << gTMaxCut << std::endl;
    }
    else if (strncmp(arg,"--ntracks=",10)==0)
    {
      sscanf(arg,
          "--ntracks=%d",&NTrackCut);
    }
    else 
    {
      int iLen = strlen(arg);
      if (iLen >= 4 && strcmp(&arg[iLen - 5], ".root") == 0)
      sprintf(filename,"%s",arg);
    }  
  }  //Arguemenbt loop
  
  
  if (!PHASE_SET)
  {
    cout <<"No PHASE specified... exiting..."<<endl;
    exit(123);
  }  
  std::cout<<"Phase: "<<phase<<std::endl;
  std::cout<<"Selecting events with "<<NTrackCut<<" tracks: "<<std::endl;
  std::cout << "processing file: " << filename << std::endl;
  dumper(filename, phase, NTrackCut);
  return 0;
  //exit;
}
