#!/bin/sh
# merge.sh runlist.runs
echo Processing files for runlist $1 with prefix $2


prefix=$2
MVANAME=$3 #optional folder to put a single study in... useful for comparing MVA studies
#if MVANAME is unset... default (old) directories are used
# e.g.: prefix=quench (n.b.: minuscolo)

for ii in all `seq 1 20` ; do
  echo "Merging ${ii} tracks files"
  rm -f ${MVANAME}listtomerge${ii}.tmp~ ${MVANAME}root_files_with_no_trees${ii}.txt ${MVANAME}root_files_with_empty_trees${ii}.txt
  for i in `cat $1`; do 
    for j in `ls $MVATREES/${ii}tracks_${prefix}*_tree${i}offline*.root`; do
    #k="${EOS_MGM_URL}//eos/experiment/alpha/DumperTrees/${prefix}_tree${i}offline*.root"
    #ls $RELEASE/alphaMVA/randforest/seldata/${prefix}_tree${i}offline.root 
    if [ -e $j ]; then
      if [ `echo "TFile::Open(\"${j}\")->ls()" | root -l -b | grep TTree | wc -l` -gt 0 ]; then
        if [ `echo "TFile::Open(\"${j}\")->ReadKeys()" | root -l -b | grep -o '[0-9]' ` -gt 0 ]; then

          #Has tree inside
          echo "Found:${j}"
          echo "${j}" >> ${MVANAME}listtomerge${ii}.tmp~
        
        else
        echo "${j}" >> ${MVANAME}root_files_with_empty_trees${ii}.txt
        fi
        
      else
        echo "${j}" >> ${MVANAME}root_files_with_no_trees${ii}.txt
      fi
    fi
    done
  done

  index=0
  if [ ` echo "$MVANAME" | wc -c` -gt 2 ]; then
    cd $RELEASE/alphaMVA/trackDumper/
    echo "Making mergeroot file in $MVANAME"
    root -l -b -q merger.C'(0,1000000,"'${MVANAME}'listtomerge'${ii}'.tmp~","$MVANAME/merged_'${ii}'_tracks_'${prefix}'_'`basename $1`'",'${index}',"selected_'${prefix}'")' > $MVANAME/`basename $1`.log
    cd -
  else
    while [ -a mergeroot/merged_${ii}_tracks_${prefix}_$(basename $1)${index}.root ]; do
      ls mergeroot/merged_${ii}_tracks_${prefix}_$(basename $1)${index}.root
      index=$((index+1))
    done
    echo $index
    #echo root -l -b -q merger.C'(0,1000,"listtomerge.tmp~","mergeroot/merged_'${prefix}'_'`basename $1`'",0,"selected_'${prefix}'")'
    # root -l -b -q merger.C'(0,1000,"listtomerge.tmp~","mergeroot/merged_'${prefix}'_'`basename $1`'",0,"selected_'${prefix}'")'
    root -l -b -q merger.C'(0,1000000,"listtomerge'${ii}'.tmp~","mergeroot/merged_'${ii}'_tracks_'${prefix}'_'`basename $1`'",'${index}',"selected_'${prefix}'")' > `basename $1`.log
  fi
done
