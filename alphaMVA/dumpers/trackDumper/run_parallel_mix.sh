#!/bin/bash
################################################################
#  BASIC ANALYSIS SCRIPT FOR USING ALL CPU CORES ON A MACHINE  #
#                                                              #
#  Usage:                                                      #
#    ./run_parallel.sh listfile.txt                            #
################################################################

#Check myconfig settings
if [ ${#RELEASE} -gt 2 ]; then
  echo "myconfig sourced... good"
else
  echo "Please source myconfig.sh"
  exit
fi


#Set up analysis 'job'
job()
{

  local RunNumber=${1}
  #cd $RELEASE/alphaStrips
  #./alphaStrips.exe --EOS midasdata/run${RunNumber}sub00000.mid.gz &> ${RELEASE}/R${RunNumber}.log
  #cd $RELEASE/alphaAnalysis
  #./alphaAnalysis.exe --EOS midasdata/run${RunNumber}sub00000.mid.gz &>> ${RELEASE}/R${RunNumber}.log
  
  eos cp /eos/experiment/alpha/alphaTrees/tree${RunNumber}offline.root ${TREEFILES}/
  cd $RELEASE/alphaAnalysis/macros
  NMIX=`echo ".L PlotDumps.C+
PrintSequenceQOD(${RunNumber})
.q
"|root -l -b | grep SIS_FLAG | grep MIXING | wc -l`
echo "NMIX: ${NMIX}"
  cd $RELEASE/alphaMVA/dumpers/trackDumper/
  ./hit_trackDumper.exe ${TREEFILES}/tree${RunNumber}offline.root MIX3 1 --autoQOD &> R${RunNumber}_1_MixingDumperQOD.log
  for i in `seq 3 2 ${NMIX}`; do
    ./hit_trackDumper.exe ${TREEFILES}/tree${RunNumber}offline.root MIX3 ${i} --autoQOD &> R${RunNumber}_${i}_MixingDumperQOD.log
  done
  cd $RELEASE/alphaAnalysis/macros
  echo ".L PlotDumps.C+
  RunMVAPreCatchCosmicDumper(${RunNumber},15.,3);
  .q
  "|root -l -b
  mv CosmicDumper${RunNumber}.log ${RELEASE}/alphaMVA/dumpers/trackDumper/PreCatchCosmicDumper${RunNumber}.log
  echo ".L PlotDumps.C+
  RunMVAPostTrappingCosmicDumper(${RunNumber},15.,3);
  .q
  "|root -l -b
  mv CosmicDumper${RunNumber}.log ${RELEASE}/alphaMVA/dumpers/trackDumper/PostTrappingCosmicDumper${RunNumber}.log

  rm ${TREEFILES}/tree${RunNumber}offline.root

#  cd $RELEASE/alphaAnalysis/macros
#  ./BasicBaselines.sh  ${RunNumber}  &>> ${RELEASE}/R${RunNumber}.log

}


#Fancy process bar:
function print_progress ()
{
Job=$1
Total=$2
echo " "
echo "Job $Job of $Total at `date` "

# Process data
    let _progress=(${1}*100/${2}*100)/100
    let _done=(${_progress}*4)/10
    let _left=40-$_done
# Build progressbar string lengths
    _fill=$(printf "%${_done}s")
    _empty=$(printf "%${_left}s")
printf "\rProgress : [${_fill// /#}${_empty// /-}] ${_progress}%%"
echo ""
}



#One can override 'maxjobs' if you want to set the number of CPUs manually
maxjobs=`nproc`

#List file given as first arguement
LISTFILE=${1}


NUMOFLINES=`cat $LISTFILE | wc -l` #$(wc -l < "$LISTFILE")
JobCount=0

while IFS='' read -r RUNNO || [[ -n ${RUNNO} ]]; do
	if [ $maxjobs -gt 1 ]
	then
		jobcnt=(`jobs -p`)
		#echo "Jobcount ${jobcnt[@]}"
		if [ ${#jobcnt[@]} -lt $maxjobs ] ; then
			echo "Starting Job with ${RUNNO} "
			JobCount=$((JobCount+1))
			print_progress $JobCount $NUMOFLINES
			job "${RUNNO}" &
		else
			echo "waiting"
			while [ ${#jobcnt[@]} -ge $maxjobs ]
			do
				sleep 30
				echo "$maxjobs jobs running... sleeping 30s"
				jobcnt=(`jobs -p`)
			done
			echo "Running strips $RUNNO "
			JobCount=$((JobCount+1))
			print_progress $JobCount $NUMOFLINES
			job "${RUNNO}" &
		fi
    else
		job  "${RUNNO}" 
    fi
done < "$LISTFILE"
echo "waiting for jubs to finish"
wait

echo "done..."
