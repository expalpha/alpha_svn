#!/bin/bash


#1, extract SPR
echo "installing to $SPRfolder"
mkdir -p $SPRfolder
SPRbuild=${SPRfolder}_temp
mkdir -p $SPRbuild


tar -xvf SPR-3.3.2.tar.gz -C $SPRbuild

mv ${SPRbuild}/SPR-3.3.2/* $SPRbuild
cd $SPRbuild
rmdir SPR-3.3.2
cd src
#SLC 6.6 and CentOS7 hack:
sed -i '1s/^/#include <cstdio> /' SprAddBaggersApp.cc
sed -i '1s/^/#include <cstdio> /' SprAddColumnsForMCLApp.cc 
sed -i '1s/^/#include <cstdio> /' SprIndicatorMatrix.cc 
sed -i '1s/^/#include <cstdio> /' SprIndicatorMatrixApp.cc 

cd ..
export ROOTCONFIG=`which root-config`
export CXXFLAGS=`root-config --cflags`
./configure --prefix=${SPRfolder}

rm -rf src/SprAdapterDict.C
rootcint src/SprAdapterDict.C -v -I./include include/StatPatternRecognition/SprRootAdapter.hh
cp ${SPRbuild}/SprAdapterDict_rdict.pcm ${SPRbuild}/lib/
make
make install

rm -rf $SPRbuild

for file in `ls ${SPRinclude}`; do
    sed -i 's/\#include \"StatPatternRecognition\//\#include \"/g' ${SPRinclude}/$file
done

for file in `ls ${SPRinclude}`; do
sed -i 's/\#include \"math\//\#include \"/g' ${SPRinclude}/$file
done
