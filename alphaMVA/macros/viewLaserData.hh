#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TLine.h"

#include "TString.h"
#include "TMath.h"
using namespace TMath;

#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

Bool_t gSave=kTRUE;
Bool_t gDraw=kTRUE;

//Double_t gRFcut=0.39;
//Double_t gRFcut=0.54;
//Double_t gRFcut=0.3; //Background
Double_t gRFcut=.58;
//Double_t gTimeCut=0.03; //s
//Double_t gTimeCut=0.1;  //background
//Double_t gTimeCut=1.5;
Double_t gTimeCutMax=0.0025; //Qgate
Double_t gTimeCutMin=0.0005; //Qgate

Double_t gQuenchGate = 0.1;
Double_t rad_cut = 4.20; // cm
Double_t res_cut_eq2 = 2.50; // cm^2
Double_t res_cut_gr2 = 0.08; // Andrea's new optimization
// Int_t  Nruns = 6; // trapping 301
//Int_t  Nruns = 16; // trapping 300
//Int_t  Nruns = 13; // trapping 302
//Int_t Nruns = 9; //trapping 303, 308
//Int_t Nruns = 5; //trapping 304
//Int_t Nruns =  49; //300-04
Int_t Nruns = 6; //trapping 312
