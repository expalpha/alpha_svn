#include "viewQuenchLRData.hh"

void viewQuenchLRData()
{
  if(gSave) cout<<"Saving Plots..."<<endl;
  else cout<<"No Plots"<<endl;

  TFile* fin = (TFile*) gROOT->GetListOfFiles()->First();
  cout<<"Opening "<<fin->GetName()<<" ..."<<endl;
  TTree* tin = (TTree*) fin->Get("vars");
  cout<<tin->GetName()<<" - Entries:" <<tin->GetEntries()<<endl;

  gStyle->SetOptStat(kFALSE);

  tin->Draw("rfout>>hrf(100,0.,1.)","","goff");
  TH1F* hrf = (TH1F*) gROOT->FindObject("hrf");
  hrf->SetLineColor(kRed);
  //  hrf->Scale(1./hrf->Integral());

  Double_t max = hrf->GetMaximum();

  TLine* l = new TLine(gRFcut,0.,gRFcut,max*1.1);
  l->SetLineColor(kBlack);
  l->SetLineWidth(2);
  l->SetLineStyle(2);  

  TLegend* leg = new TLegend(0.17,0.8,0.5,0.91);
  leg->AddEntry(hrf,fin->GetName(),"l");
  leg->AddEntry(l,"RF cut","l");
  
  TCanvas* crf = new TCanvas("crf","crf",1000,800);
  //  hrf->GetYaxis()->SetRangeUser(1.e-3,max*1.1);
  hrf->SetTitle("Random Forest Output;RF");
  hrf->Draw();
  l->Draw("same");
  leg->Draw("same");
  //  crf->SetLogy();
  crf->SaveAs("plots/cRF.png"); 

  TString tsel = TString::Format("tfromquench<=%1.3f",gTimeCut);
  cout<<tsel<<endl;
  tin->Draw("rfout>>hrfcut(100,0.,1.)",tsel.Data(),"goff");
  TH1F* hrfcut = (TH1F*) gROOT->FindObject("hrfcut");
  hrfcut->SetLineColor(kRed);  
  TCanvas* crfcut = new TCanvas("crfcut","crfcut",1000,800);
  hrfcut->SetTitle("Random Forest Output After Time Cut;RF");
  hrfcut->Draw();
  l->Draw("same");
  leg->Draw("same");
  crfcut->SaveAs("plots/cRFcut.png");


  TCanvas* gc = new TCanvas("HbarRFvsSTD","HbarRFvsSTD",1300,1000);
  TLegend* gleg = new TLegend(0.7,0.8,0.9,0.95);

//========================================================================================

  cout<<"Hbar Counting"<<endl;

  Double_t NVtxPassCuts=0., NPassCuts=0., NPassRF=0., NVtxPassRF=0., NEvnt=0.;
  Double_t mean, err;

  Double_t rad, zed, phi, time, rf, res;
  Int_t run, nvtx, nhel;
  Bool_t pass;
  tin->SetBranchAddress("r",&rad);
  tin->SetBranchAddress("z",&zed);
  tin->SetBranchAddress("phi",&phi);
  tin->SetBranchAddress("tfromquench",&time);
  tin->SetBranchAddress("rfout",&rf);
  tin->SetBranchAddress("run",&run);
  tin->SetBranchAddress("nvertices",&nvtx);
  tin->SetBranchAddress("ntracks",&nhel);
  tin->SetBranchAddress("passcut",&pass);
  tin->SetBranchAddress("residual",&res);

  Int_t id;  Double_t tt;
  tin->SetBranchAddress("tvf48",&tt);
  tin->SetBranchAddress("entry",&id);

  tin->GetEntry(0);
  Int_t run_temp = run;
  Double_t NevtCut=0.,Hbar=0.,NVtx=0.,NCosm=0.,TotCosm=0.,TotVtx=0.;

  TH1D* hHbar = new TH1D("hHbar","#bar{H} Counting;Passed-Cuts;Freq.",6,0.,6.);
  TH2D* hzt = new TH2D("hzt","Vertex Axial Position in Time [After RF cut];z [cm];t [s]",50,-26.,26.,100,0.,0.1);
  TH2D* hzphi = new TH2D("hzphi","Vertex Axial and Azimuthal Position [After RF cut];z [cm];#phi [deg]",50,-26.,26.,50,-180.,180.);
  
  ofstream list("statRF.dat");
  list<<setw(5)<<"Run"<<setw(5)<<"ID"<<setw(8)<<"RF"<<setw(11)<<"time [s]"<<setw(6)<<"Hel"<<setw(12)<<"Res [cm^2]"<<setw(5)<<"Vtx"<<setw(9)<<"Rad [cm]"<<setw(9)<<"Zed [cm]"<<setw(9)<<"Phi [deg]"<<setw(4)<<"Pass"<<endl;      
  int prec=list.precision();
  for(Int_t e=0; e<tin->GetEntries(); ++e)
    {
      tin->GetEntry(e);
      
      if(run!=run_temp)
	{
	  hHbar->Fill(Hbar);
	  Hbar=0.;
	  run_temp=run;
	}

      if(time<=gTimeCut)
	++NevtCut;

      if(rf>gRFcut) ++NPassRF;
      else ++TotCosm;

      if(rf>gRFcut && time<=gTimeCut)
	{
	  ++Hbar;
	  ++NPassCuts;
	}
      else if(rf<=gRFcut && time<=gTimeCut) ++NCosm;

      if(nvtx==1 && rf>gRFcut) 
	{
	  ++TotVtx;
	  hzt->Fill(zed,time);
	  hzphi->Fill(zed,phi);
	}
      
      if(nvtx==1 && rf>gRFcut && time<=gTimeCut) ++NVtx;
      
      if(run==36730)
       	cout<<"***DEBUG*** entry: "<<id<<"\t time: "<<tt<<" s"<<endl;
      
      list<<setw(5)<<run;
      list<<setw(5)<<id;
      list.precision(2);
      list<<setw(8)<<rf;
      list.precision(3);
      list<<setw(11)<<time;
      list.precision(prec);
      list<<setw(6)<<nhel;
      list.precision(3);
      list<<setw(11)<<res;
      list.precision(prec);
      list<<setw(5)<<nvtx;
      if(nvtx==1)
	{
	  list.precision(2);
	  list<<setw(9)<<rad<<setw(9)<<zed<<setw(9)<<phi;
	  list.precision(prec);
	  list<<setw(4)<<pass;
	}
      list<<"\n";     
    }
  list.close();
  hHbar->Fill(Hbar);  //gets last run value
  //  *NPass=hHbar->Integral();
  NEvnt=tin->GetEntries();
  cout<<"DEBUG:"<<endl;
  cout<<"# runs = "<<Nruns<<endl;
  cout<<"# evts = "<<NEvnt<<endl;
  cout<<"Cosm = "<<TotCosm<<" Pass = "<<NPassRF<<" Vtx = "<<TotVtx<<endl;
  cout<<"# evts [30 ms] = "<<NevtCut<<endl;
  cout<<"Cosm = "<<NCosm<<" Pass = "<<NPassCuts<<" Vtx = "<<NVtx<<endl;
  Double_t Punzi = NPassCuts/(1.5+Sqrt(NCosm));
  cout<<"\t\t FOM = "<<Punzi<<endl;

  hHbar->GetXaxis()->SetNdivisions(100+hHbar->GetNbinsX());
  hHbar->GetXaxis()->CenterLabels();
  hHbar->SetMarkerStyle(20);
  hHbar->SetMarkerColor(kBlack);
  hHbar->SetLineColor(kBlack);
  //  hHbar->Scale(1./hHbar->Integral());
  TF1* fHbar = new TF1("pois","[0]*TMath::PoissonI(x,[1])",0.,10.);
  fHbar->SetParNames("amplitude","mean");
  hHbar->Fit(fHbar,"0L");
  mean=fHbar->GetParameter(1);
  err=fHbar->GetParError(1);
  TCanvas* c1 = new TCanvas("cHbar","cHbar",1000,800);
  hHbar->Draw("E1");
  fHbar->Draw("same");
  c1->SaveAs("plots/cHbarRF.png");

  TCanvas* c2 = new TCanvas("corvar","corvar",1200,1000);
  c2->Divide(2,1);
  c2->cd(1);
  hzt->Draw("colz");
  c2->cd(2);
  hzphi->Draw("colz");
  c2->SaveAs("plots/corvar.png");
  Double_t gMax=hHbar->GetMaximum();
  gleg->AddEntry(hHbar,"Passed-Cut RF","pl");
  gleg->AddEntry(fHbar,"Poisson Fit RF","l");
  gc->cd();
  hHbar->Draw("E1");
  fHbar->Draw("same");

  Double_t NPassRFerr=Sqrt(NPassRF);
  Double_t NPassCutserr=Sqrt(NPassCuts);

//========================================================================================

  cout<<"Vertex Distributions"<<endl;
  cout<<tin->GetName()<<endl;
  
  TString sel = TString::Format("nvertices==1 && rfout>%.2f",gRFcut);
  TString selL = TString::Format("(run==38398||run==38399||run==38401||run==38403||run==38405||run==38408)&&nvertices==1 && rfout>%.2f",gRFcut);
  
  cout<<"\nRF selection: "<<sel<<endl;

  tin->Draw("r>>hrcut(60,0.,12.)",sel.Data(),"goff");
  TH1F* hrcut = (TH1F*) gROOT->FindObject("hrcut");
  hrcut->SetLineColor(kRed);
  //hrcut->Scale(1./hrcut->Integral());
  tin->Draw("r>>hrcutL(60,0.,12.)",selL.Data(),"goff");
  TH1F* hrcutL = (TH1F*) gROOT->FindObject("hrcutL");
  hrcutL->SetLineColor(kBlue);
  TLegend* vleg = new TLegend(0.17,0.8,0.5,0.91);
  vleg->AddEntry(hrcut,"All RFcut counts overlayed by QWP","l");
  vleg->AddEntry(hrcutL,"QWP Left only","l");

  max = hrcut->GetMaximum();
  TCanvas* crcut = new TCanvas("crcut","crcut",1000,800);
  hrcut->GetYaxis()->SetRangeUser(0.,max*1.1);
  hrcut->SetTitle("Vertex Radial Position [After RF cut];r [cm]");
  hrcut->Draw();
  hrcutL->Draw("same");
  vleg->Draw("same");
  NVtxPassRF = hrcut->GetEntries();


  tin->Draw("tfromquench>>htcut(100,0.,0.1)",sel.Data(),"goff");
  TH1F* htcut = (TH1F*) gROOT->FindObject("htcut");
  htcut->SetLineColor(kRed);
  //  htcut->Scale(1./htcut->Integral());
  tin->Draw("tfromquench>>htcutL(100,0.,0.1)",selL.Data(),"goff");
  TH1F* htcutL = (TH1F*) gROOT->FindObject("htcutL");
  htcutL->SetLineColor(kBlue);
  TLegend* tleg = new TLegend(0.17,0.8,0.5,0.91);
  tleg->AddEntry(htcut,"All RFcut counts overlayed by QWP","l");
  tleg->AddEntry(htcutL,"QWP Left only","l");

  max = htcut->GetMaximum();
  TCanvas* ctcut = new TCanvas("ctcut","ctcut",1000,800);
  htcut->GetYaxis()->SetRangeUser(0.,max*1.1);
  htcut->SetTitle("Annihilation Time [After RF cut];t [s]");
  htcut->Draw();
  htcutL->Draw("same");
  tleg->Draw("same");

  tin->Draw("z>>hzcut(100,-26.,26.)",sel.Data(),"goff");
  TH1F* hzcut = (TH1F*) gROOT->FindObject("hzcut");
  hzcut->SetLineColor(kRed);
  //hzcut->Scale(1./hzcut->Integral());
  tin->Draw("z>>hzcutL(100,-26.,26.)",selL.Data(),"goff");
  TH1F* hzcutL = (TH1F*) gROOT->FindObject("hzcutL");
  hzcutL->SetLineColor(kBlue);
  TLegend* zleg = new TLegend(0.17,0.8,0.5,0.91);
  zleg->AddEntry(hzcut,"All RFcut counts overlayed by QWP","l");
  zleg->AddEntry(hzcutL,"QWP Left only","l");
 
  max = hzcut->GetMaximum();
  TCanvas* czcut = new TCanvas("czcut","czcut",1000,800);
  hzcut->GetYaxis()->SetRangeUser(0.,max*1.1);
  hzcut->SetTitle("Vertex Axial Position [After RF cut];z [cm]");
  hzcut->Draw();
  hzcutL->Draw("same");
  zleg->Draw("same");

  tin->Draw("phi>>hphicut(100,-180.,180.)",sel.Data(),"goff");
  TH1F* hphicut = (TH1F*) gROOT->FindObject("hphicut");
  hphicut->SetLineColor(kRed);
  //hphicut->Scale(1./hphicut->Integral());
  tin->Draw("phi>>hphicutL(100,-180.,180.)",selL.Data(),"goff");
  TH1F* hphicutL = (TH1F*) gROOT->FindObject("hphicutL");
  hphicutL->SetLineColor(kBlue);

  max = hphicut->GetMaximum();
  TCanvas* cphicut = new TCanvas("cphicut","cphicut",1000,800);
  hphicut->GetYaxis()->SetRangeUser(0.,max*1.1);
  hphicut->SetTitle("Vertex Azimuthal Position [After RF cut];phi [deg]");
  hphicut->Draw();
  hphicutL->Draw("same");

  tin->Draw("ntracks>>hhelcut(8,0,8)",sel.Data(),"goff");
  TH1F* hhelcut = (TH1F*) gROOT->FindObject("hhelcut");
  hhelcut->SetLineColor(kRed);
  //hhelcut->Scale(1./hhelcut->Integral());

  max = hhelcut->GetMaximum();
  TCanvas* cchelcut = new TCanvas("cchelcut","cchelcut",1000,800);
  hhelcut->GetYaxis()->SetRangeUser(0.,max*1.1);
  hhelcut->SetTitle("Number of Helices [After RF cut];# of Helices");
  hhelcut->Draw();

  if(gSave)
    {
      ctcut->SaveAs("plots/ctcut.png");
      crcut->SaveAs("plots/crcut.png");
      czcut->SaveAs("plots/czcut.png");
      cphicut->SaveAs("plots/cphicut.png");
      cchelcut->SaveAs("plots/cchelcut.png"); 
    }

  TString scut = TString::Format("nvertices==1 && rfout>%.2f && tfromquench<=%.3f",gRFcut,gTimeCut);
  cout<<scut<<endl;
  tin->Draw("r>>hrtimecut(100,0.,10.)",scut.Data(),"goff");
  TH1F* hrtimecut = (TH1F*) gROOT->FindObject("hrtimecut");
  NVtxPassCuts = hrtimecut->GetEntries();
  //  *NVtxPassCuts=0.;

  Double_t NVtxPassRFerr=Sqrt(NVtxPassRF);
  Double_t NVtxPassCutserr=Sqrt(NVtxPassCuts);

//========================================================================================
  cout<<"Saving Output..."<<endl;
  ofstream fout("viewRFquench.dat");
  fout<<"Events: "<<NEvnt<<endl;
  fout<<"\n====== RF output ======"<<endl;
  fout<<"RF selection: "<<gRFcut<<endl;
  fout<<"Pass RF: "<<NPassRF<<" +/- "<<NPassRFerr<<endl;
  fout<<"Time Cut: "<<gTimeCut*1.e3<<" ms"<<endl;
  fout<<"Pass RF and Time Cut: "<<NPassCuts<<" +/- "<<NPassCutserr<<endl;
  fout<<"Punzi Significance: "<<Punzi<<endl;
  //  fout<<"Mean per attempt: "<<mean<<" +/- "<<err<<endl;
  fout<<"Mean per attempt: "<<NPassRF*1.0/Nruns<<endl;
  fout<<"[Vtx Pass RF: "<<NVtxPassRF<<" +/- "<<NVtxPassRFerr<<"]"<<endl;
  fout<<"[Vtx Pass Cut: "<<NVtxPassCuts<<" +/- "<<NVtxPassCutserr<<"]"<<endl;
  //========================================================================================
  cout<<"Vertex Distributions STD"<<endl;
  cout<<tin->GetName()<<endl;
  fout<<"\n\n====== STD output ======"<<endl;
  Double_t NPassSTD=0.;
  TString selSTD = TString::Format("passcut==1");
  //  TString selSTD = TString::Format("nvertices==1 && r<%2.2f && ( ( ntracks==2 && residual>%2.2f) || (ntracks>2 && residual>%2.2f) )",rad_cut,res_cut_eq2,res_cut_gr2);
  cout<<"\nSTD selection: "<<selSTD<<endl;

  tin->Draw("r>>hrstd(60,0.,12.)",selSTD.Data(),"goff");
  TH1F* hrstd = (TH1F*) gROOT->FindObject("hrstd");
  hrstd->SetLineColor(kRed);
  //hrstd->Scale(1./hrstd->Integral());

  if(gDraw)
    {
      max = hrstd->GetMaximum();
      TCanvas* crstd = new TCanvas("crstd","crstd",1000,800);
      hrstd->GetYaxis()->SetRangeUser(0.,max*1.1);
      hrstd->SetTitle("Vertex Radial Position [After STD cuts];r [cm]");
      hrstd->Draw();
      if(gSave) crstd->SaveAs("plots/crstd.png");
    }

  //  NPassSTD = hrstd->GetEntries();


  tin->Draw("z>>hzstd(100,-26.,26.)",selSTD.Data(),"goff");
  TH1F* hzstd = (TH1F*) gROOT->FindObject("hzstd");
  hzstd->SetLineColor(kRed);
  //  hzstd->Scale(1./hzstd->Integral());
  
  if(gDraw)
    {
      max = hzstd->GetMaximum();
      TCanvas* czstd = new TCanvas("czstd","czstd",1000,800);
      hzstd->GetYaxis()->SetRangeUser(0.,max*1.1);
      hzstd->SetTitle("Vertex Axial Position [After STD cuts];z [cm]");
      hzstd->Draw();
      if(gSave) czstd->SaveAs("plots/czstd.png");
    }


  tin->Draw("phi>>hphistd(100,-180.,180.)",selSTD.Data(),"goff");
  TH1F* hphistd = (TH1F*) gROOT->FindObject("hphistd");
  hphistd->SetLineColor(kRed);
  //hphistd->Scale(1./hphistd->Integral());
 
  if(gDraw)
    {
      max = hphistd->GetMaximum();
      TCanvas* cphistd = new TCanvas("cphistd","cphistd",1000,800);
      hphistd->GetYaxis()->SetRangeUser(0.,max*1.1);
      hphistd->SetTitle("Vertex Azimuthal Position [After STD cuts];#phi [deg]");
      hphistd->Draw();
      if(gSave) cphistd->SaveAs("plots/cphistd.png");
    }


  tin->Draw("ntracks>>hhelstd(8,0,8)",selSTD.Data(),"goff");
  TH1F* hhelstd = (TH1F*) gROOT->FindObject("hhelstd");
  hhelstd->SetLineColor(kRed);
  //hhelstd->Scale(1./hhelstd->Integral());

  if(gDraw)
    {
      max = hhelstd->GetMaximum();
      TCanvas* cchelstd = new TCanvas("cchelstd","cchelstd",1000,800);
      hhelstd->GetYaxis()->SetRangeUser(0.,max*1.1);
      hhelstd->SetTitle("Number of Good Helices [After STD cuts];# of Helices");
      hhelstd->Draw();
      if(gSave) cchelstd->SaveAs("plots/cchelstd.png");
    } 

  // Double_t NPassSTDerr = Sqrt(NPassSTD);
  // fout<<"Vtx Pass STD: "<<NPassSTD<<" +/- "<<NPassSTDerr<<endl;

  //========================================================================================
  Double_t meanSTD, errSTD,NPassSTD_;
  // Double_t rad, time, res;
  // Int_t run, nvtx, nhel;
  // Bool_t pass;
  // tree->SetBranchAddress("r",&rad);
  // tree->SetBranchAddress("residual",&res);
  // tree->SetBranchAddress("tfromquench",&time);
  // tree->SetBranchAddress("run",&run);
  // tree->SetBranchAddress("nvertices",&nvtx);
  // tree->SetBranchAddress("ntracks",&nhel);
  // tree->SetBranchAddress("passcut",&pass);

  tin->GetEntry(0);
  run_temp=run;
  Hbar=0.;
  NCosm=0.;

  TH1D* hHbarSTD = new TH1D("hHbarSTD","#bar{H} Counting [Passed-Cut];Passed-Cut;Freq.",6,0.,6.);
 
  for(Int_t e=0; e<tin->GetEntries(); ++e)
    {
      tin->GetEntry(e);
      
      if(run!=run_temp)
	{
	  hHbarSTD->Fill(Hbar);
	  Hbar=0.;
	  run_temp=run;
	}
      
      if( time>gTimeCut ) continue;

      if(pass==kTRUE)
	{
	  ++Hbar;
	  ++NPassSTD;
	}
      else ++NCosm;

    }
  hHbarSTD->Fill(Hbar); //gets last run

  Double_t NPassSTDerr = Sqrt(NPassSTD);
  fout<<"Vtx Pass STD: "<<NPassSTD<<" +/- "<<NPassSTDerr<<endl;

  cout<<"DEBUG STD: \t Cosm = "<<NCosm<<" Pass = "<<NPassSTD<<endl;
  Double_t PunziSTD = NPassSTD/(1.5+Sqrt(NCosm));
  cout<<"\t\t FOM = "<<PunziSTD<<endl;

  hHbarSTD->GetXaxis()->SetNdivisions(100+hHbarSTD->GetNbinsX());
  hHbarSTD->GetXaxis()->CenterLabels();
  hHbarSTD->SetMarkerStyle(21);
  hHbarSTD->SetMarkerColor(kBlack);
  hHbarSTD->SetLineColor(kBlack);
  hHbarSTD->SetLineStyle(2);
  //  hHbarSTD->Scale(1./hHbar->Integral());
  TF1* fHbarSTD = new TF1("pois","[0]*TMath::PoissonI(x,[1])",0.,10.);
  fHbarSTD->SetLineStyle(2);
  fHbarSTD->SetParNames("amplitude","mean");
  hHbarSTD->Fit(fHbarSTD,"0L");
  Double_t mup=fHbarSTD->GetParameter(1);
  Double_t sip=fHbarSTD->GetParError(1);
  TCanvas* c3 = new TCanvas("cHbarSTD","cHbarSTD",1000,800);
  hHbarSTD->Draw("E1");
  fHbarSTD->Draw("same");
 
  gleg->AddEntry(hHbarSTD,"Passed-Cut STD","pl");
  gleg->AddEntry(fHbarSTD,"Poisson Fit STD","l");

  gc->cd();
  hHbarSTD->Draw("sameE1");
  fHbarSTD->Draw("same");

  //  Double_t NPassSTD_err = Sqrt(NPassSTD_);
  //  fout<<"Vtx Pass STD (dbg): "<<NPassSTD_<<" +/- "<<NPassSTD_err<<endl;
  fout<<"Punzi Significance: "<<PunziSTD<<endl;
  fout<<"Mean per attempt: "<< NPassSTD*1.0/Nruns<<endl;
  fout.close();
  //========================================================================================

  gc->cd();
  gleg->Draw("same");
  gc->SaveAs("plots/cHbar.png");

  cout<<"==================================="<<endl;
  cout<<"\n====== RF output ======"<<endl;
  cout<<"RF selection: "<<gRFcut<<endl;
  cout<<"Pass RF: "<<NPassRF<<" +/- "<<NPassRFerr<<endl;
  cout<<"Time Cut: "<<gTimeCut*1.e3<<" ms"<<endl;
  cout<<"Pass RF and Time Cut: "<<NPassCuts<<" +/- "<<NPassCutserr<<endl;
  cout<<"Punzi Significance: "<<Punzi<<endl;
  cout<<"Mean per attempt: "<<NPassCuts*1.0/Nruns<<endl;
  cout<<"[Vtx Pass RF: "<<NVtxPassRF<<" +/- "<<NVtxPassRFerr<<"]"<<endl;
  cout<<"[Vtx Pass Cut: "<<NVtxPassCuts<<" +/- "<<NVtxPassCutserr<<"]"<<endl;
  cout<<"\n\n====== STD output ======"<<endl;
  cout<<"Vtx Pass STD: "<<NPassSTD<<" +/- "<<NPassSTDerr<<endl;
  //  cout<<"Vtx Pass STD (dbg): "<<NPassSTD_<<" +/- "<<NPassSTD_err<<endl;
  cout<<"Punzi Significance: "<<PunziSTD<<endl;
  cout<<"Mean per attempt: "<<NPassSTD*1.0/Nruns<<endl;
}
