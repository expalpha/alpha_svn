#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TLine.h"

#include "TString.h"
#include "TMath.h"
using namespace TMath;

#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

Bool_t gSave=kTRUE;
Bool_t gDraw=kTRUE;

Double_t gRFcut=0.89;
Double_t gTimeCut=0.03; //s
Double_t gZedCut=13.;//cm

Double_t rad_cut = 4.15; // cm
Double_t res_cut_eq2 = 2.50; // cm^2
Double_t res_cut_gr2 = 0.06;
