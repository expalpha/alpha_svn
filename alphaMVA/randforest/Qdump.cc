/*
 * 
 * How to compile this code:
 * g++ -g -Wno-deprecated -Wno-write-strings `root-config --cflags` -L$SPRlib -lSPR `root-config --glibs`  -lXMLParser -lXMLIO -lThread -Wl,-rpath,$ROOTSYS/lib/root -DUSE_ALPHAVMC -lVMC -lGeom -lEG  -L$RELEASE/alphaAnalysis/lib -lAlphaAnalysis -L$RELEASE/alphavmc/lib/tgt_linuxx8664gcc -lalphavmc -I$SPRinclude -I$SPRfolder -o Qdump.exe Qdump.cc

 * Instruct the program about the file name (full path) and the kind of data
 * you want to skim (i.e., cosmics, mixing events ...)
 * eg dumper.exe $TREEFILES/tree38627offline.root COSMICS
 * see enum RunPhase below for possible values
 * Only cosmics for now others to be done.
 * LD_LIBRARY_PATH must include
 * setenv  LD_LIBRARY_PATH {$SPRlib}:$HOME/alphaSoftware2012/alphaAnalysis/lib:$HOME/alphaSoftware2012/alphavmc/lib/tgt_linuxx8664gcc:$ROOTSYS/lib

 * 
 */

#include "../../alphaAnalysis/lib/include/TSISChannels.h"
#include "../../alphaAnalysis/lib/include/TSeq_Event.h"
#include "../../alphaAnalysis/lib/include/TSisEvent.h"
#include "../../alphaAnalysis/lib/include/TSiliconEvent.h"
#include "../../alphavmc/include/TAlphaEvent.h"
#include "../../alphavmc/include/TAlphaEventVertex.h"



#include "TMatrixDEigen.h"

#include "TROOT.h"
#include "TSystem.h"
#include "TFile.h"
#include "TTree.h"
#include "TEventList.h"

#include <iostream>
#include <algorithm>
#include <vector>
#include <unistd.h> //for sleep
#define VERBOSE 1

enum RunPhase{ COSMICS, MIX, WAIT, QUENCH, ALL, UV, muW, BKG, STOCH};


Double_t gInterc;
//Double_t gSlope = 1.000008814;
Double_t gSlope = 1.;
Double_t gClockDiff = 0.;
Double_t prevtvf48;

TFile* f_curr;
TFile* f_fitData;

TTree* nt_fitData;
// Laser Phase Useage
// hold phase is start and stop of laser dump from sequencer
// sweep phase timestamps are laser - shutter open and close times 
std::vector<Double_t>  sweep_phase_start_ts;
std::vector<Double_t>  sweep_phase_stop_ts;

Double_t hold_phase_start_ts=0.;
Double_t hold_phase_stop_ts=0.;

Double_t stoch_phase_start_ts=0.;
Double_t stoch_phase_stop_ts=0.;

Double_t mix_phase_start_ts=0.;
Double_t mix_phase_stop_ts=0.;

Double_t wait_phase_start_ts=0.;
Double_t wait_phase_stop_ts=0.;

Double_t quench_phase_start_ts=0.;
Double_t quench_phase_stop_ts=0.;  

Double_t bkg_phase_start_ts=0.;
Double_t bkg_phase_stop_ts=0.;

Double_t sweep_phase_latest_ts=0.;
Double_t QFreq = 0.;
#include "Utils.cc"
#include "generalizedspher.cc"



void InsertValue (const char* name, Int_t* value)
{
  TString tmp = TString::Format("%s/I",name);
  const char* type = tmp.Data();

  //  std::cout<<"branch: "<<name<<"\t"<<type<<"\t";

  TBranch* b_variable = nt_fitData->GetBranch(name);
  if (!b_variable)
    {
      nt_fitData->Branch(name,value,type);
      //      std::cout<<"created!"<<std::endl;
    }
  else
    {
      nt_fitData->SetBranchAddress(name,value);
      //      std::cout<<"updated!"<<std::endl;
    }
}


void InsertValue (const char* name, UChar_t* value) 
{
  TString tmp = TString::Format("%s/b",name);
  const char* type = tmp.Data();
  TBranch* b_variable = nt_fitData->GetBranch(name);
  if (!b_variable)
    nt_fitData->Branch(name,value,type);
  else
    nt_fitData->SetBranchAddress(name,value);

}

void InsertValue (const char* name, Bool_t* value)
{
  TString tmp = TString::Format("%s/O",name);
  const char* type = tmp.Data();
  TBranch* b_variable = nt_fitData->GetBranch(name);
  if (!b_variable)
    nt_fitData->Branch(name,value,type);
  else
    nt_fitData->SetBranchAddress(name,value);
}

// RunTime is saved as a double; should consider moving to float
// void InsertValue (const char* name, Float_t* value) {

void InsertValue (const char* name, Double_t* value) 
{
  TString tmp = TString::Format("%s/D",name);//  TString tmp = TString::Format("%s/F",name);
  const char* type = tmp.Data();
  TBranch* b_variable = nt_fitData->GetBranch(name);
  if (!b_variable)
    nt_fitData->Branch(name,value,type);
  else
    nt_fitData->SetBranchAddress(name,value);
}


void SaveEventVariablesASCII(TSiliconEvent* siliconEvent, Int_t runnumber, Int_t phaseid, Int_t entry){

}

Double_t rad_cut = 4.20; // cm  Updated to 2014 capra optimization
Double_t res_cut_eq2 = 2.50; // cm^2
Double_t res_cut_gr2 = 0.08;

void SaveEventVariables(TSiliconEvent* siliconEvent, TAlphaEvent* alphaEvent, 
			Int_t runnumber, Int_t phaseid, Int_t entry, Double_t tfromQ, Int_t LaserDumpNum=-1)
{

  TVector3* vtx;
  Double_t r, z, phi, phioffset;
  Double_t t, tfrommix, tfromquench, tfromwait, SiRunTime, tfromsweep, tfromstoch, tfromhold;
  Double_t tvf48, deltatvf48;
  Int_t ntracks, nvertices;
  Double_t residual;
  Double_t residualL;
  Double_t cosmChi2ProbNdof1; // def impropria, perche` i residui non vengono divisi per l'errore prima di sommarli
  Double_t dca, cosphi, dcar;
  Bool_t passcut;
  
  ntracks = siliconEvent->GetNTracks();
  nvertices = siliconEvent->GetNVertices();
  residual = siliconEvent->GetResidual();
  dca = siliconEvent->GetDCA();

  if (residual > 0.)
    residualL = TMath::Log(residual);
  else
    residualL = -99.;

  if (TMath::IsNaN(residualL))
    residualL = -99.;

  cosmChi2ProbNdof1 = TMath::Prob(residual,1);

  vtx = siliconEvent->GetVertex();
  if (nvertices > 0)
    {
      r = vtx->Perp();
      z = vtx->Z();
      phi = vtx->Phi();
      cosphi = TMath::Cos(phi);
      phi*=TMath::RadToDeg();
    }    
  else
    {
      r = -99.;
      z = -99.;
      phi = -99.;
      cosphi = -99.;
    }
  phioffset = 0.; // check
  // checked: very small from cosmic analysis

  // dca of a 2-tracks event failing the residual cut
  if(ntracks==2 && residual<res_cut_eq2)
    dcar=dca;
  else
    dcar=-99.;

  passcut = ( nvertices==1 && r<rad_cut &&
	      ( (ntracks==2 && residual>res_cut_eq2) || (ntracks>2 && residual > res_cut_gr2) )
	      );

  //  cout<<"\nEvent: "<<entry<<"\t pass: "<<passcut<<"\t";
  
  // Double_t xi= siliconEvent->GetVF48Timestamp(); 
  // Double_t yi= siliconEvent->GetRunTime();
  // SiRunTime = yi;

  //  Double_t toff = siliconEvent->GetRunTime() - siliconEvent->GetVF48Timestamp();

  // // actually, not necessary, since setruntime is run in the calling function dumpntuple() before passing siliconEvent to this one.

  // t = gInterc + xi*gSlope;

  //  siliconEvent->SetRunTime(t);

  SiRunTime = siliconEvent->GetRunTime();

  // tfrommix = t - mix_phase_start_ts;
  // tfromquench = t - quench_phase_start_ts;
  // tfromwait = t - wait_phase_start_ts;
  // tfromsweep = t - sweep_phase_latest_ts;  

  tfrommix = SiRunTime - mix_phase_start_ts;
  tfromquench = SiRunTime - quench_phase_start_ts;
  tfromwait = SiRunTime - wait_phase_start_ts;
  tfromsweep = SiRunTime - sweep_phase_latest_ts;
  tfromstoch = SiRunTime - stoch_phase_start_ts;
  tfromhold = SiRunTime - hold_phase_start_ts;

    // testing 
    tvf48 = siliconEvent->GetVF48Timestamp();
  deltatvf48 = tvf48 - prevtvf48;
  prevtvf48 = tvf48;

  t = gInterc + tvf48 * gSlope;

  // alpha event part 
  TAlphaEventVertex* aevtx = alphaEvent->GetVertex();

  Double_t norm=0.;

  std::vector<double> velx;
  std::vector<double> vely;
  std::vector<double> velz;

  std::vector<double> velxraw;
  std::vector<double> velyraw;
  std::vector<double> velzraw;


  Int_t nAT =  alphaEvent->GetNHelices(); // all tracks
  Int_t nCT = 0;
  Int_t nraw = 0;
  for (int i = 0; i< nAT ; ++i)
    {
      TAlphaEventHelix* aehlx = alphaEvent->GetHelix(i);
      Double_t fc = aehlx->Getfc();
      Double_t fphi0 = aehlx->Getfphi(); 
      Double_t fLambda = aehlx->Getflambda(); 
      
      Double_t s=0.; // calculates velx,y,z at POCA
      // special case for s = 0

      // select good helices, after removal of duplicates
      if (aehlx->GetHelixStatus()==1)
	{
	  ++nraw; // == ntracks
	  velxraw.push_back( - TMath::Sin(fphi0));
	  velyraw.push_back( TMath::Cos(fphi0)) ;
	  velzraw.push_back( fLambda ); 
	  ++nCT; 
	}
      delete aehlx;
    }

  Int_t nGTL = aevtx->GetNHelices(); 
  int nGT = 0;
  for (int i = 0; i< nGTL ; ++i)
    {
      TAlphaEventHelix* aehlx = aevtx->GetHelix(i);
      //    if(aehlx->GetHelixStatus()<0) continue;
      Double_t fc = aehlx->Getfc();
      Double_t fphi0 = aehlx->Getfphi(); 
      Double_t fLambda = aehlx->Getflambda(); 
    
      Double_t s=0.; // calculates velx,y,z at POCA
      // special case for s = 0
      velx.push_back( - TMath::Sin(fphi0) );
      vely.push_back( TMath::Cos(fphi0) ) ;
      velz.push_back( fLambda );  
      
      // select good helices, after removal of duplicates
      if (aehlx->GetHelixStatus()==1) ++nGT;
      delete aehlx;
    }

  //  cout<<"\t nAT: "<<nAT<<" nCT: "<<nCT<<" nraw: "<<nraw<<" nGTL: "<<nGTL<<" nGT: "<<nGT<<" Ntracks: "<<ntracks<<endl;
  

  TVector3* S0axis;
  TVector3* S0values;

  TVector3* S0axisraw;
  TVector3* S0valuesraw;

  Double_t S0 = -99.;
  Double_t S0raw = -99.;

  Double_t S0Perp = -99.;
  Double_t S0rawPerp = -99.;

  Double_t S0rawl1 = -99.;
  Double_t S0rawl2 = -99.;
  Double_t S0rawl3 = -99.;

  Double_t S0l1 = -99.;
  Double_t S0l2 = -99.;
  Double_t S0l3 = -99.;

  Double_t phi_S0axis = -99.;
  Double_t phi_absS0axis = -99.;

  Double_t phi_S0axisraw = -99.;
  Double_t phi_absS0axisraw = -99.;

  //   Double_t S0raw = -99.; // C, D, A
 
  Double_t S0axisrawX = -99.;
  Double_t S0axisrawY = -99.;
  Double_t S0axisrawZ = -99.;

  Double_t S0axisX = -99.;
  Double_t S0axisY = -99.;
  Double_t S0axisZ = -99.;

  Int_t nhits = alphaEvent->GetNHits();
  Int_t nhits_nomatch = -99.;
  Double_t nhitsasym = -99.; // diff between hemispheres
  Double_t nhitsasymraw = -99.; // diff between hemispheres


  if(nraw>0)
    {
      nhitsasymraw = 0; 
      
      sphericity(velxraw, velyraw, velzraw, 0, &S0axisraw, &S0valuesraw); // generalizedspher.h
    
      S0raw = 1.5*(S0valuesraw->Y()+S0valuesraw->Z());
      S0rawPerp = S0valuesraw->Perp();

      S0rawl1 = S0valuesraw->X();
      S0rawl2 = S0valuesraw->Y();
      S0rawl3 = S0valuesraw->Z();
      
      S0axisrawX = S0axisraw->X();
      S0axisrawY = S0axisraw->Y();
      S0axisrawZ = S0axisraw->Z();
      
      phi_S0axisraw = TMath::ACos(S0axisrawY/TMath::Sqrt(S0axisrawX*S0axisrawX+S0axisrawY*S0axisrawY));
      phi_absS0axisraw = TMath::ACos(TMath::Abs(S0axisrawY/TMath::Sqrt(S0axisrawX*S0axisrawX+S0axisrawY*S0axisrawY)));

      for (int i = 0; i< nhits ; ++i)
	{
	  TAlphaEventHit* aehit = alphaEvent->GetHit(i);
	  TVector3* hitpos = new TVector3(aehit->X(),aehit->Y(),aehit->Z());
	  Double_t dotprod = hitpos->Dot(*S0axisraw);
	  if (dotprod>0.)
	    ++nhitsasymraw;
	  else if (dotprod<0.)
	    --nhitsasymraw;
	  delete hitpos;
	}
      nhitsasymraw/=nhits;
    }

  if(nGTL>0)
    {
      // deg=1 => linearized sphericity; deg=2 => default non infrared-safe sphericity
      nhitsasym = 0; 
      
      sphericity(velx, vely, velz, 0, &S0axis, &S0values); // generalizedspher.h
      
      S0 = 1.5*(S0values->Y()+S0values->Z());
      S0Perp = S0values->Perp();

      S0l1 = S0values->X();
      S0l2 = S0values->Y();
      S0l3 = S0values->Z();
      
      S0axisX = S0axis->X();
      S0axisY = S0axis->Y();
      S0axisZ = S0axis->Z();
      
      phi_S0axis = TMath::ACos(S0axisY/TMath::Sqrt(S0axisX*S0axisX+S0axisY*S0axisY));
      phi_absS0axis = TMath::ACos(TMath::Abs(S0axisY/TMath::Sqrt(S0axisX*S0axisX+S0axisY*S0axisY)));

      for (int i = 0; i< nhits ; ++i)
	{
	  TAlphaEventHit* aehit = alphaEvent->GetHit(i);
	  TVector3* hitpos = new TVector3(aehit->X(),aehit->Y(),aehit->Z());
	  Double_t dotprod = hitpos->Dot(*S0axis);
	  if (dotprod>0.)
	    ++nhitsasym;
	  else if (dotprod<0.)
	    --nhitsasym;
	  delete hitpos;
        }
      nhitsasym/=nhits;
    }


  InsertValue("nhits",&nhits);
  InsertValue("nhitsasym",&nhitsasym);
  InsertValue("nhitsasymraw",&nhitsasymraw);

  InsertValue("S0",&S0);
  InsertValue("S0raw",&S0raw);  
  InsertValue("S0Perp",&S0Perp);
  InsertValue("S0rawPerp",&S0rawPerp);

  InsertValue("S0axisrawX",&S0axisrawX);
  InsertValue("S0axisrawY",&S0axisrawY);
  InsertValue("S0axisrawZ",&S0axisrawZ);

  InsertValue("S0axisX",&S0axisX);
  InsertValue("S0axisY",&S0axisY);
  InsertValue("S0axisZ",&S0axisZ);

  InsertValue("nAT",&nAT);
  InsertValue("nCT",&nCT);
  InsertValue("nGT",&nGT);
  InsertValue("nGTL",&nGTL);

  InsertValue("S0rawl1",&S0rawl1);
  InsertValue("S0rawl2",&S0rawl2);
  InsertValue("S0rawl3",&S0rawl3);

  InsertValue("S0l1",&S0l1);
  InsertValue("S0l2",&S0l2);
  InsertValue("S0l3",&S0l3);

  InsertValue("phi_S0axis",&phi_S0axis);
  InsertValue("phi_absS0axis",&phi_absS0axis);

  InsertValue("phi_S0axisraw",&phi_S0axisraw);
  InsertValue("phi_absS0axisraw",&phi_absS0axisraw);

  InsertValue("run",&runnumber);
  InsertValue("entry",&entry);
  InsertValue("phaseid",&phaseid);
  
  InsertValue("r",&r);
  InsertValue("z",&z);
  InsertValue("phi",&phi);
  InsertValue("phioffset",&phioffset);

  InsertValue("cosphi",&cosphi);
	    
  InsertValue("tvf48",&tvf48);  
  InsertValue("deltatvf48",&deltatvf48);

  InsertValue("t",&t);
  InsertValue("SiRunTime",&SiRunTime);

  InsertValue("tfromQ",&tfromQ);

  InsertValue("tfrommix",&tfrommix);
  InsertValue("tfromquench",&tfromquench);
  InsertValue("tfromwait",&tfromwait);
  InsertValue("tfromstoch",&tfromstoch);
	    
  InsertValue("ntracks",&ntracks);
  InsertValue("nvertices",&nvertices);
  InsertValue("residual",&residual);
  InsertValue("residualL",&residualL);
  InsertValue("cosmChi2ProbNdof1",&cosmChi2ProbNdof1);

  InsertValue("tracksdca",&dca);
  InsertValue("tracksdcacut",&dcar);

  InsertValue("passcut",&passcut);

  InsertValue("LaserDumpNum",&LaserDumpNum);
  InsertValue("tfromsweep",&tfromsweep);
  
  nt_fitData->Fill();

}


void AddAlphaEventVariables(TAlphaEvent* alphaEvent, Int_t runnumber, Int_t phaseid, Int_t entry)
{

  // insert custom code here

}


void GetRunTimes(TTree* SISTree, std::vector<double>* timestamps)
{
  TSisEvent* SISEvent = new TSisEvent();

  SISTree->SetBranchAddress("SisEvent", &SISEvent );
  Double_t runtime;
  for( Int_t i=0; i<SISTree->GetEntries(); ++i) 
    {
      SISTree->GetEntry(i);
      runtime = SISEvent->GetRunTime();
      timestamps->push_back(runtime);
    }

  delete SISEvent;
}

void dumper(char* f_name = "data/tree19324_sub0offline.root", RunPhase phaseid = COSMICS)
{
  //  std::cout << "processing file "<< f_name << " for runphase=" << phaseid << std::endl;
  //   libsToLoad();

  std::cout << "processing file "<< f_name << " for runphase=" << phaseid << std::endl;
  if(0)  exit(123); // test

  TFile* f_curr = new TFile(f_name);
  std::cout << f_curr <<"\t"<<f_curr->GetName() <<std::endl;

  Int_t LaserDumpNum=-1; 
  std::cout<<"sweep: "<<LaserDumpNum<<std::endl;

  // parse input file name and prepare output file name / get run number
  TString treename="selected";
  TString filename="seldata/";
  std::cout<<treename<<std::endl;

  TString basename(f_curr->GetName()); 
  basename.Remove(0,basename.Last('/')+1); // strip path name

  TString runname(basename.Data(),9); // tree#####
  runname.Remove(0,4); // remove "tree"
  
  Int_t runnumber = atoi(runname.Data());
  std::cout<<runnumber<<std::endl;
  TTree* shutter_open_tree;
  TTree* shutter_close_tree;
  TTree* Qpulse_tree;
  Int_t NQPulses=-1;
  Int_t iQPulse=-1;
  Int_t NShutterGates =0;
  Double_t tfromQ =-99.;
  Double_t timeOfLaserStart;
  Double_t timeOfLaserStop;
  //  strcat(ofname,"seldata/");
  switch (phaseid)
    {
    case COSMICS: 
      filename+="cosm_";
      treename+="_cosm";
      break;
    case MIX: 
      filename+="mix_";
      treename+="_mix";
      break;
    case WAIT: 
      filename+="wait_";
      treename+="_wait";  
      break;
    case QUENCH: 
      filename+="quench_";
      treename+="_quench"; 
      break;    
    case BKG: 
      filename+="bkg_";
      treename+="_bkg";
      break;
    case ALL: 
      filename+="all_";
      treename+="_all";
      break;
    case UV:
      filename+="laser_";
      treename+="_laser";
      break;
    case muW:
      filename+="muW_";
      treename+="_muW";
      break;
    case STOCH:
      filename+="stoch_";
      treename+="_stoch";
      break;
    }

  filename.Append(basename.Data(),strlen(basename.Data())-5);
  filename+=".root";
  //  const char* ofname = filename.Data();
  std::cout << filename << std::endl;

  //  char ofnamesearch[128];
  //  sprintf(ofnamesearch,"%s",ofname);
  // open output file and create tree 
  //  TFile* f_fitData = (TFile*)gROOT->FindObject(ofnamesearch);
  TFile* f_fitData = (TFile*) gROOT->FindObject(filename.Data());
  if (f_fitData) f_fitData->Close();
  gROOT->cd();
  f_fitData = new TFile(filename.Data(),"RECREATE");

  //  const char* otname=treename.Data();
  //  nt_fitData=new TTree(otname,otname);
  std::cout << treename << std::endl;
  nt_fitData=new TTree(treename.Data(),treename.Data());

  std::cout << nt_fitData->GetName() <<" in "<< f_fitData->GetName() << std::endl;

  mix_phase_start_ts=0.;
  mix_phase_stop_ts=0.;
  
  wait_phase_start_ts=0.;
  wait_phase_stop_ts=0.;

  quench_phase_start_ts=0.;
  quench_phase_stop_ts=0.;

  hold_phase_start_ts=0.;
  hold_phase_stop_ts=0.;  

  stoch_phase_start_ts=0.;
  stoch_phase_stop_ts=0.;

  bkg_phase_start_ts=0.;
  bkg_phase_stop_ts=0.;

  gSISch = new TSISChannels(runnumber);

  // Double_t startsweep=0.; 
  // Double_t stopsweep=0.;

  // for (int i=0; i<20; i++){
  //   int found = GetPhaseStartStop(sequencerTree,seqEvent,SISTrees,SISEvent,
  //                                 TString("\"Microwave Sweep"),&startsweep,&stopsweep,i);

  //   if (found < 0) 
  //     break;

  //   sweep_phase_start_ts.push_back(startsweep);
  //   sweep_phase_stop_ts.push_back(stopsweep);
    
  //   cout << i << " " << found << " " << sweep_phase_start_ts.at(i) << " " << sweep_phase_stop_ts.at(i) << endl;

  // }
  //
  // GetPhaseStartStop(sequencerTree,seqEvent,SISTrees,SISEvent,
  //                   TString("\"Hold Before"),&hold_phase_start_ts,&hold_phase_stop_ts,i);
  // cout << "Hold: " << hold_phase_start_ts << " " << hold_phase_stop_ts << endl;
  //Calculate clock correction
  Int_t vf48ch =gSISch->GetChannel("SIS_VF48_CLOCK");
  if(vf48ch>1) 
    gClockDiff=GetClockCalib(f_curr);//chan defined for this run
  std::cout<<"gSlope = 1 - "<<gClockDiff<<std::endl;
  gSlope = 1.- gClockDiff;
  if(phaseid != COSMICS )
    { 
      //      FindDumpTime(f_curr,"Mixing",mix_phase_start_ts,mix_phase_stop_ts);
      //      FindMixingGate(f_curr,mix_phase_start_ts,mix_phase_stop_ts);
      GetDumpTime(f_curr,(char*)"Mixing",mix_phase_start_ts,mix_phase_stop_ts);
      std::cout << "Mix Phase Start: " << mix_phase_start_ts <<  " s  ; ";
      std::cout << "Mix Phase Stop: " << mix_phase_stop_ts << " s  ; " ;
      std::cout << "Mix Phase Time: " << mix_phase_stop_ts - mix_phase_start_ts << " s  " << std::endl;
          
      //      FindDumpTime(f_curr,(char*)"Background Si2",bkg_phase_start_ts,bkg_phase_stop_ts);
      GetDumpTime(f_curr,(char*)"Background Si2",bkg_phase_start_ts,bkg_phase_stop_ts);
      std::cout << "Bkg Phase Start: " << bkg_phase_start_ts <<  " s  ; ";
      std::cout << "Bkg Phase Stop: " << bkg_phase_stop_ts << " s  ; " ;
      std::cout << "Bkg Phase Time: " << bkg_phase_stop_ts - bkg_phase_start_ts << " s  " << std::endl;
    
      //      FindDumpTime(f_curr,(char*)"Quench",quench_phase_start_ts,quench_phase_stop_ts);
      FindQuenchGate(f_curr,quench_phase_start_ts,quench_phase_stop_ts);
      //      GetDumpTime(f_curr,(char*)"Quench",quench_phase_start_ts,quench_phase_stop_ts);
      std::cout << "Quench Phase Start: " << quench_phase_start_ts <<  " s  ; ";
      std::cout << "Quench Phase Stop: " << quench_phase_stop_ts << " s  ; " ;
      std::cout << "Quench Phase Time: " << quench_phase_stop_ts - quench_phase_start_ts << " s  " << std::endl;
    }

  if( phaseid == UV )
    { 
      //      FindDumpTime(f_curr,(char*)"Laser Dump",hold_phase_start_ts,hold_phase_stop_ts);
      //GetDumpTime(f_curr,(char*)"Laser Dump",hold_phase_start_ts,hold_phase_stop_ts);
      // FindLaserGate(f_curr, hold_phase_start_ts, hold_phase_stop_ts ,  QFreq) ;// Laser dump sequence is missing do it directly
      // std::cout << "Laser Start: " << hold_phase_start_ts <<  " s  ; ";
      // std::cout << "Laser Stop: " << hold_phase_stop_ts << " s  ; " ;
      // std::cout << "Laser Time: " << hold_phase_stop_ts -hold_phase_start_ts << " s  " << std::endl;
      //Get the shutter_open_tree, shutter_close_trees and NShutterGates, Qpulse_tree NQPulses, QFreq
      Int_t shutter_open_channel=gSISch->GetChannel("QPULSE_SHUTTER_OPEN");
      shutter_open_tree = GetSISTree(f_curr,shutter_open_channel);
      timeOfLaserStart = GetRunTimeOfEntry(shutter_open_tree,0);
      assert(timeOfLaserStart>0.);
      NShutterGates=shutter_open_tree->GetEntries(); 
      timeOfLaserStop = GetRunTimeOfEntry(shutter_open_tree, NShutterGates-1);
      std::cout<<"LASER TS: "<<timeOfLaserStart<<" s to"<<timeOfLaserStop<<" s"<<std::endl;
      assert(timeOfLaserStop>=timeOfLaserStart);
      hold_phase_start_ts=  timeOfLaserStart;
      hold_phase_stop_ts=  timeOfLaserStop;
      Int_t QPulse_channel=gSISch->GetChannel("QPULSE"); 
      Qpulse_tree = GetSISTree(f_curr,QPulse_channel);
      NQPulses =  Qpulse_tree->GetEntries();
      QFreq = (NQPulses-1)/(GetRunTimeOfEntry(Qpulse_tree,NQPulses-1)-GetRunTimeOfEntry(Qpulse_tree,0));
      std::cout <<"Q pulser rate: "<<QFreq<<" NQPulses "<<NQPulses<<" first time "<<GetRunTimeOfEntry(Qpulse_tree,0)<<std::endl;
      iQPulse=-1;

    }

  if( phaseid == STOCH )
    {
      GetDumpTime(f_curr,(char*)"Stochastic Dump",stoch_phase_start_ts,stoch_phase_stop_ts);
      std::cout << "Stochastic Heating Start: " << stoch_phase_start_ts <<  " s  ; ";
      std::cout << "Stochastic Heating Stop: " << stoch_phase_stop_ts << " s  ; " ;
      std::cout << "Stochastic Heating Time: " << stoch_phase_stop_ts - stoch_phase_start_ts << " s  " << std::endl;
      
    }
  

  Double_t readstart=0.0, readstop=0.0;
  switch (phaseid)
    {
    case MIX: 
      readstart = mix_phase_start_ts;
      readstop = mix_phase_stop_ts;
      break;
    case WAIT: 
      readstart = wait_phase_start_ts;
      readstop = wait_phase_stop_ts;
      break;
    case QUENCH: 
      readstart = quench_phase_start_ts;
      readstop = quench_phase_stop_ts;
      break;    
    case BKG: 
      readstart = bkg_phase_start_ts;
      readstop = bkg_phase_stop_ts;
      break;
    case UV: 
      readstart = hold_phase_start_ts;
      readstop = hold_phase_stop_ts;
      break;
    case STOCH:
      readstart = stoch_phase_start_ts;
      readstop = stoch_phase_stop_ts;
      break;
    }
  
  std::vector<double> adtimestamps; // timestamps for AD injections/extractions

  int SIS_AD_IN=gSISch->GetChannel((char*)"SIS_DIX_ALPHA"),
    SIS_AD_EJ=gSISch->GetChannel((char*)"SIS_AD");

  GetRunTimes(GetSISTree(f_curr,SIS_AD_IN), &adtimestamps); // injections
  GetRunTimes(GetSISTree(f_curr,SIS_AD_EJ), &adtimestamps); // extractions 

  std::sort(adtimestamps.begin(), adtimestamps.end()); // sort them 

  //  for (int i=0; i<adtimestamps.size(); i++) std::cout << adtimestamps.at(i) << std::endl;


  // read silicon tree
  TTree* vertexTree = GetSiliconEvents(f_curr);
  TSiliconEvent* siliconEvent = new TSiliconEvent();
  Int_t eventID;
  vertexTree->SetBranchAddress("SiliconEvent", &siliconEvent );
  vertexTree->SetBranchAddress("eventID", &eventID );

  // read alphaevent tree
  TTree* alphaeventTree = GetAlphaEvents(f_curr);
  TAlphaEvent* alphaEvent = 0;
  Int_t alphaeventID;
  alphaeventTree->SetBranchAddress("AlphaEvent", &alphaEvent );
  alphaeventTree->SetBranchAddress("eventID", &alphaeventID );

  
  if( vertexTree == NULL ) 
    {
      std::cout << "vertexTree == NULL !!!!!" <<std::endl;
      return;
    }

  vertexTree->GetEntry(0);
  // //  Double_t toff = siliconEvent->GetRunTime() - siliconEvent->GetVF48Timestamp(); 
  
  // Double_t x0,y0;
  // Double_t xi,yi;

  // //  Double_t slope = 1.000008814;
  // Double_t slope = 1.;

  // x0= siliconEvent->GetVF48Timestamp(); 
  // y0= siliconEvent->GetRunTime();

  // Double_t interc = y0 - slope * x0;
  // gInterc = y0 - slope * x0;

  gInterc = siliconEvent->GetRunTime() - gSlope * siliconEvent->GetVF48Timestamp();

  // if cosmics, veto neighbours of ad events timestamps 
  // read tree sequencially, and assume runtimes are properly ordered (!)

  if ( phaseid == COSMICS )
    {
      double ad_eps = 0.5;
      
      for(Int_t i=0; i<vertexTree->GetEntries(); ++i)
	{
	  siliconEvent->ClearEvent();
	  alphaEvent->DeleteEvent();
	  if(i%5000==0) std::cout<<i<<std::endl;

	  char aeselection[128];

	  Int_t entry = i;
	  vertexTree->GetEntry(i);
	  
	  // xi= siliconEvent->GetVF48Timestamp(); 
	  // yi= siliconEvent->GetRunTime();
      
	  // siliconEvent->SetRunTime(interc + xi*slope);
           
	  bool ad_veto = kFALSE;

	  // da scommentare
	  //	  assert(siliconEvent->GetRunTime() == interc + xi*slope);

	  for(int ad_ii=0; ad_ii<adtimestamps.size(); ++ad_ii)
	    { 
	      if ( fabs( siliconEvent->GetRunTime() - adtimestamps.at(ad_ii)) < ad_eps )
		{ 
		  ad_veto=kTRUE;
		  break;
		}
	    } 
	  
	  if(ad_veto) continue;
	  
	  //      cout << eventID << " : " << eventList->GetN() << endl;      
	  
	  sprintf(aeselection, "eventID==%d", eventID);
	  alphaeventTree->Draw(">>alphaeventList", aeselection);
	  TEventList* alphaeventList = (TEventList*)gDirectory->Get("alphaeventList");

	  if(alphaeventList==NULL) {}  
	  if(alphaeventList->GetN()==0)
	    { 
	      std::cout << "0 events selected. Check selection: " << aeselection << std::endl;
	      exit(123);
	    }
	  
	  if(alphaeventList->GetN()>1) 
	    {
	      std::cout << "exiting " << alphaeventList->GetN() << std::endl;
	      exit(123);
	    }

	  Int_t aeentry = alphaeventList->GetEntry(0);
	  alphaeventTree->GetEntry(aeentry);

	  SaveEventVariables(siliconEvent, alphaEvent, runnumber, phaseid, eventID,-1);
	}
      //      cout<<eventID<<endl;
    } // phaseid cosmics
  else 
    {
      char selection[128];
      char aeselection[128];
      Double_t SiRunTime;
      Int_t currentShutter=0;
      for(Int_t i=0; i<vertexTree->GetEntries(); ++i)
	{
	  vertexTree->GetEntry(i);
	  if(i%2000==0) std::cout<<i<<std::endl;
	  
	  SiRunTime = siliconEvent->GetRunTime();
	  SiRunTime-=gClockDiff*SiRunTime;
	  siliconEvent->SetRunTime(SiRunTime);
	  //	  std::cout<<" to "<<SiRunTime<<std::endl;
	  sprintf(selection, "RunTime>=%lf && RunTime<=%lf", readstart,readstop);
	  if( SiRunTime < readstart || SiRunTime > readstop )
	    continue;

	  //      cout << eventID << " : " << eventList->GetN() << endl;      

	  sprintf(aeselection, "eventID==%d", eventID);
	  alphaeventTree->Draw(">>alphaeventList", aeselection);
	  TEventList* alphaeventList = (TEventList*)gDirectory->Get("alphaeventList");
	  
	  if(alphaeventList==NULL) {}  
	  if(alphaeventList->GetN()==0)
	    { 
	      std::cout << "0 events selected. Check selection: " << selection << std::endl;
	      exit(123);
	    }

	  if (alphaeventList->GetN()>1) 
	    {
	      std::cout << "exiting " << alphaeventList->GetN() << std::endl;
	      exit(123);
	    }
	  
	  Int_t aeentry = alphaeventList->GetEntry(0);
	  alphaeventTree->GetEntry(aeentry);
          if ( phaseid == UV ) //Calculate the additional vars tfromQ, laserDumpNum here
	    {//Loop on shutter opens, test SiRunTime, find iQpulse
	      //Get the shutter_open_tree, shutter_close_tree and NShutterGates, Qpulse_tree NQPulses, QFreq

	      if (timeOfLaserStart > SiRunTime) continue;

	      else
		{
		  if (timeOfLaserStop > SiRunTime)
		    {
		      LaserDumpNum=1;
		      iQPulse = (SiRunTime-timeOfLaserStart)*QFreq;
		      tfromQ=SiRunTime-GetRunTimeOfEntry(shutter_open_tree,iQPulse);
		      // std::cout<<"iQPulse1 "<<iQPulse<< " tfromQ " <<tfromQ<<std::endl;

		      while (tfromQ<0.0 &&iQPulse>0) tfromQ=SiRunTime-GetRunTimeOfEntry(shutter_open_tree,--iQPulse);
		      // std::cout<<"iQPulse2 "<<iQPulse<< " tfromQ " <<tfromQ<<std::endl;

		      while(tfromQ>1./QFreq&&iQPulse<NQPulses) tfromQ=SiRunTime -GetRunTimeOfEntry(shutter_open_tree,iQPulse++);
		      // std::cout<<"iQPulse3 "<<iQPulse<<" SiRunTime "<<SiRunTime<< " tfromQ " <<tfromQ<<std::endl;
		      SaveEventVariables(siliconEvent, alphaEvent, runnumber, phaseid, eventID, tfromQ, LaserDumpNum);       
		    }			  
		}//QShutter open
	    }
	  else
	    {
		SaveEventVariables(siliconEvent, alphaEvent, runnumber, phaseid, eventID,-0.5, -1 );
	      // std::cout<<"No UV "<<iQPulse<<"SiRunTime"<<SiRunTime<< " tfromQ " <<tfromQ<<std::endl;
 	    }
	}//vertex loop
    }//Not cosmics
  delete siliconEvent;	    
  std::cout<<vertexTree->GetEntries()<<std::endl;
  delete gSISch;

  f_fitData->cd();
  //  nt_fitData->Write("",TObject::kOverwrite);
  nt_fitData->Write();
  f_fitData->Close();
  f_curr->cd();
  //  f_curr->Close();
  return;
} 
int main(int argc, char *argv[])
{
  std::cout <<argc<< " Arg1 "<<argv[1]<<" Arg2 "<<argv[2]<<std::endl;
  char filename[128];
  char phasechar[128];
  RunPhase phase;
  //  if(argc==4)
  if(argc==3)
    {
  std::cout <<argc<< " Arg1 "<<argv[1]<<" Arg2 "<<argv[2]<<std::endl;
      if (!strcmp(argv[2], "COSMICS")) phase =COSMICS; 
      else if (!strcmp(argv[2], "MIX")) phase =MIX; 
      else if (!strcmp(argv[2], "WAIT")) phase =WAIT; 
      else if (!strcmp(argv[2], "QUENCH")) phase =QUENCH; 
      else if (!strcmp(argv[2], "ALL")) phase =ALL; 
      else if (!strcmp(argv[2], "UV")) phase =UV; 
      else if (!strcmp(argv[2], "muW")) phase =muW;
      else if (!strcmp(argv[2], "BKG")) phase =BKG;  
      else if (!strcmp(argv[2], "STOCH")) phase =STOCH;
    }
  else
    phase =COSMICS;

  std::cout<<"Phase: "<<phase<<std::endl;
  //  sleep(20);
  sprintf(filename,"%s",argv[1]);
  std::cout << "processing file: " << filename << std::endl;
  dumper(filename, phase);
  return 1;
}
