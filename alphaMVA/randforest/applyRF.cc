#include "SprClassifierReader.hh"
#include "SprAbsTrainedClassifier.hh"


/* These headers are not used...
#include "../../alphaAnalysis/lib/include/TSeq_Event.h"
#include "../../alphaAnalysis/lib/include/TSisEvent.h"
#include "../../alphaAnalysis/lib/include/TSiliconEvent.h"
#include "../../alphaAnalysis/lib/include/TSISChannels.h"
#include "../../alphaAnalysis/lib/include/TTCEvent.h"
#include "../../alphaAnalysis/lib/include/TLabVIEWEvent.h"
#include "../lib/include/SIS_Channels.h.alpha2"
#include "../../alphaAnalysis/lib/include/TVF48SiMap.h"
#include "../../alphavmc/include/TAlphaEvent.h"
#include "../../alphaAnalysis/lib/include/TAnalysisReport.h"
*/

#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TLeaf.h"
#include "TString.h"
#include "TSystem.h"


#include <stdlib.h> 
#include <iostream>
#include "Riostream.h"
//#include <fstream>
#include <string>

//#include "Riostream.h"

using namespace std;
int main(int argc, char *argv[]) 
{ 
  char ifilename[256];
  char filename[256];
  char trainfile[256];
  char phase[50];

  if(argc>=4) 
  {
	cout <<"Recieved 3+ arguments.."<< endl;
    sprintf(ifilename,"%s",argv[1]);
    sprintf(filename,"%s",argv[2]);
    sprintf(trainfile,"%s",argv[3]);
   }
  else
  {
    if(argc==3)
	{
		  cout <<"Recieved 2 arguments.."<< endl;
      sprintf(trainfile,"%s",argv[2]);
    }
    else 
    {
	  sprintf(trainfile,"%s","randforest.spr");
	  cout <<"No training file declased, using default: "<< endl;
	}
  sprintf(ifilename,"%s",argv[1]);
  sprintf(filename,"%s",argv[1]);
  }
  std::cout<<"Train File: "<<trainfile<<std::endl;

  std::string inputfname = ifilename; // "testsig.root";
  std::cout << "processing file: " << inputfname  << std::endl;
  std::cout << "output file: " << filename << std::endl;

  TFile* of1=new TFile(filename,"RECREATE");
  TTree* ot1=new TTree("vars","vars");
  
  gROOT->cd();
  
  TFile* f=new TFile(ifilename);
  
  char treename[256];
  
  //    sprintf(treename,"selected_%s",argv[2]);
  if (argc==5) sprintf(phase,"selected_%s",argv[4]);
  else sprintf(phase,"vars");

  //  TTree* t0=(TTree*)f->Get("selected");
  //    TTree* t0=(TTree*)f->Get(treename);
  
  TTree* t0=(TTree*)f->Get(phase);  
  
  gROOT->cd();
  
  std::cout << "check tree name: " << t0->GetName() << std::endl;
  std::cout << "nentries: " << t0->GetEntries() <<std::endl;
    
  TObjArray* brlist = t0->GetListOfBranches();
  
  Int_t brsize = brlist->GetSize(); 
  
  Float_t* fvar =new Float_t[brsize];
  Double_t* dvar= new Double_t[brsize];
  Int_t* ivar = new Int_t[brsize];
  Bool_t* bvar = new Bool_t[brsize];
  
  for (int kk=0; kk<brsize; kk++)
    {
      fvar[kk]=0;
      dvar[kk]=0;
      ivar[kk]=0;
      bvar[kk]=0;
    }
  
  for (int i=0; i<brsize; i++)
    {
      TBranch* br = (TBranch*) brlist->At(i);
      if (br!=0)
	{
      	  TObjArray* lflist = br->GetListOfLeaves();
	  Int_t lfsize = lflist->GetSize(); 
	  
	  if (lfsize>0)
	    {
	      TLeaf* lf = (TLeaf*) lflist->At(0);
	      std::string leaftype(lf->GetTypeName());
	      Int_t leaflen = lf->GetLenType();
	      //   std::cout << br->GetName() << " " << lf->GetTypeName() << " -> type:" << leaflen << std::endl;
	      std::string brname(br->GetName());
	      std::string lfname(br->GetName());
	      // assumo solo bool, int, float, double.
	      switch(leaflen)
		{
		case 1: // bool
		  lfname+="/O";
		  t0->SetBranchAddress(brname.c_str(),bvar+i);
		  ot1->Branch(brname.c_str(),bvar+i,lfname.c_str());
		  break;
		case 4: // int or float
		  if (leaftype.length() == 5)
		    { // int
		      lfname+="/I";
		      t0->SetBranchAddress(brname.c_str(),&ivar[i]);
		      ot1->Branch(brname.c_str(),&ivar[i],lfname.c_str());
		    } 
		  else
		    {
		      lfname+="/F";
		      t0->SetBranchAddress(brname.c_str(),&fvar[i]);
		      ot1->Branch(brname.c_str(),&fvar[i],lfname.c_str());
		    } // float          
		  break;
		case 8: // double
		  lfname+="/D";
		  t0->SetBranchAddress(brname.c_str(),&dvar[i]);
		  ot1->Branch(brname.c_str(),&dvar[i],lfname.c_str());
		  break;
		}
	    }
	}
    }

  //  SprAbsTrainedClassifier* rftrained = SprClassifierReader::readTrained("randforest.spr");
  SprAbsTrainedClassifier* rftrained = SprClassifierReader::readTrained(trainfile);
  assert(rftrained);
  
  //Getting MVA vars from same place as training...
  std::ifstream in;
  TString MVANAME=gSystem->Getenv("MVANAME");
  TString InFile="";
  if (MVANAME.Sizeof()>1) 
  {  
	  InFile+=gSystem->Getenv("RELEASE");
	  InFile+="/alphaMVA/";
	  InFile+=MVANAME;
	  InFile+="test_randforest.txt";
	  cout << InFile<<endl;
  }
  else 
  {
	  InFile=gSystem->Getenv("RELEASE");
	  InFile+="/alphaMVA/training/test_randforest.txt";
  }
  in.open(InFile.Data()); //use env vars later
  TString a;
  cout<<"scanning..." <<a<<endl;
  for (int i=0;i<10;i++) {
    a.ReadToken(in);//,' ');
    //cout << a;// << endl;
    if (a.Index("Leaves:")>=0) break;
    if (a.Index("Weight:")>=0) break;
  }
  cout << a << endl;
  if (a.Index("Weight:")>=0) return -1; // to early for this line... something has gone wrong
  std::vector<TString> vars;
  a.ReadToken(in);
  for (int i=0;i<50;i++){
    cout << "Pushing back:"<< a<< endl;
    vars.push_back(a);
    a.ReadToken(in);
    if (a.Index("Weight:")>=0) break;
  }
  cout << vars.size() << " total vars"<<endl;


  //Full list of dumper vars... if you update dumper... update this
  Double_t residual,residualL, z, phi, r, dca, cp;
  Double_t phi_S0axisraw, S0axisrawZ, S0raw, nhitsasymraw, S0rawPerp;
  Double_t phi_S0axis, S0axisZ, S0, nhitsasym;
  Double_t cosmChi2ProbNdof1;
  Int_t nhits, nCT, nGT, ntracks; 
  Bool_t vB[vars.size()];
  Int_t vI[vars.size()];
  Double_t vF[vars.size()];
  Double_t vD[vars.size()];
  
  Double_t rfout;

  ot1->Branch("rfout",&rfout,"rfout/D");
  //  ot1->SetBranchAddress("phi_S0axis",&phi_S0axis);
  //  ot1->SetBranchAddress("S0axisZ",&S0axisZ);
  // ot1->SetBranchAddress("S0",&S0);
  //  ot1->SetBranchAddress("nhitsasym",&nhitsasym);
 /* ot1->SetBranchAddress("phi_S0axisraw",&phi_S0axisraw);
  ot1->SetBranchAddress("S0axisrawZ",&S0axisrawZ);
  ot1->SetBranchAddress("S0raw",&S0raw);
  ot1->SetBranchAddress("nhitsasymraw",&nhitsasymraw);
  ot1->SetBranchAddress("residual",&residual); 
  ot1->SetBranchAddress("tracksdca",&dca);
  ot1->SetBranchAddress("nhits",&nhits);
  ot1->SetBranchAddress("phi",&phi);
  ot1->SetBranchAddress("cosphi",&cp);
  ot1->SetBranchAddress("r",&r);
  ot1->SetBranchAddress("nCT",&nCT);
  ot1->SetBranchAddress("nGT",&nGT);
  ot1->SetBranchAddress("S0rawPerp",&S0rawPerp);
*/
int vBc=0;
int vIc=0;
int vFc=0;
int vDc=0; //Counters for each data type
Int_t DataType[vars.size()];  //
for (int i=0; i<vars.size(); i++)
{
  //if (strcmp(vars[i].Data(),"phi_S0axisraw") t0->SetBrachAddress("phi_S0axisraw",&phi_S0axisraw)

	TLeaf* lf = (TLeaf*)t0->GetLeaf(vars[i].Data());
	std::string leaftype(lf->GetTypeName());
	Int_t leaflen = lf->GetLenType();
	switch(leaflen)
	{
	case 1: // bool
	  //lfname+="/O";
	  DataType[i]=0;
	  cout << i << ":Bool"<<endl;
	  t0->SetBranchAddress(vars[i].Data(),&vB[vBc]);
	  ot1->Branch(vars[i].Data(),&vB[vBc]);
	  vBc++;
	  break;
	case 4: // int or float
	  if (leaftype.length() == 5)
	    { // int
	      //lfname+="/I";
	      DataType[i]=1;
	      cout << i << ":Int"<<endl;
	      t0->SetBranchAddress(vars[i].Data(),&vI[vIc]);
	      ot1->Branch(vars[i].Data(),&vI[vIc]);
	      vIc++;
	    } 
	  else
	    {
	      //lfname+="/F";
	      DataType[i]=2;
	      cout << i << ":Float"<<endl;
	      t0->SetBranchAddress(vars[i].Data(),&vF[vFc]);
	      ot1->Branch(vars[i].Data(),&vF[vFc]);
	      vFc++;
	    } // float          
	  break;
	case 8: // double
	  //lfname+="/D";
	  DataType[i]=3;
	  cout << i << ":Double"<<endl;
	  t0->SetBranchAddress(vars[i].Data(),&vD[vDc]);
	      ot1->Branch(vars[i].Data(),&vD[vDc]);
	      vDc++;
	  break;
	}      
	
	//ot1->SetBranchAddress(vars[i].Data(),&v[i]);
	//t0->SetBranchAddress(vars[i].Data(),&v[i]);
}
cout <<"vBc:"<<vBc<<endl;
cout <<"vIc:"<<vIc<<endl;
cout <<"vFc:"<<vBc<<endl;
cout <<"vDc:"<<vDc<<endl;

  std::vector<double> input;
  //Reset counters

  for (int i=0; i<t0->GetEntries(); i++)
    {
	  if(i%5000==0) std::cout<<i<<"/"<<t0->GetEntries()<<std::endl;
      input.clear();
      t0->GetEntry(i);
      vBc=0;
      vIc=0;
      vFc=0;
      vDc=0;
      for (int j=0; j<vars.size(); j++)
      {
		  switch(DataType[j])
		  {
			case 0: //Bool_t
			input.push_back((double)vB[vBc]);
		
			//cout <<j<<":Bool:"<< vB[vBc] <<endl;
			vBc++;
			break;
			case 1: //Int_t
			//cout <<j<<":int:"<< vI[vIc] <<endl;
			input.push_back((double)vI[vIc]);
			
			vIc++;
			break;
			case 2: //Float_t
			input.push_back((double)vF[vFc]);
			//cout <<j<<":Float:"<< vF[vFc] <<endl;
			vFc++;
			break;
			case 3: //Double_t
			//cout <<j<<":Double:"<< vD[vDc] <<endl;
			input.push_back((double)vD[vDc]);
			vDc++;
			break;
		  }
//		  input.push_back(vD[j]);
		  //cout <<j<<":"<< vD[j] <<endl;
	  }


      rfout = rftrained->response(input);
      //cout <<"RFOUT:"<<rfout<<endl;
      ot1->Fill();
    }
  
  of1->cd();
  ot1->Write();
  of1->Close();
  
  delete rftrained;
  cout <<"I (applyRF.exe) have finished. I have a problem with a descrutor somewhere..."<<endl;
  cout <<"Therefor I hang when trying to exit cleanly... about to abort..."<<endl;
  abort();
  //return 1;
}


