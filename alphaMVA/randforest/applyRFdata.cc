#include "SprClassifierReader.hh"
#include "SprAbsTrainedClassifier.hh"



#include "../../alphaAnalysis/lib/include/TSeq_Event.h"
#include "../../alphaAnalysis/lib/include/TSisEvent.h"
#include "../../alphaAnalysis/lib/include/TSiliconEvent.h"
#include "../../alphaAnalysis/lib/include/TSISChannels.h"
#include "../../alphaAnalysis/lib/include/TTCEvent.h"
#include "../../alphaAnalysis/lib/include/TLabVIEWEvent.h"
//#include "../lib/include/SIS_Channels.h.alpha2"
#include "../../alphaAnalysis/lib/include/TVF48SiMap.h"
#include "../../alphavmc/include/TAlphaEvent.h"
#include "../../alphaAnalysis/lib/include/TAnalysisReport.h"


#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TLeaf.h"
#include "TString.h"

#include <stdlib.h> 
#include <iostream>
#include <string>

int main(int argc, char *argv[]) 
{ 
  char ifilename[128];
  char filename[128];
  char trainfile[128];

  if(argc==4) {
  //  if(argc==3)
    //	std::cout <<"Received 3 arguments.."<< std::endl;
    sprintf(ifilename,"mergeroot/%s",argv[1]);
    sprintf(filename,"rfdata/%s",argv[1]);
    sprintf(trainfile,"%s",argv[3]); }
  else { 
	sprintf(trainfile,"%s","randforest.spr");
    sprintf(ifilename,"mergeroot/%s",argv[1]);
    sprintf(filename,"rfdata/%s",argv[1]); }
  
  std::cout<<"Train File: "<<trainfile<<std::endl;
  std::string inputfname = ifilename; // "testsig.root";
  std::cout << "processing file: " << inputfname << std::endl;

  TFile* of1=new TFile(filename,"RECREATE");
  TTree* ot1=new TTree("vars","vars");
  gROOT->cd();
  
  TFile* f=new TFile(inputfname.c_str());
  char treename[25];
  sprintf(treename,"selected_%s",argv[2]);
  TTree* t0=(TTree*)f->Get(treename);
  //  TTree* t0=(TTree*)f->Get("selected");
  //  TTree* t0=(TTree*)f->Get("vars");
  gROOT->cd();

  std::cout << "check tree name: " << t0->GetName() << std::endl;
  
  std::cout << "nentries: " << t0->GetEntries() <<std::endl;
    
  TObjArray* brlist = t0->GetListOfBranches();
  
  Int_t brsize = brlist->GetSize(); 
  
  Float_t* fvar =new Float_t[brsize];
  Double_t* dvar= new Double_t[brsize];
  Int_t* ivar = new Int_t[brsize];
  Bool_t* bvar = new Bool_t[brsize];
  Char_t cvar[brsize][30];
  //  auto cvar = new Char_t[brsize][30]; // -std=c++11
  
  for (int kk=0; kk<brsize; kk++)
    {
      fvar[kk]=0;
      dvar[kk]=0;
      ivar[kk]=0;
      bvar[kk]=0;
    }
  
  for (int i=0; i<brsize; i++)
    {
      TBranch* br = (TBranch*) brlist->At(i);
      
      if (br!=0)
	{
      	  TObjArray* lflist = br->GetListOfLeaves();
	  Int_t lfsize = lflist->GetSize(); 
	  
	  if (lfsize>0)
	    {
	      TLeaf* lf = (TLeaf*) lflist->At(0);
	      std::string leaftype(lf->GetTypeName());
	      Int_t leaflen = lf->GetLenType();
         //	      std::cout << br->GetName() << " " << lf->GetTypeName() << " -> type:" << leaflen << " length " << leaftype.length()<<std::endl;
	      std::string brname(br->GetName());
	      std::string lfname(br->GetName());
	      // assumo solo bool, int, float, double. char*
	      switch(leaflen)
		{
		case 1: // bool or char
        if (!strcmp(leaftype.c_str(),"Bool_t")){ 
		    lfname+="/O";
		    t0->SetBranchAddress(brname.c_str(),bvar+i);
		    ot1->Branch(brname.c_str(),bvar+i,lfname.c_str());
		    break;
        }
          else { //char*
            lfname+="/C";
		      t0->SetBranchAddress(brname.c_str(),&cvar[i][0]);
		      ot1->Branch(brname.c_str(),&cvar[i][0],lfname.c_str());
		      break;
          }
		case 4: // int or float
		  if (leaftype.length() == 5)
		    { // int
		      lfname+="/I";
		      t0->SetBranchAddress(brname.c_str(),&ivar[i]);
		      ot1->Branch(brname.c_str(),&ivar[i],lfname.c_str());
		    } 
		  else
		    {
		      lfname+="/F";
		      t0->SetBranchAddress(brname.c_str(),&fvar[i]);
		      ot1->Branch(brname.c_str(),&fvar[i],lfname.c_str());
		    } // float          
		  break;
		case 8: // double
		  lfname+="/D";
		  t0->SetBranchAddress(brname.c_str(),&dvar[i]);
		  ot1->Branch(brname.c_str(),&dvar[i],lfname.c_str());
		  break;
		}
	    }
	}
    }

  //  SprAbsTrainedClassifier* rftrained = SprClassifierReader::readTrained("randforest.spr");
  SprAbsTrainedClassifier* rftrained = SprClassifierReader::readTrained(trainfile);
  assert(rftrained);
  
  Double_t residual, z, phi, r, dca, cp;
  Double_t phi_S0axisraw, S0axisrawZ, S0raw, nhitsasymraw, S0rawPerp;
  Double_t phi_S0axis, S0axisZ, S0, nhitsasym;
  Int_t nhits,  nCT, nGT; 
  
  Double_t rfout;

  ot1->Branch("rfout",&rfout,"rfout/D");
  //  ot1->SetBranchAddress("phi_S0axis",&phi_S0axis);
  //  ot1->SetBranchAddress("S0axisZ",&S0axisZ);
  // ot1->SetBranchAddress("S0",&S0);
  //  ot1->SetBranchAddress("nhitsasym",&nhitsasym);
  ot1->SetBranchAddress("phi_S0axisraw",&phi_S0axisraw);
  ot1->SetBranchAddress("S0axisrawZ",&S0axisrawZ);
  ot1->SetBranchAddress("S0raw",&S0raw);
  ot1->SetBranchAddress("nhitsasymraw",&nhitsasymraw);
  ot1->SetBranchAddress("residual",&residual); 
  ot1->SetBranchAddress("tracksdca",&dca);
  ot1->SetBranchAddress("nhits",&nhits);
  ot1->SetBranchAddress("phi",&phi);
  ot1->SetBranchAddress("cosphi",&cp);
  ot1->SetBranchAddress("r",&r);
  ot1->SetBranchAddress("nCT",&nCT);
  ot1->SetBranchAddress("nGT",&nGT);
  ot1->SetBranchAddress("S0rawPerp",&S0rawPerp);


  //  t0->SetBranchAddress("phi_S0axis",&phi_S0axis);
  //  t0->SetBranchAddress("S0axisZ",&S0axisZ);
  //  t0->SetBranchAddress("S0",&S0);
  //  t0->SetBranchAddress("nhitsasym",&nhitsasym);
  t0->SetBranchAddress("phi_S0axisraw",&phi_S0axisraw);
  t0->SetBranchAddress("S0axisrawZ",&S0axisrawZ);
  t0->SetBranchAddress("S0raw",&S0raw);
  t0->SetBranchAddress("nhitsasymraw",&nhitsasymraw);
  t0->SetBranchAddress("residual",&residual);
  t0->SetBranchAddress("tracksdca",&dca);
  t0->SetBranchAddress("nhits",&nhits);
  t0->SetBranchAddress("phi",&phi);
  t0->SetBranchAddress("cosphi",&cp);
  t0->SetBranchAddress("r",&r);
  t0->SetBranchAddress("nCT",&nCT);
  t0->SetBranchAddress("nGT",&nGT);
  t0->SetBranchAddress("S0rawPerp",&S0rawPerp);


  std::vector<double> input;
  for (int i=0; i<t0->GetEntries(); i++)
    {
      input.clear();
      t0->GetEntry(i);
      
      input.push_back(phi_S0axisraw);
      input.push_back(S0axisrawZ);
      //      input.push_back(S0raw);
      input.push_back(S0rawPerp);
      input.push_back(residual);
      //      input.push_back(dca);
      //      input.push_back(nhitsasymraw);
      input.push_back(nhits);
      input.push_back(phi);
      //      input.push_back(cp);
      input.push_back(r);
      input.push_back(nCT);
      input.push_back(nGT);
      
      rfout = rftrained->response(input);
      ot1->Fill();
    }
  
  of1->cd();
  ot1->Write();
  of1->Close();
  
  delete rftrained;
  
  return 0;
}


