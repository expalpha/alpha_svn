#!/bin/tcsh
g++  -Wno-deprecated -Wno-write-strings `root-config --cflags`  `root-config --glibs`  -L$ROOTSYS/lib -lXMLParser -lXMLIO -lThread -Wl,-rpath,$ROOTSYS/lib/root -DUSE_ALPHAVMC -lVMC -lGeom -lEG  -L$RELEASE/alphaAnalysis/lib -lAlphaAnalysis -L$RELEASE/alphavmc/lib/tgt_linuxx8664gcc -lalphavmc  -I`root-config --incdir` -I$RELEASE/alphaAnalysis/lib/include -I$RELEASE/alphavmc/include -o dumper.exe dumper.cc
setenv  LD_LIBRARY_PATH $RELEASE/alphaAnalysis/lib:$RELEASE/alphavmc/lib/tgt_linuxx8664gcc:$LD_LIBRARY_PATH

