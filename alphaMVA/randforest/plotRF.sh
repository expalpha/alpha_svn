#!/bin/bash

rm -f rfoutput/testRF.out

#RFout="RFout_sh_quench-sorted.txt"
#TrainedDir="sh_quench_trained"
#AppliedDir="$RELEASE/alphaMVA/randforest/sh_quench_RFapplied"
RFout="RFout_sh_hold-sorted.txt"
TrainedDir="sh_hold_trained"
AppliedDir="$RELEASE/alphaMVA/randforest/sh_hold_RFapplied"

for tf in `ls "$TrainedDir"`; do
    echo "@ train file: $tf"
    arch=$(echo $tf | sed 's/randforest//g')
    arch=$(echo $arch | sed 's/.spr//g')
    echo $arch
    patt=$(echo $arch | sed 's/^_//g' | sed 's/_/ /g')
    echo $patt
    rfcut=$(tail -10 ${RFout} | grep "$patt" | awk '{print $10}')
    rfcut=$(printf "%2.2f" $rfcut;)
    echo "rfcut:" $rfcut
    echo "------------------"   

    export SIGFILE=$AppliedDir/testsig${arch}.root
    echo $SIGFILE
    export BKGFILE=$AppliedDir/testbkg${arch}.root
    echo $BKGFILE

    cd rfoutput
    pwd
    root -q -b viewRFout.C\($rfcut,1\) > rfout${arch}.dat
    mv crf.png crf${arch}.png
    CUT=$(grep "RF selection: " viewRF.out)
    RATE=$(grep "Background Rate: " viewRF.out)
    ACC=$(grep "Signal Acceptance: " viewRF.out)
    echo $patt $CUT $RATE $ACC >> testRF.out
    rm viewRF.out    
    cp -r plots plots${arch}
    cd -

    echo "END"
    echo ""
done

unset SIGFILE
unset BKGFILE
rm -f rfoutput/plots/*
rm -f rfoutput/testsig.root
rm -f rfoutput/testbkg.root

cat rfoutput/testRF.out