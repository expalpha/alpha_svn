#!/bin/bash

if [ -z "$SPRfolder" ]
then
    echo "need to set SPR path"
    echo "source $RELEASE/alphaMVA/SPRconfig.sh"
    echo "exiting..."
    exit 123
fi

echo $SPRfolder

rm -f applyRFdata.exe
#g++ -Wno-deprecated `root-config --cflags` -L$SPRlib -lSPR `root-config --libs` -I$SPRinclude -I$SPRfolder applyRFdata.cc -o applyRFdata
make

#INFILE="merged_laser_trap312.list0.root"
#INFILE="../seldata/cosm_tree36810offline.root"
INFILE="merged_cosm_36811.list0.root"
TRAINF="trainedDTs/randforest_100_4_16.spr"

if [ -x applyRFdata ]; then
    echo "applyRF compiled!"
    echo "Running on " ${INFILE} " with " ${TRAINF}
    data=`echo $INFILE | sed 's/merged_\([a-z]*\)_.*/\1/'`
    echo "Run(s) Type: " ${data}
    ./applyRFdata.exe  mergeroot/${INFILE} rfdata/${data} ${TRAINF}
else
    echo "compilation failed"
    echo "exiting..."
    exit 123
fi

