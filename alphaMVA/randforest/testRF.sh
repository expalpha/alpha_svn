#!/bin/bash


#
RFout="myRFoutput-sorted.txt"
if [ -n "$MVANAME" ]; then
  TrainedDir=$RELEASE/alphaMVA/$MVANAME
  mkdir -p $RELEASE/alphaMVA/$MVANAME/rfoutput
  rm -f $RELEASE/alphaMVA/$MVANAME/rfoutput/testRF.out

  mkdir -p $RELEASE/alphaMVA/$MVANAME/rfoutput/plots
else
  cd $RELEASE/alphaMVA/randforest/
  #./compileRF.sh
  make
  rm -f rfoutput/testRF.out

  TrainedDir="trainedDTs"
fi
echo $TrainedDir
for tf in `ls "$TrainedDir"`; do
    if [[ ${tf##*.} = spr ]]; then  
    echo "@ train file: $tf"
    arch=$(echo $tf | sed 's/randforest//g')
    arch=$(echo $arch | sed 's/.spr//g')
    echo $arch
    patt=$(echo $arch | sed 's/^_//g' | sed 's/_/ /g')
    echo $patt
    if [ -n "$RATE_LIMIT" ]; then
      rfcut=$(tail -10 ${MVANAME}${RFout} | grep "$patt" | awk '{print $5}')
    else
      rfcut=$(tail -10 ${MVANAME}${RFout} | grep "$patt" | awk '{print $10}')
    fi
    rfcut=$(printf "%f" $rfcut;)
    if [ -n "$1" ]; then
      rfcut=$1
      echo "Forcing set rfcut:" $rfcut
    else
      echo "rfcut:" $rfcut
	fi
    echo "------------------"   

    export SIGFILE=testsig${arch}.root
    export BKGFILE=testbkg${arch}.root

    FILE="${MVANAME}splitroot/testsig.root "
    echo $FILE
    echo `ls -lh $FILE`
    echo `ls -lh  $TrainedDir/$tf`
    cd $RELEASE/alphaMVA
    if [ -n $MVANAME ]; then
      ${RELEASE}/alphaMVA/randforest/applyRF.exe ${FILE} ${MVANAME}rfoutput/${SIGFILE} ${TrainedDir}/${tf}
    else
      ${RELEASE}/alphaMVA/randforest/applyRF.exe ${FILE} randforest/rfoutput/${SIGFILE} ${TrainedDir}/${tf}
    fi
    #cp -v ${MVANAME}rfoutput/$FILE ${MVANAME}rfoutput/${SIGFILE}
    echo "------------------"
    FILE="${MVANAME}splitroot/testbkg.root"
    echo $FILE
    echo `ls -lh $FILE`
    if [ -n $MVANAME ]; then
      ${RELEASE}/alphaMVA/randforest/applyRF.exe ${FILE} ${MVANAME}rfoutput/${BKGFILE} ${TrainedDir}/${tf}
      cd ${MVANAME}rfoutput
    else
      ${RELEASE}/alphaMVA/randforest/applyRF.exe ${FILE} randforest/rfoutput/${BKGFILE} ${TrainedDir}/${tf}
      cd randforest/rfoutput
    fi
    #cp -v ${MVANAME}rfoutput/$FILE ${MVANAME}rfoutput/${BKGFILE}
    echo "------------------"
    
    pwd
    root -q -b $RELEASE/alphaMVA/randforest/rfoutput/viewRFout.C\($rfcut,1\) > rfout${arch}.dat
    mv crf.png crf${arch}.png
    CUT=$(grep "RF selection: " viewRF.out)
    RATE=$(grep "Background Rate: " viewRF.out)
    REJ=$(grep "Background Rejected: " viewRF.out)
    ACC=$(grep "Signal Acceptance: " viewRF.out)
    echo $patt $CUT $RATE $REJ $ACC >> testRF.out
    rm viewRF.out    
    cp -r plots plots${arch}
    cd -

    echo "END"
    echo ""
    fi
done

unset SIGFILE
unset BKGFILE
rm -f rfoutput/plots/*
rm -f rfoutput/testsig.root
rm -f rfoutput/testbkg.root
if [ -n $MVANAME ]; then
  cat ${RELEASE}/alphaMVA/${MVANAME}rfoutput/testRF.out
else
  cat ${RELEASE}/alphaMVA/randforest/rfoutput/testRF.out
fi
