#!/bin/bash


#
function RunTest {
  tf=$1
  TrainedDir="$2"
  RFout="${RELEASE}/alphaMVA/${MVANAME}myRFoutput-sorted.txt"
  TestLength=`echo $tf | grep randforest `
  if [ ${#TestLength} -ge 10 ]; then
    arch=$(echo $tf | sed 's/randforest//g')
    arch=$(echo $arch | sed 's/.spr//g')
    echo $arch
    mkdir -p ${RELEASE}/alphaMVA/${MVANAME}/rfoutput/plot${arch}
    patt=$(echo $arch | sed 's/^_//g' | sed 's/_/ /g')
    echo $patt
    if [ -n "$RATE_LIMIT" ]; then
      rfcut=$(tail -10 ${RFout} | grep "$patt" | awk '{print $5}')
    else
      rfcut=$(tail -10 ${RFout} | grep "$patt" | awk '{print $10}')
    fi
    rfcut=$(printf "%f" $rfcut;)
    if [ -n "$3" ]; then
      rfcut=$3
      echo "Forcing set rfcut:" $rfcut
    else
      echo "rfcut:" $rfcut
	fi
    echo "------------------"   

    export SIGFILE=testsig${arch}.root
    export BKGFILE=testbkg${arch}.root

    FILE="${RELEASE}/alphaMVA/${MVANAME}splitroot/testsig.root "
    echo $FILE
    echo `ls -lh $FILE`
    echo `ls -lh  $TrainedDir/$tf`
    cd $RELEASE/alphaMVA
    if [ -n $MVANAME ]; then
      ${RELEASE}/alphaMVA/randforest/applyRF.exe ${FILE} ${RELEASE}/alphaMVA/${MVANAME}rfoutput/${SIGFILE} ${TrainedDir}/${tf}
    else
      ${RELEASE}/alphaMVA/randforest/applyRF.exe ${FILE} ${RELEASE}/alphaMVA/randforest/rfoutput/${SIGFILE} ${TrainedDir}/${tf}
    fi
    #cp -v ${MVANAME}rfoutput/$FILE ${MVANAME}rfoutput/${SIGFILE}
    echo "------------------"
    FILE="${RELEASE}/alphaMVA/${MVANAME}splitroot/testbkg.root"
    echo $FILE
    echo `ls -lh $FILE`
    if [ -n $MVANAME ]; then
      ${RELEASE}/alphaMVA/randforest/applyRF.exe ${FILE} ${RELEASE}/alphaMVA/${MVANAME}rfoutput/${BKGFILE} ${TrainedDir}/${tf}
    else
      ${RELEASE}/alphaMVA/randforest/applyRF.exe ${FILE} ${RELEASE}/alphaMVA/randforest/rfoutput/${BKGFILE} ${TrainedDir}/${tf}
    #  cd randforest/rfoutput
    fi
    #cp -v ${MVANAME}rfoutput/$FILE ${MVANAME}rfoutput/${BKGFILE}
    echo "------------------"
    
    
          cd ${MVANAME}rfoutput

    if [ -n $MVANAME ]; then
    echo "     root -q -b $RELEASE/alphaMVA/randforest/rfoutput/viewRFout.C\($rfcut,1,\"${arch}\"\) > ${TrainedDir}/rfoutput/rfout${arch}.dat"
      root -q -b $RELEASE/alphaMVA/randforest/rfoutput/viewRFout.C\($rfcut,1,\"${arch}\"\) > ${TrainedDir}/rfoutput/rfout${arch}.dat
    else
      root -q -b $RELEASE/alphaMVA/randforest/rfoutput/viewRFout.C\($rfcut,1\) > rfout${arch}.dat
      mv crf.png crf${arch}.png
      CUT=$(grep "RF selection: " viewRF${arch}.out)
      RATE=$(grep "Background Rate: " viewRF${arch}.out)
      REJ=$(grep "Background Rejected: " viewRF${arch}.out)
      ACC=$(grep "Signal Acceptance: " viewRF${arch}.out)
      echo $patt $CUT $RATE $REJ $ACC >> testRF.out
      rm viewRF.out    
      cp -r plots plots${arch}
      cd -
    fi
  fi
}




if [ -n "$MVANAME" ]; then

  TrainedDir=$RELEASE/alphaMVA/$MVANAME
  mkdir -p $RELEASE/alphaMVA/$MVANAME/rfoutput
  rm -f $RELEASE/alphaMVA/$MVANAME/rfoutput/testRF.out

  mkdir -p $RELEASE/alphaMVA/$MVANAME/rfoutput/plots
else
RFout="myRFoutput-sorted.txt"
  cd $RELEASE/alphaMVA/randforest/
  #./compileRF.sh
  make
  rm -f rfoutput/testRF.out

  TrainedDir="trainedDTs"
fi
echo $TrainedDir
for tf in `ls -r "$TrainedDir"`; do
  if [[ ${tf##*.} = spr ]]; then  
  
  
    maxjobs=`nproc`
    jobcnt=(`jobs -p`)
    echo "Jobcount ${#jobcnt[@]}"
    if [ ${#jobcnt[@]} -lt $maxjobs ] ; then
      echo "@ train file: $tf"
      RunTest $tf "$TrainedDir" $1 & #$1 is a forced RF cut!!!
      sleep 1
    else
      while [ ${#jobcnt[@]} -ge $maxjobs ]
      do
        echo "$maxjobs jobs running... sleeping 30s"
        sleep 30
        jobcnt=(`jobs -p`)
      done
      echo "@ train file: $tf"
      RunTest $tf "$TrainedDir" $1 & #$1 is a forced RF cut!!!
      sleep 1
    fi
  fi
done
echo "Waiting for jobs to finish"
wait
echo "Bulding summary of testRF..."
    
for tf in `ls "$TrainedDir/rfoutput"`; do
  if [[ ${tf##*.} = out ]]; then  
    if [ -n $MVANAME ]; then
      cd ${RELEASE}/alphaMVA/${MVANAME}/rfoutput
      echo "@ train file: $tf"
      
      arch=$(echo $tf | sed 's/viewRF//g')
      arch=$(echo $arch | sed 's/.out//g')
      echo $arch
      patt=$(echo $arch | sed 's/^_//g' | sed 's/_/ /g')
      echo $patt
    
      CUT=$(grep "RF selection: " viewRF${arch}.out)
      RATE=$(grep "Background Rate: " viewRF${arch}.out)
      REJ=$(grep "Background Rejected: " viewRF${arch}.out)
      ACC=$(grep "Signal Acceptance: " viewRF${arch}.out)
      echo $patt $CUT $RATE $REJ $ACC >> testRF.out
    fi
    echo "END"
    echo ""
  fi
done
cat *.csv > testRF.csv

unset SIGFILE
unset BKGFILE
#~ rm -f rfoutput/plots/*
#~ rm -f rfoutput/testsig.root
#~ rm -f rfoutput/testbkg.root
if [ -n $MVANAME ]; then
  cat ${RELEASE}/alphaMVA/${MVANAME}rfoutput/testRF.out
else
  cat ${RELEASE}/alphaMVA/randforest/rfoutput/testRF.out
fi
