#!/bin/bash
set -e

if [ -e $RELEASE/alphaMVA/randforest/applyRF.exe ]; then
  echo "applyRF is compiled... probably ok"
else
  echo "$RELEASE/alphaMVA/randforest/compile.sh not run... cd do the following:
  
  cd $RELEASE/alphaMVA/randforest
  make

  "
  exit 1
fi



#source SPRconfig.sh
#RATE_LIMIT in Hz
if [ -z "$8" ]; then
 # export RATE_LIMIT=-1
 EAT_CAKE=1
else
  export RATE_LIMIT=$8
fi
EMAIL_RESULTS=0
MAILTO="your.email.address@cern.ch"
help(){
echo "
	Normal Usage:
	./runMyMVA \"SignalList.list\" \"PHASE\" \"BackgroundList.list\" \"PHASE\"
	example:
	./runMyMVA List/MixingList.list MIX List/CosmicRunList.list COSMICS
	
	
	Single line execution mode:
	./DoItAll SignalList PHASE BackgroundList PHASE SignalWeight BackgroundWeight StudyName
	
	optional 8th input: RATE (in Hz)
	"
}

MakeVarDumps(){
  #Select data and build new simpler trees: (slow)... space here for parrellisation...
  #Check if dumper is needed...
  echo "Checking that trees haven't already been made (delete them if you want to re-run dumper.C)"
  sleep 2
  AllTreesExist=1
  for i in `cat $SigList | uniq`
  do
    echo "$i"

    if [ -e ${MVATREES}/${Sigphase}_tree${i}${TREEPOSTFIX} ]; then
	#Do nothing, file exists... make a count of exisiting files here
	EATCAKE=1
	echo "${Sigphase}_tree${i}${TREEPOSTFIX} found in ${MVATREES} - Good"
    else
    if [ `eos ls /eos/experiment/alpha/DumperTrees/${Sigphase}_tree${i}${TREEPOSTFIX} | wc -c` -gt 20 ]; then
    echo "$i found on eos"
    else

      maxjobs=`nproc`
      jobcnt=(`jobs -p`)
      echo "Jobcount ${#jobcnt[@]}"
	  if [ ${#jobcnt[@]} -lt $maxjobs ] ; then
	    cd $RELEASE/alphaMVA/randforest/
	    if [ -e $TREEFILES/tree${i}${TREEPOSTFIX} ]; then
	      ./dumper.exe $TREEFILES/tree${i}${TREEPOSTFIX} $SigPHASE &
	    else
	      ./dumper.exe $EOS_MGM_URL//eos/experiment/alpha/alphaTrees/tree${i}${TREEPOSTFIX} $SigPHASE &
	    fi
	    cd -
      else
	    while [ ${#jobcnt[@]} -ge $maxjobs ]
		do
		  echo "$maxjobs jobs running... sleeping 30s"
	      sleep 30
		  jobcnt=(`jobs -p`)
		done
		 cd $RELEASE/alphaMVA/randforest/
		 if [ -e $TREEFILES/tree${i}${TREEPOSTFIX} ]; then
	      ./dumper.exe $TREEFILES/tree${i}${TREEPOSTFIX} $SigPHASE &
	    else
	      ./dumper.exe $EOS_MGM_URL//eos/experiment/alpha/alphaTrees/tree${i}${TREEPOSTFIX} $SigPHASE &
	    fi
	    cd -
	  fi
	fi
	fi
  done
  
  echo "Waiting for dumper of signal runs to finish..."
  wait 
  
  for i in `cat $BkgList | uniq`
  do
    if [ -e ${MVATREES}/${Bkgphase}_tree${i}${TREEPOSTFIX} ]; then
	#Do nothing, file exists... make a count of exisiting files here
	  EATCAKE=1
	  echo "${Bkgphase}_tree${i}${TREEPOSTFIX} found in ${MVATREES} - Good"
    else
    if [ `eos ls /eos/experiment/alpha/DumperTrees/${Bkgphase}_tree${i}${TREEPOSTFIX} | wc -c` -gt 20 ]; then
    echo "$i found on eos"
    else
	#AllTreesExist=0
	  maxjobs=`nproc`
      jobcnt=(`jobs -p`)
      echo "Jobcount ${#jobcnt[@]}"
	  if [ ${#jobcnt[@]} -lt $maxjobs ] ; then
	    cd ${RELEASE}/alphaMVA/randforest/
	    if [ -e ${TREEFILES}/tree${i}${TREEPOSTFIX} ]; then
	      ./dumper.exe ${TREEFILES}/tree${i}${TREEPOSTFIX} $BkgPHASE &
	    else
	      ./dumper.exe ${EOS_MGM_URL}//eos/experiment/alpha/alphaTrees/tree${i}${TREEPOSTFIX} $BkgPHASE &
	    fi
	    cd -
      else
	    while [ ${#jobcnt[@]} -ge $maxjobs ]
		do
		  echo "$maxjobs jobs running... sleeping 30s"
	      sleep 30
		  jobcnt=(`jobs -p`)
		done
		 cd ${RELEASE}/alphaMVA/randforest/
	    if [ -e ${TREEFILES}/tree${i}${TREEPOSTFIX} ]; then
	      ./dumper.exe ${TREEFILES}/tree${i}${TREEPOSTFIX} $BkgPHASE &
	    else
	      ./dumper.exe ${EOS_MGM_URL}//eos/experiment/alpha/alphaTrees/tree${i}${TREEPOSTFIX} $BkgPHASE &
	    fi
	    cd -
	  fi
	  fi
	  #cd $RELEASE/alphaMVA/randforest/
	  #./dumper.exe $TREEFILES/tree${i}${TREEPOSTFIX} $BkgPHASE
	  #cd -
    fi
  done
  
  echo "Waiting for dumper of cosmic runs to finish..."
  wait
  
}

MergeDumpFiles(){  

#Merge root files (note, PHASE now changes to phase...) (fast)
mkdir -p mergeroot
$RELEASE/alphaMVA/randforest/merge.sh $SigList $Sigphase "$RELEASE/alphaMVA/$MVANAME"
sleep 1
$RELEASE/alphaMVA/randforest/merge.sh $BkgList $Bkgphase "$RELEASE/alphaMVA/$MVANAME"

wait
}

SplitDumpFiles(){
	
  #SplitRootFiles (v fast)
  mkdir -p "${MVANAME}splitroot"

  #split the signal file
  if [ -n "$MVANAME" ]; then
    printf "\n===============================\n"
    echo " Splitting files into $MVANAME "
    printf "===============================\n\n"
    #echo "removing existing files if they exist..."
    #rm -rv "${MVANAME}splitroot/*"
    index=0
    while [ -a $RELEASE/alphaMVA/$MVANAME/merged_${Sigphase}_${SigListName}${index}.root ]; do
      ls  $RELEASE/alphaMVA/$MVANAME/merged_${Sigphase}_${SigListName}${index}.root
      index=$((index+1))
    done
    index=$((index-1))
    if [ $index -gt 0 ]; then
      echo "merged output for this signal list exists... using new one with index: $index"
    fi
    File1="$RELEASE/alphaMVA/$MVANAME/merged_${Sigphase}_${SigListName}${index}.root"
    root -b -q -l '$RELEASE/alphaMVA/randforest/splitfile.C("'${File1}'",1)' #Signal splitter
    echo "Signal split... removing merged file for space..."
    rm $File1

    index=0
    while [ -a $RELEASE/alphaMVA/$MVANAME/merged_${Bkgphase}_${BkgListName}${index}.root ]; do
      ls $RELEASE/alphaMVA/$MVANAME/merged_${Bkgphase}_${BkgListName}${index}.root
      index=$((index+1))
    done
    index=$((index-1))
    if [ $index -gt 0 ]; then
      echo "merged output for this background exists... using new one with index: $index"
    fi
    File2="$RELEASE/alphaMVA/$MVANAME/merged_${Bkgphase}_${BkgListName}${index}.root"
    root -b -q -l '$RELEASE/alphaMVA/randforest/splitfile.C("'${File2}'",0)' #Signal splitter
    echo "Signal split... removing merged file for space..."
    rm $File2
  else #else case shouldn't happen
    echo "Splitter failed..."
    exit
  fi
}

source ../myconfig.sh
if [ -z "$RELEASE" ]; then
  echo "RELEASE not set... did you source myconfig.sh? "
  exit 
fi



export SigList=$1
export SigPHASE=$2
export BkgList=$3
export BkgPHASE=$4


##CHECK INPUTs:
if [ -z "$SigList" ]; then
    echo "You need to set Signal List"
    help
    exit 1
fi  
echo "Signal data list: $SigList" 

if [ -z "$SigPHASE" ]; then
    echo "You need to set Signal PHASE"
    help
    exit 1
fi  
echo "Signal PHASE: $SigPHASE" 

if [ -z "$BkgList" ]; then
    echo "You need to set Background List"
    help
    exit 1
fi  
echo "Background data list: $BkgList" 

if [ -z "$BkgPHASE" ]; then
    echo "You need to set Background PHASE"
    help
    exit 1
fi  
echo "Background PHASE: $BkgPHASE" 

echo "all seem valid... testing for recognised phase..."

case "$SigPHASE" in #See inside dumper.C for this list... there may be additions...
COSMICS  )	export Sigphase="cosm" ;;
ANTIDUMP )  export Sigphase="antidump" ;;
MIX  )		export Sigphase="mix" ;;
MIX2  )		export Sigphase="mix2" ;;
WAIT  )		export Sigphase="wait" ;;
QUENCH  )	export Sigphase="quench" ;;
BKG  )		export Sigphase="bkg" ;;
all  )		export Sigphase="all" ;;
UV  )		export Sigphase="laser" ;;
muW  )		export Sigphase="muW" ;;
STOCH  )	export Sigphase="stoch" ;;
*  )	echo "Signal PHASE not recognised... if its new edit this bash script and dumper.C"
		help 
		exit 1;;
esac
echo "PHASE \"$SigPHASE\" ok"


case "$BkgPHASE" in #See inside dumper.C for this list... there may be additions...
COSMICS  )	export Bkgphase="cosm" ;;
ANTIDUMP )	export Bkgphase="antidump" ;;
MIX  )		export Bkgphase="mix" ;;
MIX2  )		export Bkgphase="mix2" ;;
WAIT  )		export Bkgphase="wait" ;;
QUENCH  )	export Bkgphase="quench" ;;
BKG  )		export Bkgphase="bkg" ;;
all  )		export Bkgphase="all" ;;
UV  )		export Bkgphase="laser" ;;
muW  )		export Bkgphase="muW" ;;
STOCH  )	export Bkgphase="stoch" ;;
*  )	echo "Background PHASE not recognised... if its new edit this bash script and dumper.C"
		help 
		exit 1;;
esac
echo "PHASE \"$BkgPHASE\" ok"


export SigListName="$(basename "$SigList")"
export BkgListName="$(basename "$BkgList")"

if [ -z "$5" ]; then
  echo "Please give weight of signal (eg 0.0002)"
  read SigWeight 
else
  SigWeight=$5
fi

if [ -z "$6" ]; then
  #echo "Please give weight of background (eg 0.00003)"
  echo "Please give the length of the background window for whole experiment eg 1 (second)"
  read BkgWeight 
else
  BkgWeight=$6
fi

if [ -z "$7" ]; then 
  echo "Please give a name for this study (no spaces):"
  echo " Previous Examples: "
  ls MVAStudies/
  read MVANAME
  MVANAME="MVAStudies/${MVANAME}/"
  if [[ "$MVANAME" =~ \ |\' ]]    #  slightly more readable: if [[ "$string" =~ ( |\') ]]
  then
    echo "Study name must not include spaces! Starting again..."
  exit 1
  fi

  export MVANAME="$MVANAME"
  echo "Please give a description of this MVA study (this will go into a text file inside your study folder named above)"
  read MVADescription
else
  export MVANAME="MVAStudies/${7}/"
  MVADescription="One line execution... no description given"
fi
mkdir -p "$MVANAME"
echo "
Signal List: $SigListName
Signal PHASE: $SigPHASE
Signal phase: $Sigphase
Signal weight: $SigWeight
Background List: $BkgListName
Background PHASE: $BkgPHASE
Background phase: $Bkgphase
Background weight: $BkgWeight

Rate Limit: $RATE_LIMIT

Description: 
$MVADescription

=========================================

" &>  "${MVANAME}/StudyDescription.txt"
cp $BkgList "${MVANAME}/${BkyListName}"
cp $SigList "${MVANAME}/${SigListName}"
##Check input done

MakeVarDumps  #Function that make variable dump files... if they exist already, they are skipped

echo "SigList: $SigList"
echo "Sigphase: $Sigphase"
echo "BkgList: $BkgList"
echo "Bkgphase: $Bkgphase"

MergeDumpFiles  #Function that merges dump files

BkgBASENAME=`basename $BkgList`
SigBASENAME=`basename $SigList`
BackgroundEvents=`grep 'entries to merge' $MVANAME/$BkgBASENAME.log | sed 's/[^0-9]//g'`
SignalEvents=`grep 'entries to merge' $MVANAME/$SigBASENAME.log | sed 's/[^0-9]//g'`


echo "De-normalising signal weight (Weight / Events): $SigWeight / $SignalEvents"
SigWeight=`echo " $SigWeight / $SignalEvents / 3. " | bc -l`
echo "= $SigWeight"

echo "De-normalising background weight (Weight / Events): $BkgWeight / $BackgroundEvents"
BkgWeight=`echo " $BkgWeight / $BackgroundEvents / 3. " | bc -l`
echo "= $BkgWeight"

SplitDumpFiles #Function to split root files into opt, test and val

##TRAINING:
printf "\n==============================\n"
echo "  Beginning training  "
printf "==============================\n\n"


MVAMODELIST=( "opt" "test" "val" ) 
for MVAMODE in "${MVAMODELIST[@]}"
do
echo "Setting up training: $MVAMODE"
echo "Tree: vars
Leaves: phi_S0axisraw S0axisrawZ S0rawPerp residual nhits phi r nCT nGT
Weight: $SigWeight
File: $RELEASE/alphaMVA/${MVANAME}/splitroot/${MVAMODE}sig.root 0- 1
Weight: $BkgWeight
File: $RELEASE/alphaMVA/${MVANAME}/splitroot/${MVAMODE}bkg.root 0- 0
" > ${MVANAME}/${MVAMODE}_randforest.txt
cat ${MVANAME}/${MVAMODE}_randforest.txt >> "${MVANAME}/StudyDescription.txt"
done


cd ${RELEASE}/alphaMVA/
$RELEASE/alphaMVA/training/scantrainRF_parrellel.sh
echo "Done training..."
cat ${RELEASE}/alphaMVA/${MVANAME}myRFoutput.txt

sleep 10
echo "Sorting (sortMyRFout.sh)..."
cd $RELEASE/alphaMVA/$MVANAME
$RELEASE/alphaMVA/training/sortMyRFout.sh

cat ${RELEASE}/alphaMVA/${MVANAME}myRFoutput-sorted.txt

sleep 10
cd ${RELEASE}/alphaMVA/
echo "Testing RF..."
${RELEASE}/alphaMVA/randforest/testRF_parrellel.sh

cd $RELEASE/alphaMVA
BESTPIC=`tail -1 ${MVANAME}myRFoutput-sorted.txt | awk '{print "rfoutput/crf_"$1"_"$2"_"$3".png"}'`
echo "Bestpic: $BESTPIC"
if [ $EMAIL_RESULTS -eq 1 ]; then
  cat ${MVANAME}StudyDescription.txt ${MVANAME}rfoutput/testRF.out | mailx -s "$MVANAME" -a "${MVANAME}/${BESTPIC}" -a "${MVANAME}rfoutput/testRF.csv" $MAILTO
fi

echo "Deleteing bulky .spr and root files"
rm -rv $RELEASE/alphaMVA/$MVANAME/RFout/*.spr
rm -rv $RELEASE/alphaMVA/$MVANAME/rfoutput/*.root
rm -rv $RELEASE/alphaMVA/$MVANAME/WriterOut/*.root
rm -rv $RELEASE/alphaMVA/$MVANAME/splitroot/*.root
