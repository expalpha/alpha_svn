#!/bin/bash

if [ ${#1} -lt 3 ]; then
echo "No input file..."
exit
fi

export STRIPFILES=${RELEASE}/alphaMVA/stripNN/data/MixingTemp
mkdir -p ${STRIPFILES}
for i in `cat ${1}`; do
  cd ${RELEASE}/alphaAnalysis/macros/
  if [ -e ${STRIPFILES}/alphaStrips${i}offline.root ]; then
  echo "${i} already exists"
  else
  echo ".L PlotDumps.C+
  DumpMixingADCtree(${i})
  .q
  "|root -l -b
  fi
  cd -
done
echo "Merging mixing data"
root -l -b -q fastMerge.C'("'${1}'",1,"data/merged_MIX_'`basename $1`'.root")' 2>&1 | tee -a MIX_merge.log
echo "Cleaning up temporary files"
for i in `cat ${1}`; do
  rm -v data/MixingTemp/alphaStrips${i}offline.root
done

echo "Shuffling mixing data"
root -l -b -q shuffle.C'("data/merged_MIX_'`basename $1`'.root","data/shuffled_merged_MIX_'`basename $1`'.root")' 2>&1 | tee -a MIX_shuffle.log
#
rm -v 'data/merged_MIX_'`basename $1`'.root'

export STRIPFILES=${RELEASE}/alphaMVA/stripNN/data/PreCatchTemp
mkdir -p ${STRIPFILES}
for i in `cat ${1}`; do
  cd ${RELEASE}/alphaAnalysis/macros/
  
  if [ -e ${STRIPFILES}/alphaStrips${i}offline.root ]; then
  echo "${i} already exists"
    sleep 0.1
  else
  echo ".L PlotDumps.C+
   DumpPreCatchADCtree(${i})
  .q
  "|root -l -b
  fi
  cd -
done


export STRIPFILES=${RELEASE}/alphaMVA/stripNN/data/PostTrapTemp
mkdir -p ${STRIPFILES}
for i in `cat ${1}`; do
  cd ${RELEASE}/alphaAnalysis/macros/
  if [ -e ${STRIPFILES}/alphaStrips${i}offline.root ]; then
  echo "${i} already exists"
  sleep 0.1
  else
  echo ".L PlotDumps.C+
   DumpPostTrappingCosmicLongTree(${i})
  .q
  "|root -l -b
  fi
  cd -
done
echo "Merging background data"
root -l -b -q fastMerge.C'("'${1}'",0,"data/merged_BKG_'`basename $1`'.root")' 2>&1 | tee -a BKG_merge.log
for i in `cat ${1}`; do
  rm -v data/PreCatchTemp/alphaStrips${i}offline.root
  rm -v data/PostTrapTemp/alphaStrips${i}offline.root
done
echo "Shuffling background data"
root -l -b -q shuffle.C'("data/merged_BKG_'`basename $1`'.root","data/shuffled_merged_BKG_'`basename $1`'.root")' 2>&1 | tee -a BKG_shuffle.log
echo "Cleaning up temporary files"

rm -v 'data/merged_BKG_'`basename $1`.root
