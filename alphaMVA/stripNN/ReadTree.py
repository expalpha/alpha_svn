#!/usr/bin/env python

########################################
# Imports
########################################

import ROOT
import resource
import numpy as np

print ROOT

########################################
# Event loop
########################################
def get_data(infile="data/merged_MIX_test.list.root", ibatchsize = 1000, start_point=0, ):

  #Allocate memory for data
  data=np.zeros((ibatchsize,36864),dtype=np.float)
  #Open input file
  input_root_file =  ROOT.TFile.Open(infile)
  #Get the tree containing the ADC data
  input_tree = input_root_file.Get('ADCRawTree')
  
  #Count the entries in the tree once:
  n_entries = input_tree.GetEntriesFast() 
  #Check we wont read past the end of the file
  if (n_entries<(start_point+ibatchsize)):
    print "Error: requested data exceeds size of file"
    return
  
  #Get the data over from 'start_point' for 'ibatchsize' entries
  for idx, i_event in enumerate(range(start_point,start_point+ibatchsize)):
    input_tree.GetEntry( i_event )
    #input_tree.ADC is the i_event'th entry in the tree file :)
    #Add data as row in array
    data[idx,:]=(np.array(input_tree.ADC) - 511) / 511
  #Close the root file
  input_root_file.Close()
  #Return the loaded data
  return data


