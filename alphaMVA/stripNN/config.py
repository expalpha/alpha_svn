import logging
import os
import pprint
import sys
import yaml


SAVED_MATRICES_DIR = "savedMatrices"


class LayerConfig(object):
    """ Configuration describing one layer in a neural network.

    A layer is a vector of neurons, each with some number of inputs and a
    single output.  The function producing the output is the node's type.
    """
    # The number of nodes in the layer
    nodeCount = 0
    # The type of each node in the layer (activation function of tanh or Rectified Linear Unit)
    typ = 'relu'

    def __init__(self,
                 nodeCount=0,
                 typ='relu'):
        self.nodeCount = nodeCount
        self.typ = typ


class MomentumConfig(object):
    """ Configuration describing momentum during weight updates.

    Momentum is a technique for avoiding local minima by increasing weight
    updates when they've all been in the same direction for a while. A
    fraction of the previous weight update is applied to each subsequent
    weight update, making the algorithm "gain momentum" when it's
    travelling down a long descent, and "lose momentum" when the gradient
    reverses.
    """
    enabled = False
    # The fraction of the previous weight update to re-apply
    fraction = 0.0

    def __init__(self,
                 enabled=False,
                 fraction=0.0):
        self.enabled = enabled
        self.fraction = fraction


class DropoutConfig(object):
    """ Configuration describing when neurons should be "dropped out".

    Dropout is a technique for combatting overfitting and averaging across
    models by "dropping out" random nodes for each training sample.

    Nodes are dropped with a certain probability described below, and at
    the end, all weights are multiplied by this value to get their expected
    value.
    """
    enabled = False
    # The probability of a node being dropped out
    probability = 0.0

    def __init__(self,
                 enabled=False,
                 probability=0.0):
        self.enabled = enabled
        self.probability = probability


#TODO(jfriedly):  Figure out how this works and document it
class RegularizationConfig(object):
    """ Configuration describing L2 regularization.

    Weight decay is a technique for combatting overfitting that makes all
    weights in the model decay exponentially towards zero. With this
    constraint, networks become less flexible (they have to "keep exercising"
    their neurons or they decay).

    The decay factor is simply multiplied by the current value of the weight
    matrix and subtracted from it. Multiple decay factors (for each layer)
    are supported.
    """
    enabled = False
    # L2 regularization on panel layers
    # (needs panel layer count + 1 values)
    sharedReg = []
    # L2 regularization on post-panel layers
    # (needs post-panel layer count + 1 values)
    nonsharedReg = []

    def __init__(self,
                 enabled=False,
                 sharedReg=[],
                 nonsharedReg=[]):
        self.enabled = enabled
        self.sharedReg = sharedReg
        self.nonsharedReg = nonsharedReg


class ReluRespawnConfig(object):
    """ Configuration describing Rectified Linear Unit respawning.

    ReLU nodes in a neural network can "die" when they get a weight so low
    that they just always return zero, regardless of the input.  These nodes
    also stop transmitting gradients backwards.
    If this happens too much, (more than respawnCutoff times in a row), we
    tried "respawning" the dead nodes and making them return random values
    (but also setting the weights on them to zero).  It didn't really help
    much and isn't recommended.

    If all layers in a network use the tanh activation function, this config
    is meaningless.
    """
    enabled = False
    # Respawn nodes after they've returned zero this many times in a row
    respawnCutoff = 1000

    def __init__(self,
                 enabled=False,
                 respawnCutoff=1000):
        self.enabled = enabled
        self.respawnCutoff = respawnCutoff


#TODO(jfriedly):  Delete redundant params, e.g. number of layers
class TwoStageNNConfig(object):
    """ Configuration describing a two-stage neural network.

    ALPHA's raw silicon detector data comes from 72 silicon strips (aka 
    "panels"), each of which has 512 vertical and 512 horizontal voltage
    indicators. With this many inputs, it was decided to not use a fully-
    connected graph, but to instead have two stages of neural networks.
    The first stage is a small independent network for each panel, and
    the outputs of these networks are then merged into a second-stage network
    which ultimately results in a final binary classification (cosmic ray or
    annihilation).
    """
    # The number of hidden layers in the first stage, acting on each panel
    sharedHiddenLayerCount = 0
    # A list of LayerConfig objects describing hidden layer topology of the
    # first stage networks.
    sharedHiddenLayers = []

    # The number of outputs for each panel's network. These are the inputs
    # to the second stage
    sharedOuts = 0
    # The type of the output (tanh or relu for Rectified Linear Units)
    sharedOutputType = "relu"

    # The number of hidden layers in the second stage, acting on the outputs
    # above
    hiddenLayerCount = 0
    # A list of LayerConfig objects describing hidden layer topology of the
    # second stage network.
    postPanelHiddenLayers = []

    # The maximum number of training samples
    maxTrainings = 100000000
    # An initial factor to scale all the training rates by.
    initialFactor = 1.0
    # Learning rates -- set by method below
    lRates = []

    momentum = MomentumConfig()
    dropout = DropoutConfig()
    regularization = RegularizationConfig()
    reluRespawn = ReluRespawnConfig()

    # A file that can be followed to monitor training progress
    stdoutfilename = ""
    # A dir inside savedMatrices to save neural networks and matrices to
    outputFolder = ""
    # Whether or not to save all matrices, or just the final ones
    saveAll = False
    # The number of training epochs (times through the entire training set)
    trainingEpochs = 1000

    def __init__(self,
                 sharedHiddenLayerCount=0,
                 sharedHiddenLayers=[],
                 sharedOuts=0,
                 sharedOutputType="relu",
                 hiddenLayerCount=0,
                 postPanelHiddenLayers=[],
                 maxTrainings=100000000,
                 initialFactor=1.0,
                 lRates=[],
                 momentum=dict(),
                 dropout=dict(),
                 regularization=dict(),
                 reluRespawn=dict(),
                 stdoutfilename="",
                 outputFolder="",
                 saveAll=False,
                 trainingEpochs=1000):
        self.sharedHiddenLayerCount     = sharedHiddenLayerCount
        # sharedHiddenLayers will be read from config as a list of dicts
        self.sharedHiddenLayers         = [LayerConfig(**layer) for layer in sharedHiddenLayers]
        self.sharedOuts                 = sharedOuts
        self.sharedOutputType           = sharedOutputType
        self.hiddenLayerCount           = hiddenLayerCount
        # postPanelHiddenLayers will be read from config as a list of dicts
        self.postPanelHiddenLayers      = [LayerConfig(**layer) for layer in postPanelHiddenLayers]
        self.maxTrainings               = maxTrainings
        self.initialFactor              = initialFactor
        # self.lRates is deliberately ommitted; it will be set below
        self.momentum                   = MomentumConfig(**momentum)
        self.dropout                    = DropoutConfig(**dropout)
        self.regularization             = RegularizationConfig(**regularization)
        self.reluRespawn                = ReluRespawnConfig(**reluRespawn)
        self.stdoutfilename             = stdoutfilename
        # TODO(jfriedly):  Verify that this is actually the desired behavior...
        self.outputFolder               = os.path.join(SAVED_MATRICES_DIR, outputFolder)
        self.saveAll                    = saveAll
        self.trainingEpochs             = trainingEpochs
        self._setLearningRates()
        logging.debug("Loaded config:\n%s", pprint.pformat(vars(self)))

    def _setLearningRates(self):
        if self.sharedHiddenLayerCount==1 and self.hiddenLayerCount==1:
            # fully tested
            self.lRates = [self.initialFactor*baseRate for baseRate in [0.0003125, 0.17855, 0.3125, 0.08332]]
        elif self.sharedHiddenLayerCount==2 and self.hiddenLayerCount==1:
            #                                              un      un       tested tested    tested
            self.lRates = [self.initialFactor*baseRate for baseRate in [0.000218, 0.168345, 0.2928, 0.0074, 0.0101]]
        elif self.sharedHiddenLayerCount==1 and self.hiddenLayerCount==2:
            #                                   untested, untested, tested, halved cuz i said so, tested
            self.lRates = [self.initialFactor*baseRate for baseRate in [0.000245, 0.0854, 0.2339, 0.4, 0.00658]]
        elif self.sharedHiddenLayerCount==2 and self.hiddenLayerCount==2:
            self.lRates = [self.initialFactor*baseRate for baseRate in [.0002324,.2161,.3689,.3069,.00783,.01127]]
        else:
            logging.error("Im gonna error out now because I don't know learning rates for this setup")
            sys.exit(1)


def getConfig(args):
    """ Reads NN parameters from a config file, or falls back to interactive.

    :param args: parsed arguments from argparse.parse_args
    :returns: an TwoStageNNConfig object
    """
    if args.configFile:
        with args.configFile:
            logging.debug("Loading config from: %s", args.configFile)
            return TwoStageNNConfig(**yaml.load(args.configFile))
    else:
        logging.debug("No config file found. Falling back to interactive")
        return interactiveConfig()


def interactiveConfig():
    """ Interactively prompts for NN parameters

    :returns:  an TwoStageNNConfig object
    """
    sharedHiddenLayerCount = int(raw_input("How many panel hidden layers? "))
    sharedHiddenLayers = []
    for i in range(sharedHiddenLayerCount):
        typ = str(raw_input("layer "+str(i)+" panel hidden layer type (tanh or relu): "))
        count = int(raw_input("layer "+str(i)+" panel hidden layer hidden unit count: "))
        sharedHiddenLayers.append({"nodeCount": count, "typ": typ})
    sharedOuts=int(raw_input("Number of outputs per panel: "))
    sharedOutputType=str(raw_input("Panel output type (tanh or relu): "))

    hiddenLayerCount=int(raw_input("How many post-panel hidden layers? "))
    postPanelHiddenLayers=[]
    for i in range(hiddenLayerCount):
        typ = raw_input("layer "+str(i)+" post-panel hidden layer type (tanh or relu): ")
        count = int(raw_input("layer "+str(i)+" post-panel hidden layer hidden unit count: "))
        postPanelHiddenLayers.append({"nodeCount": count, "typ": typ})

    maxTrainings=int(int(raw_input("maximum number of training samples (enter 100000000 for no limit): "))/2)

    print("suggestion: enter .25 for full dataset training, .5 for training over 10000 samples")
    initialFactor = float(raw_input("factor to multiply training rate by: "))

    wd=raw_input("Do you want to use momentum? (y/n): ")
    momentum={"enabled": False, "fraction": 0.0}
    if wd[0]=="y":
        momentum["enabled"]=True
        momentum["fraction"]=float(raw_input("Momentum fraction: "))

    dr = raw_input("Do you want to use dropout? (y/n): ")
    dropout={"enabled": False, "probability": 0.0}
    if dr[0]=="y":
        dropout["enabled"]=True
        dropout["probability"]=float(raw_input("Dropout probability: "))

    re=raw_input("Do you want to use l2 regularization? (y/n): ")
    regularization = {"enabled": False,
            "sharedReg": [0 for i in range(sharedHiddenLayerCount+1)],
            "nonsharedReg": [0 for i in range(hiddenLayerCount+1)]
    }
    if re[0]=="y":
        regularization["enabled"]=True
        for i in range(len(regularization["sharedReg"])):
            regularization["sharedReg"][i]=float(raw_input("l2 regulariztion on panel layer "+str(i)+": "))
        for i in range(len(regularization["nonsharedReg"])):
            regularization["nonsharedReg"][i]==float(raw_input("l2 regulariztion on post-panel layer "+str(i)+": "))

    reluresp=raw_input("Do you want to use relu respawn (y/n): ")
    reluRespawn={"enabled": False, "respawnCutoff": 1000}
    if reluresp[0]=="y":
        reluRespawn["enabled"]=True
        reluRespawn["respawnCutoff"]=int(raw_input("relu nodes respawn if they return 0 more than this many times: "))

    stdoutfilename=raw_input("stdout replacement filename: ")

    outputFolder = raw_input("folder inside savedMatrices to save to: ")

    saveEach=raw_input("do you want to save all matrices (y/n)? ")
    saveAll=False
    if saveEach[0]=='y':
        saveAll=True

    trainingEpochs=int(raw_input("how many training epochs do you want? "))

    return TwoStageNNConfig(sharedHiddenLayerCount=sharedHiddenLayerCount,
            sharedHiddenLayers=sharedHiddenLayers,
            sharedOuts=sharedOuts,
            sharedOutputType=sharedOutputType,
            hiddenLayerCount=hiddenLayerCount,
            postPanelHiddenLayers=postPanelHiddenLayers,
            maxTrainings=maxTrainings,
            initialFactor=initialFactor,
            momentum=momentum,
            dropout=dropout,
            regularization=regularization,
            reluRespawn=reluRespawn,
            stdoutfilename=stdoutfilename,
            outputFolder=outputFolder,
            saveAll=saveAll,
            trainingEpochs=trainingEpochs
    )
