import logging
import numpy as np
import random
import math
import sys
import os
import random as rand
import ROOT
import resource
import os
import os.path
import argparse

import config


logging.basicConfig(level=logging.DEBUG)


def parseArgs():
    """ Parses command-line arguments.

    :returns: an argparse Namespace object with formatted arguments
    """
    parser = argparse.ArgumentParser(description="Use a neural network to identify cosmic rays")
    parser.add_argument("--configFile", "-c", type=argparse.FileType('r'))
    parser.add_argument("--annihilationData", "-a", required=False, default="/eos/experiment/alpha/stripsNN/shuffled_merged_MIX_SeptMix2.list.root")
    parser.add_argument("--backgroundData", "-b", required=False, default="data/shuffled_merged_BKG_SeptMix2.list.root")
    args = parser.parse_args()

    for arg, value in sorted(vars(args).items()):
        logging.debug("Got argument %s: %r", arg, value)

    return args


def getData(input_tree, averages,ibatchsize = 1000, start_point=0):
    data=np.zeros((ibatchsize,36864),dtype=np.float)
    n_entries = input_tree.GetEntriesFast() 
    if (n_entries<(start_point+ibatchsize)):
        print("Error: requested data exceeds size of file")
        return
    for idx, i_event in enumerate(range(start_point,start_point+ibatchsize)):
        input_tree.GetEntry( i_event )
        data[idx,:]=(np.array(input_tree.ADC) - averages) / 512.0
    return data


def getTrainingBatch(filesCount,\
    input_tree_mixing,
    input_tree_cosmic,
    trainingAbsoluteStartPointMixing,\
    trainingCurrentStartPointMixing,\
    trainingAbsoluteEndPointMixing,\
    trainingAbsoluteStartPointCosmic,\
    trainingCurrentStartPointCosmic,\
    trainingAbsoluteEndPointCosmic,averages):
    if trainingCurrentStartPointMixing+int(filesCount/2)<trainingAbsoluteEndPointMixing:
        mixingdata=getData(input_tree_mixing,averages, ibatchsize = int(filesCount/2),start_point=trainingCurrentStartPointMixing)
        trainingCurrentStartPointMixing+=int(filesCount/2)
    else:
        if trainingAbsoluteEndPointMixing-trainingCurrentStartPointMixing!=0:
            mixingdata=getData(input_tree_mixing,averages, ibatchsize = trainingAbsoluteEndPointMixing-trainingCurrentStartPointMixing,start_point=trainingCurrentStartPointMixing)
        mixingdata=np.append(mixingdata,getData(input_tree_mixing,averages, ibatchsize = int(filesCount/2)-(trainingAbsoluteEndPointMixing-trainingCurrentStartPointMixing),start_point=trainingAbsoluteStartPointMixing),axis=0)
        trainingCurrentStartPointMixing=trainingAbsoluteStartPointMixing+int(filesCount/2)-(trainingAbsoluteEndPointMixing-trainingCurrentStartPointMixing)

    if trainingCurrentStartPointCosmic+int(filesCount/2)<trainingAbsoluteEndPointCosmic:
        cosmicdata=getData(input_tree_cosmic,averages, ibatchsize = int(filesCount/2),start_point=trainingCurrentStartPointCosmic)
        trainingCurrentStartPointCosmic+=int(filesCount/2)
    else:
        if trainingAbsoluteEndPointCosmic-trainingCurrentStartPointCosmic!=0:
            cosmicdata=getData(input_tree_cosmic,averages, ibatchsize = trainingAbsoluteEndPointCosmic-trainingCurrentStartPointCosmic,start_point=trainingCurrentStartPointCosmic)
        cosmicdata=np.append(cosmicdata,getData(input_tree_cosmic,averages, ibatchsize = int(filesCount/2)-(trainingAbsoluteEndPointCosmic-trainingCurrentStartPointCosmic),start_point=trainingAbsoluteStartPointCosmic),axis=0)
        trainingCurrentStartPointCosmic=trainingAbsoluteStartPointCosmic+int(filesCount/2)-(trainingAbsoluteEndPointCosmic-trainingCurrentStartPointCosmic)
    mixingdata=mixingdata.reshape((mixingdata.shape[0],72,int(mixingdata.shape[1]/72)))
    cosmicdata=cosmicdata.reshape((cosmicdata.shape[0],72,int(cosmicdata.shape[1]/72)))
    taggedNoise = [(cosmicdata[i,:,:],0) for i in range(cosmicdata.shape[0])]
    taggedSignal= [(mixingdata[i,:,:],1) for i in range(mixingdata.shape[0])]
    allData = taggedNoise+taggedSignal
    random.shuffle(allData)
    dat=np.array([thing[0] for thing in allData])
    lab=np.array([thing[1] for thing in allData])
    return dat, lab,trainingCurrentStartPointCosmic, trainingCurrentStartPointMixing


def getTestBatch(filesCount,\
    input_tree_mixing,
    input_tree_cosmic,
    testAbsoluteStartPointMixing,\
    testCurrentStartPointMixing,\
    testAbsoluteEndPointMixing,\
    testAbsoluteStartPointCosmic,\
    testCurrentStartPointCosmic,\
    testAbsoluteEndPointCosmic,averages):
    if testCurrentStartPointMixing+int(filesCount/2)<testAbsoluteEndPointMixing:
        mixingdata=getData(input_tree_mixing,averages, ibatchsize = int(filesCount/2),start_point=testCurrentStartPointMixing)
        testCurrentStartPointMixing+=int(filesCount/2)
    else:
        if testAbsoluteEndPointMixing-testCurrentStartPointMixing!=0:
            mixingdata=getData(input_tree_mixing,averages, ibatchsize = testAbsoluteEndPointMixing-testCurrentStartPointMixing,start_point=testCurrentStartPointMixing)
        mixingdata=np.append(mixingdata,getData(input_tree_mixing,averages, ibatchsize = int(filesCount/2)-(testAbsoluteEndPointMixing-testCurrentStartPointMixing),start_point=testAbsoluteStartPointMixing),axis=0)
        testCurrentStartPointMixing=testAbsoluteStartPointMixing+int(filesCount/2)-(testAbsoluteEndPointMixing-testCurrentStartPointMixing)

    if testCurrentStartPointCosmic+int(filesCount/2)<testAbsoluteEndPointCosmic:
        cosmicdata=getData(input_tree_cosmic,averages, ibatchsize = int(filesCount/2),start_point=testCurrentStartPointCosmic)
        testCurrentStartPointCosmic+=int(filesCount/2)
    else:
        if testAbsoluteEndPointCosmic-testCurrentStartPointCosmic!=0:
            cosmicdata=getData(input_tree_cosmic,averages, ibatchsize = testAbsoluteEndPointCosmic-testCurrentStartPointCosmic,start_point=testCurrentStartPointCosmic)
        cosmicdata=np.append(cosmicdata,getData(input_tree_cosmic,averages, ibatchsize = int(filesCount/2)-(testAbsoluteEndPointCosmic-testCurrentStartPointCosmic),start_point=testAbsoluteStartPointCosmic),axis=0)
        testCurrentStartPointCosmic=testAbsoluteStartPointCosmic+int(filesCount/2)-(testAbsoluteEndPointCosmic-testCurrentStartPointCosmic)
    mixingdata=mixingdata.reshape((mixingdata.shape[0],72,int(mixingdata.shape[1]/72)))
    cosmicdata=cosmicdata.reshape((cosmicdata.shape[0],72,int(cosmicdata.shape[1]/72)))
    taggedNoise = [(cosmicdata[i,:,:],0) for i in range(cosmicdata.shape[0])]
    taggedSignal= [(mixingdata[i,:,:],1) for i in range(mixingdata.shape[0])]
    allData = taggedNoise+taggedSignal
    random.shuffle(allData)
    dat=np.array([thing[0] for thing in allData])
    lab=np.array([thing[1] for thing in allData])
    return dat,lab, testCurrentStartPointCosmic,testCurrentStartPointMixing


class TwoStageNeuralNetwork():
    def __init__(self, sharing, inputsPerShared=512, blockCount=72, \
                 sharedOutputsCount=15,sharedOutputType="tanh", \
                 outputCount=1, outputType="sigmoid", \
                 sharedHiddenLayerDescriptions=[(80,"tanh"),(80,"tahn")], \
                 postPanelHiddenLayerDescriptions=[(400,"tanh"),(400,"tahn")],randomFactor=.5,\
                 hasWeightDecay = False, weightDecayFactor = 0.9,\
                 hasDropout = False, dropoutProbability = 0.5,
                 regularizationsOnShared = [0,0,0], regularizationsOnUnshared = [0,0,0],
                reluRespawn=False,reluRespawnType="match magnitudes",reluRespawnCutoff=1000,reluRespawnDefinedMagnitude=.1,reluRespawnMinReplacements=10):
        self.sharedWeightMatrices = []
        self.weightMatrices=[]
        self.hiddenLayers=postPanelHiddenLayerDescriptions
        self.sharedOutputType=sharedOutputType
        self.sharedHiddenLayers=sharedHiddenLayerDescriptions
        self.outputType=outputType
        self.outputCount=outputCount
        self.sharing=sharing
        self.inputsPerShared=inputsPerShared
        self.blockCount=blockCount
        self.sharedOutputsCount=sharedOutputsCount
        self.hasWeightDecay = hasWeightDecay
        self.weightDecayFactor = weightDecayFactor
        self.hasDropout = hasDropout
        self.dropoutProbability = dropoutProbability
        self.regularizationsOnShared=regularizationsOnShared
        self.regularizationsOnUnshared=regularizationsOnUnshared
        self.reluRespawn=reluRespawn
        self.reluRespawnType=reluRespawnType
        self.reluRespawnCutoff=reluRespawnCutoff
        self.reluRespawnDefinedMagnitude=reluRespawnDefinedMagnitude
        self.reluRespawnMinReplacements=reluRespawnMinReplacements
        currentThickness=inputsPerShared
        if self.reluRespawn:
            self.allZeroCounts = []
            self.allZeroSharedCounts = []
            for layerDescription in sharedHiddenLayerDescriptions:
                self.allZeroSharedCounts.append(np.zeros((blockCount,layerDescription.nodeCount)))
            self.allZeroSharedCounts.append(np.zeros((blockCount,sharedOutputsCount)))
            for layerDescription in postPanelHiddenLayerDescriptions:
                self.allZeroCounts.append(np.zeros(layerDescription.nodeCount))
        if self.hasWeightDecay:
            self.weightDecayShared = []
            self.weightDecay = []
        if sharing:
            for layerDescription in sharedHiddenLayerDescriptions:
                self.sharedWeightMatrices.append(randomFactor*(np.random.rand(currentThickness+1,layerDescription.nodeCount)-0.5))  
                if self.hasWeightDecay:
                    self.weightDecayShared.append(np.zeros((currentThickness+1,layerDescription.nodeCount)))
                currentThickness=layerDescription.nodeCount
        else:
            for layerDescription in sharedHiddenLayerDescriptions:
                self.sharedWeightMatrices.append(randomFactor*(np.random.rand(blockCount,currentThickness+1,layerDescription.nodeCount)-0.5))  
                if self.hasWeightDecay:
                    self.weightDecayShared.append(np.zeros((blockCount,currentThickness+1,layerDescription.nodeCount)))
                currentThickness=layerDescription.nodeCount
        if not sharing:
            self.sharedWeightMatrices.append(randomFactor*(np.random.rand(blockCount,currentThickness+1,self.sharedOutputsCount)-0.5))
            if self.hasWeightDecay:
                self.weightDecayShared.append(np.zeros((blockCount,currentThickness+1,self.sharedOutputsCount)))
        else:
            self.sharedWeightMatrices.append(randomFactor*(np.random.rand(currentThickness+1,self.sharedOutputsCount)-0.5))
            if self.hasWeightDecay:
                self.weightDecayShared.append(np.zeros((currentThickness+1,self.sharedOutputsCount)))
        currentThickness = blockCount*sharedOutputsCount
        for layerDescription in postPanelHiddenLayerDescriptions:
            self.weightMatrices.append(randomFactor*(np.random.rand(currentThickness+1,layerDescription.nodeCount)-0.5))
            if self.hasWeightDecay:
                self.weightDecay.append(np.zeros((currentThickness+1,layerDescription.nodeCount)))
            currentThickness=layerDescription.nodeCount
        self.weightMatrices.append(randomFactor*(np.random.rand(currentThickness+1,outputCount)-0.5))
        if self.hasWeightDecay:
            self.weightDecay.append(np.zeros((currentThickness+1,outputCount)))

    def classify(self, samples, tensordot = True):
        mat=np.array(samples)
        if self.sharing:
            for i in range(len(self.sharedWeightMatrices)):
                if self.hasDropout and i!=0:
                    mat *= 1-self.dropoutProbability
                mat = np.append(mat,np.ones((mat.shape[0],mat.shape[1],1)),axis=2)
                if not tensordot:
                    mat = np.einsum("abc,ce->abe",mat,self.sharedWeightMatrices[i])
                else:
                    mat = np.tensordot(mat,self.sharedWeightMatrices[i],axes=([2],[0]))
                if i==len(self.sharedWeightMatrices)-1:
                    if self.sharedOutputType=="tanh":
                        mat=np.tanh(mat)
                    elif self.sharedOutputType=="relu":
                        np.maximum(mat,0,mat)
                else:
                    if self.sharedHiddenLayers[i].typ=="tanh":
                        mat=np.tanh(mat)
                    elif self.sharedHiddenLayers[i].typ=="relu":
                        np.maximum(mat,0,mat)
            mat=mat.reshape((mat.shape[0],mat.shape[1]*mat.shape[2]))
        else:
            for i in range(len(self.sharedWeightMatrices)):
                if self.hasDropout and i!=0:
                    mat *= 1-self.dropoutProbability
                mat = np.append(mat,np.ones((mat.shape[0],mat.shape[1],1)),axis=2)
                if not tensordot:
                    mat = np.einsum("abc,bce->abe",mat,self.sharedWeightMatrices[i])
                else:
                    newmat = np.zeros((mat.shape[0],mat.shape[1],self.sharedWeightMatrices[i].shape[2]))
                    for k in range(mat.shape[1]):
                        newmat[:,k,:] = np.tensordot(mat[:,k,:],self.sharedWeightMatrices[i][k,:,:],axes=([1],[0]))
                    mat = newmat
                if i==len(self.sharedWeightMatrices)-1:
                    #print(mat)
                    if self.sharedOutputType=="tanh":
                        mat=np.tanh(mat)
                    elif self.sharedOutputType=="relu":
                        np.maximum(mat,0,mat)
                else:
                    #print(mat)
                    if self.sharedHiddenLayers[i].typ=="tanh":
                        mat=np.tanh(mat)
                    elif self.sharedHiddenLayers[i].typ=="relu":
                        np.maximum(mat,0,mat)
            mat=mat.reshape((mat.shape[0],mat.shape[1]*mat.shape[2]))
        for i in range(len(self.weightMatrices)):
            if self.hasDropout:
                mat*=1-self.dropoutProbability
            mat = np.append(mat,np.ones((mat.shape[0],1)),axis=1)
            if not tensordot:
                mat = np.einsum("ac,ce->ae",mat,self.weightMatrices[i])
            else:
                mat = np.tensordot(mat,self.weightMatrices[i],axes=([1],[0]))
            if i==len(self.weightMatrices)-1:
                if self.outputType=="sigmoid":
                    #print(mat)
                    mat=1.0/(1+np.exp(-1*mat))
                else:
                    pass
            else:
                #print(mat)
                if self.hiddenLayers[i].typ=="tanh":
                    mat=np.tanh(mat)
                elif self.hiddenLayers[i].typ=="relu":
                    np.maximum(mat,0,mat)
        return mat

    def classifyTest(self,samples,results,boundary = .5,tensordot=True):
        outputs=self.classify(samples,tensordot)
        for i in range(len(outputs)):
            if outputs[i]>boundary:
                outputs[i]=1
            else:
                outputs[i]=0
        total=0
        totalRight=0
        falsePositives=0
        falseNegatives=0
        correctPositives=0
        correctNegatives=0
        for res in range(len(results)):
            if results[res]==outputs[res]:
                totalRight+=1
                if results[res]==1:
                    correctPositives+=1
                else:
                    correctNegatives+=1
            else:
                if outputs[res]==1:
                    falsePositives+=1
                else:
                    falseNegatives+=1
            total+=1
        return (1.0*totalRight)/total, correctPositives, correctNegatives, falsePositives, falseNegatives

    def trainBatchSGD(self,samples,correctAnswers,epochs,\
                      sharedLearningRates,learningRates,samplesAtOnce=10,tensordot=True,printingMagnitudes=False):
        lambdas=self.regularizationsOnUnshared
        sharedLambdas=self.regularizationsOnShared
        for epoch in range(epochs):
            currSampleStart=0
            while currSampleStart<samples.shape[0]:
                #forward Pass
                respawnFlag=False
                if samples.shape[0]-currSampleStart>samplesAtOnce+1:
                    currTraining=samples[currSampleStart:currSampleStart+samplesAtOnce,:,:]
                    currLabels = correctAnswers[currSampleStart:currSampleStart+samplesAtOnce]
                else:
                    currTraining=samples[currSampleStart:,:,:]
                    currLabels=correctAnswers[currSampleStart:]
                forwardPassSharedLayers=[]
                forwardPassLayers=[]
                mat=currTraining
                if self.hasDropout:
                    dropoutMatrices = []
                    dropoutMatricesShared = []
                if self.sharing:
                    for i in range(len(self.sharedWeightMatrices)):
                        if self.hasDropout and i!=0:
                            newdrop=np.where(np.random.rand(mat.shape[2])>self.dropoutProbability,1,0)
                            dropoutMatricesShared.append(np.append(newdrop,np.ones(1)))
                            mat*=newdrop
                        mat = np.append(mat,np.ones((mat.shape[0],mat.shape[1],1)),axis=2)
                        forwardPassSharedLayers.append(mat)
                        if not tensordot:
                            mat = np.einsum("abc,ce->abe",mat,self.sharedWeightMatrices[i])
                        else:
                            mat = np.tensordot(mat,self.sharedWeightMatrices[i],axes=([2],[0]))
                        if i==len(self.sharedWeightMatrices)-1:
                            if self.sharedOutputType=="tanh":
                                mat=np.tanh(mat)
                            elif self.sharedOutputType=="relu":
                                np.maximum(mat,0,mat)
                        else:
                            if self.sharedHiddenLayers[i].typ=="tanh":
                                mat=np.tanh(mat)
                            elif self.sharedHiddenLayers[i].typ=="relu":
                                np.maximum(mat,0,mat)
                    forwardPassSharedLayers.append(mat)
                    mat=mat.reshape((mat.shape[0],mat.shape[1]*mat.shape[2]))
                else:
                    for i in range(len(self.sharedWeightMatrices)):
                        if self.hasDropout and i!=0:
                            newdrop=np.where(np.random.rand(mat.shape[1],mat.shape[2])>self.dropoutProbability,1,0)
                            dropoutMatricesShared.append(np.append(newdrop,np.ones((mat.shape[1],1)),axis=1))
                            mat*=newdrop
                        mat = np.append(mat,np.ones((mat.shape[0],mat.shape[1],1)),axis=2)

                        forwardPassSharedLayers.append(mat)
                        if not tensordot:
                            mat = np.einsum("abc,bce->abe",mat,self.sharedWeightMatrices[i])
                        else:
                            newmat = np.zeros((mat.shape[0],mat.shape[1],self.sharedWeightMatrices[i].shape[2]))
                            for k in range(mat.shape[1]):
                                newmat[:,k,:] = np.tensordot(mat[:,k,:],self.sharedWeightMatrices[i][k,:,:],axes=([1],[0]))
                            mat = newmat
                        if i==len(self.sharedWeightMatrices)-1:
                            if self.sharedOutputType=="tanh":
                                mat=np.tanh(mat)
                            elif self.sharedOutputType=="relu":
                                np.maximum(mat,0,mat)
                                if self.reluRespawn:
                                    self.allZeroSharedCounts[i]=np.where(np.all(mat==0,axis=0),\
                                                                         self.allZeroSharedCounts[i]+samplesAtOnce,\
                                                                         0)
                                    deadNeurons=np.where(self.allZeroSharedCounts[i]>self.reluRespawnCutoff)
                                    self.allZeroSharedCounts[i]=np.where(np.all(mat==0,axis=0),\
                                                                         self.allZeroSharedCounts[i]+samplesAtOnce,\
                                                                         0)
                                    deadNeurons=np.where(self.allZeroSharedCounts[i]>self.reluRespawnCutoff)
                                    if len(deadNeurons[0])>=self.reluRespawnMinReplacements:
                                        respawnFlag=True
                                        #print("error before first: " +str(self.classifyTest(samples[:1000],correctAnswers[:1000])))
                                        for deadInd in range(len(deadNeurons[0])):
                                            neuronPanel=deadNeurons[0][deadInd]
                                            neuronIndex=deadNeurons[1][deadInd]
                                            mag=0
                                            if self.reluRespawnType=="match magnitudes":
                                                shp=self.sharedWeightMatrices[i].shape
                                                mag=np.average(np.abs(self.sharedWeightMatrices[i][:,:shp[1]-1,:]))
                                            if self.reluRespawnType=="defined magnitude" or self.reluRespawnType=="match magnitudes":
                                                if self.reluRespawnType=="defined magnitude":
                                                    mag=2*self.reluRespawnDefinedMagnitude
                                                self.sharedWeightMatrices[i][neuronPanel,:,neuronIndex]=\
                                                    mag*(np.random.rand(self.sharedWeightMatrices[i].shape[1])-.5)
                                                self.allZeroSharedCounts[i][neuronPanel,neuronIndex]=0
                                                self.weightMatrices[0][neuronPanel*self.sharedOutputsCount+neuronIndex,:]=0
                                        #print("error after first: " +str(self.classifyTest(samples[:1000],correctAnswers[:1000])))

                        else:
                            if self.sharedHiddenLayers[i].typ=="tanh":
                                mat=np.tanh(mat)
                            elif self.sharedHiddenLayers[i].typ=="relu":
                                np.maximum(mat,0,mat)
                                if self.reluRespawn:
                                    self.allZeroSharedCounts[i]=np.where(np.all(mat==0,axis=0),\
                                                                         self.allZeroSharedCounts[i]+samplesAtOnce,\
                                                                         0)
                                    deadNeurons=np.where(self.allZeroSharedCounts[i]>self.reluRespawnCutoff)
                                    if len(deadNeurons[0])>=self.reluRespawnMinReplacements:
                                        respawnFlag=True
                                        #print("error before mid: " +str(self.classifyTest(samples[:1000],correctAnswers[:1000])))

                                        for deadInd in range(len(deadNeurons[0])):
                                            neuronPanel=deadNeurons[0][deadInd]
                                            neuronIndex=deadNeurons[1][deadInd]
                                            mag=0
                                            if self.reluRespawnType=="match magnitudes":
                                                shp=self.sharedWeightMatrices[i].shape
                                                mag=np.average(np.abs(self.sharedWeightMatrices[i][:,:shp[1]-1,:]))
                                            if self.reluRespawnType=="defined magnitude" or self.reluRespawnType=="match magnitudes":
                                                if self.reluRespawnType=="defined magnitude":
                                                    mag=2*self.reluRespawnDefinedMagnitude
                                                self.sharedWeightMatrices[i][neuronPanel,:,neuronIndex]=\
                                                    mag*(np.random.rand(self.sharedWeightMatrices[i].shape[1])-.5)
                                                self.allZeroSharedCounts[i][neuronPanel,neuronIndex]=0
                                                self.sharedWeightMatrices[i+1][neuronPanel,neuronIndex,:]=0
                                        #print("error after mid: " +str(self.classifyTest(samples[:1000],correctAnswers[:1000])))

                    forwardPassSharedLayers.append(mat)
                    mat=mat.reshape((mat.shape[0],mat.shape[1]*mat.shape[2]))
                for i in range(len(self.weightMatrices)):
                    if self.hasDropout:
                        newdrop = np.where(np.random.rand(mat.shape[1])>self.dropoutProbability,1,0)
                        dropoutMatrices.append(np.append(newdrop,np.ones(1)))
                        mat*=newdrop
                    mat = np.append(mat,np.ones((mat.shape[0],1)),axis=1)
                    forwardPassLayers.append(mat)
                    if not tensordot:
                        mat = np.einsum("ac,ce->ae",mat,self.weightMatrices[i])
                    else:
                        mat = np.tensordot(mat,self.weightMatrices[i],axes=([1],[0]))
                    if i==len(self.weightMatrices)-1:
                        if self.outputType=="sigmoid":
                            mat=1.0/(1+np.exp(-1*mat))
                        else:
                            pass
                    else:
                        if self.hiddenLayers[i].typ=="tanh":
                            mat=np.tanh(mat)
                        elif self.hiddenLayers[i].typ=="relu":
                            np.maximum(mat,0,mat)
                            if self.reluRespawn:
                                self.allZeroCounts[i]=np.where(np.all(mat==0,axis=0),\
                                                                     self.allZeroCounts[i]+samplesAtOnce,\
                                                                     0)
                                deadNeurons=np.where(self.allZeroCounts[i]>self.reluRespawnCutoff)
                                if len(deadNeurons[0])>=self.reluRespawnMinReplacements:
                                    respawnFlag=True
                                    #print("error before late: " +str(self.classifyTest(samples[:1000],correctAnswers[:1000])))
                                    for neuronIndex in deadNeurons[0]:
                                        mag=0
                                        if self.reluRespawnType=="match magnitudes":
                                            shp=self.weightMatrices[i].shape
                                            mag=np.average(np.abs(self.weightMatrices[i][:shp[0]-1,:]))
                                        if self.reluRespawnType=="defined magnitude" or self.reluRespawnType=="match magnitudes":
                                            if self.reluRespawnType=="defined magnitude":
                                                mag=2*self.reluRespawnDefinedMagnitude
                                            self.weightMatrices[i][:,neuronIndex]=\
                                                mag*(np.random.rand(self.weightMatrices[i].shape[0])-.5)
                                            self.allZeroCounts[i][neuronIndex]=0
                                            self.weightMatrices[i+1][neuronIndex,:]=0
                                    #print("error after late: " +str(self.classifyTest(samples[:1000],correctAnswers[:1000])))
                forwardPassLayers.append(mat)
                if respawnFlag:
                    continue
                #backward pass
                depth = len(forwardPassLayers)-1
                diff=forwardPassLayers[depth]-currLabels.reshape((\
                     forwardPassLayers[depth].shape[0],forwardPassLayers[depth].shape[1]))
                if not tensordot:
                    firstGrad = np.einsum("ij,ik->ijk",forwardPassLayers[depth-1],diff)
                else:
                    firstGrad=np.zeros((diff.shape[0],forwardPassLayers[depth-1].shape[1],diff.shape[1]))
                    for i in range(diff.shape[0]):
                        firstGrad[i,:,:]=np.outer(forwardPassLayers[depth-1][i],diff[i])
                totalgrad=np.sum(firstGrad,axis=0)/firstGrad.shape[0]
                regularization = lambdas[depth-1]*self.weightMatrices[depth-1]
                regularization[regularization.shape[0]-1,:]=0
                adding = (totalgrad+regularization)
                if printingMagnitudes:
                    sys.stdout.write(str(np.average(np.abs(adding)))+"\t")
                if self.hasDropout:
                    adding*=np.repeat(np.reshape(dropoutMatrices[depth-1],(adding.shape[0],1)),adding.shape[1],axis=1)
                if not self.hasWeightDecay:
                    self.weightMatrices[depth-1]-=learningRates[depth-1]*adding
                else:
                    self.weightDecay[depth-1]=self.weightDecayFactor*self.weightDecay[depth-1]+(1-self.weightDecayFactor)*adding
                    self.weightMatrices[depth-1]-=learningRates[depth-1]*self.weightDecay[depth-1]
                if not tensordot:
                    prevLayer=np.einsum("ij,kj->ik",diff,self.weightMatrices[depth-1])
                else:
                    prevLayer = np.tensordot(diff,self.weightMatrices[depth-1],axes=([1],[1]))
                depth-=1
                while depth>0:
                    if self.hiddenLayers[depth-1].typ=="tanh":
                        grad=np.multiply(1-np.square(forwardPassLayers[depth]),prevLayer)
                    elif self.hiddenLayers[depth-1].typ=="relu":
                        grad=np.multiply(np.where(forwardPassLayers[depth]>0,1,0),prevLayer)
                    grad=np.delete(grad,grad.shape[1]-1,axis=1)
                    if not tensordot:
                        newgrad=np.einsum("ij,ik->ikj",grad,forwardPassLayers[depth-1])
                    else:
                        newgrad = np.zeros((grad.shape[0],forwardPassLayers[depth-1].shape[1],grad.shape[1]))
                        for l in range(grad.shape[0]):
                            newgrad[l,:,:]=np.outer(forwardPassLayers[depth-1][l],grad[l])
                    totalgrad=np.sum(newgrad,axis=0)/totalgrad.shape[0]

                    regularization = lambdas[depth-1]*self.weightMatrices[depth-1]
                    regularization[regularization.shape[0]-1,:]=0
                    adding = (totalgrad+regularization)

                    if printingMagnitudes:
                        sys.stdout.write(str(np.average(np.abs(adding)))+"\t")
                    if self.hasDropout:
                        adding*=np.repeat(np.reshape(dropoutMatrices[depth-1],(adding.shape[0],1)),adding.shape[1],axis=1)
                    if not self.hasWeightDecay:
                        self.weightMatrices[depth-1]-=learningRates[depth-1]*adding
                    else:
                        self.weightDecay[depth-1]=self.weightDecayFactor*self.weightDecay[depth-1]+(1-self.weightDecayFactor)*adding
                        self.weightMatrices[depth-1]-=learningRates[depth-1]*self.weightDecay[depth-1]
                    if not tensordot:
                        prevLayer=np.einsum("kj,ij->ki",grad,self.weightMatrices[depth-1])
                    else:
                        prevLayer = np.tensordot(grad,self.weightMatrices[depth-1],axes=([1],[1]))
                    depth-=1

                depth=len(forwardPassSharedLayers)-1

                prevLayer=np.delete(prevLayer,prevLayer.shape[1]-1,axis=1)
                prevLayer=prevLayer.reshape((prevLayer.shape[0],self.blockCount,int(round(prevLayer.shape[1]/self.blockCount))))

                if self.sharedOutputType=="tanh":
                    grad = np.multiply(1-np.square(forwardPassSharedLayers[depth]),prevLayer)
                elif self.sharedOutputType=="relu":
                    grad=np.multiply(np.where(forwardPassSharedLayers[depth]>0,1,0),prevLayer)
                if not tensordot:
                    newGrad=np.einsum("ijk,ijl->ijlk",grad,forwardPassSharedLayers[depth-1])
                else:
                    newGrad=np.zeros((grad.shape[0],grad.shape[1],forwardPassSharedLayers[depth-1].shape[2],grad.shape[2]))
                    for k in range(grad.shape[0]):
                        for l in range(grad.shape[1]):
                            newGrad[k,l,:,:]=np.outer(forwardPassSharedLayers[depth-1][k,l],grad[k,l])
                if self.sharing:
                    totalgrad=np.sum(np.sum(newGrad,axis=0),axis=0)/(totalgrad.shape[0]*totalgrad.shape[1])
                    regularization = sharedLambdas[depth-1]*self.sharedWeightMatrices[depth-1]
                    regularization[regularization.shape[0]-1,:]=0
                    adding = (totalgrad+regularization)

                    if printingMagnitudes:
                        sys.stdout.write(str(np.average(np.abs(adding)))+"\t")
                    if self.hasDropout:
                        adding*=np.repeat(dropoutMatricesShared[depth-2],adding.shape[2],axis=2)
                    if not self.hasWeightDecay:
                        self.sharedWeightMatrices[depth-1]-=sharedLearningRates[depth-1]*adding
                    else:
                        self.weightDecayShared[depth-1]=self.weightDecayFactor*self.weightDecayShared[depth-1]+(1-self.weightDecayFactor)*adding
                        self.sharedWeightMatrices[depth-1]-=sharedLearningRates[depth-1]*self.weightDecayShared[depth-1]
                    if not tensordot:
                        prevLayer=np.einsum("ijk,lk->ijl",grad,self.sharedWeightMatrices[depth-1])
                    else:
                        prevLayer = np.tensordot(grad,self.sharedWeightMatrices[depth-1],axes=([2],[1]))
                else:
                    totalgrad=np.sum(newGrad,axis=0)/totalgrad.shape[0]
                    regularization = sharedLambdas[depth-1]*self.sharedWeightMatrices[depth-1]
                    regularization[:,regularization.shape[1]-1,:]=0
                    adding = totalgrad+regularization

                    if printingMagnitudes:
                        sys.stdout.write(str(np.average(np.abs(adding)))+"\t")
                    if self.hasDropout:
                        adding*=np.repeat(np.reshape(dropoutMatricesShared[depth-2],(dropoutMatricesShared[depth-2].shape[0],dropoutMatricesShared[depth-2].shape[1],1)),adding.shape[2],axis=2)
                    if not self.hasWeightDecay:
                        self.sharedWeightMatrices[depth-1]-=sharedLearningRates[depth-1]*adding
                    else:
                        self.weightDecayShared[depth-1]=self.weightDecayFactor*self.weightDecayShared[depth-1]+(1-self.weightDecayFactor)*adding
                        self.sharedWeightMatrices[depth-1]-=sharedLearningRates[depth-1]*self.weightDecayShared[depth-1]
                    if not tensordot:
                        prevLayer=np.einsum("ijk,jlk->ijl",grad,self.sharedWeightMatrices[depth-1])
                    else:
                        prevLayer=np.zeros((grad.shape[0],grad.shape[1],self.sharedWeightMatrices[depth-1].shape[1]))
                        for l in range(grad.shape[1]):
                            prevLayer[:,l,:]=np.tensordot(grad[:,l,:],self.sharedWeightMatrices[depth-1][l],axes=([1],[1]))
                depth-=1
                while depth>0:
                    if self.sharedHiddenLayers[depth-1].typ=="tanh":
                        grad=np.multiply(1-np.square(forwardPassSharedLayers[depth]),prevLayer)
                    elif self.sharedHiddenLayers[depth-1].typ=="relu":
                        grad=np.multiply(np.where(forwardPassSharedLayers[depth]>0,1,0),prevLayer)
                    grad=np.delete(grad,grad.shape[2]-1,axis=2)
                    if not tensordot:
                        newgrad=np.einsum("ijk,ijl->ijlk",grad,forwardPassSharedLayers[depth-1])
                    else:
                        newgrad=np.zeros((grad.shape[0],grad.shape[1],forwardPassSharedLayers[depth-1].shape[2],grad.shape[2]))
                        for k in range(grad.shape[0]):
                            for l in range(grad.shape[1]):
                                newgrad[k,l,:,:]=np.outer(forwardPassSharedLayers[depth-1][k,l],grad[k,l])
                    if self.sharing:
                        totalgrad=np.sum(np.sum(newgrad,axis=0),axis=0)/(newgrad.shape[0]*newgrad.shape[1])
                        regularization = sharedLambdas[depth-1]*self.sharedWeightMatrices[depth-1]
                        regularization[regularization.shape[0]-1,:]=0
                        adding = (totalgrad+regularization)

                        if printingMagnitudes:
                            sys.stdout.write(str(np.average(np.abs(adding)))+"\t")
                        if self.hasDropout and depth>1:
                            #pass
                            adding*=np.repeat(np.reshape(dropoutMatricesShared[depth-2],(dropoutMatricesShared[depth-2].shape[0],dropoutMatricesShared[depth-2].shape[1],1)),adding.shape[2],axis=2)
                        if not self.hasWeightDecay:
                            self.sharedWeightMatrices[depth-1]-=sharedLearningRates[depth-1]*adding
                        else:
                            self.weightDecayShared[depth-1]=self.weightDecayFactor*self.weightDecayShared[depth-1]+(1-self.weightDecayFactor)*adding
                            self.sharedWeightMatrices[depth-1]-=sharedLearningRates[depth-1]*self.weightDecayShared[depth-1]
                        if not tensordot:
                            prevLayer=np.einsum("ijk,lk->ijl",grad,self.sharedWeightMatrices[depth-1])
                        else:
                            prevLayer = np.tensordot(grad,self.sharedWeightMatrices[depth-1],axes=([2],[1]))
                    else:
                        totalgrad=np.sum(newgrad,axis=0)/newgrad.shape[0]
                        regularization = sharedLambdas[depth-1]*self.sharedWeightMatrices[depth-1]
                        regularization[:,regularization.shape[1]-1,:]=0
                        adding = totalgrad+regularization

                        if printingMagnitudes:
                            sys.stdout.write(str(np.average(np.abs(adding)))+"\t")
                        if self.hasDropout and depth>1:
                            adding*=np.repeat(np.reshape(dropoutMatricesShared[depth-2],(dropoutMatricesShared[depth-2].shape[0],dropoutMatricesShared[depth-2].shape[1],1)),adding.shape[2],axis=2)
                        if not self.hasWeightDecay:
                            self.sharedWeightMatrices[depth-1]-=sharedLearningRates[depth-1]*adding
                        else:
                            self.weightDecayShared[depth-1]=self.weightDecayFactor*self.weightDecayShared[depth-1]+(1-self.weightDecayFactor)*adding
                            self.sharedWeightMatrices[depth-1]-=sharedLearningRates[depth-1]*self.weightDecayShared[depth-1]
                        if not tensordot:
                            prevLayer=np.einsum("ijk,jlk->ijl",grad,self.sharedWeightMatrices[depth-1])
                        else:
                            prevLayer=np.zeros((grad.shape[0],grad.shape[1],self.sharedWeightMatrices[depth-1].shape[1]))
                            for l in range(grad.shape[1]):
                                prevLayer[:,l,:]=np.tensordot(grad[:,l,:],self.sharedWeightMatrices[depth-1][l],axes=([1],[1]))
                    depth-=1
                if printingMagnitudes:
                    sys.stdout.write("\n")
                    if currSampleStart>1000:
                        return
                currSampleStart+=samplesAtOnce

    def saveNeuralNet(self,folder):
        if not os.path.exists(folder):
            os.makedirs(folder)
        if not self.sharing:
            for i in range(len(self.sharedWeightMatrices)):
                for j in range(self.sharedWeightMatrices[0].shape[0]):
                    with open(folder+"/"+str(i)+"_"+str(j)+"_shared_matrix_saved.npy","wb") as file:
                        np.save(file,self.sharedWeightMatrices[i][j,:,:])
            for i in range(len(self.weightMatrices)):
                with open(folder+"/"+str(i)+"_unshared_matrix_saved.npy","wb") as file:
                    np.save(file,self.weightMatrices[i])
        with open(folder+"/"+"configuration.txt","w") as file:
            if not os.path.exists(folder):
                os.makedirs(folder)
            #TODO(jfriedly):  This should probably just call file.write(vars(nnConfig))
            file.write(str(self.hiddenLayers)+"\n")
            file.write(str(self.sharedOutputType)+"\n")
            file.write(str(self.sharedHiddenLayers)+"\n")
            file.write(str(self.outputType)+"\n")
            file.write(str(self.outputCount)+"\n")
            file.write(str(self.sharing)+"\n")
            file.write(str(self.inputsPerShared)+"\n")
            file.write(str(self.blockCount)+"\n")
            file.write(str(self.sharedOutputsCount)+"\n")

    def loadNeuralNet(self,folder):
        if not self.sharing:
            for i in range(len(self.sharedWeightMatrices)):
                for j in range(self.sharedWeightMatrices[0].shape[0]):
                    with open(folder+"/"+str(i)+"_"+str(j)+"_shared_matrix_saved.npy","rb") as file:
                        self.sharedWeightMatrices[i][j,:,:]=np.load(file)
            for i in range(len(self.weightMatrices)):
                with open(folder+"/"+str(i)+"_unshared_matrix_saved.npy","rb") as file:
                    self.weightMatrices[i]=np.load(file)


def main(nnConfig):
    input_root_file_mixing = ROOT.TFile.Open(args.annihilationData)
    input_root_file_cosmic = ROOT.TFile.Open(args.backgroundData)
    input_tree_mixing = input_root_file_mixing.Get('ADCRawTree')
    input_tree_cosmic = input_root_file_cosmic.Get('ADCRawTree')

    trainingAbsoluteStartPointMixing= 0
    trainingCurrentStartPointMixing= 0
    trainingAbsoluteEndPointMixing= min(int(input_tree_mixing.GetEntriesFast()*4/5),nnConfig.maxTrainings)
    testAbsoluteStartPointMixing = min(int(input_tree_mixing.GetEntriesFast()*4/5),nnConfig.maxTrainings)
    testCurrentStartPointMixing = testAbsoluteStartPointMixing
    testAbsoluteEndPointMixing = input_tree_mixing.GetEntriesFast()

    trainingAbsoluteStartPointCosmic= 0
    trainingCurrentStartPointCosmic= 0
    trainingAbsoluteEndPointCosmic= min(int(input_tree_cosmic.GetEntriesFast()*4/5),trainingAbsoluteEndPointMixing)
    testAbsoluteStartPointCosmic = min(int(input_tree_cosmic.GetEntriesFast()*4/5),testAbsoluteStartPointMixing)
    testCurrentStartPointCosmic = testAbsoluteStartPointCosmic
    testAbsoluteEndPointCosmic = min(input_tree_cosmic.GetEntriesFast(),testAbsoluteEndPointMixing)

    if os.path.isfile("averageData.npy"):
        logging.info("Average data found. Loading and continuing.")
        averageData=np.load("averageData.npy")
    else:
        logging.info("Average data not found. Computing.")
        totals=np.zeros(72*512)
        count=0
        for i in range(trainingAbsoluteEndPointMixing):
            if i%1000==0:
                logging.debug("Processed the first %d mixing data points", i)
            input_tree_mixing.GetEntry(i)
            totals+=np.array(input_tree_mixing.ADC)
            count+=1
        for i in range(trainingAbsoluteEndPointCosmic):
            if i%1000==0:
                logging.debug("Processed the first %d cosmic data points", i)
            input_tree_cosmic.GetEntry(i)
            totals+=np.array(input_tree_cosmic.ADC)
            count+=1
        averageData=totals/count
        with open("averageData.npy","wb") as file:
            np.save(file, averageData)
        logging.info("Computed and saved average data. Exiting.")
        exit()

    nn=TwoStageNeuralNetwork(False,\
                             sharedOutputsCount=nnConfig.sharedOuts, \
                             sharedOutputType=nnConfig.sharedOutputType,\
                             sharedHiddenLayerDescriptions=nnConfig.sharedHiddenLayers, \
                             postPanelHiddenLayerDescriptions=nnConfig.postPanelHiddenLayers,\
                             randomFactor=.5,\
                             hasWeightDecay=nnConfig.momentum.enabled, weightDecayFactor=nnConfig.momentum.fraction,\
                             hasDropout=nnConfig.dropout.enabled, dropoutProbability=nnConfig.dropout.probability,\
                             regularizationsOnUnshared=nnConfig.regularization.nonsharedReg,regularizationsOnShared=nnConfig.regularization.sharedReg,\
                             reluRespawn=nnConfig.reluRespawn.enabled, reluRespawnCutoff=nnConfig.reluRespawn.respawnCutoff)

    testData,testLabels,testCurrentStartPointCosmic,testCurrnetStartPointMixing = getTestBatch(1000,\
        input_tree_mixing,
        input_tree_cosmic,
        testAbsoluteStartPointMixing,\
        testCurrentStartPointMixing,\
        testAbsoluteEndPointMixing,\
        testAbsoluteStartPointCosmic,\
        testCurrentStartPointCosmic,\
        testAbsoluteEndPointCosmic,averageData)

    trainData,trainLabels,trainingCurrentStartPointCosmic, trainingCurrentStartPointMixing = getTrainingBatch(1000,\
        input_tree_mixing,
        input_tree_cosmic,
        trainingAbsoluteStartPointMixing,\
        trainingCurrentStartPointMixing,\
        trainingAbsoluteEndPointMixing,\
        trainingAbsoluteStartPointCosmic,\
        trainingCurrentStartPointCosmic,\
        trainingAbsoluteEndPointCosmic,averageData)

    with open(nnConfig.stdoutfilename,"w+") as stdoutFile:
        oldTrainError = nn.classifyTest(trainData,trainLabels)[0]
        newTestError = nn.classifyTest(testData,testLabels)[0]
        stdoutFile.write("Train Error: " +str(oldTrainError)+"\n")
        stdoutFile.write("Test Error: "+str(newTestError)+"\n")
        stdoutFile.flush()
        logging.info("Beginning training")
        for i in range(0,nnConfig.trainingEpochs):
            stdoutFile.write(str(i)+"\n")
            stdoutFile.flush()
            newTrainError=0
            newTestError=0
            mult =int(math.ceil(trainingAbsoluteEndPointMixing/500.0))
            for k in range(3*mult):
                if 3*mult<20 or (k%int(math.floor(3*mult/20))==0 and k!=0):
                    stdoutFile.write('\t'+str(k)+"\n")
                    stdoutFile.flush()
                trainData,trainLabels,trainingCurrentStartPointCosmic, trainingCurrentStartPointMixing = getTrainingBatch(1000,\
                    input_tree_mixing,
                    input_tree_cosmic,
                    trainingAbsoluteStartPointMixing,\
                    trainingCurrentStartPointMixing,\
                    trainingAbsoluteEndPointMixing,\
                    trainingAbsoluteStartPointCosmic,\
                    trainingCurrentStartPointCosmic,\
                    trainingAbsoluteEndPointCosmic,averageData)
                if k>=2*mult:
                    newTrainError += nn.classifyTest(trainData,trainLabels)[0]/mult
                if k>=2*mult:
                    testData,testLabels,testCurrentStartPointCosmic,testCurrentStartPointMixing = getTestBatch(1000,\
                        input_tree_mixing,
                        input_tree_cosmic,
                        testAbsoluteStartPointMixing,\
                        testCurrentStartPointMixing,\
                        testAbsoluteEndPointMixing,\
                        testAbsoluteStartPointCosmic,\
                        testCurrentStartPointCosmic,\
                        testAbsoluteEndPointCosmic,averageData)
                    newTestError += nn.classifyTest(testData,testLabels)[0]/mult  
                factor = 2.0/(2.0+i)
                nn.trainBatchSGD(trainData,trainLabels,1,\
                             list(reversed([thing*factor for thing in nnConfig.lRates[nnConfig.hiddenLayerCount+1:]])),\
                             list(reversed([thing*factor for thing in nnConfig.lRates[:nnConfig.hiddenLayerCount+1]])))
            stdoutFile.write("Train Error: " +str(newTrainError)+"\n")
            stdoutFile.write("Test Error: "+str(newTestError)+"\n")
            if not nnConfig.saveAll:
                nn.saveNeuralNet(nnConfig.outputFolder)
            else:
                nn.saveNeuralNet(nnConfig.outputFolder+"_"+str(i))
            stdoutFile.flush()
            oldTrainError=newTrainError
        logging.info("Completed training for %d epochs. Saving and exiting.", nnConfig.trainingEpochs)
        nn.saveNeuralNet(nnConfig.outputFolder)
    input_root_file_mixing.Close()
    input_root_file_cosmic.Close()


if __name__ == "__main__":
    args = parseArgs()
    nnConfig = config.getConfig(args)
    main(nnConfig)
