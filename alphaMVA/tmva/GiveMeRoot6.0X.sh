#!/bin/bash

VER=$1

if [ ${VER} -eq 4 ]; then
  source /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.04.18/x86_64-centos7-gcc48-opt/root/bin/thisroot.sh

  else if [ ${VER} -eq 6 ]; then
    source /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.06.08/x86_64-centos7-gcc48-opt/root/bin/thisroot.sh

    else if [ ${VER} -eq 8 ]; then
      source /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.08.06/x86_64-centos7-gcc48-opt/root/bin/thisroot.sh

      else if [ ${VER} -eq 9 ]; then
        source /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.09.02/x86_64-centos7-gcc48-opt/root/bin/thisroot.sh
	  else 
	    echo "Give me a valid number (4,6,8 or 9)"
      fi
    fi
  fi
fi


echo "You are now using root: " `root-config --version`
