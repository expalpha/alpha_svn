for i in `ls -d ../*.root`; do
    BASE=`basename ${i} .root`
    echo ".L EvaluateMVA.C
    PrintAllTMVAReport(\"${BASE}\")
    .q
    " | root -l
    echo ".L EvaluateMVA.C
    PlotAllTMVAReport(\"${BASE}\")
    .q
    " | root -l
done
