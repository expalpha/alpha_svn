#!/usr/bin/env python

from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout
from keras.regularizers import l2
#keras 1 form:
#from keras import initializations
#keras 2 form:
from keras import initializers
from keras.optimizers import SGD

# Setup the model here
num_input_nodes = 14
num_output_nodes = 2
num_hidden_layers = 3
nodes_hidden_layer = 256
l2_val = 1e-5
ModelName='14_Deep_3_model6.4'

# NOTE: Either you can use predefined initializations (see Keras documentation)
# or you can define your own initialization in such a function

#Keras 1 form:
#def normal(shape, name=None):
    #return initializations.normal(shape, scale=0.05, name=name)
#Keras 2 form:
def normal():
    return initializers.random_normal( stddev=0.05)
    #return initializers.RandomNormal(shape, stddev=0.05 )
    
model = Sequential()

# Hidden layer 1
# NOTE: Number of input nodes need to be defined in this layer
#Keras 1 style:
#model.add(Dense(nodes_hidden_layer, init=normal, activation='relu', W_regularizer=l2(l2_val), input_dim=num_input_nodes))
#Keras 2:
model.add(Dense(nodes_hidden_layer, 
                activation='sigmoid', 
                kernel_initializer=normal(),
                kernel_regularizer=l2(l2_val), input_dim=num_input_nodes))

# Hidden layer 2 to num_hidden_layers
# NOTE: Here, you can do what you want

model.add(Dense(nodes_hidden_layer, 
                    activation='relu', 
                    kernel_initializer=normal(),
                    kernel_regularizer=l2(l2_val)))
model.add(Dense(nodes_hidden_layer, 
                    activation='relu', 
                    kernel_initializer=normal(),
                    kernel_regularizer=l2(l2_val)))
'''
for k in range(num_hidden_layers-1):
	#Keras 1
	#model.add(Dense(nodes_hidden_layer, init=normal, activation='relu', W_regularizer=l2(l2_val)))
	#Keras 2
    model.add(Dense(nodes_hidden_layer, 
                    activation='relu', 
                    kernel_initializer=normal(),
                    kernel_regularizer=l2(l2_val)))
                  '''
# Ouput layer
# NOTE: Use following output types for the different tasks
# Binary classification: 2 output nodes with 'softmax' activation
# Regression: 1 output with any activation ('linear' recommended)
# Multiclass classification: (number of classes) output nodes with 'softmax' activation
model.add(Dense(num_output_nodes, init=normal(), activation='softmax'))

# Compile model
# NOTE: Use following settings for the different tasks
# Any classification: 'categorical_crossentropy' is recommended loss function
# Regression: 'mean_squared_error' is recommended loss function
model.compile(loss='categorical_crossentropy', optimizer=SGD(lr=0.001), metrics=['accuracy',])

# Save model
model.save(ModelName+'.h5')

# Additional information about the model
# NOTE: This is not needed to run the model

# Print summary
model.summary()

# Visualize model as graph
try:
	from keras.utils.vis_utils import plot_model
	plot_model(model, to_file=ModelName+'.png', show_shapes=True)
except:
    print('[INFO] Failed to make model plot')
