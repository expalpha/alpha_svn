#!/usr/bin/env python

from keras.models import Sequential, Model
from keras.layers.core import  Dense, Activation, Dropout, Flatten
from keras.layers import  Conv1D, Conv2D, concatenate, Reshape, MaxPooling2D, Cropping1D, Multiply, Concatenate
from keras.regularizers import l2
#from keras.backend import reshape
from numpy import array
import math
#keras 1 form:
#from keras import initializations
#keras 2 form:
from keras import Input,  initializers, optimizers
from keras.optimizers import SGD
from keras import backend
import sys, getopt


# Setup the model here

input_height =2
kernx=2
input_width= 72
kerny=1

HITVARS=144
TRACKNVARS=18
TRACKWIDTH=9
TRACKVARS=TRACKWIDTH*TRACKNVARS
MVAVARS=14
num_input_nodes = HITVARS + TRACKVARS + MVAVARS
num_output_nodes = 2
num_hidden_layers = 3
nodes_hidden_layer = 512

l2_val = 1e-5
NETS=['A','B','C','AB','AC','BC','ABC'];

for NETWORK in NETS:
	#NETWORK='AC'
	#ModelName='CNN20_VarAndTrack_1xTanh_'+str(num_input_nodes) + '_extraTanh_' + str(num_hidden_layers) + '_' + str( nodes_hidden_layer) + '_wDropout_model'
	ModelName='ATModel'+NETWORK+'_3_'+ str(num_input_nodes)
	def normal():
		return initializers.random_normal( stddev=0.05)


	input_arr0=Input(shape=(num_input_nodes,))
	input_arr1=Reshape((num_input_nodes,1,),input_shape=(num_input_nodes,))(input_arr0)


	#ORDER: MVAVARS, HITVARS, TRACKVARS



	CropTradVars=Cropping1D(cropping=(0,HITVARS+TRACKVARS),input_shape=(num_input_nodes,1,))(input_arr1)
	tradditional_vars=Flatten()(CropTradVars)

	phits_vars=Cropping1D(cropping=(MVAVARS,TRACKVARS),input_shape=(num_input_nodes,1,))(input_arr1)
	phits_arr=Reshape((input_height,input_width,1),input_shape=(num_input_nodes,))(phits_vars)

	track_vars=Cropping1D(cropping=(MVAVARS+HITVARS,0),input_shape=(num_input_nodes,1,))(input_arr1)


	##HITS MVA:
	kernx=2
	kerny=36
	filters=int(kernx*kerny*10)
	phits_conv_1=Conv2D(filters,(kernx,kerny),padding='same', activation='relu')(phits_arr)
	phits_max_1=MaxPooling2D((2,2))(phits_conv_1)
	kernx=2
	kerny=6
	filters=int(kernx*kerny*10)
	phits_conv_2=Conv2D(filters,(kernx,kerny),padding='same', activation='relu')(phits_max_1)
	phits_max_2=MaxPooling2D((1,2))(phits_conv_2)

	flat_tower=Flatten()(phits_max_2)
	dense1=Dense(nodes_hidden_layer, 
	            activation='relu', 
	            kernel_initializer=normal(),
	            kernel_regularizer=l2(l2_val)) (flat_tower)

	##MVA VARS:
	var_dense0=Dense(nodes_hidden_layer,
                activation='tanh',
                kernel_initializer=normal(),
                kernel_regularizer=l2(l2_val)) (tradditional_vars)
	var_dense1=Dense(nodes_hidden_layer, 
                activation='relu', 
                kernel_initializer=normal(),
                kernel_regularizer=l2(l2_val)) (var_dense0)
	var_dense2=Dense(nodes_hidden_layer, 
                activation='relu', 
                kernel_initializer=normal(),
                kernel_regularizer=l2(l2_val)) (var_dense1)
	var_dense3=Dense(nodes_hidden_layer, 
                activation='relu', 
                kernel_initializer=normal(),
                kernel_regularizer=l2(l2_val)) (var_dense2)

	##TRACK MVA:

	'''
	track_input_arr1=Reshape((TRACKVARS,1,),input_shape=(TRACKVARS,))(track_vars)
	CropFirst=Cropping1D(cropping=(0,TRACKVARS*3/4),
	input_shape=(TRACKVARS,1,))(track_input_arr1)
	FlatFirst=Flatten()(CropFirst)
	
	CropSecond=Cropping1D(cropping=(TRACKVARS/4,TRACKVARS*2/4),
	input_shape=(num_input_nodes,1,))(track_input_arr1)
	FlatSecond=Flatten()(CropSecond)

	CropThird=Cropping1D(cropping=(TRACKVARS*2/4,TRACKVARS*1/4),
	input_shape=(TRACKVARS,1,))(track_input_arr1)
	FlatThird=Flatten()(CropThird)

	CropForth=Cropping1D(cropping=(TRACKVARS*3/4,0),
	input_shape=(TRACKVARS,1,))(track_input_arr1)
	FlatForth=Flatten()(CropForth)

	FlatMix1=Multiply()([CropFirst, CropSecond])
	FlatMix2=Multiply()([CropFirst, CropThird])
	FlatMix3=Multiply()([CropFirst, CropForth])
	FlatMix=Concatenate(axis=2)([FlatMix1,FlatMix2,FlatMix3])
	'''

	track_input_arr1=Reshape((TRACKVARS,1,),input_shape=(TRACKVARS,))(track_vars)
	CropFirst=Cropping1D(cropping=(TRACKVARS*0/9,TRACKVARS*8/9),
	input_shape=(TRACKVARS,1,))(track_input_arr1)
	FlatFirst=Flatten()(CropFirst)

	CropSecond=Cropping1D(cropping=(TRACKVARS*1/9,TRACKVARS*7/9),
	input_shape=(TRACKVARS,1,))(track_input_arr1)
	FlatSecond=Flatten()(CropSecond)

	CropThird=Cropping1D(cropping=(TRACKVARS*2/9,TRACKVARS*6/9),
	input_shape=(TRACKVARS,1,))(track_input_arr1)
	FlatThird=Flatten()(CropThird)

	CropForth=Cropping1D(cropping=(TRACKVARS*3/9,TRACKVARS*5/9),
	input_shape=(TRACKVARS,1,))(track_input_arr1)
	FlatForth=Flatten()(CropForth)

	CropFifth=Cropping1D(cropping=(TRACKVARS*4/9,TRACKVARS*4/9),
	input_shape=(TRACKVARS,1,))(track_input_arr1)
	FlatFifth=Flatten()(CropFifth)

	CropSixth=Cropping1D(cropping=(TRACKVARS*5/9,TRACKVARS*3/9),
	input_shape=(TRACKVARS,1,))(track_input_arr1)
	FlatSixth=Flatten()(CropSixth)

	CropSeventh=Cropping1D(cropping=(TRACKVARS*6/9,TRACKVARS*2/9),
	input_shape=(TRACKVARS,1,))(track_input_arr1)
	FlatSeventh=Flatten()(CropSeventh)

	CropEigth=Cropping1D(cropping=(TRACKVARS*7/9,TRACKVARS*1/9),
	input_shape=(TRACKVARS,1,))(track_input_arr1)
	FlatEigth=Flatten()(CropEigth)

	CropNinth=Cropping1D(cropping=(TRACKVARS*8/9,TRACKVARS*0/9),
	input_shape=(TRACKVARS,1,))(track_input_arr1)
	FlatNinth=Flatten()(CropNinth)

	FlatMix1=Multiply()([FlatFirst, FlatSecond])
	FlatMix2=Multiply()([FlatFirst, FlatThird])
	FlatMix3=Multiply()([FlatFirst, FlatForth])
	FlatMix4=Multiply()([FlatFirst, FlatFifth])
	FlatMix5=Multiply()([FlatFirst, FlatSixth])
	FlatMix6=Multiply()([FlatFirst, FlatSeventh])
	FlatMix7=Multiply()([FlatFirst, FlatEigth])
	FlatMix8=Multiply()([FlatFirst, FlatNinth])
	FlatMix=Concatenate()([FlatMix1,FlatMix2,FlatMix3,FlatMix4,FlatMix5,FlatMix6,FlatMix7,FlatMix8])

	#######################

	ReshapedMix=Reshape((TRACKNVARS,TRACKWIDTH-1,1))(FlatMix)
	kernx=4
	kerny=TRACKWIDTH-1
	#filters=int(kernx*kerny*10)
	filters=100
	conv_1=Conv2D(filters,(kernx,1),padding='valid', activation='tanh')(ReshapedMix)
	conv_2=Conv2D(filters,(kernx,1),padding='valid', activation='relu')(conv_1)
	conv_3=Conv2D(filters,(kernx,1),padding='valid', activation='relu')(conv_2)
	FlatFinal=Flatten()(conv_3)

	track_Layer1=Dense(nodes_hidden_layer/2, 
                activation='relu', 
                kernel_initializer=normal(),
                kernel_regularizer=l2(l2_val))(FlatFinal)
	'''
	track_Layer2=Dense(nodes_hidden_layer, 
                activation='tanh', 
                kernel_initializer=normal(),
                kernel_regularizer=l2(l2_val))(FlatMix)
	track_Layer1=Dense(nodes_hidden_layer, 
                activation='relu', 
                kernel_initializer=normal(),
                kernel_regularizer=l2(l2_val))(track_Layer2)
	'''

	#track_dense1=Flatten()(track_Layer1)

	##RECOMBINE
	all_vars=concatenate([dense1, var_dense3, track_Layer1])
	AB=concatenate([dense1, var_dense3])
	AC=concatenate([dense1, track_Layer1])
	BC=concatenate([ var_dense3, track_Layer1])

	if (NETWORK=='A'):
		drop1=Dropout(0.2)(dense1)
	if (NETWORK=='B'):
		drop1=Dropout(0.2)(var_dense3)
	if (NETWORK=='C'):
		drop1=Dropout(0.2)(track_Layer1)
	if (NETWORK=='AB'):
		drop1=Dropout(0.2)(AB)
	if (NETWORK=='AC'):
		drop1=Dropout(0.2)(AC)
	if (NETWORK=='BC'):
		drop1=Dropout(0.2)(BC)
	if (NETWORK=='ABC'):
		drop1=Dropout(0.2)(all_vars)

	#drop1=Dropout(0.2)(AB)
	#drop1=Dropout(0.2)(AC)
	#drop1=Dropout(0.2)(BC)

	dense2=Dense(nodes_hidden_layer, 
                activation='relu', #//tanh for 2x
                kernel_initializer=normal(),
                kernel_regularizer=l2(l2_val)) (drop1)
	#drop2=Dropout(0.5)(dense2)
	dense3=Dense(nodes_hidden_layer, 
                activation='relu', #//tanh for 3x 
                kernel_initializer=normal(),
                kernel_regularizer=l2(l2_val)) (dense2)
	#drop3=Dropout(0.5)(dense3)
	output=Dense(num_output_nodes, activation='softmax')(dense3)
	model = Model(input_arr0,output)
	# Hidden layer 1
	# NOTE: Number of input nodes need to be defined in this layer
	#Keras 1 style:
	#model.add(Dense(nodes_hidden_layer, init=normal, activation='relu', W_regularizer=l2(l2_val), input_dim=num_input_nodes))
	#Keras 2:


	# Compile model
	# NOTE: Use following settings for the different tasks
	# Any classification: 'categorical_crossentropy' is recommended loss function
	# Regression: 'mean_squared_error' is recommended loss function

	sgd = optimizers.SGD(lr=0.0001, decay=1e-6, momentum=0.9, nesterov=True)
	#model.compile(loss='categorical_crossentropy', optimizer=SGD(lr=0.001), metrics=['accuracy',])
	model.compile(loss='categorical_crossentropy', optimizer=sgd, metrics=['accuracy',])

	# Save model
	model.save(ModelName+'.h5')

	# Additional information about the model
	# NOTE: This is not needed to run the model

	# Print summary
	model.summary()
	

	# Visualize model as graph
	
	try:
		from keras.utils.vis_utils import plot_model
		plot_model(model, to_file=ModelName+'.png', show_shapes=True)
	except:
		print('[INFO] Failed to make model plot')
		

	backend.clear_session()
