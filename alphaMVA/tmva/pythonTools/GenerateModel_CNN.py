#!/usr/bin/env python

from keras.models import Sequential, Model
from keras.layers.core import  Dense, Activation, Dropout, Flatten
from keras.layers import  Conv1D, Conv2D, concatenate, Reshape, MaxPooling2D
from keras.regularizers import l2
#from keras.backend import reshape
from numpy import array
import math
#keras 1 form:
#from keras import initializations
#keras 2 form:
from keras import Input,  initializers, optimizers
from keras.optimizers import SGD

import sys, getopt


# Setup the model here

input_height =2
kernx=2
input_width= 72
kerny=1
num_input_nodes = input_height*input_width
num_output_nodes = 2
num_hidden_layers = 3
nodes_hidden_layer = 256 

l2_val = 1e-5
ModelName='CNN12_MANYKERNS'+str(num_input_nodes) + '_Deep_' + str(num_hidden_layers) + '_' + str( nodes_hidden_layer) + '_Dropout_model'



# NOTE: Either you can use predefined initializations (see Keras documentation)
# or you can define your own initialization in such a function

#Keras 1 form:
#def normal(shape, name=None):
    #return initializations.normal(shape, scale=0.05, name=name)
#Keras 2 form:
def normal():
    return initializers.random_normal( stddev=0.05)
    #return initializers.RandomNormal(shape, stddev=0.05 )
    
#model = Sequential()

input_arr0=Input(shape=(num_input_nodes,))
input_arr=Reshape((input_height,input_width,1),input_shape=(num_input_nodes,))(input_arr0)

kernx=2
kerny=36
filters=int(kernx*kerny*10)
conv_1=Conv2D(filters,(kernx,kerny),padding='same', activation='relu')(input_arr)
max_1=MaxPooling2D((2,2))(conv_1)
#kernx=2
#kerny=12
#filters=int(kernx*kerny*10)
#conv_2=Conv2D(filters,(kernx,kerny),padding='same', activation='relu')(max_1)
#max_2=MaxPooling2D((1,2))(conv_2)

kernx=2
kerny=2
filters=int(kernx*kerny*10)
conv_3=Conv2D(filters,(kernx,kerny),padding='same', activation='relu')(max_1)
max_3=MaxPooling2D((1,2))(conv_3)
flat_tower=Flatten()(max_3)
'''kernx=1
kerny=2
filters=int(math.ceil((num_input_nodes/(kernx*kerny))))
conv_1=Conv2D(filters,(kernx,kerny),padding='same', activation='relu')(input_arr)
max_1=MaxPooling2D((2,2))(conv_1)
tower_1=Flatten()(max_1)
kernx=2
kerny=10
filters=int(math.ceil((num_input_nodes/(kernx*kerny))))
conv_2=Conv2D(filters,(kernx,kerny),padding='same', activation='relu')(input_arr)
max_2=MaxPooling2D((1,2))(conv_2)
tower_2=Flatten()(max_2)
kernx=2
kerny=12
filters=int(math.ceil((num_input_nodes/(kernx*kerny))))
conv_3=Conv2D(filters,(kernx,kerny),padding='same', activation='relu')(input_arr)
max_3=MaxPooling2D((1,2))(conv_3)
tower_3=Flatten()(max_3)
flat_tower= concatenate([tower_1, tower_2, tower_3])#,axis=1)
'''
#filters=int(math.ceil((num_input_nodes/(kernx*kerny))))
#conv=Conv2D(filters,(kernx,kerny),padding='valid', activation='relu')(input_arr)
#tower_mix=MaxPooling2D((1,2))(conv)
#flat_tower=Flatten()(tower_mix)
dense1=Dense(nodes_hidden_layer, 
                activation='relu', 
                kernel_initializer=normal(),
                kernel_regularizer=l2(l2_val)) (flat_tower)
dense2=Dense(nodes_hidden_layer, 
                activation='relu', 
                kernel_initializer=normal(),
                kernel_regularizer=l2(l2_val)) (dense1)
output=Dense(num_output_nodes, activation='softmax')(dense2)
model = Model(input_arr0,output)
# Hidden layer 1
# NOTE: Number of input nodes need to be defined in this layer
#Keras 1 style:
#model.add(Dense(nodes_hidden_layer, init=normal, activation='relu', W_regularizer=l2(l2_val), input_dim=num_input_nodes))
#Keras 2:


# Compile model
# NOTE: Use following settings for the different tasks
# Any classification: 'categorical_crossentropy' is recommended loss function
# Regression: 'mean_squared_error' is recommended loss function

sgd = optimizers.SGD(lr=0.0001, decay=1e-6, momentum=0.9, nesterov=True)
#model.compile(loss='categorical_crossentropy', optimizer=SGD(lr=0.001), metrics=['accuracy',])
model.compile(loss='categorical_crossentropy', optimizer=sgd, metrics=['accuracy',])

# Save model
model.save(ModelName+'.h5')

# Additional information about the model
# NOTE: This is not needed to run the model

# Print summary
model.summary()

# Visualize model as graph
try:
	from keras.utils.vis_utils import plot_model
	plot_model(model, to_file=ModelName+'.png', show_shapes=True)
except:
    print('[INFO] Failed to make model plot')
