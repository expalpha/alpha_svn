#!/usr/bin/env python

from keras.models import Sequential, Model
from keras.layers.core import  Dense, Activation, Dropout, Flatten
from keras.layers import  Conv1D, Conv2D, concatenate, Reshape, MaxPooling2D, Cropping2D, ZeroPadding2D
from keras.regularizers import l2
#from keras.backend import reshape
from numpy import array
import math
import tensorflow as tf
#keras 1 form:
#from keras import initializations
#keras 2 form:
from keras import Input,  initializers, optimizers
from keras.optimizers import SGD

import sys, getopt


# Setup the model here

input_height =2
kernx=2
input_width= 72
kerny=1
num_input_nodes = input_height*input_width
num_output_nodes = 2
num_hidden_layers = 3
nodes_hidden_layer = 256 

l2_val = 1e-5
ModelName='CNN4_split'+str(num_input_nodes) + '_Deep_' + str(num_hidden_layers) + '_' + str( nodes_hidden_layer) + '_Dropout_model'



# NOTE: Either you can use predefined initializations (see Keras documentation)
# or you can define your own initialization in such a function

#Keras 1 form:
#def normal(shape, name=None):
    #return initializations.normal(shape, scale=0.05, name=name)
#Keras 2 form:
def normal():
    return initializers.random_normal( stddev=0.05)
    #return initializers.RandomNormal(shape, stddev=0.05 )
    
#model = Sequential()

input_arr0=Input(shape=(num_input_nodes,))
#print(input_arr0._keras_history)

input_arr1=Reshape((input_height,input_width,1),input_shape=(num_input_nodes,))(input_arr0)
input_arr=MaxPooling2D(2,1)(input_arr1)
#print(input_arr._keras_history)
#l1_us, l2_us, l3_us, l1_ds, l2_ds, l3_ds = tf.split(input_arr, [10, 12, 14, 10, 12, 14],2)
#l1_us=l1_us(input_arr)
#print(l1_us)

l1_pad=9
l2_pad=11
l3_pad=13
#Split layers and add padding (make padding reflect cylindrical nature of layer, ie cat two together before conv)
l1_us=Cropping2D(cropping=((0,0),    (0,(          12+14+10+12+14))))(input_arr)
l1_us_pad=Cropping2D(cropping=((0,0),(0,(10-l1_pad+12+14+10+12+14))))(input_arr)
l2_us=Cropping2D(cropping=((0,0),    (10,(          14+10+12+14))))(input_arr)
l2_us_pad=Cropping2D(cropping=((0,0),(10,(12-l2_pad+14+10+12+14))))(input_arr)
l3_us=Cropping2D(cropping=((0,0),    ((10+12),(          10+12+14))))(input_arr)
l3_us_pad=Cropping2D(cropping=((0,0),((10+12),(14-l3_pad+10+12+14))))(input_arr)

l1_ds=Cropping2D(cropping=((0,0),    ((10+12+14),(          12+14))))(input_arr)
l1_ds_pad=Cropping2D(cropping=((0,0),((10+12+14),(10-l1_pad+12+14))))(input_arr)
l2_ds=Cropping2D(cropping=((0,0),    ((10+12+14+10),(          14))))(input_arr)
l2_ds_pad=Cropping2D(cropping=((0,0),((10+12+14+10),(12-l2_pad+14))))(input_arr)
l3_ds=Cropping2D(cropping=((0,0),    ((10+12+14+10+12),(          0))))(input_arr)
l3_ds_pad=Cropping2D(cropping=((0,0),((10+12+14+10+12),(14-l3_pad+0))))(input_arr)

#Cat to make cylinder
l1_us_cyl=concatenate([l1_us, l1_us_pad],axis=2)
l2_us_cyl=concatenate([l2_us, l2_us_pad],axis=2)
l3_us_cyl=concatenate([l3_us, l3_us_pad],axis=2)
l1_ds_cyl=concatenate([l1_ds, l1_ds_pad],axis=2)
l2_ds_cyl=concatenate([l2_ds, l2_ds_pad],axis=2)
l3_ds_cyl=concatenate([l3_ds, l3_ds_pad],axis=2)

#l1_us=concatenate([input_arr[:,:,0:10,:],input_arr[:,:,0:1,:]],axis=2)
print(l1_us)
#print(l1_us._keras_history)
l1_filters=300
l2_filters=300
l3_filters=300
arrwidth=1
l1_us_conv=Conv2D(l1_filters,(arrwidth,l1_pad+1),padding='valid', activation='relu')(l1_us_cyl)
l2_us_conv=Conv2D(l2_filters,(arrwidth,l2_pad+1),padding='valid', activation='relu')(l2_us_cyl)
l3_us_conv=Conv2D(l3_filters,(arrwidth,l3_pad+1),padding='valid', activation='relu')(l3_us_cyl)
l1_ds_conv=Conv2D(l1_filters,(arrwidth,l1_pad+1),padding='valid', activation='relu')(l1_ds_cyl)
l2_ds_conv=Conv2D(l2_filters,(arrwidth,l2_pad+1),padding='valid', activation='relu')(l2_ds_cyl)
l3_ds_conv=Conv2D(l3_filters,(arrwidth,l3_pad+1),padding='valid', activation='relu')(l3_ds_cyl)

l1_us_square=ZeroPadding2D(padding=(0, 2))(l1_us_conv)
l2_us_square=ZeroPadding2D(padding=(0, 1))(l2_us_conv)
l3_us_square=ZeroPadding2D(padding=(0, 0))(l3_us_conv)
l1_ds_square=ZeroPadding2D(padding=(0, 2))(l1_ds_conv)
l2_ds_square=ZeroPadding2D(padding=(0, 1))(l2_ds_conv)
l3_ds_square=ZeroPadding2D(padding=(0, 0))(l3_ds_conv)
tower_2d=concatenate([l1_us_square, l2_us_square, l3_us_square, l1_ds_square, l2_ds_square, l3_ds_square],axis=1)
#flat_tower= concatenate([l1_us_conv, l1_ds_conv, l2_us_conv, l2_ds_conv, l3_us_conv, l3_ds_conv])#,axis=1) 
#filters=int(math.ceil((num_input_nodes/(kernx*kerny))))
#conv=Conv2D(filters,(kernx,kerny),padding='valid', activation='relu')(input_arr)
#tower_mix=MaxPooling2D((1,2))(conv)
#flat_tower=Flatten()(tower_mix)
#max_2d=MaxPooling2D(2,2)(tower_2d)
final_conv=Conv2D(500,(6,6),padding='valid')(tower_2d)
flat_tower=Flatten()(final_conv)
dense1=Dense(nodes_hidden_layer, 
                activation='relu', 
                kernel_initializer=normal(),
                kernel_regularizer=l2(l2_val)) (flat_tower)
dense2=Dense(nodes_hidden_layer, 
                activation='relu', 
                kernel_initializer=normal(),
                kernel_regularizer=l2(l2_val)) (dense1)
dense3=Dense(nodes_hidden_layer,
                activation='relu',
                kernel_initializer=normal(),
                kernel_regularizer=l2(l2_val)) (dense2)
output=Dense(num_output_nodes, activation='softmax')(dense3)
model = Model(input_arr0,output)
# Hidden layer 1
# NOTE: Number of input nodes need to be defined in this layer
#Keras 1 style:
#model.add(Dense(nodes_hidden_layer, init=normal, activation='relu', W_regularizer=l2(l2_val), input_dim=num_input_nodes))
#Keras 2:


# Compile model
# NOTE: Use following settings for the different tasks
# Any classification: 'categorical_crossentropy' is recommended loss function
# Regression: 'mean_squared_error' is recommended loss function

sgd = optimizers.SGD(lr=0.0001, decay=1e-6, momentum=0.9, nesterov=True)
#model.compile(loss='categorical_crossentropy', optimizer=SGD(lr=0.001), metrics=['accuracy',])
model.compile(loss='categorical_crossentropy', optimizer=sgd, metrics=['accuracy',])

# Save model
model.save(ModelName+'.h5')

# Additional information about the model
# NOTE: This is not needed to run the model

# Print summary
model.summary()

# Visualize model as graph
try:
	from keras.utils.vis_utils import plot_model
	plot_model(model, to_file=ModelName+'.png', show_shapes=True)
except:
    print('[INFO] Failed to make model plot')
