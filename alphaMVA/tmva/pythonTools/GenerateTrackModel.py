#!/usr/bin/env python

from keras.models import Sequential, Model
from keras.layers.core import Dense, Activation, Dropout, Reshape, Flatten
from keras.layers import Multiply, Cropping1D, Concatenate
from keras.regularizers import l2
#keras 1 form:
#from keras import initializations
#keras 2 form:
from keras import Input, initializers
from keras.optimizers import SGD

# Setup the model here
num_input_nodes = 3*4
num_output_nodes = 2
num_hidden_layers = 3
nodes_hidden_layer = 256
Activation1='relu'
Activation2='relu'
Activation3='relu'
l2_val = 1e-5
ModelName='TrackNet_Multiply3_'+ Activation1[0]+ Activation2[0]+ Activation3[0]+'_' + str(num_input_nodes) + '_Deep_' + str(num_hidden_layers) + 'x' + str(nodes_hidden_layer) + '_model'

# NOTE: Either you can use predefined initializations (see Keras documentation)
# or you can define your own initialization in such a function

#Keras 1 form:
#def normal(shape, name=None):
    #return initializations.normal(shape, scale=0.05, name=name)
#Keras 2 form:
def normal():
    return initializers.random_normal( stddev=0.05)
    #return initializers.RandomNormal(shape, stddev=0.05 )
    
#model = Sequential()


input_arr0=Input(shape=(num_input_nodes,))

input_arr1=Reshape((num_input_nodes,1,),input_shape=(num_input_nodes,))(input_arr0)
CropFirst=Cropping1D(cropping=(0,num_input_nodes*2/3),
input_shape=(num_input_nodes,1,))(input_arr1)
FlatFirst=Flatten()(CropFirst)

CropSecond=Cropping1D(cropping=(num_input_nodes/3,num_input_nodes/3),
input_shape=(num_input_nodes,1,))(input_arr1)
FlatSecond=Flatten()(CropSecond)

CropThird=Cropping1D(cropping=(num_input_nodes*2/3,0),
input_shape=(num_input_nodes,1,))(input_arr1)
FlatThird=Flatten()(CropThird)

FlatMix1=Multiply()([FlatFirst, FlatSecond])
FlatMix2=Multiply()([FlatFirst, FlatThird])
FlatMix=Concatenate()([FlatMix1,FlatMix2])
kernx=2

# Hidden layer 1
# NOTE: Number of input nodes need to be defined in this layer
#Keras 1 style:
#model.add(Dense(nodes_hidden_layer, init=normal, activation='relu', W_regularizer=l2(l2_val), input_dim=num_input_nodes))
#Keras 2:
Layer1=Dense(nodes_hidden_layer, 
                activation=Activation1, 
                kernel_initializer=normal(),
                kernel_regularizer=l2(l2_val))(FlatMix)

# Hidden layer 2 to num_hidden_layers
# NOTE: Here, you can do what you want

Layer2=Dense(nodes_hidden_layer, 
                    activation=Activation2, 
                    kernel_initializer=normal(),
                    kernel_regularizer=l2(l2_val))(Layer1)
Layer3=Dense(nodes_hidden_layer, 
                    activation=Activation3, 
                    kernel_initializer=normal(),
                    kernel_regularizer=l2(l2_val))(Layer2)
'''
for k in range(num_hidden_layers-1):
	#Keras 1
	#model.add(Dense(nodes_hidden_layer, init=normal, activation='relu', W_regularizer=l2(l2_val)))
	#Keras 2
    model.add(Dense(nodes_hidden_layer, 
                    activation='relu', 
                    kernel_initializer=normal(),
                    kernel_regularizer=l2(l2_val)))
                  '''
# Ouput layer
# NOTE: Use following output types for the different tasks
# Binary classification: 2 output nodes with 'softmax' activation
# Regression: 1 output with any activation ('linear' recommended)
# Multiclass classification: (number of classes) output nodes with 'softmax' activation
#model.add(Dense(num_output_nodes, init=normal(), activation='softmax'))
output=Dense(num_output_nodes, activation='softmax')(Layer3)
model = Model(input_arr0,output)
# Compile model
# NOTE: Use following settings for the different tasks
# Any classification: 'categorical_crossentropy' is recommended loss function
# Regression: 'mean_squared_error' is recommended loss function
model.compile(loss='categorical_crossentropy', optimizer=SGD(lr=0.001), metrics=['accuracy',])

# Save model
model.save(ModelName+'.h5')

# Additional information about the model
# NOTE: This is not needed to run the model

# Print summary
model.summary()

# Visualize model as graph
try:
	from keras.utils.vis_utils import plot_model
	plot_model(model, to_file=ModelName+'.png', show_shapes=True)
except:
    print('[INFO] Failed to make model plot')
