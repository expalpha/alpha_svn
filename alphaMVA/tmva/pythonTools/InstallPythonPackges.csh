#!/bin/csh

##TODO: ADD CVMFS PATH
if ( -d ~/packages/pythonEnv ) then
  echo "pythonEnv folder found..."
  virtualenv --system-site-packages ~/packages/pythonEnv
  source ~/packages/pythonEnv/bin/activate.csh
else
  mkdir -p ~/packages/pythonEnv
  virtualenv --system-site-packages ~/packages/pythonEnv
  source ~/packages/pythonEnv/bin/activate.csh
  pip install --upgrade pip
  pip install --upgrade tensorflow 
  pip install theano
  pip install keras
  pip install h5py
  pip install pypng
  pip install sklearn
endif

  


