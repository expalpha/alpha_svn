#!/bin/bash

##TODO: ADD CVMFS PATH
if [ -d ~/packages/pythonEnv ]; then
  echo "pythonEnv folder found..."
  virtualenv --system-site-packages ~/packages/pythonEnv
  . ~/packages/pythonEnv/bin/activate
else
  mkdir -p ~/packages/pythonEnv
  virtualenv --system-site-packages ~/packages/pythonEnv
  . ~/packages/pythonEnv/bin/activate
  pip install --upgrade pip
  pip install --upgrade tensorflow 
  pip install theano
  pip install keras
  pip install h5py
  pip install pypng
  pip install sklearn
fi  
  


