#!/bin/bash
ARR=$1

  echo ",,,,,,,,,
,,,,,,,,Title,\"=CONCATENATE(TRIM(RIGHT(SUBSTITUTE(B20,\"\"_\"\",REPT(\"\" \"\",100)),100)))\"
,,,,,,,,,
,,,,,,,,=C20,=AVERAGE(I20:I31)
,,,,,,,,=C34,=AVERAGE(I34:I45)
,,,,,,,,=C48,=AVERAGE(I48:I59)
,,,,,,,,=C62,=AVERAGE(I62:I73)
,,,,,,,,=C76,=AVERAGE(I76:I87)
,,,,,,,,Total (without BDTG),=AVERAGE(J4:J8)
,,,,,,,,Total,=AVERAGE(J4:J8)
,,,,,,,,,
,,,,,,,,,
,,,,,,,,,
,,,,,,,,,
,,,,,,,,,
,,,,,,,,,
,,,,,,,,,
,,,,,,,,SignalEff,
,MVAName,Method,RFCut,Suppression,,Bkg Rate (Hz),,\"=CONCATENATE(TRIM(RIGHT(SUBSTITUTE(B20,\"\"_\"\",REPT(\"\" \"\",100)),100)),\"\"-\"\",C20)\"," > Summary_${ARR}.csv
  for line in `cat ${ARR}.Evaluation_BDT.csv`; do
    echo ",${line}" | grep BDT >>  Summary_${ARR}.csv
  done
  echo ",,,,,,,,,
,MVAName,Method,RFCut,Suppression,,Bkg Rate (Hz),,\"=CONCATENATE(TRIM(RIGHT(SUBSTITUTE(B34,\"\"_\"\",REPT(\"\" \"\",100)),100)),\"\"-\"\",C34)\"," >>  Summary_${ARR}.csv
  for line in `cat ${ARR}.Evaluation_BDTB.csv`; do
    echo ",${line}" | grep BDTB >>  Summary_${ARR}.csv
  done
  echo ",,,,,,,,,
,MVAName,Method,RFCut,Suppression,,Bkg Rate (Hz),,\"=CONCATENATE(TRIM(RIGHT(SUBSTITUTE(B34,\"\"_\"\",REPT(\"\" \"\",100)),100)),\"\"-\"\",C48)\"," >>  Summary_${ARR}.csv
  for line in `cat ${ARR}.Evaluation_BDTD.csv`; do
    echo ",${line}" | grep BDTD >>  Summary_${ARR}.csv
  done
  echo ",,,,,,,,,
,MVAName,Method,RFCut,Suppression,,Bkg Rate (Hz),,\"=CONCATENATE(TRIM(RIGHT(SUBSTITUTE(B34,\"\"_\"\",REPT(\"\" \"\",100)),100)),\"\"-\"\",C62)\"," >>  Summary_${ARR}.csv
  for line in `cat ${ARR}.Evaluation_BDTF.csv`; do
    echo ",${line}" | grep BDTF >>  Summary_${ARR}.csv
  done
  echo ",,,,,,,,,
,MVAName,Method,RFCut,Suppression,,Bkg Rate (Hz),,\"=CONCATENATE(TRIM(RIGHT(SUBSTITUTE(B34,\"\"_\"\",REPT(\"\" \"\",100)),100)),\"\"-\"\",C76)\"," >>  Summary_${ARR}.csv
  for line in `cat ${ARR}.Evaluation_BDTG.csv`; do
    echo ",${line}" | grep BDTG >>  Summary_${ARR}.csv
  done
