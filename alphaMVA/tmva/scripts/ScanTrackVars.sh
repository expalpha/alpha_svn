#!/bin/bash
#NTRACKS=$1
cd ..
#VarList="phi_S0axisraw S0axisrawZ S0rawPerp residual nhits phi r nCT nGT"
VarList="phi_S0axisraw S0axisrawZ S0rawPerp residual nhits phi r"
#VarList="residual r phi_S0axisraw nhits nCT curvemean S0rawPerp nhitsasymraw S0axisZ"
#VarList="residual r phi_S0axisraw nhits nCT curvemean S0rawPerp curvemin"
#VarList="residual r phi_S0axisraw curvemean"

#TESTSIG="../../trackDumper/mergeroot/merged_${NTRACKS}_tracks_mix2_SeptMix.list0.root"
SIGPHASE="mix2"
#TESTBKG="../../trackDumper/mergeroot/merged_${NTRACKS}_tracks_cosm_SeptCosmics1.root"
BKGPHASE="cosm"
#NewVarList=`cat AdditionalVars.list`
#NewVarList=`cat AdditionalVars_sorted.list`
#NewVarList="`seq 2 5 `"
#| tac`"

echo "VarList: $VarList"

CommandsToRun=""

#function RunWithVar
#{
  LocalTRACKS=$1
  
  OUTPUTFILE="${LocalTRACKS}tracks"
  
TRAINSIG="../../trackDumper/mergeroot/merged_${LocalTRACKS}_tracks_mix2_SeptMix.list0.root"
TRAINBKG="../../trackDumper/mergeroot/merged_${LocalTRACKS}_tracks_cosm_SeptCosmics1.root"

  for i in `seq 1 ${LocalTRACKS}`; do
    Index=$((${i}-1));
    VarList="$VarList track${Index}_fc track${Index}_fphi0 track${Index}_fLambda" 
  done

  #echo "${j} is new"
  #./alphaClassification --VarList phi_S0axisraw,S0axisrawZ,S0rawPerp,residual,nhits,phi,r,nCT,nGT, --signal merged_mix2.root mix2 --background merged_cosm.root cosm
  Command="./alphaClassification --VarList "
  #VARLISTFULL="${VarList}"
  for ii in $VarList; do
    VARLISTFULL="${VARLISTFULL}${ii},"
  #  OUTPUTFILE="${OUTPUTFILE}_${ii}"
  done
  #VARLISTFULL="${VARLISTFULL}${j}"
  Command="${Command}${VARLISTFULL}"
  Command="${Command}  --signal $TRAINSIG mix2 --background $TRAINBKG cosm --output ${OUTPUTFILE}.root BDTB BDTD DNN Cuts"
      
  echo "${Command}"
  sleep 1
  ${Command} &> ${OUTPUTFILE}.train.log 
  cd macros
  for j in BDT BDTG BDTB BDTD BDTF; do
    echo ".L EvaluateMVA.C
    PrintTMVAReport(\"${OUTPUTFILE}\",\"${j}\")
    .q
    " | root -l -b
    echo "$line" | awk -F ',' '{print $3}'
    RFCUT=`echo "${line}" | awk -F ',' '{print $3}'`
    echo ".L TestMVA.C+
    RunTest(\"${OUTPUTFILE}\",\"${j}\",\"${TESTSIG}\",\"${SIGPHASE}\",\"${TESTBKG}\",\"${BKGPHASE}\")
    .q
    " | root -l -b
    echo " "
  done
  cd ../



  echo ",,,,,,,,,
,,,,,,,,Title,\"=CONCATENATE(TRIM(RIGHT(SUBSTITUTE(B20,\"\"_\"\",REPT(\"\" \"\",100)),100)))\",,Test Sample:,,Evaluation - Test Difference,,Difference (%),
,,,,,,,,,
,,,,,,,,=C20,=AVERAGE(I20:I31),,=AVERAGE(G96:G107),,=ABS(L4-J4),,=100*N4/L4
,,,,,,,,=C34,=AVERAGE(I34:I45),,=AVERAGE(G110:G121),,=ABS(L5-J5),,=100*N5/L5
,,,,,,,,=C48,=AVERAGE(I48:I59),,=AVERAGE(G124:G135),,=ABS(L6-J6),,=100*N6/L6
,,,,,,,,=C62,=AVERAGE(I62:I73),,=AVERAGE(G138:G149),,=ABS(L7-J7),,=100*N7/L7
,,,,,,,,=C76,=AVERAGE(I76:I87),,=AVERAGE(G152:G163),,=ABS(L8-J8),,=100*N8/L8
,,,,,,,,Total (without BDTG),=AVERAGE(J4:J7),,=AVERAGE(L4:L7),,=AVERAGE(N4:N7),,=AVERAGE(P4:P7)
,,,,,,,,Total,=AVERAGE(J4:J8),,=AVERAGE(L4:L8),,=AVERAGE(N4:N8),,=AVERAGE(P4:P8)
,,,,,,,,,
,,,,,,,,,
,,,,,,,,,
,,,,,,,,,
,,,,,,,,,
,,,,,,,,,
,,,,,,,,,
,,,,,,,,SignalEff,
,MVAName,Method,RFCut,Suppression,,Bkg Rate (Hz),,\"=CONCATENATE(TRIM(RIGHT(SUBSTITUTE(B20,\"\"_\"\",REPT(\"\" \"\",100)),100)),\"\"-\"\",C20)\"," > Summary_${OUTPUTFILE}.csv
  for line in `cat macros/${OUTPUTFILE}.Evaluation_BDT.csv`; do
    echo ",${line}" | grep BDT >>  Summary_${OUTPUTFILE}.csv
  done
  echo ",,,,,,,,,
,MVAName,Method,RFCut,Suppression,,Bkg Rate (Hz),,\"=CONCATENATE(TRIM(RIGHT(SUBSTITUTE(B34,\"\"_\"\",REPT(\"\" \"\",100)),100)),\"\"-\"\",C34)\"," >>  Summary_${OUTPUTFILE}.csv
  for line in `cat macros/${OUTPUTFILE}.Evaluation_BDTB.csv`; do
    echo ",${line}" | grep BDTB >>  Summary_${OUTPUTFILE}.csv
  done
  echo ",,,,,,,,,
,MVAName,Method,RFCut,Suppression,,Bkg Rate (Hz),,\"=CONCATENATE(TRIM(RIGHT(SUBSTITUTE(B34,\"\"_\"\",REPT(\"\" \"\",100)),100)),\"\"-\"\",C48)\"," >>  Summary_${OUTPUTFILE}.csv
  for line in `cat macros/${OUTPUTFILE}.Evaluation_BDTD.csv`; do
    echo ",${line}" | grep BDTD >>  Summary_${OUTPUTFILE}.csv
  done
  echo ",,,,,,,,,
,MVAName,Method,RFCut,Suppression,,Bkg Rate (Hz),,\"=CONCATENATE(TRIM(RIGHT(SUBSTITUTE(B34,\"\"_\"\",REPT(\"\" \"\",100)),100)),\"\"-\"\",C62)\"," >>  Summary_${OUTPUTFILE}.csv
  for line in `cat macros/${OUTPUTFILE}.Evaluation_BDTF.csv`; do
    echo ",${line}" | grep BDTF >>  Summary_${OUTPUTFILE}.csv
  done
  echo ",,,,,,,,, 
,MVAName,Method,RFCut,Suppression,,Bkg Rate (Hz),,\"=CONCATENATE(TRIM(RIGHT(SUBSTITUTE(B34,\"\"_\"\",REPT(\"\" \"\",100)),100)),\"\"-\"\",C76)\"," >>  Summary_${OUTPUTFILE}.csv
  for line in `cat macros/${OUTPUTFILE}.Evaluation_BDTG.csv`; do
    echo ",${line}" | grep BDTG >>  Summary_${OUTPUTFILE}.csv
  done
echo ",,,,,,,,,">>Summary_${OUTPUTFILE}.csv
echo "Testing:">>Summary_${OUTPUTFILE}.csv
echo ",,,,,,,,,,,
Testing signal set:,${TESTSIG},${SIGPHASE}
Testing background set:,${TESTBKG},${BKGPHASE}
,,,,,
,,,,,
,MVAName,Method,RFCut,Background Rate (Hz),Error,Signal Efficiency, Error">>Summary_${OUTPUTFILE}.csv
  for line in `cat macros/${OUTPUTFILE}.Test_BDT.csv`; do
    echo ",${line}" | grep BDT >>  Summary_${OUTPUTFILE}.csv
  done
echo ",,,,,,,
,MVAName,Method,RFCut,Background Rate (Hz),Error,Signal Efficiency, Error">>Summary_${OUTPUTFILE}.csv
  for line in `cat macros/${OUTPUTFILE}.Test_BDTB.csv`; do
    echo ",${line}" | grep BDTB >>  Summary_${OUTPUTFILE}.csv
  done
echo ",,,,,,,
,MVAName,Method,RFCut,Background Rate (Hz),Error,Signal Efficiency, Error">>Summary_${OUTPUTFILE}.csv
  for line in `cat macros/${OUTPUTFILE}.Test_BDTD.csv`; do
    echo ",${line}" | grep BDTD >>  Summary_${OUTPUTFILE}.csv
  done
echo ",,,,,,,
,MVAName,Method,RFCut,Background Rate (Hz),Error,Signal Efficiency, Error">>Summary_${OUTPUTFILE}.csv
  for line in `cat macros/${OUTPUTFILE}.Test_BDTF.csv`; do
    echo ",${line}" | grep BDTF >>  Summary_${OUTPUTFILE}.csv
  done
echo ",,,,,,,
,MVAName,Method,RFCut,Background Rate (Hz),Error,Signal Efficiency, Error">>Summary_${OUTPUTFILE}.csv
  for line in `cat macros/${OUTPUTFILE}.Test_BDTG.csv`; do
    echo ",${line}" | grep BDTG >>  Summary_${OUTPUTFILE}.csv
  done

  sleep 5
     
  #    export CommandsToRun="${CommandToRun}
  #    ${Command}"
      




#for j in $NewVarList; do
#  jobcnt=(`jobs -p`)
#  #echo "Jobcount ${jobcnt[@]}"
#  if [ ${#jobcnt[@]} -lt `nproc` ] ; then
#    echo "Starting Job with for ${j} "
#    RunWithVar ${j} #&
#  else
#    echo "waiting"
#    while [ ${#jobcnt[@]} -ge `nproc` ]; do
#      sleep 30
#      echo "`nproc` jobs running... sleeping 30s"
#      jobcnt=(`jobs -p`)
#    done
#    echo "Running strips $RUNNO "
#    RunWithVar ${j} #&
#  fi
#done
