#!/bin/bash

NT=$1
NF=$2
NE=$3
#MVANAME=$1
if [ -n "$MVANAME" ]; then
  cd ${RELEASE}/alphaMVA/${MVANAME}
  
fi

rm -f PRIMout/myPRIMoutput_"$NT"_"$NF"_"$NE".txt

#term
n=1
#ratio
r=47
#initial value
y=0.99
#10 terms
while [ $n -le 10 ]
do
    echo " $n --> $y"
    echo "---start---" >> PRIMout/myPRIMoutput_"$NT"_"$NF"_"$NE".txt 
    echo >> PRIMout/myPRIMoutput_"$NT"_"$NF"_"$NE".txt 
    echo " $n --> $y" >> PRIMout/myPRIMoutput_"$NT"_"$NF"_"$NE".txt

# Bump Hunter
#-a input ascii file mode
#-b requested number of bumps (def=1)
#-n minimal number of events per bump (def=1)
#-x max fraction of events peeled off in one try
#-c criterion for optimization
#-f store trained bump hunter to file
#-t read validation/test data from a file
#-o output Tuple file
#-p output file to store validation/test data
#-z exclude input variables from the list
#    SprBumpHunterApp -a 7 -b 1 -n 100 -x $y -c 9 -f bump.spr -t valrf.out -o validation.root -p validation.root testrf.out -z "`cat opt_randforest.txt | sed -n 's/Leaves: //p' | sed -e 's/ /,/g'`"
    SprBumpHunterApp -a 7 -b 1 -n 100 -x $y -c 9 -f bump_"$NT"_"$NF"_"$NE".spr -t WriterOut/valrf_"$NT"_"$NF"_"$NE".out -o WriterOut/validation_"$NT"_"$NF"_"$NE".root -p WriterOut/validation_"$NT"_"$NF"_"$NE".root WriterOut/testrf_"$NT"_"$NF"_"$NE".out -z "`cat opt_randforest.txt | sed -n 's/Leaves: //p' | sed -e 's/ /,/g'`"
    
    cat bump_"$NT"_"$NF"_"$NE".spr >> PRIMout/myPRIMoutput_"$NT"_"$NF"_"$NE".txt
    
    echo "---stop ---" >> PRIMout/myPRIMoutput_"$NT"_"$NF"_"$NE".txt 
    echo " " >> PRIMout/myPRIMoutput_"$NT"_"$NF"_"$NE".txt    

    n=$((n+1))
    y=$(echo "$y*$r/100" | bc -l)
done
