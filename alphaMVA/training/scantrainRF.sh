#!/bin/bash


rm -f myRFoutput.txt
rm -f completed.info
if [ -n "$MVANAME" ]; then
 cd "${RELEASE}/alphaMVA/${MVANAME}"
fi

mkdir -p RFout
mkdir -p PRIMout
mkdir -p WriterOut
mkdir -p rfoutput


for nt in 100 300 500; do
  for nf in 1 2 4 8; do
	for ne in 16 8 4 2 1; do
      if [ -n "$MVANAME" ]; then
        echo "scantrainRF.sh Using folder: $RELEASE/alphaMVA/$MVANAME"
        cd "$RELEASE/alphaMVA/$MVANAME"
      fi
	  echo "-->" $nt $nf $ne
# RF with bagging (Bootstrap AGGregatING) [training_data_file]
#-n number of Bagger training cycles
#-l minimal number of entries per tree leaf (def=1)
#-s max number of sampled features (def=0 no sampling)
#-c criterion for decision tree optimization (def=5) 5 = Gini index 
#-C criterion to be monitored on test data (def=1) 9 = Punzi's sensitivity s/(0.5*nSigma+sqrt(b))
#-g per-event loss for (cross-)validation 1 - quadratic loss (y-f(x))^2
#-f store trained Bagger to file
#-t read validation/test data from a file
#-d frequency of print-outs for validation data
	   SprBaggerDecisionTreeApp -n $nt -l $ne -s $nf -c 5 -C 9 -g 1 -f randforest.spr -t val_randforest.txt -d 5 opt_randforest.txt

      if [ 1 -eq 1 ]; then
# Save Output [list_of_classifier_config_files input_data_file output_tuple_file]
#-A save output data in ascii instead of Root
#-C output classifier names (in quotes, separated by commas)    
# write ASCII
	    SprOutputWriterApp -A -C 'rf' 'randforest.spr' val_randforest.txt valrf.out
	    SprOutputWriterApp -A -C 'rf' 'randforest.spr' test_randforest.txt testrf.out
# write ROOT
	    SprOutputWriterApp -C 'rf' 'randforest.spr' val_randforest.txt valrf.root
	    SprOutputWriterApp -C 'rf' 'randforest.spr' test_randforest.txt testrf.root

	    # evaluate RF performance: Bump Hunter
	    cd $RELEASE/alphaMVA/training
	    ./PRIMRF.sh
        # cd "$RELEASE/alphaMVA/randforest/$MVANAME"
		cd -
	    maxval=$(grep FOM myPRIMoutput.txt | awk '{print $5,$0}' | sed 's/FOM=//' | sort -g | tail -1 | awk '{print $6,$7,$8,$9,$10,$11}')
	    line=$(grep -n FOM myPRIMoutput.txt | awk '{print $5,$0}' | sed 's/FOM=//' | sort -g | tail -1 | awk '{print $2}' | sed 's/:Bump//')
	    RFcut=$(head -$((line+1)) myPRIMoutput.txt | tail -1 | awk '{print $2,$4}')
      else #Development tests: To use it, set [ 1 -eq 0 ] above
        cd $RELEASE/alphaMVA
        ${RELEASE}/alphaMVA/randforest/applyRF.exe ${MVANAME}splitroot/valbkg.root ${MVANAME}rfoutput/valbkg.root ${MVANAME}randforest.spr
        ${RELEASE}/alphaMVA/randforest/applyRF.exe ${MVANAME}splitroot/valsig.root ${MVANAME}rfoutput/valsig.root ${MVANAME}randforest.spr
        if [ -n "$RATE_LIMIT" ]; then
          root -l -q '${RELEASE}/alphaMVA/training/FindByBackgroundLimit.C($RATE_LIMIT)' > ${MVANAME}myPRIMoutput.txt #Test value (similary to passed cuts)
        else
          echo "Failed... Set RATE_LIMIT enviroment var... "
          exit 1
        fi
        RFcut=`grep 'RF:' ${MVANAME}myPRIMoutput.txt | sed 's/[^0-9.]//g'`
        maxval=`grep 'Efficiency: ' ${MVANAME}myPRIMoutput.txt | sed 's/[^0-9.]//g'`
      fi
      echo "@" $maxval $RFcut
      # RF optimization output
	  echo $nt $nf $ne $maxval $RFcut>> ${MVANAME}myRFoutput.txt
      # save ALL the outputs
	  mv -v ${MVANAME}randforest.spr ${MVANAME}RFout/randforest_"$nt"_"$nf"_"$ne".spr
	  cp -v ${MVANAME}myPRIMoutput.txt ${MVANAME}PRIMout/myPRIMoutput_"$nt"_"$nf"_"$ne".txt
	  mv -v ${MVANAME}/rfoutput/valbkg.root ${MVANAME}WriterOut/valrf_"$nt"_"$nf"_"$ne".root 
      mv -v ${MVANAME}/rfoutput/valsig.root ${MVANAME}WriterOut/valrf_"$nt"_"$nf"_"$ne".root
	  # cp ../randforest/${MVANAME}valrf.root WriterOut/valrf_"$nt"_"$nf"_"$ne".root
	  # cp ../randforest/${MVANAME}testrf.root WriterOut/testrf_"$nt"_"$nf"_"$ne".root
	done
  done
done
	    
touch ${MVANAME}completed.info
