#!/bin/bash


train_me()
{
	local NT=$1
	local NF=$2
	local NE=$3

	  echo "-->" $NT $NF $NE
# RF with bagging (Bootstrap AGGregatING) [training_data_file]
#-n number of Bagger training cycles
#-l minimal number of entries per tree leaf (def=1)
#-s max number of sampled features (def=0 no sampling)
#-c criterion for decision tree optimization (def=5) 5 = Gini index 
#-C criterion to be monitored on test data (def=1) 9 = Punzi's sensitivity s/(0.5*nSigma+sqrt(b))
#-g per-event loss for (cross-)validation 1 - quadratic loss (y-f(x))^2
#-f store trained Bagger to file
#-t read validation/test data from a file
#-d frequency of print-outs for validation data
	  SprBaggerDecisionTreeApp -n $NT -l $NE -s $NF -c 5 -C 9 -g 1 -f RFout/randforest_"$NT"_"$NF"_"$NE".spr -t val_randforest.txt -d 5 opt_randforest.txt
      if [ -z "$RATE_LIMIT" ]; then
        if [ -n "$MVANAME" ]; then
          cd ${RELEASE}/alphaMVA/${MVANAME}
        fi

# Save Output [list_of_classifier_config_files input_data_file output_tuple_file]
#-A save output data in ascii instead of Root
#-C output classifier names (in quotes, separated by commas)    
# write ASCII
	    SprOutputWriterApp -A -C 'rf' RFout/randforest_"$NT"_"$NF"_"$NE".spr val_randforest.txt WriterOut/valrf_"$NT"_"$NF"_"$NE".out
	    SprOutputWriterApp -A -C 'rf' RFout/randforest_"$NT"_"$NF"_"$NE".spr test_randforest.txt WriterOut/testrf_"$NT"_"$NF"_"$NE".out
# write ROOT
	    SprOutputWriterApp -C 'rf' RFout/randforest_"$NT"_"$NF"_"$NE".spr val_randforest.txt WriterOut/valrf_"$NT"_"$NF"_"$NE".root
	    SprOutputWriterApp -C 'rf' RFout/randforest_"$NT"_"$NF"_"$NE".spr test_randforest.txt WriterOut/testrf_"$NT"_"$NF"_"$NE".root

	    # evaluate RF performance: Bump Hunter
	    cd $RELEASE/alphaMVA/training
	    ./PRIMRF.sh $NT $NF $NE
	    cd "$RELEASE/alphaMVA/$MVANAME"
	    rm  WriterOut/valrf_"$NT"_"$NF"_"$NE".out                                                                                                                                                                
            rm WriterOut/testrf_"$NT"_"$NF"_"$NE".out
		#cd -
	    maxval=$(grep FOM PRIMout/myPRIMoutput_"$NT"_"$NF"_"$NE".txt | awk '{print $5,$0}' | sed 's/FOM=//' | sort -g | tail -1 | awk '{print $6,$7,$8,$9,$10,$11}')
	    line=$(grep -n FOM PRIMout/myPRIMoutput_"$NT"_"$NF"_"$NE".txt | awk '{print $5,$0}' | sed 's/FOM=//' | sort -g | tail -1 | awk '{print $2}' | sed 's/:Bump//')
	    RFcut=$(head -$((line+1)) PRIMout/myPRIMoutput_"$NT"_"$NF"_"$NE".txt | tail -1 | awk '{print $2,$4}')
	    
	    
	    echo $RFcut
	    echo $RFcut
	    echo $RFcut
	    echo $RFcut
	    echo $RFcut
	    echo $RFcut
	    echo $RFcut
	    echo $RFcut
	    echo $RFcut
	    echo $RFcut
      else
        cd $RELEASE/alphaMVA
        ${RELEASE}/alphaMVA/randforest/applyRF.exe ${MVANAME}splitroot/valbkg.root ${MVANAME}WriterOut/valbkg_"$NT"_"$NF"_"$NE".root ${MVANAME}RFout/randforest_"$NT"_"$NF"_"$NE".spr
        ${RELEASE}/alphaMVA/randforest/applyRF.exe ${MVANAME}splitroot/valsig.root ${MVANAME}WriterOut/valsig_"$NT"_"$NF"_"$NE".root ${MVANAME}RFout/randforest_"$NT"_"$NF"_"$NE".spr
        ROOT_STRING=${RELEASE}"/alphaMVA/training/FindByBackgroundLimit.C(\""${MVANAME}"WriterOut/valbkg_"${NT}"_"${NF}"_"${NE}".root\",\""${MVANAME}"WriterOut/valsig_"${NT}"_"${NF}"_"${NE}".root\","${RATE_LIMIT}")"
        #echo $ROOT_STRING
        root -l -b -q "$ROOT_STRING" > ${MVANAME}PRIMout/myPRIMoutput_"$NT"_"$NF"_"$NE".txt #Test value (similary to passed cuts)`
        RFcut=`grep 'RF:' ${MVANAME}PRIMout/myPRIMoutput_"$NT"_"$NF"_"$NE".txt | sed 's/[^0-9.]//g'`
        maxval=`grep 'Signal Efficiency: ' ${MVANAME}PRIMout/myPRIMoutput_"$NT"_"$NF"_"$NE".txt | sed 's/[^0-9.]//g'`
      fi
      echo "@" $maxval $RFcut
     
	
}


if [ -n "$MVANAME" ]; then
 cd "${RELEASE}/alphaMVA/${MVANAME}"
fi
rm -f myRFoutput.txt
rm -f completed.info

mkdir -p RFout
mkdir -p PRIMout
mkdir -p WriterOut
mkdir -p rfoutput


#for nt in 300 100; do
for nt in 500 300 100; do
#for nt in 50 ; do
#  for nf in 4 8; do
  for nf in 8 4 2; do
#	for ne in 8  2; do
	for ne in 16 8 4; do
      if [ -n "$MVANAME" ]; then
        echo "scantrainRF.sh Using folder: $RELEASE/alphaMVA/$MVANAME"
        cd "$RELEASE/alphaMVA/$MVANAME"
      fi
      
      maxjobs=`nproc`
      jobcnt=(`jobs -p`)
      echo "Jobcount ${#jobcnt[@]}"
	  if [ ${#jobcnt[@]} -lt $maxjobs ] ; then
	    echo "Training: $nt $nf $ne"
        train_me $nt $nf $ne  > "train_${nt}_${nf}_${ne}.log" &
      else
	    while [ ${#jobcnt[@]} -ge $maxjobs ]
		do
		  echo "$maxjobs jobs running... sleeping 30s"
	      sleep 30
		  jobcnt=(`jobs -p`)
		done
		echo "Training: $nt $nf $ne"
		train_me $nt $nf $ne > "train_${nt}_${nf}_${ne}.log" &
	  fi
        
      # save ALL the outputs
     #mv -v ${MVANAME}randforest.spr ${MVANAME}RFout/randforest_"$nt"_"$nf"_"$ne".spr
#	  cp -v ${MVANAME}myPRIMoutput.txt ${MVANAME}PRIMout/myPRIMoutput_"$nt"_"$nf"_"$ne".txt
#	  mv -v ${MVANAME}/rfoutput/valbkg.root ${MVANAME}WriterOut/valrf_"$nt"_"$nf"_"$ne".root
#      mv -v ${MVANAME}/rfoutput/valsig.root ${MVANAME}WriterOut/valrf_"$nt"_"$nf"_"$ne".root #JOE I DONT WORK WITH YOUR CHANGES
	  # cp ../randforest/${MVANAME}valrf.root WriterOut/valrf_"$nt"_"$nf"_"$ne".root #JOE I DONT WORK WITH YOUR CHANGES
	  # cp ../randforest/${MVANAME}testrf.root WriterOut/testrf_"$nt"_"$nf"_"$ne".root
	done
  done
done
echo "******************************"
echo "* Waiting for jobs to finish *"
echo "******************************"
wait
echo "Waiting over...
Bulding summary"
cd $RELEASE/alphaMVA/
#for nt in 100 300; do
for nt in 100 300 500; do
#for nt in 50; do
#  for nf in 4 8; do
  for nf in 2 4 8; do
#	for ne in 8 2; do
	for ne in 4 8 16; do
      if [ -z "$RATE_LIMIT" ]; then
        maxval=$(grep FOM ${MVANAME}PRIMout/myPRIMoutput_"$nt"_"$nf"_"$ne".txt | awk '{print $5,$0}' | sed 's/FOM=//' | sort -g | tail -1 | awk '{print $6,$7,$8,$9,$10,$11}')
	    line=$(grep -n FOM ${MVANAME}PRIMout/myPRIMoutput_"$nt"_"$nf"_"$ne".txt | awk '{print $5,$0}' | sed 's/FOM=//' | sort -g | tail -1 | awk '{print $2}' | sed 's/:Bump//')
	    RFcut=$(head -$((line+1)) ${MVANAME}PRIMout/myPRIMoutput_"$nt"_"$nf"_"$ne".txt | tail -1 | awk '{print $2,$4}')
        # RF optimization output
        echo $nt $nf $ne $maxval $RFcut>> ${MVANAME}myRFoutput.txt
      else   
        RFcut=`grep 'RF:' ${MVANAME}PRIMout/myPRIMoutput_"$nt"_"$nf"_"$ne".txt | sed 's/[^0-9.]//g'`
        maxval=`grep 'Signal Efficiency: ' PRIMout/myPRIMoutput_"$nt"_"$nf"_"$ne".txt | sed 's/[^0-9.]//g'`
        # RF optimization output
	    echo $nt $nf $ne $maxval $RFcut>> ${MVANAME}myRFoutput.txt
	  fi
	done
  done
done
cat  ${MVANAME}myRFoutput.txt
	    
touch ${MVANAME}completed.info
