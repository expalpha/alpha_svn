#!/bin/bash

if [ -n "$MVANAME" ]; then
  cd "$RELEASE/alphaMVA/$MVANAME"
fi
# pick the 10 best performant DTs
RFout="myRFoutput.txt"
awk '{print $4,$0}' ${RFout} | sed 's/FOM=//' | sort -g | awk '{ $1=""; print }' | tail -10

RFoutsort="myRFoutput-sorted.txt"
awk '{print $4,$0}' ${RFout} | sed 's/FOM=//' | sort -g | awk '{ $1=""; print }' > ${RFoutsort}

# clean-up
rm -f randforest.spr bump.spr myPRIMoutput.txt *.out *.root

# fetch best performance DTs
# copy to $PWD
pwd
i=1
while [ $i -le 10 ]; do
    trees=$(tail -${i} ${RFoutsort} | head -1 | awk '{print $1}')
    feat=$(tail -${i} ${RFoutsort} | head -1 | awk '{print $2}')
    evnt=$(tail -${i} ${RFoutsort} | head -1 | awk '{print $3}')
    ls -lh RFout/randforest_${trees}_${feat}_${evnt}.spr
    cp RFout/randforest_${trees}_${feat}_${evnt}.spr .
    ls -lh PRIMout/myPRIMoutput_${trees}_${feat}_${evnt}.txt
    cp PRIMout/myPRIMoutput_${trees}_${feat}_${evnt}.txt .
    i=$(( i + 1 ))
done
