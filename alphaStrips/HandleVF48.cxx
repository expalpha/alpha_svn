// HandleVF48.cxx

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdint.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <string>

#include "TFile.h"
#include "TH2.h"
#include "TF1.h"
#include "TLatex.h"
#include "TText.h"
#include "TBox.h"
#include "TApplication.h"
#include "TCanvas.h"
#include "TTree.h"

#include "UnpackVF48.h"
UnpackVF48* vfu = new UnpackVF48();

#include "TROOT.h"
#include "TFolder.h"
#include "TMath.h"
//#include  "Math/MinimizerOptions.h"
//#include "Minuit2/Minuit2Minimizer.h"
//#include "Math/Functor.h"
#include "TStyle.h"
#include "libMidasServer/midasServer.h"

#include <TGFrame.h>
#include <TGNumberEntry.h>
#include <TButton.h>

#include "../alphaAnalysis/lib/include/TSiliconStripRMS.h"
#include "../alphaAnalysis/lib/include/TSiliconStrip.h"
#include "../alphaAnalysis/lib/include/TSiliconVA.h"
#include "../alphaAnalysis/lib/include/TUtilities.h"
extern TFile          *gOutputFile;
extern int gRunNumber;
extern TUtilities* gUtils;
#include "TVF48SiMap.h"
extern TVF48SiMap * gVF48SiMap;
#include "TSettings.h"
extern TSettings      *gSettingsDB;
#include "include/XmlOdb.h"
extern XmlOdb *gOdb;

#include "TalphaStripsReport.h"

extern Int_t           gPass;
extern bool            gADCspecs;
extern bool            gADCTreeDump;
extern bool            gPedSubADC;
extern bool            gPolyPedSubADC;
extern bool            gADCdump;
#define VF48_COINCTIME 0.000010

#define MAX_CHANNELS VF48_MAX_CHANNELS // defined in UnpackVF48.h
#define NUM_SI_MODULES nSil // defined in alphavmc/include/SiMod.h
#define NUM_VF48_MODULES nVF48 // defined in alphavmc/include/SiMod.h
#define NUM_SI_ALPHA1 nSiAlpha1 // defined in alphavmc/include/SiMod.h
#define NUM_SI_ALPHA2 nSiAlpha2 // defined in alphavmc/include/SiMod.h

#define NSIGMATHRES 3. // threshold for removing hits


double gSettingsFrequencies[VF48_MAX_MODULES];
double StripGains[512][NUM_SI_MODULES];

// VF48 sampling parameters from sqlite db

  // Sub-sampling settings
 // int soffset(-1);
 // double subsample(-1.);
//  int offset(-1);
 double gSubSample[NUM_VF48_MODULES];
 int gOffset[NUM_VF48_MODULES];
 int gSOffset[NUM_VF48_MODULES];

extern Bool_t gUseTimeRange;
extern Double_t starttime;
extern Double_t stoptime;

int ShowMem(const char* label);

TSiliconStripRMS StripRMSs[NUM_SI_MODULES*4*128];
TSiliconStripRMS StripRMSsAfterFilter[NUM_SI_MODULES*4*128];
TSiliconStripRMS StripRMSsMeanSub[NUM_SI_MODULES*4*128];
TH2D* adc_hist[NUM_SI_MODULES*4];

TSiliconStripRMS StrippolRMSs[NUM_SI_MODULES*4*128];



extern TTree* polStripTree;// = new TTree("polStripTree","polStripTree");
extern TTree* ADCTree;
Int_t VF48EventNumber;
extern bool gPolyFit;
Float_t pol0[NUM_SI_MODULES*4];
Float_t pol1[NUM_SI_MODULES*4];
Float_t pol2[NUM_SI_MODULES*4];
Float_t chi[NUM_SI_MODULES*4];
    
//    Int_t polstripNumber;
//    Float_t polRMS;

extern TalphaStripsReport* gStripReport;
extern TTree* StripReportTree;
TDirectory* sub_dir_adc;

//ADCdump variables
uint16_t sortedADCs[72][4][128];
extern Float_t gADCFloats[72][4][128];
extern Float_t gPedSubADCFloats[72][4][128];
extern Float_t gPolyPedSubADCFloats[72][4][128];
bool ADCdump_binary=kFALSE; //set to true to dump binary ADC data not ASCII

std::ofstream ADCfile;

using namespace std;

void VF48BeginRun()
{
  int module = 0;
  int samples    = gOdb->odbReadInt("/equipment/VF48/Settings/VF48_NumSamples",module,0);
  int grpEnabled = gOdb->odbReadInt("/equipment/VF48/Settings/VF48_GroupEnable",module,0);

  printf("Module %d, samples: %d, grpEnable: 0x%x\n", module, samples, grpEnabled);
  vfu->SetFlushIncompleteThreshold(40);
  vfu->SetNumModules(NUM_VF48_MODULES);
  vfu->SetGroupEnableMask(-1, grpEnabled);
  vfu->SetNumSamples(-1, samples);
  vfu->SetCoincTime(VF48_COINCTIME);
  vfu->Reset();

  Int_t Module, ASIC, k;
  Module=ASIC=k=0;


  for (int m=0; m<NUM_VF48_MODULES; m++)
    {
      gSettingsFrequencies[m]= gSettingsDB->GetVF48Frequency( gRunNumber, m);
      vfu->SetTsFreq(m,gSettingsFrequencies[m]);
      // extract VF48 sampling parameters from sqlite db
      gSubSample[m] = gSettingsDB->GetVF48subsample( gRunNumber,m );
      gOffset[m] = gSettingsDB->GetVF48offset( gRunNumber, m );
      gSOffset[m] = gSettingsDB->GetVF48soffset( gRunNumber, m );
      if( gSubSample[m] < 1. || gOffset[m] < 0. || gSOffset[m] < 0. )     {
      printf("PROBLEM: Unphysical VF48 sampling parameters:\n");
      printf("subsample = %f \t offset = %d \t soffset = %d \n", gSubSample[m], gOffset[m], gSOffset[m]);
      exit(0);
    }
    }

  // load gains for each strip in each module
  TString gainstr;
  TString stripgn_filename(getenv("RELEASE"));

  if(NUM_SI_MODULES == NUM_SI_ALPHA2)
    stripgn_filename+="/aux/HYBGAINS.ALPHA2.TXT";
  else if(NUM_SI_MODULES == NUM_SI_ALPHA1)
    stripgn_filename+="/aux/HYBGAINS.ALPHA1.TXT";
  else {
    printf("error while loading gain file: num of silicon modules mismatch: %d", NUM_SI_MODULES);
    exit(123);
  }
  std::cout<<"Using GAIN file: "<<stripgn_filename<<std::endl;

  std::ifstream myfile(stripgn_filename.Data());
  std::string aline;

  double gainMin=FLT_MAX;
  double gainMax=0;
  int loop=0;
  if (myfile.is_open()) { //if the file is open
    while(myfile.good()&& loop<512){
      getline(myfile,aline); //get one line from the file
      std::stringstream ss(aline);
      for (int b=0;b<NUM_SI_MODULES;b++) { // silicon module
        ss >> StripGains[loop][b];
        if (StripGains[loop][b]<gainMin)
          gainMin=StripGains[loop][b];
        if (StripGains[loop][b]>gainMax)
          gainMax=StripGains[loop][b];
      }
      loop++;
    }
    myfile.close(); //closing the file
  }
  else {
    std::cout << "Unable to open file " << stripgn_filename.Data() << std::endl; //if the file is not open output
  }
  std::cout << " gain range: " << gainMin << " - " << gainMax << std::endl;
  std::cout << " These gains are read but currently not used"<< std::endl;

  // init TSiliconStripRMS containers for results
  for(Int_t i=0; i<NUM_SI_MODULES*4*128; i++ ) {
    StripRMSsAfterFilter[i].SetStripNumber( k );
    StripRMSsAfterFilter[i].SetASICNumber( ASIC );
    StripRMSsAfterFilter[i].SetModuleNumber( Module );

    //printf("%d \t %d \t %d \n", Module, ASIC, k );
    k++;
    if( k%128==0 ) {
      k=0;
      ASIC++;
      if( ASIC%4==0 ) {
        ASIC=0;
        Module++;
        }
      }
  }

  if ((gPass==0)&&gADCdump) {
	  //char* ADCfilename[100];
	  TString ADCfilename;
	  ADCfilename+=getenv("STRIPFILES");
	  ADCfilename+="/R";
	  ADCfilename+=gRunNumber;
	  ADCfilename+="_ADCdump";
	  if (ADCdump_binary) { ADCfilename+=".bin"; }
	  else {ADCfilename+=".txt";}
	ADCfile.open (ADCfilename,ofstream::binary);
  }

  if ((gPass==1)&&gADCspecs) { //make this 0?
    TDirectory * sub_dir_adc = gUtils->Get_Directory( "",  gOutputFile );
    char name[256];
    char title[256];
    int m, c, ttcchannel;

    for (int isil=0; isil<NUM_SI_MODULES; isil++)  {
      for( int iASIC = 1; iASIC <= 4; iASIC++ ) {
        gVF48SiMap->GetVF48( isil,iASIC, m, c, ttcchannel);
        sprintf( name, "VF48%d_Channel%02d_Si%02d_ASIC%d_ADC_Strip", m, c, isil, iASIC );
        sprintf( title, "VF48# %d Channel# %3d Si# %02d ASIC# %d : ADC vs strip", m, c, isil, iASIC );
        if(iASIC <3) {
          adc_hist[isil*4 + (iASIC-1)] =  gUtils->Get_TH2D( sub_dir_adc, name, title,300,-511.,512.,128,-0.5,127.4999 );
        }
        else {
          adc_hist[isil*4 + (iASIC-1)] =  gUtils->Get_TH2D( sub_dir_adc, name, title,300,-511.,512.,128,-0.5,127.4999 );
        }
      }
    }

  } //gPass==1 or should I be 0...?
  if (gPass==1 && gPolyFit)
  {
    polStripTree->Branch("VF48EventNumber",&VF48EventNumber,"VF48EventNumber/I");
    polStripTree->Branch("pol0",&pol0,"pol0[288]/F");
    polStripTree->Branch("pol1",&pol1,"pol1[288]/F");
    polStripTree->Branch("pol2",&pol2,"pol2[288]/F");
    polStripTree->Branch("chi",&chi,"chi[288]/F");
    
  }
  if (gPass==1 && (gADCTreeDump || gPedSubADC || gPolyPedSubADC ))
    ADCTree->Branch("VF48EventNumber",&VF48EventNumber,"VF48EventNumber/I"); 
  if (gPass==1 && gADCTreeDump)
    ADCTree->Branch("ADC",&gADCFloats,"gADCFloats[72][4][128]/F");
  if (gPass==1 && gPedSubADC)
    ADCTree->Branch("PedSubADC",&gPedSubADCFloats,"gPedSubADCFloats[72][4][128]/F");
  if (gPass==1 && gPolyPedSubADC)
    ADCTree->Branch("PolyPedSubADC",&gPolyPedSubADCFloats,"gPolyPedSubADCFloats[72][4][128]/F");

  
}

int gBadVF48Events = 0;
int gGoodVF48Events = 0;

extern int gVerbose;

void HandleVF48event(const VF48event* e)
{
	
	
  if (gPolyFit) {

    for (Int_t i=0;i<288;i++)
    {
       pol0[i]=-99.;
       pol1[i]=-99.;
       pol2[i]=-99.;
       chi[i]=-99.;
    }
  }

//polStripTree->Branch("polStripNumber",&polStripNumber,"polStripNumber/I");
//polStripTree->Branch("polRMS",&polRMS,"polRMS/F");

  ShowMem("HandleVF48::start");

  if (gVerbose) {
      printf("=======================================================================================\n");
      printf("--> "); e->PrintSummary();
      printf("=======================================================================================\n");
  }
  // search for events within selected range
  if(gUseTimeRange){
    if(e->timestamp<starttime) {
      delete e;
      return;
    }
  }
  if(gUseTimeRange){
    if(e->timestamp>stoptime) {
      delete e;
      return;
    }
  }
  // check for errors
  int trigs = 0;
  for( int imod = 0; imod < NUM_VF48_MODULES; imod++) {

    VF48module* the_Module = e->modules[imod];

    // All modules should be present
    // there is probably a problem with the event
    // <<<< -----
    if(  !the_Module ) {
      printf("Event %d: Error VF48 module %d not present\n", (int)e->eventNo, imod);
      if(gVerbose)
      e->PrintSummary();
      gBadVF48Events++;
      delete e;
      return;
    }

    if( !the_Module ) continue;


    if( the_Module->error != 0 ) {
      printf("Event %d: Found VF48 error, not using event\n", (int)e->eventNo);
      if(gVerbose) e->PrintSummary();
      gBadVF48Events++;
      delete e;
      return;
    }

    if ( the_Module->trigger == 0 ) trigs ++;

    if (trigs >= 1) {
      printf("Event %d: Found VF48 trig error, not using event\n", (int)e->eventNo);
      gBadVF48Events++;
      delete e;
      return;
    }

  }
  gGoodVF48Events++;
  // end checking for errors
  VF48EventNumber=e->eventNo;
        
  // Sub-sampling settings - moved to Begin Run function
  //int soffset(-1);
  //double subsample(-1.);
  //int offset(-1);
  int SiModNumber = -1;
  int ASIC = -1;
  int FRCNumber = -1;
  int FRCPort = -1;
  int TTCChannel = -1;

  for (int i = 0; i < 72; i++)
  {
	  for (int j = 0; j < 4; j++)
	  {
	    for (int k = 0; k < 128; k++)
      {
        sortedADCs[i][j][k]=0;
	    }
	  }
  }

  for( int vf48modnum=0; vf48modnum<NUM_VF48_MODULES; vf48modnum++ ) {

    // Get the VF48 module
    VF48module* the_Module = e->modules[vf48modnum];
    if( !the_Module ) continue;

    // identify number of samples/strip runs
    //VF48channel channel = the_Module->channels[0];

    int vf48group=-1;
    for( int vf48chan=0; vf48chan<48; vf48chan++ ){
      if( vf48chan%16==0 ) vf48group++;

      gVF48SiMap->GetSil( vf48modnum, vf48chan, SiModNumber, ASIC, FRCNumber, FRCPort, TTCChannel );

      if( SiModNumber < 0 ) continue; // why would this check do anything ???

      // Get the VF48 channel
      if( vf48chan >= MAX_CHANNELS ) { printf("Exceeded MAX_CHANNELS\n"); exit(1); }
      VF48channel the_Channel = the_Module->channels[vf48chan];

      TSiliconVA* SiliconVA = new TSiliconVA( ASIC, vf48chan );
      if(vf48chan%4==2 || vf48chan%4==3) SiliconVA->SetPSide( true );

      int s = gSOffset[vf48modnum];


      // Determine the raw ADC for each strip by subsampling the VF48 samples
      for( int k=0; k<128; k++){
        if( s >= the_Channel.numSamples ) continue;
          TSiliconStrip* SiliconStrip = new TSiliconStrip( k, the_Channel.samples[s]  );
          SiliconVA->AddStrip( SiliconStrip );
          if (gADCTreeDump) gADCFloats[SiModNumber][ASIC-1][k]=the_Channel.samples[s];
          if (gADCdump) {
            sortedADCs[SiModNumber][ASIC-1][k]= the_Channel.samples[s];
            //std::cout << "Si:" << SiModNumber << " ASIC:" << ASIC <<" Strip:" << k <<" ADC:" <<sortedADCs[SiModNumber][k][ASIC-1]<< std::endl;
          }
          s += (int)gSubSample[vf48modnum];
        }

      if( SiliconVA->NoStrips() ) {
        delete SiliconVA;
        SiliconVA = NULL;
        continue;
      }

      // Calculate the ASIC strip mean/rms
      SiliconVA->CalcRawADCMeanSigma();

      // Calculate the filtered ASIC strip mean by removing hit-like strips
      SiliconVA->CalcFilteredADCMean();

      // Subtract the mean (filtered) ASIC strip value from each strip (pedestal subtraction)
      SiliconVA->CalcPedSubADCs_NoFit();

      Double_t StripRMSdouble[128]; 
      
      // Calculate for each strip the mean/rms (1st pass) and filtered mean/rms (2nd pass)
      for( int k=0; k<128; k++){
        Int_t stripNumber = 512*(SiModNumber) + 128*(ASIC-1) + k;
        assert( stripNumber >= 0 && stripNumber < 512*NUM_SI_MODULES );

        TSiliconStrip* Strip = (TSiliconStrip*) SiliconVA->GetStripNumber(k);
        if ( !Strip ){
          printf("SiliconVA error: Strip not found\n");
          delete SiliconVA;
          return;
        }

        // Calculate strip mean/rms
        if( gPass==0 )    {
          StripRMSs[ stripNumber ].AddADCValue( Strip->GetPedSubADC(), Strip->GetRawADC() );
          //adc_hist[SiModNumber*4+ ASIC-1]->Fill( Strip->GetPedSubADC(),(double)k );
        }//fi gPass==0

        // Calculate filtered strip mean/rms
        else if( gPass==1 ){
          StripRMSdouble[k]=StripRMSs[stripNumber].GetStripRMS();
          Double_t stripRMS = StripRMSs[stripNumber].GetStripRMS();
          if(gADCspecs)
            adc_hist[SiModNumber*4+ ASIC-1]->Fill( Strip->GetPedSubADC(),(double)k );
          if(gPedSubADC)
            gPedSubADCFloats[SiModNumber][ASIC-1][k]=(Float_t)Strip->GetPedSubADC();
          // remove hits
          if( fabs(Strip->GetPedSubADC()/stripRMS) < NSIGMATHRES )   {
            //StripRMSsAfterFilter[ stripNumber ].AddADCValue( Strip->GetPedSubADC() );  //wrong place? I think we have a alphaStrips bug fix here!
          }

          if( fabs( ( Strip->GetRawADC() - StripRMSs[ stripNumber ].GetRawStripMean() )/stripRMS) < NSIGMATHRES ) {
            StripRMSsMeanSub[ stripNumber ].AddADCValue( Strip->GetRawADC() - StripRMSs[ stripNumber ].GetRawStripMean() );
            StripRMSsAfterFilter[ stripNumber ].AddADCValue( Strip->GetPedSubADC() );
          }
        }//fi gPass==1
      }
      
      
      if (gPass==1 && gPolyFit){
        // std::cout <<"Fitting ";
        //std::cout <<"Module:"<<SiModNumber<<"\t"<<ASIC<<"\t"<<VF48EventNumber<<std::endl;
        SiliconVA->FitP2Pedestal(StripRMSdouble,SiModNumber);
        SiliconVA->CalcPedSubADCs();
        Double_t Pol0=SiliconVA->GetPedFitP0();
        Double_t Pol1=SiliconVA->GetPedFitP1();
        Double_t Pol2=SiliconVA->GetPedFitP2();
        Double_t Chi=SiliconVA->GetPedFitChi();
        for( int k=0; k<128; k++){
          Int_t stripNumber = 512*(SiModNumber) + 128*(ASIC-1) + k;
          assert( stripNumber >= 0 && stripNumber < 512*NUM_SI_MODULES );
          Double_t stripRMS = StripRMSs[stripNumber].GetStripRMS();
          TSiliconStrip* Strip = (TSiliconStrip*) SiliconVA->GetStripNumber(k);
          if ( !Strip ){
            printf("SiliconVA error: Strip not found\n");
            delete SiliconVA;
            return;
          }
          if (gPolyPedSubADC) gPolyPedSubADCFloats[SiModNumber][ASIC-1][k]=( Strip->GetRawADC() - Pol0 + Pol1*k + Pol2*k*k );
          if( fabs( ( Strip->GetRawADC() - Pol0 + 
          Pol1*k +
          Pol2*k*k )/stripRMS) < NSIGMATHRES ) {
         
            StrippolRMSs[ stripNumber ].AddADCValue( Strip->GetRawADC() 
              - Pol0 + Pol1*k + Pol2*k*k );
          }
        }
        pol0[4*(SiModNumber)+(ASIC-1)]=(Float_t)Pol0;
        pol1[4*(SiModNumber)+(ASIC-1)]=(Float_t)Pol1;
        pol2[4*(SiModNumber)+(ASIC-1)]=(Float_t)Pol2;
        chi[4*(SiModNumber)+(ASIC-1)]=(Float_t)Chi;
      }
      delete SiliconVA;
    }//loop over VF48 channels
  } // loop over VF48 modules
  if ((gADCTreeDump || gPedSubADC || gPolyPedSubADC) && gPass==1) ADCTree->Fill();
  if (gPolyFit && gPass==1) polStripTree->Fill();
  if (gADCdump && gPass==0) {
    //ofstream ADCfile;
    //ADCfile.open ("example.bin",ofstream::binary);
    for (int i = 0; i < 72; i++)
    {
      for (int j = 0; j < 4; j++)
      {
        for (int k = 0; k < 128; k++)
        {
          //myfile << "Si:" << i << " ASIC:" << k << " ADC:" <<sortedADCs[i][j][k]<< std::endl;
          if (ADCdump_binary) {
            //This is bugged somehow... fix me, by default now turned off
            ADCfile.write((char*)&sortedADCs[i][j][k], sizeof (sortedADCs[i][j][k]));
          }
          else {
            ADCfile << sortedADCs[i][j][k]; //dump as ASCII (larger output file)
            ADCfile << '\t';
          }
          //cout << sortedADCs[i][j][k]<< endl;
        }
      }
    }
    if (!ADCdump_binary) {
      ADCfile << endl;
    }
    //exit(1); exit line if you just want 1 event...
  }
  delete e;
}


void VF48EndRun()
{

  while (1)
  {
    VF48event* e = vfu->GetEvent(true);
    if (!e) break;
    HandleVF48event(e);
  }

  gOutputFile->cd();

  if( gPass==1) {
    if (gADCdump )  {
      std::cout <<"Closing ADC dump file"<<std::endl;
      ADCfile.close();
    }
    // // write to a tree
    // TTree* SiliconStripRMSTree = new TTree("SiliconStripRMS Tree", "SiliconStripRMSTree");
    // TSiliconStripRMS* SiliconStripRMS = new TSiliconStripRMS();
    // SiliconStripRMSTree->Branch("SiliconStripRMS","TSiliconStripRMS",&SiliconStripRMS,16000,1);

    StripReportTree = new TTree("StripReportTree","TalphaStripsReport");
    StripReportTree->Branch("gStripReport","TalphaStripsReport",&gStripReport,16000,1);
    
    TTree* alphaStripTree = new TTree("alphaStrip Tree","alphaStrip Tree");
    Int_t stripNumber;
    Float_t stripMean;
    Float_t stripRMS;
    Float_t stripMeanSubRMS;
    Float_t stripPolRMS;

    Float_t fitMean;
    Float_t fitRMS;
    Float_t fitmin;
    Float_t fitmax;

    alphaStripTree->Branch("stripNumber",&stripNumber, "stripNumber/I");
    alphaStripTree->Branch("stripMean",&stripMean, "stripMean/F");
    alphaStripTree->Branch("stripRMS",&stripRMS, "stripRMS/F");
    TH2D* stripRMS_pside=new TH2D( "stripRMS_pside", "Strip RMS for pside; arr; barr",1000,-1.,99.,72,-0.5,71.4999 );
    stripRMS_pside->SetDirectory(0); 
    
    TH2D* stripRMS_nside=new TH2D( "stripRMS_nside", "Strip RMS for nside; arr; barr",1000,-1.,99.,72,-0.5,71.4999 );
    stripRMS_nside->SetDirectory(0); 
    
    alphaStripTree->Branch("stripMeanSubRMS",&stripMeanSubRMS, "stripMeanSubRMS/F");
    if (gPolyFit) alphaStripTree->Branch("stripPolRMS",&stripPolRMS, "stripPolRMS/F"); //re-added am I threshold?
   

    //    alphaStripTree->Branch("fitMean",&fitMean,"fitMean/F");
    //    alphaStripTree->Branch("fitRMS",&fitRMS,"fitRMS/F");
    //ROOT::MATH::MinimizerOptions::SetDefaultMinimizer("Minuit2", "Minuit2Minimizer");
    
    
    TF1 *fitf = new TF1("fitf","gaus(0)");
    //TH1D* StripRMSMeanSub=new TH1D("StripRMSMeanSub","StripRMSMeanSub",1000,-1,99);
    for(Int_t i=0; i<NUM_SI_MODULES*512; i++){
      stripNumber = i;
      //if(StripRMSs[i].GetNumberEvents()>0) {   // I am testing exclusing this if statement, Joe
      //stripMean = (Float_t) StripRMSs[i].GetStripMean(); // use this if you want to see the RMS without the rejection of hits
      stripMean = (Float_t) StripRMSsAfterFilter[i].GetStripMean();
      //stripRMS = (Float_t) StripRMSs[i].GetStripRMS(); // use this if you want to see the RMS without the rejection of hits
      stripRMS = (Float_t) StripRMSsAfterFilter[i].GetStripRMS();
      stripMeanSubRMS = (Float_t) StripRMSsMeanSub[i].GetStripRMS();
      
      stripPolRMS = (Float_t) StrippolRMSs[i].GetStripRMS();
      if ( (i/256)%2==0)
        stripRMS_nside->Fill(StripRMSsAfterFilter[i].GetStripRMS(),(Double_t)i/512);
      else
        stripRMS_pside->Fill(StripRMSsAfterFilter[i].GetStripRMS(),(Double_t)i/512);
      if(gADCspecs) {
        TH1D* htemp=adc_hist[(Int_t)(i/128)]->ProjectionX("htemp",i%128+1,i%128+1,"");
        fitf->SetParameters(htemp->GetBinContent((Int_t)stripMean),(Double_t)stripMean,(Double_t)stripRMS);
        //fitf->SetParLimits(2,0.,100.); //This line seems to be the source of failing fits...

        if (0) { //'0' statement removes limiting of ranges
          fitmin=stripMean-30.;
          fitmax=stripMean+30.;
          if ((i/128)%4<2) {
            if (fitmin<-110.){
              fitmin=-110.; fitmax = 0.;
            }
            if (fitmax > 400.){
              fitmax=400.; fitmin=340.;
            }
          }
         else {
           if (fitmin<-400.){
             fitmin=-400.; fitmax = -340.;
           }
           if (fitmax > 110.){
             fitmax=110.; fitmin=0.;
           }
         }
        }
        else {
          fitmin=-512; fitmax=512;
        }//Allowing fitting over entire range of ADC}//do this if above is set to zero}
        Int_t status = htemp->Fit("fitf","B","",fitmin,fitmax);
        if(status==0 || status==3){
          fitMean=fitf->GetParameter(1);
          fitRMS=fitf->GetParameter(2);
          printf("Si: %d  ASIC: %d  Strip %d fitM %f RMS %f \n", i/512, (i/128)%4+1,i%128+1, fitMean, fitRMS);
        }
        else{
          fitMean = stripMean;
          fitRMS=stripRMS;
          printf("Failed fit i=%d \t fitmean=%f \t fitrms=%f \n", i,fitf->GetParameter(1),fitf->GetParameter(2));
#if 0     //Set to 1 to enable saving bad fits to the root tree
            char temp_name[256];
            sprintf( temp_name, "Si: %d  ASIC: %d  Strip %d fitM %f RMS %f",i/512, (i/128)%4+1,i%128+1,fitMean, fitRMS );
            TH1D* htemp1=adc_hist[(Int_t)(i/128)]->ProjectionX(temp_name,i%128+1,i%128+1,"");
            fitf->SetParameters(htemp1->GetBinContent((Int_t)stripMean),(Double_t)stripMean,(Double_t)stripRMS);
            fitf->SetParLimits(2,0.,100.);
            Int_t status = htemp1->Fit("fitf","B","",fitmin,fitmax);
#endif
        }
      } //fi adcspecs

      //printf("Si: %d  ASIC: %d  Strip %d Mean %f RMS %f mean_sub_rms %f \n", i/512, (i/128)%4+1,i%128+1, stripMean, stripRMS, stripMeanSubRMS);

      alphaStripTree->Fill();
      // printf("i=%d \t mean=%f \t rms=%f \t sum0 %d \n", i, StripRMSsAfterFilter[i].GetStripMean(), StripRMSsAfterFilter[i].GetStripRMS(), StripRMSsAfterFilter[i].GetNumberEvents() );
    }//for loop over modules
  delete fitf;

  gStripReport->SetVF48Events(gGoodVF48Events,gBadVF48Events);
 // gStripReport->SetDeadStrips( StripRMSMeanSub->Integral(0,StripRMSMeanSub->GetYaxis()->FindBin(2.))); //Less than one
  //gStripReport->SaveHistogram(StripRMSMeanSub);
  
  gStripReport->SaveHistogram2D(stripRMS_pside);
  gStripReport->SaveHistogram2D(stripRMS_nside);
  //delete stripRMS_nside;
  //delete stripRMS_pside;
  //delete StripRMSMeanSub;
  StripReportTree->Fill();
  
  }//fi gPass==1

}

// end
