// ===========================================================================
//
// alphaStrips
//
// MIDAS analyser to characterise silicon strip pedestals. 
//
// JWS 13/07/2009.
//
// ===========================================================================



#include <stdio.h>
//#include <sys/time.h>
#include <signal.h>
#include <assert.h>
#include <iostream>
#include <time.h>
#include <stdint.h>

//#include "include/TMidasOnline.h"
#include "include/TMidasEvent.h"
#include "include/TMidasFile.h"
#include "include/XmlOdb.h"
//#include "obsolete/midasServer.h"
#include "UnpackVF48.h"
extern UnpackVF48* vfu;

#include <TROOT.h>
#include <TSystem.h>
#include <TClass.h>
#include <TApplication.h>
#include <TTimer.h>
#include <TFile.h>
#include <TDirectory.h>
#include <TFolder.h>
#include <TTree.h>
#include <TBranch.h>
#include <TError.h>

#include "TVF48SiMap.h"
#include "TSettings.h"
#include "TUtilities.h"
#include "alphaStripsSVNLog.h"
#include "TalphaStripsReport.h"
int ShowMem(const char* label);

// Global Variables
int                  gRunNumber = 0;
bool                 gIsRunning = false;
bool                 gVf48disasm = false;
bool                 gADCspecs=kFALSE;
bool                 gADCdump=kFALSE;
bool                 gADCTreeDump=false;
bool                 gPedSubADC=false;
bool                 gPolyPedSubADC=false;
double               gVf48eventTime = 0;
bool                 gIsCosmicRun = false;
bool                 gEOS = false;
bool                 gWait = false;
bool                 gPolyFit = false;
Bool_t gUseTimeRange = kFALSE;
Double_t starttime = 0;
Double_t stoptime = 0;
Bool_t gForceFilename = kFALSE;
char CustomOutput[80];

Float_t gADCFloats[72][4][128];
Float_t gPedSubADCFloats[72][4][128];
Float_t gPolyPedSubADCFloats[72][4][128];

int gVerbose = 0;

TFile 				*gOutputFile = NULL;
VirtualOdb	 		*gOdb = NULL;
VirtualOdb	 		*endOdb = NULL;
TVF48SiMap 			*gVF48SiMap = NULL;
TSettings           		*gSettingsDB = NULL;

Int_t                gPass;
char                 gSiRMSFilename[80];
//Bool_t               gCalcStripMeanRMS = true;
TUtilities* gUtils = NULL;


TTree* polStripTree; 
TTree* ADCTree;
TTree* PedSubADCTree;
TTree* PolyPedSubADCTree;
Int_t EventNumber=0;
TTree* EventNo;

Int_t polstripNumber;
Float_t polRMS;

TalphaStripsReport* gStripReport=new TalphaStripsReport();

TTree *StripReportTree = NULL;


#define NUM_VF48_MODULES nVF48 // defined in alphavmc/include/SiMod.h

// Local Variables
int EventCutoff = 0;

void VF48BeginRun();
void VF48EndRun();
void HandleVF48event(const VF48event *e);

void startRun(int transition,int run,int time) {
  gIsRunning = true;
  gRunNumber = run;
  
  gStripReport->SetRunNumber(gRunNumber);
  gStripReport->SetAlphaStripsExeDateBinary(AlphaStripsCompileTime);
  gStripReport->SetSVNalphaStripsRevision(_AlphaStripsSVNRevision_);
  gStripReport->SetSVNalphaStripsFULL(_AlphaStripsSVNFull_);
  gStripReport->SetHostName(gSystem->HostName());
  gStripReport->SetStartTimeBinary(gOdb->odbReadUint32("/Runinfo/Start time binary"));
  ShowMem("startRun::entry");
  
  // create root file
  char filename[80];

  const char* dir = getenv("STRIPFILES");
  if (dir == NULL)  dir = ".";
  if (gForceFilename)
  {
    if (CustomOutput[0]=='/') 
    {
      //cout << "leading slash found" << endl;
      sprintf(filename,"%s",CustomOutput);      
    }
    else
    {
      //cout << "no leading slash" <<endl;
      sprintf(filename,"%s/%s",dir,CustomOutput);
    }  
  }
  else
  {
    sprintf(filename,"%s/alphaStrips%05doffline.root", dir, gRunNumber);
  }


  gOutputFile = new TFile(filename,"RECREATE");
  gOutputFile->cd();
  if (gPolyFit) polStripTree = new TTree("polStripTree","polStripTree");
  if (gADCTreeDump || gPedSubADC ||gPolyPedSubADC)
  {
    ADCTree = new TTree("ADCTree","ADCTree");
  }
  
  printf("Output file is %s\n", filename);
  gUtils = new TUtilities();
  printf("ALPHA  Version: %d silicon modules \n",nSil);
  // load the sqlite3 db
  char dbName[255]; 
  sprintf(dbName,"%s/aux/main.db",getenv("RELEASE"));
  gSettingsDB = new TSettings(dbName,gRunNumber);

  // load the the VF48SiMap
  char name[256];
  sprintf(name,"%s%s%s",getenv("RELEASE"),gSettingsDB->GetVF48MapDir().Data(),gSettingsDB->GetVF48Map(gRunNumber).Data());
  gVF48SiMap = new TVF48SiMap(name);
  
  VF48BeginRun(); 
  ShowMem("startRun::exit");
}

void endRun(int transition,int run,int time) {
  gIsRunning = false;
  gRunNumber = run;

  if (endOdb!=NULL)
  {
    gStripReport->SetStopTimeBinary(endOdb->odbReadUint32("/Runinfo/Stop time binary"));
  }
  else
  {
    gStripReport->SetStopTimeBinary(0);
  }

  ShowMem("endRun::entry");
  
  VF48EndRun();	  
  if(gOutputFile) gOutputFile->Write();
  if(gOutputFile) delete gOutputFile;
  
  printf("End of run %d\n",run);
  gStripReport->Print();
  
}

void HandleMidasEvent(TMidasEvent& event)
{

  int eventId = event.GetEventId();
  
  if (eventId == 11) {    // VME bulk data
    gVf48eventTime = event.GetTimeStamp();
      
    for (int i=0; i<NUM_VF48_MODULES; i++){
      char bankname[5];
      void *ptr;
      bankname[0] = 'V';
      bankname[1] = 'F';
      bankname[2] = 'A';
      bankname[3] = '0' + i;
      bankname[4] = 0;
      int size = event.LocateBank(NULL,bankname,&ptr);
      if (size > 0) 
        {
          //          UnpackVF48(i, size, ptr, gVf48disasm);
          vfu->UnpackStream(i, ptr, size);
          while (1) 
            {
              VF48event* e = vfu->GetEvent();
              if (!e) break;
              HandleVF48event(e);
            }
        }
    }
  }
}



void eventHandler(const void*pheader,const void*pdata,int size){
  TMidasEvent event;
  memcpy(event.GetEventHeader(),pheader,sizeof(TMidas_EVENT_HEADER));
  event.SetData(size, (char*)pdata);
  event.SetBankList();
  HandleMidasEvent(event);
}


bool WaitForFile(TMidasFile f, const char* fname)
{
  Int_t WaitTime=0;
  std::cout <<"Waiting for file to appear in path..."<<std::endl;
  while (WaitTime<(60*60*1.5)) // wait up an hour and a half
  {
    bool tryOpen = f.Open(fname);
    if (tryOpen) {
      std::cout <<"File found"<<std::endl;
      return true;
    }
    gSystem->Sleep(1000); //If not there, wait 
    WaitTime++;
    if ((WaitTime%20)==0)  std::cout <<"\b-";//<<WaitTime<<std::endl;
    if ((WaitTime%20)==5)  std::cout <<"\b\\";//<<WaitTime<<std::endl;
    if ((WaitTime%20)==15)  std::cout <<"\b|";//<<WaitTime<<std::endl;
    if ((WaitTime%20)==20)  std::cout <<"\b/";//<<WaitTime<<std::endl;
  }
  std::cout <<"wait timed out after "<<WaitTime <<" seconds"<<std::endl;
  return false;
}

int CopyMidasFileFromEOS(TString filename, Int_t AllowedRetry=5)
{

  if(   (strncmp(gSystem->HostName(),"alphacpc",8)==0) || //AND I am NOT an alphacpc* machine (a safety system to stop deletion of files)
      (strncmp(gSystem->HostName(),"alphadaq",8)==0) ) //AND I am NOT an alphadaq* machine (a safety system to stop deletion of files)
  {
    std::cerr <<"This machine is blacklisted from using --EOS flag"<<std::endl;
    gEOS=false;
    return -99;
  }

  TString EOSdir="/eos/experiment/alpha/midasdata/";
  EOSdir+=filename;
  TString LocalPath=getenv("MIDASDIR");
  LocalPath+="/";
  LocalPath+=filename;
  TString EOScheck="eos ls ";
  EOScheck+=EOSdir;
  EOScheck+="*";

  assert(EOSdir.EndsWith(".mid.gz"));
  TMidasFile f;
  Int_t status=-99;
  Bool_t tryOpen = f.Open(LocalPath);
  if (tryOpen)
  {
    std::cout <<filename <<" found locally, not fetching from EOS"<<std::endl;
    return -1;
  }
  if (gSystem->GetFromPipe(EOScheck).Sizeof()!=1 ) //If file exists, 
  {
    std::cout << "Midas file not found, --EOS enabled, hostname matches compatibility list... fetching file from EOS" << std::endl;  //Don't check the first file... I want an error printed if there is no file
    TString EOScopy="eos cp ";
      EOScopy+=EOSdir;
      EOScopy+=" ";
      EOScopy+=getenv("MIDASDIR");
      EOScopy+="/";
      status=gSystem->Exec(EOScopy);
    if (status!=0 )
    {
      if (AllowedRetry<=0) exit(555);
      std::cout <<"Fetching failed with error: "<<status<<std::endl;
      gSystem->Sleep(5000);
      std::cout <<"Trying again ("<<AllowedRetry<<" more attempt(s) until abort"<<std::endl;
      CopyMidasFileFromEOS(filename,AllowedRetry-1);
    }
    else
    {
      //fetchedFromEOS=true;
    }
    tryOpen = f.Open(filename);
    std::cout << "Status:"<<status<<" File fetched successfully from EOS, I will remove the midas file when done with it" << std::endl;
    f.Close();
    return status;
  }
  else 
  {
    std::cout<< std::endl <<filename<< " not found on EOS" << std::endl;
    return -2;
  }
}


int ProcessMidasFile(TApplication*app,const char*fname) {
  TMidasFile f;
  bool tryOpen = f.Open(fname);
  bool fetchedFromEOS[100];
  TString EOSdir="/eos/experiment/alpha/midasdata/";
  //std::cout<<gSystem->HostName()<<std::endl;
  if (gEOS )
  {
    TString midasFileName="";
    for (uint i = strlen(fname)-23; i < strlen(fname); i++) //I assume the midas filename is 23 chars long...
    {
      midasFileName+=fname[i];
    }
    Int_t status=CopyMidasFileFromEOS(midasFileName);
    if (status==0) fetchedFromEOS[0]=true;
    tryOpen = f.Open(fname);
  }
  // parse the filename to extract the current "sub"-index for that run
  int midindex = 0;
  int urindex = 0;
  std::string sfname(fname);
  char substr[200];
  char basestr[200];
  char sufstr[200];
  char gMidasPath[200];
  gMidasPath[(sfname.find("sub"))-5-4]='\0';

  strncpy(gMidasPath,&fname[0],(sfname.find("sub"))-5-4);
  std::cout <<"Midas file path: "<<gMidasPath <<std::endl;
  strncpy(basestr,fname,sfname.find("sub")+3);
  strncpy(substr,&fname[sfname.find("sub")+4],5);
  strcpy(sufstr,&fname[sfname.find("sub")+4+5]);
  midindex=atoi(substr);
  urindex = midindex;
  
  
  if (!tryOpen && gWait) {
    tryOpen=WaitForFile(f,fname);
  }
  if (!tryOpen) {
    printf("Cannot open input file \"%s\", error %d (%s)\n", fname, f.GetLastErrno(), f.GetLastError());
    return -1;
  }

  tryOpen = f.Open(fname);
  int event_number=0;
  
  // 1st loop over VF48 events to calculate strip mean and rms
  gPass=0;
  if(gErrorIgnoreLevel<2000) printf("1st loop over VF48 events to calculate raw strip mean and rms");
  while (1)  {
    TMidasEvent event;
    if (!f.Read(&event)){
      printf("Cannot read event from file, error %d (%s)\n", f.GetLastErrno(), f.GetLastError());
      break;
    }
    
    int eventId = event.GetEventId();
    
    if ((eventId & 0xFFFF) == 0x8000 ) { // begin run
      // Load ODB contents from the ODB XML file      
      if (midindex==urindex){       
        if (gOdb)
          delete gOdb;
        gOdb = new XmlOdb(event.GetData(),event.GetDataSize());
        startRun(0,event.GetSerialNumber(),0);
      }
    }
    else if ((eventId & 0xFFFF) == 0x8001) {           // end run
      endOdb = new XmlOdb(event.GetData(),event.GetDataSize()); //load end of run odb data
      f.Close();          //          std::cout << "closing " << fname << std::endl;
      char currentfname[200];
      sprintf(currentfname,"%s/run%05dsub%05d.mid.gz",getenv("MIDASDIR"),gRunNumber,midindex);
      
      midindex++;
      char nextfname[200];
      //      printf( "basestr %s sufstr %s \n", basestr,sufstr);
      // AO: the original line screws up with C++11 inserting an extra non-printing char
      //      sprintf(nextfname,"%sb%05d.%s",basestr,midindex,sufstr);
      sprintf(nextfname,"%s/run%05dsub%05d.mid.gz",gMidasPath,gRunNumber,midindex);
      tryOpen = f.Open(nextfname);
      // std::cout << "opening " << nextfname << std::endl;
      
      if (gEOS ) 
      {
		  char nextFileName[200];
		  sprintf(nextFileName,"run%05dsub%05d.mid.gz",gRunNumber,midindex);
		  Int_t status=CopyMidasFileFromEOS((TString)nextFileName);
          tryOpen = f.Open(nextfname);
          if (fetchedFromEOS[midindex-1] && gEOS && gPass==1) {
              TString DeleteFile="rm -v ";
              DeleteFile+=currentfname;
              gSystem->Exec(DeleteFile); 
          }
          if (status==0) fetchedFromEOS[midindex]=true;
          else {
          if (!tryOpen)
		    {
              printf("... run%05dsub%05d.mid.gz not found on EOS... ending run\n",gRunNumber,midindex);
              break;
			}
            //std::cout<< std::endl <<nextfname<< " already exists... assuming intentional... not fetching from EOS, I wont delete it either (safty feature)" << std::endl << std::endl;
                  fetchedFromEOS[midindex]=false;
          }
      }
      
      if (!tryOpen && gWait && !endOdb->odbReadUint32("/Runinfo/Stop time binary")) {
        tryOpen=WaitForFile(f,nextfname);
        if (tryOpen) f.Open(nextfname);
      }
      if (!tryOpen){
        printf("Checking for next input file \"%s\", midindex %d, returned %d (%s)\n", nextfname,midindex, f.GetLastErrno(), f.GetLastError());
        //endRun(0,gRunNumber,0);
        break;
      }
      else
      {
        printf("Opened next input file \"%s\", midindex %d\n", nextfname,midindex);
      }
    }
    else    {
      event.SetBankList();
      if(gErrorIgnoreLevel<2000)  printf("Event no: %d\n", event_number);  
      HandleMidasEvent(event);
    }
	
    event_number++;
    if ((EventCutoff!=0)&&(event_number>=EventCutoff)) {
      if (endOdb)
        delete endOdb;
      endOdb = new XmlOdb(event.GetData(),event.GetDataSize()); //load end of run odb data
     
      printf("Reached event %d, exiting loop.\n", event_number);
      break;
    }
  }//while(1)
  while (1) // new VF48event::EndRun();
    {
      VF48event* e = vfu->GetEvent(true);
      if (!e) break;
      HandleVF48event(e);
    }

  f.Close();
  
  // // 2nd loop over VF48 events to calculate filtered strip mean and rms
  gPass=1;
  midindex=0; 
  VF48BeginRun();
  tryOpen = f.Open(fname);
  event_number=0;



  std::cout << std::endl<<" resetting midas index back to 0 " <<std::endl;
   if(gErrorIgnoreLevel<2000) printf("2nd loop over VF48 events to calculate filtered strip mean and rms");
   while (1)    {

     TMidasEvent event;
     if (!f.Read(&event)){
       printf("Cannot read event from file, error %d (%s)\n", f.GetLastErrno(), f.GetLastError());
       break;
     }
    
     int eventId = event.GetEventId();
    
     if ((eventId & 0xFFFF) == 0x8000){
       if (midindex==urindex){       
         if (gOdb) delete gOdb;
         gOdb = new XmlOdb(event.GetData(),event.GetDataSize());
         startRun(0,event.GetSerialNumber(),0);
       }
     }
    else if ((eventId & 0xFFFF) == 0x8001) {           // end run
      //endOdb = new XmlOdb(event.GetData(),event.GetDataSize()); //load end of run odb data
        
      f.Close();          //          std::cout << "closing " << fname << std::endl;
      
      midindex++;
      char nextfname[200];
      //      printf( "basestr %s sufstr %s \n", basestr,sufstr);
      // AO: the original line screws up with C++11 inserting an extra non-printing char
      //      sprintf(nextfname,"%sb%05d.%s",basestr,midindex,sufstr);
      sprintf(nextfname,"%s/run%05dsub%05d.mid.gz",gMidasPath,gRunNumber,midindex);
      tryOpen = f.Open(nextfname);
      // std::cout << "opening " << nextfname << std::endl;
      
      if (gEOS ) 
      {
		  char nextFileName[200];
		  sprintf(nextFileName,"run%05dsub%05d.mid.gz",gRunNumber,midindex);
		  Int_t status=CopyMidasFileFromEOS((TString)nextFileName);
          tryOpen = f.Open(nextfname);
         /* if (fetchedFromEOS[midindex-1] && gEOS && gPass==1) {
              TString DeleteFile="rm -v ";
              DeleteFile+=currentfname;
              gSystem->Exec(DeleteFile); 
          }*/
          if (status==0) fetchedFromEOS[midindex]=true;
          else {
		    if (!tryOpen)
		    {
              printf("... run%05dsub%05d.mid.gz not found on EOS... ending run\n",gRunNumber,midindex);
              break;
			}
            //std::cout<< std::endl <<nextfname<< " already exists... assuming intentional... not fetching from EOS, I wont delete it either (safty feature)" << std::endl << std::endl;
            fetchedFromEOS[midindex]=false;
          }
       }
       
      
      
    }
     else    {
     event.SetBankList();
       if(gErrorIgnoreLevel<2000)  printf("Event no: %d\n", event_number);  
       HandleMidasEvent(event);
     }
	
     event_number++;
     if ((EventCutoff!=0)&&(event_number>=EventCutoff)){
       printf("Reached event %d, exiting loop.\n", event_number);
       break;
     }
   }//while(1)
   while (1) // new VF48event::EndRun();
     {
       VF48event* e = vfu->GetEvent(true);
       if (!e) break;
       HandleVF48event(e);
     }

   f.Close();
  
  
  
  endRun(0,gRunNumber,0);
  f.Close();
  
  char midasfname[200];
      sprintf(midasfname,"%s/run%05dsub*.mid.gz", gMidasPath,gRunNumber);
      if (fetchedFromEOS[0] && gEOS) {
		  TString DeleteFile="rm -v ";
		  DeleteFile+=midasfname;
		  gSystem->Exec(DeleteFile); 
	  }
      
  
  return 0;
}

static bool gEnableShowMem = false;

int ShowMem(const char* label) {
  if (!gEnableShowMem)
    return 0;
  
  FILE* fp = fopen("/proc/self/statm","r");
  if (!fp)
    return 0;
  
  int mem = 0;
  fscanf(fp,"%d",&mem);
  fclose(fp);
  
  if (label)
    printf("memory at %s is %d\n", label, mem);
  
  return mem;
}
void help() 
{

  printf("\nALPHA Strips usage:\n");

  printf("\n./alphaStrips.exe [-eMaxEvents] [-m] [-v] [--vf48disasm] [--adcspecs] [file1 file2 ...]\n");

  printf("\n");

  printf("\t-h: Print this help message\n");

  printf("\n** MIDAS options:\n");

  printf("\n** Debugging options:\n");
  printf("\t-m: Enable memory leak debugging\n");
  printf("\t--vf48disasm: disassemble the VF48 data banks (very verbose)\n");

  printf("\n** Navigation options:\n");
  printf("\t-e<N>: limit the number of VF48 events to be read to N (only works in offline mode)\n");

  printf("\n** Data quality options:\n");
  printf("\t--adcspecs: creates occupancy histograms\n");

  printf("\n** Development options:\n");
  printf("\t--adcdump: dumps raw ADC values to binary file\n");
  printf("\t--adctreedump: dumps raw ADC values into root file\n");
  printf("\t--pedsubadctreedump: dumps pedestal subtracted ADC values into root file\n");
  printf("\t--polypedsubadctreedump: dumps poly pedestal subtracted raw ADC values into root file\n");
  
  
  printf("\t--polyfit: Fits cubic polynomial to pedistal for better stripRMS determination\n");
  
  printf("\n");
  printf("\t--EOS: Enable automatic fetching then deletion of midas files from EOS (not for online analysis)\n");
  printf("\n");
  
  printf("\t--wait: Wait for midas file to appear, I time out if I wait too long");
	
	
  printf("\t--usetimerange<timestart>:<timestop> : Analyze only Silicon Events in time range (in seconds). Es. --usetimerange516.089:517.687\n");

 printf("\t--output alphaStrips12345custom.root: Custom output file\n");

  printf("\n");
  printf("Example: Create pedestal file from first 20000 events : alphaStrips.exe /alpha/data/alpha/current/run00500.mid.gz\n");
  printf("\n");

  exit(1);

}


TApplication* xapp;

// Main function call

int main(int argc, char *argv[]) {
  printf("***************************\n"); 	 
  printf("     Si Strip Analyser     \n"); 	 
  printf("***************************\n\n");
  
  setbuf(stdout,NULL);
  setbuf(stderr,NULL);
  
  signal(SIGILL,  SIG_DFL);
  signal(SIGBUS,  SIG_DFL);
  signal(SIGSEGV, SIG_DFL);
  signal(SIGPIPE, SIG_DFL);
  
  std::vector<std::string> args;
  for (int i=0; i<argc; i++)  {
    args.push_back(argv[i]);
    if (strcmp(argv[i],"-h")==0 || strcmp(argv[i],"--help")==0)
      help();
  }

  TApplication *app = new TApplication("SiStripAnalyser", &argc, argv);
  extern TApplication* xapp;
  xapp = app;
   
  gErrorIgnoreLevel = 2000; // (0)>=Print,(1000)>=Info,(2000)>=Warning,(3000)>=Error
  printf("Output information level set to %d \n", gErrorIgnoreLevel );
  printf("where (0)>=Print,(1000)>=Info,(2000)>=Warning,(3000)>=Error \n\n" );

  for (unsigned int i=1; i<args.size(); i++)  { // loop over the commandline options
    
    const char* arg = args[i].c_str();
    //printf("argv[%d] is %s\n",i,arg);
	
    if (strncmp(arg,"-e",2)==0)      // Event cutoff flag
      EventCutoff = atoi(arg+2);
    else if (strncmp(arg,"-v",2)==0) // Print something for every event
      gVerbose = 1;
    else if (strncmp(arg,"-m",2)==0) // Enable memory debugging
      gEnableShowMem = true;
    else if (strcmp(arg,"--vf48disasm")==0)
      gVf48disasm = true;
    else if (strcmp(arg,"--adcspecs")==0)
      gADCspecs= true;
    else if (strcmp(arg,"--adcdump")==0)
      gADCdump= true;
    else if (strcmp(arg,"--adctreedump")==0)
      gADCTreeDump=true;
    else if (strcmp(arg,"--pedsubadctreedump")==0)
      gPedSubADC=true;
    else if (strcmp(arg,"--polypedsubadctreedump")==0)
    {
      gPolyPedSubADC=true;
      gPolyFit=true;
    }
      
    else if (strncmp(arg,"--EOS",9)==0)
      gEOS=true; // does not return
    else if (strncmp(arg,"--wait",9)==0)
      gWait=true; // does not return
    else if (strncmp(arg,"--polyfit",9)==0)
      {gPolyFit=true; 
       std::cout <<"enabling polyfit..."<<std::endl;
      }
    else if (strncmp(arg,"--usetimerange=",15)==0){
      gUseTimeRange=kTRUE;
      sscanf(arg,
             "--usetimerange=%lg:%lg",
             &starttime,&stoptime);
             std::cout <<arg <<std::endl;
      std::cout<< "selecting time range  " << starttime << " : " << stoptime << std::endl;
    }
   else if (strncmp(arg,"--output=",9)==0)
      {
        gForceFilename=kTRUE;
        //std::cout <<"scanning!!!"<<std::endl;
        sscanf(arg,
             "--output=%s",
             CustomOutput);
        //cout <<CustomOutput<<endl;
      }

  }
  
  for (unsigned int i=1; i<args.size(); i++){
    const char* arg = args[i].c_str();
    if (arg[0] != '-')  {  
      ProcessMidasFile(app,arg);
    }
  }
  delete app; //I cause a seg fault in root6?
  return 0;
  
}
//end

