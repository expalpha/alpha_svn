#!/bin/bash

echo "//TAlphaStripsLog.h: I am a file that should not be committed to the SVN!"> alphaStripsSVNLog.h
echo "//                         I am created by the makefile">> alphaStripsSVNLog.h
echo "#ifndef _TAlphaStripsVersion_">> alphaStripsSVNLog.h
echo "#define _TAlphaStripsVersion_">> alphaStripsSVNLog.h
echo "#include \"TString.h\"">> alphaStripsSVNLog.h

#Record the SVN only number:
echo -n "TString _AlphaStripsSVNRevision_=\"" >> alphaStripsSVNLog.h
svnversion -n ${RELEASE} >> alphaStripsSVNLog.h
echo "\";"  >> alphaStripsSVNLog.h

#Record the full SVN info
echo -n "TString _AlphaStripsSVNFull_=\"">> alphaStripsSVNLog.h
echo -n `svn info ${RELEASE} | sed -E ':a;N;$!ba;s/\r{0,1}\n/\\n/g' ` >> alphaStripsSVNLog.h
echo  "\";">>  alphaStripsSVNLog.h
if [ "$RPLATFORM" == "macosx64" ]; then
  echo 'unsigned int AlphaStripsCompileTime='`stat alphaStripsSVNLog.h | awk -F ' ' '{print $2}'` ';'>>  alphaStripsSVNLog.h
else
  echo 'unsigned int AlphaStripsCompileTime='`stat alphaStripsSVNLog.h -c %Y`';'>> alphaStripsSVNLog.h
fi


echo "#endif" >> alphaStripsSVNLog.h
