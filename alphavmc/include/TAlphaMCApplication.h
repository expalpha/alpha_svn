// $Id: TAlphaMCApplication.h 341 2008-05-26 11:04:57Z ivana $

//------------------------------------------------
// The Virtual Monte Carlo examples
// Copyright (C) 2007, 2008 Ivana Hrivnacova
// All rights reserved.
//
// For the licensing terms see geant4_vmc/LICENSE.
// Contact: vmc@pcroot.cern.ch
//-------------------------------------------------

/// \file TAlphaMCApplication.h
/// \brief Definition of the TAlphaMCApplication class 
///
/// Geant4 ExampleN06 adapted to Virtual Monte Carlo \n
///
/// \author I. Hrivnacova; IPN, Orsay

#ifndef TALPHA06_MC_APPLICATION_H
#define TALPHA06_MC_APPLICATION_H

#include <vector>

#include <TVector3.h>
#include <TLorentzVector.h>
#include <TVirtualMCApplication.h>
#include <TClonesArray.h>
#include <TMCVerbose.h>

#include "TAlphaEvent.h"
#include "TAlphaMCTrack.h"
//#include "TAlphaMCPMT.h"
#include "TAlphaMCStrip.h"
#include "TAlphaMCCluster.h"

class TCanvas;
class TTree;
class TFile;

class TAlphaMCStack;
class TAlphaDetectorConstruction;
class TAlphaPrimaryGenerator;

/// \ingroup E06
/// \brief Implementation of the TVirtualMCApplication
///
/// \date 16/05/2005
/// \author I. Hrivnacova; IPN, Orsay

class TAlphaMCApplication : public TVirtualMCApplication
{
  public:
    TAlphaMCApplication(const char* name,  const char *title);
    TAlphaMCApplication();
    virtual ~TAlphaMCApplication();
  
    // static access method
    static TAlphaMCApplication* Instance(); 

    // methods
    void InitMC(const char *setup);
    void RunMC(Int_t nofEvents);
 
    virtual void ConstructGeometry();
    virtual void ConstructOpGeometry();
    virtual void InitGeometry();
    virtual void GeneratePrimaries();
    virtual void BeginEvent();
    virtual void BeginPrimary();
    virtual void PreTrack();
    virtual void Stepping();
    virtual void PostTrack();
    virtual void FinishPrimary();
    virtual void FinishEvent();
    virtual void Field(const Double_t* /*x*/, Double_t* b) const;
    
    // set methods
    void  SetVerboseLevel(Int_t verboseLevel);
    void  SetEventVerboseLevel(Int_t verboseLevel) { fEventVerboseLevel = verboseLevel; }
    void  SetOutputFile( TString file ) { fOutputFile = file; }

    Int_t GetEventVerboseLevel() { return fEventVerboseLevel; }
    TTree * GetTree() { return fTree; }

    // get methods
    TAlphaPrimaryGenerator*  GetPrimaryGenerator() const;
 
  private:
    // data members
    Int_t                     fNEvent;         ///< Event counter
    Int_t                     fEventVerboseLevel;// Event Verbose level
    TMCVerbose                fVerbose;         ///< VMC verbose helper
    TAlphaMCStack*              fStack;           ///< VMC stack
    TAlphaDetectorConstruction* fDetConstruction; ///< Dector construction
    TAlphaPrimaryGenerator*     fPrimaryGenerator;///< Primary generator
    TAlphaEvent*                fEvent;           ///< Event object
    TVector3*                   fMCVertex;
    Double_t                    fMCtime;
    Int_t                       fMCType;
    TString                     fOutputFile;
    
    TClonesArray               *fTrackArray;
    TAlphaMCTrack              *fTrack;
    TClonesArray               *fStripArray;
    TAlphaMCStrip              *fStrip;
//    TClonesArray               *fPMTArray;
//    TAlphaMCPMT                *fPMT;
    TClonesArray               *fPairArray;
    TVector3                   *fPair;
  //    TClonesArray               *fClusterArray;
    TAlphaMCCluster            *fCluster;
    Int_t                       fNPair;
//    Int_t                       fNPMTs;
    Int_t                       fNTracks;
    Int_t                       fNHits;
    Int_t                       fNStrips;
  //    Int_t                       fNClusters;
    Int_t                       fNRawHits;
    Int_t                       fFs;
    Double_t                    fEventWeight;

    Int_t                       fNpp;
    Int_t                       fNpm;
    Int_t                       fNp0;

    std::vector<Int_t>          fModList;

    TFile *                     fFile;            // File
    TTree *                     fTree;            // Event Tree

    ClassDef(TAlphaMCApplication,2);  //Interface to MonteCarlo application
};

// inline functions

/// \return The singleton instance 
inline TAlphaMCApplication* TAlphaMCApplication::Instance()
{ return (TAlphaMCApplication*)(TVirtualMCApplication::Instance()); }

/// Set verbosity 
/// \param verboseLevel  The new verbose level value
inline void  TAlphaMCApplication::SetVerboseLevel(Int_t verboseLevel)
{ fVerbose.SetLevel(verboseLevel); }

/// \return The primary generator
inline TAlphaPrimaryGenerator*  TAlphaMCApplication::GetPrimaryGenerator() const
{ return fPrimaryGenerator; }


#endif //TALPHA06_MC_APPLICATION_H

