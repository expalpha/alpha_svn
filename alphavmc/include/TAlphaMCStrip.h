#ifndef __TAlphaMCStrip__
#define __TAlphaMCStrip__

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TAlphaMCStrip                                                        //
//                                                                      //
// Monte Carlo Strip                                                    //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include <TObject.h>

class TAlphaMCStrip : public TObject {
private:
  Int_t fModule;
  Int_t fASIC;
  Int_t fStrip;
  Double_t fADC;
  Double_t fFrac;

public:
  TAlphaMCStrip();
  TAlphaMCStrip( Int_t Module, Int_t ASIC, Int_t Strip, Double_t ADC, Double_t fFrac );
  TAlphaMCStrip( TAlphaMCStrip* & );
  virtual ~TAlphaMCStrip();

  Int_t GetModule() { return fModule; }
  Int_t GetASIC() { return fASIC; }
  Int_t GetStrip() { return fStrip; }
  Double_t GetADC() { return fADC; }
  Double_t GetFrac() { return fFrac; }
  
  ClassDef(TAlphaMCStrip,1);
};

#endif
