// $Id: TAlphaPrimaryGenerator.h 341 2008-05-26 11:04:57Z ivana $

//------------------------------------------------
// The Virtual Monte Carlo examples
// Copyright (C) 2007, 2008 Ivana Hrivnacova
// All rights reserved.
//
// For the licensing terms see geant4_vmc/LICENSE.
// Contact: vmc@pcroot.cern.ch
//-------------------------------------------------

/// \file TAlphaPrimaryGenerator.h
/// \brief Definition of the TAlphaPrimaryGenerator class 
///
/// Geant4 ExampleN06 adapted to Virtual Monte Carlo \n
/// Id: ExN06PrimaryGeneratorAction.hh,v 1.4 2003/01/23 15:34:23 maire Exp \n
/// GEANT4 tag Name: geant4-07-00-cand-01 
///
/// \author I. Hrivnacova; IPN, Orsay

#ifndef TALPHA06_PRIMARY_GENERATOR_H
#define TALPHA06_PRIMARY_GENERATOR_H

#include <TVirtualMCApplication.h>

class TVirtualMCStack;
class TVector3;

/// \ingroup E06
/// \brief The primary generator
///
/// \date 16/05/2005
/// \author I. Hrivnacova; IPN, Orsay

class TAlphaPrimaryGenerator : public TObject
{
  public:
    TAlphaPrimaryGenerator(TVirtualMCStack* stack); 
    TAlphaPrimaryGenerator();
    virtual ~TAlphaPrimaryGenerator();

    // methods
    void GeneratePrimaries();
    std::pair <Int_t,Double_t> GeneratePrimary();

    // set methods
    void SetParticle(Int_t pdg);
    void SetKinEnergy(Double_t kinEnergy);
    void SetDirection(Double_t dirX, Double_t dirY, Double_t dirZ);
    void SetOptPhotonPolar(Double_t angle);
    void SetNofPrimaries(Int_t nofPrimaries);
 
  private:
    // methods

    // data members
    TVirtualMCStack*  fStack;        ///< VMC stack
    Int_t             fPdg;          ///< Particle PDG encoding 
    Double_t          fKinEnergy;    ///< Particle kinetic energy
    Double_t          fDirX;         ///< Particle direction - x component
    Double_t          fDirY;         ///< Particle direction - y component
    Double_t          fDirZ;         ///< Particle direction - z component
    Double_t          fPolAngle;     ///< Particle polarization angle
    Int_t             fNofPrimaries; ///< Number of primary particles

    ClassDef(TAlphaPrimaryGenerator,1);  //TAlphaPrimaryGenerator
};

// inline functions

/// Set particle type
/// \param pdg  The new particle PDG encoding
inline void TAlphaPrimaryGenerator::SetParticle(Int_t pdg)
{ fPdg = pdg; }

/// Set kinetic energy
/// \param kinEnergy  The new particle kinetic energy (in GeV)
inline void TAlphaPrimaryGenerator::SetKinEnergy(Double_t kinEnergy)
{ fKinEnergy = kinEnergy; }

/// Set photon polarization
/// \param angle  The new polarization angle (in degrees)
inline void TAlphaPrimaryGenerator::SetOptPhotonPolar(Double_t angle) 
{ fPolAngle = angle; }  

/// Set the number of particles to be generated
/// \param nofPrimaries The number of particles to be generated
inline void TAlphaPrimaryGenerator::SetNofPrimaries(Int_t nofPrimaries)
{ fNofPrimaries = nofPrimaries; }

#endif //TALPHA06_PRIMARY_GENERATOR_H

