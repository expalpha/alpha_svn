#!/bin/tcsh

cd /home/alpha/capra/alphaSoftware2011/
source myconfig.csh
cd $RELEASE/alphavmc/macros/

if ( -e ./data/ResSummary.dat ) then
    rm ./data/ResSummary.dat
endif

@ i = 1
while ($i <= 3)
	echo " "
	echo "*** Run # $i"
	root -l runG3.C\($i\) 
	echo "*** Rec -- Run # $i"
	root -l RecVertex.C\($i\) > /dev/null
	echo "*** Sum -- Run # $i"
#	root -l VertexResolution.C\($i,0\)
	echo " "
@ i += 1
end

if ( -e ./data/ResSummary.txt ) then
    rm ./data/ResSummary.txt
endif
root -l ResSummary.C >>! ./data/ResSummary.txt
cat ./data/ResSummary.dat
tail -9 data/ResSummary.txt
