#!/bin/bash

cd $RELEASE/alphavmc/macros/
pwd

run=1 #First Run
f=$run
NumOfEvts=20000
ApplyCuts=1
LastRun=5

#Make sure macros are compiled:
echo "Checking macros are compiled"
echo ".L runG3.C+
.q
" | root -l &> MacroMakeLog.txt
echo ".L RecMC.C+
.q
" | root -l &>> MacroMakeLog.txt
cat MacroMakeLog.txt


echo "Running parrellel jobs:"

while [ $run -le $LastRun ]
do
	echo " "
	echo "*** Run # $run"
	#root -l -q sim.C\("${run}","${NumOfEvts}","${ApplyCuts}"\) 1> /dev/null&
	root -l -q sim.C\("${run}","${NumOfEvts}","${ApplyCuts}"\) 1> Sim_${run}.log &
	PIDlist[run]=$!
	if [ $run -lt 10 ] ; then
	    histo[run]=data/MCA2histo0${run}.root
	else
	    histo[run]=data/MCA2histo${run}.root
	fi
	echo " "
((run++))
done
for pid in "${PIDlist[@]}"; do
    wait $pid
done

hadd data/MCA2HistoSum_${f}.root "${histo[@]}"
root -l VertexResolution.C\("-${f}"\)
