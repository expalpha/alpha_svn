// Adapted from:
//------------------------------------------------
// The Virtual Monte Carlo examples
// Copyright (C) 2007, 2008 Ivana Hrivnacova
// All rights reserved.
//
// For the licensing terms see geant4_vmc/LICENSE.
// Contact: vmc@pcroot.cern.ch
//-------------------------------------------------
// Geant4 ExampleN03 adapted to Virtual Monte Carlo \n
// Id: ExN06DetectorConstruction.cc,v 1.14 2004/03/17 22:41:12 gum Exp 
// GEANT4 tag Name: geant4-07-00-cand-01 
//
// \date 16/05/2005
// \author I. Hrivnacova; IPN, Orsay
 
#include <Riostream.h>
#include <TVirtualMC.h>
#include <TGeoManager.h>
#include <TSystem.h>

#include "TAlphaDetectorConstruction.h"
#include "TAlphaGeoMaterialXML.h"
#include "TAlphaGeoDetectorXML.h"
#include "TAlphaGeoEnvironmentXML.h"
#include "TAlphaGeoPMTXML.h"

ClassImp(TAlphaDetectorConstruction)

//_____________________________________________________________________________
TAlphaDetectorConstruction::TAlphaDetectorConstruction()
  : TObject()
{
// Default constuctor
}

//_____________________________________________________________________________
TAlphaDetectorConstruction::~TAlphaDetectorConstruction()
{
// Destructor
}

//
// public methods
//

//_____________________________________________________________________________
void TAlphaDetectorConstruction::ConstructMaterials()
{
// Construct materials using TGeo modeller

  // Create Root geometry manager 

  new TGeoManager("TGeo", "Root geometry manager");

  TAlphaGeoMaterialXML * materialXML = new TAlphaGeoMaterialXML();
  
  Char_t pathname[80];
  if(nSil == 72) {
    sprintf(pathname,"%s/geo/material2.xml",gSystem->pwd());
    printf("ALPHA2 materials and magnetic field using %s", pathname);
  }
  else if(nSil == 60) {
    sprintf(pathname,"%s/geo/material.xml",gSystem->pwd());
    printf("ALPHA1 materials and magnetic field using %s", pathname);
  }
  else
    printf ("Wrong number of Silicon modules: %d \n", nSil);

  materialXML->ParseFile(pathname);
  delete materialXML;
}    

//_____________________________________________________________________________
void TAlphaDetectorConstruction::ConstructGeometry()
{
// Contruct volumes using TGeo modeller

// The experimental Hall
//
  TAlphaGeoEnvironmentXML * environmentXML = new TAlphaGeoEnvironmentXML();

  Char_t pathname[80];
  if(nSil == 72) {
    sprintf(pathname,"%s/geo/environment2_geo.xml",gSystem->pwd());
    printf("ALPHA2 detector constructed using %s", pathname);
  }
  else if (nSil == 60){
    sprintf(pathname,"%s/geo/environment_geo.xml",gSystem->pwd());
    printf("ALPHA1 detector constructed using %s", pathname);
  }
  else 
    printf ("Wrong number of Silicon modules: %d \n", nSil);
  environmentXML->ParseFile(pathname);
  delete environmentXML;

  TAlphaGeoDetectorXML * detectorXML = new TAlphaGeoDetectorXML();
  if(nSil==72){
    sprintf(pathname,"%s/geo/detector2_geo.xml",gSystem->pwd());
    printf(" and %s \n", pathname);
  }
  else if(nSil == 60){
    sprintf(pathname,"%s/geo/detector_geo.xml",gSystem->pwd());
    printf(" and %s \n", pathname);
  }
  detectorXML->ParseFile(pathname);
  delete detectorXML;

//   TAlphaGeoPMTXML * PMTXML = new TAlphaGeoPMTXML();
//   sprintf(pathname,"%s/geo/pmt_geo.xml",gSystem->pwd());
//   PMTXML->ParseFile(pathname);
//   delete PMTXML;


  // close geometry
  gGeoManager->CloseGeometry();

  // notify VMC about Root geometry
  gMC->SetRootGeometry();
}
    
//_____________________________________________________________________________
void TAlphaDetectorConstruction::ConstructOpGeometry()
{
// Define material optical properties
}

