// $Id: Ex06MCApplication.cxx 341 2008-05-26 11:04:57Z ivana $

//------------------------------------------------
// The Virtual Monte Carlo examples
// Copyright (C) 2007, 2008 Ivana Hrivnacova
// All rights reserved.
//
// For the licensing terms see geant4_vmc/LICENSE.
// Contact: vmc@pcroot.cern.ch
//-------------------------------------------------

/// \file Ex06MCApplication.cxx
/// \brief Implementation of the Ex06MCApplication class 
///
/// Geant4 ExampleN06 adapted to Virtual Monte Carlo
///
/// \date 16/05/2005
/// \author I. Hrivnacova; IPN, Orsay

#include <stdlib.h>
#include <iostream>

#include <TROOT.h>
#include <TInterpreter.h>
#include <TVirtualMC.h>
#include <TGeoManager.h>
#include <TVirtualGeoTrack.h>
#include <TParticle.h>
#include <TMCProcess.h>
#include <TFile.h>
#include <TTree.h>
#include <TClonesArray.h>

#include "TAlphaMCApplication.h"
#include "TAlphaMCStack.h"
#include "TAlphaDetectorConstruction.h"
#include "TAlphaPrimaryGenerator.h"

#include "TAlphaEvent.h"
#include "TAlphaEventSil.h"
#include "TAlphaMCTrack.h"

#include "TAlphaMCDigi.h"
using std::cout;
using std::endl;

TTree* gFRDtree;
Float_t gFRDx,gFRDy,gFRDz,gFRDt;
int gEntry;

/// \cond CLASSIMP
ClassImp(TAlphaMCApplication)
/// \endcond

extern "C" int operainit_(char*filename);
extern "C" int gufl(const float vector[3], float b[3]);

//_____________________________________________________________________________
TAlphaMCApplication::TAlphaMCApplication(const char *name, const char *title) 
  : TVirtualMCApplication(name,title),
    fNEvent(0),
    fVerbose(0),
    fStack(0),
    fDetConstruction(0),
    fPrimaryGenerator(0),
    fEvent(0),
    fMCtime(0.0),
    fMCType(0),
    fOutputFile(TString("out1.root")),
    fTrackArray(0),
    fTrack(0),
    fStripArray(0),
    fStrip(0),
//    fPMTArray(0),
//    fPMT(0),
//    fNPMTs(0),
    fNTracks(0),
    fNHits(0),
    fNStrips(0),
    fNRawHits(0),
    fFs(0),
    fEventWeight(0),
    fFile(0),
    fTree(0)
{
/// Standard constructor
/// \param name   The MC application name 
/// \param title  The MC application description

  //gRandom->SetSeed(0);

  // Create a user stack
  fStack = new TAlphaMCStack(1000);
  
  // Create detector construction
  fDetConstruction = new TAlphaDetectorConstruction();
  // Create a primary generator
  fPrimaryGenerator = new TAlphaPrimaryGenerator(fStack);
  TString fname = TString::Format("%s/aux/frdtree.root",getenv("RELEASE"));
  TFile* ffrd = TFile::Open(fname.Data(),"READ");
  gFRDtree = (TTree*) ffrd->Get("frdsim");
  gFRDtree -> SetBranchAddress("x",&gFRDx);
  gFRDtree -> SetBranchAddress("y",&gFRDy);
  gFRDtree -> SetBranchAddress("z",&gFRDz);
  gFRDtree -> SetBranchAddress("time",&gFRDt);
  gEntry=0;
}

//_____________________________________________________________________________
TAlphaMCApplication::TAlphaMCApplication()
  : TVirtualMCApplication(),
    fNEvent(0),
    fVerbose(0),
    fStack(0),
    fDetConstruction(0),
    fPrimaryGenerator(0)
{    
/// Default constructor
}

//_____________________________________________________________________________
TAlphaMCApplication::~TAlphaMCApplication() 
{
/// Destructor  
  delete fTree;
  delete fFile;
  delete fEvent;
  delete fStack;
  delete fDetConstruction;
  delete fPrimaryGenerator;
  // delete fTrackArray;
  // delete fPMTArray;
  // delete fMCVertex;
  delete gMC;
  //  gMC = 0;
}

//
// public methods
//

//_____________________________________________________________________________
void TAlphaMCApplication::InitMC(const char* setup)
{    
/// Initialize MC.
/// The selection of the concrete MC is done in the macro.
/// \param setup The name of the configuration macro 

  fVerbose.InitMC();

  gROOT->LoadMacro(setup);
  gInterpreter->ProcessLine("Config()");
 
  gMC->SetStack(fStack);
  gMC->Init();
  gMC->BuildPhysics();

  // Initialize event
  fEvent   = new TAlphaEvent();

  // Track Array
  fTrackArray = new TClonesArray("TAlphaMCTrack");
  fTrack = new TAlphaMCTrack();

  // Strip Array
  fStripArray = new TClonesArray("TAlphaMCStrip");
  fStrip = new TAlphaMCStrip();

  // Pair Array
  fPairArray = new TClonesArray("TVector3");
  fPair = new TVector3();

  // PMT array
//  fPMTArray = new TClonesArray("TAlphaMCPMT");
//  fPMT = new TAlphaMCPMT();

  // Cluster array
  //  fClusterArray = new TClonesArray("TAlphaMCCluster");
  //  fCluster = new TAlphaMCCluster();

  // Vertex
  fMCVertex = new TVector3();
      
  // Initialize tree
  fFile = new TFile(fOutputFile.Data(),"recreate");
  fTree = new TTree("TAlphaMCEvents","Monte Carlo Events",1);
    
  fTree->Branch("fMCType",&fMCType,"fMCType/I");
  fTree->Branch("fNEvent",&fNEvent,"fNEvent/I");
  fTree->Branch("fTrackArray",&fTrackArray);
  fTree->Branch("fStripArray",&fStripArray);
//  fTree->Branch("fPMTArray",&fPMTArray);
  fTree->Branch("fPairArray",&fPairArray);
  //  fTree->Branch("fClusterArray",&fClusterArray);
  fTree->Branch("fMCVertex",&fMCVertex);
  fTree->Branch("fMCtime",&fMCtime,"fMCtime/D");

  fTree->Branch("fNTracks",&fNTracks,"fNTracks/I");
  fTree->Branch("fNRawHits",&fNRawHits,"fNRawHits/I");
//  fTree->Branch("fNPMTs",&fNPMTs,"fNPMTs/I");
  fTree->Branch("fNPair",&fNPair,"fNPair/I");
  //  fTree->Branch("fNClusters",&fNClusters,"fNClusters/I");
  fTree->Branch("fFs",&fFs);
  fTree->Branch("fEventWeight",&fEventWeight);
  fTree->Branch("fNpp",&fNpp);
  fTree->Branch("fNpm",&fNpm);
  fTree->Branch("fNp0",&fNp0);
  
}

//_____________________________________________________________________________
void TAlphaMCApplication::RunMC(Int_t nofEvents)
{    
/// Run MC.
/// \param nofEvents Number of events to be processed

  fVerbose.RunMC(nofEvents);

  gMC->ProcessRun(nofEvents);

  if( fFile )
    {
      fFile->Write();
      if(nSil==72) gGeoManager->Export("geo2.root");
      else if(nSil==60) gGeoManager->Export("geo.root");
      else cout<<"Geometry file saved with default name"<<endl;
    }
  fVerbose.FinishRun();
}

//_____________________________________________________________________________
void TAlphaMCApplication::ConstructGeometry()
{    
/// Construct geometry using detector contruction class.
/// The detector contruction class is using TGeo functions or
/// TVirtualMC functions (if oldGeometry is selected)

  fVerbose.ConstructGeometry();

  // Cannot use Root geometry if not supported with 
  // selected MC
  if ( ! gMC->IsRootGeometrySupported() ) {
    std::cerr << "Selected MC does not support TGeo geometry"<< endl;
    std::cerr << "Exiting program"<< endl;
    exit(1);
  } 

  cout << "Geometry will be defined via TGeo" << endl;
  fDetConstruction->ConstructMaterials(); 
  fDetConstruction->ConstructGeometry();     
}

//_____________________________________________________________________________
void TAlphaMCApplication::ConstructOpGeometry()
{    
/// Define material optical properties
  fVerbose.ConstructGeometry();

  fDetConstruction->ConstructOpGeometry();  
}

//_____________________________________________________________________________
void TAlphaMCApplication::InitGeometry()
{    
/// Initialize geometry
  fVerbose.InitGeometry();
}

//_____________________________________________________________________________
void TAlphaMCApplication::GeneratePrimaries()
{    
/// Fill the user stack (derived from TVirtualMCStack) with primary particles.
  
  fVerbose.GeneratePrimaries();
  if(gEntry > 9999 ) gEntry-=10000;
  std::pair <Int_t,Double_t> es =  fPrimaryGenerator->GeneratePrimary();
  fMCtime=gEvent->GetMCtime();
  ++gEntry;
  fFs = es.first;
  fEventWeight = es.second;
  fMCVertex = gEvent->GetMCVertex();
  fMCType = gEvent->IsACosmic();

  for(Int_t i=0; i<fStack->GetNtrack(); i++)
    {
      Int_t pdg = (Int_t) fStack->GetParticle(i)->GetPdgCode();

      switch( pdg )
	{
	case 111:
	  fNp0++;
	  break;
	case 211:
	  fNpp++;
	  break;
	case -211:
	  fNpm++;
	  break;
	default:
	  break;
	}
    }
}

//_____________________________________________________________________________
void TAlphaMCApplication::BeginEvent()
{    
/// User actions at beginning of event

  fVerbose.BeginEvent();
  fNTracks = 0;
//  fNPMTs = 0;
  fNHits = 0;
  fNRawHits = 0;
  fNStrips = 0;
  fTrackArray->Clear();
  fStripArray->Clear();
  //  fClusterArray->Clear();
  fPairArray->Clear();
  fTrack->ClearTrack();
//  fPMTArray->Clear();
  fEvent->SetVerboseLevel( fEventVerboseLevel );
  fEvent->GetVerbose()->PrintMem("TAlphaMCApplication::BeginEvent");

  fFs=-1;
  fEventWeight=0;
  fNpp=0;
  fNpm=0;
  fNp0=0;
  fNPair=0;
  // fNClusters=0;
  
  fEvent->DeleteEvent();
}

//_____________________________________________________________________________
void TAlphaMCApplication::BeginPrimary()
{    
/// User actions at beginning of a primary track

  fVerbose.BeginPrimary();
}

//_____________________________________________________________________________
void TAlphaMCApplication::PreTrack()
{    
/// User actions at beginning of each track

  fVerbose.PreTrack();

  TParticle * current_track = fStack->GetCurrentTrack();
 
  fTrack->SetParticleID( current_track->GetPdgCode() );
  fTrack->SetStatus( current_track->GetUniqueID() );
 
  if( current_track->GetFirstMother() >=0 )
    {
      TParticle * parent = fStack->GetParticle( current_track->GetFirstMother() );
      fTrack->SetParent( parent->GetPdgCode() );
    }
  else
    {
      fTrack->SetParent( 0 );
    }

  if( current_track->GetSecondMother() >=0 )
    {
      TParticle * parent = fStack->GetParticle( current_track->GetSecondMother() );
      fTrack->SetSecondParent( parent->GetPdgCode() );
    }
  else
    {
      fTrack->SetSecondParent( 0 );
    }


  Double_t px, py, pz, etot;
  gMC->TrackMomentum( px, py, pz, etot );
  fTrack->SetPx( px );
  fTrack->SetPy( py );
  fTrack->SetPz( pz );
  fTrack->SetEtot( etot );
  fTrack->SetNSil(0);
  fTrack->SetNlayer1(0);
  fTrack->SetNlayer2(0);
  fTrack->SetNlayer3(0);
  fTrack->SetIsTrack(kFALSE);

  fModList.clear();
}

//_____________________________________________________________________________
void TAlphaMCApplication::Stepping()
{    
/// User actions at each step

// <<<<< ----- Segue il passo della traccia attraverso il materiale
// <<<<< ----- determina se ha depositato energia nel silicio e quanta
// <<<<< ----- determina la posizione di ingresso e uscita dal silicio
  
  fVerbose.Stepping();
  Int_t start = gEvent->GetVerbose()->PrintMem(NULL);


  TGeoVolume * cvol = gGeoManager->GetCurrentVolume();
  TGeoMaterial * cmat = cvol->GetMaterial();

  Double_t cpoint[3];
  static Double_t epoint[3]; // why static?
  static Double_t Edep = 0.;
  gMC->TrackPosition( cpoint[0], cpoint[1], cpoint[2] );
  
//----------------------------------------------------------------------------------------
// Store Track Position at each step
//   if(0) std::cout<<fTrack->StorePosition(cpoint[0], cpoint[1], cpoint[2])<<std::endl;
//   if(0) fTrack->StorePosition(cpoint[0], cpoint[1], cpoint[2]);
   //printf("Store Track Position at each step\n");
//----------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------
// Record the number and the position of lepton pair production  
  TArrayI proc;
  gMC->StepProcesses(proc);
  for(Int_t i=0; i<proc.GetSize(); i++)
    {
      if( proc[i] == kPPair )
	{
	  TClonesArray &pair = *fPairArray;
	  pair[fNPair] = new(pair[fNPair]) TVector3( cpoint[0], cpoint[1], cpoint[2] );

	  fNPair++;
	}
    }
//----------------------------------------------------------------------------------------
  
  // if a particle is crossing a silicon module
  // digitize the hit and store info for out tree
  if( strcmp( cmat->GetName(), "Silicon" ) == 0 )
    {
      if( gMC->IsTrackEntering() )
        {
          gMC->TrackPosition( epoint[0], epoint[1], epoint[2] );
          Edep = 0.;
        }
        Edep += gMC->Edep();

	// Digitization Step
        TAlphaMCDigi *digi = new TAlphaMCDigi(cvol,fEvent,Edep);
	digi->SharedHit(cpoint,epoint);
	
	// Fill Output Arrays
	//digi->CreateClusterOutput(fClusterArray,&fNClusters);
	Int_t save = digi->CreateStripOutput(fStripArray,&fNStrips,&fNRawHits);
	digi->FillTrackOutput(fTrack,cpoint,save,&fModList);
	
	//delete digi;
    }

  Int_t end = gEvent->GetVerbose()->PrintMem(NULL);
  if( (end-start)>0 ) printf("Stepping, diff %d %d %d\n",end-start,start,end);

//  printf("TAlphaMCApplication::Stepping(): NSil = %d\n",fTrack->GetNSil());

}

//_____________________________________________________________________________
void TAlphaMCApplication::PostTrack()
{    
/// User actions after finishing of each track
  fVerbose.PostTrack();

  TClonesArray &track = *fTrackArray;
  track[fNTracks] = new(track[fNTracks]) TAlphaMCTrack( fTrack );
  
  fNTracks++;
}

//_____________________________________________________________________________
void TAlphaMCApplication::FinishPrimary()
{    
/// User actions after finishing of a primary track

  fVerbose.FinishPrimary();
}

//_____________________________________________________________________________
void TAlphaMCApplication::FinishEvent()
{    
/// User actions after finishing of an event

  fVerbose.FinishEvent();
      
  fTree->Fill();
  fStack->Reset();

  fNEvent++;
} 

//_____________________________________________________________________________
void TAlphaMCApplication::Field(const Double_t* x, Double_t* b) const
{
  // User generated magnetic field
  // note, field is given in kGauss (for whatever reason, GEANT...)

 // x is in cm
 // start with some 'default' values in kG
  if( x[0]*x[0] + x[1]*x[1] < 20.*20. ) b[2] = 10.1;
  else b[2] = 0.;
  b[0] = 0.;
  b[1] = 0.;
  
  // Double to float
  float fx[3];
  float fb[3];
  for( Int_t i = 0; i < 3; i++)
    {
      fx[i] = (float) x[i];
      fb[i] = (float) b[i];
    }

//printf("Pos: %lf %lf %lf \n",fx[0],fx[1],fx[2]);
  Int_t fldflag = 0;
  if( fabs(x[0]) < 5. && fabs(x[1]) < 5. && fabs(x[2])<42.) fldflag = gufl(fx,fb);
  
  //Float to Double
  if(!fldflag) 
    for( Int_t i = 0; i < 3; i++ ) b[i] = (Double_t) fb[i];
  
  // set B = 0
  //for( Int_t i = 0; i < 3; i++ ) b[i] = 0.;
  
  //printf("Pos: %lf %lf %lf B: %lf %lf %lf\n\n",x[0],x[1],x[2],b[0],b[1],b[2]);
  
}
