//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TAlphaMCTrack                                                        //
//                                                                      //
// Monte Carlo Track
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include "TAlphaMCTrack.h"

#include <iostream>

ClassImp(TAlphaMCTrack);

//______________________________________________________________________________
TAlphaMCTrack::TAlphaMCTrack()
{
  ClearTrack();
  //fPosition(10);
//  fPosition.SetOwner(kTRUE);
}

//______________________________________________________________________________
TAlphaMCTrack::TAlphaMCTrack( TAlphaMCTrack* &track )
{
  fParticleID = track->GetParticleID();		
  fPx = track->Px();
  fPy = track->Py();
  fPz = track->Pz();
  fEtot = track->Etot();
  fNSil = track->GetNSil();
  fNlayer1 = track->GetNlayer1();
  fNlayer2 = track->GetNlayer2();
  fNlayer3 = track->GetNlayer3();
  fParent = track->GetParent();
  fStatus = track->GetStatus();
  fIsTrack = track->IsTrack();
  for(Int_t i = 0; i<8; i++)
    {
      fx[i] = track->X(i);
      fy[i] = track->Y(i);
      fz[i] = track->Z(i);
      fSilNum[i] = track->GetSilNum(i);
    }
    
//   fPosition=track->GetPositionArray();
//   fPosition.SetOwner(kTRUE);
}

//______________________________________________________________________________
TAlphaMCTrack::~TAlphaMCTrack()
{ }

//______________________________________________________________________________
void TAlphaMCTrack::ClearTrack()
{
  fParticleID = 0;
  fPx = 0.;
  fPy = 0.;
  fPz = 0.;
  fEtot = 0.;
  fNSil = 0;
  fNlayer1 = 0;
  fNlayer2 = 0;
  fNlayer3 = 0;
  fParent = 0;
  fStatus = 0;
  for(Int_t i = 0; i<8; i++)
    {
      fx[i] = 0.;
      fy[i] = 0.;
      fz[i] = 0.;
      fSilNum[i] = -1;
    }

  fIsTrack = kFALSE;
  //std::cout<<"Clear"<<std::endl;
//  fPosition.Delete();
  
}

// //______________________________________________________________________________
// Int_t TAlphaMCTrack::StorePosition(Double_t xx, Double_t yy, Double_t zz)
// {
// //	std::cout<<xx<<"\t"<<yy<<"\t"<<zz<<std::endl;
// 	fPosition.Add(new TVector3(xx, yy, zz));
// 	return fPosition.GetEntriesFast();
// }
// 
// //______________________________________________________________________________
// TVector3* TAlphaMCTrack::RetrievePosition(Int_t i)
// {
// 	if(i<fPosition.GetEntriesFast())
// 		return (TVector3*) fPosition.At(i);
// 	else
// 		return 0;
// }
