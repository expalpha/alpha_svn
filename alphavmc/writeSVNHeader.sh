#!/bin/bash

echo "//TSiliconLibVersion.h: I am a file that should not be committed to the SVN!"> include/TvmcVersion.h
echo "//                      I am created by the makefile">> include/TvmcVersion.h
echo "#ifndef _VMCVersion_
#define _VMCVersion_
#include \"TString.h\"
#include <TObject.h>
class TvmcVersion: public TObject {
private:
">> include/TvmcVersion.h
  echo -n "TString _vmcSVNRevision_=\"">> include/TvmcVersion.h
  svnversion -n ${RELEASE} >> include/TvmcVersion.h
  echo "\";"  >> include/TvmcVersion.h

  #Record the full SVN info
  echo -n "TString _vmcSVNFull_=\"" >> include/TvmcVersion.h
  echo -n `svn info ${RELEASE} | sed -E ':a;N;$!ba;s/\r{0,1}\n/\\n/g' ` >> include/TvmcVersion.h
  echo  "\";">> include/TvmcVersion.h
   if [ "$RPLATFORM" == "macosx64" ]; then
    echo 'unsigned int gvmcTime=' `stat include/TvmcVersion.h | awk -F ' ' '{print $2}'` ';'>> include/TvmcVersion.h
  else
    echo 'unsigned int gvmcTime='`stat include/TvmcVersion.h -c %Y`';'>> include/TvmcVersion.h
  fi
echo " 
public:
  TvmcVersion();
  virtual ~TvmcVersion();
  
  TString GetvmcSVNRevision() { return _vmcSVNRevision_; }
  TString GetvmcSVNFull() { return _vmcSVNFull_; }
  unsigned int GetgvmcTime() { return gvmcTime; }
  ClassDef(TvmcVersion,1);
};
#endif
">> include/TvmcVersion.h

# C file:
echo "
#include \"TvmcVersion.h\"
ClassImp(TvmcVersion);


//______________________________________________________________________________
TvmcVersion::TvmcVersion()
{
" > src/TvmcVersion.cxx

 
echo "}
TvmcVersion::~TvmcVersion()
{
}" >> src/TvmcVersion.cxx
#echo "#endif" >> include/TvmcVersion.h
