#!/bin/bash
#overwrite tree output dir (set as home directory of local processing machine)
export TREE=`pwd`
#overwrite midas file store dir
export MID=`pwd`

echo "TREE:${TREE}"
echo "MID:${MID}"
#Go to folder where alphaSoftware is installed (AFS, CVMFS or EOS)
cd /afs/cern.ch/user/j/jmckenna/alphaSoftware2017
#set up environment variables (including which gcc and root)
source myconfig.sh
#overwrite tree output dir ~(set as home directory of local processing machine
export TREEFILES=${TREE}
#overwrite midas file store dir
export MIDASDIR=${MID}

echo "TREEFILES:${TREEFILES}"
echo "MIDASDIR:${MIDASDIR}"

#Print hostname...
echo "Hostname:"
hostname
#Setup input vars
RUNNO=${1}
echo "input 1: ${1}"
echo "input 2: ${2}"
echo "input 3: ${3}"

#I want to run alphaAnalysis... go to alphaAnalysis folder...
cd alphaAnalysis

 #EOS flag should be used in batch... this fetches each run file as its needed
 #experimental cut tuned in this demo
./alphaAnalysis.exe --EOS --nsigma ${2} --psigma ${3} ${MIDASDIR}/run${RUNNO}sub00000.mid.gz

#Full midas path needed despite overwriting MIDASDIR environment var...?

echo "TREEFILES:${TREEFILES}"
echo "MIDASDIR:${MIDASDIR}"

cd macros/
#Load plot dumps and analyse results with some fancy macro
echo ".L PlotDumps.C+
PrintDetectorQOD(${RUNNO})
.q
" | root -l -b > Run${RUNNO}_nSigma${2}_pSigma${3}.log;
cp Run${RUNNO}_nSigma${2}_pSigma${3}.log /eos/users/j/jmckenna/2D_Scan_log/
#Use EOS to copy final output tree to a demo directory
eos cp ${TREEFILES}/tree${RUNNO}offline.root /eos/ad/alpha/demodir/
#ok the above isnt working... if you want the root file to land in your 
#launch folder dont delete it like this:
rm -v ${TREEFILES}/tree${RUNNO}offline.root
