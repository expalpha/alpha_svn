CERN Batch processing instrucitons: CONDOR
Full documentation to get started is available here: 
http://batchdocs.web.cern.ch/batchdocs/ 
In particular, the  sections here: 
http://batchdocs.web.cern.ch/batchdocs/local/index.html

I have setup in this folder the script:
demo_job.sh (a script containing a demo script to run alphaAnalysis)
and:
demo.sub (a demo job submission script).

To submit a job run:
condor_submit demo.sub

To monitor the job run
condor_q



NOTES:

Condor does not yet support the launching scripts from EOS (but code and files can be on EOS). I am told launching from EOS will soon be added. 
There are issues copying final tree file to a shared EOS directoy... this is a work in progress
