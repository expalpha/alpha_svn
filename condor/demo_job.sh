#!/bin/bash
#overwrite tree output dir (set as home directory of local processing machine)
export TREE=`pwd`
#overwrite midas file store dir
export MID=`pwd`/midaadata
export NODEHOME=`pwd`

echo "TREE:${TREE}"
echo "MID:${MID}"
#Go to folder where alphaSoftware is installed (AFS, CVMFS or EOS)
cd /afs/cern.ch/work/j/jmckenna/alphaSoftware
#cd /afs/cern.ch/user/j/jmckenna/alphaSoftware2017
echo "CodePATH:" `pwd`
#set up environment variables (including which gcc and root)
source myconfig.sh
#overwrite tree output dir ~(set as home directory of local processing machine
export TREEFILES=${TREE}
export STRIPFILES=${TREE}
#export STRIPFILES="/eos/experiment/alpha/alphaStrips/"

#overwrite midas file store dir
export MIDASDIR=${MID}
mkdir ${MIDASDIR}
#${MID}

echo "TREEFILES:${TREEFILES}"
echo "MIDASDIR:${MIDASDIR}"

#Print hostname...
echo "Hostname:"
hostname
#Setup input vars
RUNNO=${1}
echo "input 1: ${1}"
echo "input 2: ${2}"
echo "input 3: ${3}"

#eos cp /eos/experiment/alpha/midasdata/run${RUNNO}sub*.mid.gz ${MIDASDIR}
#I want to run alphaAnalysis... go to alphaAnalysis folder...
#cd alphaAnalysis
 #EOS flag should be used in batch... this fetches each run file as its needed
 #experimental cut tuned in this demo

cd ${RELEASE}/alphaStrips/
./alphaStrips.exe  --EOS ${MIDASDIR}/run${RUNNO}sub00000.mid.gz
cd ${RELEASE}/alphaAnalysis/
./alphaAnalysis.exe --EOS ${MIDASDIR}/run${RUNNO}sub00000.mid.gz
cd ${RELEASE}/alphaAnalysis/macros
./BasicBaselines.sh ${RUNNO}
#Full midas path needed despite overwriting MIDASDIR environment var...?

echo "TREEFILES:${TREEFILES}"
echo "MIDASDIR:${MIDASDIR}"

eos cp ${TREEFILES}/tree${RUNNO}*.root /eos/experiment/alpha/alphaTrees/
eos cp ${STRIPFILES}/alphaStrips${RUNNO}*.root /eos/experiment/alpha/alphaStrips/
#cp -v ${TREEFILES}/*.root /eos/experiment/alpha/joseph/
rm -v ${TREEFILES}/tree${RUNNO}offline.root
rm -v ${STRIPFILES}/alphaStrips${RUNNO}offline.root

#ok the above isnt working... if you want the root file to land in your 
#launch folder dont delete it like this:

