//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// 05-May-2015  S. Freeman, s6freema@uwaterloo.ca
//
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "G4RunManager.hh"
#include "G4UImanager.hh"

#include "DetectorConstruction.hh"
#include "PhysicsList.hh"
#include "ActionInitialization.hh"

#include <TMath.h>
#include <fstream>

#ifdef G4VIS_USE
#include "G4VisExecutive.hh"
#endif

#ifdef G4UI_USE
#include "G4UIExecutive.hh"
#endif

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

int main(int argc, char** argv)
{

  // Choose the Random engine
  CLHEP::HepRandom::setTheEngine(new CLHEP::RanecuEngine);

  char setname[80];
  sprintf(setname,"%s/settings.dat",getenv("RUN_ALPHA2"));
  std::ifstream fset(setname);
  //FIXME: fill geometrical parameters if applicable
  fset.close();

  // Construct the default run manager
  G4RunManager* runMan = new G4RunManager;

  // Initialize detector, physics list, action initialization
  runMan->SetUserInitialization(new DetectorConstruction);

  runMan->SetUserInitialization(new PhysicsList);

  runMan->SetUserInitialization(new ActionInitialization);

  // Initialize G4 kernel
  runMan->Initialize();

  // get pointer to UI manager
  G4UImanager* UIMan = G4UImanager::GetUIpointer();

  if(argc != 1){ // batch mode

    G4String command = "/control/execute ";
    G4String fileName = argv[1];
    UIMan->ApplyCommand(command+fileName);

  } else { // interactive mode: define visualization and UI terminal

#ifdef G4VIS_USE
    G4VisManager* visMan = new G4VisExecutive;
    visMan->Initialize();
#ifdef G4UI_USE
    G4UIExecutive* ui = new G4UIExecutive(argc, argv);
#endif
    UIMan->ApplyCommand("/control/execute vis.mac");
#endif

#ifdef G4UI_USE
    ui->SessionStart();
    delete ui;
#endif

#ifdef G4VIS_USE
    delete visMan;
#endif

  }

  // Job Termination
  delete runMan;

  return 0;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
