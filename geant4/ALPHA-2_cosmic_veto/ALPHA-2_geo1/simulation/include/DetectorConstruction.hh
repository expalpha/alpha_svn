//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// May-05-2015, S. Freeman, s6freema@uwaterloo.ca
//
/// \file DetectorConstruction.hh
/// \brief Definition of DetectorConstruction class for ALPHA-2

#ifndef DetectorConstruction_h
#define DetectorConstruction_h 1

#include "G4VUserDetectorConstruction.hh"

#include "globals.hh"

class G4VPhysicalVolume;

class DetectorMessenger;

class G4LogicalVolume;

// DetectorConstruction specifies geometry and materials of detector
class DetectorConstruction : public G4VUserDetectorConstruction
{

public:
  DetectorConstruction();
  virtual ~DetectorConstruction();
  
  virtual G4VPhysicalVolume* Construct();
  virtual void ConstructSDandField();
  void UpdateGeometry();

  inline void SetTrapLength(G4double l) { fTraplen=l; }
  inline G4double GetTrapLength() { return fTraplen; }

private:
  G4double fTraplen;

  DetectorMessenger* fpDetectorMessenger;

  // logical volumes for sensitive volumes at class level as in example B5
  // for access in ConstructSDandField

  G4LogicalVolume* uscint_logic;
  G4LogicalVolume* annihil_logic;
  G4LogicalVolume* lscint_logic;

  G4LogicalVolume* mag_logic;

  G4LogicalVolume* silicon_inner_logic;
  G4LogicalVolume* silicon_middle_logic;
  G4LogicalVolume* silicon_outer_logic;

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
