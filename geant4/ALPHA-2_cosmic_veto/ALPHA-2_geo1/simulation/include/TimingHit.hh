//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// May-07-2015, S. Freeman, s6freema@uwaterloo.ca

#ifndef TimingHit_h
#define TimingHit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"

#include "G4ThreeVector.hh"

class G4AttDef;

// all sensitive detectors store information through particle track hits as they are defined here

class TimingHit : public G4VHit
{

public:
  TimingHit();
  ~TimingHit();
  TimingHit(const TimingHit &right);

  const TimingHit& operator=(const TimingHit &right);
  G4int operator==(const TimingHit &right) const;
  
  inline void *operator new(size_t);
  inline void operator delete(void *aHit);
  
  void Draw();
  void Print();

  const std::map<G4String,G4AttDef>* GetAttDefs() const;
  std::vector<G4AttValue>* CreateAttValues() const;

  inline void SetTrackID(G4int tID) { trackID = tID; }
  inline G4int GetTrackID() { return trackID; }
  inline void SetPDGcode(G4int code) { PDGcode = code; }
  inline G4int GetPDGcode() { return PDGcode; }
  inline void SetTime(G4double t) { globtime = t; }
  inline G4double GetTime() { return globtime; }
  inline void SetVolumeID(G4int vID) { volumeID = vID; }
  inline G4int GetVolumeID() { return volumeID; }
  inline void SetMadeInSD(G4bool b) { madeinSD = b; }
  inline G4bool GetMadeInSD() { return madeinSD; }
  inline void SetParentID(G4int pID) { parentID = pID; }
  inline G4int GetParentID() { return parentID; }
  inline void SetEdep(G4double de) { edep = de; }
  inline G4double GetEdep() { return edep; }

  inline void SetPosition(G4ThreeVector s) { pos = s; }
  inline G4double GetPosX() { return pos.x(); }
  inline G4double GetPosY() { return pos.y(); }
  inline G4double GetPosZ() { return pos.z(); }

  inline void SetDirection(G4ThreeVector p) { dir = p; }
  inline G4double GetDirX() { return dir.x(); }
  inline G4double GetDirY() { return dir.y(); }
  inline G4double GetDirZ() { return dir.z(); }

private:
  G4double      globtime;
  G4ThreeVector pos;
  G4ThreeVector dir;
  G4int         trackID;
  G4int         PDGcode;
  G4int         volumeID;
  G4bool        madeinSD;
  G4int         parentID;
  G4double      edep;

  static std::map<G4String,G4AttDef> fAttDefs;

};

typedef G4THitsCollection<TimingHit> TimingHitsCollection;

extern G4Allocator<TimingHit> TimingHitAllocator;

inline void* TimingHit::operator new(size_t)
{
  void *aHit;
  aHit = (void *) TimingHitAllocator.MallocSingle();
  return aHit;
}

inline void TimingHit::operator delete(void *aHit)
{
  TimingHitAllocator.FreeSingle( (TimingHit*) aHit);
}

#endif
