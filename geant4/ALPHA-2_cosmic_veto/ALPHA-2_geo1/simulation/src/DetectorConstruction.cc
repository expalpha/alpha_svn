//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id$
//

#include "DetectorConstruction.hh"

#include "globals.hh"
#include "G4ExceptionSeverity.hh"

#include "G4RunManager.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4RotationMatrix.hh"
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"
#include "G4UnitsTable.hh"

#include "G4GeometryManager.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4SolidStore.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"

#include "G4Region.hh"
#include "G4RegionStore.hh"

#include "DetectorMessenger.hh"

#include "G4TransportationManager.hh"
#include "G4FieldManager.hh"
#include "G4UniformMagField.hh"

#include "G4SDManager.hh"
#include "TimingSD.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::DetectorConstruction()
  : G4VUserDetectorConstruction(),
    uscint_logic(0), annihil_logic(0), lscint_logic(0),
    mag_logic(0),
    silicon_inner_logic(0), silicon_middle_logic(0), silicon_outer_logic(0)
{
  fTraplen=1.5*m;

  fpDetectorMessenger = new DetectorMessenger(this);   
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::~DetectorConstruction()
{
  if(fpDetectorMessenger) delete fpDetectorMessenger;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

// called by geant4
G4VPhysicalVolume* DetectorConstruction::Construct(){

  //--------------------------------------------------------------------

  G4double world_X = 10.0*m;
  G4double world_Y = world_X;
  G4double world_Z = world_X;

  G4double hpanel_width = 400*mm;
  G4double hpanel_height = 600*mm;
  G4double hpanel_thick = 10*mm;
  G4double hpanel_vert_displacement = 100*mm;
  G4double hpanel_left_dist = 785*mm;
  G4double hpanel_right_dist = 803*mm;

  G4double vpanel_width = 1525*mm;
  G4double vpanel_ulength = 2400*mm;
  G4double vpanel_llength = 1250*mm;
  G4double vpanel_thick = 10*mm;
  G4double vpanel_uvert_displacement = 3.0*m;
  G4double vpanel_lvert_displacement = 1.4*m;
  G4double vpanel_u_forward_disp = 0.5*vpanel_ulength - 830*mm;

  G4double silicon_length = 46.0*cm;
  G4double silicon_thick = 0.3*mm;
  G4double silicon_inner_IR = 91.75*mm - 0.5*silicon_thick;
  G4double silicon_middle_IR = 110.75*mm - 0.5*silicon_thick;
  G4double silicon_outer_IR = 129.75*mm - 0.5*silicon_thick;

  G4double trap_height = fTraplen;
  G4double trap_radius = 2.2275*cm;
  G4double trap_thick = 1.5*mm;

  G4double annihil_height = 25.0*cm;
  G4double annihil_radius = 22.25*mm;

  G4double mag_height = trap_height;
  G4double mag_radius = annihil_radius;

  G4double UHV_radius = trap_radius+trap_thick;
  G4double UHV_thick = 1.25*mm;

  G4double OVCin_radius = 5.5*cm;
  G4double OVCin_thick = 0.21*cm;

  G4double shld_radius = 6.2*cm;
  G4double shld_thick = 0.32*cm;

  G4double OVCout_radius = 7.6*cm;
  G4double OVCout_thick = 0.34*cm;

  G4double oct_radius = UHV_radius+UHV_thick;
  G4double oct_thick = 1.4*cm;

  G4double mirr_radius = oct_radius+oct_thick;
  G4double mirr_thick = 0.5*cm;
  G4double mirr_width = 4.*cm;

  //--------------------------------------------------------------------

  // Get nist material manager
  G4NistManager* nist = G4NistManager::Instance();

  G4Material* vacuum   = nist->FindOrBuildMaterial("G4_Galactic");
  //  G4cout<<"vacuum: "<<G4BestUnit(vacuum->GetRadlen(),"Length")<<G4endl;
  //  G4cout<<vacuum<<G4endl;
  G4Material* air      = nist->FindOrBuildMaterial("G4_AIR");
  //  G4cout<<air<<G4endl;
  //  G4cout<<"air: "<<G4BestUnit(air->GetRadlen(),"Length")<<G4endl;
  //  G4Material* elec_mat = nist->FindOrBuildMaterial("G4_Au");
  G4Material* elec_mat = nist->FindOrBuildMaterial("G4_Al");
  //  G4cout<<"electrodes: "<<G4BestUnit(elec_mat->GetRadlen(),"Length")<<G4endl;
  // G4cout<<"electrodes: "<<G4BestUnit(elec_mat->GetDensity(),"Volumic Mass")<<G4endl;
  //  G4cout<<elec_mat<<G4endl;
  G4Material* chamb_mat = nist->FindOrBuildMaterial("G4_STAINLESS-STEEL");
  //  G4cout<<"chamber: "<<G4BestUnit(chamb_mat->GetRadlen(),"Length")<<G4endl;
  G4Material* shld_mat = nist->FindOrBuildMaterial("G4_Cu");
  //  G4cout<<"shield: "<<G4BestUnit(shld_mat->GetRadlen(),"Length")<<G4endl;

  G4Element* He = nist->FindOrBuildElement("He");
  G4Material* lHe = new G4Material("lHe",0.125*g/cm3,1);
  lHe->AddElement(He,1.);
  //  G4cout<<"LHe: "<<G4BestUnit(lHe->GetRadlen(),"Length")<<G4endl;

   // define elements for superconducting magnets
  G4Element* Cu = nist->FindOrBuildElement("Cu");
  G4Element* Nb = nist->FindOrBuildElement("Nb");
  G4Element* Ti = nist->FindOrBuildElement("Ti");
  G4Material* mag_mat = new G4Material("CuNbTi",5.6*g/cm3,3);
  mag_mat->AddElement(Cu,0.29);
  mag_mat->AddElement(Nb,0.47);
  mag_mat->AddElement(Ti,0.24);
  //  G4cout<<"magnets: "<<G4BestUnit(mag_mat->GetRadlen(),"Length")<<G4endl;

  // panel material
  //FIXME: there may a better, more specific material choice for scintillator panels that corresponds to the actualy material used for ALPHA-2!
  G4Material* scint_mat = nist->FindOrBuildMaterial("G4_PLASTIC_SC_VINYLTOLUENE");

  // silicon material
  //FIXME: there may a better, more specific material choice for silicon detectors that corresponds to the actualy material used for ALPHA-2!
  G4Material* silicon_mat = nist->FindOrBuildMaterial("G4_Si");

  //--------------------------------------------------------------------
  // Option to switch on/off checking of volumes overlaps
  G4bool checkOverlaps = true;

  //--------------------------------------------------------------------

  // world
  G4Box* solidWorld = new G4Box
    ("World_sol", 0.5*world_X, 0.5*world_Y, 0.5*world_Z);

  G4LogicalVolume* logicWorld = new G4LogicalVolume
    (solidWorld, air, "World_log");
  
  G4VPhysicalVolume* physWorld = new G4PVPlacement
    (0, G4ThreeVector(), logicWorld, "World", 0, false, 0, checkOverlaps);

  // panels in horizontal plane: 2 panels of same solid definition
  G4Box *hscint_solid = new G4Box
    ("hscint", 0.5*hpanel_thick, 0.5*hpanel_height, 0.5*hpanel_width);

  G4LogicalVolume *hscint_logic = new G4LogicalVolume
    (hscint_solid, scint_mat, "hscint");

  G4ThreeVector hscint_lpos = G4ThreeVector(-1.0*(hpanel_left_dist + 0.5*hpanel_thick), hpanel_vert_displacement, 0.0);
  new G4PVPlacement
    (0, hscint_lpos, hscint_logic, "hscint", logicWorld, false, 0, checkOverlaps);

  G4ThreeVector hscint_rpos = G4ThreeVector(hpanel_right_dist + 0.5*hpanel_thick, hpanel_vert_displacement, 0.0);
  new G4PVPlacement
    (0, hscint_rpos, hscint_logic, "hscint", logicWorld, false, 1, checkOverlaps);

  // panels in vertical plane: 2 panels of different solid definitions
  G4Box *uscint_solid = new G4Box
    ("uscint",0.5*vpanel_width,0.5*vpanel_thick,0.5*vpanel_ulength);
  G4Box *lscint_solid = new G4Box
    ("lscint",0.5*vpanel_width,0.5*vpanel_thick,0.5*vpanel_llength);

  uscint_logic = new G4LogicalVolume
    (uscint_solid, scint_mat, "uscint");
  lscint_logic = new G4LogicalVolume
    (lscint_solid, scint_mat, "lscint");

  G4ThreeVector uscint_pos = G4ThreeVector(0.0, vpanel_uvert_displacement + 0.5*vpanel_thick, vpanel_u_forward_disp);
  new G4PVPlacement
    (0, uscint_pos, uscint_logic, "uscint", logicWorld, false, 0, checkOverlaps);

  G4ThreeVector lscint_pos = G4ThreeVector(0.0, -1.0*(vpanel_lvert_displacement + 0.5*vpanel_thick), 0.0);
  new G4PVPlacement
    (0, lscint_pos, lscint_logic, "lscint", logicWorld, false, 0, checkOverlaps);

  // silicon detectors: 3 rings summarize all silicon detectors
  G4Tubs* silicon_inner_solid = new G4Tubs
    ("silicon_inner", silicon_inner_IR, silicon_inner_IR + silicon_thick, 0.5*silicon_length, 0.0, 360.0*deg);
  G4Tubs* silicon_middle_solid = new G4Tubs
    ("silicon_middle", silicon_middle_IR, silicon_middle_IR + silicon_thick, 0.5*silicon_length, 0.0, 360.0*deg);
  G4Tubs* silicon_outer_solid = new G4Tubs
    ("silicon_outer", silicon_outer_IR, silicon_outer_IR + silicon_thick, 0.5*silicon_length, 0.0, 360.0*deg);

  silicon_inner_logic = new G4LogicalVolume
    (silicon_inner_solid, silicon_mat, "silicon_inner");
  silicon_middle_logic = new G4LogicalVolume
    (silicon_middle_solid, silicon_mat, "silicon_middle");
  silicon_outer_logic = new G4LogicalVolume
    (silicon_outer_solid, silicon_mat, "silicon_outer");

  new G4PVPlacement
    (0, G4ThreeVector(), silicon_inner_logic, "silicon_inner", logicWorld, false, 0, checkOverlaps);
  new G4PVPlacement
    (0, G4ThreeVector(), silicon_middle_logic, "silicon_middle", logicWorld, false, 0, checkOverlaps);
  new G4PVPlacement
    (0, G4ThreeVector(), silicon_outer_logic, "silicon_outer", logicWorld, false, 0, checkOverlaps);

  // The Vacuum
  G4Tubs* solidChamber = new G4Tubs
    ("Chamber_sol", 0., OVCout_radius, 0.5*trap_height, 0., 360.*deg);

  G4LogicalVolume* logicChamber = new G4LogicalVolume
    (solidChamber, vacuum, "Chamber_log");

  new G4PVPlacement
    (0, G4ThreeVector(), logicChamber, "Vacuum", logicWorld, false, 0, checkOverlaps);

  // outer OVC
  G4Tubs* solidOVCout = new G4Tubs
    ("OVCout_sol", OVCout_radius, OVCout_radius+OVCout_thick, 0.5*trap_height, 0., 360.*deg);

  G4LogicalVolume* logicOVCout = new G4LogicalVolume
    (solidOVCout, chamb_mat, "OVCout_log");

  new G4PVPlacement
    (0, G4ThreeVector(), logicOVCout, "outerOVC", logicWorld, false, 0, checkOverlaps);

  // heat shield
  G4Tubs* solidShield = new G4Tubs
    ("Shield_sol", shld_radius, shld_radius+shld_thick, 0.5*trap_height, 0.,360.*deg);

  G4LogicalVolume* logicShield = new G4LogicalVolume
    (solidShield, shld_mat, "Shield_log");

  new G4PVPlacement
    (0, G4ThreeVector(), logicShield, "HeatShield", logicChamber, false, 0, checkOverlaps);

  // inner OVC
  G4Tubs* solidOVCin = new G4Tubs
    ("OVCin_sol", OVCin_radius, OVCin_radius+OVCin_thick, 0.5*trap_height, 0., 360.*deg);
      
  G4LogicalVolume* logicOVCin = new G4LogicalVolume
    (solidOVCin, chamb_mat, "OVCin_log");

  new G4PVPlacement
    (0, G4ThreeVector(), logicOVCin, "innerOVC", logicChamber, false, 0, checkOverlaps);  

  // Helium Space
  G4Tubs* solidHe = new G4Tubs
    ("He_sol", UHV_radius+UHV_thick, OVCin_radius, 0.5*trap_height, 0., 360.*deg);

  G4LogicalVolume* logicHe = new G4LogicalVolume
    (solidHe, lHe, "He_log");

  new G4PVPlacement
    (0, G4ThreeVector(), logicHe, "LiquidHelium", logicChamber, false, 0, checkOverlaps);

  // Octupole
  //FIXME: this geometry needs to be updated so that z-length is between external mirror coils
  G4Tubs* solidOct = new G4Tubs
    ("Octupole_sol", oct_radius, oct_radius+oct_thick, 0.5*trap_height, 0., 30.*deg);

  G4LogicalVolume* logicOct = new G4LogicalVolume
    (solidOct, mag_mat, "Octupole_log");

  for(double o=0.; o<8.; ++o){
    G4double phi = 0.25*pi*o;
    G4RotationMatrix *rotm = new G4RotationMatrix(G4ThreeVector(0.0, 0.0, 1.0),
						  phi);
    new G4PVPlacement
      (rotm, G4ThreeVector(), logicOct, "Octupole", logicHe, false, o, checkOverlaps);  
  }

  // Mirror Coils
  G4Tubs* solidMirr = new G4Tubs
    ("Mirror_sol", mirr_radius, mirr_radius+mirr_thick, 0.5*mirr_width, 0., 360.*deg);

  G4LogicalVolume* logicMirr = new G4LogicalVolume
    (solidMirr, mag_mat, "Mirror_log");

  // check with Berkeley code
  G4double coil1_posZ = -0.5*trap_height+20.*cm;
  G4double coil_sep = 6.85*cm;
  for(int j=0; j<5; ++j){
    new G4PVPlacement
      (0, G4ThreeVector(0.,0.,coil1_posZ+j*coil_sep), logicMirr, "MirrorCoils",logicHe, false, j, checkOverlaps);
  }

  // UHV
  G4Tubs* solidUHV = new G4Tubs
    ("UHV_sol", UHV_radius, UHV_radius+UHV_thick, 0.5*trap_height, 0., 360.*deg);

  G4LogicalVolume* logicUHV = new G4LogicalVolume
    (solidUHV, chamb_mat, "UHV_log");

  new G4PVPlacement
    (0, G4ThreeVector(), logicUHV, "UHV", logicChamber, false, 0, checkOverlaps);

  // Electrodes A.K.A. Trap's wall
  //FIXME: make this shorter length
  G4Tubs* solidElec = new G4Tubs
    ("Elec_sol", trap_radius, trap_radius+trap_thick, 0.5*trap_height, 0., 360.*deg);

  G4LogicalVolume* logicElec = new G4LogicalVolume
    (solidElec, elec_mat, "Elec_log");

  new G4PVPlacement
    (0, G4ThreeVector(), logicElec, "Electrodes", logicChamber, false, 0, checkOverlaps);

  // Trap's Volume
  G4Tubs* solidTrap = new G4Tubs
    ("Trap_sol", 0.0, trap_radius, 0.5*trap_height, 0., 360.*deg);
     
  G4LogicalVolume* logicTrap = new G4LogicalVolume
    (solidTrap, vacuum, "Trap_log");
               
  new G4PVPlacement
    (0, G4ThreeVector(), logicTrap, "Trap", logicChamber, false, 0, checkOverlaps);

  // 1 T magnet region
  G4Tubs *mag_solid = new G4Tubs
    ("mag", 0.0, mag_radius, 0.5*mag_height, 0.0, 360.0*deg);

  mag_logic = new G4LogicalVolume
    (mag_solid, vacuum, "mag");

  new G4PVPlacement
    (0, G4ThreeVector(), mag_logic, "mag", logicTrap, false, 0, checkOverlaps);

  // annihilation region
  G4Tubs* annihil_solid = new G4Tubs
    ("annihil", 0.0, annihil_radius, 0.5*annihil_height, 0.0, 360.0*deg);

  annihil_logic = new G4LogicalVolume
    (annihil_solid, vacuum, "annihil");

  new G4PVPlacement
    (0, G4ThreeVector(), annihil_logic, "annihil", mag_logic, false, 0, checkOverlaps);

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  // Visualization attributes
  logicWorld->SetVisAttributes(G4VisAttributes::Invisible);

  G4VisAttributes* ElecVisAtt= new G4VisAttributes(G4Colour(1.0,1.0,0.0,0.8));
  ElecVisAtt->SetVisibility(true);
  logicElec->SetVisAttributes(ElecVisAtt);

  G4VisAttributes* UHVVisAtt= new G4VisAttributes(G4Colour(0.46,0.53,0.6,0.4));
  UHVVisAtt->SetVisibility(true);
  logicUHV->SetVisAttributes(UHVVisAtt);

  G4VisAttributes* HeVisAtt= new G4VisAttributes(G4Colour::Blue());
  HeVisAtt->SetVisibility(true);
  logicHe->SetVisAttributes(HeVisAtt);

  G4VisAttributes* OctVisAtt= new G4VisAttributes(G4Colour::Red());
  OctVisAtt->SetVisibility(true);
  logicOct->SetVisAttributes(OctVisAtt);  
  G4VisAttributes* MirVisAtt= new G4VisAttributes(G4Colour::Green());
  MirVisAtt->SetVisibility(true);
  logicMirr->SetVisAttributes(MirVisAtt);

  G4VisAttributes* OVCVisAtt= new G4VisAttributes(G4Colour(0.46,0.53,0.6,0.8));
  OVCVisAtt->SetVisibility(true);
  logicOVCin->SetVisAttributes(OVCVisAtt);
  logicOVCout->SetVisAttributes(OVCVisAtt);

  G4VisAttributes* ShldVisAtt= new G4VisAttributes(G4Colour(1.0,0.5,0.));
  ShldVisAtt->SetVisibility(true);
  logicShield->SetVisAttributes(ShldVisAtt);

  G4VisAttributes* SiliconVisAtt = new G4VisAttributes(G4Colour(0.5,0.5,0.5));
  ShldVisAtt->SetVisibility(true);
  silicon_inner_logic->SetVisAttributes(SiliconVisAtt);
  silicon_middle_logic->SetVisAttributes(SiliconVisAtt);
  silicon_outer_logic->SetVisAttributes(SiliconVisAtt);
  
  logicChamber->SetVisAttributes(G4VisAttributes::Invisible);
  logicTrap->SetVisAttributes(G4VisAttributes::Invisible);
  mag_logic->SetVisAttributes(G4VisAttributes::Invisible);
  annihil_logic->SetVisAttributes(G4VisAttributes::Invisible);

  //always return the physical World
  return physWorld;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

// called by geant4 just after Construct()
void DetectorConstruction::ConstructSDandField()
{

  // 1 T magnetic field
  G4MagneticField* fMag = new G4UniformMagField(G4ThreeVector(0.0, 0.0, 1.0*tesla));
  G4FieldManager* localFieldMan = new G4FieldManager(fMag);
  localFieldMan->SetDetectorField(fMag);
  localFieldMan->CreateChordFinder(fMag);
  mag_logic->SetFieldManager(localFieldMan, true);

  // sensitive detector volumes
  G4SDManager* SDMan = G4SDManager::GetSDMpointer();

  // two scintillator panels with annihilation region
  TimingSD *uscint_sd = new TimingSD("/uscint_sd");
  SDMan->AddNewDetector(uscint_sd);
  uscint_logic->SetSensitiveDetector(uscint_sd);
  uscint_sd->SetLogicalVolume(uscint_logic);

  TimingSD *annihil_sd = new TimingSD("/annihil_sd");
  SDMan->AddNewDetector(annihil_sd);
  annihil_logic->SetSensitiveDetector(annihil_sd);
  annihil_sd->SetLogicalVolume(annihil_logic);

  TimingSD *lscint_sd = new TimingSD("/lscint_sd");
  SDMan->AddNewDetector(lscint_sd);
  lscint_logic->SetSensitiveDetector(lscint_sd);
  lscint_sd->SetLogicalVolume(lscint_logic);

  // 3 silicon detectors (representing all panels as 3 rings)
  TimingSD *silicon_inner_sd = new TimingSD("/silicon_inner_sd");
  SDMan->AddNewDetector(silicon_inner_sd);
  silicon_inner_logic->SetSensitiveDetector(silicon_inner_sd);
  silicon_inner_sd->SetLogicalVolume(silicon_inner_logic);

  TimingSD *silicon_middle_sd = new TimingSD("/silicon_middle_sd");
  SDMan->AddNewDetector(silicon_middle_sd);
  silicon_middle_logic->SetSensitiveDetector(silicon_middle_sd);
  silicon_middle_sd->SetLogicalVolume(silicon_middle_logic);

  TimingSD *silicon_outer_sd = new TimingSD("/silicon_outer_sd");
  SDMan->AddNewDetector(silicon_outer_sd);
  silicon_outer_logic->SetSensitiveDetector(silicon_outer_sd);
  silicon_outer_sd->SetLogicalVolume(silicon_outer_logic);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

// called by geant4 when ask to update geometry
void DetectorConstruction::UpdateGeometry()
{  
  G4GeometryManager::GetInstance()->OpenGeometry();
  G4PhysicalVolumeStore::GetInstance()->Clean();
  G4LogicalVolumeStore::GetInstance()->Clean();
  G4SolidStore::GetInstance()->Clean();
  //  G4RegionStore::GetInstance()->Clean();
  //  G4SDManager::GetSDMpointer()->Clean();
  G4RunManager::GetRunManager()->DefineWorldVolume(Construct());
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
