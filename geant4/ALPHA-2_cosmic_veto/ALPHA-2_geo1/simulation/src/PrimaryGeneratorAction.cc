//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// $Id$
//
// 

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "PrimaryGeneratorAction.hh"
#include "PrimaryGeneratorMessenger.hh"

#include "SecondaryProducer.hh"
#include "G4PrimaryParticle.hh"
#include "G4PrimaryVertex.hh"

#include "G4RunManager.hh"
#include "G4Event.hh"
#include "G4ParticleGun.hh"

#include "G4RandomDirection.hh"
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"

#include "CRYSetup.h"
#include "CRYGenerator.h"
#include "CRYParticle.h"
#include "CRYUtils.h"
#include "RNGWrapper.hh"

#include <stdio.h>
#include <stdlib.h>
#include <cmath>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PrimaryGeneratorAction::PrimaryGeneratorAction():fType(0)
{
  // create annihilation products
  fHbarAnnihilation = new SecondaryProducer();

  // get annihilation position
  char fname[80];
  sprintf(fname,"%s/annihilation.dat",getenv("RUN_ALPHA2"));
  fin.open(fname,std::ios::in);
  G4cout<<"Annihilation position loaded from "<<fname<<G4endl;

  // define a particle gun
  fParticleGun = new G4ParticleGun();

  // Read the cry input file
  std::ifstream CRYFile;
  char CRYname[80];
  sprintf(CRYname,"%s/cry.file",getenv("RUN_ALPHA2"));
  CRYFile.open(CRYname,std::ios::in);
  char buffer[1000];
  std::string setupString("");
  while ( !CRYFile.getline(buffer,1000).eof())
    {
      setupString.append(buffer);
      setupString.append(" ");
    }
  CRYFile.close();
  char datadir[80];
  sprintf(datadir,"%s",getenv("CRYDATAPATH"));
  // setup CRY generator
  CRYSetup *setup=new CRYSetup(setupString,datadir);
  fCosmicGen = new CRYGenerator(setup);

  // set random number generator
  RNGWrapper<CLHEP::HepRandomEngine>::set(CLHEP::HepRandom::getTheEngine(),&CLHEP::HepRandomEngine::flat);
  setup->setRandomFunction(RNGWrapper<CLHEP::HepRandomEngine>::rng);
  // create a vector to store the CRY particle properties
  fvect=new std::vector<CRYParticle*>;

  //create a messenger for this class
  fMessenger = new PrimaryGeneratorMessenger(this);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
  delete fHbarAnnihilation;
  delete fParticleGun;
  delete fCosmicGen;

  if (fin.is_open()) fin.close();

  delete fMessenger;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

// called by geant4 just before BeginOfEventAction, for each event
void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{

  G4double x0,y0,z0,t0;

  switch(fType){
  case 1:{//NOTE: not in use for analysis
    if( fin.is_open() && fin.good() ){
      fin>>t0>>x0>>y0>>z0;
    } else {
      G4cout<<"Check annihilation data input"<<G4endl;
      assert(0);
    }

    G4PrimaryVertex *vt = new G4PrimaryVertex(x0, y0, z0, t0);
    // produce secondaries
    G4int nSec = fHbarAnnihilation->Produce();
    // loop over the produced secondaries
    for( G4int i=0; i<nSec; ++i ){
      G4PrimaryParticle *pp = fHbarAnnihilation->GetSecondary(i);
      vt->SetPrimary(pp);
    }    
    anEvent->AddPrimaryVertex(vt);

    fHbarAnnihilation->ClearSecondaries();

    break;

  }
  case 2:{// cosmics

    // for a given generated vector of primaries, check that at least one of the generated primaries has a sufficiently vertical direction that it is likely to interact with the geometry; if all primaries generated are too angled, then generate primaries again
    G4bool momentumPass = false;
    G4double cryMomX;
    G4double cryMomY;
    G4double cryMomZ;

    while(momentumPass == false){

      fvect->clear();

      fCosmicGen->genEvent(fvect);

      for(unsigned j = 0;j < fvect->size();j++){
	// the CRY generator by default generates cosmic rays on a 2D-surface in the xy-plane at z = 0, and cosmics are directed towards negative z-values. With coordinate mappings, cosmics in this simulation are generated on a 2D-surface in the xz-plane at y = +3.1m (see below), and cosmics are directed in the negative y-direction.
	// rotate CRY particle momentum direction instead of entire geometry (v is py and w is pz)
	cryMomX = ((*fvect)[j])->u();
	cryMomY = ((*fvect)[j])->w();
	cryMomZ = ((*fvect)[j])->v();

	momentumPass = ValidMomentum(cryMomX, cryMomY, cryMomZ);

	if(momentumPass == true){
	  break;
	}
      }

    }

    G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();

    for ( unsigned j=0; j<fvect->size(); j++){

      fParticleGun->SetParticleDefinition(particleTable->FindParticle((*fvect)[j]->PDGid()));
      fParticleGun->SetParticleEnergy((*fvect)[j]->ke()*MeV);

      // note that the positions also must be mapped as the momentum coordinates are above
      G4double cryPosX = ((*fvect)[j])->x() * m;
      G4double cryPosY = ((*fvect)[j])->z()* m + 310.0*cm; // 310cm above origin, 10cm above upper scintillator
      G4double cryPosZ = ((*fvect)[j])->y() * m;
      fParticleGun->SetParticlePosition
	(G4ThreeVector(cryPosX, cryPosY, cryPosZ));

      cryMomX = ((*fvect)[j])->u();
      cryMomY = ((*fvect)[j])->w();
      cryMomZ = ((*fvect)[j])->v();
      fParticleGun->SetParticleMomentumDirection
	(G4ThreeVector(cryMomX, cryMomY, cryMomZ));

      //NOTE: CRY does return a time for events, but the time value is often some strange value, so I am setting it to 0.
      fParticleGun->SetParticleTime(0);

      fParticleGun->GeneratePrimaryVertex(anEvent);

      delete (*fvect)[j];

    }

    break;

  }
  default:{// annihilations

    // NOTE: annihilations are simulated just inside the cylindirical surface where they annihilate so that they are registered by the 'annihilation region' sensitive detector
    G4double annihil_radius = (22.25*mm - 0.001*mm);
    G4double annihil_length = 25.0*cm;
    G4double theta  = G4UniformRand()*twopi;
    x0     = annihil_radius*cos(theta);
    y0     = annihil_radius*sin(theta);
    z0     = (G4UniformRand()-0.5)*annihil_length;
    t0     = 0.0;
    ofstream fout("outAnn.dat",std::fstream::app);
    fout<<x0/mm<<"\t"<<y0/mm<<"\t"<<z0/mm<<"\t"<<t0<<"\n";
    fout.close();

    // produce secondaries
    G4int nSec = fHbarAnnihilation->Produce();

    // loop over the produced secondaries
    for( G4int i=0; i<nSec; ++i ){
      G4PrimaryVertex *vt = new G4PrimaryVertex(x0, y0, z0, t0);

      G4PrimaryParticle *pp = fHbarAnnihilation->GetSecondary(i);
      vt->SetPrimary(pp);

      anEvent->AddPrimaryVertex(vt);
    }
	
    fHbarAnnihilation->ClearSecondaries();


  }// default
  }// switch case

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool PrimaryGeneratorAction::ValidMomentum(G4double px, G4double py, G4double pz)
{

  G4double zenith = acos(py*py / 
			 (px*px + py*py + pz*pz));

    if(zenith >= 0.28){
      return false;
    } else {
      return true;
    }
}
