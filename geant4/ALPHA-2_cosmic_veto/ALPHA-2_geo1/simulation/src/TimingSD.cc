//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//

#include "TimingSD.hh"
#include <vector>
#include "TimingHit.hh"

#include "G4HCofThisEvent.hh"
#include "G4SDManager.hh"

#include "G4Track.hh"
#include "G4Step.hh"
#include "G4StepPoint.hh"
#include "G4TouchableHistory.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"

#include "G4VProcess.hh"
#include "G4ios.hh"

TimingSD::TimingSD(G4String name)
  : G4VSensitiveDetector(name),
    TimingCollection(0), HCID(-1), LV(0)
{
  // collectionName is a class member of G4VSensitiveDetector -> here sensitive detector names are stored
  collectionName.insert("timingcollection");
}

TimingSD::~TimingSD(){}

// called by geant4 at the beginning of each event
void TimingSD::Initialize(G4HCofThisEvent* HCE)
{

  TimingCollection = new TimingHitsCollection(SensitiveDetectorName, collectionName[0]);

  // HCID does not change from event to event within a run, so initialize at the first event
  if(HCID < 0){
    HCID = G4SDManager::GetSDMpointer()->GetCollectionID(TimingCollection);
  }
  HCE->AddHitsCollection(HCID, TimingCollection);

}

// called by geant4 when a hit in a sensitive detector occurs (ie. with several SD's, a hit in SD #1 trigger ProcessHits for SD #1 and no other SD)
G4bool TimingSD::ProcessHits(G4Step* step, G4TouchableHistory*)
{

  // called by G4SteppingManager when PRE-step-point is in the sensitive detector

  G4int trackID = step->GetTrack()->GetTrackID();
  G4int parentID = step->GetTrack()->GetParentID();
  G4int PDGcode = step->GetTrack()->GetParticleDefinition()->GetPDGEncoding();
  G4int volumeID = 0;
  G4double edep = step->GetTotalEnergyDeposit();

  G4bool madeinSD = false;

  // tracks which impact this SD are saved in a class-level vector trackNums -> this is accessed in EventAction to know how many different tracks have impacted each SD
  if(InTrackRecord(trackID) == false){

    trackNums.push_back(trackID);

    if(InTrackRecord(parentID) == true){
      madeinSD = true;
    }

  }

  G4double globtime = -1.0;
  G4ThreeVector pos = G4ThreeVector();
  G4ThreeVector dir = G4ThreeVector();

  globtime = step->GetPreStepPoint()->GetGlobalTime();
  if(globtime == 0.0){
    madeinSD = true;
  }

  pos.setX((step->GetPreStepPoint()->GetPosition()).x());
  pos.setY((step->GetPreStepPoint()->GetPosition()).y());
  pos.setZ((step->GetPreStepPoint()->GetPosition()).z());

  dir.setX((step->GetPreStepPoint()->GetMomentumDirection()).x());
  dir.setY((step->GetPreStepPoint()->GetMomentumDirection()).y());
  dir.setZ((step->GetPreStepPoint()->GetMomentumDirection()).z());

  if(SensitiveDetectorName == "uscint_sd"){
    volumeID = 1;
  } else if(SensitiveDetectorName == "annihil_sd"){
    volumeID = 2;
  } else if(SensitiveDetectorName == "lscint_sd"){
    volumeID = 3;
  } else if(SensitiveDetectorName == "silicon_inner_sd"){
    volumeID = 4;
  } else if(SensitiveDetectorName == "silicon_middle_sd"){
    volumeID = 5;
  } else if(SensitiveDetectorName == "silicon_outer_sd"){
    volumeID = 6;
  }
 
  TimingHit* newHit = new TimingHit();
  newHit->SetTime(globtime);
  newHit->SetPosition(pos);
  newHit->SetDirection(dir);
  newHit->SetTrackID(trackID);
  newHit->SetPDGcode(PDGcode);
  newHit->SetVolumeID(volumeID);
  newHit->SetMadeInSD(madeinSD);
  newHit->SetParentID(parentID);
  newHit->SetEdep(edep);

  TimingCollection->insert(newHit);

  return true;

}

G4bool TimingSD::InTrackRecord(G4int trackID)
{
  G4int tCount = trackNums.size();
  G4bool inTrack = false;
  for(G4int i = 0;i < tCount;i++){
    if(trackNums[i] == trackID){
      inTrack = true;
      break;
    }
  }

  return inTrack;
}
