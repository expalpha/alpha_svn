#! /bin/bash

echo "set environment variables for ALPHA-2.."

#FIXME: replace DIR with current directory (ie. the directory of this file)
export RUN_ALPHA2=/home/skyler/Desktop/g4_alpha/variations_ALPHA-2/TEMP_ALPHA-2_geo2/run
export SOURCE_ALPHA2=/home/skyler/Desktop/g4_alpha/variations_ALPHA-2/TEMP_ALPHA-2_geo2/simulation
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib

echo "root version `root-config --version`"

echo "done!"