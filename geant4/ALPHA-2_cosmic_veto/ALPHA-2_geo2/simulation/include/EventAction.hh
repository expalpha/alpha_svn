//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// May-05-2015, S. Freeman, s6freema@uwaterloo.ca
//
/// \file include/EventAction.hh
/// \brief Definition of the EventAction class
// 

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef EventAction_h
#define EventAction_h 1

#include "G4UserEventAction.hh"
#include "globals.hh"

#include "strSDTrack.hh"
#include <vector>

class G4Event;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class EventAction : public G4UserEventAction
{

public:
  EventAction();
  virtual ~EventAction();

  void InitializeTreeBranches();
  G4int TrackIndex(G4int trackID, std::vector<strSDTrack>* vSD);
  void ClearEventData();
  void PrintEventSummary();

  void  BeginOfEventAction (const G4Event* event);
  void EndOfEventAction (const G4Event* event);

private:
  G4bool init_HCID;

  G4int HCID_upper;
  G4int HCID_annihil;
  G4int HCID_lower;

  G4int HCID_silicon_inner;
  G4int HCID_silicon_middle;
  G4int HCID_silicon_outer;

  // Global information:
  // 1. Run number
  // 2. Event number
  // 3. Number of primary particles
  // (for each primary particle)
  // 4. Global time at start
  // 5. Track ID
  // 6. PDG code
  // 7. Position
  // 8. Momentum direction
  // (for each SD)
  // 9. Number of tracks

  int runNum;
  int eventNum;
  int primaryCount;
  std::vector<double> initTime;
  std::vector<int> initTrackID;
  std::vector<int> initPDGcode;
  std::vector<double> initPosX;
  std::vector<double> initPosY;
  std::vector<double> initPosZ;
  std::vector<double> initDirX;
  std::vector<double> initDirY;
  std::vector<double> initDirZ;
  std::vector<int> trackCount;

  static const G4int detCount = 6;

  // each track that impacts a sensitive detector has related its info written to file. Individual track summaries, which come as strSDTrack struct's, are saved in a vector for an individual sensitive detector. Note that the following information generally only saves entry and exit information for a track; energy deposit is the only variable that is accumulated from all track hits in the SD.
  // FIXME: due to how geant4 appears to simulate tracks, a hit is generally not produced when a track exits a sensitive detector. Due to this fact, exit information is generally not reliable, and was unused in all analysis.

  // sensitive detector variables:
  // (for each track)
  // 1. Track ID
  // 2. PDG code
  // 3. Boolean for whether track was produced in SD
  // 4. Parent ID
  // 5. Energy deposit
  // (at entry into SD (or when made in SD)
  // 6. Global time
  // 7. Position
  // 8. Momentum direction
  // (upon exiting SD)
  // 9. Global time
  // 10. Position
  // 11. Momentum direction

  std::vector<strSDTrack> upperSD;
  std::vector<strSDTrack> annihilSD;
  std::vector<strSDTrack> lowerSD;
  std::vector<strSDTrack> siInnerSD;
  std::vector<strSDTrack> siMiddleSD;
  std::vector<strSDTrack> siOuterSD;

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
