//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// May-07-2015, S. Freeman, s6freema@uwaterloo.ca
//
// 

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef PrimaryGeneratorAction_h
#define PrimaryGeneratorAction_h 1

#include "G4VUserPrimaryGeneratorAction.hh"
#include "globals.hh"

#include "Randomize.hh"

#include <fstream>
#include <vector>

class G4Event;
class SecondaryProducer;

class G4ParticleGun;
class CRYGenerator;
class CRYParticle;

class PrimaryGeneratorMessenger;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class PrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{

public:
  PrimaryGeneratorAction();    
  virtual ~PrimaryGeneratorAction();

  inline void SetType(G4int t) { fType=t; }

  G4bool ValidMomentum(G4double px, G4double py, G4double pz);

  void GeneratePrimaries(G4Event*);

private:
  G4int    fType;

  std::ifstream      fin;               // get annihilation position
  SecondaryProducer* fHbarAnnihilation; // create annihilation products 

  G4ParticleGun*             fParticleGun; // pointer a to G4 particle generator
  CRYGenerator*              fCosmicGen;   // cosmic generator
  std::vector<CRYParticle*>* fvect;        // vector of generated cosmic particles

  PrimaryGeneratorMessenger* fMessenger; //messenger of this class

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
