//strSDTrack.hh

#ifndef strSDTrack_h
#define strSDTrack_h 1

struct strSDTrack {

  int trackID;
  int PDGcode;
  bool madeinSD;
  int parentID;
  double eDep;

  double entryTime;
  double entryPosX;
  double entryPosY;
  double entryPosZ;
  double entryDirX;
  double entryDirY;
  double entryDirZ;

  double exitTime;
  double exitPosX;
  double exitPosY;
  double exitPosZ;
  double exitDirX;
  double exitDirY;
  double exitDirZ;

};

#endif
