//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file src/EventAction.cc
/// \brief Implementation of the EventAction class
// 

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "G4RunManager.hh"
#include "G4Run.hh"
#include "Run.hh"
#include "RunAction.hh"

#include "G4Event.hh"
#include "EventAction.hh"
#include "G4PrimaryVertex.hh"

#include "G4SDManager.hh"
#include "G4VSensitiveDetector.hh"
#include "TimingSD.hh"
#include "G4VHitsCollection.hh"
#include "G4THitsCollection.hh"
#include "G4VHit.hh"
#include "TimingHit.hh"
#include "G4HCofThisEvent.hh"

#include "G4ThreeVector.hh"
#include "G4UnitsTable.hh"

#include "strSDTrack.hh"
#include <vector>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EventAction::EventAction()
  : init_HCID(false),
    HCID_upper(0), HCID_annihil(0), HCID_lower(0),
    HCID_silicon_inner(0), HCID_silicon_middle(0), HCID_silicon_outer(0)
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EventAction::~EventAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

// called by geant4 after all the primaries are generated for a given event
void EventAction::BeginOfEventAction(const G4Event* event)
{

  // clear vectors that are being used to save data
  ClearEventData();

  G4int evNum = event->GetEventID();
  G4cout << "->Event: " << evNum << G4endl;

  if(init_HCID == false){

    G4SDManager* SDMan = G4SDManager::GetSDMpointer();

    HCID_upper = SDMan->GetCollectionID("uscint_sd/timingcollection");
    HCID_annihil = SDMan->GetCollectionID("annihil_sd/timingcollection");
    HCID_lower = SDMan->GetCollectionID("lscint_sd/timingcollection");

    HCID_silicon_inner = SDMan->GetCollectionID("silicon_inner_sd/timingcollection");
    HCID_silicon_middle = SDMan->GetCollectionID("silicon_middle_sd/timingcollection");
    HCID_silicon_outer = SDMan->GetCollectionID("silicon_outer_sd/timingcollection");

    // on the first call of BeginOf EventAction, initialize the structure of the tree being saved
    InitializeTreeBranches();

    init_HCID = true;

  }

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

// called by geant4 after all simulations have terminated
void EventAction::EndOfEventAction(const G4Event* event)
{

  // at end of event, want to write to all variables before RecordEvent where the trees are filled

  // first save top-level information
  Run* run = (Run*)G4RunManager::GetRunManager()->GetCurrentRun();
  runNum = run->GetRunID();
  eventNum = event->GetEventID();
  primaryCount = event->GetNumberOfPrimaryVertex();

  // then save information on the primary particles
  for(G4int i = 0;i < primaryCount;i++){
    G4PrimaryVertex* v = event->GetPrimaryVertex(i);
    initTime.push_back(v->GetT0());
    initTrackID.push_back(v->GetPrimary()->GetTrackID());
    initPDGcode.push_back(v->GetPrimary()->GetPDGcode());
    initPosX.push_back(v->GetX0());
    initPosY.push_back(v->GetY0());
    initPosZ.push_back(v->GetZ0());
    G4ThreeVector p = v->GetPrimary()->GetMomentum();
    p = p.unit();
    initDirX.push_back(p.x());
    initDirY.push_back(p.y());
    initDirZ.push_back(p.z());
  }

  // choose the correct hits collection to analyze
  G4HCofThisEvent* HCE = event->GetHCofThisEvent();
  TimingHitsCollection* THC;

  // loop over all sensitive detector options
  for(G4int SDID = 1;SDID <= detCount;SDID++){

    std::vector<strSDTrack>* vSD = 0;

    switch(SDID){
    case 1: // upper scintillator
      THC = (TimingHitsCollection*)HCE->GetHC(HCID_upper);
      vSD = &upperSD;
      break;
    case 2: // annihilation region
      THC = (TimingHitsCollection*)HCE->GetHC(HCID_annihil);
      vSD = &annihilSD;
      break;
    case 3: // lower scintillator
      THC = (TimingHitsCollection*)HCE->GetHC(HCID_lower);
      vSD = &lowerSD;
      break;
    case 4: // inner silicon ring
      THC = (TimingHitsCollection*)HCE->GetHC(HCID_silicon_inner);
      vSD = &siInnerSD;
      break;
    case 5: // middle silicon ring
      THC = (TimingHitsCollection*)HCE->GetHC(HCID_silicon_middle);
      vSD = &siMiddleSD;
      break;
    case 6: // outer silicon ring
      THC = (TimingHitsCollection*)HCE->GetHC(HCID_silicon_outer);
      vSD = &siOuterSD;
      break;
    }

    // this prints out all hits encounted by the hits collection
    //THC->PrintAllHits();

    // Count of tracks in selected SD
    G4String SDName = THC->GetSDname();
    G4SDManager* SDMan = G4SDManager::GetSDMpointer();
    TimingSD* tSD = (TimingSD*)SDMan->FindSensitiveDetector(SDName, true);
    std::vector<G4int>* tracks = tSD->GetTrackNums();
    trackCount.push_back((int)tracks->size());

    // initialize size of vectors
    for(G4int i = 0;i < trackCount[SDID - 1];i++){
      (*vSD).push_back(strSDTrack());
      ((*vSD)[i]).trackID = (*tracks)[i];
      ((*vSD)[i]).entryTime = -1.0;
      ((*vSD)[i]).exitTime = -1.0;
    }

    // once done with track ID's in a given SD, clear it
    tracks->clear();

    // if track size is 0, continue
    if(trackCount[SDID - 1] == 0){
      continue;
    }

    size_t hitCount = THC->GetSize();

    // now process all hits
    if(hitCount > 0){

      for(size_t hitIndex = 0;hitIndex < hitCount;hitIndex++){

	// hit me
	TimingHit* hit = (TimingHit*)THC->GetHit(hitIndex);

	// find the vector index for the track that this hit is from
	G4int currIndex = TrackIndex(hit->GetTrackID(), vSD);

	if(currIndex == -1){
	  G4cout << "Problems Occurred with TrackIndex!" << G4endl;
	  continue;
	}

	// store PDG and Parent ID from this hit
	((*vSD)[currIndex]).PDGcode = hit->GetPDGcode();
	((*vSD)[currIndex]).parentID = hit->GetParentID();

	// use timing to determine if current hit should represent the entry information for the track
	if((hit->GetTime() < ((*vSD)[currIndex]).entryTime) ||
	   (((*vSD)[currIndex]).entryTime == -1.0)){

	  ((*vSD)[currIndex]).entryTime = hit->GetTime();

	  ((*vSD)[currIndex]).madeinSD = hit->GetMadeInSD();

	  ((*vSD)[currIndex]).entryPosX = hit->GetPosX();
	  ((*vSD)[currIndex]).entryPosY = hit->GetPosY();
	  ((*vSD)[currIndex]).entryPosZ = hit->GetPosZ();

	  ((*vSD)[currIndex]).entryDirX = hit->GetDirX();
	  ((*vSD)[currIndex]).entryDirY = hit->GetDirY();
	  ((*vSD)[currIndex]).entryDirZ = hit->GetDirZ();

	}

	// use timing to determine if current hit should represent the exit information for the track
	if((hit->GetTime() > ((*vSD)[currIndex]).exitTime) ||
	   (((*vSD)[currIndex]).exitTime == -1.0)){

	  ((*vSD)[currIndex]).exitTime = hit->GetTime();

	  ((*vSD)[currIndex]).exitPosX = hit->GetPosX();
	  ((*vSD)[currIndex]).exitPosY = hit->GetPosY();
	  ((*vSD)[currIndex]).exitPosZ = hit->GetPosZ();

	  ((*vSD)[currIndex]).exitDirX = hit->GetDirX();
	  ((*vSD)[currIndex]).exitDirY = hit->GetDirY();
	  ((*vSD)[currIndex]).exitDirZ = hit->GetDirZ();

	}

	// in any case, accumulate the energy deposit
	((*vSD)[currIndex]).eDep += hit->GetEdep();

      }

    }

  }

  //PrintEventSummary();

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void EventAction::InitializeTreeBranches()
{

  Run* run = (Run*)G4RunManager::GetRunManager()->GetCurrentRun();

  TTree* t = run->GetOutTree();

  t->Branch("runNum", &runNum, "runNum/I");
  t->Branch("eventNum", &eventNum, "eventNum/I");
  t->Branch("primaryCount", &primaryCount, "primaryCount/I");
  t->Branch("initTime", &initTime);
  t->Branch("initTrackID", &initTrackID);
  t->Branch("initPDGcode", &initPDGcode);
  t->Branch("initPosX", &initPosX);
  t->Branch("initPosY", &initPosY);
  t->Branch("initPosZ", &initPosZ);
  t->Branch("initDirX", &initDirX);
  t->Branch("initDirY", &initDirY);
  t->Branch("initDirZ", &initDirZ);

  t->Branch("trackCount", &trackCount);
  t->Branch("upperSD", &upperSD);
  t->Branch("annihilSD", &annihilSD);
  t->Branch("lowerSD", &lowerSD);
  t->Branch("siInnerSD", &siInnerSD);
  t->Branch("siMiddleSD", &siMiddleSD);
  t->Branch("siOuterSD", &siOuterSD);

}

// this helper function locates the index in the given vector that corresponds to the track of the hit being currently analyzed
G4int EventAction::TrackIndex(G4int trackID, std::vector<strSDTrack>* vSD)
{

  G4int vTrCount = vSD->size();
  G4int vIndex = -1;
  for(G4int iTr = 0;iTr < vTrCount;iTr++){
    if(((*vSD)[iTr]).trackID == trackID){
      vIndex = iTr;
      break;
    }
  }

  return vIndex;

}

void EventAction::ClearEventData()
{

  initTime.clear();
  initTrackID.clear();
  initPDGcode.clear();
  initPosX.clear();
  initPosY.clear();
  initPosZ.clear();
  initDirX.clear();
  initDirY.clear();
  initDirZ.clear();
  trackCount.clear();

  upperSD.clear();
  annihilSD.clear();
  lowerSD.clear();
  siInnerSD.clear();
  siMiddleSD.clear();
  siOuterSD.clear();

}

// this function may be calledin EndOfEventAction to display all information that is going to be saved to file
void EventAction::PrintEventSummary()
{

  G4cout
    << "////////// Event " << eventNum << " Summary:" << G4endl
    << primaryCount << " particle(s)" << G4endl;

  for(G4int iTr = 0;iTr < primaryCount;iTr++){
    G4cout
      << "Primary #" << iTr+1 << ":" << G4endl
      << "  Starting at time " << G4BestUnit(initTime[iTr],"Time") << G4endl
      << "  Track ID: " << initTrackID[iTr]
      << "; PDG: " << initPDGcode[iTr] << G4endl
      << "  (x,y,z) = ("
      << G4BestUnit(initPosX[iTr],"Length") << ", "
      << G4BestUnit(initPosY[iTr],"Length") << ", "
      << G4BestUnit(initPosZ[iTr],"Length") << ")" << G4endl
      << "  (px,py,pz) = ("
      << initDirX[iTr] << ","
      << initDirY[iTr] << ","
      << initDirZ[iTr] << ")" << G4endl;
  }

  G4cout << G4endl;

  for(G4int SDID = 1;SDID <= detCount;SDID++){
    G4cout
      << "Sensitive Detector " << SDID
      << " has " << trackCount[SDID - 1] << " distinct track(s)"
      << G4endl;
  }

  G4cout << G4endl;

  for(G4int SDID = 1;SDID <= detCount;SDID++){

    std::vector<strSDTrack>* vSD = 0;
    switch(SDID){
    case 1:
      vSD = &upperSD;
      break;
    case 2:
      vSD = &annihilSD;
      break;
    case 3:
      vSD = &lowerSD;
      break;
    case 4:
      vSD = &siInnerSD;
      break;
    case 5:
      vSD = &siMiddleSD;
      break;
    case 6:
      vSD = &siOuterSD;
      break;
    }

    G4cout << G4endl;

    G4cout << "Looking at Sensitive Detector " << SDID
	   << ":" << G4endl;
    G4cout << "  Vector size = " << vSD->size() << G4endl;
    if(vSD->size() == 0){
      G4cout << "  (no actual hits)" << G4endl;
    }

    G4int tCount = trackCount[SDID - 1];
    for(G4int i = 0;i < tCount;i++){

      G4cout
	<< "  Looking at track " << 	((*vSD)[i]).trackID
	<< ":" << G4endl
	<< "    PDG = " << ((*vSD)[i]).PDGcode << "; "
	<< "Parent ID = " << ((*vSD)[i]).parentID;
      if(((*vSD)[i]).madeinSD == true){
	G4cout << " (made in SD)";
      }

      G4cout
	<< "; "
	<< "Energy Deposit = "
	<< G4BestUnit(((*vSD)[i]).eDep,"Energy")
	<< G4endl
	<< "    Entry (x,y,z,t) = ("
	<< G4BestUnit(((*vSD)[i]).entryPosX,"Length")
	<< ", "
	<< G4BestUnit(((*vSD)[i]).entryPosY,"Length")
	<< ", "
	<< G4BestUnit(((*vSD)[i]).entryPosZ,"Length")
	<< ", "
	<< G4BestUnit(((*vSD)[i]).entryTime,"Time")
	<< ")" << G4endl
	<< "    Entry (px,py,pz) = ("
	<< ((*vSD)[i]).entryDirX << ","
	<< ((*vSD)[i]).entryDirY << ","
	<< ((*vSD)[i]).entryDirZ << ")"
	<< G4endl
	<< "    Exit (x,y,z,t) = ("
	<< G4BestUnit(((*vSD)[i]).exitPosX,"Length")
	<< ", "
	<< G4BestUnit(((*vSD)[i]).exitPosY,"Length")
	<< ", "
	<< G4BestUnit(((*vSD)[i]).exitPosZ,"Length")
	<< ", "
	<< G4BestUnit(((*vSD)[i]).exitTime,"Time")
	<< ")" << G4endl
	<< "    Exit (px,py,pz) ("
	<< ((*vSD)[i]).exitDirX << ","
	<< ((*vSD)[i]).exitDirY << ","
	<< ((*vSD)[i]).exitDirZ << ")"
	<< G4endl;

    }
  }
  G4cout << "///////////////////////////////" << G4endl;
}
