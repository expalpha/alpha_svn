//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//

#include "TimingHit.hh"

#include "G4VVisManager.hh"
#include "G4Circle.hh"
#include "G4Colour.hh"
#include "G4VisAttributes.hh"

#include "G4UnitsTable.hh"
#include "G4AttValue.hh"
#include "G4AttDef.hh"
#include "G4AttCheck.hh"

#include "G4ThreeVector.hh"

G4Allocator<TimingHit> TimingHitAllocator;

TimingHit::TimingHit()
  : G4VHit(),
    globtime(0.0), pos(G4ThreeVector()),
    dir(G4ThreeVector()),
    trackID(0), PDGcode(0),
    volumeID(0), madeinSD(false),
    parentID(0),
    edep(0.0)
{}

TimingHit::~TimingHit()
{}

TimingHit::TimingHit(const TimingHit &right) : G4VHit()
{
  globtime = right.globtime;
  pos = right.pos;
  dir = right.dir;
  trackID  = right.trackID;
  PDGcode  = right.PDGcode;
  volumeID = right.volumeID;
  madeinSD = right.madeinSD;
  parentID = right.parentID;
  edep     = right.edep;
}

const TimingHit& TimingHit::operator=(const TimingHit &right)
{
  globtime = right.globtime;
  pos = right.pos;
  dir = right.dir;
  trackID  = right.trackID;
  PDGcode  = right.PDGcode;
  volumeID = right.volumeID;
  madeinSD = right.madeinSD;
  parentID = right.parentID;
  edep     = right.edep;
  return *this;
}

G4int TimingHit::operator==(const TimingHit &right) const
{
  return (this == &right) ? 1 : 0;
}

void TimingHit::Draw()
{
  G4VVisManager* pVVisManager = G4VVisManager::GetConcreteInstance();
  if(pVVisManager)
  {
    G4Circle circle(pos);
    circle.SetScreenSize(10.0);
    circle.SetFillStyle(G4Circle::filled);
    G4Colour colour(1.,1.,0.);
    G4VisAttributes attribs(colour);
    circle.SetVisAttributes(attribs);
    pVVisManager->Draw(circle);
  }
}

std::map<G4String,G4AttDef> TimingHit::fAttDefs;

const std::map<G4String,G4AttDef>* TimingHit::GetAttDefs() const
{
  // G4AttDefs have to have long life.  Use static member...
  if (fAttDefs.empty()) {
    fAttDefs["HitType"] =
      G4AttDef("HitType","Type of hit","Physics","","G4String");
  }
  return &fAttDefs;
}

std::vector<G4AttValue>* TimingHit::CreateAttValues() const
{
  // Create expendable G4AttsValues for picking...
  std::vector<G4AttValue>* attValues = new std::vector<G4AttValue>;
  attValues->push_back
    (G4AttValue("HitType","TimingHit",""));
  //G4cout << "Checking...\n" << G4AttCheck(attValues, GetAttDefs());
  return attValues;
}

// called by geant4 PrintAllHits(), which I use in EndOfEventAction to view hits
void TimingHit::Print()
{
  G4cout
    << "All hit data:" << G4endl
    << "(x,y,z,t) = "
    << "(" << G4BestUnit(pos.x(),"Length")
    << ", " << G4BestUnit(pos.y(),"Length")
    << ", " << G4BestUnit(pos.z(),"Length")
    << ", " << G4BestUnit(globtime,"Time") << ")" << G4endl
    << "(px,py,pz) = "
    << "(" << dir.x() << "," << dir.y() << "," << dir.z() << ")" << G4endl
    << "Track ID: " << trackID << G4endl
    << "PDGcode: " << PDGcode << G4endl;

  switch(volumeID){
  case 1:
    G4cout << "Volume: " << "Upper Scintillator" << G4endl;
    break;
  case 2:
    G4cout << "Volume: " << "Annihilation Region" << G4endl;
    break;
  case 3:
    G4cout << "Volume: " << "Lower Scintillator" << G4endl;
    break;
  case 4:
    G4cout << "Volume: " << "Inner Silicon" << G4endl;
    break;
  case 5:
    G4cout << "Volume: " << "Middle Silicon" << G4endl;
    break;
  case 6:
    G4cout << "Volume: " << "Outer Silicon" << G4endl;
    break;
  default:
    G4cout << "Volume: " << "INVALID" << G4endl;
    break;
  }
  if(madeinSD == true){
    G4cout << "Track produced in this SD" << G4endl;
  }
  G4cout
    << "Parent Track ID: " << parentID << G4endl
    << "Energy Deposit: " << G4BestUnit(edep,"Energy") <<
    G4endl;
}
