// tof_analysis.h
// May-11-2015, S. Freeman, s6freema@uwaterloo.ca
//
// This root macro looks at the event data generated in the alpha-2
// cosmic veto (this code works with both geo1 and geo2 tree data).
// Much of this is unoptimized since implementation of cuts and
// processing changed often as patterns were discovered.
//
// The general utility is to:
// 1. load a file
// 2. set a number of cuts on particle tracks in tree data
// 3. loop through data and analyze those tracks which remain
// even after these cuts have been made
//
// Output is to .root with a number of histograms--the histograms
// which are filled are independent of the cuts imposed. When
// Step 3 is done, a folder containing all histograms is made in
// the output file--the name of the folder corresponds to the cuts
// made. Any number of these folders can be made for a single output
// file, and the folders made for a given output file are meant to
// have different but similar cuts.

#include "TFile.h"

#include "TTree.h"
#include "TBranch.h"

#include "strSDTrack.hh"
#include <vector>

#include "TH1D.h"
#include "TH2D.h"

// simply open a TBrowser (quicker than the command 'new TBrowser')
void browse();

// the first function to call -> load tree of event data
void LoadFileAndTree(Char_t* fInName);
TFile* f;
Char_t fName[1024];
TTree* t;

Int_t evCount = 0;

TBranch* bRunNum = 0;
Int_t runNum = 0;
TBranch* bEventNum = 0;
Int_t eventNum = 0;
TBranch* bPrimaryCount = 0;
Int_t primaryCount = 0;
TBranch* bInitTime = 0;
std::vector<Double_t>* initTime = 0;
TBranch* bInitTrackID = 0;
std::vector<Int_t>* initTrackID = 0;
TBranch* bInitPDGcode = 0;
std::vector<Int_t>* initPDGcode = 0;
TBranch* bInitPosX = 0;
std::vector<Double_t>* initPosX = 0;
TBranch* bInitPosY = 0;
std::vector<Double_t>* initPosY = 0;
TBranch* bInitPosZ = 0;
std::vector<Double_t>* initPosZ = 0;
TBranch* bInitDirX = 0;
std::vector<Double_t>* initDirX = 0;
TBranch* bInitDirY = 0;
std::vector<Double_t>* initDirY = 0;
TBranch* bInitDirZ = 0;
std::vector<Double_t>* initDirZ = 0;
TBranch* bTrackCount = 0;
std::vector<Int_t>* trackCount = 0;
TBranch* bUpperSD = 0;
std::vector<strSDTrack>* upperSD;
TBranch* bAnnihilSD = 0;
std::vector<strSDTrack>* annihilSD;
TBranch* bLowerSD = 0;
std::vector<strSDTrack>* lowerSD;
TBranch* bInnerSD = 0;
std::vector<strSDTrack>* siInnerSD;
TBranch* bMiddleSD = 0;
std::vector<strSDTrack>* siMiddleSD;
TBranch* bOuterSD = 0;
std::vector<strSDTrack>* siOuterSD;

// called in LoadFileAndTree. See vCut for more detail
void InitializeCutVector(std::vector< std::vector< std::vector<Int_t> > >* v, Int_t val);
// this sets the cut vector (see vCut below) from a vector
// obtained externally. See vFinalCut for more details.
void ExternalCutVector();

// this is called whenever event data is being looped through. It is simply used to reset data extracted from branches in the input file.
void ClearInputData();

// store the list of vectors used for cutting
// vCut is central to analysis. For every particle track in every event, vCut stores a Boolean that is either 1 (include the track in analysis) or 0 (ignore track in analysis). Its size is initialized in LoadFileAndTree and its entries are set to 0 through the use of cut methods.
std::vector< std::vector< std::vector<Int_t> > > vCut;
// After histograms are generated, vFinalCut, with the exact same structure of vCut, contains the information on all tracks used in analysis. It might be intuitive to think that vCut accomplishes this since it decides what is analyzed, but this is not completely true. vCut is changed by 'cut' methods outlined below (ie. cut in energy deposit, pdg, etc) - all of which are cuts that are independent of each other. vCut does not handle the case when cuts are related or dependent on each other (for example, this analysis supports cutting on tracks which hit a volume first, AFTER other cuts, and so it is reliant on other cuts). Thus vFinalCut is a subset of vCut and set in the histogram filling function generate_plots.
std::vector< std::vector< std::vector<Int_t> > > vFinalCut;

// vCut1 and vCut2 can be set using TakeVector. The idea is to then use VectorSubtract to set vCut later on. VectorSUbtract returns the component-wise difference between vCut1 and vCut2.
std::vector< std::vector< std::vector<Int_t> > > vCut1;
std::vector< std::vector< std::vector<Int_t> > > vCut2;
Int_t VectorSubtract(Int_t iEv, Int_t iSD, Int_t iTr);
void TakeVector(std::vector< std::vector< std::vector<Int_t> > >* v, Int_t indexVector);
void PrintDifference();

// General Cuts:

// some cut processes make these selections when cutting:
// - "Init" refers to primary particles (0)
// - "Upper" refers to upper scintillator (1)
// - "Annihil" refers to annihilation region (2)
// - "Lower" refers to lower scintillator (3)
// - "Inner" refers to inner silicon ring (4)
// - "Middle" refers to middle silicon ring (5)
// - "Outer" refers to outer silicon ring (6)
// calling a cutter more than once can only reduce selection

void RegionNumtoString(Int_t regionNum, Char_t* regionName);

// cut on track count range in region
void tracks_setcuts(Int_t minTracks, Int_t maxTracks, Int_t regionNum);
TString cut_Tracks("");

// make maximum time cut in region
// doesn't apply to region Init
void time_setcuts(Double_t maxTime, Int_t regionNum);
TString cut_Time("");

// cut on PDG in region
void PDG_setcuts(Int_t PDGcut, Int_t regionNum);
TString cut_PDG("");

// cut on energy deposit range in region
void energy_setcuts(Double_t minEnergy, Double_t maxEnergy, Int_t regionNum);
TString cut_Energy("");

// cut on min//max of position in region
void pos_setcuts(Double_t minX, Double_t maxX, Double_t minY, Double_t maxY, Double_t minZ, Double_t maxZ, Int_t regionNum);
TString cut_Pos("");

// cut on min//max of direction in region
void dir_setcuts(Double_t minPX, Double_t maxPX, Double_t minPY, Double_t maxPY, Double_t minPZ, Double_t maxPZ, Int_t regionNum);
TString cut_Dir("");

// Special cuts (based on combination of all other cuts):

// Boolean cut on whether only earliest events
void earliest_setcuts(Int_t earliest);
// useEarliest is either 1 (only fill histograms with the earliest track that hits an SD in a given event) or 0 (fill histograms with ALL tracks that hit all SD's in a given event -> this was not used in any analysis)
Int_t useEarliest = 0;
TString cut_Earliest("");

// cut out events with first interaction in region
// this only applied if useEarliest is set to 1
void first_setcuts(Int_t regionNum);
// the entries of cutFirst are either 1 (cut event when the SD of the corresponding index is hti first) or 0 (don't cut (default)). For example, cutFirst[3] = 1 means to cut event that hit the lower scintillator first (it has index 3)
Int_t cutFirst[6] = {0};
TString cut_First("");

// just default cuts to set without doing them manually every time
void default_setcuts();
void PDGCuts();
// sumName stores the folder name to be made in the output
Char_t sumName[1024];

void print_cutdetails();

void all_clearcuts();

// must be called before generate_plots to set the output file. The 'option' is simply the root options for files. It can be "RECREATE", "UPDATE", etc.
void SetOutput(Char_t* fOutName, Char_t* option);
TFile* fOut;
Char_t dirName[1024];
// generate_plots performs Step 3 as outlined at the top of this header.
void generate_plots();
// various helper functions for generate_plots
void InitializeOutputHists();
Int_t FindEarliestIndex(std::vector<strSDTrack>* vSD, Int_t iEv, Int_t iSD);
Double_t RadAngle(Double_t x, Double_t y);
Double_t ULDist(Double_t posX1, Double_t posY1, Double_t posZ1, Double_t posX2, Double_t posY2, Double_t posZ2);

// constant values for cutting
Double_t eThresholdScint = 0.5; //MeV
Double_t eThresholdSilicon = 0.1; //MeV

Double_t tMax = 100.0; //ns

// output histograms
Int_t initialized = 0;

// Init
TH1D* hPrim_Count;
TH1D* hPrim_Time;
TH1D* hPrim_PDG;
TH1D* hPrim_PosX;
TH1D* hPrim_PosY;
TH1D* hPrim_PosZ;
TH1D* hPrim_DirX;
TH1D* hPrim_DirY;
TH1D* hPrim_DirZ;

// Upper
TH1D* hU_Count;
TH1D* hU_Time;
TH1D* hU_PDG;
TH1D* hU_Energy;
TH1D* hU_PosX;
TH1D* hU_PosY;
TH1D* hU_PosZ;
TH1D* hU_DirX;
TH1D* hU_DirY;
TH1D* hU_DirZ;
TH2D* hU_RadPosvsRadDir;

// Annihil
TH1D* hA_Count;
TH1D* hA_Time;
TH1D* hA_PDG;
TH1D* hA_Energy;
TH1D* hA_PosX;
TH1D* hA_PosY;
TH1D* hA_PosZ;
TH1D* hA_DirX;
TH1D* hA_DirY;
TH1D* hA_DirZ;
TH2D* hA_RadPosvsRadDir;

// Lower
TH1D* hL_Count;
TH1D* hL_Time;
TH1D* hL_PDG;
TH1D* hL_Energy;
TH1D* hL_PosX;
TH1D* hL_PosY;
TH1D* hL_PosZ;
TH1D* hL_DirX;
TH1D* hL_DirY;
TH1D* hL_DirZ;
TH2D* hL_RadPosvsRadDir;

// Inner
TH1D* hS_Count;
TH1D* hS_Time;
TH1D* hS_PDG;
TH1D* hS_Energy;
TH1D* hS_PosX;
TH1D* hS_PosY;
TH1D* hS_PosZ;
TH1D* hS_DirX;
TH1D* hS_DirY;
TH1D* hS_DirZ;
TH2D* hS_RadPosvsRadDir;

// Middle
TH1D* hM_Count;
TH1D* hM_Time;
TH1D* hM_PDG;
TH1D* hM_Energy;
TH1D* hM_PosX;
TH1D* hM_PosY;
TH1D* hM_PosZ;
TH1D* hM_DirX;
TH1D* hM_DirY;
TH1D* hM_DirZ;
TH2D* hM_RadPosvsRadDir;

// Outer
TH1D* hO_Count;
TH1D* hO_Time;
TH1D* hO_PDG;
TH1D* hO_Energy;
TH1D* hO_PosX;
TH1D* hO_PosY;
TH1D* hO_PosZ;
TH1D* hO_DirX;
TH1D* hO_DirY;
TH1D* hO_DirZ;
TH2D* hO_RadPosvsRadDir;

// First
TH1D* hFirst_Reg;
TH1D* hFirst_Time;
TH1D* hFirst_PDG;
TH1D* hFirst_Energy;
TH1D* hFirst_PosX;
TH1D* hFirst_PosY;
TH1D* hFirst_PosZ;
TH1D* hFirst_DirX;
TH1D* hFirst_DirY;
TH1D* hFirst_DirZ;

// TOF correlations (TOF := lTime - uTime)
TH1D* hTOF;
TH2D* hTOFvsPrimPosX;
TH2D* hTOFvsPrimPosY;
TH2D* hTOFvsPrimPosZ;
TH2D* hTOFvsUTime;
TH2D* hTOFvsLTime;
TH2D* hTOFvsUEnergy;
TH2D* hTOFvsLEnergy;
TH2D* hTOFvsSEnergy;
TH2D* hTOFvsUZ;
TH2D* hTOFvsLZ;
TH2D* hTOFvsSZ;
TH2D* hTOFvsURadDir;
TH2D* hTOFvsLRadDir;
TH2D* hTOFvsSRadDir;
TH2D* hTOFvsUZDir;
TH2D* hTOFvsLZDir;
TH2D* hTOFvsSZDir;
TH2D* hTOFvsULDist;
TH2D* hTOFvsRadDist;
TH2D* hTOFvsXDist;
TH2D* hTOFvsYDist;
TH2D* hTOFvsZDist;
TH2D* hTOFvsFirstReg;
TH2D* hTOFvsFirstX;
TH2D* hTOFvsFirstY;
TH2D* hTOFvsFirstZ;

// other plots
TH1D* hULDist;
TH2D* hULDistvsZDist;
TH2D* hULDistvsRadDist;
TH2D* hUEnergyvsLEnergy;
TH2D* hPrimPDGvsFirstReg;
TH2D* hURadDirvsLRadDir;
TH2D* hSEnergyvsNormalAngle;

