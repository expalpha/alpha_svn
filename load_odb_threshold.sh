#!/bin/bash

echo "Please consult a detector expert before using this script"
echo "Please consult a detector expert before using this script"
echo "Please consult a detector expert before using this script"

echo "Run No, enter -1 to set file path"
read RUNNO

if [ $RUNNO -lt 0 ]
then
  echo "Give full path the Threshold CSV file"
  read INPUT
else
  echo "Threshold"
  read THRESHOLD
  INPUT=$RELEASE/alphaAnalysis/ThrLevels/Run${RUNNO}Threshold_Levels${THRESHOLD}.csv
fi


echo "Testing $INPUT exists..."
if [ -a $INPUT ]
then
  echo "It exists... loading..."
  sleep 1
  while read line; do #Load threshold values into the array: all_asics
    line=( ${line//,/ } )
    #echo "0: ${line[0]}, 1: ${line[1]}, all: ${line[@]}" 
    all_asics=( ${all_asics[@]}   ${line[@]} );
    #echo "A: ${all_asics[0]}, all: ${all_asics[@]}"
  done < $INPUT
  #echo "All asics ${all_asics[@]}"
  echo -e "ASIC \t Threshold"
  for ASIC in {0..383}
  do
    odbedit -d /Equipment/VF48/Settings/ChSuppThresholds -c "set PerChannel[${ASIC}] ${all_asics[$ASIC]}"
    echo -e "${ASIC} \t ${all_asics[$ASIC]}"
  done
  #EXCEPTIONS:
  odbedit -d /Equipment/VF48/Settings/ChSuppThresholds -c "set PerChannel[267] 0"
  #Quite module
  odbedit -d /Equipment/VF48/Settings/ChSuppThresholds -c "set PerChannel[96] 0"
#Noisy module

  echo "done"
else
  echo "CSV file not found... make sure you include the full path..."
fi

