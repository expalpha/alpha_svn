#!/bin/tcsh

#Functions not supported in tcsh... this is going to get messy... myconfig.sh uses functions...

setenv ANALYSISNOTES "" #Environment varialbe for users to put notes that they would like saved inside the TAnalysisReport object that is saved in root trees
setenv TREEPREFIX "/tree" #Set the default tree file file pattern 
setenv TREEPOSTFIX "offline.root" #Set the default tree file file pattern 

switch (`hostname`)
case alphavme*:
    breaksw
    

case alphacpc0*:
	echo "alphacpc05 or alphacpc09 detected..."
	echo "Setting CentOS7 environment variables"
	
	
	#Set up basic directories:
	setenv RCURRENT $PWD
	#/home/alpha/alphaSoftware2012
	setenv RELEASE /home/alpha/alphaSoftware2012
	setenv STRIPFILES $RELEASE/alphaStrips/data
	setenv TREEFILES $RELEASE/alphaAnalysis/data
	setenv MIDASDIR $RELEASE/alphaAnalysis/midasdata
	#setenv MVATREES $RELEASE/alphaMVA/randforest/seldata	
	setenv MVATREES $TREEFILES
	
	#setenv ROOTSYS /home/alpha/packages/root_v5.34.32_centos7/
	#setenv ROOTSYS /home/alpha/packages/root_build/
	setenv ROOTSYS /home/alpha/packages/root_build_6.14/
	setenv ROOTVER 6.14.00
	setenv PATH $ROOTSYS/bin:$PATH
	source $RELEASE/alphaMVA/SPRconfig.csh
	setenv LD_LIBRARY_PATH $ROOTSYS/lib:/home/alpha/packages/pythia6/pythia6:/home/alpha/packages/SPR-3.3.2/lib
	setenv RPLATFORM `root-config --arch`
	setenv LD_LIBRARY_PATH $RELEASE/alphavmc/lib/tgt_linuxx8664gcc:$LD_LIBRARY_PATH
	setenv ROOT_INCLUDE_PATH ${RELEASE}/alphaAnalysis/lib/include:${RELEASE}/alphavmc/include
	setenv LD_LIBRARY_PATH $ROOTSYS/lib:/home/alpha/packages/pythia6/pythia6:/home/alpha/packages/SPR-3.3.2/lib:$RELEASE/alphaAnalysis/lib
	
	setenv EOS_MGM_URL root://eospublic.cern.ch 
	#Report directories:
	echo  $RELEASE
	#echo  $SVNROOT
	echo  $RCURRENT
	echo  $ROOTSYS
	echo  $ROOTVER
	echo  $OSTYPE
	echo  $MACHTYPE
	gcc -v
	cat /etc/*-release
	#cd $RELEASE
setenv RPLATFORM `root-config --arch`
setenv TMVASYS $ROOTSYS/tmva
setenv PATH $ROOTSYS/bin:$PATH
setenv LD_LIBRARY_PATH ${LD_LIBRARY_PATH}:${RELEASE}/alphavmc/lib/tgt_linuxx8664gcc:$ROOTSYS/lib:/home/alpha/packages/pythia6/pythia6:/home/alpha/packages/SPR-3.3.2/lib:$RELEASE/alphaAnalysis/lib

	breaksw

	
case alphadaq*cern*:

	#Set up basic directories:
	setenv RCURRENT $PWD
	#/home/alpha/alphaSoftware2012
	setenv RELEASE /home/alpha/alphaSoftware2012
	setenv STRIPFILES $RELEASE/alphaStrips/data
	setenv TREEFILES $RELEASE/alphaAnalysis/data
	setenv MIDASDIR $RELEASE/alphaAnalysis/midasdata
	#setenv MVATREES $RELEASE/alphaMVA/randforest/seldata	
	setenv MVATREES $TREEFILES
	
	#setenv ROOTSYS /home/alpha/packages/root_v5.34.32_centos7/
	#setenv ROOTSYS /home/alpha/packages/root_build/
	setenv ROOTSYS /home/alpha/packages/root_build_6.14/
	setenv ROOTVER 6.14.00
	setenv PATH $ROOTSYS/bin:$PATH
	source $RELEASE/alphaMVA/SPRconfig.csh
	setenv LD_LIBRARY_PATH $ROOTSYS/lib:/home/alpha/packages/pythia6/pythia6:/home/alpha/packages/SPR-3.3.2/lib
	setenv RPLATFORM `root-config --arch`
	setenv LD_LIBRARY_PATH ::$LD_LIBRARY_PATH
	setenv ROOT_INCLUDE_PATH ${RELEASE}/alphaAnalysis/lib/include:${RELEASE}/alphavmc/include
	setenv EOS_MGM_URL root://eospublic.cern.ch 
	#Report directories:
	echo  $RELEASE
	#echo  $SVNROOT
	echo  $RCURRENT
	echo  $ROOTSYS
	echo  $ROOTVER
	echo  $OSTYPE
	echo  $MACHTYPE
	gcc -v
	cat /etc/*-release
#	cd $RELEASE

setenv RPLATFORM `root-config --arch`
setenv TMVASYS $ROOTSYS/tmva
setenv PATH $ROOTSYS/bin:$PATH
setenv LD_LIBRARY_PATH ${LD_LIBRARY_PATH}:${RELEASE}/alphavmc/lib/tgt_linuxx8664gcc:$ROOTSYS/lib:/home/alpha/packages/pythia6/pythia6:/home/alpha/packages/SPR-3.3.2/lib:$RELEASE/alphaAnalysis/lib
breaksw
case *triumf.ca
    echo "$HOSTNAME"

	#Set up basic directories:
	setenv RCURRENT $PWD
	setenv RELEASE $RCURRENT
	setenv STRIPFILES $RELEASE/alphaStrips/data
	setenv TREEFILES $RELEASE/alphaAnalysis/data
	setenv MVATREES $RELEASE/alphaMVA/randforest/seldata
	source $RELEASE/alphaMVA/SPRconfig.csh
	setenv MIDASDIR $RELEASE/alphaAnalysis/midasdata
	setenv EOS_MGM_URL root://eospublic.cern.ch #EOS may not work from TRIUMF without kerbos tickets (or at all)
    if `lsb_release -d` == "Description: Scientific Linux release 6.7 (Carbon)" then

	setenv ROOTSYS /home/alpha/packages/root_v5.34.19
	cd   $ROOTSYS/bin
	source thisroot.csh
	cd $RELEASE
	setenv ROOTVER 5.34.19
    else if `lsb_release -d` == "Description: Scientific Linux release 6.6 (Carbon)" then

	setenv ROOTSYS /home/alpha/packages/root_v5.34.19
	cd   $ROOTSYS/bin
	source thisroot.csh
	cd $RELEASE
	setenv ROOTVER 5.34.19

else if `lsb_release -d` == "Description: CentOS Linux release 7.1.1503 (Core)" then

    setenv ROOTSYS /home/alpha/packages/root_v5.34.32
    cd   $ROOTSYS/bin
    source thisroot.csh
    cd $RELEASE
    setenv ROOTVER 5.34.32
    else if `lsb_release -d` =~ "Description: CentOS Linux release 7.3.1611 (Core)" then

	setenv ROOTSYS /home/alpha/packages/root-6.06.08
	cd   $ROOTSYS/bin
	source thisroot.csh
	echo $ROOTSYS
	cd $RELEASE
	setenv ROOTVER 6.06.08

    else if `lsb_release -d` =~ "Description: Fedora release 21 (Twenty One)" then
      setenv ROOTSYS $HOME/packages/root5.32_gcc4.9
    cd $ROOTSYS/bin
    source thisroot.csh
    echo $ROOTSYS
    cd $RELEASE
    setenv ROOTVER 5.32
    setenv RPLATFORM `root-config --arch`
    setenv LD_LIBRARY_PATH $RELEASE/alphavmc/lib/tgt_linuxx8664gcc:$LD_LIBRARY_PATH
    else if `lsb_release -d` =~ "Description: Fedora release 23 (Twenty Three)" then
    setenv ROOTSYS $HOME/packages/root-6.06.08
    cd $ROOTSYS/bin
    source thisroot.csh
    echo $ROOTSYS
    cd $RELEASE
    setenv ROOTVER 6.06.08
    setenv RPLATFORM `root-config --arch`
    setenv LD_LIBRARY_PATH $RELEASE/alphavmc/lib/tgt_linuxx8664gcc:$LD_LIBRARY_PATH
  else
	echo "Set up root for this OS"
    endif
  setenv TMVASYS $ROOTSYS/tmva
  setenv PATH $ROOTSYS/bin:$PATH
  setenv LD_LIBRARY_PATH $ROOTSYS/lib:$HOME/packages/SPR-3.3.2/lib
  setenv RPLATFORM `root-config --arch`
breaksw

case *:
	echo "No case setting for this PC... testing if I am an lxbatch/lxplus or virtual machine..."
	if ( -d "/afs/cern.ch/sw/lcg/external/gcc/4.8/x86_64-slc6/" ) then
		echo "I am connected to AFS, assuming I am not a control room PC... "
		echo "Using AFS root and AFS compiler... (gcc48)"
		echo "Setting lxplus/batch environment variables"
		unset ROOTSYS
		source /afs/cern.ch/project/eos/installation/client/etc/setup.csh
		setenv EOS_MGM_URL root://eospublic.cern.ch
		
		#source /afs/cern.ch/sw/lcg/external/gcc/4.8/x86_64-slc6/setup.csh
		#cd /afs/cern.ch/sw/lcg/app/releases/ROOT/5.34.25/x86_64-slc6-gcc48-opt/root/bin
		#source thisroot.csh
		. /cvmfs/sft.cern.ch/lcg/releases/gcc/4.8.4/x86_64-centos7/setup.csh
		. /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.10.02/x86_64-centos7-gcc48-opt/root/bin/thisroot.csh
		setenv RPLATFORM `root-config --arch`
		setenv LD_LIBRARY_PATH $RELEASE/alphavmc/lib/tgt_linuxx8664gcc:$LD_LIBRARY_PATH
		
			
		#Set up basic directories:
		setenv RCURRENT $PWD
		#/home/alpha/alphaSoftware2012
		setenv RELEASE $RCURRENT
		setenv STRIPFILES $RELEASE/alphaStrips/data
		setenv TREEFILES $RELEASE/alphaAnalysis/data
		setenv MVATREES $RELEASE/alphaMVA/randforest/seldata

		setenv MIDASDIR $RELEASE/alphaAnalysis/midasdata
	
	else
		echo "ERROR: I am neither an lxplus/ lxbatch machine... nor a control room PC... please review $PWD/myconfig.csh... maybe add a new case for $HOST"
	fi
endsw


#The following are probably not needed anymore
#setenv SVNROOT svn://ladd00.triumf.ca/alpha
#setenv PATH=$ROOTSYS/bin:$PATH
#setenv LD_LIBRARY_PATH=$ROOTSYS/lib:/usr/local/cern/pythia6


