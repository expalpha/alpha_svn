#!/bin/bash

alphaSLC6()
{
	echo "Setting SLC6 environment variables"
	export ROOTSYS=/home/alpha/packages/root_v5.34.19/
	export ROOTVER=5.34.19
	export PATH=$ROOTSYS/bin:$PATH
	export LD_LIBRARY_PATH=$ROOTSYS/lib:/home/alpha/packages/pythia6/pythia6:/home/alpha/packages/SPR-3.3.2/lib:$RELEASE/alphaAnalysis/lib:$LD_LIBRARY_PATH
	export RPLATFORM=`root-config --arch`
}
alphaCentOS7()
{
	echo "Setting CentOS7 environment variables"
	#export ROOTSYS=/home/alpha/packages/root_v5.34.32_centos7/
	export ROOTSYS=/home/alpha/packages/root_build_6.14/
	export ROOTVER=6.14.00
	export PATH=$ROOTSYS/bin:$PATH
	export LD_LIBRARY_PATH=$ROOTSYS/lib:/home/alpha/packages/pythia6/pythia6:/home/alpha/packages/SPR-3.3.2/lib:$RELEASE/alphaAnalysis/lib:$LD_LIBRARY_PATH
	export RPLATFORM=`root-config --arch`
}
triumfCentOS7()
{
	echo "Setting CentOS7 environment variables"
	#export ROOTSYS=$HOME/packages/root-6.08.06-install
	export ROOTVER=$(root-config --version | sed 's/\//./g')
	export PATH=$ROOTSYS/bin:$PATH
	export LD_LIBRARY_PATH=$ROOTSYS/lib:$HOME/packages/pythia6:$HOME/packages/SPR-3.3.2/lib
	export RPLATFORM=$(root-config --arch)
	#ln -s $HOME/packages/rootana_pro $RELEASE/rootana
	#ln -s $HOME/packages/geant3.1.14 $RELEASE/alphavmc/geant3
}
standardDirs()
{
	export ANALYSISNOTES="" #Environment varialbe for users to put notes that they would like saved inside the TAnalysisReport object that is saved in root trees
	SOURCE="${BASH_SOURCE[0]}"
	echo $SOURCE

	while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
      DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
      SOURCE="$(readlink "$SOURCE")"
      [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
	done
	DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

#	export RCURRENT=$PWD
	export RCURRENT=$DIR
	#/home/alpha/alphaSoftware2012
	export RELEASE=$RCURRENT
	#$PWD
	export MIDASDIR=$RELEASE/alphaAnalysis/midasdata
	export STRIPFILES=$RELEASE/alphaStrips/data
	export TREEFILES=$RELEASE/alphaAnalysis/data
	export TREEPREFIX="/tree"
	export TREEPOSTFIX="offline.root" #Set alphaAnalysis output filename pattern
	export MVATREES=$RELEASE/alphaMVA/randforest/seldata
	export EOS_MGM_URL=root://eospublic.cern.ch
	export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${RELEASE}/alphaAnalysis/lib
}
rootDepDirs()
{
	export ROOTVER=`root-config --version`
	export RPLATFORM=`root-config --arch`
	#Variables that need to be setup after root is sourced
	export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${RELEASE}/alphavmc/lib/tgt_${RPLATFORM}
	export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${RELEASE}/alphavmc/geant3/lib/tgt_${RPLATFORM}
	export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:$RELEASE/alphavmc/geant3/tgt_${RPLATFORM}/TGeant3
	export ROOT_INCLUDE_PATH=${RELEASE}/alphaAnalysis/lib/include:${RELEASE}/alphavmc/include
	source $RELEASE/alphaMVA/SPRconfig.sh

}
lxplus()
{

	unset ROOTSYS
	#Gone:
	#source /afs/cern.ch/project/eos/installation/client/etc/setup.sh
  if [ `lsb_release -a | grep "Scientific Linux" | wc -c` -gt 5 ]; then 
	echo "Setting (SLC6) lxplus/batch environment variables"
	#source /cvmfs/sft.cern.ch/lcg/views/LCG_88/x86_64-slc6-gcc49-opt/setup.sh
	source /cvmfs/sft.cern.ch/lcg/views/LCG_92/x86_64-slc6-gcc7-opt/setup.sh
    #source /afs/cern.ch/sw/lcg/external/gcc/4.8/x86_64-slc6/setup.sh
    #source /afs/cern.ch/sw/lcg/app/releases/ROOT/6.06.08/x86_64-slc6-gcc48-opt/root/bin/thisroot.sh
  else
    echo "Setting (CentOS7) lxplus/batch environment variables"
	if [ -d "/cvmfs/sft.cern.ch/lcg/releases/gcc/4.8.4/x86_64-centos7/" ]; then
      . /cvmfs/sft.cern.ch/lcg/releases/gcc/4.8.4/x86_64-centos7/setup.sh
      #Use our own build of root (include xrootd,R, Python2.7 and minuit2)
      . /cvmfs/alpha.cern.ch/CC7/packages/root/root_build/bin/thisroot.sh
      #. /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.10.02/x86_64-centos7-gcc48-opt/root/bin/thisroot.sh
    else
      echo "cvmfs not found! Please install and mount cvmfs"
    fi
  fi
  #pyROOT libraries:
  export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/rh/python27/root/usr/lib64/
	export ROOTVER=`root-config --version`
	export RPLATFORM=`root-config --arch`

}
alphaVM()
{
	echo "Setting alpha virtual machine environment variables"
	unset ROOTSYS
	#Gone:
	#source /afs/cern.ch/project/eos/installation/client/etc/setup.sh

	#Settings for alphaAnalysis 2014 - 2016
	#source /afs/cern.ch/sw/lcg/external/gcc/4.8/x86_64-slc6/setup.sh
	#source /afs/cern.ch/sw/lcg/app/releases/ROOT/5.34.25/x86_64-slc6-gcc48-opt/root/bin/thisroot.sh
	#export ROOTVER=5.34.25/x86_64-slc6-gcc48-opt

	#Settings for 2017

	#source /afs/cern.ch/sw/lcg/external/gcc/4.8/x86_64-centos7-gcc48-opt/setup.sh
	#source /afs/cern.ch/sw/lcg/app/releases/ROOT/6.06.08/x86_64-centos7-gcc48-opt/root/bin/thisroot.sh
	
	#. /cvmfs/sft.cern.ch/lcg/contrib/gcc/4.8/x86_64-centos7-gcc48-opt/setup.sh
	#. /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.08.04/x86_64-centos7-gcc48-opt/root/bin/thisroot.sh
	if [ -d "/cvmfs/sft.cern.ch/lcg/releases/gcc/4.8.4/x86_64-centos7/" ]; then
      . /cvmfs/sft.cern.ch/lcg/releases/gcc/4.8.4/x86_64-centos7/setup.sh
      . /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.14.00/x86_64-centos7-gcc48-opt/root/bin/thisroot.sh
    else
      echo "cvmfs not found! Please install and mount cvmfs"
    fi
	export ROOTVER=`root-config --version`
	export RPLATFORM=`root-config --arch`
}
Joe()
{
  . ~/packages/root_6_14_xrootd/bin/thisroot.sh
  	export ROOTVER=`root-config --version`
	export RPLATFORM=`root-config --arch`
}
reportVersions()
{
#echo  $SVNROOT
#echo  $RCURRENT
echo  "Root path:$ROOTSYS"
echo  "Root version:$ROOTVER"
#echo  $OSTYPE
#echo  $MACHTYPE
gcc --version | grep gcc
#cat /etc/*-release
echo "RELEASE: $RELEASE"
#cd $RELEASE
}

STARTDIR=$PWD
echo "############## myconfig.sh ##############" 
if [ `root-config --arch` = "macosx64" ]; then
	echo "mac OS (with root installtion) detected"
	#root and gcc must be installed... root must be configured before this script
	#(source thisroot.sh)... go set it in ~/.profile or custom script
  standardDirs
	rootDepDirs
	#export LD_LIBRARY_PATH=$ROOTSYS/lib:/home/alpha/packages/pythia6/pythia6:/home/alpha/packages/SPR-3.3.2/lib
	export ROOTVER=`root-config --version`
	export RPLATFORM=`root-config --arch`
	reportVersions
  return;
fi #Else if not MacOS then identify machine by hostname
case `hostname` in
alphavme*  )
	echo "alphavme detected..."
	echo "not sourcing alphaAnalysis environment settings"
	;;

alphacpc04* | alphadaq* )
	echo -e " \e[33malphacpc04 or 09 detected...\033[0m"
	standardDirs
	alphaCentOS7
	rootDepDirs
	reportVersions
	;;
alphacpc09* )
	standardDirs
	rootDepDirs
	reportVersions
	;;
alphabeast* )
        Joe
        standardDirs
        rootDepDirs
        reportVersions
        ;;
alpha00.triumf.ca | alpha01.triumf.ca | alpha02.triumf.ca | koala01.cern.ch)
	echo -e " \e[33malphaXX@triumf detected...\033[0m"
	standardDirs
	triumfCentOS7
	rootDepDirs
	reportVersions
	;;

*alpha*.cern.ch | *joe*.cern.ch )
  echo -e "alpha virtual machine detected..."
	standardDirs
	alphaVM
	rootDepDirs
	reportVersions
  ;;
#if you want your own custom setup... you can add more cases here...
* )
	echo "No case setting for this PC... "
	if [ `echo ~/ | grep "/pool/condor/" | wc -c` -gt 3 ]; then
	    echo "Condor node detected"
		standardDirs
		lxplus
		rootDepDirs
		reportVersions
	else
	  if [ -d "/cvmfs/sft.cern.ch/lcg/releases/gcc/4.8.4/x86_64-centos7/" ]; then
		echo "    AFS found, assuming I am not a control room PC... attempting lxbatch/lxplus setup "
		echo "    You may want to add a new case in $RELEASE/myconfig.sh"
		echo "    Using AFS root and AFS compiler..."
		echo ""
		standardDirs
		lxplus
		rootDepDirs
		reportVersions
	  else
		echo "ERROR: I am neither an lxplus/ lxbatch machine... nor a control room PC... please review $RELEASE/myconfig.sh... maybe add a new case for $HOST"
	  fi
	fi

esac
cd $STARTDIR #return to the directory user was in before sourcing...

echo "#########################################"
#The following are probably not needed anymore
#export SVNROOT svn://ladd00.triumf.ca/alpha
#export PATH=$ROOTSYS/bin:$PATH
#export LD_LIBRARY_PATH=$ROOTSYS/lib:/usr/local/cern/pythia6
