#!/bin/bash

# This is a 'super' bash script to handle most opporations for online
# analysis. alphaStrips and alphaAnalysis (with or without reconstruction
# are run as required. If tree files exist, and are approximately the 
# right size (and age in the case of alphaStrips), analysis is skipped.
# Root macros and built per run number, then executed automatically.
# Some common tasks (such as mixing and trapping analysis) save plots 
# automatically.

# It is designed to work on a generalised machine, but it is 
# only tested on alphacpc04 and alphacpc09 (our online analysis machines)
#
# Detector experts should not be dependant on this script, but it may 
# serve as a useful reference.

EXTRA_FLAGS=" "
FORCE_RUN=0
CRUNCH=0

MainMenu()
{
	
	#check run number against current run:
	if [ `hostname` == "alphadaq.cern.ch" ]; then
		Run_number=`odbedit -e ${MIDAS_EXPT_NAME} -c 'ls "/Runinfo/Run number"'`
		number=`echo $Run_number | awk '{print $3}'`
		echo "Current run according to midas: ${number}"
		if [ ${RUN_NO} -gt ${number} ]; then
			echo "Run number ${RUN_NO} hasn't happened yet... go back to working on a time machine"
			exit
		fi
		if [ ${RUN_NO} -eq ${number} ]; then
			STATE=`odbedit -e ${MIDAS_EXPT_NAME} -c 'ls "/Runinfo/state"'`
			STATE_NO=`echo $STATE | awk '{print $2}'`
			if [ ${STATE_NO} -gt 1 ]; then
				echo "Run number ${RUN_NO} is still going... be patient!"
				exit
			fi
		fi
	fi
echo "
################################################################################
			MENU: Please input number below:
################################################################################


1. Pbar lifetime 
2. Pbar Verticies
3. PMT Data
4. Pbar temparature measurment 
5. Advanced Pbar temparature measurement (development)
6. Makoto - prototype dumps

7. Run Cosmic QOD analysis and upload to elog
8. Advanced SVD analysis
0. More information
Please direct any comments towards Joseph :)
---------------------------------------------------------------------
NOTE: PLEASE RUN ME ON ALPHACPC04 or CPC09... you'll also want X11 forwarding...
---------------------------------------------------------------------
"
#9. Help, options and blurb - nothing here yet...
#Planned work: More tests... faster running...

read MENUVAR
case $MENUVAR in
0  ) PrintInfo;;
1  ) LifeTimeMenu;;
2  ) PbarVertexMenu;;
3  ) PbarPMTMenu;;
4  ) echo "AutoTemp removed... development moving ahead too fast... taking you to menu"
PbarTempMenu;;
#runNoRecoAnalysis
#		RunPbarAutoTemp;;
5  ) PbarTempMenu;;
6  ) MacotoMacro;;
7  ) 
	#checkIfCosmicTriggerIsOff $RUN_NO
	echo "Please enter elog entry number you wish to reply to " 
	read ELOGSUBMIT
	if [ $ELOGSUBMIT -le $RUN_NO ]
	then
		echo " Elog number too low"
		exit
	else
		echo "Elog entry: $ELOGSUBMIT"
		echo "If this is correct, please enter: Yes"
		read YES
		if [ "$YES" == "Yes" ]
		then
			echo "Ok... go and make a cup of tea... I will reply to elog entry $ELOGSUBMIT when im done"
			sleep 3
			FORCE_RUN=1
			NOTALK=1
			runStrips $RUN_NO $EXTRA_FLAGS
			EXTRA_FLAGS=" --iscosmic --cosmicutil "
			runAnalysis $RUN_NO "$EXTRA_FLAGS" $FORCE_RUN
			EXTRA_FLAGS=" --adcspecs "
			runAnalysis $RUN_NO "$EXTRA_FLAGS" $FORCE_RUN
			NOTALK=0
			RunDetectorQOD
			reportCosmicAnalysisDone
		else
			echo "String did not match:Yes"
			echo "exiting"
			exit
		fi
	fi
	
	;;
8  ) AdvancedSVD;;
esac
echo "Cleaning up \"MenuMacro${RUN_NO}.C\"..."
rm -v $RELEASE/alphaAnalysis/macros/MenuMacro${RUN_NO}.C
echo "clean! (probably)"

echo "Done so fast? Come back soon!"


}



DecodeBatchFlag()
{
	echo "Decoding flags..."
	echo "All args: $@"
	ELOG="0" #by default, turn elog submission off
	
   while [[ $# > 1 ]]
do
key="$1"

case $key in
    --list)
		LISTRUN=1
		FORCE_RUN=1  #Force re-analysis even if root files exist
		LISTFILE="$2"
		shift # past argument
    ;;
    --NOTALK)
    	NOTALK=1 #Don't announced passed cuts
	#	shift
	;;
    --FORCE)
		FORCE_RUN=1
		shift
	;;
    --STRIPFLAGS)
		STRIPFLAGS+=" $2 "
		if [[ $2 != "--"* ]]
		then
			STRIPFLAGS+=" $2 "
		fi
		shift # past argument
    ;;
    --ALPHAFLAGS)
		ALPHAFLAGS+=" $2 "
		if [[ $2 != "--"* ]]
		then
			ALPHAFLAGS+=" $2 "
		fi
		shift # past argument
    ;;
	--MAXJOBS)
		maxjobs="$2"
		shift
    ;;
    --SKIPSTRIPS )
		SKIPSTRIPS=1
    ;;
	--QUENCH )
		echo "Running Quench analysis"
		QUENCH=1
		RUN_NO="$2"
		shift
    ;;
    --FRD )
		echo "Running FRD analysis"
		FRD=1
		RUN_NO="$2"
		shift
	;;
    --CRUNCH )
		echo "Running in CRUNCH mode"
		CRUNCH=1
		RUN_NO="$2"
		shift
	;;
    --LXBATCH )
		echo "LXBATCH TASK..."
		LXBATCH=1
		shift
    ;;
    --ELOG )
		ELOG_ID="$2"
		ELOG=1
		if [[ $ELOG_ID -lt 44860 ]]
		then
		echo "Elog number probably too low (less than 44860)"
		exit 0
		fi
		if [[ $ELOG_ID -gt 50000 ]]
		then
		echo "Elog number probably too high (greater than 50000)"
		exit 0
		fi
		shift
    ;;
    --MIXING )
		case `hostname` in
		alphavmcluster* | alphacrunch* | alphanibble* )
	
	echo "Yep, I am an alphacrunch node"
	FORCE_RUN=1
	cd ~/crunchAnalysis #Set which version of alphaAnalysis here!
	source myconfig.sh 
	cd $RELEASE
		echo "Mixing run detected";
		FORCE_RUN=1  #Force re-analysis even if root files exist
		RUN_NO="$2"
		sleep 2
		FORCE_RUN=1
		NOTALK=1
		EXTRA_FLAGS=" --EOS "
		runStrips $RUN_NO $EXTRA_FLAGS
		EXTRA_FLAGS=" --EOS --iscosmic --cosmicutil "
		runAnalysis $RUN_NO "$EXTRA_FLAGS" $FORCE_RUN
		EXTRA_FLAGS=" --EOS "
		runAnalysis $RUN_NO "$EXTRA_FLAGS" $FORCE_RUN
		cd $RELEASE/alphaAnalysis/macros
		echo "#include \"$PLOTDUMPSVER+\"
		Int_t MenuMacro${RUN_NO}(Int_t runNumber=$RUN_NO){
		PrintDetectorQOD(${RUN_NO},\"Mixing\"); 
		}
		">MenuMacro$RUN_NO.C
		root -b -q -l MenuMacro$RUN_NO.C 
		exit 0
		;;
		esac
	;;
    --COSMIC )
		case `hostname` in
		alphavmcluster* | alphacrunch* | alphanibble* )
	
	echo "Yep, I am an alphacrunch node"
	FORCE_RUN=1
	cd ~/crunchAnalysis #Set which version of alphaAnalysis here!
	source myconfig.sh 
	cd $RELEASE
		echo "Cosmic run detected";
		FORCE_RUN=1  #Force re-analysis even if root files exist
		RUN_NO="$2"
		sleep 2
		FORCE_RUN=1
		NOTALK=1
		EXTRA_FLAGS=" --EOS "
		runStrips $RUN_NO $EXTRA_FLAGS
		EXTRA_FLAGS=" --EOS --iscosmic --cosmicutil "
		runAnalysis $RUN_NO "$EXTRA_FLAGS" $FORCE_RUN
		EXTRA_FLAGS=" --EOS --adcspecs "
		runAnalysis $RUN_NO "$EXTRA_FLAGS" $FORCE_RUN
		cd $RELEASE/alphaAnalysis/macros
		echo "#include \"$PLOTDUMPSVER+\"
		Int_t MenuMacro${RUN_NO}(Int_t runNumber=$RUN_NO){
		PrintDetectorQOD(${RUN_NO}); 
		}
		">MenuMacro$RUN_NO.C
		root -b -q -l MenuMacro$RUN_NO.C 
		exit 0
		;;
		esac
	;;
	*)
		# unknown option
	;;
esac
shift #This shift is needed

done
echo "TEST OUTPUTS:"#Test output 
echo "List file: $LISTFILE"
echo "Strip flags: $STRIPFLAGS"
echo "Alpha flags: $ALPHAFLAGS"
echo "Force Run: $FORCE_RUN"
echo "Skip Strips: $SKIPSTRIPS" 
echo "FRD ON?: $FRD"
echo "NOTALK?: $NOTALK"

if [[ $maxjobs -ge 2 ]]
then
	echo "MAXJOBS flags: $maxjobs"
else
	echo "Maxjobs not set... using the default for system..."
	case `hostname` in
	alphacrunch*  )
	maxjobs=8
	;;
	alphadaq*  )
	maxjobs=3
	;;
	alphacpc04* | alphacpc09* )
	maxjobs=2
	;;
	esac
	echo "MAXJOBS set to: $maxjobs"
fi

if [[ LXBATCH -eq 1 ]]
then
echo "LXBATCH JOB... please implement code to submit job"

	if [[ LISTRUN -eq 1 ]]
	then
		echo "I AM ALSO A LIST OF RUNS... IMPLEMENT A MODE FOR THIS"
		else
		echo "JUST 1 RUN... $RUN_NO"
		
	fi
fi
if [[ LISTRUN -eq 1 ]]
then
echo "I am a list run... preparing... "
RunListAnalysis
fi

if [[ CRUNCH -eq 1 ]]
then
	echo "I am a crunch run"
	NOTALK=1
	case `hostname` in
	alphacrunch* | alphanibble* )
	
	echo "Yep, I am an alphacrunch node"
	FORCE_RUN=1
	cd ~/crunchAnalysis #Set which version of alphaAnalysis here!
	source myconfig.sh 
	cd $RELEASE
#	echo "Self updating..."
#	SVNSTATUS=`svn up` # comment this out if you want to run a non standard version of the analysis
#	if [[ $SVNSTATUS == "*At revision*" ]]
#		then 
#		echo "alphaSoftware up to date"
#		else
#		cd $RELEASE/aux
#		rm main.db
#		sqlite3 main.db < settings.sql
#		cd $RELEASE/alphavmc
#		#make clean
#		make
#		cd $RELEASE/alphaAnalysis/lib
#		#make clean
#		make
#		cd $RELEASE/alphaAnalysis
#		#make clean
#		make
#		cd $RELEASE/alphaStrips
#		#make clean
#		make
#		echo "Self update done"
#	fi
	echo "Strip files location: $STRIPFILES"
	runStrips $RUN_NO "$STRIPFLAGS"
	cd alphaAnalysis
	echo "Tree files location: $TREEFILES"
	runAnalysis $RUN_NO "$ALPHAFLAGS" $FORCE_RUN
	echo "analysis done... cleaning up midas files"
	rm -v $RELEASE/alphaAnalysis/midasdata/run${RUN_NO}sub0*.mid.gz
	rm -v $RELEASE/alphaStrips/midasdata/run${RUN_NO}sub0*.mid.gz
	;;
	esac
fi

if [[ QUENCH -eq 1 ]]
then
echo "I am a quench run... "
	SaveMixingAndQuench
	runStrips $RUN_NO "$STRIPLAGS"
	runAnalysis $RUN_NO "$ALPHAFLAGS" $FORCE_RUN
	cd macros
	RunQuenchOrFRDMacro
fi

if [[ FRD -eq 1 ]]
then
	SaveMixingAndFRD
	runStrips $RUN_NO $EXTRA_FLAGS
	runAnalysis $RUN_NO "$ALPHAFLAGS" $FORCE_RUN
	cd macros
	RunQuenchOrFRDMacro
fi

echo " "
exit 0
}

RunQuenchOrFRDMacro()
{		echo "#include \"$PLOTDUMPSVER\"
		Int_t MenuMacro${RUN_NO}(Int_t runNumber=$RUN_NO){
		TTimeStamp *t1 = new TTimeStamp();
		const unsigned int date = t1->GetDate();
		//Announce the quench passed cuts:	
			
		Int_t first_VF48;
		Int_t last_VF48;
		ReturnQuenchVF48Numbers( runNumber, -1,  first_VF48, last_VF48, $NOTALK);
		
		$PLOTVERTEX
		
		if ( $ELOG ) // if upload to elog
		{
		
			cout << \"uploading the following to the elog: \" << endl;
			cout << MixingName << endl;
			TString Upload=\"ssh -x alphacpc05 ~/joseph/elog_test/elog-3.1.1/elog -h localhost -p 8080 -l test -f \";
			Upload+=\"${RELEASE}/alphaAnalysis/macros/\";
			Upload+=MixingName;
			
			cout << QuenchName << endl;
			Upload+=\" -f \";
			Upload+=\"${RELEASE}/alphaAnalysis/macros/\";
			Upload+=QuenchName;
			

			cout << Upload << endl;
			for (Int_t i=first_VF48; i<=last_VF48; i++)
			{
				Upload+=\" -f \";
				Upload+=\"${RELEASE}/alphaAnalysis/plots/R${RUN_NO}_E\";
				Upload+=i;
				Upload+=\".png \";
			}
			
			Upload+=\" -e 249 -v -x\";
			gSystem->Exec(Upload);
			cout << Upload << endl;
			
			PrintQuenchGateWindow($RUN_NO,-1); //Print to terminal again... there has been allot of junk put in there...
		}
		
		return 0;
		}
		">MenuMacro$RUN_NO.C
	root -b -q MenuMacro$RUN_NO.C
}

RunListAnalysisLXBATCH()
{
	echo "!@£$"
#	EXTRAFLAGS=$STRIPFLAGS
#	runStrips
#	EXTRAFLAGS=$ALPHAFLAGS
#	runAnalysis
	
}

RunListAnalysis()
{

echo "LIST FILE $LISTFILE"
echo "EXTRA FLAGS FOR STRIPS $STRIPFLAGS"
echo "EXTRA FLAGS FOR ALPHAANALYSIS $ALPHAFLAGS"
echo "FORCING RUN IF FILE EXISTS $FORCE_RUN"


NUMOFLINES=`cat $LISTFILE | wc -l` #$(wc -l < "$LISTFILE")
JobCount=0

while IFS='' read -r RUNNO || [[ -n ${RUNNO} ]]; do
	if [ $maxjobs -gt 1 ]
	then
		jobcnt=(`jobs -p`)
		#echo "Jobcount ${jobcnt[@]}"
		if [ ${#jobcnt[@]} -lt $maxjobs ] ; then
			echo "Starting Job with ${RUNNO} "
			JobCount=$((JobCount+1))
			print_progress $JobCount $NUMOFLINES
			do_analysis_locally  "${RUNNO}" "$STRIPFLAGS" "$ALPHAFLAGS" "$FORCE_RUN" &
		else
			echo "waiting"
			while [ ${#jobcnt[@]} -ge $maxjobs ]
			do
				sleep 30
				echo "$maxjobs jobs running... sleeping 30s"
				jobcnt=(`jobs -p`)
			done
			echo "Running strips $RUNNO "
			JobCount=$((JobCount+1))
			print_progress $JobCount $NUMOFLINES
			do_analysis_locally  "${RUNNO}" "$STRIPFLAGS" "$ALPHAFLAGS" "$FORCE_RUN" &
		fi
    else
		do_analysis_locally  "${RUNNO}" "$STRIPFLAGS" "$ALPHAFLAGS" "$FORCE_RUN"
    fi
done < "$LISTFILE"

}

do_analysis_locally()
{
	local RUN_NO=$1
	local STRIPFLAGS=$2
	local ALPHAFLAGS=$3
	local FORCE_RUN=$4
	runStrips $RUN_NO "$STRIPFLAGS" "$FORCE_RUN"
	runAnalysis $RUN_NO "$ALPHAFLAGS" "$FORCE_RUN"
}
function print_progress ()
{
Job=$1
Total=$2
echo " "
echo "Job $Job of $Total at `date` "

# Process data
    let _progress=(${1}*100/${2}*100)/100
    let _done=(${_progress}*4)/10
    let _left=40-$_done
# Build progressbar string lengths
    _fill=$(printf "%${_done}s")
    _empty=$(printf "%${_left}s")
# 1.2 Build progressbar strings and print the ProgressBar line
# 1.2.1 Output example:
# 1.2.1.1 Progress : [########################################] 100%
printf "\rProgress : [${_fill// /#}${_empty// /-}] ${_progress}%%"
echo ""
}


SetListFlagsMenu()
{
	echo "
################################################################################
			MENU: Please input number below:
################################################################################

1. Cosmic analysis - full: --iscosmic --cosmicutils
2. Cosmic analysis - part of alphaStrips and part of alphaAnalysis 
	(first 10k events for alphaStrips and 10k events for alphaAnalysis)
3. Cosmic analysis - part of alphaStrips and part of alphaAnalysis 
	(first 10k events for alphaStrips and 50k events for alphaAnalysis)
4. Cosmic analysis - part of alphaStrips (first 10k events for alphaStrips)
5. ADC specs
6. Custom"
read MENUVAR
case $MENUVAR in
1  )
	ALPHAFLAGS=" --iscosmic --cosmicutil " ;;
2  )  
	STRIPFLAGS=" -e10000 "
	ALPHAFLAGS=" -e10000 --iscosmic --cosmicutil  " ;;
3  )  
	STRIPFLAGS=" -e10000 "
	ALPHAFLAGS=" -e50000 --iscosmic --cosmicutil " ;;
4  )
	STRIPFLAGS=" -e10000 "
	ALPHAFLAGS=" --iscosmic --cosmicutil " ;;
5  )
	ALPHAFLAGS=" --adcspecs " ;;
6  ) 
	echo "Input flags for alphaStrips:"
	read STRIPFLAGS
	STRIPFLAGS=" $STRIPFLAGS " #adding space before and after
	echo "Input flags for alphaAnalysis:"
	read ALPHAFLAGS 
	ALPHAFLAGS=" $ALPHAFLAGS " #adding space before and after 
	;;
esac
}

ListMenu()
{
	export STRIPFLAGS=" "
	export ALPHAFLAGS=" "
	echo "=============================================================================="
	echo "Force Running analysis even if tree exists? (Y/N)"
	echo "=============================================================================="
	read -n 1 YN
	case $YN in
	Y | y )
		echo "I will over write any root tress... ok"
		FORCE_RUN=1
	;;
	N | n )
		echo "Old trees will not be over written"
	;;
	*  )
		echo "Input not understood, I will re-run all analysis and overwrite existing files..."
	;;
	esac
	echo "=============================================================================="
	echo "Do you want to run to use additional flags for analysis? (Y/N)"
	echo "=============================================================================="
	read -n 1 YN
	echo ""
	case $YN in
	Y | y )
		echo "YES"
		SetListFlagsMenu
	;;
	N | n )
		echo "NO"
	;;
	* )
		echo "Bad option... press y or n... start again"
	exit 0
	;;
	esac
	echo "=============================================================================="
	echo "How many jobs do you want to run in parrellel ( must be greater or equal to 2)"
	echo "=============================================================================="
	read maxjobs
	if [[ $maxjobs -lt 1 ]]
	then
		echo "Max jobs number is too low... start again"
	exit 0
	fi
	
	#run_some_analysis.sh --batch --list $LISTFILE --STRIPFLAGS $STRIPFLAGS --ALHPAFLAGS $ALPHAFLAGS --MAXJOBS $maxjobs
	
echo " 
"
RunListAnalysis
}

getstartstop () 
{
echo "Please enter in a start time"
read USER_START
echo "Please enter in a stop time (Set -1 if you want 'end of run'"
read USER_STOP
echo "Assuming you want cuts :D "
}

reportStripsDone()
{
	if [ $NOTALK -eq 0 ]
	then
	case  `hostname` in
	alphadaq* | alphacpc* )
		ssh -x alphadaq "echo 'alpha Strips done ' | ~/online/bin/google_speech/speech_google.py"
	;;
	esac
	fi
}

reportAlphaAnalysisDone()
{
	if [ $NOTALK -eq 0 ]
	then
	case  `hostname` in
	alphadaq* | alphacpc* )
		ssh -x alphadaq "echo 'alpha Analysis done ' | ~/online/bin/google_speech/speech_google.py"
	;;
	esac
	fi
}


reportCosmicAnalysisDone()
{
	if [ $NOTALK -eq 0 ]
	then
	case  `hostname` in
	alphadaq* | alphacpc* )
		ssh -x alphadaq "echo 'Cosmic Analysis done ' | ~/online/bin/google_speech/speech_google.py"
	;;
	esac
	fi
}

fetchMidasFiles()
{
	if [[ CRUNCH -eq 0 ]]
	then #I am not in crunchmode: (A batch mode designed to fetch data one by one on the fly inside alphaAnalysis, a mode to save disk space)
	local RUN_NO="$1"
	echo "RUN $RUN_NO"
	echo "Fetching..." #remember to have a valid kerbos ticket!
	midasFile="midasdata/run${RUN_NO}sub00000.mid.gz"
	if [ -e $midasFile ]
	then
		echo "midas file found"
	else
		if [ -d "midasdata" ] 
		then
			case `hostname` in
			alphacrunch*  )
			#cat /afs/cern.ch/project/eos/installation/client/etc/setup.sh
			NUM_OF_FILES=`/afs/cern.ch/project/eos/installation/0.3.84-aquamarine/bin/eos.select ls /eos/experiment/alpha/midasdata/run${RUN_NO}sub*.mid.gz | wc -l `
			if [ $NUM_OF_FILES -eq 1 ]
			then 
				/afs/cern.ch/project/eos/installation/0.3.84-aquamarine/bin/eos.select cp /eos/experiment/alpha/midasdata/run${RUN_NO}sub00000.mid.gz midasdata/
			else 
				if  [ $NUM_OF_FILES -lt 10 ]
				then
					for i in `seq 0 9`;
					do
						/afs/cern.ch/project/eos/installation/0.3.84-aquamarine/bin/eos.select cp /eos/experiment/alpha/midasdata/run${RUN_NO}sub0000${i}.mid.gz midasdata/
					done
					else 
					if [ $NUM_OF_FILES -lt 100 ]
					then
						for i in `seq 0 9`;
						do
							/afs/cern.ch/project/eos/installation/0.3.84-aquamarine/bin/eos.select cp /eos/experiment/alpha/midasdata/run${RUN_NO}sub0000${i}.mid.gz midasdata/
						done
						for i in `seq 10 99`;
						do
							/afs/cern.ch/project/eos/installation/0.3.84-aquamarine/bin/eos.select cp /eos/experiment/alpha/midasdata/run${RUN_NO}sub000${i}.mid.gz midasdata/
						done
					fi
				fi
			fi
			
			;;
			alphadaq* | alphacpc04* | alphacpc09* )
				echo "Midas file not found. Check for error, or use an offline analysis machine"
			;;
			esac
		else
			echo "midasdata folder not found... are you in the right folder? I am currently in $PWD"
		fi
		
	fi	
	fi
}

runStrips() 
{
	local RUN_NO=$1
	local EXTRA_FLAGS=$2
	checkIfCosmicTriggerIsOff $RUN_NO
echo "Running alphaStrips.."
echo "RUN_NO: $RUN_NO"
echo "FLAGS: $EXTRA_FLAGS"
echo "FORCED TO RUN: $FORCE_RUN"
fileStrip=$RELEASE'/alphaStrips/data/alphaStrips'$RUN_NO'offline.root'
cd $RELEASE/alphaStrips
pwd
if [ $FORCE_RUN -eq 0 ]
then
	#cd $RELEASE/alphaStrips
	if [ -e $fileStrip ]
	then
		echo "alphaStrips already run and found... skipping"
	else
		for (( run=$RUN_NO; run> $(($RUN_NO-100)); run--))
		do
			fileStrip=$STRIPFILES'/alphaStrips'$run'offline.root'
			#echo $run
			if [ -e $fileStrip ]
			then
				echo "Found:   $fileStrip"
				break
			else
				echo "No file: $fileStrip"
			fi
		done
		echo $fileStrip
		now=$(date +%s)
		limit=$(( $now - 32400))  # 9 hours ago
		ftime=`date -r $fileStrip +%s`
		echo $ftime
		echo $now
		if [[ $ftime -lt $limit ]] ; then
			echo "File too old (more than 9 hours) NB: seach limited to 100 runs..."
			echo " alphaStrips is going to be executed "
			sleep 1
			echo "************************************************"     
			echo "alphaStrips is being executed, please wait....:D"
			echo "************************************************"  
			fetchMidasFiles "$RUN_NO"
			./alphaStrips.exe ${EXTRA_FLAGS} midasdata/run${RUN_NO}sub00000.mid.gz
			reportStripsDone
		fi
		echo "done"
		
		cd ..
	fi
else
	if [ $FORCE_RUN -eq 1 ]
	then
		fetchMidasFiles "$RUN_NO"
		echo "./alphaStrips.exe ${EXTRA_FLAGS} midasdata/run${RUN_NO}sub00000.mid.gz" #  > alphaStrips.log
		./alphaStrips.exe ${EXTRA_FLAGS} midasdata/run${RUN_NO}sub00000.mid.gz
		echo "done"
		reportStripsDone
		cd ..
	fi
fi


}

checkIfCosmicTriggerIsOff()
{
	local RUN_NO=$1
	echo "Run number: $RUN_NO"
ans=`grep '<key name="Cosmic_Run" type="BOOL">' $RELEASE/alphaAnalysis/midasdata/run$RUN_NO.xml`
 
if [[ $ans == *'<key name="Cosmic_Run" type="BOOL">y</key>'* ]]
then
	echo "'Cosmic_Run' toggle turned on in midas... bad"
	case  `hostname` in
	alphadaq* | alphacpc* )
		ssh -x alphadaq "echo 'Warning: Analysis found Cosmic Run toggle on ' | ~/online/bin/google_speech/speech_google.py"
	;;
	esac
else if [[ $ans == *'<key name="Cosmic_Run" type="BOOL">n</key>'* ]]
then
	echo "'Cosmic_Run' trigger not turned on in midas... Good. It shouldn't be used"
else 
	echo "Unknown Cosmic toggle status"
fi
fi
}
runAnalysis()
{
	local RUN_NO=$1
	local EXTRA_FLAGS=$2
	local FORCE_RUN=$3
	echo "RUN_NO: $RUN_NO"
	echo "FLAGS: $EXTRA_FLAGS"
	echo "FORCED TO RUN: $FORCE_RUN"
	checkIfCosmicTriggerIsOff $RUN_NO

cd $RELEASE/alphaAnalysis
if [ $FORCE_RUN -eq 0 ]
then
	fileData=$TREEFILES'/tree'$RUN_NO'offline.root'
	filesize=$(wc -c <"$fileData")
	if [ -e $fileData ]
	then
		if [ $filesize -ge 50000000 ]
		then
			echo "file size is larger than 50Mb... skipping analysis"
			sleep 1
		else
			echo "Running alphaAnalysis..."
			fetchMidasFiles "$RUN_NO"
			
			echo "./alphaAnalysis.exe ${EXTRA_FLAGS} midasdata/run${RUN_NO}sub00000.mid.gz"
			./alphaAnalysis.exe ${EXTRA_FLAGS} midasdata/run${RUN_NO}sub00000.mid.gz 
			
			echo "done"
			reportAlphaAnalysisDone
		fi
	else
		echo "Root files not found..."
		echo "Running alphaAnalysis..."
		fetchMidasFiles "$RUN_NO"
		echo "./alphaAnalysis.exe ${EXTRA_FLAGS} midasdata/run${RUN_NO}sub00000.mid.gz"
	
		./alphaAnalysis.exe ${EXTRA_FLAGS} midasdata/run${RUN_NO}sub00000.mid.gz 
		
		echo "done"
		reportAlphaAnalysisDone
	fi
else
	echo "Running alphaAnalysis..."
	fetchMidasFiles "$RUN_NO"
	echo "./alphaAnalysis.exe ${EXTRA_FLAGS} midasdata/run${RUN_NO}sub00000.mid.gz"
	./alphaAnalysis.exe ${EXTRA_FLAGS} midasdata/run${RUN_NO}sub00000.mid.gz 	
	echo "done"
	reportAlphaAnalysisDone
fi

}

runNoRecoAnalysis ()
{
fileData=$RELEASE'/alphaAnalysis/data/tree'$RUN_NO'offline.root'
cd $RELEASE/alphaAnalysis/

if [ $FORCE_RUN -eq 0 ]
then
	if [ -e $fileData ]
	then
	
		echo "alphaAnalysis file already run and found... "
		filesize=$(wc -c <"$fileData")
		if [ $filesize -ge 40000 ] 
		then
			echo "file size is larger than 40000... skipping analysis"
			sleep 1
		else
			echo "file size is too small! Running analysis again..."
			echo "./alphaAnalysis.exe --recoff midasdata/run${RUN_NO}sub00000.mid.gz"
			./alphaAnalysis.exe --recoff midasdata/run${RUN_NO}sub00000.mid.gz  # > alphaAnalysis.log
			echo "done"
		fi
		
	else
		echo "Running alphaAnalysis... with no reconstruction to speed things up"
		echo "./alphaAnalysis.exe --recoff midasdata/run${RUN_NO}sub00000.mid.gz"
		./alphaAnalysis.exe --recoff midasdata/run${RUN_NO}sub00000.mid.gz  # > alphaAnalysis.log
		echo "done"
	fi
else
	echo "./alphaAnalysis.exe --recoff midasdata/run${RUN_NO}sub00000.mid.gz"
	
	./alphaAnalysis.exe --recoff midasdata/run${RUN_NO}sub00000.mid.gz  # > alphaAnalysis.log
	echo "done"

fi

}
MakotoMacro()
{

cd $RELEASE/alphaAnalysis/macros/
echo "Running special root macro"
echo "#include \"$PLOTDUMPSVER\"
		Int_t MenuMacro${RUN_NO}(Int_t runNumber=$RUN_NO){
		
		TCanvas* cSpecialDumps = new TCanvas(\"Title\",\"Description\",1200,800);
  
  
		cSpecialDumps->Divide(3,2);
		gStyle->SetOptStat(\"ni\");
		Double_t sstart = MatchEventToTime(runNumber,\"startDump\",\"Cold\");
		Double_t sstop = MatchEventToTime(runNumber,\"stopDump\",\"Cold\");
		char* description=\"Cold\";
		
		cSpecialDumps->cd(1);
		gh=PlotSIScounts($RUN_NO, 4, sstart, sstop);
		char htitle[501];
		snprintf(htitle, 500, \"%s - %s\",gh->GetTitle(), description);
		gh->SetTitle(htitle);
  		gh->Draw(\"E1 HIST\");
		

		cSpecialDumps->cd(2);
		gh=PlotSIScounts($RUN_NO, 7, sstart, sstop);
		char htitle[501];
		snprintf(htitle, 500, \"%s - %s\",gh->GetTitle(), description);
		gh->SetTitle(htitle);
  		gh->Draw(\"E1 HIST\");
		

		cSpecialDumps->cd(3);
		gh=PlotSIScounts($RUN_NO, 42, sstart, sstop);
		char htitle[501];
		snprintf(htitle, 500, \"%s - %s\",gh->GetTitle(), description);
		gh->SetTitle(htitle);
  		gh->Draw(\"E1 HIST\");
		
		
		Double_t sstart = MatchEventToTime(runNumber,\"startDump\",\"RCT Hot Dump\");
		Double_t sstop = MatchEventToTime(runNumber,\"stopDump\",\"RCT Hot Dump\");	
		char* description=\"RCT Hot\";
		
		cSpecialDumps->cd(4);
		gh=PlotSIScounts($RUN_NO, 4, sstart, sstop);
		char htitle[501];
		snprintf(htitle, 500, \"%s - %s\",gh->GetTitle(), description);
		gh->SetTitle(htitle);
  		gh->Draw(\"E1 HIST\");
		

		cSpecialDumps->cd(5);
		gh=PlotSIScounts($RUN_NO, 7, sstart, sstop);
		char htitle[501];
		snprintf(htitle, 500, \"%s - %s\",gh->GetTitle(), description);
		gh->SetTitle(htitle);
  		gh->Draw(\"E1 HIST\");

		
		cSpecialDumps->cd(6);
		gh=PlotSIScounts($RUN_NO, 42, sstart, sstop);
		char htitle[501];
		snprintf(htitle, 500, \"%s - %s\",gh->GetTitle(), description);
		gh->SetTitle(htitle);
  		gh->Draw(\"E1 HIST\");

		
	}
">MenuMacro$RUN_NO.C
		root -l MenuMacro$RUN_NO.C
}

CatchANDLifetime2014() #RETIRED
{
	cd $RELEASE/alphaAnalysis/macros/
	echo "#include \"$PLOTDUMPSVER\"
		Int_t MenuMacro${RUN_NO}(Int_t runNumber=$RUN_NO){
		Double_t sstart = MatchEventToTime(runNumber,\"stopDump\",\"Hot Dump\");
  		Double_t sstop = MatchEventToTime(runNumber,\"startDump\",\"Cold Dump\");
		cout<<\"Start\" << sstart << \"Stop \" << sstop <<endl;
//		Plot_CATCH_AND($RUN_NO,sstart,sstop);
		SetBinNumber(100);
		PlotPbarLifetimeL(runNumber);
	}
	">MenuMacro$RUN_NO.C
	root -l MenuMacro$RUN_NO.C
}
CatchORLifetime2016()
{
	cd $RELEASE/alphaAnalysis/macros/
	echo "#include \"$PLOTDUMPSVER\"
		Int_t MenuMacro${RUN_NO}(Int_t runNumber=$RUN_NO){
		gc=Plot_CATCH_Lifetime_With_ADVeto(runNumber);
		SaveCanvas(runNumber,\"-CatchOR_lifetime_with_ADveto\");
	}
	">MenuMacro$RUN_NO.C
	root -l MenuMacro$RUN_NO.C
}

AtomANDLifetime2014() #RETIRED
{
	cd $RELEASE/alphaAnalysis/macros/
	echo "#include \"$PLOTDUMPSVER\"
	Int_t MenuMacro${RUN_NO}(Int_t runNumber=$RUN_NO){
		Double_t sstart = MatchEventToTime(runNumber,\"stopDump\",\"Hot Dump\");
  		Double_t sstop = MatchEventToTime(runNumber,\"startDump\",\"Cold Dump\");
		cout<<\"Start \" << sstart << \"Stop \" << sstop <<endl;
		Plot_ATOM_AND($RUN_NO,sstart,sstop);
		SetBinNumber(100);
		PlotPbarLifetime();
	}
	">MenuMacro$RUN_NO.C
	root -l MenuMacro$RUN_NO.C
}
PlotLifetimeGeneral()
{

	cd $RELEASE/alphaAnalysis/macros/
	echo "#include \"$PLOTDUMPSVER\"
	Int_t MenuMacro${RUN_NO}(Int_t runNumber=$RUN_NO){
		Double_t sstart = MatchEventToTime(runNumber,\"startDump\",\"$LIFEDUMP\");
  		Double_t sstop = MatchEventToTime(runNumber,\"stopDump\",\"$LIFEDUMP\");
		cout<<\"Start \" << sstart << \"  Stop \" << sstop <<endl;
		gc=Plot${CATCH_ATOM}${AND_OR}($RUN_NO,sstart,sstop);
		SaveCanvas($RUN_NO,\"Lifetime\");
		SetBinNumber(100);
		PlotPbarLifetime();
		SaveCanvas($RUN_NO,\"Lifetime-afterFit\");
		cout<<\"If the fit for the lifetime is bad (ie thrown off be injections, use the command: PlotPbarLifetime(StartTime,StopTime) to refit...\" << endl;
	}
	">MenuMacro$RUN_NO.C
	root -l MenuMacro$RUN_NO.C
}

CatchANDLifetime2015()
{
	AND_OR="AND"
	CATCH_ATOM="_CATCH_"
	PlotLifetimeGeneral
}

CatchORLifetime2015()
{
	AND_OR="OR"
	CATCH_ATOM="_CATCH_"
	PlotLifetimeGeneral
}
AtomANDLifetime2015()
{
	AND_OR="AND"
	CATCH_ATOM="_ATOM_"
	PlotLifetimeGeneral
}
AtomORLifetime2015()
{
	AND_OR="OR"
	CATCH_ATOM="_ATOM_"
	PlotLifetimeGeneral
}	
	
SiLifetime()
{
CATCH_ATOM=PbarLifetimeVert
PlotLifetimeGeneral
#	cd macros
#	echo "#include \"$PLOTDUMPSVER\"
#	Int_t MenuMacro${RUN_NO}(Int_t runNumber=$RUN_NO){
#		Double_t sstart = MatchEventToTime(runNumber,\"startDump\",\"Hold Dump\");
#		Double_t sstop = MatchEventToTime(runNumber,\"stopDump\",\"Hold Dump\");
#		cout<<\"Start\" << sstart << \" Stop \" << sstop <<endl;
#		Plot_Si_PASS($RUN_NO,sstart,sstop);
#		SetBinNumber(100);
#		PlotPbarLifetime();
#	}
#	">MenuMacro$RUN_NO.C
#	root -l MenuMacro$RUN_NO.C
}

PrintInfo()
{

echo ""
echo "
This is a 'Master' script for running analysis on antiprotons. This
has been created in order to avoid flooding working directories with
different scripts for different tasks in the analysis. This is not
designed for batch processing, rather for a fool proof access to
analysis during shift work.

Options are available for plotting PMT counts, tempurature measurements
and the Silicon Vertex detector verticies and lifetime measurements.
This bash script has been designed to be a little more user friendly
for those not experienced with root, operated by menus. If you want to
see more functionality, or report a bug, please contact Joseph.

***Detector experts should be familiar with the manual methods of
running analysis and know how to use PlotDumps.C***

This script handles running alphaStrips, alphaAnalysis and root as
required by each test. To avoid clutter, each run you analyse, will make
a macro in the directory:
~alphaSoftware2012/alphaAnalysis/macros
with the file name \"MenuMacro$RUN_NO.C\". For those interested in learning
root and how the analysis works, you may want to study this file
for each analysis... or read the code in this file.
(NOTE: The \"MenuMacro$RUN_NO.C\" file will get over written every time you run
this script)

Hopefully the menu choices are self explanatory, each script will end
with a root figure you can save to png for the elog :) The or there will
be in terminal instructions for the next step in how to apply a fit.

Have fun!

Functionality will be added upon requested.

Cheers

Joe
28/07/2015


"
}


SaveMixing()
{
	DDUMP="Mixing"
		PLOTVERTEX="
		TCanvas* canvas = new TCanvas(\"cVTX\",\"cVTX\",1600,800);
		//canvas = PlotVertices(runNumber,\"$DDUMP\");
		
		canvas =  PlotMixingFlagVertices(runNumber);
		TString savFolder=MakeAutoPlotsFolder();
		//(\"AutoPlots/\");
		//savFolder+=date; 
		//gSystem->mkdir(savFolder);
		
		
		TString MixingName(savFolder);
		MixingName+=(\"/${DDUMP}VerticesRun\");
		MixingName+=${RUN_NO};
		MixingName+=(\".png\");
		
		canvas->SaveAs(MixingName);
		PrintAnalysisReport($RUN_NO);
		cout << \"Plots saved to \" << savFolder << endl;"
		
}
SaveQuench()
{
		DDUMP="Quench"
		PLOTVERTEX=" $PLOTVERTEX
		
		TCanvas* canvas2 = new TCanvas(\"cVTX\",\"cVTX\",1600,800);
		//canvas2 = PlotVertices(runNumber,\"$DDUMP\");
		canvas =  PlotQuenchFlagVertices(runNumber);
		
		TString QuenchName(savFolder);
		QuenchName+=(\"/${DDUMP}VerticesRun\");
		QuenchName+=${RUN_NO};
		QuenchName+=(\".png\");
		
		canvas2->SaveAs(QuenchName);
		
		SaveQuenchVF48Events($RUN_NO);
		cout << \"Plots saved to \" << savFolder << \"... Now paste the following into the elog: \\n\\n\\n\\n\" << endl;
		PrintAnalysisReport($RUN_NO);
		PrintQuenchGateWindow($RUN_NO);
		"
}
Save243andFRD()
{
	
	
		DDUMP="FastRampDown"
		PLOTVERTEX=" $PLOTVERTEX
		
		
		TCanvas* canvas2 = new TCanvas(\"cVTX\",\"cVTX\",1600,800);
		//canvas2 = PlotVertices(runNumber,\"$DDUMP\");
		
		canvas2 =  PlotQuenchFlagVertices(runNumber);
		TString FRDName(savFolder);
		FRDName+=(\"/${DDUMP}VerticesRun\");
		FRDName+=${RUN_NO};
		FRDName+=(\".png\");
		canvas2->SaveAs(FRDName);
		
		
		cout << \"DD dump summary: \" << endl;
		
		TCanvas* canvasDD =PlotLaserVerticesMultiDump_SeqPulseTiming(${RUN_NO},\"DD\",-0.006,0.004);
		
		TString DDName(savFolder);
		DDName+=(\"/DD_Pulse_VerticesRun\");
		DDName+=${RUN_NO};
		DDName+=(\".png\");
		canvasDD->SaveAs(DDName);
		
		cout << \"CC dump summary: \" << endl;
		
		TCanvas* canvasCC = PlotLaserVerticesMultiDump_SeqPulseTiming(${RUN_NO},\"CC\",-0.006,0.004);

		
		TString CCName(savFolder);
		CCName+=(\"/CC_Pulse_VerticesRun\");
		CCName+=${RUN_NO};
		CCName+=(\".png\");
		canvasCC->SaveAs(CCName);
		
		TCanvas* canvas3= new TCanvas;
		canvas3= Plot243Power(${RUN_NO});
		TString Transmission(savFolder);
		Transmission+=(\"R${RUN_NO}_243Transmission\");
		Transmission+=(\".png\");
		canvas3->SaveAs(Transmission);
		
		
		TCanvas *canvasDDfreq= Plot243frequencyInDump(${RUN_NO},\"DD\",1,0);
		TString DDfreq(savFolder);
		DDfreq+=(\"R${RUN_NO}_d-d_frequency\");
		DDfreq+=(\".png\");
		canvasDDfreq->SaveAs(DDfreq);
		
		TCanvas *canvasCCfreq= Plot243frequencyInDump(${RUN_NO},\"CC\",1,0);
		TString CCfreq(savFolder);
		CCfreq+=(\"R${RUN_NO}_c-c_frequency\");
		CCfreq+=(\".png\");
		canvasCCfreq->SaveAs(CCfreq);
		
		 
		TCanvas *canvasBMON= PlotBMON(${RUN_NO});
		TString BMON(savFolder);
		BMON+=(\"R${RUN_NO}_c-c_frequency\");
		BMON+=(\".png\");
		canvasCCfreq->SaveAs(BMON);
		
		
		
		
		SaveQuenchVF48Events($RUN_NO,-1,kTRUE);
		
		cout << \"Plots saved to \" << savFolder << \"... Now paste the following into the elog: \\n\\n\\n\\n\" << endl;
		PrintAnalysisReport($RUN_NO);
		PrintLaserVerticesMultiDump_SeqPulseTiming(runNumber,\"DD\",-0.006,0.004);
		PrintLaserVerticesMultiDump_SeqPulseTiming(runNumber,\"CC\",-0.006,0.004);
		cout<<\"\n\n*******************************\" << endl;
		cout<<\"|   FRD Dump window summary   |\"<<endl;
		cout<<\"*******************************\" << endl;
		PrintQuenchGateWindow($RUN_NO,-1);
		"
		
	
}
SaveFRD()
{
		DDUMP="FastRampDown"
		PLOTVERTEX=" $PLOTVERTEX
		
		TCanvas* canvas2 = new TCanvas(\"cVTX\",\"cVTX\",1600,800);
		//canvas2 = PlotVertices(runNumber,\"$DDUMP\");
		canvas2 =  PlotQuenchFlagVertices(runNumber);
		TString FRDName(savFolder);
		FRDName+=(\"/${DDUMP}VerticesRun\");
		FRDName+=${RUN_NO};
		FRDName+=(\".png\");
		
		canvas2->SaveAs(FRDName);
		
		SaveQuenchVF48Events($RUN_NO,-1,kTRUE);
		cout << \"Plots saved to \" << savFolder << \"... Now paste the following into the elog: \\n\\n\\n\\n\" << endl;
		PrintAnalysisReport($RUN_NO);
		PrintQuenchGateWindow($RUN_NO,-1);
		"
}

SaveMixingAndDDAndCCFRD()
{
	SaveMixing
	Save243andFRD
	#SaveFRD
	
}

SaveMixingAndFRD()
{
	SaveMixing
	SaveFRD
}

SaveMixingAndQuench()
{
	SaveMixing
	SaveQuench
}


LifeTimeMenu()
{
	clear
	FORCE_RUN=0
	CONTINUE=0
	while [ $CONTINUE -eq 0 ]
	do
	echo "



======================================================================"
	echo "Lifetime SUBMENU: Please input number below:"
	echo "======================================================================
	1. Catching Trap 'OR' with AD veto (measured between Lifetime dump) (2016 style)
	2. Catching Trap 'AND' (measured between Lifetime dump) (2016 style)
	3. Atom Trap 'AND' (measured between Lifetime dump) (2016 style)
	4. Silicon Detector (measured during 'Lifetime Dump')
	
	Semi manual:
	5. Catching Trap 'AND' (On the 'Hold dump') (2015 style)
	6. Catching Trap 'OR' (On the 'Hold dump') (2015 style)
	7. Atom Trap 'AND' (On the 'Hold dump') (2015 style)
	8. Atom Trap 'OR' (On the 'Hold dump') (2015 style)

	9. FORCE SKIP of alphaAnalysis and alphaStrips (faster, but advanced)
	10. FORCE RERUN STRIPS! (Try this if the code does not work :) )"
	read SUBMENUVAR
	CONTINUE=1
case $SUBMENUVAR in 
1 )		echo "Running Catching Trap 'OR' lifetime"
		LIFEDUMP="Lifetime"
		cd alphaAnalysis
		runNoRecoAnalysis
		CatchORLifetime2016;;
2 )		echo "Running Catching Trap 'AND' lifetime"
		LIFEDUMP="Lifetime"
		cd alphaAnalysis
		runNoRecoAnalysis
		CatchANDLifetime2014;;
3  )	echo "Running Atom Trap 'AND' lifetime"
		LIFEDUMP="Lifetime"
		cd alphaAnalysis
		runNoRecoAnalysis
		AtomANDLifetime2014;;
4  )	echo "Running Catching Si lifetime"
		LIFEDUMP="Lifetime"
		runStrips $RUN_NO
		cd alphaAnalysis
		runAnalysis $RUN_NO "$ALPHAFLAGS" $FORCE_RUN
		SiLifetime;;
5  )	echo "Running Catching Trap 'AND' lifetime on 'Hold dump'"
		runNoRecoAnalysis
		CatchANDLifetime2015;;
6  )	echo "Running Catching Trap 'OR' lifetime on 'Hold dump'"
		runNoRecoAnalysis
		CatchORLifetime2015;;
7  )	echo "Running Catching Trap 'AND' lifetime"
		runNoRecoAnalysis
		AtomANDLifetime2015;;
8  )	echo "Running Catching Trap 'AND' lifetime"
		runNoRecoAnalysis
		AtomANDLifetime2015;;
9  )	CONTINUE=0
		FORCE_RUN=-1
		echo "TOGGLEING FOR NO alphaStrips.exe or alphaAnalysis.exe.. I WILL TURN BACK TO NORMAL WHEN QUIT";;
10  )	CONTINUE=0
		FORCE_RUN=1
		echo "TOGGLEING ON FOR RERUN STRIPS... I WILL TURN OFF WHEN QUIT";;
	esac
	done
}

PbarVertexMenu()
{
	clear
	FORCE_RUN=0
	CONTINUE=0
	while [ $CONTINUE -eq 0 ]
	do
#	10. FORCE SKIP of alphaAnalysis and alphaStrips (faster, but advanced)
		echo "



======================================================================"
		echo "Pbar Verteixing - SUBMENU: Please input number below:"
		echo "======================================================================
	1. Hold Dump with cuts
	2. Hold Dump without cuts
	3. Mixing with cuts
	4. Quench with cuts
	5. 243 LASER CC and DD!!!
	6. Mixing AND FastRampDown (FRD)
	7. Custom Time Window
	8. Custom Dump
	9. Entire Run

	EXTRA:
	10. Force re-running of alphaAnalysis (Try this if the code does not work :) )
	11. Force re-running of alphaStrips and alphaAnalysis (Try this if the above doesn't work)
"
		read SUBMENUVAR
		CONTINUE=1
		
		if [ $SUBMENUVAR -eq 11 ]
		then
			CONTINUE=0
			FORCE_RUN=1
			echo "TOGGLEING ON FOR RERUN STRIPS... I WILL TURN OFF WHEN QUIT"
		fi
		if [ $SUBMENUVAR -eq 10 ]
		then
			CONTINUE=0
			FORCE_RUN=-1
			echo "TOGGLEING FOR RERUNNING ALPHAANALYSIS"
		fi
	done
	
	LASER=0
	DO_CUTS=1
	SAVE_VF48_EVENTS=0
	case $SUBMENUVAR in
	1  )DDUMP="Hold Dump"
		PLOTVERTEX="gc=PlotVertices(runNumber,\"$DDUMP\");
		cout << \"To save the plot in AutoPlots, enter: \"<< endl;
		cout << \"SaveCanvas(\" << runNumber << \",\\\"$DDUMP\\\")\" << endl; " ;;
	2  )DDUMP="Hold Dump"
		DO_CUTS=0
		PLOTVERTEX="gc=PlotVertices(runNumber,\"$DDUMP\");
		cout << \"To save the plot in AutoPlots, enter: \"<< endl;
		cout << \"SaveCanvas(\" << runNumber << \",\\\"$DDUMP\\\")\" << endl; " ;;
	3  )SaveMixing ;;
	4  )SaveQuench ;;
	5  )SaveMixingAndDDAndCCFRD	
	SAVE_VF48_EVENTS=1
	LASER=1;;
	6  )SaveMixingAndFRD
	SAVE_VF48_EVENTS=1;;
	7  )getstartstop
		PLOTVERTEX="gc=PlotVertices(runNumber,$USER_START,$USER_STOP);
		cout << \"To save the plot in AutoPlots, enter: \"<< endl;
		cout << \"SaveCanvas(\" << runNumber << \",\\\"timeWindow\\\")\" << endl; " ;;
	8  )echo "Please enter dump name"
		read DDUMP
		PLOTVERTEX="gc=PlotVertices(runNumber,\"$DDUMP\");
		cout << \"To save the plot in AutoPlots, enter: \"<< endl;
		cout << \"SaveCanvas(\" << runNumber << \",\\\"$DDUMP\\\")\" << endl; " ;;
	9  )USER_START="0."
		USER_STOP="-1"
		PLOTVERTEX="gc=PlotVertices(runNumber,$USER_START,$USER_STOP);
		cout << \"To save the plot in AutoPlots, enter: \"<< endl;
		cout << \"SaveCanvas(\" << runNumber << \",\\\"WholeRun\\\")\" << endl; " ;;
		
	esac
	runStrips $RUN_NO
	runAnalysis $RUN_NO "$ALPHAFLAGS" $FORCE_RUN
	cd macros
	if [ $SAVE_VF48_EVENTS -eq 1 ]
	then
		RunQuenchOrFRDMacro
	else
		if [ $DO_CUTS -eq 1 ]
		then
			echo "#include \"$PLOTDUMPSVER\"
			Int_t MenuMacro${RUN_NO}(Int_t runNumber=$RUN_NO){
			TTimeStamp *t1 = new TTimeStamp();
			const unsigned int date = t1->GetDate();

			$PLOTVERTEX
			
			return 0;
			}
			">MenuMacro$RUN_NO.C
		else
			echo "#include \"$PLOTDUMPSVER\"
			Int_t MenuMacro${RUN_NO}(Int_t runNumber=$RUN_NO){
			TTimeStamp *t1 = new TTimeStamp();
			t1->GetDate();
	
			gApplyCuts=0;
			$PLOTVERTEX
			return 0;
			}
			">MenuMacro$RUN_NO.C
		fi
	root -l MenuMacro$RUN_NO.C
	fi
}

PbarPMTMenu()
{
	clear
	ChooseBinning
	FORCE_RUN=0
	CONTINUE=0
	while [ $CONTINUE -eq 0 ]
	do
		echo "



======================================================================"
		echo "Pbar PMT plotting - SUBMENU: Please input number below:"
		echo "======================================================================
	1. Catching PMT OR
	2. Catching PMT AND
	3. Atom Trap PMT OR
	4. Atom Trap PMT AND
	5. PMT 9	
	6. PMT 10 and 11 (ATOM_STICK)
	EXTRA:
	8. FORCE SKIP of alphaAnalysis and alphaStrips (faster, but advanced)"
		read SUBMENUVAR
		CONTINUE=1
		if [ $SUBMENUVAR -eq 8 ]
		then
			CONTINUE=0
			FORCE_RUN=-1
			echo "TOGGLEING ON FOR RERUN STRIPS... I WILL TURN OFF WHEN QUIT"
		fi
	done
	
	case $SUBMENUVAR in
	1 ) PLOTPMT="Plot_CATCH_OR(runNumber,sstart,sstop);" ;;
	2 ) PLOTPMT="Plot_CATCH_AND(runNumber,sstart,sstop);" ;;
	3 ) PLOTPMT="Plot_ATOM_OR(runNumber,sstart,sstop);" ;;
	4 ) PLOTPMT="Plot_ATOM_AND(runNumber,sstart,sstop);" ;;
	5 ) PLOTPMT="Plot_PMT9(runNumber,sstart,sstop);" ;;
	6 ) PLOTPMT="Plot_ATOM_STICK(runNumber,sstart,sstop);" ;;
	esac
		echo "



======================================================================"
		echo "Pbar PMT plotting - MORE OPTIONS: Please input number below:"
		echo "======================================================================
	1. 'Hold' Dump (Typical for Catching Trap)
	2. 'Cold' Dump
	3. Custom Time Window
	4. Custom Dump Window
	5. Custom time window by Dumps- End of one 'Dump', start of another 'Dump'
	6. Entire run"
		read MOREOPTIONS
	case $SUBMENUVAR in
	1 ) DDUMP="Hold"
		START_STR="Double_t sstart = MatchEventToTime(runNumber,\"startDump\",\"$DDUMP\");"
		STOP_STR="Double_t sstop = MatchEventToTime(runNumber,\"stopDump\",\"$DDUMP\");"
	;;
	2 ) DDUMP="Cold Dump"
		START_STR="Double_t sstart = MatchEventToTime(runNumber,\"startDump\",\"$DDUMP\");"
		STOP_STR="Double_t sstop = MatchEventToTime(runNumber,\"stopDump\",\"$DDUMP\");"
	;;
	3 ) getstartstop
		START_STR="Double_t sstart = $USER_START;"
		STOP_STR="Double_t sstop =  $USER_STOP;"
	;;
	4 ) echo "Please enter dump name"
		read DDUMP
		START_STR="Double_t sstart = MatchEventToTime(runNumber,\"startDump\",\"$DDUMP\");"
		STOP_STR="Double_t sstop = MatchEventToTime(runNumber,\"stopDump\",\"$DDUMP\");"
	;;
	5 ) echo "Please enter first dump name, we will use the 'stopDump' time of this..."
		read USER_DUMP1
		echo "Please enter second dump name, we will use the 'startDump' time of this..."
		read USER_DUMP2
		START_STR="Double_t sstart = MatchEventToTime(runNumber,\"stopDump\",\"$USER_DUMP1\");"
		STOP_STR="Double_t sstop = MatchEventToTime(runNumber,\"startDump\",\"$USER_DUMP2\");"
	;;	
	6 ) START_STR="Double_t sstart = 0;"
		STOP_STR="Double_t sstop = -1;"
	;;
	esac
	runNoRecoAnalysis
	cd $RELEASE/alphaAnalysis/macros/
	echo "#include \"$PLOTDUMPSVER\"
	Int_t MenuMacro${RUN_NO}(Int_t runNumber=$RUN_NO){
	$START_STR
	$STOP_STR
	SetBinNumber($BINNING);
	$PLOTPMT
	return 0;
	}
	">MenuMacro$RUN_NO.C
	root -l MenuMacro$RUN_NO.C
}

RunPbarAutoTemp()
{
	cd $RELEASE/alphaAnalysis/macros
	echo "#include \"$PLOTDUMPSVER\"
	Int_t MenuMacro${RUN_NO}(Int_t runNumber=$RUN_NO){
	PlotEnergyDump($RUN_NO);
	}
	">MenuMacro$RUN_NO.C
	root -l MenuMacro$RUN_NO.C
}

RunPbarTemp()
{
	cd $RELEASE/alphaAnalysis/macros
	echo "#include \"$PLOTDUMPSVER\"
	Int_t MenuMacro${RUN_NO}(Int_t runNumber=$RUN_NO){
	PlotEnergyDump($RUN_NO,\"$DUMP_FILE\",\"$DDUMP\",$SIS,1000$REPETITION);
	cout << endl << endl;
	cout << \"Plotting command used:\" << endl << \"PlotEnergyDump($RUN_NO,\\\"$DUMP_FILE\\\",\\\"$DDUMP\\\",$SIS,1000$REPETITION);\" << endl<< endl;
	cout<<\" Attempting first guess fit of energy dump: \" << endl;
	cout<<\"$FIRSTGUESS\" << endl;
	$FIRSTGUESS
	cout<<\" \"<<endl;
	cout<<\"=============================================\"<<endl;
	cout<<\"#  More instructions:\"<<endl;
	cout<<\"=============================================\"<<endl;
	cout<<\" \"<<endl;
	cout<<\"1. Time to fit to the dump... zoom in to the left (0 - ~0.5eV) in the top right plot\"<<endl;
	cout<<\"2. Try to fit with the linear decay with:\"<<endl;
	cout<<\"FitEnergyDump(StartOfDecay,StopOfDecay)\"<<endl;
	cout<<\"Save the plot when your ready, either do it manually, or with the function SaveCanvas\" << endl;
	cout<<\"SaveCanvas(${RUN_NO},\\\"temp\\\")\";
	cout<<\" \"<<endl;
	cout<<\"An example can be found here: $EXAMPLE \"<< endl;
	return 0;
	}	
	">MenuMacro$RUN_NO.C
	root -l MenuMacro$RUN_NO.C
}

PbarTempMenu()
{
	clear
	echo "



======================================================================"
		echo "Pbar Temperature dumps - SUBMENU: Please input number below:"
		echo "======================================================================
	Which Dump do you need?
	1. E4E5 with CATCH_OR - (dumpfiles/ColdDumpE4E5.dump)
	2. E5E6 with ATOM_STICK - (dumpfiles/ColdDump_E5E6_500ms_withOffsets_20141105.dat)
	3. E11 with ATOM_STICK - (dumpfiles/ColdDump_E11_500ms_20141105.dat) (2014 or 2016)
	4. E11 with ATOM_STICK - (dumpfiles/cold_dump_E09E10_clear_positrons_1.2_mixE11_E13-E14.dumpfile) (2015 style)
	5. E3E4 and E4E5 with CATCH_OR (ColdDumpHalf_C3C4.dat and dumpfiles/ColdDumpE4E5.dump)
	6. 13E14 ATOM_STICK - (pbar_dump_E13E14.dat)
	7. E10E11 with ATOM_STICK (2016 post short style)
	8. E14 with ATOM_STICK (dumpfiles/LifetimeFinalColdDumpRightFromE14 2016 style)
	9. E14E15 with ATOM_STICK (dumpfiles/AT_pbar_cold_dump_E14E15 2016 style)
	10. E11 with ATOM_STICK - (dumpfiles/Pre-mix_SlowDump_2s_20180505.dat)
	"
	read SUBMENUVAR
	CONTINUE=1
	
	case $SUBMENUVAR in
	1 )
		DDUMP="Cold Dump"
		SIS=3 #CATCHING TRAP PMT OR
		DUMP_FILE="dumpfiles/ColdDumpE4E5.dump"
		EXAMPLE=" https://alphacpc05.cern.ch:8443/elog/DataLog/150708_211427/Run37269Cold.png "
		FIRSTGUESS=" FitEnergyDump(0.15,0.3); "
		runNoRecoAnalysis
		RunPbarTemp
	;;
	
	2 )
		DDUMP="Cold Dump"
		SIS=42
		DUMP_FILE="dumpfiles/ColdDump_E5E6_500ms_withOffsets_20141105.dat"
		EXAMPLE=" https://alphacpc05.cern.ch:8443/elog/DataLog/150710_221719/R37357.png "
		runNoRecoAnalysis
		RunPbarTemp
	;;
	
	3 )
		DDUMP="Cold Dump"
		SIS=42
		DUMP_FILE="dumpfiles/ColdDump_E11_500ms_20141105.dat"
		EXAMPLE="https://alphacpc05.cern.ch:8443/elog/DataLog/150718_022529/Run37729_Temp.png"
		runNoRecoAnalysis
		RunPbarTemp
	;;
	4)
		DDUMP="Cold Dump"
		SIS=42
		DUMP_FILE="dumpfiles/cold_dump_E09E10_clear_positrons_1.2_mixE11_E13-E14.dumpfile"
		EXAMPLE="https://alphacpc05.cern.ch:8443/elog/DataLog/151004_135956/R40656Temp.png"
		FIRSTGUESS="FitEnergyDump(0.05,0.09); "
		runNoRecoAnalysis
		RunPbarTemp
	;;
	
	5 )
		DDUMP="Cold Dump"
		SIS=3 #CATCHING TRAP PMT OR
		DUMP_FILE="dumpfiles/ColdDumpHalf_C3C4.dat"
		EXAMPLE=" https://alphacpc05.cern.ch:8443/elog/DataLog/150904_154932/R38977-Colddump1-Temp.png "
		FIRSTGUESS=" FitEnergyDump(0.15,0.3); "
		runNoRecoAnalysis
		RunPbarTemp
		DDUMP="Cold Dump"
		SIS=3 #CATCHING TRAP PMT OR
		DUMP_FILE="dumpfiles/ColdDumpE4E5.dump"
		EXAMPLE=" https://alphacpc05.cern.ch:8443/elog/DataLog/150904_151527/R38977-Colddump2-Temp.png "
		FIRSTGUESS=" FitEnergyDump(0.15,0.3); "
		REPETITION=",2"
		runNoRecoAnalysis
		RunPbarTemp
	;;
	
	6  )
		DDUMP="Cold Dump"
		SIS=42
		DUMP_FILE="dumpfiles/pbar_dump_E13E14.dat"
		FIRSTGUESS=" FitEnergyDump(0.05,0.1); "
		EXAMPLE="https://alphacpc05.cern.ch:8443/elog/DataLog/150718_022529/Run37729_Temp.png"
		runNoRecoAnalysis
		RunPbarTemp
	;;
	7)
		DDUMP="Cold Dump"
		SIS=42
		DUMP_FILE="dumpfiles/ARTrappingv1.1_ColdDump_pre-mixing_20160713.dat"
		runNoRecoAnalysis
		RunPbarTemp
	;;
	8)
		DDUMP="Cold Dump"
		SIS=42
		DUMP_FILE="dumpfiles/LifetimeFinalColdDumpRightFromE14"
		FIRSTGUESS="FitEnergyDump(0.1,0.4);"
		runNoRecoAnalysis
		RunPbarTemp
	;;
	9)
	 	DDUMP="Cold Dump"
		SIS=42
		DUMP_FILE="dumpfiles/AT_pbar_cold_dump_E14E15"
		FIRSTGUESS="FitEnergyDump(0.04,0.09)"
		runNoRecoAnalysis
		RunPbarTemp
	;;
	10)	
		echo "slow dump pre-mix"
		DDUMP="Cold Dump"
		SIS=42
		DUMP_FILE="dumpfiles/Pre-mix_SlowDump_2s_20180505.dat"
		EXAMPLE="https://alphacpc05.cern.ch:8443/elog/DataLog/150718_022529/Run37729_Temp.png"
		runNoRecoAnalysis
		RunPbarTemp	
	;;
	esac
}

ChooseBinning()
{
		echo "



======================================================================"
echo "How many bids do you want?"
echo "======================================================================"
echo "	1. Default (100)"
echo "	2. 1000"
echo "	3. Nbins = SIS readouts (allot! 450us bins) - I assume you look at whole run"
echo "	4. Nbins = SIS readouts (allot! 450us bins) - I assume you have start and stop times"
echo "	5. Custom"
read SUBMENUVAR
	CONTINUE=1
	case $SUBMENUVAR in
	* | 1 )	BINNING=100 ;;
	2  )	BINNING=1000 ;;
	3  )	BINNING="GetTotalRunTime($RUNNO)/450E-6" ;;
	4  )	BINNING="(sstop-sstart)/450E-6" ;;
	5  )	read BINNING ;;
	esac
return BINNING
}

RunDetectorQOD()
{
	cd $RELEASE/alphaAnalysis/macros

	echo "#include \"$PLOTDUMPSVER+\"
	Int_t MenuMacro${RUN_NO}(Int_t runNumber=$RUN_NO){
	gc= PerModuleOccupancy(${RUN_NO});
	SaveCanvas($RUN_NO,\"-ModuleOccupancy\");
	}	
	">MenuMacro$RUN_NO.C
	root -b -q MenuMacro$RUN_NO.C
	
	echo "#include \"$PLOTDUMPSVER+\"
	Int_t MenuMacro${RUN_NO}(Int_t runNumber=$RUN_NO){
	gApplyCuts=0;
	gc= PlotVertices($RUN_NO);
	SaveCanvas($RUN_NO,\"-Vertices_NoCuts\");
	}	
	">MenuMacro$RUN_NO.C
	root -b -q MenuMacro$RUN_NO.C
	
	echo "#include \"$PLOTDUMPSVER+\"
	Int_t MenuMacro${RUN_NO}(Int_t runNumber=$RUN_NO){
	gApplyCuts=1;
	gc= PlotVertices($RUN_NO);
	SaveCanvas($RUN_NO,\"-Vertices_WithCuts\");
	}	
	">MenuMacro$RUN_NO.C
	root -b -q MenuMacro$RUN_NO.C
	
	echo "#include \"$PLOTDUMPSVER\"
	
	Int_t MenuMacro${RUN_NO}(Int_t runNumber=$RUN_NO){
	TH1D* NNHit=PlotNHitHisto(runNumber,0., -1., kFALSE);
	TH1D* RawHit=PlotNHitHisto(runNumber,0., -1., kTRUE);
	gc = new TCanvas();
	gc->Divide(1, 2);
	gc->cd(1);
	NNHit->Draw();
	gc->cd(2);
	RawHit->Draw();
	SaveCanvas($RUN_NO,\"-HitNumberPerEvent\");
	}	
	">MenuMacro$RUN_NO.C
	root -b -q MenuMacro$RUN_NO.C
	
	 echo "#include \"$PLOTDUMPSVER\"
	 Int_t MenuMacro${RUN_NO}(Int_t runNumber=$RUN_NO){
	 Int_t nBinsOld=gNbin;
	 gc=PlotOccupancyAgainstTime_fixedBinWidth($RUN_NO,0.,-1,10);
	 SaveCanvas($RUN_NO,\"-OccupancyVsTime\");
	 gNbin=nBinsOld;
	 }	
	 ">MenuMacro$RUN_NO.C
	 root -b -q MenuMacro$RUN_NO.C
	 
	 echo "#include \"$PLOTDUMPSVER+\"
	 Int_t MenuMacro${RUN_NO}(Int_t runNumber=$RUN_NO){
	 SaveCosmicUtilsPlots(${RUN_NO});
	 }	
	 ">MenuMacro$RUN_NO.C
	 root -b -q MenuMacro$RUN_NO.C
	
	 echo "#include \"$PLOTDUMPSVER\"
	 Int_t MenuMacro${RUN_NO}(Int_t runNumber=$RUN_NO){
	 SavePerStripOccupancy(${RUN_NO},\"R${RUN_NO}-Occupancy\");
	 }	
	 ">MenuMacro$RUN_NO.C
	 root -b -q MenuMacro$RUN_NO.C
	
	 echo "#include \"$PLOTDUMPSVER+\"
	 Int_t MenuMacro${RUN_NO}(Int_t runNumber=$RUN_NO){
	 SaveStripMeanCompareHistogramsSummary(${RUN_NO},\"R${RUN_NO}-StripMeanVsRMS\");
	 //cout <<\"Now paste the following into the elog: \"<< endl<< endl<< endl;
	 //PrintDetectorQOD(${RUN_NO})
	 }	
	 ">MenuMacro$RUN_NO.C
	 root -b -q MenuMacro$RUN_NO.C
	 
	 
	 
	 echo "#include \"$PLOTDUMPSVER\"
	 Int_t MenuMacro${RUN_NO}(Int_t runNumber=$RUN_NO){
	 PrintDetectorQOD(${RUN_NO}); 
	 }
	 ">MenuMacro$RUN_NO.C
	 root -b -q -l MenuMacro$RUN_NO.C > AutoPlots/$(date +%Y%m%d)/R${RUN_NO}cosmicAnalysis.log
	 
	 
	 
	ElogTitleMessage=`grep cosmicAnalysisError: AutoPlots/$(date +%Y%m%d)/R${RUN_NO}cosmicAnalysis.log | sed -r 's/^.{20}//'`

	 
	#echo "AutoPlots/$(date +\"%m_%d_%Y\")"
	#echo "AutoPlots/$(date +%Y%m%d)"
	if [ -n "$ELOGSUBMIT" ]
	then
		if [ $ELOGSUBMIT -gt $RUN_NO ]
		then
			UPLOADDIR="$RELEASE/alphaAnalysis/macros/AutoPlots/$(date +%Y%m%d)/"
			echo "Uploading to elog"
			ssh -X alphacpc05 "elog -h localhost -a Author=run_some_analysis.sh -a Subject=\"CosmicAnalysis$ElogTitleMessage\" -p 8080 -l DataLog -m $UPLOADDIR/R${RUN_NO}cosmicAnalysis.log -f $UPLOADDIR/R${RUN_NO}-OccupancyVsTime.png -f $UPLOADDIR/R${RUN_NO}-ModuleOccupancy.png -f $UPLOADDIR/R${RUN_NO}-HCor.png -f $UPLOADDIR/R${RUN_NO}-HDir.png -f $UPLOADDIR/R${RUN_NO}-HTexp.png -f $UPLOADDIR/R${RUN_NO}-Occupancy.png -f $UPLOADDIR/R${RUN_NO}-StripMeanVsRMS.png -f $UPLOADDIR/R${RUN_NO}-HitNumberPerEvent.png -f $UPLOADDIR/R${RUN_NO}-Vertices_NoCuts.png -f $UPLOADDIR/R${RUN_NO}-Vertices_WithCuts.png -r $ELOGSUBMIT -v "
		else
			echo "Bad elog number"
		fi
	else
	echo "Skipping elog upload..."
	fi
}

AdvancedSVD()
{
echo "

1. Cosmic QOD (basic)
2. Detector QOD
3. Cosmic and Detector QOD (full) (no audio announcements)
4. Threshold Scan QUICK and dirty (30k events)
5. Threshold Scan FULL (WARNING... I TAKE A LOOONG TIME) (30k events)
"
read SUBMENUVAR
	CONTINUE=1
	case $SUBMENUVAR in
	1 )	EXTRA_FLAGS=" -e30000 "
		runStrips $RUN_NO $EXTRA_FLAGS
		EXTRA_FLAGS=" --iscosmic --cosmicutil "
		runAnalysis $RUN_NO "$EXTRA_FLAGS" $FORCE_RUN
	;;

	2 )	runStrips $RUN_NO $EXTRA_FLAGS
		EXTRA_FLAGS=" --adcspecs "
		runAnalysis $RUN_NO "$EXTRA_FLAGS" $FORCE_RUN
		RunDetectorQOD
	;;

	3 )	echo "Ok... go and make a cup of tea..."
		sleep 2
		FORCE_RUN=1
		NOTALK=1
		runStrips $RUN_NO $EXTRA_FLAGS
		EXTRA_FLAGS=" --iscosmic --cosmicutil "
		runAnalysis $RUN_NO "$EXTRA_FLAGS" $FORCE_RUN
		EXTRA_FLAGS=" --adcspecs "
		runAnalysis $RUN_NO "$EXTRA_FLAGS" $FORCE_RUN
		RunDetectorQOD
	;;

	* ) CONTINUE=1
	;;
	4 )
		JOB_LIST="5 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 95 100 110 120 130 140 150"
	;;
	5 )
		JOB_LIST="1 2 3 4 6 8 10 12 14 16 18 20 22 24 26 28 30 32 34 36 38 40 42 44 46 48 50 52 54 56 58 60 62 64 66 68 70 75 80 85 90 95 100 110 120 130 140 150"
#33 35 37 39 41 43 45 47 49 51 53 55 57 59 61 63 65 67 69 71 73 74 76 77 78 79 82 84 86 88 90 92 10 12 14 18 20 22 24 26 28 30 32 34 36 38 40 42 44 46 48 50 52 54 56 58 60 62 64 66 68 70 75 80 95 100 110 120 130 140 150 200
	;;
	2 | 3 )
	
#	echo "What computer do you want to use for analysis?"
#	read ANALYSIS_PC
	echo "How many threads do you want to use? (Recommendation: 2 or more)"
	read maxjobs
	EXTRA_FLAGS=" -e30000 "
	runStrips $RUN_NO $EXTRA_FLAGS
	
	for thres in $JOB_LIST
	  do
	    EXTRA_FLAGS=" -e30000 --enablesupp${thres} "
	    mkdir -p "$RELEASE/alphaAnalysis/data/threshold_scan_${thres}/"
	    TREEFILES=${RELEASE}/alphaAnalysis/data/threshold_scan_${thres}/
	    if [ maxjobs -eq 1 ]
		then
			runAnalysis $RUN_NO "$ALPHAFLAGS" $FORCE_RUN
		else
			jobcnt=(`jobs -p`)
			echo "Jobcount ${jobcnt}"
			if [ ${#jobcnt[@]} -lt $maxjobs ] ; then
				echo "Running strips $RUNNO "
				runAnalysis $RUN_NO "$ALPHAFLAGS" $FORCE_RUN &
			else
				echo "waiting"
				while [ ${#jobcnt[@]} -ge $maxjobs ]
				do
					sleep 30
					echo "$maxjobs jobs running... sleeping 30s"
					jobcnt=(`jobs -p`)
				done
				echo "Running strips $RUNNO "
				runAnalysis $RUN_NO "$ALPHAFLAGS" $FORCE_RUN &
			fi
	    fi
	done
	;;
	esac
}
Logo1()
{
   echo "                    * * * * * * * * * * * * * * * * *"
   echo "                    *                               *"
   echo "                    *              #### #           *"
   echo "                    *             #_   #            *"
echo -e "                    *           AL""\e[7mP""\e[27mHA  #            *"
   echo "                    *             #    #            *"
   echo "                    *              #### #           *"
   echo "                    *                               *"
   echo "                    * * * * * * * * * * * * * * * * *"


}

Logo2()
{
echo  "                                        .,,╓╓,.                                 "
echo  "                                  ,╦▒╬╬╬╬╬╬╬╬╬╬╬╬▒╦╖               KKKKKKKK⌂    "
echo  "                               ,╗╣╬╬╬╬╬╬Å╙╙╙╙╙╜╩╬╬╬╬╬▒╕           ╢╬╬╬╬╬╬╬╩     "
echo  "                             ╖╬╬╬╬╬╬╬╬┘          \`╙╣╬╬╬▒┐        j╬╬╬╬╬╬╬╬      "
echo  "                           ╓╬╬╬╬╬╬╬╬Å               ╙╣╬╬╬φ      ╓╬╬╬╬╬╬╬╬       "
echo  "                          ╬╬╬╬╬╬╬╬╬M                  ╚╬╬╬Ñ     ╬╬╬╬╬╬╬╬U       "
echo  "                         ╬╬╬╬╬╬╬╬╬Å                    ╚╬╬╬H   ╬╬╬╬╬╬╬╬M        "
echo  "                        ]╬╬╬╬╬╬╬╬╬                      ╬╬╬╬  ╢╬╬╬╬╬╬╬╝         "
echo  "                        ╬╬╬╬╬╬╬╬╬Å                      ²╬╬╬▒j╬╬╬╬╬╬╬╬          "
echo  "                       j╬╬╬╬╬╬╬╬╬\" ▀▀▀▀▀▀▀▀▀             ╢╬╬╬╬╬╬╬╬╬╬╬           "
echo  "     ▓██▄     ▓█M      ╠\`\`,,,\`\`╙╬ :██⌐   ▐██     ▄██▌    j╬╬╬╬╬╬╬╬╬╬U         "  
echo  "    ██▌██▌    ██▌      ╬  ╠╬╬╬  ; j██▄╓╓╓╣██    ▓█▌▓██    ╬╬╬╬╬╬╬╬╬M            "
echo  "   ███▄▄██▓   ██▌      ╬  ,,,╓╓╗╬ j██▀▀▀▀███   ▓██▄▄███   ╠╬╬╬╬╬╬╬╝             "
echo  "  ██▀╙╙╙╙███  ██▓▄▄▄▄▄ ╬  ╠╬╬╬╬╬╬ j██⌐   ║██  ███╙╙╙╙▀██ .╬╬╬╬╬╬╬╬              "
echo  " \"╙╙      ╙╙\` \"╙╙╙╙╙╙╙ ╢▒K╬╬╬╬╬╬╬  ╙╙     ╙^ \`╙╙      ╙╙T╬╬╬╬╬╬╬╬\`           "   
echo  "                       ╚╬╬╬╬╬╬╬╬╬H                      ╢╬╬╬╬╬╬╬▒               "
echo  "                        ╬╬╬╬╬╬╬╬╬▒                     ]╬╬╬╬╬╬╬╬╬┐           ., "
echo  "                        ²╬╬╬╬╬╬╬╬╬┐                   ]╬╬╬╬╬╬╬╙╬╬╬           ╢╬ "
echo  "                         ╙╬╬╬╬╬╬╬╬▒                  φ╬╬╬╬╬╬╬\` ²╬╬╬,        j╬╬ "
echo  "                          ²╬╬╬╬╬╬╬╬▒                ╬╬╬╬╬╬╬R    ²╬╬╬▒,    ,╗╬╬╬ "
echo  "                            ╚╬╬╬╬╬╬╬╣╦            φ╬╬╬╬╬╬╩\"      ²╬╬╬╬╬▒▒╣╬╬╬╬Ω "
echo  "                              ╙╩╬╬╬╬╬╬╬K╖.    ,╦▒╬╬╬╬╬╬╨\`         \`╢╬╬╬╬╬╬╬╬╬M  "
echo  "                                 \"╜╩╬╬╬╬╬╬╬╬╬╬╬╬╬╬╩╨^\`              ╙╬╬╬╬╬╬Å\`   "
echo  "                                      \`\"^╙╙╙^^\`                        \`\"\`      "
echo  ""

}



Logo3()
{
echo "
                                                                   
                                                                   
                                                                   
                                                                   
                                                                   
                                 .;;'';;\`                          
                              :''''''''''',             '''''''\`   
                            :'''''''''''''''            '''''''    
                           ''''''';...;''''''.         .'''''':    
                          '''''''        '''''.        '''''''     
                        \`'''''''          :''''        '''''''     
                        '''''''\`           .''''      :'''''',     
                       ''''''';             :'''.     '''''''      
                      :'''''''               ''''    \`'''''';      
                      ''''''';                '''\`   ;''''''.      
                     ;'''''''                 ''';   '''''''       
                     ''''''''                 \`'''  \`'''''';       
                    .''''''';                  '''\` ;''''''        
                    ;''''''',                  ''': '''''''        
                    ''''''''\`                  ,''','''''';        
                    ''''''''                    ''''''''''         
                    ''''''''                    ''''''''''         
     ,,     ,,     \`:::::;'' ,,    ,,     ,,    '''''''''\:         
    \:@@,    @@     \`       ' @@    @@    #@@\`   ;''''''''          
    @@@@    @@     :  ''', , @@    @@    @#@#   ,''''''';          
   ,@::@,   @@     :  '''\` : @@,,,,@@   +@ @@   .'''''''.          
   @@  @@   @@     :      \`' @@@@@@@@   @@  @@   '''''''           
  .@@@@@@,  @@     :  ;;'''' @@    @@  +@@@@@@\`  '''''';           
  @@....@@  @@     :  '''''' @@    @@  @@...,@@ .''''''\`           
 \`@+    \'@: @@@@@@@:  '''''' @@    @@ +@.    @@ '''''''            
                   \`''''''''                    '''''';            
                   \`''''''''                   ,''''''             
                    ''''''''                   '''''''             
                    ''''''''\`                 \`''''''',            
                    ;''''''',                 ;''''''''          . 
                    .''''''';                 '''''';''         \`' 
                     ''''''''                :'''''' ''.        ,' 
                     ;'''''''                ''''''. ;''        '' 
                      ''''''':              :''''''  \`'':       '' 
                      ;'''''''              ''''''    ''':     ''' 
                       ''''''';            '''''':    ,''';   '''' 
                        ''''''',          '''''';      ''''''''''; 
                        \`''''''',        '''''';       ,'''''''''\` 
                          ''''''''     ,'''''',         '''''''''  
                           :'''''''''''''''''            '''''''   
                             :'''''''''''':              \`'''''    
                               \`:''''';,                   :;:     
                                                                   
                                                                   
"

}




cd $RELEASE
#Logo

PLOTDUMPSVER="PlotDumps.C+"  #_Joe2 This will migrate over to PlotDumps.C soon
NOTALK=1 #by default, announce passed cuts  
VERBOSE="1"
TRUE="1"
if [[ $# -ge 1 ]]
then

	case $@ in
		#Must check if I am in batch mode first...


		*"--batch"* | *"-b"* )
		echo "Batch mode detected"
		DecodeBatchFlag $@
		;;

		*"--list"* )
		echo "List mode detected"
		LISTFILE=$2
		echo "List file detector: $LISTFILE " 
		ListMenu
		;;
		
		*"--NOTALK"* )
		NOTALK=1
		;;
		
		*"--temp"* )
		echo "Entering automatic tempurature estimation mode"
		RUN_NO=$2
		runNoRecoAnalysis
		RunPbarAutoTemp
		;;
		
		* )	
		RUN_NO=$@
		if [[ $RUN_NO -gt 30000 ]]
		then
			echo "Run number given seems realistic"
			MainMenu
		else
			echo "
Arguments given not recognised or run number is bad...

Valid arguements are:

--batch: Turn off interactive mode

--list <LISTFILE>

--temp RUN_NO

--batch --QUENCH <runNumber>  : Quench analysis (mixing AND quench)

--batch --FRD <runNumber> : Fast Ramp Down Analysis (mixing AND FRD)

Please input Run Number... or press ctrl + c to exit
"
			read RUN_NO
			MainMenu
		fi
		;;
	esac
else
	echo "
You can pass a number in when running the script... for more information, use the arguement '-h'

Please input Run Number to continue... or press ctrl + c to exit
"
	read RUN_NO
	MainMenu
fi

