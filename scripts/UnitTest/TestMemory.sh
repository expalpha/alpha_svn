#!/bin/bash
RUNNO=$1

if [ `echo "$RELEASE" | wc -c` -gt 3 ]; then
  echo "RELEASE set ok..."
else
  echo "myconfig.sh not set"
fi


if [ `echo "$RUNNO" | wc -c` -gt 3 ]; then
  echo "Using run $RUNNO"
else
  RUNNO=45000
  echo "Using default run $RUNNO"
fi

for i in `seq 1 1000`; do
  if [ -e LeakTest${i}.log ]; then
  EATCAKE=1
  else
    if [ -e StripTest${i}.log ]; then
    EATCAKE=2
    else
      if [ -e AnalysisTest${i}.log ]; then
      EATCAKE=3
      else
        if [ -e MacroTest${i}.log ]; then
          echo -n "."
        else
          STRIPTEST="${RELEASE}/scripts/UnitTest/StripTest${i}.log"
          LEAKTEST="${RELEASE}/scripts/UnitTest/LeakTest${i}.log"
          ALPHATEST="${RELEASE}/scripts/UnitTest/AnalysisTest${i}.log"
          MACROTEST="${RELEASE}/scripts/UnitTest/MacroTest${i}.log"
          SVNDIFF="${RELEASE}/scripts/UnitTest/SVN_DiffTest${i}.log"
          TESTID=${i}
          break
        fi
      fi
    fi
  fi
done

cd $RELEASE
svn diff > ${SVNDIFF}

echo "Running alphaStrips..."
cd $RELEASE/alphaStrips
./alphaStrips.exe --EOS midasdata/run${RUNNO}sub00000.mid.gz &> ${STRIPTEST}

echo "Running alphaAnalysis..."
cd $RELEASE/alphaAnalysis
#Suppress false positives: https://root.cern.ch/how/how-suppress-understood-valgrind-false-positives
valgrind --leak-check=full --error-limit=no --suppressions=${ROOTSYS}/etc/valgrind-root.supp  --log-file="${LEAKTEST}" ./alphaAnalysis.exe --EOS midasdata/run${RUNNO}sub00000.mid.gz &> ${ALPHATEST}
 
cat ${LEAKTEST} | cut -f2- -d' ' > ${LEAKTEST}.nopid
 
 
cd $RELEASE/alphaAnalysis/macros
echo "Running macros..."

echo "#include \"PlotDumps.C+\"
void test${TESTID}()
{
PrintDetectorQOD(${RUNNO},\"Mixing\");
}
" > test${TESTID}.c

/usr/bin/time -v root -l -q test${TESTID}.c &> ${MACROTEST}

cat ${LEAKTEST}.nopid | tail -n 16

echo "done..."
echo "check:
  ${STRIPTEST}
  ${LEAKTEST}
  ${ALPHATEST}
  ${MACROTEST}
          "
#cd $RELEASE
