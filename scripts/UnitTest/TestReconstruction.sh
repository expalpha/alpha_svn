#!/bin/bash
RUNNO=$1

if [ `echo "$RELEASE" | wc -c` -gt 3 ]; then
  echo "RELEASE set ok..."
else
  echo "myconfig.sh not set"
fi


if [ `echo "$RUNNO" | wc -c` -gt 3 ]; then
  echo "Using run $RUNNO"
else
  RUNNO=45000
  echo "Using default run $RUNNO"
fi

for i in `seq 1 1000`; do
  if [ -e CosmicTest${i}.log ]; then
  EATCATE=1
  else
    if [ -e StripTest${i}.log ]; then
    EATCATE=2
    else
      if [ -e AnalysisTest${i}.log ]; then
      EATCATE=3
      else
        if [ -e MacroTest${i}.log ]; then
          echo -n "."
        else
          STRIPTEST="${RELEASE}/scripts/UnitTest/StripTest${i}.log"
          COSMICTEST="${RELEASE}/scripts/UnitTest/CosmicTest${i}.log"
          ALPHATEST="${RELEASE}/scripts/UnitTest/AnalysisTest${i}.log"
          MACROTEST="${RELEASE}/scripts/UnitTest/MacroTest${i}.log"
          break
        fi
      fi
    fi
  fi
done
echo "Re-compiling everything..."
cd ${RELEASE}
make clean
make

echo "Running alphaStrips..."
cd ${RELEASE}/alphaStrips
./alphaStrips.exe --EOS midasdata/run${RUNNO}sub00000.mid.gz &> ${STRIPTEST}
cat ${STRIPTEST}
echo "Running alphaAnalysis..."
cd ${RELEASE}/alphaAnalysis

./alphaAnalysis.exe --EOS midasdata/run${RUNNO}sub00000.mid.gz &> ${ALPHATEST}
cat ${ALPHATEST}
./alphaAnalysis.exe --iscosmic --cosmicutil --EOS midasdata/run${RUNNO}sub00000.mid.gz &> ${COSMICTEST}
cat ${COSMICTEST}

cd ${RELEASE}/alphaAnalysis/macros
echo "Running macros..."
echo ".L PlotDumps.C+
PrintDetectorQOD(${RUNNO},\"Mixing\")
.q
"|root -l &> ${MACROTEST}
${MACROTEST}

echo "done..."
echo "check:
  ${STRIPTEST}
  ${LEAKTEST}
  ${ALPHATEST}
  ${MACROTEST}
          "
#cd $RELEASE
