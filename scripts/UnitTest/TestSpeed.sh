#!/bin/bash
RUNNO=$1
echo "Warning: I take a long time... like 10x normal speed..."
if [ `echo "$RELEASE" | wc -c` -gt 3 ]; then
  echo "RELEASE set ok..."
else
  echo "myconfig.sh not set"
fi


if [ `echo "$RUNNO" | wc -c` -gt 3 ]; then
  echo "Using run $RUNNO"
else
  RUNNO=45000
  echo "Using default run $RUNNO"
fi

for i in `seq 1 1000`; do
  if [ -e LeakTest${i}.log ]; then
  EATCAKE=1
  else
    if [ -e StripTest${i}.log ]; then
    EATCAKE=2
    else
      if [ -e AnalysisTest${i}.log ]; then
      EATCAKE=3
      else
        if [ -e MacroTest${i}.log ]; then
          echo -n "."
        else
          STRIPTEST="${RELEASE}/scripts/UnitTest/StripTest${i}.log"
          SPEEDTEST="${RELEASE}/scripts/UnitTest/SpeedTest${i}.out"
          ALPHATEST="${RELEASE}/scripts/UnitTest/AnalysisTest${i}.log"
          MACROTEST="${RELEASE}/scripts/UnitTest/MacroTest${i}.log"
          SVNDIFF="${RELEASE}/scripts/UnitTest/SVN_DiffTest${i}.log"
          TESTID=${i}
          break
        fi
      fi
    fi
  fi
done

cd $RELEASE
svn diff > ${SVNDIFF}

echo "Running alphaStrips..."
cd $RELEASE/alphaStrips
./alphaStrips.exe --EOS midasdata/run${RUNNO}sub00000.mid.gz &> ${STRIPTEST}

echo "Running alphaAnalysis..."
cd $RELEASE/alphaAnalysis
#Suppress false positives: https://root.cern.ch/how/how-suppress-understood-valgrind-false-positives
valgrind --tool=callgrind --callgrind-out-file="${SPEEDTEST}" ./alphaAnalysis.exe --EOS midasdata/run${RUNNO}sub00000.mid.gz &> ${ALPHATEST}
 
 
cd $RELEASE/alphaAnalysis/macros
echo "Running macros..."

echo "#include \"PlotDumps.C+\"
void test${TESTID}()
{
PrintDetectorQOD(${RUNNO},\"Mixing\");
}
" > test${TESTID}.c

/usr/bin/time -v root -l -q test${TESTID}.c &> ${MACROTEST}


echo "done..."
echo "check:
  ${STRIPTEST}
  kcachegrind ${SPEEDTEST}
  ${ALPHATEST}
  ${MACROTEST}
          "
#cd $RELEASE
