#!/bin/bash

export RUNNO=$1
if [[ "$HOSTNAME" = "alphaautomagic2.cern.ch" ]]; then
  echo "Good, we are on $HOSTNAME"
else
  echo "Script only designed to run on alphaautomagic2.cern.ch"
  exit
fi

mkdir ~/AnalysisOutput/${RUNNO}/

cd alphaSoftware2012
source myconfig.sh > /dev/null



#Post elog placeholder here!
ELOG_NO=`ssh -X alphadaq "~/packages/elog/elog -h localhost -a Author=$HOSTNAME -a Subject=\"Analysis of ${RUNNO} in progress\" -p 8080 -l AutoAnalysis  -v \"Fetching file from EOS...\"" | grep ID= | tr 'Message successfully transmitted, ID=' "\n"`
echo "$ELOG_NO"
ELOG_NO=`echo "$ELOG_NO" | grep -o '[0-9]*'`
echo "$ELOG_NO"
ELOG_SUBJECT=""


#rsync --rsync-path="nice rsync" alphadaq:/alpha/data39b/alpha/current/run${RUNNO}sub00*.mid.gz ${MIDASDIR}/
#echo "rsync done"
EOS_READY=0
TIME_WAITED=0
while [ $EOS_READY -eq 0 ]; do
  EOS_SIZE=`eos ls -l /eos/experiment/alpha/midasdata/run${RUNNO}sub00000.mid.gz | awk -F$' ' '{print $5}'`
  #if [ -z `echo "$EOS_SIZE" | grep "No such file or directory"` ]; then
#  if [ -I "$EOS_SIZE" ]; then
  if [ "$EOS_SIZE" -eq "$EOS_SIZE" ] 2>/dev/null; then
    EOS_HAS_FILE=1
  else
    EOS_SIZE=-1
    echo "No file found on EOS yet..."
  fi
  DAQ_SIZE=`ssh alphadaq ls -l /home/alpha/alphaSoftware2012/alphaAnalysis/midasdata/run${RUNNO}sub00000.mid.gz`
  DAQ_SIZE=`echo "$DAQ_SIZE" | awk -F$' ' '{print $5}'`
  if [ $EOS_SIZE -eq $DAQ_SIZE ]; then
    EOS_READY=1
    echo "Files on EOS and alphadaq match sizes... Good"
  elif [ ${#DAQ_SIZE} -lt 1 ]; then
     EOS_READY=1
     echo "Files only on EOS"
  else 
    echo "Files on EOS and alphadaq do not match in size: $EOS_SIZE vs $DAQ_SIZE... sleeping "
    sleep 5
    TIME_WAITED=$((${TIME_WAITED} + 5))
    if [ $TIME_WAITED -gt 300 ]; then
      echo "waited more than 300s, error with alphadaq or EOS?...exiting"
      ssh -X alphadaq "~/packages/elog/elog -h localhost -a Author=$HOSTNAME -a Subject=\"Analysis of ${RUNNO} aborted\" -e $ELOG_NO -p 8080 -l AutoAnalysis  -v \"I waited for 500s but didn't find the midas file on EOS...\
      \nCheck Lazy EOS is running\
      \nCheck EOS status\
      \nCheck AFS for EOS executable\
      \nTry again... : \
      \n\
      \nssh alphaautomagic2\
      \ntouch watch_me/{RUNNO}\""

      exit
    fi
  fi
done

#if [ -I $ELOG_NO ] ; then

re='^[0-9]+$'
if  [[ "$ELOG_NO" =~ $re ]]; then
  echo "Elog placeholder made successfully"
else
  echo "Failed to post to elog... exiting..."
  exit
fi
echo "Copying files..."
eos cp /eos/experiment/alpha/midasdata/run${RUNNO}sub00000.mid.gz ${MIDASDIR}/

	ssh -X alphadaq "~/packages/elog/elog -h localhost -a Author=$HOSTNAME -a Subject=\"Analysis of ${RUNNO} in progress\" -e $ELOG_NO -p 8080 -l AutoAnalysis  -v \Processing"



#Start alphaStrips
export STRIPFILES=${RELEASE}/alphaStrips/data/${RUNNO}/
export MIDASDIR=${RELEASE}/alphaStrips/midasdata
mkdir -p ${STRIPFILES}
cd ${RELEASE}/alphaStrips
if [ -e ${STRIPFILES}/alphaStrips${RUNNO}offline.root ]; then
  echo "alphaStrips has been run before on this run... error.
  Delete ${STRIPFILES}/alphaStrips${RUNNO}offline.root to fix issue"
   STRIPS_PID="Strips not running"
  #exit 1
else
  echo "${STRIPFILES}/alphaStrips${RUNNO}offline.root file does not exist"
  ./alphaStrips.exe -e20000 midasdata/run${RUNNO}sub00000.mid.gz &> ~/Logs/StripsR${RUNNO}.log & # && eos cp ${STRIPFILES}/alphaStrips${STRIPNO}offline.root /eos/ad/alpha/stripfiles/ &
  STRIPS_PID=$!
  echo "alphaStrips PID: $STRIPS_PID"

fi

cd ${RELEASE}/alphaAnalysis/
./alphaAnalysis.exe --EOS --recoff midasdata/run${RUNNO}sub00000.mid.gz &> ~/Logs/alphaAnalysisR${RUNNO}.RecOff.log
cd ${RELEASE}/alphaAnalysis/macros


echo ".L PlotDumps.C+
if (IsDumpInATM(${RUNNO},\"startDump\",\"FastRampDown\") && IsDumpInATM(${RUNNO},\"stopDump\",\"FastRampDown\")) {cout <<\"FastRampDown dump found\" << endl; }
if (IsDumpInATM(${RUNNO},\"startDump\",\"Microwave\") && IsDumpInATM(${RUNNO},\"stopDump\",\"Microwave\")) {cout <<\"Microwave dump found\" << endl; }
.q
" | root -l -b > ~/AnalysisOutput/${RUNNO}/R${RUNNO}_SequenceQOD.log
sleep 1
cat ~/AnalysisOutput/${RUNNO}/R${RUNNO}_SequenceQOD.log
MICROWAVE=`grep 'Microwave dump found' ~/AnalysisOutput/${RUNNO}/R${RUNNO}_SequenceQOD.log`
FASTRAMPDOWN=`grep 'FastRampDown dump found' ~/AnalysisOutput/${RUNNO}/R${RUNNO}_SequenceQOD.log`
##################################################################
# Test if alphaStrips is finished... if not... find a valid one...
##################################################################
export STRIPNO=$RUNNO
#Loop over last 10 runs... look for valid alphaStrips output file
while [ $STRIPNO -gt $(( $RUNNO - 10 )) ]; do
  echo $STRIPNO
  # Test for 'no keys' error (alphaStrips not finished)
  sleep 1
  FILE_CLOSED=`root -l -b -q ${RELEASE}/alphaStrips/data/${STRIPNO}/alphaStrips${STRIPNO}offline.root 2>&1 >/dev/null | grep 'keys' `
  #FILE_CLOSED=`root -l -b -q ${RELEASE}/alphaStrips/data/${STRIPNO}/alphaStrips${STRIPNO}offline.root 1>/dev/null |  grep "keys" `

  echo "file closed:$FILE_CLOSED."
  #FILE_CLOSED=`root -l -q ${RELEASE}/alphaStrips/data/${STRIPNO}/alphaStrips${STRIPNO}offline.root >/dev/null |&  (grep 'keys') `
  if [ ${#FILE_CLOSED} -gt 4 ]; then
    #alphaStrips not finished... moved to older file
    echo "$STRIPNO still running strips"
  else
    #alphaStrips finished... point to the file directory
    export STRIPFILES=${RELEASE}/alphaStrips/data/${STRIPNO}/
    if [ -e ${STRIPFILES}/alphaStrips${STRIPNO}offline.root ]; then
      echo "File age in seconds: $(( - `date -r ${STRIPFILES}/alphaStrips${STRIPNO}offline.root +%s` + `date +%s` ))"
      #If alphaStrips completed less than ~3 hours ago (10k seconds) its ok to use
      if [ `date -r ${STRIPFILES}/alphaStrips${STRIPNO}offline.root +%s` -gt $(( `date +%s` - 10000 )) ]; then
        echo "alphaStrips file is young enough"
      else
        #If alphaStrips was run long ago... wait for latest run to finish
        echo "alphaStrips file too old... waiting for pid: $STRIPS_PID"
        wait $STRIPS_PID
      fi
      break
    else
      echo "File: ${STRIPFILES}/alphaStrips${STRIPNO}offline.root does not exist"
    fi
  fi
  #Count backwards over runs
  STRIPNO=$(( $STRIPNO - 1 ))
  if  [ $STRIPNO -eq $(( $RUNNO - 10 )) ]; then #On last loop itteration 
    echo "No good strips files found... waiting for alphaStrips.exe (PID:${STRIPS_PID}) to finish"
    wait ${STRIPS_PID}
    export STRIPFILES=${RELEASE}/alphaStrips/data/${RUNNO}/
    break
  fi
done
echo "$STRIPFILES"


##################################################################
# Run reconstruction AS FAST AS POSSIBLE and announce MICROWAVE passed cuts
##################################################################



if [ ${#MICROWAVE} -ge 3 ]; then
  echo "MICROWAVE:${MICROWAVE}found... Getting results ASAP for control room!"
  echo "MICROWAVE: ${#MICROWAVE} found... Getting results ASAP for control room!"
  echo "MICROWAVE: $MICROWAVE found... Getting results ASAP for control room!"
  

  #RUSH ANALYSIS OF MICROWAVES
  cd ${RELEASE}/alphaAnalysis/macros
  #export ORIGINALTREEFILES=$TREEFILES
  #export TREEFILES="$TREEFILES/MICROWAVE"
  #mkdir -p $TREEFILES
  #cp $ORIGINALTREEFILES/tree${RUNNO}offline.root $TREEFILES/tree${RUNNO}offline.root 
  echo ".L PlotDumps.C+
  Int_t runNumber=$RUNNO;
  TTimeStamp *t1 = new TTimeStamp();
  const unsigned int date = t1->GetDate();
 
Double_t start_time=MatchEventToTime(runNumber,\"startDump\",\"Microwave\");
start_time=start_time-0.5;
Double_t stop_time=MatchEventToTime(runNumber,\"stopDump\",\"Microwave\");
stop_time=stop_time+0.5;
if (start_time>0 && stop_time>0) {
TString RunThis=\"cd $RELEASE/alphaAnalysis && ./alphaAnalysis.exe --EOS --savespace --output=tree${RUNNO}MICROWAVE.root --usetimerange=\";
RunThis+=start_time;
RunThis+=\":\";
RunThis+=stop_time;
RunThis+=\" midasdata/run\";
RunThis+=runNumber;
//RunThis+=\"sub00000.mid.gz >& /dev/null\";
RunThis+=\"sub00000.mid.gz \";
cout << RunThis << endl;
gSystem->Exec(RunThis);
}
.q

" | root -b -l&
MICROWAVE_PID=$!
fi

##################################################################
# Run reconstruction AS FAST AS POSSIBLE and announce FRD passed cuts
##################################################################
if [ ${#FASTRAMPDOWN} -ge 3 ]; then
  echo "FastRampDown:${FASTRAMPDOWN}found... Getting results ASAP for control room!"
  echo "FastRampDown: ${#FASTRAMPDOWN} found... Getting results ASAP for control room!"
  echo "FastRampDown: $FASTRAMPDOWN found... Getting results ASAP for control room!"
  


  #RUSH ANALYSIS OF FASTRAMPDOWN
  cd ${RELEASE}/alphaAnalysis/macros

  echo ".L PlotDumps.C+
  Int_t runNumber=$RUNNO;
  TTimeStamp *t1 = new TTimeStamp();
  const unsigned int date = t1->GetDate();
 
Double_t start_time=MatchEventToTime(runNumber,\"startDump\",\"FastRampDown\");
start_time=start_time-0.5;
Double_t stop_time=MatchEventToTime(runNumber,\"stopDump\",\"FastRampDown\");
stop_time=stop_time+0.5;
if (start_time>0 && stop_time>0) {
TString RunThis=\"cd $RELEASE/alphaAnalysis && ./alphaAnalysis.exe --EOS --savespace --output=tree${RUNNO}FRD.root --usetimerange=\";
RunThis+=start_time;
RunThis+=\":\";
RunThis+=stop_time;
RunThis+=\" midasdata/run\";
RunThis+=runNumber;
//RunThis+=\"sub00000.mid.gz >& /dev/null\";
RunThis+=\"sub00000.mid.gz \";
cout << RunThis << endl;
gSystem->Exec(RunThis);
}
.q

" | root -b -l &
FRD_PID=$!

fi



if [ ${#MICROWAVE} -ge 3 ]; then
echo "Waiting for microwaves:"
#wait $MICROWAVE_PID
while [[ ${?} == 0 ]]      # Repeat until the process has terminated.
do
    echo "micro sleep"
    sleep 1       # Wait a bit before testing.
    ps -p $MICROWAVE_PID >/dev/null  # Check if the process has terminated.
done
echo "Microwave focused tree file made..."
#mv $TREEFILES/tree${RUNNO}offline.root $ORIGINALTREEFILES/tree${RUNNO}offline.root 
#export TREEFILES=$ORIGINALTREEFILES
  #Plot the events... if they exist  
echo ".L PlotDumps.C+
  gTreePostfix=\"MICROWAVE.root\";
  PrintMicrowaveSweep(${RUNNO},\"Microwave\");
.q
" | root -l -b | grep -v Microwave &> ~/AnalysisOutput/${RUNNO}/MicrowaveTable.log  


echo ".L PlotDumps.C+
  gTreePostfix=\"MICROWAVE.root\";
  TSISChannels* sisch = new TSISChannels(${RUNNO});
  Int_t Step_Start_Channel=sisch->GetChannel(\"MIC_SYNTH_STEP_START\");
  Int_t Step_Stop_Channel=sisch->GetChannel(\"MIC_SYNTH_STEP_STOP\");
  Int_t SweepSteps=Count_SIS_Triggers(${RUNNO},Step_Start_Channel, \"Microwave\");
  gNbin=SweepSteps;
  gc=PlotVertices($RUNNO,\"Microwave\");
  SaveCanvas($RUNNO,\"Microwave\");
  //AnnounceOnSpeaker(\"Microwave analysis done\");  
.q
" | root -l -b && mv AutoPlots/$(date +%Y%m%d)/R${RUNNO}Microwave.png ~/AnalysisOutput/${RUNNO}/ 


scp ~/AnalysisOutput/${RUNNO}/*.png ~/AnalysisOutput/${RUNNO}/*.log alpha@alphadaq:~/alphaautomagic/${RUNNO}/
MICROWAVEFILES=""
ELOG_SUBJECT="[Microwave]$ELOG_SUBJECT"
for i in `ls ~/AnalysisOutput/${RUNNO}/*`; do
  MICROWAVEFILES="$MICROWAVEFILES -f $i"
done
MICROWAVEFILES=${MICROWAVEFILES//'AnalysisOutput'/'alphaautomagic'}
ssh -X alphadaq "~/packages/elog/elog -h localhost -a Author=$HOSTNAME -a Subject=\"$ELOG_SUBJECT [Processing still going]\" -e $ELOG_NO -m ~/alphaautomagic/${RUNNO}/MicrowaveTable.log ${MICROWAVEFILES} -p 8080 -l AutoAnalysis  -v " &>> ~/AnalysisOutput/R${RUNNO}.Elog_earlyupload.log
rm ${RELEASE}/alphaAnalysis/data/tree${RUNNO}MICROWAVE.root
fi &

if [ ${#FASTRAMPDOWN} -ge 3 ]; then
echo "Waiting for FRD:"
#wait $FRD_PID

while [[ ${?} == 0 ]]      # Repeat until the process has terminated.
do
    echo "frd sleep"
    sleep 1       # Wait a bit before testing.
    ps -p $FRD_PID >/dev/null  # Check if the process has terminated.
done
echo "FRD focused tree file made..."

  #Plot the events... if they exist  
mkdir -p $RELEASE/alphaAnalysis/plots
echo ".L PlotDumps.C+
  gTreePostfix=\"FRD.root\";
gAnnounce=kFALSE;
if (PrintQuenchGateWindow( ${RUNNO},-1) < 8) {
SaveQuenchVF48Events(${RUNNO},-1,kTRUE);
return 0;
}
.q
" | root -l -b && mv AutoPlots/$(date +%Y%m%d)/R${RUNNO}_E*.png ~/AnalysisOutput/${RUNNO}/ && rm ${TREEFILES}/tree${RUNNO}.root &# and here for speed boost!
EVENTS_PID=$!
#Root must be re-loaded to clear open files...
#echo "#include \"PlotDumps_C.so\"

#Int_t MenuMacro${RUNNO}(Int_t runNumber=$RUNNO){
echo ".L PlotDumps.C+
gTreePostfix=\"FRD.root\";
Int_t runNumber=${RUNNO};
if (MatchEventToTime(runNumber,\"startDump\",\"FastRampDown\")>0 && MatchEventToTime(runNumber,\"stopDump\",\"FastRampDown\") >0) {
  Int_t first_VF48;
  Int_t last_VF48;
//cout << \"1\" << endl;
  PrintQuenchGateWindow( runNumber );
  
//cout << \"2\" << endl;
  ReturnQuenchVF48Numbers( runNumber, -1,  first_VF48, last_VF48, 0); //Includes announcements

//cout << \"3\" << endl;
  gc=PlotVertices( runNumber,\"FastRampDown\");

//cout << \"4\" << endl;
  TString savFolder=\"~/AnalysisOutput/${RUNNO}\";
  TString FRDName=\"\";
  FRDName+=savFolder;
  FRDName+=(\"/R\");
  FRDName+=${RUNNO};
  FRDName+=(\"-FastRampDown.png\");
  gc->SaveAs(FRDName);

//PrintAnalysisReport($RUNNO);
//PrintQuenchGateWindow($RUNNO);
}
//return 0;
.q
"|	root -b -l  &> ~/AnalysisOutput/${RUNNO}/R${RUNNO}_FastRampDown.log
sleep 1
#Submit FastRampDown results to elog here! (as a temporary entry)
RESULT=`grep '\[C\|\Passed' ~/AnalysisOutput/${RUNNO}/R${RUNNO}_FastRampDown.log | grep "\]"`
PLOT_DIR=$(ls -t $RELEASE/alphaAnalysis/macros/AutoPlots | head -1)
echo "$RESULT"
ELOG_SUBJECT="R${RUNNO} FRD:$RESULT "
ssh alpha@alphadaq "mkdir -p ~/alphaautomagic/${RUNNO}
exit
"
echo "waiting..."
wait $EVENTS_PID
echo "wait over"
#scp ~/AnalysisOutput/${RUNNO}/R${RUNNO}-FastRampDown.png ~/AnalysisOutput/${RUNNO}/R${RUNNO}_FastRampDown.log ~/AnalysisOutput/${RUNNO}/R${RUNNO}_SequenceQOD.log alpha@alphadaq:~/alphaautomagic/${RUNNO}/
#ssh -X alphadaq "~/packages/elog/elog -h localhost -a Author=$HOSTNAME -a Subject=\"$ELOG_SUBJECT [Processing still going]\" -e $ELOG_NO -m ~/alphaautomagic/${RUNNO}/R${RUNNO}_FastRampDown.log -f ~/alphaautomagic/${RUNNO}/R${RUNNO}-FastRampDown.png -f ~/alphaautomagic/${RUNNO}/R${RUNNO}_SequenceQOD.log -p 8080 -l AutoAnalysis  -v "
scp ~/AnalysisOutput/${RUNNO}/*.png ~/AnalysisOutput/${RUNNO}/*.log alpha@alphadaq:~/alphaautomagic/${RUNNO}/
FRDFILES=""
for i in `ls ~/AnalysisOutput/${RUNNO}/*`; do
  FRDFILES="$FRDFILES -f $i"
done
FRDFILES=${FRDFILES//'AnalysisOutput'/'alphaautomagic'}
ssh -X alphadaq "~/packages/elog/elog -h localhost -a Author=$HOSTNAME -a Subject=\"$ELOG_SUBJECT [Processing still going]\" -e $ELOG_NO -m ~/alphaautomagic/${RUNNO}/R${RUNNO}_FastRampDown.log ${FRDFILES} -p 8080 -l AutoAnalysis  -v " 

rm ${RELEASE}/alphaAnalysis/data/tree${RUNNO}FRD.root

fi




#Rush job done...
##################################################################
# Run Slower full analysis from here
##################################################################

echo ".L PlotDumps.C+
gProgressBars=kFALSE;
PrintSequenceQOD(${RUNNO})
.q
" | root -l -b #
cp AutoPlots/$(date +%Y%m%d)/R${RUNNO}_SequenceQOD.log ~/AnalysisOutput/${RUNNO}/R${RUNNO}_SequenceQOD.log
SequenceQOD=`grep Error ~/AnalysisOutput/${RUNNO}/R${RUNNO}_SequenceQOD.log`
ELOG_SUBJECT="$ELOG_SUBJECT $SequenceQOD"

echo "============================"
echo "alphaAnalysis dump list done"
echo "============================"


#Test for dumps... insert more if you want them here:
MIXING=`grep "Mixing"  ~/AnalysisOutput/${RUNNO}/R${RUNNO}_SequenceQOD.log | grep -v '\\\' `
QUENCH=`grep "Quench dump"  ~/AnalysisOutput/${RUNNO}/R${RUNNO}_SequenceQOD.log`
FRD=`grep "FRD"  ~/AnalysisOutput/${RUNNO}/R${RUNNO}_SequenceQOD.log | grep -v '\\\' `
FASTRAMPDOWN=`grep "FastRampDown"  ~/AnalysisOutput/${RUNNO}/R${RUNNO}_SequenceQOD.log | grep -v '\\\' `
PBAR=`grep "Pbar"  ~/AnalysisOutput/${RUNNO}/R${RUNNO}_SequenceQOD.log | grep -v '\\\' `
ANYDUMP=`grep "\." ~/AnalysisOutput/${RUNNO}/R${RUNNO}_SequenceQOD.log | grep -v SIS_AD | tail | grep -v '\\\' `
ALWAYSEMPTY=`grep "A string like me shouldnt exist..."  ~/AnalysisOutput/${RUNNO}/R${RUNNO}_SequenceQOD.log`
POSITRONS=`grep Positrons ~/AnalysisOutput/${RUNNO}/R${RUNNO}_SequenceQOD.log | grep -v '\\\' | tail `
LIFETIME=`grep Lifetime ~/AnalysisOutput/${RUNNO}/R${RUNNO}_SequenceQOD.log | grep -v '\\\' `
DD=`grep DD ~/AnalysisOutput/${RUNNO}/R${RUNNO}_SequenceQOD.log | grep -v '\\\' `
CC=`grep CC ~/AnalysisOutput/${RUNNO}/R${RUNNO}_SequenceQOD.log | grep -v '\\\' `
RCTHOT=`grep "RCT Hot" ~/AnalysisOutput/${RUNNO}/R${RUNNO}_SequenceQOD.log | tail`
LASER243=`grep 243nm ~/AnalysisOutput/${RUNNO}/R${RUNNO}_SequenceQOD.log | grep -v '\\\' `

echo "[code]" > ~/AnalysisOutput/R${RUNNO}.Elog.txt
sleep 0.1
cat AutoPlots/$(date +%Y%m%d)/R${RUNNO}_SequenceQOD.log >> ~/AnalysisOutput/R${RUNNO}.Elog.txt
sleep 0.1
echo "[/code]" >> ~/AnalysisOutput/R${RUNNO}.Elog.txt
#cp AutoPlots/$(date +%Y%m%d)/R${RUNNO}_SequenceQOD.log ~/AnalysisOutput/R${RUNNO}.Elog.txt
#cp ~/AnalysisOutput/${RUNNO}/R${RUNNO}_SequenceQOD.log ~/AnalysisOutput/R${RUNNO}.Elog.txt

#Clean no char strings
echo "MIXING:${MIXING}"
MIXING=${MIXING//[^a-zA-Z0-9]/}
echo "NMix"
NMix=$(( `cat ~/AnalysisOutput/${RUNNO}/R${RUNNO}_SequenceQOD.log | grep MIXING_FLAG | wc -l` / 2 )) 
#if [ $NMix -gt 1 ]; then
#  NMix=echo $(( ${NMix} / 2 ))
#fi
echo "NMix:$NMix"
echo "QUENCH:$QUENCH"
QUENCH=${QUENCH//[^a-zA-Z0-9]/}
echo "FRD:$FRD"
FRD=${FRD//[^a-zA-Z0-9]/}
echo "FASTRAMPDOWN:$FASTRAMPDOWN"
FASTRAMPDOWN=${FASTRAMPDOWN//[^a-zA-Z0-9]/}
echo "ANYDUMP:$ANYDUMP"
ANYDUMP=${ANYDUMP//[^a-zA-Z0-9]/}
echo "ALWAYSEMPTY:$ALWAYSEMPTY"
ALWAYSEMPTY=${ALWAYSEMPTY//[^a-zA-Z0-9]/}
echo "PBAR:$PBAR"
PBAR=${PBAR//[^a-zA-Z0-9]/}
echo "POSITRONS:$POSITRONS"
POSITRONS=${POSITRONS//[^a-zA-Z0-9]/}
echo "LIFETIME:$LIFETIME"
LIFETIME=${LIFETIME//[^a-zA-Z0-9]/}
echo "DD:$DD"
DD=${DD//[^a-zA-Z0-9]/}
echo "DD:$DD"
echo "CC:$CC"
CC=${CC//[^a-zA-Z0-9]/}
echo "CC:$CC"
RCTHOT=${RCTHOT//[^a-zA-Z0-9]/}
echo "RCTHOT:$RCTHOT"
LASER243=${LASER243//[^a-zA-Z0-9]/}

echo "LASER243:$LASER243"

#If there is evidence we have a trapping like run:

if [ ${#MIXING} -gt 3 ] && [ ${#FRD} -lt 3 ] && [ ${#QUENCH} -lt 3 ] && [ ${#FASTRAMPDOWN} -lt 3 ]; then
  ELOG_SUBJECT="MIXING $ELOG_SUBJECT"
fi

if [ ${#LIFETIME} -gt 3 ]; then
  ELOG_SUBJECT="LIFETIME QOD $ELOG_SUBJECT"
  cd ${RELEASE}/alphaAnalysis
  ./alphaAnalysis.exe --EOS midasdata/run${RUNNO}sub00000.mid.gz &> ~/AnalysisOutput/${RUNNO}/alphaAnalysis.R${RUNNO}.log
  cd ${RELEASE}/alphaAnalysis/macros
  echo "#include \"PlotDumps.C+\"
  Int_t MenuMacro${RUNNO}(Int_t runNumber=$RUNNO){
    if (IsDumpInATM(${RUNNO}, \"startDump\", \"Lifetime\") || IsDumpInRCT(${RUNNO}, \"startDump\", \"Lifetime\")) {
      gProgressBars=kFALSE;
      PrintDetectorQOD(${RUNNO},\"Lifetime\");
    }
    else {
      cout << \" Lifetime not in ATOM trap...  \" << endl; 
    }
    return 0;
  }">MenuMacro$RUNNO.C
   echo "[code]" > ~/AnalysisOutput/R${RUNNO}.Elog.txt
    #root -b -q -l MenuMacro$RUNNO.C >> ~/AnalysisOutput/R${RUNNO}.Elog.txt
  #root -l -b -q MenuMacro$RUNNO.C > ~/AnalysisOutput/${RUNNO}/R${RUNNO}_LifetimeQOD.log
  root -l -b -q MenuMacro${RUNNO}.C >> ~/AnalysisOutput/R${RUNNO}.Elog.txt
  echo "[/code]" >> ~/AnalysisOutput/R${RUNNO}.Elog.txt
  
  echo "#include \"PlotDumps.C+\"
  Int_t MenuMacro${RUNNO}(Int_t runNumber=$RUNNO){
    gc=PlotVertices(${RUNNO},\"Lifetime\");
    gc->SaveAs(\"~/AnalysisOutput/${RUNNO}/${RUNNO}_Lifetime.png\");
    return 0;
  }">MenuMacro$RUNNO.C
  root -l -b -q MenuMacro$RUNNO.C
  
  RCTHold=`echo ".L PlotDumps.C+
gProgressBars=kFALSE;
TrappingVacuumBaseline($RUNNO)
.q
" | root -b -l | grep "\["`
  ELOG_SUBJECT="$RCTHold $ELOG_SUBJECT"
  	#MVATREES=$TREEFILES
	#${RELEASE}/alphaMVA/randforest/dumper.exe $TREEFILES/tree${RUNNO}offline.root ALL &
elif [ -n "$MIXING" ]; then #|| [ -n "$QUENCH" ] || [ -n "$FASTRAMPDOWN" ]; then
echo "Mixing dump found... "
echo "============================"
echo "   Running full analysis"
echo "============================"

	cd ${RELEASE}/alphaAnalysis
	./alphaAnalysis.exe --EOS midasdata/run${RUNNO}sub00000.mid.gz &> ~/AnalysisOutput/${RUNNO}/alphaAnalysis.R${RUNNO}.log
	cd ${RELEASE}/alphaAnalysis/macros
	
	echo ".L PlotDumps.C+
AntiDumpCosmicBaseline(${RUNNO}, 1000.)
.q
"| root -l -b > ~/AnalysisOutput/${RUNNO}/R${RUNNO}_AntiDumpQOD.log
	
	cp ~/AnalysisOutput/${RUNNO}/alphaAnalysis.R${RUNNO}.log ~/AnalysisOutput/R${RUNNO}.Elog.txt

echo ".L PlotDumps.C+
PrintDetectorQOD($RUNNO,\"Mixing\")
.q
" | root -l -b > ~/AnalysisOutput/${RUNNO}/R${RUNNO}_MixingQOD.log
	
	
	if [ $NMix -gt 1 ]; then
	for ExtraMix in `seq 2 $NMix` ; do
echo "#include \"PlotDumps.C+\"
	Int_t MenuMacro${RUNNO}(Int_t runNumber=$RUNNO){
	gProgressBars=kFALSE;
	PrintDetectorQOD($RUNNO,\"Mixing\",${ExtraMix});
	return 0;
	}">MenuMacro$RUNNO.C
	
	root -l -b -q MenuMacro$RUNNO.C > R${RUNNO}_MixingQOD${ExtraMix}.log
		
	if [ `cat R${RUNNO}_MixingQOD${ExtraMix}.log | wc -l` -gt 5 ]; then
		mv R${RUNNO}_MixingQOD${ExtraMix}.log  ~/AnalysisOutput/${RUNNO}/R${RUNNO}_MixingQOD${ExtraMix}.log
	fi
	
		
		echo "#include \"PlotDumps.C+\"
Int_t MenuMacro${RUNNO}(Int_t runNumber=$RUNNO){

	TCanvas* canvas =  PlotMixingFlagVertices(runNumber,${ExtraMix});
	TString savFolder=\"~/AnalysisOutput/${RUNNO}\";

	TString MixingName(savFolder);
	MixingName+=(\"/R\");
	MixingName+=${RUNNO};
	MixingName+=(\"-Mixing${ExtraMix}.png\");
	canvas->SaveAs(MixingName);
	return 0;
}">MenuMacro$RUNNO.C
	root -l -b -q MenuMacro$RUNNO.C 
	
	
	done
	
	fi
	
		echo "#include \"PlotDumps.C+\"
Int_t MenuMacro${RUNNO}(Int_t runNumber=$RUNNO){

	TCanvas* canvas =  PlotMixingFlagVertices(runNumber);
	TString savFolder=\"~/AnalysisOutput/${RUNNO}\";

	TString MixingName(savFolder);
	MixingName+=(\"/R\");
	MixingName+=${RUNNO};
	MixingName+=(\"-Mixing.png\");
	canvas->SaveAs(MixingName);
	return 0;
}">MenuMacro$RUNNO.C
	root -l -b -q MenuMacro$RUNNO.C 
MixingQOD=`grep ERROR Warning ~/AnalysisOutput/${RUNNO}/R${RUNNO}_MixingQOD.log`
ELOG_SUBJECT="$ELOG_SUBJECT $MixingQOD"

RCTHold=`echo ".L PlotDumps.C+
	gProgressBars=kFALSE;
	TrappingVacuumBaseline($RUNNO)
	.q
	" | root -b -l | grep "\["`
ELOG_SUBJECT="$RCTHold $ELOG_SUBJECT"
elif [ -n "$RCTHOT" ]; then
echo "RCT Hold dump found... "
echo "============================"
echo "   Running full analysis"
echo "============================"

	cd ${RELEASE}/alphaAnalysis
	./alphaAnalysis.exe --EOS midasdata/run${RUNNO}sub00000.mid.gz &> ~/AnalysisOutput/${RUNNO}/alphaAnalysis.R${RUNNO}.log
	#MVATREES=$TREEFILES
	#${RELEASE}/alphaMVA/randforest/dumper.exe $TREEFILES/tree${RUNNO}offline.root ALL &
	#eos cp ${TREEFILES}/tree${RUNNO}offline.root /eos/ad/alpha/alphaTrees/ &
	cd ${RELEASE}/alphaAnalysis/macros
	echo ".L PlotDumps.C+
 AntiDumpCosmicBaseline(${RUNNO}, 1000.)
 .q
 "| root -l -b > ~/AnalysisOutput/${RUNNO}/R${RUNNO}_AntiDumpQOD.log
RCTHold=`echo ".L PlotDumps.C+
	gProgressBars=kFALSE;
	TrappingVacuumBaseline($RUNNO)
	.q
	" | root -b -l | grep "\["`
ELOG_SUBJECT="$RCTHold $ELOG_SUBJECT"
fi




#If there is a fast ramp down dump...
if [ -n "$FASTRAMPDOWN" ] && [ -n "$MIXING" ]; then
echo "FastRampDown dump found..."
cd ${RELEASE}/alphaAnalysis/macros
#	echo "#include \"PlotDumps_C.so\"

#	TCanvas* canvas =  PlotMixingFlagVertices(runNumber);
#//Int_t MenuMacro${RUNNO}(Int_t runNumber=$RUNNO){
#//	TString MixingName(savFolder);
#//	MixingName+=(\"/R\");
#//	MixingName+=${RUNNO};
#//	MixingName+=(\"-Mixing.png\");
#//	canvas->SaveAs(MixingName);


	echo ".L PlotDumps.C+
TString savFolder=\"~/AnalysisOutput/${RUNNO}\";
Int_t runNumber=${RUNNO};
TCanvas* canvas2 =  PlotQuenchFlagVertices(runNumber);
TString FRDName=\"~/AnalysisOutput/${RUNNO}\";
FRDName+=(\"/R\");
FRDName+=${RUNNO};
FRDName+=(\"-FastRampDown.png\");
canvas2->SaveAs(FRDName);
.q
 "| root -l -b # &> ~/AnalysisOutput/${RUNNO}/R${RUNNO}_FastRampDown.log
	echo ".L PlotDumps.C+
TString savFolder=\"~/AnalysisOutput/${RUNNO}\";
Int_t runNumber=${RUNNO}; 
Double_t start=MatchEventToTime(runNumber,\"startDump\",\"FastRampDown\",1,0);
Double_t stop=MatchEventToTime(runNumber,\"stopDump\",\"FastRampDown\",1,0);
if ((stop-start)<10) {
  PrintTrappingReportForExcel(runNumber,PrintQuenchGateWindow($RUNNO));
}else { cout << \"FastRampDown abnormally long (\" << stop-start << \"s), skipping listing Si Events\"<< endl;} 
.q
" | root -l -b  &>> ~/AnalysisOutput/${RUNNO}/R${RUNNO}_FastRampDown.log
	echo ".L PlotDumps.C+
PrintAnalysisReport($RUNNO);
.q
" | root -l -b  &>> ~/AnalysisOutput/${RUNNO}/R${RUNNO}_FastRampDown.log
  cp ~/AnalysisOutput/${RUNNO}/R${RUNNO}_FastRampDown.log ~/AnalysisOutput/R${RUNNO}.Elog.txt

  echo ".L PlotDumps.C+
gAnnounce=kFALSE;
gProgressBars=kFALSE;
PrintDetectorQOD(${RUNNO},\"FastRampDown\");
.q
" | root -b -l 

fi
#If there is a fast ramp down dump...
if [ -n "$QUENCH" ] && [ -n "$MIXING" ]; then
echo "Quench dump found"
cd ${RELEASE}/alphaAnalysis/macros

	echo "#include \"PlotDumps.C+\"
Int_t MenuMacro${RUNNO}(Int_t runNumber=$RUNNO){

TCanvas* canvas =  PlotMixingFlagVertices(runNumber);
TString savFolder=\"~/AnalysisOutput/${RUNNO}\";

TString MixingName(savFolder);
MixingName+=(\"/R\");
MixingName+=${RUNNO};
MixingName+=(\"-Mixing.png\");
canvas->SaveAs(MixingName);

TCanvas* canvas2 =  PlotQuenchFlagVertices(runNumber);
TString FRDName=\"~/AnalysisOutput/${RUNNO}\";
FRDName+=(\"/R\");
FRDName+=${RUNNO};
FRDName+=(\"-Quench.png\");
canvas2->SaveAs(FRDName);


PrintAnalysisReport($RUNNO);
PrintQuenchGateWindow($RUNNO);
return 0;
}">MenuMacro$RUNNO.C
	root -l -b -q MenuMacro$RUNNO.C > ~/AnalysisOutput/${RUNNO}/R${RUNNO}_Quench.log

RESULT=`grep "\[" ~/AnalysisOutput/${RUNNO}/R${RUNNO}_Quench.log | grep "\]"`
echo "$RESULT"
ELOG_SUBJECT="R${RUNNO} Quench:$RESULT $ELOG_SUBJECT"

fi


#243 experiment

if [ ${#LASER243} -gt 3 ]; then
  echo "Running 243 baselines"
  echo ".L PlotDumps.C+
  //gc=Plot243powerInDump(${RUNNO}, \"243nm\", 1, 0);
  gc=Plot243PowerAndRMS(${RUNNO});
  gc->SaveAs(\"~/AnalysisOutput/${RUNNO}/R${RUNNO}-243PowerAndRMS.png\");
.q
" | root -l -b 
fi

if [ ${#DD} -gt 3 ] && [ ${#CC} -gt 3 ]; then

  cd $RELEALSE/alphaAnalysis/macros/
  echo "#include \"PlotDumps.C+\"
  Int_t MenuMacro${RUNNO}(Int_t runNumber=$RUNNO){
	Double_t DDstart=MatchEventToTime(runNumber,\"startDump\",\"DD\",1,0);
	Double_t CCstart=MatchEventToTime(runNumber,\"startDump\",\"DD\",1,0);
	Double_t DDstop=MatchEventToTime(runNumber,\"stopDump\",\"CC\",1,0);
	Double_t CCstop=MatchEventToTime(runNumber,\"stopDump\",\"CC\",1,0);
	if (DDstart>0 && DDstop>0 && CCstart>0 && CCstop>0 ) {
	cout <<endl<<endl<<endl;
	  PrintLaserReportForExcel(runNumber);
	}
	return 0;
}">MenuMacro$RUNNO.C
	root -l -b -q MenuMacro$RUNNO.C | grep -A5 TABLE >> ~/AnalysisOutput/R${RUNNO}.Elog.txt


 #PrintLaserReportForExcel(45475)
  echo "#include \"PlotDumps.C+\"
  Int_t MenuMacro${RUNNO}(Int_t runNumber=$RUNNO){
	gc=Plot243frequencyInDump(runNumber,\"CC\",1,0);
	SaveCanvas(${RUNNO},\"243Freq-CC\");
	gc=Plot243frequencyInDump(runNumber,\"DD\",1,0);    
	SaveCanvas(${RUNNO},\"243Freq-DD\");
	gc=Plot243PowerAndRMS(runNumber);
    gc->SaveAs(\"~/AnalysisOutput/${RUNNO}/${RUNNO}-243PowerAndRMS.png\");
    return 0;
  }
  ">MenuMacro$RUNNO.C
  root -l -b -q MenuMacro$RUNNO.C
  #montage AutoPlots/$(date +%Y%m%d)/R${RUNNO}-HCor.png\
  AutoPlots/$(date +%Y%m%d)/R${RUNNO}-HDir.png\
  AutoPlots/$(date +%Y%m%d)/R${RUNNO}-HTexp.png\
  -tile 3x1 -geometry +0+0 ~/AnalysisOutput/R${RUNNO}-CosmicUtils.png
 
  convert +append AutoPlots/$(date +%Y%m%d)/R${RUNNO}-243Freq-CC.png\
  AutoPlots/$(date +%Y%m%d)/R${RUNNO}-243Freq-DD.png\
  ~/AnalysisOutput/${RUNNO}/R${RUNNO}-243Frequencies.png
  
  
fi


#if ( [ -z "$ANYDUMP" ] || ( [ -z "$PBAR" ] && [ -z "$POSITRONS" ] ) ) ; then
if  [ ${#PBAR} -lt 3 ] && [ ${#POSITRONS} -lt 3 ] && [ ${#MIXING} -lt 3 ]; then
  echo "No dumps mentioning Pbar, Positron or Mixing in run... Assuming its a cosmic run... "
  if [ ${#ANYDUMP} -lt 3 ]; then
    echo "No dumps at all... we can be patient... waiting for alphaStrips to finish"
    #if [ `ps -p ${STRIPS_PID} | grep alphaStrip | wc -c` -gt 3 ]; then
    #  echo "Waiting for pid: $STRIPS_PID"
      wait $STRIPS_PID
    #fi
    export STRIPFILES=${RELEASE}/alphaStrips/data/${RUNNO}/
  fi
  cd ${RELEASE}/alphaAnalysis
  ./alphaAnalysis.exe --EOS --adcspecs midasdata/run${RUNNO}sub00000.mid.gz &> ~/AnalysisOutput/${RUNNO}/alphaAnalysis.R${RUNNO}.log
  #MVATREES=$TREEFILES
  #${RELEASE}/alphaMVA/randforest/dumper.exe $TREEFILES/tree${RUNNO}offline.root COSMICS &
  #eos cp ${TREEFILES}/tree${RUNNO}offline.root /eos/ad/alpha/alphaTrees/ &
  ./alphaAnalysis.exe --EOS --iscosmic --cosmicutil midasdata/run${RUNNO}sub00000.mid.gz &> ~/AnalysisOutput/${RUNNO}/alphaAnalysis.Cosmic.R${RUNNO}.log
  #eos cp ${TREEFILES}/tree${RUNNO}_IsCosmic_CosmicUtiloffline.root /eos/ad/alpha/alphaTrees/ &
  cd ${RELEASE}/alphaAnalysis/macros
  cp ~/AnalysisOutput/${RUNNO}/alphaAnalysis.R${RUNNO}.log ~/AnalysisOutput/R${RUNNO}.Elog.txt

  #Check for any detector triggers
  echo "#include \"PlotDumps.C+\"
  Int_t MenuMacro${RUNNO}(Int_t runNumber=$RUNNO){
  TAnalysisReport* gAnalysisReport= GetAnalysisReport(runNumber);
  if (gAnalysisReport->GetTotalVF48Events()<=0)
    cout <<\"No VF48 Events in run! Si detector off?\"<<endl;
    return 0;
  }
  ">MenuMacro$RUNNO.C
  DETOFF=`root -l -b -q MenuMacro$RUNNO.C | grep "No VF48 Events in run"`

  if [ ${#DETOFF} -gt 3 ]; then
	echo "No VF48 Events in run"
	ELOG_SUBJECT="Si Detector off? (no VF48 events)"
  else
    SequenceQOD=`grep Error ~/AnalysisOutput/${RUNNO}/R${RUNNO}_SequenceQOD.log`
	
	echo "Pipe out PrintDetectorQOD"

   echo "[code]" > ~/AnalysisOutput/R${RUNNO}.Elog.txt

	echo ".L PlotDumps.C+
  gErrorIgnoreLevel = kWarning;
  Int_t runNumber=${RUNNO};
  gProgressBars=kFALSE;
  if (GetTotalRunTime(${RUNNO})<100) {
    cout<<\"Total time: \"<<GetTotalRunTime(${RUNNO})<<\"s\"<< endl;
  } else {
      PrintDetectorQOD(${RUNNO});
  }
  .q
"|  root -b -l &>> ~/AnalysisOutput/R${RUNNO}.Elog.txt
    echo "[/code]" >> ~/AnalysisOutput/R${RUNNO}.Elog.txt
 	 
 	RUNTIME=`grep "Total time"  ~/AnalysisOutput/R${RUNNO}.Elog.txt | sed 's/[^0-9.]//g'`
    
    if [ ${RUNTIME%.*} -gt 100 ]; then
      RUNLENGTH="SHORT"
 	fi
    if [ ${RUNTIME%.*} -gt 500 ]; then
      RUNLENGTH="MIDLENGTH"
 	fi
    if [ ${RUNTIME%.*} -gt 3600 ]; then
      RUNLENGTH="LONG"
 	fi
 	if [ ${RUNTIME%.*} -eq 1 ]; then
 	  ELOG_SUBJECT="WARNING: SIS timing bug! Check LNE! Check Clocks!"
 	fi
	if [ ${RUNTIME%.*} -lt 100 ] && [ ${RUNTIME%.*} -gt 1 ]; then
      ELOG_SUBJECT="Cosmic run shorter than 100s, (${RUNTIME%.*}) QOD skipped "
    else
	  if [ ${#ANYDUMP} -lt 3 ]; then
		#No sequence activity
	    TAG="DedicatedCosmic"
	    ELOG_SUBJECT="Dedicated $RUNLENGTH Cosmic Run $ELOG_SUBJECT"
	  else
		ELOG_SUBJECT="Cosmic QOD $RUNLENGTH $ELOG_SUBJECT; $SequenceQOD"
	  fi
      #Attachment 1
      echo "Saving R$RUNNO-ModuleOccupancy.png"
      echo "#include \"PlotDumps.C+\"
      Int_t MenuMacro${RUNNO}(Int_t runNumber=$RUNNO){
        gc= PerModuleOccupancy(${RUNNO});
        gc->SaveAs(\"~/AnalysisOutput/${RUNNO}/${RUNNO}-ModuleOccupancy.png\");
        return 0;
      }
      ">MenuMacro$RUNNO.C
	  root -l -b -q MenuMacro$RUNNO.C
	  
	  
	  #Attachment 2
      echo "Plotting R$RUNNO-MultiplicityQOD.png"
	  echo "#include \"PlotDumps.C+\"
	  Int_t MenuMacro${RUNNO}(Int_t runNumber=$RUNNO){
	    gc=PlotGeneralMultiplicityQOD(runNumber);
	    gc->SaveAs(\"~/AnalysisOutput/${RUNNO}/${RUNNO}-MultiplicityQOD.png\");
	    return 0;
	  }
	  ">MenuMacro$RUNNO.C
	  root -l -b -q MenuMacro$RUNNO.C

      #Attachment 3
	  echo "Plotting R${RUNNO}-OccupancyVsTime.png"
	  echo "#include \"PlotDumps.C+\"
	  Int_t MenuMacro${RUNNO}(Int_t runNumber=$RUNNO){
	    Int_t nBinsOld=gNbin;
	    gc=PlotOccupancyAgainstTime_fixedBinWidth($RUNNO,0.,-1,10);
	    gc->SaveAs(\"~/AnalysisOutput/${RUNNO}/${RUNNO}-OccupancyVsTime.png\");
	    gNbin=nBinsOld;
	    return 0;
	  }
	  ">MenuMacro$RUNNO.C
	  root -l -b -q MenuMacro$RUNNO.C
	  
	  if [ ${#ANYDUMP} -lt 3 ]; then
	  #Attachment 4
	  echo "Plotting R${RUNNO}-PMT_timing_and_status.png"
	  echo "#include \"PlotDumps.C+\"
	  Int_t MenuMacro${RUNNO}(Int_t runNumber=$RUNNO){
		gc=PlotPMTTimings($RUNNO);
		gc->SaveAs(\"~/AnalysisOutput/${RUNNO}/${RUNNO}-PMT_timing_and_status.png\");
		return 0;
 	  }
	  ">MenuMacro$RUNNO.C
	  root -l -b -q MenuMacro$RUNNO.C

	  fi
	 #Attachment 5
	   echo "#include \"PlotDumps.C+\"
      Int_t MenuMacro${RUNNO}(Int_t runNumber=$RUNNO){
        gc= PlotSiDetectorTimings(${RUNNO});
        gc->SaveAs(\"~/AnalysisOutput/${RUNNO}/${RUNNO}-SiDetectorTimings.png\");
        return 0;
      }
      ">MenuMacro$RUNNO.C
	  root -l -b -q MenuMacro$RUNNO.C
	  

	  #Attachment 6
      echo "Saving Vertex plot (without cuts)"
	  echo "#include \"PlotDumps.C+\"
	  Int_t MenuMacro${RUNNO}(Int_t runNumber=$RUNNO){
        gApplyCuts=0;
	    gc= PlotVertices($RUNNO);
	    gc->SaveAs(\"~/AnalysisOutput/${RUNNO}/${RUNNO}-Vertices_NoCuts.png\");
	    return 0;
	  }
	  ">MenuMacro$RUNNO.C
	  root -l -b -q MenuMacro$RUNNO.C

	  #Attachment 7
      echo "Saving Vertex plot (with cuts applied)"
	  echo "#include \"PlotDumps.C+\"
	  Int_t MenuMacro${RUNNO}(Int_t runNumber=$RUNNO){
	    gApplyCuts=1;
	    gc= PlotVertices($RUNNO);
	    gc->SaveAs(\"~/AnalysisOutput/${RUNNO}/${RUNNO}-Vertices_WithCuts.png\");
	    return 0;
	  }
	  ">MenuMacro$RUNNO.C
	  root -l -b -q MenuMacro$RUNNO.C

	  #Attachment 8
	  echo "Saving CosmicUtils Plots"
	  echo "#include \"PlotDumps.C+\"
	  Int_t MenuMacro${RUNNO}(Int_t runNumber=$RUNNO){
	    SaveCosmicUtilsPlots(${RUNNO});
	    return 0;
	  }
	  ">MenuMacro$RUNNO.C
	  root -l -b -q MenuMacro$RUNNO.C
	  #montage AutoPlots/$(date +%Y%m%d)/R${RUNNO}-HCor.png\
	  AutoPlots/$(date +%Y%m%d)/R${RUNNO}-HDir.png\
	  AutoPlots/$(date +%Y%m%d)/R${RUNNO}-HTexp.png\
	  -tile 3x1 -geometry +0+0 ~/AnalysisOutput/R${RUNNO}-CosmicUtils.png
	 
      convert +append AutoPlots/$(date +%Y%m%d)/R${RUNNO}-HCor.png\
	  AutoPlots/$(date +%Y%m%d)/R${RUNNO}-HDir.png\
	  AutoPlots/$(date +%Y%m%d)/R${RUNNO}-HTexp.png\
	  ~/AnalysisOutput/${RUNNO}/R${RUNNO}-CosmicUtils.png
	 
	  #Attachment 9
	  echo "Plotting R${RUNNO}-Occupancy.png"
	  echo "#include \"PlotDumps.C+\"
	  Int_t MenuMacro${RUNNO}(Int_t runNumber=$RUNNO){
	    SavePerStripOccupancy(${RUNNO},\"R${RUNNO}-Occupancy\");
	    return 0;
	  }
	  ">MenuMacro$RUNNO.C
	  root -l -b -q MenuMacro$RUNNO.C
      cp AutoPlots/$(date +%Y%m%d)/R${RUNNO}-Occupancy.png ~/AnalysisOutput/${RUNNO}/

      


	  #Attachment 10?)
	  echo "Plotting R${RUNNO}-StripMeanVsRMS.png"
	  echo "#include \"PlotDumps.C+\"
	  Int_t MenuMacro${RUNNO}(Int_t runNumber=$RUNNO){
	    SaveStripMeanCompareHistogramsSummary(${RUNNO},\"R${RUNNO}-StripMeanVsRMS\");
	    //cout <<\"Now paste the following into the elog: \"<< endl<< endl<< endl;
	    //PrintDetectorQOD(${RUNNO})
	    return 0;
	  }
	  ">MenuMacro$RUNNO.C
	  root -l -b -q MenuMacro$RUNNO.C
	  cp AutoPlots/$(date +%Y%m%d)/R${RUNNO}-StripMeanVsRMS.png ~/AnalysisOutput/${RUNNO}/

      #SiQOD=`grep Warning: ~/AnalysisOutput/${RUNNO}/R${RUNNO}cosmicAnalysis.log`
	  SiQOD=`grep Warning: ~/AnalysisOutput/R${RUNNO}.Elog.txt`
	  ELOG_SUBJECT="$ELOG_SUBJECT $SiQOD"
	  #SiQOD=`grep ALARM: ~/AnalysisOutput/${RUNNO}/R${RUNNO}cosmicAnalysis.log`
	  SiQOD=`grep ALARM: ~/AnalysisOutput/R${RUNNO}.Elog.txt`
	  ELOG_SUBJECT="$ELOG_SUBJECT $SiQOD"
	  SiQOD=`grep ERROR: ~/AnalysisOutput/R${RUNNO}.Elog.txt`
	  ELOG_SUBJECT="$ELOG_SUBJECT $SiQOD"
	  #cp ~/AnalysisOutput/${RUNNO}/R${RUNNO}cosmicAnalysis.log ~/AnalysisOutput/R${RUNNO}.Elog.txt
    
    fi
  fi 
else
echo "Antidump QOD given up on"
  #if [ ${#MIXING} -lt 3 ]; then
	#echo "Attempting QOD outside of dump markers.. "
    ##Check for any detector triggers
    #echo "#include \"PlotDumps.C+\"
  #Int_t MenuMacro${RUNNO}(Int_t runNumber=$RUNNO){
  #TAnalysisReport* gAnalysisReport= GetAnalysisReport(runNumber);
  #if (gAnalysisReport->GetTotalVF48Events()==0)
    #cout <<\"No VF48 Events in run! Si detector off?\"<<endl;
  #}
  #">MenuMacro$RUNNO.C
    #DETOFF=`root -l -b -q MenuMacro$RUNNO.C | grep "No VF48 Events in run"`
    #if [ ${#DETOFF} -gt 3 ]; then
	  #echo "No VF48 Events in run"
	  #ELOG_SUBJECT="Si Detector off? (no VF48 events)"
    #else
      #SequenceQOD=`grep Error ~/AnalysisOutput/${RUNNO}/R${RUNNO}_SequenceQOD.log`
    #wait
    #cd ${RELEASE}/alphaAnalysis
    #./alphaAnalysis.exe --EOS midasdata/run${RUNNO}sub00000.mid.gz
    #cd ${RELEASE}/alphaAnalysis/macros
	#echo "Pipe out PrintDetectorQOD"
	#echo "#include \"PlotDumps.C+\"
    #gErrorIgnoreLevel = kWarning;
    #Int_t MenuMacro${RUNNO}(Int_t runNumber=$RUNNO){
    #gProgressBars=kFALSE;
    #if (GetTotalRunTime(${RUNNO})<100) {
	  #cout<<\"Total time: \"<<GetTotalRunTime(${RUNNO})<<\"s\"<< endl;
	#}
	#else {
	#Double_t tmin,tmax;
	#AntiSeqSingleWindow(${RUNNO},tmin,tmax);
	#if ((tmax-tmin)>1000)
      #PrintDetectorQOD(${RUNNO},tmin,tmax,\"AntiDump\");
    #}
    #}
	#">MenuMacro$RUNNO.C
    ##root -b -q -l MenuMacro$RUNNO.C > ~/AnalysisOutput/${RUNNO}/R${RUNNO}cosmicAnalysis.log
    #echo "[code]" > ~/AnalysisOutput/R${RUNNO}.Elog.txt
    #root -b -q -l MenuMacro$RUNNO.C >> ~/AnalysisOutput/R${RUNNO}.Elog.txt
    #echo "[/code]" >> ~/AnalysisOutput/R${RUNNO}.Elog.txt
 	#RUNTIME=`grep "Total time"  ~/AnalysisOutput/R${RUNNO}.Elog.txt | sed 's/[^0-9.]//g'`
    #if [ ${RUNTIME%.*} -gt 100 ]; then
      #RUNLENGTH="SHORT"
 	#fi
    #if [ ${RUNTIME%.*} -gt 500 ]; then
      #RUNLENGTH="MIDLENGTH"
    #fi
    #if [ ${RUNTIME%.*} -gt 1000 ]; then
       #ELOG_SUBJECT="$ELOG_SUBJECT AntiDumpQOD "
  	#fi
 	#fi
    #if [ ${RUNTIME%.*} -gt 3600 ]; then
      #RUNLENGTH="LONG"
 	#fi
  #NOTHING=123
       ##Attachment 1
      #echo "Saving R$RUNNO-ModuleOccupancy.png"
      #echo "#include \"PlotDumps.C+\"
      #Int_t MenuMacro${RUNNO}(Int_t runNumber=$RUNNO){
      #Double_t tmin,tmax;
	#AntiSeqSingleWindow(${RUNNO},tmin,tmax);
	#if ((tmax-tmin)>1000){
        #gc= PerModuleOccupancy(${RUNNO},tmin,tmax);
        #gc->SaveAs(\"~/AnalysisOutput/${RUNNO}/${RUNNO}-ModuleOccupancy.png\");}
      #}
      #">MenuMacro$RUNNO.C
	  #root -l -b -q MenuMacro$RUNNO.C 
	  ##Attachment 2
      #echo "Plotting R$RUNNO-MultiplicityQOD.png"
	  #echo "#include \"PlotDumps.C+\"
	  #Int_t MenuMacro${RUNNO}(Int_t runNumber=$RUNNO){
	  #Double_t tmin,tmax;
	#AntiSeqSingleWindow(${RUNNO},tmin,tmax);
	#if ((tmax-tmin)>1000){
	    #gc=PlotGeneralMultiplicityQOD(runNumber,tmin,tmax);
	    #gc->SaveAs(\"~/AnalysisOutput/${RUNNO}/${RUNNO}-MultiplicityQOD.png\");}
	  #}
	  #">MenuMacro$RUNNO.C
	  #root -l -b -q MenuMacro$RUNNO.C
      ##Attachment 3
	  #echo "Plotting R${RUNNO}-OccupancyVsTime.png"
	  #echo "#include \"PlotDumps.C+\"
	  #Int_t MenuMacro${RUNNO}(Int_t runNumber=$RUNNO){
	    #Int_t nBinsOld=gNbin;
	    #Double_t tmin,tmax;
	#AntiSeqSingleWindow(${RUNNO},tmin,tmax);
	#if ((tmax-tmin)>1000){
	    #gc=PlotOccupancyAgainstTime_fixedBinWidth($RUNNO,tmin,tmax,10);
	    #gc->SaveAs(\"~/AnalysisOutput/${RUNNO}/${RUNNO}-OccupancyVsTime.png\");}
	    #gNbin=nBinsOld;
	  #}
	  #">MenuMacro$RUNNO.C
	  #root -l -b -q MenuMacro$RUNNO.C
	 ##Attachment 5
	   #echo "#include \"PlotDumps.C+\"
      #Int_t MenuMacro${RUNNO}(Int_t runNumber=$RUNNO){
      	  #Double_t tmin,tmax;
	    #AntiSeqSingleWindow(${RUNNO},tmin,tmax);
	    #if ((tmax-tmin)>1000){
        #gc= PlotSiDetectorTimings(${RUNNO},tmin,tmax);
        #gc->SaveAs(\"~/AnalysisOutput/${RUNNO}/${RUNNO}-SiDetectorTimings.png\");}
      #}
      #">MenuMacro$RUNNO.C
	  #root -l -b -q MenuMacro$RUNNO.C
	  ##Attachment 6
      #echo "Saving Vertex plot (without cuts)"
	  #echo "#include \"PlotDumps.C+\"
	  #Int_t MenuMacro${RUNNO}(Int_t runNumber=$RUNNO){
        #gApplyCuts=0;
        #Double_t tmin,tmax;
	#AntiSeqSingleWindow(${RUNNO},tmin,tmax);
	#if ((tmax-tmin)>1000){
	    #gc= PlotVertices($RUNNO,tmin,tmax);
	    #gc->SaveAs(\"~/AnalysisOutput/${RUNNO}/${RUNNO}-Vertices_NoCuts.png\");}
	  #}
	  #">MenuMacro$RUNNO.C
	  #root -l -b -q MenuMacro$RUNNO.C
	  ##Attachment 7
      #echo "Saving Vertex plot (with cuts applied)"
	  #echo "#include \"PlotDumps.C+\"
	  #Int_t MenuMacro${RUNNO}(Int_t runNumber=$RUNNO){
	    #gApplyCuts=1;
	    #Double_t tmin,tmax;
	#AntiSeqSingleWindow(${RUNNO},tmin,tmax);
	#if ((tmax-tmin)>1000){
	    #gc= PlotVertices($RUNNO,tmin,tmax);
	    #gc->SaveAs(\"~/AnalysisOutput/${RUNNO}/${RUNNO}-Vertices_WithCuts.png\");}
	  #}
	  #">MenuMacro$RUNNO.C
	  #root -l -b -q MenuMacro$RUNNO.C
    #fi
  #fi
fi


ELOG_SIZE=$(wc -c <~/AnalysisOutput/R${RUNNO}.Elog.txt)
if [ $ELOG_SIZE -ge 25000 ]; then
  mv  ~/AnalysisOutput/R${RUNNO}.Elog.txt  ~/AnalysisOutput/${RUNNO}/Big.Elog.txt
  echo "Elog message too big, see attachment
  
  Tail of attachment below:
  "> ~/AnalysisOutput/R${RUNNO}.Elog.txt
  cat ~/AnalysisOutput/${RUNNO}/Big.Elog.txt | tail -n 30 >> ~/AnalysisOutput/R${RUNNO}.Elog.txt
  cd $RELEALSE/alphaAnalysis/macros/
#	echo ".L PlotDumps.C+
#Int_t runNumber=${RUNNO};
#Double_t start=MatchEventToTime(runNumber,\"startDump\",\"FastRampDown\",1,0);
#Double_t stop=MatchEventToTime(runNumber,\"stopDump\",\"FastRampDown\",1,0);
#if ((stop-start)<10 && start>0 && stop>0) {
#  PrintTrappingReportForExcel(runNumber);
#}
#else { cout << \"FastRampDown abnormally long (\" << stop-start << \"s), skipping listing Si Events\"<< endl;} 
#.q
#"| root -l -b   >> ~/AnalysisOutput/R${RUNNO}.Elog.txt
  cd ~/
fi

echo "Copying files to alphadaq"
ssh alpha@alphadaq "mkdir ~/alphaautomagic/${RUNNO}
exit
"
scp ~/AnalysisOutput/${RUNNO}/* alpha@alphadaq:~/alphaautomagic/${RUNNO}/
echo "updating elog"
NFILES=0
for i in `ls ~/AnalysisOutput/${RUNNO}/*`; do
  let NFILES=$NFILES+1 
  if [ $NFILES -lt 49 ]; then
    FILES="$FILES -f $i"
  else
    if [ $NFILES -lt 99 ]; then
        REPLY1="$REPLY1 -f $i"
    else
      if [ $NFILES -gt 149 ]; then
        REPLY2="$REPLY2 -f $i"
      else
        REPLY3="$REPLY3 -f $i"
      fi
    fi
  fi
done
FILES=${FILES//'AnalysisOutput'/'alphaautomagic'}

echo "Files:
$FILES"
scp ~/AnalysisOutput/R${RUNNO}.Elog.txt alpha@alphadaq:~/alphaautomagic/
ELOG_SUBJECT=$(echo $ELOG_SUBJECT | tr '\n' '  ' )
echo "ELOG SUBJECT:
$ELOG_SUBJECT" >  ~/AnalysisOutput/R${RUNNO}.Elog_upload.log
echo "ELOG TXT:"
cat ~/AnalysisOutput/R${RUNNO}.Elog.txt
echo "

end of elog text"
echo "
ssh -X alphadaq \"~/packages/elog/elog -h localhost -a Author=$HOSTNAME -a Run=\\\"$RUNNO\\\" -a Tags=\\\"$TAG\\\" -a Subject=\\\"$ELOG_SUBJECT\\\" -m ~/alphaautomagic/R${RUNNO}.Elog.txt -e $ELOG_NO $FILES -p 8080 -l AutoAnalysis  -v " >> ~/AnalysisOutput/R${RUNNO}.Elog_upload.log

ssh -X alphadaq "~/packages/elog/elog -h localhost -a Author=$HOSTNAME -a Run=\"$RUNNO\" -a Tags=\"$TAG\" -a Subject=\"$ELOG_SUBJECT\" -m ~/alphaautomagic/R${RUNNO}.Elog.txt -e $ELOG_NO $FILES -p 8080 -l AutoAnalysis  -v " &>> ~/AnalysisOutput/R${RUNNO}.Elog_upload.log

if [ ${#REPLY1} -gt 3 ]; then
echo "First reply needed for all attachements" >>  ~/AnalysisOutput/R${RUNNO}.Elog_upload.log
echo "
ssh -X alphadaq \"~/packages/elog/elog -h localhost -a Author=$HOSTNAME -a Run=\\\"$RUNNO\\\" -a Tags=\\\"$TAG\\\" -a Subject=\\\"$ELOG_SUBJECT\\\"  -r $ELOG_NO $REPLY1 -p 8080 -l AutoAnalysis  -v " >> ~/AnalysisOutput/R${RUNNO}.Elog_upload.log
ssh -X alphadaq "~/packages/elog/elog -h localhost -a Author=$HOSTNAME -a Run=\"$RUNNO\" -a Tags=\"$TAG\" -a Subject=\"$ELOG_SUBJECT\" -m \"Additional attachments 1\" -e $ELOG_NO $REPLY1 -p 8080 -l AutoAnalysis  -v " &>> ~/AnalysisOutput/R${RUNNO}.Elog_upload.log
fi
if [ ${#REPLY2} -gt 3 ]; then
echo "Second reply needed for all attachements" >>  ~/AnalysisOutput/R${RUNNO}.Elog_upload.log
echo "
ssh -X alphadaq \"~/packages/elog/elog -h localhost -a Author=$HOSTNAME -a Run=\\\"$RUNNO\\\" -a Tags=\\\"$TAG\\\" -a Subject=\\\"$ELOG_SUBJECT\\\"  -r $ELOG_NO $REPLY1 -p 8080 -l AutoAnalysis  -v " >> ~/AnalysisOutput/R${RUNNO}.Elog_upload.log
ssh -X alphadaq "~/packages/elog/elog -h localhost -a Author=$HOSTNAME -a Run=\"$RUNNO\" -a Tags=\"$TAG\" -a Subject=\"$ELOG_SUBJECT\" -m \"Additional attachments 2\" -e $ELOG_NO $REPLY2 -p 8080 -l AutoAnalysis  -v " &>> ~/AnalysisOutput/R${RUNNO}.Elog_upload.log
fi
if [ ${#REPLY3} -gt 3 ]; then
echo "Third reply needed for all attachements" >>  ~/AnalysisOutput/R${RUNNO}.Elog_upload.log
echo "
ssh -X alphadaq \"~/packages/elog/elog -h localhost -a Author=$HOSTNAME -a Run=\\\"$RUNNO\\\" -a Tags=\\\"$TAG\\\" -a Subject=\\\"$ELOG_SUBJECT\\\"  -r $ELOG_NO $REPLY1 -p 8080 -l AutoAnalysis  -v " >> ~/AnalysisOutput/R${RUNNO}.Elog_upload.log
ssh -X alphadaq "~/packages/elog/elog -h localhost -a Author=$HOSTNAME -a Run=\"$RUNNO\" -a Tags=\"$TAG\" -a Subject=\"$ELOG_SUBJECT\" -m \"Additional attachments 3\" -e $ELOG_NO $REPLY3 -p 8080 -l AutoAnalysis  -v " &>> ~/AnalysisOutput/R${RUNNO}.Elog_upload.log
fi
echo "done"


echo "Moving files to alphadaq"
wait
rsync --remove-source-files --ignore-existing /home/alpha/alphaSoftware2012/alphaAnalysis/data/tree*${RUNNO}*.root alpha@alphadaq:/alpha/data39a/alpha/tree_files_2018/

rsync --remove-source-files --ignore-existing /home/alpha/alphaSoftware2012/alphaAnalysis/data/*tree*${RUNNO}*.root alpha@alphadaq:/alpha/data39a/alpha/tree_files_2018/
echo "cleaning up files"
rm ${MIDASDIR}/run${RUNNO}sub00*.mid.gz




rsync --ignore-existing /home/alpha/alphaSoftware2012/alphaStrips/data/${RUNNO}/alphaStrips${RUNNO}offline.root alpha@alphadaq:/alpha/data39a/alpha/tree_files_2018/

DFSPATH="/run/user/500/gvfs/dav:host=dfs.cern.ch,ssl=true,prefix=%2Fdfs/Experiments/ALPHA/AutoAnalysis"
ssh alpha@alphadaq "mkdir ${DFSPATH}/${RUNNO}
exit
"
scp ~/AnalysisOutput/${RUNNO}/* alpha@alphadaq:${DFSPATH}/${RUNNO}/


wait




