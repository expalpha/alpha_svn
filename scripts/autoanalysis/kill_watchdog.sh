#!/bin/bash

PID=`pgrep watcher.sh`
echo "watcher.sh PID: $PID"
echo "killing..."
kill $PID
echo "done"
echo "Deleting crontab job"
crontab -r
echo "Crontab jobs:"
crontab -l

