#!/bin/bash


ANS=`pgrep watcher.sh`
echo "ANS:$ANS."
if [ -n "$ANS" ]; then
echo "Watcher running"
else
echo "Watcher not running"
echo "Starting watcher"
./watcher.sh &

echo "Cleaning and re-installing crontab settings"
crontab -r
crontab my_cron_backup.txt
echo "Crontab jobs:"
CRONLIST=`crontab -l`
echo "$CRONLIST"

ssh -X alphadaq "echo \"./watcher.sh started... \nCron job added to reload watcher if it dies: $CRONLIST \" | ~/packages/elog/elog -h localhost -a Author=$HOSTNAME -a Subject=\"Watcher script started on $HOSTNAME\"  -p 8080 -l AutoAnalysis  -v  "

fi

