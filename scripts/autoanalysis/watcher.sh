#!/bin/bash

while true; do

  #for i in `ls ~/watch_me`; do #check this folder for files with run numbers as names
   # jobcnt=(`jobs -p`)
   # JOBS=${#jobcnt[@]}
   JOBS=`pgrep screen | wc -l`
  echo "checking (${JOBS} screen jobs running)"
    cd ~/watch_me

    if [[ $JOBS -lt 4 ]]; then
    
      i=`ls [0-9][0-9][0-9][0-9][0-9] | sort | head -1`
 #     i=`ls [0-9][0-9][0-9][0-9][0-9] | sort | tail -1` #newest first
      if [ ${#i} -gt 0 ]; then
        echo "running $i"
        rm ~/watch_me/$i
        cd ~/
        screen -t run$i -d -m ./autoAnalysis.sh $i 
      fi
    else
      echo "$JOBS jobs running >= 4 CPUs...  sleeping 1 minute"
      sleep 60
    fi
 # done
  sleep 10
done
