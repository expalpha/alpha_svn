#!/bin/bash
RUNNO=${1}
source $(dirname $0)/../myconfig.sh &> /dev/null

cd ${RELEASE}/alphaStrips
ls ${STRIPFILES}/alphaStrips${RUNNO}offline.root
if [ -e ${STRIPFILES}/alphaStrips${RUNNO}offline.root ]; then
  echo "Strip file found... testing if its ok"
  
  # Test for 'no keys' error (alphaStrips not finished)
  root -l -b -q ${STRIPFILES}/alphaStrips${RUNNO}offline.root 
  FILE_CLOSED=`root -l -b -q ${STRIPFILES}/alphaStrips${RUNNO}offline.root 2>&1 >/dev/null | grep 'keys' `
  
  echo "file closed:$FILE_CLOSED."
  if [ ${#FILE_CLOSED} -gt 4 ]; then
    #alphaStrips not finished... 
    echo "alphaStrips${STRIPNO}offline.root corrupt... re-running alphaStrips"
    ./alphaStrips.exe --EOS midasdata/run${RUNNO}sub00000.mid.gz
  fi
else
  echo "Running alphaStrips..."
  ./alphaStrips.exe --EOS midasdata/run${RUNNO}sub00000.mid.gz
fi
echo "Running alphaAnalysis..."

cd ${RELEASE}/alphaAnalysis
./alphaAnalysis.exe --EOS midasdata/run${RUNNO}sub00000.mid.gz

echo "done"
