package main
 	

//import s "strings"
import (
	"fmt"
	"flag"
	"strconv"
	"time"
	"log"
	"os"
	"os/exec"
)

func fetchFromEOS( run int ) int {
  INDEX := 0
  ZEROS :=""
  filename :=""
  for i := 0; i < 150; i++ {
    
    //Pad the sum file number
    if INDEX < 10 {
		ZEROS = "0000"
    } else { 
		if INDEX < 100 { 
			ZEROS = "000"
		} else {
			ZEROS = "00"
		} 
	}
  
    filename= "run" + strconv.Itoa(run) + "sub" + ZEROS + strconv.Itoa(INDEX) + ".mid.gz"
    //fmt.Println(filename)
    //Test if file is on EOS
    out, err :=  exec.Command("eos","ls /eos/experiment/alpha/midasdata/"+filename).Output()
    //If file isn't there... wait for 60s
	if err != nil {
		fmt.Printf("%s does not exist... sleeping (%vs)\r",filename,i*60)	
		time.Sleep(60 * time.Second)
		continue
	} 
	log.Printf("Found file %s", string(out))
	
	//Copy file to current path
	log.Printf("Fetching "+filename+" from EOS")
	cpout, err := exec.Command("eos","cp /eos/experiment/alpha/midasdata/"+filename+" .").Output()
	i=0
	if err != nil {
		//If the copy file, maybe its incomplete, try again in 60s
		fmt.Printf("%s copy failed",filename)
		fmt.Printf(string(cpout))
		time.Sleep(60 * time.Second)
		continue
	}
	
	log.Printf(filename+" copied...\n")
	
	fi, e := os.Stat(filename);
	if e != nil {
		log.Fatal(e)
	}
	
	//get the file size
    size := fi.Size()
    os.Rename(filename,os.Getenv("MIDASDIR")+"/"+filename)
	log.Printf("Moving "+filename+ " to " + os.Getenv("MIDASDIR"))
    if (size<1900000000) {
		fmt.Printf("Last midas file detected...")
		return INDEX
	}

	INDEX+=1
    
  }
  fmt.Printf("fetchFromEOS timed out with file: " + filename)
  return 1
}




func main() {
	fmt.Println("Starting fetch from EOS...")
	
	flag.Parse()
	s := flag.Arg(0)

	if i, err := strconv.Atoi(s); err == nil {
		fetchFromEOS(i)
	}
}
