#!/bin/bash

C_FILE_INC_PATH=${1}

DIR=$(dirname "${C_FILE_INC_PATH}")
FILE=$(basename "${C_FILE_INC_PATH}")
cd ${DIR}
CMD=`ctags -u -f - --c-kinds=-v ${FILE}`
#CMD=`ctags -u -f - --c-kinds=-v -x /%s/\t/%s $1`
#CMD=`etags -f - --no-globals $1`
IFS=$'\n'

echo "// ******************************************************* "> ${FILE}.idx
echo "// Automatically Generated Function Index for ${FILE}">> ${FILE}.idx
echo "// ******************************************************* ">> ${FILE}.idx
for LINE in $CMD; do
    echo "// $LINE">> ${FILE}.idx
done

cd -
