#!/bin/bash

RUNNO=$1
cd /local/alphacdr/alphaSoftware2012
echo $RELEASE
source myconfig.sh &> /dev/null
EOS_READY=0
TIME_WAITED=0
while [ $EOS_READY -eq 0 ]; do
  EOS_SIZE=`/afs/cern.ch/project/eos/installation/0.3.15/bin/eos.select ls -l /eos/experiment/alpha/midasdata/run${RUNNO}sub00000.mid.gz | awk -F$' ' '{print $5}'`
  #if [ -z `echo "$EOS_SIZE" | grep "No such file or directory"` ]; then
#  if [ -I "$EOS_SIZE" ]; then
  if [ "$EOS_SIZE" -eq "$EOS_SIZE" ] 2>/dev/null; then
    EOS_HAS_FILE=1
  else
    EOS_SIZE=-1
    echo "No file found on EOS yet..."
  fi
  DAQ_SIZE=`ssh alphadaq ls -l /home/alpha/alphaSoftware2012/alphaAnalysis/midasdata/run${RUNNO}sub00000.mid.gz`
  DAQ_SIZE=`echo "$DAQ_SIZE" | awk -F$' ' '{print $5}'`
  if [ $EOS_SIZE -eq $DAQ_SIZE ]; then
    EOS_READY=1
    echo "Files on EOS and alphadaq match sizes... Good"
  elif [ ${#DAQ_SIZE} -lt 1 ]; then
     EOS_READY=1
     echo "Files only on EOS"
  else 
    echo "Files on EOS and alphadaq do not match in size: $EOS_SIZE vs $DAQ_SIZE... sleeping "
    sleep 5
    TIME_WAITED=$((${TIME_WAITED} + 5))
    if [ $TIME_WAITED -gt 300 ]; then
      echo "waited more than 300s, error with alphadaq or EOS?...exiting"
      exit
    fi
  fi
done

#ps aux | grep Lazy_analysis.sh 
#echo `ps aux | grep ./Lazy_analysis.sh | wc -l`

while [ `ps aux | grep -e alphaAnalysis.exe -e alphaStrips.exe | wc -l` -gt 2 ]; do
sleep 10
echo "sleeping"
done

while [ `ps aux | grep -e alphaAnalysis.exe -e alphaStrips.exe | wc -l` -gt 2 ]; do
sleep 10
echo "sleeping"
done
#if [ `ps aux | grep ./Lazy_analysis.sh | wc -l` -lt 4 ]; then


echo $RELEASE
cd $RELEASE/alphaStrips
./alphaStrips.exe --EOS midasdata/run${RUNNO}sub00000.mid.gz
cd $RELEASE/alphaAnalysis
./alphaAnalysis.exe --EOS midasdata/run${RUNNO}sub00000.mid.gz 
cd $RELEASE/alphaAnalysis/macros/
TYPE=`source  GetAnalysisTypeFromSequenceDumps.sh ${RUNNO} | grep Cosmic`
if [ ${#TYPE} -gt 3 ]; then
cd $RELEASE/alphaAnalysis
./alphaAnalysis.exe --EOS --iscosmic --cosmicutil midasdata/run${RUNNO}sub00000.mid.gz
fi

echo "Copying files to EOS:"
shopt -s expand_aliases
eos cp $RELEASE/alphaAnalysis/data/tree${RUNNO}*.root /eos/experiment/alpha/alphaTrees/ && \
eos cp $RELEASE/alphaStrips/data/alphaStrips${RUNNO}*.root /eos/experiment/alpha/stripTrees/ && \
echo "Copying done... deleting files" &&\
rm -v $RELEASE/alphaAnalysis/data/*${RUNNO}*.root 
rm -v $RELEASE/alphaAnalysis/midasdata/*${RUNNO}*.mid.gz 

#else
#echo "sleeping" 
#sleep 100
#fi
