#!/bin/bash


echo "Running Analysis..."
SOURCE="${BASH_SOURCE[0]}"
	echo $SOURCE

	while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
      DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
      SOURCE="$(readlink "$SOURCE")"
      [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
	done
	DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
cd $DIR    
cd ../../
source myconfig.sh

cd $RELEASE
SVN=`svn info | grep "Revision" | awk '{print $2}'`
svn log -r${SVN} >> "~/SVNUpdateLog.log"
#rm -v $RELEASE/alphaAnalysis/macros/*.csv

start=$(date +%s.%N)
echo "Running 39032"

date
RUNNO="38032"
echo "

===================== $RUNNO =====================
">~/AnalysisMacro.log
cd $RELEASE/alphaStrips
./alphaStrips.exe midasdata/run${RUNNO}sub00000.mid.gz > R${RUNNO}.log
cd $RELEASE/alphaAnalysis
./alphaAnalysis.exe midasdata/run${RUNNO}sub00000.mid.gz > R${RUNNO}.log
cd $RELEASE/alphaAnalysis/macros
echo ".L PlotDumps.C+

.q
" | root -l -b &> ~/PlotDumpsCompile.log


echo ".L PlotDumps.C+
gProgressBars=0;
PrintDetectorQOD($RUNNO,0.,-1,\"Cosmic-SVN$SVN\")
.q " | root -l -b &>> ~/AnalysisMacro.log

echo "Running 39993 -mixing run"

date
RUNNO="39993"
echo "

=====================$RUNNO - Mixing=====================
">>~/AnalysisMacro.log
cd $RELEASE/alphaStrips
./alphaStrips.exe midasdata/run${RUNNO}sub00000.mid.gz > R${RUNNO}.log
cd $RELEASE/alphaAnalysis
./alphaAnalysis.exe midasdata/run${RUNNO}sub00000.mid.gz > R${RUNNO}.log
cd $RELEASE/alphaAnalysis/macros
echo ".L PlotDumps.C+
gProgressBars=0;
PrintDetectorQOD($RUNNO,\"Mixing\",1,0,\"Mixing-SVN$SVN\")
.q " | root -l -b &>> ~/AnalysisMacro.log

echo "Running long cosmic run"

date
date
RUNNO="40258"
echo "

===================== $RUNNO - Cosmics =====================
">>~/AnalysisMacro.log
cd $RELEASE/alphaStrips
./alphaStrips.exe midasdata/run${RUNNO}sub00000.mid.gz > R${RUNNO}.log
cd $RELEASE/alphaAnalysis
/usr/bin/time -v ./alphaAnalysis.exe midasdata/run${RUNNO}sub00000.mid.gz &> R${RUNNO}.log
cp R${RUNNO}.log ~/LongCosmic_MemUsage.log
cd $RELEASE/alphaAnalysis/macros
echo ".L PlotDumps.C+
gProgressBars=0;
PrintDetectorQOD($RUNNO,0.,-1,\"Cosmic-SVN$SVN\")
.q " | root -l -b &>> ~/AnalysisMacro.log

echo "done"
date




cd /home/alpha/
echo "Running root macro and emailing report"

dur=$(echo "$(date +%s.%N) - $start" | bc); \
printf "[code] Execution time: %.1f seconds\n" $dur > ExecutionTime_R${SVN}.log
#echo "[code] Execution time: $dur seconds" > ExecutionTime_R${SVN}.log
echo "[/code]" > CodeEnd.txt

#root -l -b -q -x CheckTwoRuns.C &> AnalysisMacro.log

#cat SVNUpdateLog.log AnalysisMacro.log | mail -s "SVNChecker report SVN r$SVN" root
echo "Analysis results diff:">Last_Diff_Report.log
diff -u LastAnalysisMacro.log AnalysisMacro.log >> Last_Diff_Report.log
cp AnalysisMacro.log LastAnalysisMacro.log


cat ExecutionTime_R${SVN}.log SVNUpdateLog.log Last_Diff_Report.log CodeEnd.txt > SVN_Report_R${SVN}.log
#diff -u Last_SVN_Report.log SVN_Report_R${SVN}.log > Last_Diff_Report.log
#cp SVN_Report_R${SVN}.log  Last_SVN_Report.log
cd $RELEASE/alphaAnalysis/macros/
CSVs=""
for i in `ls *SVN${SVN}*.csv`; do
CSVs="$CSVs -f ~/SVNCheckerReports/${i} "
done
echo "$CSVs"

cd /home/alpha/
cat SVN_Report_R${SVN}.log | head -c 80000  > temp.log
cp temp.log SVN_Report_R${SVN}.log
scp SVN_Report_R${SVN}.log Last_Diff_Report.log AnalysisMacro.log LongCosmic_MemUsage.log $RELEASE/alphaAnalysis/macros/*SVN${SVN}*.csv alpha@alphadaq:~/SVNCheckerReports/


ssh -X alphadaq "~/packages/elog/elog -h localhost -a Author=$HOSTNAME -a Subject=\"SVNChecker report SVN r$SVN\" -m ~/SVNCheckerReports/SVN_Report_R${SVN}.log -f ~/SVNCheckerReports/AnalysisMacro.log ${CSVs} -f ~/SVNCheckerReports/LongCosmic_MemUsage.log -p 8080 -l AutoAnalysis -v " &> elog_posting.log
ELOG_NO=`cat elog_posting.log  | grep ID= | tr 'Message successfully transmitted, ID=' "\n"| grep [0-9]`

#Leak check as a reply
cd $RELEASE/alphaAnalysis
valgrind --leak-check=yes --error-limit=no  --suppressions=${ROOTSYS}/etc/valgrind-root.supp  --log-file="LeakCheck_R${SVN}.log" ./alphaAnalysis.exe midasdata/run39993sub00000.mid.gz
touch LeakSummary_R${SVN}.log
cat LeakCheck_R${SVN}.log | tail -n 18 > LeakSummary_R${SVN}.log
scp LeakCheck_R${SVN}.log LeakSummary_R${SVN}.log alpha@alphadaq:~/SVNCheckerReports/

cd ${RELEASE}/alphaMVA/dumpers/dumper
export MVATREES=$TREEFILES
valgrind --leak-check=yes --error-limit=no --suppressions=${ROOTSYS}/etc/valgrind-root.supp  --log-file="LeakCheck_R${SVN}_dumper.log" ./dumper.exe ${TREEFILES}/tree39993offline.root ALL 
touch LeakSummary_R${SVN}_dumper.log
cat LeakCheck_R${SVN}_dumper.log | tail -n 18 > LeakSummary_R${SVN}_dumper.log
scp LeakCheck_R${SVN}_dumper.log LeakSummary_R${SVN}_dumper.log alpha@alphadaq:~/SVNCheckerReports/


echo            "~/packages/elog/elog -h localhost -a Author=$HOSTNAME -a Subject=\"SVNChecker leak test SVNr$SVN\" -r $ELOG_NO -m  ~/SVNCheckerReports/LeakSummary_R${SVN}.log -f ~/SVNCheckerReports/LeakCheck_R${SVN}.log -f ~/SVNCheckerReports/LeakCheck_R${SVN}_dumper.log -p 8080 -l AutoAnalysis  -v  "&>> elog_posting_reply.log
ssh -X alphadaq "~/packages/elog/elog -h localhost -a Author=$HOSTNAME -a Subject=\"SVNChecker leak test SVNr$SVN\" -r $ELOG_NO -m  ~/SVNCheckerReports/LeakSummary_R${SVN}.log -f ~/SVNCheckerReports/LeakCheck_R${SVN}.log -f ~/SVNCheckerReports/LeakCheck_R${SVN}_dumper.log -p 8080 -l AutoAnalysis  -v " &>> elog_posting_reply.log



date
 cat elog_posting.log 
 cat elog_posting_reply.log 

echo "done"
