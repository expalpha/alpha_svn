#!/bin/bash

case `hostname` in
alphasvnchecker*)
    echo "Good, we are on alphasvnchecker"
    ;;
*)
    echo "This script is only for alphasvnchecker..."
    exit 1
    ;;
esac

if [ $( ps aux | grep CheckSVN.sh | wc -l ) -gt 3 ]
then
echo "SVN Checker running...exiting"
exit
fi
SOURCE="${BASH_SOURCE[0]}"
	echo $SOURCE

	while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
      DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
      SOURCE="$(readlink "$SOURCE")"
      [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
	done
	DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
cd $DIR    
cd ../../
export RELEASE=`pwd`

CURRENTSVN=`svn info | grep "Revision" | awk '{print $2}'`
svn up &> /home/alpha/SVNUpdateLog.log
NEWSVN=`svn info | grep "Revision" | awk '{print $2}'`


#if change... compile...
if [ $CURRENTSVN -eq $NEWSVN ]
then
  echo "SVN unchanged!"
  exit 0
else
  cd /home/alpha/
  echo "Compiling.."
  cd ${RELEASE}/scripts/svnchecker/
  ./UpdateAndReCompile.sh &> ~/LastUpdate.log
  echo "Analysing.."
  cd ${RELEASE}/scripts/svnchecker/
  ./AnalyseSelectedRuns.sh &> ~/Analysis.log
  echo "Writing reports.."
  cd ${RELEASE}/alphaAnalysis/macros/
  ./BuildSpreadsheetBaseline.sh &> ~/Spreadsheet.log
  cd ~/
fi
