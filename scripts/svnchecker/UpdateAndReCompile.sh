#!/bin/bash

case `hostname` in
alphasvnchecker*)
    echo "Good, we are on alphasvnchecker"
    ;;
*)
    echo "This script is only for alphasvnchecker..."
    exit 1
    ;;
esac
SOURCE="${BASH_SOURCE[0]}"
	echo $SOURCE

	while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
      DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
      SOURCE="$(readlink "$SOURCE")"
      [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
	done
	DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
cd $DIR    
cd ../../
source myconfig.sh
make clean
rm -vf main.db
echo "Updating sqldatabase"
cd $RELEASE/aux
# review "dir_table" entry in $RELEASE/aux/settings.sql
sqlite3 main.db < settings.sql

echo "Compiling everything"
cd $RELEASE
rm -rf alphavmc/geant3
rm -rf rootana

make

#echo "Compiling alphavmc"
#cd $RELEASE/alphavmc/
#make clean && make

#echo "Compiling alphaAnalysis/lib"
#cd $RELEASE/alphaAnalysis/lib
#make clean && make

#echo "Compiling alphaAnalysis"
#cd $RELEASE/alphaAnalysis
#make clean && make

#echo "Compiling alphaAnalysis macros utils"
#cd $RELEASE/alphaAnalysis/macros
#make clean && make

#echo "Compiling alphaStrips"
#cd $RELEASE/alphaStrips/
#make clean && make


cd /home/alpha/
echo "================"
echo "COMPILATION DONE"
echo "================"
