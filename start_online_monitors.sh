#!/bin/sh


case `hostname` in
alphadaq*)
    echo "Good, we are on alphadaq"
    ;;
*)
    echo "The start_daq script should be executed on alphadaq"
    exit 1
    ;;
esac

cd $RELEASE/alpha2events
#cd $RELEASE/alphaonline
#./alphaonline.exe &

./alpha2dumps.exe &
