#!/bin/bash

#A bash script to submit a list of analyses to the VMCluster... It checks how many jobs are running, and limits the jobs on a node to 4 (the number of processors per node).

# Bash syntax.
    shuffle() {
       local i tmp size max rand
    
       # $RANDOM % (i+1) is biased because of the limited range of $RANDOM
       # Compensate by using a range which is a multiple of the array size.
       size=${#array[*]}
       max=$(( 32768 / size * size ))
   
      for ((i=size-1; i>0; i--)); do
         while (( (rand=$RANDOM) >= max )); do :; done
         rand=$(( rand % (i+1) ))
         tmp=${array[i]} array[i]=${array[rand]} array[rand]=$tmp
      done
   }


if [ ${#RELEASE} -gt 2 ]; then
  echo "myconfig sourced... good"
else
  echo "Please source myconfig.sh"
  exit
fi

Hostlist="alphavmcluster01
alphavmcluster02
alphavmcluster03
alphavmcluster04
alphavmcluster05
alphavmcluster06
alphavmcluster07
alphavmcluster08
alphavmcluster09
alphavmcluster10
alphavmcluster11
alphavmcluster12
alphavmcluster13
alphavmcluster14
alphavmcluster15
alphavmcluster16
alphavmcluster17
alphavmcluster18
alphavmcluster19" #Full list of VMs
Hostlist="alphavmcluster03
alphavmcluster04
alphavmcluster05
alphavmcluster06
alphavmcluster07
alphavmcluster08
alphavmcluster09
alphavmcluster10
alphavmcluster11
alphavmcluster12
alphavmcluster14
alphavmcluster15
alphavmcluster17
alphavmcluster18" #List of VMs with broken or reserved machines removed



RunList=$1


for RUNNO in $(cat $RunList); do
  echo "Run: $RUNNO"
#  sleep 1
 Hostlist=` echo "$Hostlist" | sort -R`  #Shuffle host list... 

  JOBSENT=0
  while [[ JOBSENT -eq 0 ]]
  do
    for HOSTNAME in $Hostlist
    do
      #echo "Doing ping"
      #if ping -c 1 $HOSTNAME > /dev/null
      #then
        #echo "Run: $RUNNO"
        
        #Old:
        #AnalysisCount=`ssh $HOSTNAME ps aux | grep alphaAnalysis.exe | wc -l`
        #StripsCount=`ssh $HOSTNAME ps aux | grep alphaStrips.exe | wc -l`
        #RootCount=`ssh $HOSTNAME ps aux | grep root.exe | wc -l`
        #JOBS=$((AnalysisCount + StripsCount + RootCount))
        JOBS=`ssh $HOSTNAME ps aux | grep alphaBATCH | wc -l`
        echo " $HOSTNAME $JOBS"
        
        if [[ $JOBS -lt 4 ]]
        then
          echo "Submitting run $RUNNO to $HOSTNAME at"
          date
          #Set up which analysis folder you would like to use here:
          NODE_ANALYSIS_FOLDER="${RELEASE}"
          ssh -t $HOSTNAME bash -c "'
          echo \"alphaBATCH analysis running\" #This line is needed to count number of processes on node...
          cd $NODE_ANALYSIS_FOLDER
          source myconfig.sh &> /dev/null
          cd $NODE_ANALYSIS_FOLDER/alphaStrips
          ./alphaStrips.exe --EOS midasdata/run${RUNNO}sub00000.mid.gz &> ${NODE_ANALYSIS_FOLDER}/R${RUNNO}-strips.log
          cd $NODE_ANALYSIS_FOLDER/alphaAnalysis
          ./alphaAnalysis.exe --EOS midasdata/run${RUNNO}sub00000.mid.gz &> ${NODE_ANALYSIS_FOLDER}/R${RUNNO}-Analysis.log
          
          echo "$RUNNO done on $HOSTNAME at"
          date
            '" &
          echo "$COMMAND"
          sleep 1
          
          JOBSENT=1
          
          break
        
          #echo "Node busy ($HOSTNAME)"
          #sleep 1
        fi

      #else #if failed ping
      #  echo "$HOSTNAME unreachable"
      #fi
    done
    if [[ $JOBSENT -eq 0 ]]
    then
      echo "No free cpus... sleeping"
      sleep 60
    
    fi
  done
done
jobcnt=(`jobs -p`)
#while [[ ${jobcnt[@]} -gt 1 ]]
#do
  echo "		Waiting for ${#jobcnt[@]} job(s) to finish"
#  sleep 30
#done
wait
echo "wait over"
#done < "$RunList"
